import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
    font-family: 'Roboto';
    background-color: #FFFFFF !important;
  }

  body p {
    font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }
  .file-button {
    display: inline-block!important;
   }
  .btn-default  {
    font-family: 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
   }
   .content-page {
    background-color: #FFFFFF !important;
   }
   /* #F1EEEE */
   .navbar-default {
    background-color: #FFFFFF !important;
   }
   .navbar-nav>li>.dropdown-menu {
    background-color: #FFFFFF !important;
   }
   .send-payment h4, .make-deposit h4 {
    font-family: Roboto;
    font-weight: bold;
   }
   .send-payment h4, .make-deposit button strong {
    font-family: Roboto;
    font-weight: bold;
   }
   .send-payment button[type="submit"] {
    font-family: Roboto;
    font-weight: bold;
   }
   .ui.table thead tr th {
    font-family: "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
    font-weight: normal;
   }
   .ui.table tbody tr td {
    font-family: "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
    font-weight: normal;
   }
   .content-box {
      box_sizing: border-box;
      border: 0px;
   }
   .current-value  {
    font-family: 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-weight: normal !important;
   }
   .segment-value {
    font-weight: normal !important;
   }
   table.tr.td.disabled {
    color: #A00
   }
   .modal {
    position: relative;
    font-size: 1.3rem !important;
   }
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
    -webkit-appearance: none; 
    margin: 0; 
   }
   .uploadFile div {
    width: 100% !important
   }
   .dropZone {
    border: 3px solid #fc5109;
    border-style: dotted;
    min-height: 150 !important;
   }
   /* calendar */
   .DayPicker-Day {
      display: table-cell;
      padding: 0.5em;
      border: 1px solid #EEE !important;
      margin: 0.5em !important;
      border-radius: 10% !important;
      vertical-align: middle;
      text-align: center;
      cursor: pointer;
    }
   .DayPicker-Day {
    color: #FFF !important;
    background-color: #004100;
    font-weight: 700;
   }
   .DayPicker:not(.DayPicker--interactionDisabled)
      .DayPicker-Day:not(.DayPicker-Day--disabled):not(.DayPicker-Day--selected):not(.DayPicker-Day--outside):hover {
      background-color: #2CB32C !important;
    }
   .DayPicker-Day--selected:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside):hover {
      background-color: #2CB32C !important;
    }
    .DayPicker-Day--selected:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside) {
      position: relative;
    
      background-color: #0F9A0F !important;
      color: #FFF !important;
    }
   .DayPicker-Day--today {
     background-color: #FFF;
     color: #000 !important;
     font-weight: bold !important;
   }
  .DayPicker {
    background-color: #FFF;
    display: inline-block;
    font-size: 1.3rem !important;
  }
  .DayPickerInput {
    width: 100% !important
  }
  .DayPicker-Day--disabled {
    background-color: #FFF;
    color: #CCC !important;
    cursor: default;
    text-decoration: line-through !important;
    text-decoration-style: double !important;
    text-decoration-color: #000 !important;
    /* background-color: #eff1f1; */
  }
  .DayPicker-Day--sunday:not(.DayPicker-Day--today) {
    background-color: #FFF;
    color: #0A0 !important;
  }
  .DayPicker-Day--outside {
    background-color: #FFF;
    color: #8B9898;
    cursor: default;
    text-decoration: line-through !important;
    text-decoration-style: wavy !important;
    text-decoration-color: red !important;
  }
  .dropdown .top-menu-item-xs .open {
    display: block !important;
  }
  .ui.dropdown .menu>.item {
    font-size: 1.3rem !important;
  }
  
  /*button*/
  .ui.button {
    font-size: 1.5rem !important;
  }
  /*label*/
  .ui.label, .ui.labels .label {
    font-size: 1.5rem !important;
  }
  .ui.form .field .ui.input input, .ui.form .fields .field .ui.input input {
    font-size: 1.5rem !important;
  }
  .ui.form textarea {
    font-size: 1.5rem !important;
  }
  .ui.steps {
    font-size: 1.5rem !important;
  }
  .ui.header {
    font-size: 2rem !important;
  }
  .ui.pagination.menu .item {
    font-size: 1.5rem !important;
  }
  .ui.toggle {
    font-size: 1.5rem !important;
  }
  .ui.table tr.error {
    background: #fff6f6!important;
    color: #9f3a38!important;
    font-size: inherit!important;
  }
  .ui.vertical.menu .item {
    font-size: 1.4rem !important;
    color: #000000!important;
  }
  .menuLink:link {
    color: #000000!important;
  }
  .menuLink:hover {
    color: #000000!important;
  }
  .menuLink:visited {
    color: #000000!important;
  }
  .sessionModalHeader {
    border-bottom: 0px !important;
    padding: 4px !important;
  }
  .sessionModalAction {
    background: #ffffff !important;
    padding: 1rem 1rem;
    border-top: 0px !important;
    text-align: right;
  }
  .materialInput {
    font-family: 'Roboto' !important;
    border-bottom: 2px solid #737272 !important;
    border-top: 0px !important;
    border-right: 0px !important;
    border-left: 0px !important;
    border-radius: 0px !important;
  }
  
  .materialInput:focus {
    font-family: 'Roboto' !important;
    border-bottom: 2px solid #fc5109 !important; /* #fc5109 */
    border-top: 0px !important;
    border-right: 0px !important;
    border-left: 0px !important;
    border-radius: 0px !important;
  }
  
  .materialSelection {
    font-family: 'Roboto' !important;
    border-top: 0px !important;
    border-right: 0px !important;
    border-left: 0px !important;
    border-bottom: 2px solid #737272 !important;
    border-radius: 0px !important;
  }
  
  .buttonSubmit {
    font-family: 'Roboto' !important;
    font-weight: bold;
    background-color: #fc5109 !important;
    height: 70px;
  }
  
  .buttonSubmitCancel {
    margin-top: 20px !important;
    font-family: 'Roboto' !important;
    font-weight: bold;
    background-color: #BBB !important;
    height: 70px;
  }
  .contentSeparator {
    margin-bottom: 40px; 
   }
   .headerClass {
    color: #737272;
    font-weight: 'bold';
    font-family: Roboto;
   }  
  .DayPickerInput input {
    font-family: 'Roboto' !important;
    border-bottom: 2px solid #737272 !important; /* #fc5109 */
    border-top: 0px !important;
    border-right: 0px !important;
    border-left: 0px !important;
    border-radius: 0px !important;
  }
  
  .DayPickerInput input:not([type]):focus {
    font-family: 'Roboto' !important;
    border-bottom: 2px solid #fc5109 !important; /* #fc5109 */
    border-top: 0px !important;
    border-right: 0px !important;
    border-left: 0px !important;
  }
  .content-page > .content {
    min-height: 740px !important;
  }
  
  .userTypesClass {
    font-size: 1.5rem !important;
  }
  
  .userViewField {
    font-size: 1.5rem !important;
    padding-left: 10px;
  }
  
  .ui.segment, .ui.segments .segment {
    font-size: 1.5rem;
  }
  
  .segmentStyle {
    margin-top: 50px !important;
    margin-left: auto !important;
    margin-right: auto !important;
    width: 55%;
  }
  .segmentStyleTerms {
    margin-left: auto !important;
    margin-right: auto !important;
    width: 80%;
  }
  
  .profileLabel {
    font-size: 18px !important;
    margin-left: 10px !important;
    font-weight: bold !important;
  }
  
  .profileData {
    font-size: 16px !important;
    margin-left: 20px !important;
    margin-top: 10px !important;
    width: 50% !important;
  }
  
  .ui.grid.tabGrid {
    min-width: 900px !important;
  }
  
  .ui.tabular.menu .item {
    font-size: 1.7rem !important;
  }
  
  .tabMain {
    padding-left: 20px !important;
    padding-right: 20px !important;
  }
  
  .buttonSaveProfile {
    float: right !important;
    width: 100%;
    padding-right: 20px;
  }
  
  .listStyle {
    padding: 20px;
  }
  
  .ui.popup>.ui.grid:not(.padded) {
    width: calc(100% + 1.75rem) !important;
    margin: -.7rem -.875rem !important;
    font-size: 1.5rem !important;
    padding: 3px;
  }
  
  .advanceFilterPopup {
    width: 40% !important;
    padding: 20px !important;
  }
  .advanceFilterComponent {
    margin-left: 5px;
  }
  .advanceFilterGrid {
    border: 1px solid #CCC !important;
    padding: 10px !important;
  }

  .advanceFilterLabel {
    font-weight: 1000;
  }
  .advanceFilterApply {
    text-align: right;
  }
  .advanceFilterCalendar {
    font-size: 1.3rem !important;
  }
  .advanceFilterDatePicker {
    font-size: 1.5rem !important;
    padding: 5px;
    width: 100%;
    border-radius: 5px;
    border: 1px solid #CCC;
  }
  .profileImageDiv {
    text-align: center;
  }
  .profileImageUpload {
    padding-top: 10px;
    padding-bottom: 10px;
  }
  #profilePicture {
    display: none;
  }
  .previewImg {
    border-radius: 50%;
    width: 100px;
    height: 100px;
  }
  .userPageImage {
    border-radius: 100%;
    width: 50px !important;
    height: 50px !important;
  }
  .profilePictureTopBar {
    border-radius: 100%;
    width: 25px;
    height: 25px;
  }
  .card-box-dashboard {
    padding: 20px;
    border: 1px solid #36404a0d;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    -moz-border-radius: 5px;
    background-clip: padding-box;
    margin-bottom: 20px;
    background-color: #fff;
    min-height: 200px;
  }


  .tablink {
    font-family: Roboto;
    font-size: 24px;
    font-weight: 300;
    color: #fc5109;
    background: none;
    border: none;
    padding: 10px 0;
    outline: none;
    border-radius: 40px;
    margin: 0;
    display: inline-block;
    width: 49%;
  }
  .tablinksTransaction {
    font-family: Roboto;
    font-size: 24px;
    font-weight: 300;
    color: #fc5109;
    background: none;
    border: none;
    padding: 10px 0;
    outline: none;
    border-radius: 40px;
    margin: 0;
    display: inline-block;
    width: 49%;
  }
  .tablinksPickup {
    font-family: Roboto;
    font-size: 24px;
    font-weight: 300;
    color: #fc5109;
    background: none;
    border: none;
    padding: 10px 0;
    outline: none;
    border-radius: 40px;
    margin: 0;
    display: inline-block;
    width: 49%;
  }
  .tab-active {
    color:#fff;
    background-color:#fc5109;
  }
  .service-mode-button {
    border: 3px solid #fc5109;
    max-width: 320px;
    margin: 20px auto 60px;
    border-radius: 40px;
    padding: 2px !important;
  }
  .payment-deposit-border {
    border: 2px solid #f25218;
  }
  .member-title {
    padding: 10px;
  }
  label {
    font-family: inherit !important;
    font-size: 10pt !important;
    font-weight: bold !important;
  }
  .sigCanvas {
    border: 2px dashed #f25218 !important;
    text-align: center !important;
  }
  
  /**
  .segmentStyleTerms {
    margin-left: auto !important;
    margin-right: auto !important;
    width: 80%;
  }
  **/
  @media only screen and (max-width: 400px) {
    .segmentStyleTerms {
      margin-left: auto !important;
      margin-right: auto !important;
      width: 95% !important;
    }  
  }
  
  /* The container */
  .container_checkbox {
    margin-top: 20px !important;
    display: block !important;
    position: relative !important;
    padding-left: 35px !important;
    margin-bottom: 12px !important;
    cursor: pointer !important;
    font-size: 15px !important;
    color: #000 !important;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none ;
    user-select: none !important;
  }

  .container_checkbox input {
    position: absolute !important;
    opacity: 0 !important;
    cursor: pointer !important;
    height: 0 !important;
    width: 0 !important;
  }

  ._checkmark {
    position: absolute !important;
    top: 0 !important;
    left: 0 !important;
    height: 25px !important;
    width: 25px !important;
    background-color: #eee !important;
  }
  
  .container_checkbox:hover input ~ ._checkmark {
    background-color: #ccc !important;
  }
  
  .container_checkbox input:checked ~ ._checkmark {
    background-color: #f2711c !important;
  }
  
  .container_checkbox:after {
    content: "" !important;
    position: absolute;
    display: none;
  }
  
  /* Show the checkmark when checked */
  .container_checkbox input:checked ~ ._checkmark:after {
    display: block !important;
  }
  
  /* Style the checkmark/indicator */
  .container_checkbox ._checkmark:after {
    left: 9px !important;
    top: 5px !important;
    width: 5px !important;
    height: 10px !important;
    border: solid white !important;
    border-width: 0 3px 3px 0 !important;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
  }
  
  .terms-card-box {
    padding: 20px;
    border: 1px solid #36404a0d;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    -moz-border-radius: 5px;
    background-clip: padding-box;
    margin-bottom: 20px;
    background-color: #fff;
    min-height: 200px;
  }
  .confirmText {
    color: #000 !important;
  }
  .formModal {
    font-size: 1.5rem !important;  
  }
  .inputWidth {
    width: 100% !important;
  }
`;
