/**
 *
 * MainAccount
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Divider } from 'semantic-ui-react';
import Gravatar from 'react-gravatar';
// import styled from 'styled-components';

function MainAccount({ currentUser }) {
  const parentUser = currentUser.parent_user;
  if (!parentUser) return null;
  return (
    <Grid>
      <Grid.Row>
        <Grid.Column>
          {parentUser.profile_picture ? (
            <img
              src={parentUser.profile_picture}
              alt=""
              className="previewImg"
            />
          ) : (
            <Gravatar
              email={parentUser.email}
              size={100}
              rating="pg"
              default="identicon"
              className="img-circle"
            />
          )}
          <span style={{ padding: 10, fontSize: 25 }}>
            {`${parentUser.first_name} ${parentUser.last_name}`}
          </span>
          {`@${parentUser.username}`} <b>(Main Account)</b>
        </Grid.Column>
      </Grid.Row>
      <Divider />
    </Grid>
  );
}

MainAccount.propTypes = {
  currentUser: PropTypes.object.isRequired,
};

export default MainAccount;
