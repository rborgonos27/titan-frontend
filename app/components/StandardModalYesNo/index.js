/**
 *
 * StandardModal
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal } from 'semantic-ui-react';
import styled from 'styled-components';
import titanLogo from 'images/titan-logo.png';

const ModalHeader = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: #fc5109;
`;

const CloseButton = styled.section`
  text-align: right;
  padding 10px;
  position: absolute;
  top: 15px;
  right: 0;
  width: 100px;
  text-align:right;
`;

const A = styled.a`
  font-size: 2em;
  cursor: pointer;
  color: #cccccc;
`;

const BodySection = styled.section`
  font-size: 1.3em;
  text-align: center;
  margin-left: 2em;
  margin-right: 2em;
`;

const Img = styled.img`
  align: left;
  width: 45px;
  height: 45px;
  position: absolute;
`;

const StandardModalYesNo = ({
  headerText,
  children,
  open,
  onClose,
  onActionText,
  onContinue,
  btnDisabled,
  noActionButton,
  onActionAddNewText,
}) => (
  <div>
    <Modal open={open}>
      <Modal.Header className="sessionModalHeader">
        <Img src={titanLogo} alt="" />
        <ModalHeader>{headerText}</ModalHeader>
        <CloseButton>
          <A onClick={onClose}>X</A>
        </CloseButton>
      </Modal.Header>
      <Modal.Content>
        <BodySection>{children}</BodySection>
      </Modal.Content>
      <Modal.Actions className="sessionModalAction">
        <Button disabled={btnDisabled} onClick={noActionButton}>
          {onActionAddNewText || 'No'}
        </Button>
        <Button disabled={btnDisabled} onClick={onContinue}>
          {onActionText || 'OK'}
        </Button>
      </Modal.Actions>
    </Modal>
  </div>
);

StandardModalYesNo.propTypes = {
  open: PropTypes.bool.isRequired,
  btnDisabled: PropTypes.bool,
  onClose: PropTypes.func,
  onContinue: PropTypes.func,
  noActionButton: PropTypes.func,
  headerText: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired,
  onActionText: PropTypes.string.isRequired,
  onActionAddNewText: PropTypes.string.isRequired,
};

export default StandardModalYesNo;
