import React from 'react';
import { Dimmer, Loader } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

export const Loading = ({ loading }) => {
  const component = (
    <Dimmer active inverted>
      <Loader
        content={<FormattedMessage {...messages.loading} />}
        size="large"
      />
    </Dimmer>
  );
  return loading ? component : null;;
};

Loading.propTypes = {
  loading: PropTypes.bool.isRequired,
};

export default Loading;
