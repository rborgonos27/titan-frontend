/**
 *
 * FormInput
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Form, Label, Input } from 'semantic-ui-react';
import RequiredText from './RequiredText';

function FormInput({
  name,
  value,
  placeholder,
  required,
  caption,
  handleInput,
  type,
  readOnly,
}) {
  return (
    <Form.Field>
      <Label pointing="below">{caption}</Label>
      {required ? <RequiredText>*</RequiredText> : null}
      <Input
        name={name}
        value={value}
        onChange={handleInput}
        placeholder={placeholder}
        type={type || 'text'}
        selection
        fluid
        readOnly={readOnly}
      />
    </Form.Field>
  );
}

FormInput.propTypes = {
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  caption: PropTypes.string.isRequired,
  type: PropTypes.string,
  handleInput: PropTypes.bool.isRequired,
  required: PropTypes.bool.isRequired,
  value: PropTypes.any.isRequired,
  readOnly: PropTypes.bool,
};

export default FormInput;
