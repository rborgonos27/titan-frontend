import React from 'react';
import { Segment, Loader } from 'semantic-ui-react';

const SegmentLoader = () => (
  <Segment>
    <Loader size="huge" active />
  </Segment>
);

export default SegmentLoader;
