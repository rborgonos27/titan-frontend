import React from 'react';
import PropTypes from 'prop-types';

export const RequiredText = ({ children }) => (
  <span style={style}>{children}</span>
);

const style = {
  color: '#C60000',
  marginLeft: 5,
  marginRight: 5,
  textAlign: 'justify',
};

RequiredText.propTypes = {
  children: PropTypes.any.isRequired,
};

export default RequiredText;
