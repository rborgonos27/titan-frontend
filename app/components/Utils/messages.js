/*
 * Utils Messages
 *
 * This contains all the text for the Utils component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Utils.header',
    defaultMessage: 'This is the Utils component !',
  },
  loading: {
    id: 'app.components.Utils.loading',
    defaultMessage: 'Loading',
  },
});
