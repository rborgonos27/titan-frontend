import React from 'react';
import PropTypes from 'prop-types';

export const DetailData = ({ children }) => <div style={style}>{children}</div>;

const style = {
  marginLeft: 10,
  marginTop: 5,
  marginBottom: 20,
  fontSize: 20,
  color: '#464646',
};

DetailData.propTypes = {
  children: PropTypes.any,
};

export default DetailData;
