import React from 'react';
import PropTypes from 'prop-types';

export const Title = ({ children }) => (
  <h4 className="m-t-0 header-title">
    <b>{children}</b>
  </h4>
);

Title.propTypes = {
  children: PropTypes.any,
};

export default Title;
