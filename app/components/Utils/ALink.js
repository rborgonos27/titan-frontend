import React from 'react';
import PropTypes from 'prop-types';

export const ALink = ({ children, onClick, linkColor }) => {
  /* eslint-disable */
  const color = linkColor ? linkColor : '#1F1F95'
  const style = {
    marginTop: 5,
    cursor: 'pointer',
    color,
  };
 return (
   <a onClick={onClick} style={style}>{children}</a>
 );
  /* eslint-enable */
};
ALink.propTypes = {
  children: PropTypes.any.isRequired,
  onClick: PropTypes.func.isRequired,
  linkColor: PropTypes.any,
};

export default ALink;
