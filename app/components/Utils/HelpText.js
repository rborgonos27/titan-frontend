import React from 'react';
import PropTypes from 'prop-types';

export const HelpText = ({ children }) => <div style={style}>{children}</div>;

const style = {
  marginLeft: 5,
  marginTop: 5,
  marginBottom: 5,
  marginRight: 5,
  textAlign: 'justify',
};

HelpText.propTypes = {
  children: PropTypes.any.isRequired,
};

export default HelpText;
