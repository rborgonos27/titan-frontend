/**
 *
 * LoadScripts
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

/* eslint-disable react/prefer-stateless-function */
class LoadScripts extends React.Component {
  componentWillMount() {
    // const loadScript = src => {
    //   const script = document.createElement('script');
    //   script.async = false;
    //   script.src = src;
    //   document.body.appendChild(script);
    // };
    /*
    loadScript('themes/assets/js/modernizr.min.js');
    loadScript('themes/assets/js/jquery.min.js');
    loadScript('themes/assets/js/bootstrap.min.js');
    loadScript('themes/assets/js/detect.js');
    loadScript('themes/assets/js/fastclick.js');
    loadScript('themes/assets/js/jquery.slimscroll.js');
    loadScript('themes/assets/js/jquery.blockUI.js');
    loadScript('themes/assets/plugins/jquery-knob/jquery.knob.js');
    */
  }
  componentDidMount() {
    const dropdown = document.querySelector('.dropdown-toggle.profile');
    const showDropdown = document.querySelector('#dropdownProfile');
    dropdown.onclick = function() {
      if (showDropdown.className === 'dropdown top-menu-item-xs') {
        showDropdown.className = 'dropdown top-menu-item-xs open';
      } else {
        showDropdown.className = 'dropdown top-menu-item-xs';
      }
    };
    const dropDownMenu = document.querySelector('#profileLink');
    dropDownMenu.onclick = function() {
      showDropdown.className = 'dropdown top-menu-item-xs';
    };
    const dropDownMenuActive = document.querySelector('#settingsLink');
    dropDownMenuActive.onclick = function() {
      showDropdown.className = 'dropdown top-menu-item-xs';
    };
    const sidebarToggle = document.querySelector('.open-left');
    const wrapper = document.querySelector('#wrapper'); // eslint-disable-line no-alert
    sidebarToggle.onclick = function() {
      if (wrapper.className === 'enlarged forced') {
        wrapper.className = '';
      } else {
        wrapper.className = 'enlarged forced';
      }
    };
  }
  render() {
    return <div />;
  }
}

LoadScripts.propTypes = {};

export default LoadScripts;
