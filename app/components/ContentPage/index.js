/**
 *
 * ContentPage
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';
import Chatbar from '../../components/Chatbar';

/* eslint-disable react/prefer-stateless-function */
class ContentPage extends React.Component {
  render() {
    return (
      <div className="content-page">
        <div className="content">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="card-box">
                  <h4 className="m-t-0 m-b-20 header-title">
                    <b>Last Transactions</b>
                  </h4>
                  <ul className="list-unstyled transaction-list">
                    <li>
                      <i className="ti-download text-success" />
                      <span className="tran-text">Advertising</span>
                      <span className="pull-right text-success tran-price">
                        +$230
                      </span>
                      <span className="pull-right text-muted">07/09/2015</span>
                      <span className="clearfix" />
                    </li>
                    <li>
                      <i className="ti-upload text-danger" />
                      <span className="tran-text">Support licence</span>
                      <span className="pull-right text-danger tran-price">
                        -$965
                      </span>
                      <span className="pull-right text-muted">07/09/2015</span>
                      <span className="clearfix" />
                    </li>
                    <li>
                      <i className="ti-download text-success" />
                      <span className="tran-text">Extended licence</span>
                      <span className="pull-right text-success tran-price">
                        +$830
                      </span>
                      <span className="pull-right text-muted">07/09/2015</span>
                      <span className="clearfix" />
                    </li>
                    <li>
                      <i className="ti-download text-success" />
                      <span className="tran-text">Advertising</span>
                      <span className="pull-right text-success tran-price">
                        +$230
                      </span>
                      <span className="pull-right text-muted">05/09/2015</span>
                      <span className="clearfix" />
                    </li>
                    <li>
                      <i className="ti-upload text-danger" />
                      <span className="tran-text">New plugins added</span>
                      <span className="pull-right text-danger tran-price">
                        -$452
                      </span>
                      <span className="pull-right text-muted">05/09/2015</span>
                      <span className="clearfix" />
                    </li>
                    <li>
                      <i className="ti-download text-success" />
                      <span className="tran-text">Google Inc.</span>
                      <span className="pull-right text-success tran-price">
                        +$230
                      </span>
                      <span className="pull-right text-muted">04/09/2015</span>
                      <span className="clearfix" />
                    </li>
                    <li>
                      <i className="ti-upload text-danger" />
                      <span className="tran-text">Facebook Ad</span>
                      <span className="pull-right text-danger tran-price">
                        -$364
                      </span>
                      <span className="pull-right text-muted">03/09/2015</span>
                      <span className="clearfix" />
                    </li>
                    <li>
                      <i className="ti-download text-success" />
                      <span className="tran-text">New sale</span>
                      <span className="pull-right text-success tran-price">
                        +$230
                      </span>
                      <span className="pull-right text-muted">03/09/2015</span>
                      <span className="clearfix" />
                    </li>
                    <li>
                      <i className="ti-download text-success" />
                      <span className="tran-text">Advertising</span>
                      <span className="pull-right text-success tran-price">
                        +$230
                      </span>
                      <span className="pull-right text-muted">29/08/2015</span>
                      <span className="clearfix" />
                    </li>
                    <li>
                      <i className="ti-upload text-danger" />
                      <span className="tran-text">Support licence</span>
                      <span className="pull-right text-danger tran-price">
                        -$854
                      </span>
                      <span className="pull-right text-muted">27/08/2015</span>
                      <span className="clearfix" />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <footer className="footer text-right">
            <ul className="pull-left footer-nav list-unstyled">
              <li>
                <a href="/" className="waves-effect waves-light bordered-link">
                  <i className="fa fa-facebook" />
                </a>
              </li>
              <li>
                <a href="/" className="waves-effect waves-light bordered-link">
                  <i className="fa fa-twitter" />
                </a>
              </li>
              <li>
                <a href="/" className="waves-effect waves-light bordered-link">
                  <i className="fa fa-google-plus" />
                </a>
              </li>
              <li>
                <a href="/" className="waves-effect waves-light bordered-link">
                  <i className="fa fa-rss" />
                </a>
              </li>
              <li>
                <a href="/" className="waves-effect waves-light bordered-link">
                  <i className="fa fa-envelope" />
                </a>
              </li>
            </ul>
            <ul className="pull-right footer-nav list-unstyled">
              <li>
                <a href="/" className="waves-effect waves-light">
                  Privacy
                </a>
              </li>
              <li>
                <a href="/" className="waves-effect waves-light">
                  Terms of services
                </a>
              </li>
              <li>
                <a href="/" className="waves-effect waves-light">
                  Feedback
                </a>
              </li>
              <li>
                <a href="/" className="waves-effect waves-light">
                  Legal
                </a>
              </li>
            </ul>
          </footer>
        </div>
        <Chatbar />
      </div>
    );
  }
}

ContentPage.propTypes = {};

export default ContentPage;
