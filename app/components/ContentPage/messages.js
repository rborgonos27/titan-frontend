/*
 * ContentPage Messages
 *
 * This contains all the text for the ContentPage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.ContentPage.header',
    defaultMessage: 'This is the ContentPage component !',
  },
});
