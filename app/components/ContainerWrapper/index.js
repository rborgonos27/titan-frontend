/**
 *
 * ContainerWrapper
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Footer from 'components/Footer';

function ContainerWrapper({ children }) {
  return (
    <div className="content-page">
      <div className="content">
        <div className="container">{children}</div>
      </div>
      <Footer />
    </div>
  );
}

ContainerWrapper.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ContainerWrapper;
