/*
 * ContainerWrapper Messages
 *
 * This contains all the text for the ContainerWrapper component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.ContainerWrapper.header',
    defaultMessage: 'This is the ContainerWrapper component !',
  },
});
