/**
 *
 * Sidebar
 *
 */

import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import paymentIconInactive from '../../themes/assets/images/payment-icon-active.png';
import dashboardIconInactive from '../../themes/assets/images/dashboard-icon-active.png';
import userFolowIconInactive from '../../themes/assets/images/user-follow-icon-inactive.png';
import briefCaseIconInactive from '../../themes/assets/images/icon-briefcase-inactive.png';
import userInactiveIcon from '../../themes/assets/images/user-inactive-icon.png';
import accountImage from '../../themes/assets/images/book.png';

/* eslint-disable react/prefer-stateless-function */
class Sidebar extends React.Component {
  render() {
    const { user } = this.props;
    const iconStyle = {
      width: 20,
      height: 20,
      marginLeft: 5,
      marginRight: 15,
    };
    return (
      <div className="left side-menu" style={{ position: 'fixed' }}>
        <div className="sidebar-inner slimscrollleft">
          <div id="sidebar-menu">
            <ul>
              <li className="text-muted menu-title">Navigation</li>
              <li className="has_sub">
                <NavLink
                  exact
                  className="waves-effect"
                  activeClassName="active"
                  to="/"
                >
                  <img alt="" src={dashboardIconInactive} style={iconStyle} />
                  <span> Dashboard </span>
                </NavLink>
              </li>
              {/*
              <li className="has_sub">
                <NavLink
                  exact
                  className="waves-effect"
                  activeClassName="active"
                  to="/deposit"
                >
                  <i className="icon-wallet" />
                  <span> Make Deposit </span>
                </NavLink>
              </li>
              */}
              {user.user_type === 'member' ||
              user.user_type === 'sub_member' ? (
                <li className="has_sub">
                  <NavLink
                    className="waves-effect"
                    activeClassName="active"
                    to="/payment"
                  >
                    <img alt="" src={paymentIconInactive} style={iconStyle} />
                    <span> Send Payment </span>
                  </NavLink>
                </li>
              ) : null}
              {user.user_type === 'member' ||
              user.user_type === 'sub_member' ? (
                <li className="has_sub">
                  <NavLink
                    exact
                    className="waves-effect"
                    activeClassName="active"
                    to="/deposit"
                  >
                    <i className="icon-wallet" />
                    <span> Make Deposit </span>
                  </NavLink>
                </li>
              ) : null}
              {user.user_type === 'member' ||
              user.user_type === 'sub_member' ? (
                <li className="has_sub">
                  <NavLink
                    exact
                    className="waves-effect"
                    activeClassName="active"
                    to="/statement"
                  >
                    <img alt="" src={accountImage} style={iconStyle} />
                    <span> Accounts </span>
                  </NavLink>
                </li>
              ) : null}
              <li className="has_sub">
                  <NavLink
                    className="waves-effect"
                    activeClassName="active"
                    to="/payee"
                  >
                    <img alt="" src={userFolowIconInactive} style={iconStyle} />
                    <span> Payee </span>
                  </NavLink>
              </li>
              {/*
              <li className="has_sub">
                <NavLink
                  className="waves-effect"
                  activeClassName="active"
                  to="/statement"
                >
                  <i className="icon-book-open" />
                  <span> My Statements </span>
                </NavLink>
              </li>
               */}
              {user.user_type === 'super_admin' ? (
                <li className="has_sub">
                  <NavLink
                    className="waves-effect"
                    activeClassName="active"
                    to="/pickup"
                  >
                    <img alt="" src={briefCaseIconInactive} style={iconStyle} />
                    <span> Cash Pickup </span>
                  </NavLink>
                </li>
              ) : null}
              {user.user_type === 'super_admin' ? (
                <li className="has_sub">
                  <NavLink
                    className="waves-effect"
                    activeClassName="active"
                    to="/user"
                  >
                    <img alt="" src={userInactiveIcon} style={iconStyle} />
                    <span> Users </span>
                  </NavLink>
                </li>
              ) : null}
              {user.user_type === 'member' ? (
                <li className="has_sub">
                  <NavLink
                    className="waves-effect"
                    activeClassName="active"
                    to="/member_user"
                  >
                    <img alt="" src={userInactiveIcon} style={iconStyle} />
                    <span> Member Users </span>
                  </NavLink>
                </li>
              ) : null}
            </ul>
            <div className="clearfix" />
          </div>
          <div className="clearfix" />
        </div>
      </div>
    );
  }
}

Sidebar.propTypes = {
  user: PropTypes.any.isRequired,
};

export default Sidebar;
