/**
 *
 * DataTable
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { Table } from 'semantic-ui-react';
import moment from 'moment';

const DataTable = ({ data }) => {
  /* eslint-disable */
  const { fields, tableData } = data
  let tableHeaderColumn = [];
  for (let i = 0; i < fields.length; i++) {
    const value = fields[i]
    tableHeaderColumn.push(
      <Table.HeaderCell key={i}>{value.display}</Table.HeaderCell>
    );
  }
  const { results } = tableData;
  let tableBodyData = [];
  let ctr = 0;
  results.forEach(value => {
    let tableColumnData = [];
    for (let i = 0; i < fields.length; i++) {
      const fieldValue = fields[i]
      const fieldDerivedValue = fieldValue.type === 'date' ?
        moment(value[fieldValue.name]).subtract(8, "hours").format('MMM DD, YYYY HH:MM:s') :
        value[fieldValue.name];

      tableColumnData.push(
        <Table.Cell key={i}>
          {fieldDerivedValue}
        </Table.Cell>
      );
    }

    tableBodyData.push(
      <Table.Row key={ctr}>
        {tableColumnData}
      </Table.Row>
    );
    ctr = ctr+1;
  });

  return (
    <div>
      {tableBodyData.length > 0 ? (
      <Table striped celled selectable>
        <Table.Header>
          <Table.Row>{tableHeaderColumn}</Table.Row>
        </Table.Header>
        <Table.Body>{tableBodyData}</Table.Body>
      </Table>
        ) : null }
    </div>
  );
}

DataTable.propTypes = {
  data: PropTypes.any.isRequired,
};

export default DataTable;
