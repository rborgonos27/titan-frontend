/**
 *
 * Footer
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

function Footer() {
  return (
    <footer className="footer text-right">
      {/* <ul className="pull-left footer-nav list-unstyled">
        <li>
          <a href="/" className="waves-effect waves-light bordered-link">
            <i className="fa fa-facebook" />
          </a>
        </li>
        <li>
          <a href="/" className="waves-effect waves-light bordered-link">
            <i className="fa fa-twitter" />
          </a>
        </li>
        <li>
          <a href="/" className="waves-effect waves-light bordered-link">
            <i className="fa fa-google-plus" />
          </a>
        </li>
        <li>
          <a href="/" className="waves-effect waves-light bordered-link">
            <i className="fa fa-rss" />
          </a>
        </li>
        <li>
          <a href="/" className="waves-effect waves-light bordered-link">
            <i className="fa fa-envelope" />
          </a>
        </li>
      </ul> */}
      <ul className="pull-right footer-nav list-unstyled">
        <li>
          <NavLink className="waves-effect waves-light" to="/privacy">
            <FormattedMessage {...messages.privacy} />
          </NavLink>
        </li>
        <li>
          <NavLink className="waves-effect waves-light" to="/services">
            <FormattedMessage {...messages.terms} />
          </NavLink>
        </li>
        <li>
          <NavLink className="waves-effect waves-light" to="/services">
            <FormattedMessage {...messages.legal} />
          </NavLink>
        </li>
      </ul>
    </footer>
  );
}

Footer.propTypes = {};

export default Footer;
