/*
 * Footer Messages
 *
 * This contains all the text for the Footer component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  privacy: {
    id: 'app.components.Footer.privacy',
    defaultMessage: 'Privacy',
  },
  terms: {
    id: 'app.components.Footer.terms',
    defaultMessage: 'Terms of services',
  },
  feedback: {
    id: 'app.components.Footer.feedback',
    defaultMessage: 'Feedback',
  },
  legal: {
    id: 'app.components.Footer.legal',
    defaultMessage: 'Legal',
  },
});
