/**
 *
 * PageTitle
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

function PageTitle({ children }) {
  return <h4 className="page-title m-l-10 m-b-20">{children}</h4>;
}

PageTitle.propTypes = {
  children: PropTypes.node.isRequired,
};

export default PageTitle;
