/**
 *
 * ChartPolar
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { Pie } from 'react-chartjs-2';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
class ChartPolar extends React.Component {
  render() {
    const { actualAmount, pendingPayment, pendingDeposit } = this.props;
    const available = actualAmount;
    let data = {};
    let enabledTooltip = true;
    if (available === 0 && pendingDeposit === 0 && pendingPayment === 0) {
      data = {
        datasets: [
          {
            data: [10],
            backgroundColor: ['#EEE'],
            hoverBackgroundColor: ['#EEE'],
          },
        ],
      };
      enabledTooltip = false;
    } else {
      data = {
        labels: ['Available Now', 'Pending Deposit', 'Pending Payment'],
        datasets: [
          {
            data: [available, pendingDeposit, pendingPayment],
            backgroundColor: ['#32CD32', '#faad41', '#f25218'],
            hoverBackgroundColor: ['#32CD32', '#faad41', '#f25218'],
          },
        ],
      };
    }
    // legend={{
    //   display: true,
    //   position: 'bottom',
    //   fullWidth: true,
    // }}
    /* eslint-disable */
    return (
      <Pie
        data={data}
        legend={{ display: false }}
        width={400}
        height={400}
        options={
          {
            tooltips: {
              enabled: {enabledTooltip},
            }
        }}
      />
    );
    /* eslint-enable */
  }
}

ChartPolar.propTypes = {
  actualAmount: PropTypes.number,
  pendingPayment: PropTypes.number,
  pendingDeposit: PropTypes.number,
};

export default ChartPolar;
