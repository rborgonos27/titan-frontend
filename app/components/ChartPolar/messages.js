/*
 * ChartPolar Messages
 *
 * This contains all the text for the ChartPolar component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.ChartPolar.header',
    defaultMessage: 'This is the ChartPolar component !',
  },
});
