/*
 * SessionModal Messages
 *
 * This contains all the text for the SessionModal component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SessionModal.header',
    defaultMessage: 'This is the SessionModal component !',
  },
  button: {
    id: 'app.components.SessionModal.button',
    defaultMessage: 'STAY ON',
  },
  modalHeader: {
    id: 'app.components.SessionModal.modalHeader',
    defaultMessage: 'SESSION TIMED OUT',
  },
  modalMessage: {
    id: 'app.components.SessionModal.modalMessage',
    defaultMessage:
      "You've been quiet for more than 10 minutes, please let us know if you want to stay signed in. You will lose any information you haven't saved.",
  },
  modalMessageBottom: {
    id: 'app.components.SessionModal.modalMessageBottom',
    defaultMessage: 'Session Remaining 0:00',
  },
});
