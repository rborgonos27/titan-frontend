/**
 *
 * SessionModal
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal } from 'semantic-ui-react';
import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

const ModalHeader = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: #fc5109;
`;

const CloseButton = styled.section`
  text-align: right;
`;
/*
  const BottomSection = styled.section`
  padding-top: 2.5em;
  padding-bottom: 2em;
  font-size: 1.1em;
  text-align: center;
  color: #fc5109;
`;
*/

const A = styled.a`
  font-size: 2em;
  cursor: pointer;
  color: #cccccc;
`;

const BodySection = styled.section`
  font-size: 1.3em;
  text-align: center;
  margin-left: 2em;
  margin-right: 2em;
`;

const SessionModal = ({ open, onClose, onContinue }) => (
  <div>
    <Modal open={open}>
      <Modal.Header className="sessionModalHeader">
        <ModalHeader>
          <FormattedMessage {...messages.modalHeader} />
        </ModalHeader>
        <CloseButton>
          <A onClick={onClose}>X</A>
        </CloseButton>
      </Modal.Header>
      <Modal.Content>
        <div>
          <BodySection>
            <FormattedMessage {...messages.modalMessage} />
          </BodySection>
        </div>
      </Modal.Content>
      <Modal.Actions className="sessionModalAction">
        <Button onClick={onContinue}>
          <FormattedMessage {...messages.button} />
        </Button>
      </Modal.Actions>
    </Modal>
  </div>
);

SessionModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  onContinue: PropTypes.func,
};

export default SessionModal;
