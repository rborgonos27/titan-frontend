/**
 *
 * Topbar
 *
 */

import React from 'react';
import Gravatar from 'react-gravatar';
import PropTypes from 'prop-types';
import {
  Menu,
  Popup,
  Button,
  TextArea,
  Form,
  Message,
} from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

import SmallLogo from 'themes/assets/images/logo-small.png';
import StandardModal from 'components/StandardModal';

const timeoutLength = 2500;

/* eslint-disable react/prefer-stateless-function */
class Topbar extends React.Component {
  state = {
    isOpen: false,
    isMessageOpen: false,
    message: '',
  };
  handleOpen = () => {
    this.setState({ isOpen: true });

    this.timeout = setTimeout(() => {
      this.setState({ isOpen: false });
    }, timeoutLength);
  };
  handleClose = () => {
    this.setState({ isOpen: false });
    clearTimeout(this.timeout);
  };
  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  buildMessageModal = () => {
    const { isMessageOpen, message } = this.state;
    const { user, sendMessage, loading, resetMessage } = this.props;
    const messageObject = this.props.message;
    let messageNotif = null;
    if (messageObject) {
      messageNotif = <Message positive>{messageObject}</Message>;
      setTimeout(() => {
        this.setState({ isMessageOpen: false });
        resetMessage();
      }, 2000);
    }
    const messageData = { message, user };
    return (
      <StandardModal
        btnDisabled={false}
        headerText="Message Us"
        onActionText="Send"
        onClose={() => this.setState({ isMessageOpen: false })}
        open={isMessageOpen}
        onContinue={() => sendMessage(messageData)}
      >
        {messageNotif}
        <Form loading={loading}>
          <TextArea
            onChange={this.handleInput}
            name="message"
            placeholder="Tell us more"
          />
        </Form>
      </StandardModal>
    );
  };
  render() {
    const { logout, user } = this.props;
    /* eslint-disable */
    return (
      <div>
        {this.buildMessageModal()}
        <div className="topbar">
          <div className="topbar-left">
            <div className="text-center">
              <a href="/" className="logo">
                <img alt="" src={SmallLogo} />
              </a>
            </div>
          </div>
          <div className="navbar navbar-default" role="navigation">
            <div className="container">
              <div>
                <div className="pull-left">
                  <button className="button-menu-mobile open-left">
                    <i className="md md-menu" />
                  </button>
                  <span className="clearfix" />
                </div>
                <div className="pull-left">
                  <Popup
                    className="menuTopBar"
                    trigger={
                      <button className="button-menu-mobile mobile-view">
                        <i className="md md-filter-list" />
                      </button>
                    }
                    on="click"
                    open={this.state.isOpen}
                    onClose={this.handleClose}
                    onOpen={this.handleOpen}
                    positon="bottom right"
                    hideOnScroll
                  >
                    <Menu pointing vertical>
                      <Menu.Item>
                        <NavLink className="menuLink" to="/">
                          Home
                        </NavLink>
                      </Menu.Item>
                      <Menu.Item>
                        <NavLink className="menuLink" to="/account">
                          Account Profile
                        </NavLink>
                      </Menu.Item>
                      <Menu.Item>
                        <a className="menuLink" href="/services">
                          Member Services
                        </a>
                      </Menu.Item>
                    </Menu>
                  </Popup>

                  <span className="clearfix" />
                </div>
                <ul className="nav navbar-nav hidden-xs">
                  <li>
                    <NavLink to="/" className="waves-effect waves-light">
                      Home
                    </NavLink>
                  </li>
                  <li>
                    <NavLink to="/account" className="waves-effect waves-light">
                      Account Profile
                    </NavLink>
                  </li>
                  <li>
                    <a className="waves-effect waves-light" href="/services">
                      Member Services
                    </a>
                  </li>
                </ul>
                <ul className="nav navbar-nav navbar-right pull-right">
                  {user.user_type === 'member' ?
                    (<li className="dropdown top-menu-item-xs">
                      <div style={{ marginTop: 10, padding: 0 }}>
                        <Button onClick={() => this.setState({ isMessageOpen: true })} className="topnav-btn" basic circular color='orange' icon='envelope outline' />
                      </div>
                    </li>) : null}

                  <li
                    id="dropdownProfile"
                    className="dropdown top-menu-item-xs"
                  >
                    <a className="dropdown-toggle profile waves-effect waves-light">
                      {
                        user.profile_picture ? (
                          <img
                            src={user.profile_picture}
                            className="profilePictureTopBar"
                            alt=""
                          />
                        ) : (
                          <Gravatar
                            email={user.email}
                            size={100}
                            rating="pg"
                            default="identicon"
                            className="img-circle"
                          />
                        )
                      }
                      <span className="accountname">
                        Hello {user.first_name}!
                        <i className="accountarrow ion-arrow-down-b" />
                      </span>
                    </a>
                    <ul className="dropdown-menu">
                      <li>
                        <NavLink id="profileLink"  className="dropdownLink" to="/profile">
                          <i className="ti-user m-r-10 text-custom" />Profile
                        </NavLink>
                      </li>
                      <li>
                        <NavLink id="settingsLink" className="dropdownLink" to="/settings">
                          <i className="ti-settings m-r-10 text-custom" />
                          Settings
                        </NavLink>
                      </li>
                      <li className="divider" />
                      <li>
                        <button onClick={logout}>
                          <i className="ti-power-off m-r-10 text-danger" />
                          Logout
                        </button>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Topbar.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  sendMessage: PropTypes.func,
  loading: PropTypes.bool,
  message: PropTypes.any,
  resetMessage: PropTypes.any,
};

export default Topbar;
