/**
 *
 * SimpleModal
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal } from 'semantic-ui-react';

const SimpleModal = ({
  open,
  actionName,
  children,
  cancelAction,
  submitAction,
}) => (
  <div>
    <Modal size="tiny" open={open}>
      <Modal.Header>{actionName}</Modal.Header>
      <Modal.Content>{children}</Modal.Content>
      <Modal.Actions>
        <Button
          onClick={cancelAction}
          negative
          icon="warning"
          labelPosition="right"
          content="Cancel"
        />
        {submitAction ? (
          <Button
            onClick={submitAction}
            color="orange"
            icon="checkmark"
            labelPosition="right"
            content="Submit"
          />
        ) : null}
      </Modal.Actions>
    </Modal>
  </div>
);

SimpleModal.propTypes = {
  open: PropTypes.bool.isRequired,
  actionName: PropTypes.string.isRequired,
  cancelAction: PropTypes.func,
  submitAction: PropTypes.func,
  children: PropTypes.any.isRequired,
};

export default SimpleModal;
