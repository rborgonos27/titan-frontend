/**
 *
 * CardBox
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

function CardBox({ children }) {
  return (
    <div className="card-box" style={{ overflow: 'hidden' }}>
      {children}
    </div>
  );
}

CardBox.propTypes = {
  children: PropTypes.node.isRequired,
};

export default CardBox;
