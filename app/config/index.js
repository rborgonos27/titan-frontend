import APP from './app';
import GA from './googleanalytics';

const config = {};
config.app = APP;
config.ga = GA;

export default config;
export { APP, GA };
