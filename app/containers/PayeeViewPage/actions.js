/*
 *
 * PayeeViewPage actions
 *
 */

import {
  GET_PAYEE,
  GET_PAYEE_SUCCESS,
  GET_PAYEE_FAILURE,
  GET_USER_PAYEE,
  GET_USER_PAYEE_SUCCESS,
  GET_USER_PAYEE_ERROR,
  SET_PAYEE_ID,
  GET_PAYEE_AGREEMENT,
  GET_PAYEE_AGREEMENT_SUCCESS,
  GET_PAYEE_AGREEMENT_ERROR,
  ENABLE_PAYEE,
  ENABLE_PAYEE_SUCCESS,
  ENABLE_PAYEE_ERROR,
  RESEND_PAYEE_AGREEMENT,
  RESEND_PAYEE_AGREEMENT_ERROR,
  RESEND_PAYEE_AGREEMENT_SUCCESS,
  ACTIVATE_NOTIF,
  APPROVE_PAYEE,
  APPROVE_PAYEE_SUCCESS,
  APPROVE_PAYEE_ERROR,
  CHECK_PAYEE,
  CHECK_PAYEE_SUCCESS,
  CHECK_PAYEE_ERROR,
} from './constants';

export function getPayee(data) {
  return {
    type: GET_PAYEE,
    data,
  };
}

export function getPayeeSuccess(data) {
  return {
    type: GET_PAYEE_SUCCESS,
    data,
  };
}

export function getPayeeFailure(data) {
  return {
    type: GET_PAYEE_FAILURE,
    data,
  };
}

export function getUserPayee(data) {
  return {
    type: GET_USER_PAYEE,
    data,
  };
}

export function getUserPayeeSuccess(data) {
  return {
    type: GET_USER_PAYEE_SUCCESS,
    data,
  };
}

export function getUserPayeeError(error) {
  return {
    type: GET_USER_PAYEE_ERROR,
    error,
  };
}

export function setPayeeId(data) {
  return {
    type: SET_PAYEE_ID,
    data,
  };
}

export function getPayeeAgreement(data) {
  return {
    type: GET_PAYEE_AGREEMENT,
    data,
  };
}

export function getPayeeAgreementError(error) {
  return {
    type: GET_PAYEE_AGREEMENT_ERROR,
    error,
  };
}

export function getPayeeAgreementSuccess(data) {
  return {
    type: GET_PAYEE_AGREEMENT_SUCCESS,
    data,
  };
}

export function enablePayee(data) {
  return {
    type: ENABLE_PAYEE,
    data,
  };
}

export function enablePayeeSuccess(data) {
  return {
    type: ENABLE_PAYEE_SUCCESS,
    data,
  };
}

export function enablePayeeError(error) {
  return {
    type: ENABLE_PAYEE_ERROR,
    error,
  };
}

export function resendPayeeAgreement(data) {
  return {
    type: RESEND_PAYEE_AGREEMENT,
    data,
  };
}

export function resendPayeeAgreementSuccess(data) {
  return {
    type: RESEND_PAYEE_AGREEMENT_SUCCESS,
    data,
  };
}

export function resendPayeeAgreementError(error) {
  return {
    type: RESEND_PAYEE_AGREEMENT_ERROR,
    error,
  };
}

export function activateNotif() {
  return {
    type: ACTIVATE_NOTIF,
  };
}

export function approvePayee(data) {
  return {
    type: APPROVE_PAYEE,
    data,
  };
}

export function approvePayeeSuccess(data) {
  return {
    type: APPROVE_PAYEE_SUCCESS,
    data,
  };
}

export function approvePayeeError(error) {
  return {
    type: APPROVE_PAYEE_ERROR,
    error,
  };
}

export function checkPayee(data) {
  return {
    type: CHECK_PAYEE,
    data,
  };
}

export function checkPayeeSuccess(data) {
  return {
    type: CHECK_PAYEE_SUCCESS,
    data,
  };
}

export function checkPayeeError(error) {
  return {
    type: CHECK_PAYEE_ERROR,
    error,
  };
}
