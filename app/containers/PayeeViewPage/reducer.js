/*
 *
 * PayeeViewPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  GET_PAYEE,
  GET_PAYEE_SUCCESS,
  GET_PAYEE_FAILURE,
  GET_USER_PAYEE,
  GET_USER_PAYEE_SUCCESS,
  GET_USER_PAYEE_ERROR,
  GET_PAYEE_AGREEMENT,
  GET_PAYEE_AGREEMENT_SUCCESS,
  GET_PAYEE_AGREEMENT_ERROR,
  ENABLE_PAYEE,
  ENABLE_PAYEE_SUCCESS,
  ENABLE_PAYEE_ERROR,
  RESEND_PAYEE_AGREEMENT,
  RESEND_PAYEE_AGREEMENT_ERROR,
  RESEND_PAYEE_AGREEMENT_SUCCESS,
  ACTIVATE_NOTIF,
  APPROVE_PAYEE,
  APPROVE_PAYEE_SUCCESS,
  APPROVE_PAYEE_ERROR,
  CHECK_PAYEE,
  CHECK_PAYEE_SUCCESS,
  CHECK_PAYEE_ERROR,
} from './constants';

export const initialState = fromJS({
  loading: true,
  error: false,
  payee: {},
  loadingUser: false,
  userData: [],
  payeeAgreement: [],
  notif: null,
  isNotifTimeout: false,
  payeeDuplicate: null,
});

function payeeViewPageReducer(state = initialState, action) {
  switch (action.type) {
    case GET_PAYEE:
      return state
        .set('loading', true)
        .set('payees', {})
        .set('payeeDuplicate', null)
        .set('error', false);
    case GET_PAYEE_SUCCESS: {
      return state
        .set('loading', false)
        .set('payeeDuplicate', null)
        .set('payee', action.data)
        .set('error', false);
    }
    case GET_PAYEE_FAILURE:
      return state
        .set('loading', false)
        .set('payee', {})
        .set('payeeDuplicate', null)
        .set('error', action.data.error);
    case GET_USER_PAYEE:
      return state.set('loadingUser', true).set('payeeDuplicate', null);
    case GET_USER_PAYEE_SUCCESS:
      return state
        .set('loadingUser', false)
        .set('payeeDuplicate', null)
        .set('userData', action.data);
    case GET_USER_PAYEE_ERROR:
      return state
        .set('loadingUser', false)
        .set('payeeDuplicate', null)
        .set('error', action.error);
    case GET_PAYEE_AGREEMENT:
      return state
        .set('loading', true)
        .set('payeeDuplicate', null)
        .set('payeeAgreement', null)
        .set('error', null);
    case GET_PAYEE_AGREEMENT_SUCCESS:
      return state
        .set('loading', false)
        .set('payeeDuplicate', null)
        .set('payeeAgreement', action.data)
        .set('error', null);
    case GET_PAYEE_AGREEMENT_ERROR:
      return state
        .set('loading', false)
        .set('payeeDuplicate', null)
        .set('payeeAgreement', null)
        .set('error', action.error);
    case ENABLE_PAYEE:
      return state
        .set('loading', true)
        .set('payeeDuplicate', null)
        .set('error', null);
    case ENABLE_PAYEE_SUCCESS:
      return state
        .set('loading', false)
        .set('payeeDuplicate', null)
        .set('error', null);
    case ENABLE_PAYEE_ERROR:
      return state
        .set('loading', false)
        .set('payeeDuplicate', null)
        .set('error', action.error);
    case RESEND_PAYEE_AGREEMENT:
      return state
        .set('loading', true)
        .set('error', action.error)
        .set('payeeDuplicate', null)
        .set('isNotifTimeout', false)
        .set('notif', null);
    case RESEND_PAYEE_AGREEMENT_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('payeeDuplicate', null)
        .set('isNotifTimeout', true)
        .set('notif', action.data);
    case RESEND_PAYEE_AGREEMENT_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('isNotifTimeout', false)
        .set('payeeDuplicate', null)
        .set('notif', null);
    case ACTIVATE_NOTIF:
      return state.set('isNotifTimeout', false).set('payeeDuplicate', null);
    case APPROVE_PAYEE:
      return state
        .set('loading', true)
        .set('payeeDuplicate', null)
        .set('error', null);
    case APPROVE_PAYEE_SUCCESS:
      return state
        .set('loading', false)
        .set('payeeDuplicate', null)
        .set('error', null);
    case APPROVE_PAYEE_ERROR:
      return state
        .set('loading', false)
        .set('payeeDuplicate', null)
        .set('error', action.error);
    case CHECK_PAYEE:
      return state
        .set('loading', true)
        .set('error', null)
        .set('payeeDuplicate', null);
    case CHECK_PAYEE_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('payeeDuplicate', action.data);
    case CHECK_PAYEE_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('payeeDuplicate', null);
    default:
      return state;
  }
}

export default payeeViewPageReducer;
