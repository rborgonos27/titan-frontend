import { fromJS } from 'immutable';
import payeeViewPageReducer from '../reducer';

describe('payeeViewPageReducer', () => {
  it('returns the initial state', () => {
    expect(payeeViewPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
