/*
 *
 * PayeeViewPage constants
 *
 */

export const GET_PAYEE = 'app/PayeeViewPage/GET_PAYEE';
export const GET_PAYEE_SUCCESS = 'app/PayeeViewPage/GET_PAYEE_SUCCESS';
export const GET_PAYEE_FAILURE = 'app/PayeeViewPage/GET_PAYEE_FAILURE';

export const GET_USER_PAYEE = 'app/PayeeViewPage/GET_USER_PAYEE';
export const GET_USER_PAYEE_SUCCESS =
  'app/PayeeViewPage/GET_USER_PAYEE_SUCCESS';
export const GET_USER_PAYEE_ERROR = 'app/PayeeViewPage/GET_USER_PAYEE_ERROR';
export const SET_PAYEE_ID = 'app/PayeeViewPage/SET_PAYEE_ID';

export const GET_PAYEE_AGREEMENT = 'app/PayeeViewPage/GET_PAYEE_AGREEMENT';
export const GET_PAYEE_AGREEMENT_SUCCESS =
  'app/PayeeViewPage/GET_PAYEE_AGREEMENT_SUCCESS';
export const GET_PAYEE_AGREEMENT_ERROR =
  'app/PayeeViewPage/GET_PAYEE_AGREEMENT_ERROR';

export const ENABLE_PAYEE = 'app/PayeeViewPage/ENABLE_PAYEE';
export const ENABLE_PAYEE_SUCCESS = 'app/PayeeViewPage/ENABLE_PAYEE_SUCCESS';
export const ENABLE_PAYEE_ERROR = 'app/PayeeViewPage/ENABLE_PAYEE_ERROR';

export const ACTIVATE_NOTIF = 'app/PayeeViewPage/ACTIVATE_NOTIF';

export const RESEND_PAYEE_AGREEMENT =
  'app/PayeeViewPage/RESEND_PAYEE_AGREEMENT';
export const RESEND_PAYEE_AGREEMENT_SUCCESS =
  'app/PayeeViewPage/RESEND_PAYEE_AGREEMENT_SUCCESS';
export const RESEND_PAYEE_AGREEMENT_ERROR =
  'app/PayeeViewPage/RESEND_PAYEE_AGREEMENT_ERROR';

export const APPROVE_PAYEE = 'app/PayeeViewPage/APPROVE_PAYEE';
export const APPROVE_PAYEE_SUCCESS = 'app/PayeeViewPage/APPROVE_PAYEE_SUCCESS';
export const APPROVE_PAYEE_ERROR = 'app/PayeeViewPage/APPROVE_PAYEE_ERROR';

export const CHECK_PAYEE = 'app/PayeeViewPage/CHECK_PAYEE';
export const CHECK_PAYEE_SUCCESS = 'app/PayeeViewPage/CHECK_PAYEE_SUCCESS';
export const CHECK_PAYEE_ERROR = 'app/PayeeViewPage/CHECK_PAYEE_ERROR';
