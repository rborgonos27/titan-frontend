import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { List } from 'semantic-ui-react';
import Gravatar from 'react-gravatar';

const UserListView = ({ list }) => {
  /* eslint-disable */
  let listRow = [];
  for (let i = 0; i<list.length; i++) {
    const userpayee = list[i];
    const user = userpayee.user;
    listRow.push(
      <List.Item id={i} key={i}>
        <List.Content floated="left">
          <Gravatar
            email={user.email}
            size={25}
            rating="pg"
            default="identicon"
            className="img-circle"
          />
        </List.Content>
        <List.Content>
          <List.Header>{`${user.last_name}, ${user.first_name}`}</List.Header>
          {`@${user.username} | ${user.member_account_id}`}
        </List.Content>
      </List.Item>
    )
  }
  /* eslint-enable */
  return <List divided verticalAlign="middle">{listRow}</List>;
};

UserListView.propTypes = {
  list: PropTypes.any.isRequired,
};

export default UserListView;
