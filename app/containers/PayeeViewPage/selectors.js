import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the payeeViewPage state domain
 */

const selectPayeeViewPageDomain = state =>
  state.get('payeeViewPage', initialState);

const selectGlobal = state => state.get('global');

/**
 * Other specific selectors
 */

/**
 * Default selector used by PayeeViewPage
 */

const makeSelectPayeeViewPage = () =>
  createSelector(selectPayeeViewPageDomain, substate => substate.toJS());

const makeSelectCurrentUser = () =>
  createSelector(selectGlobal, globalState => globalState.get('userData'));

export default makeSelectPayeeViewPage;
export { selectPayeeViewPageDomain, makeSelectCurrentUser };
