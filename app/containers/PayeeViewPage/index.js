/**
 *
 * PayeeViewPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import StandardModalYesNo from 'components/StandardModalYesNo';
import ContainerWrapper from 'components/ContainerWrapper';
import CardBox from 'components/CardBox';
import {
  Grid,
  Segment,
  Loader,
  Form,
  Divider,
  Button,
  Label,
  Icon,
} from 'semantic-ui-react';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { MemberAgreementTable } from './Components';
import makeSelectPayeeViewPage, { makeSelectCurrentUser } from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import {
  getPayee,
  getUserPayee,
  setPayeeId,
  getPayeeAgreement,
  enablePayee,
  resendPayeeAgreement,
  activateNotif,
  approvePayee,
  checkPayee,
} from './actions';
import UserListView from './UserListView';

/* eslint-disable react/prefer-stateless-function */
export class PayeeViewPage extends React.Component {
  componentDidMount() {
    const { match } = this.props;
    this.props.getPayee(match.params.id);
    this.props.getUserPayee(match.params.id);
    this.props.getPayeeAgreement(match.params.id);
  }
  componentWillReceiveProps(nextProps) {
    console.log('nextProps', nextProps);
    const { payeeviewpage } = nextProps;
    const { payeeDuplicate } = payeeviewpage;
    if (payeeDuplicate) {
      this.setState({ payeeCheckModal: true });
    }
  }

  state = {
    payeeCheckModal: false,
  };
  createRow = (title, value) => (
    <Grid.Row>
      <Grid.Column>
        <h4>{title}</h4>
      </Grid.Column>
      <Grid.Column>{value}</Grid.Column>
    </Grid.Row>
  );
  buildPayeeCheckModal = () => {
    const { payeeCheckModal } = this.state;
    const { payeeDuplicate, payee } = this.props.payeeviewpage;
    if (!payeeDuplicate) return null;
    return (
      <StandardModalYesNo
        headerText="Payee Matched"
        open={payeeCheckModal}
        onClose={() => this.setState({ payeeCheckModal: false })}
        onActionText="Continue"
        onActionAddNewText="Add New"
        noActionButton={() => this.props.approvePayee(payee)}
        onContinue={() => alert('continue')}
      >
        <div>
          <Grid columns={2}>
            {this.createRow('Payee Id', payeeDuplicate.payee_id)}
            {this.createRow('Name', payeeDuplicate.name)}
            {this.createRow('Bank Account', payeeDuplicate.bank_account)}
            {this.createRow(
              'Bank Account Name',
              payeeDuplicate.bank_account_name,
            )}
            {this.createRow(
              'Bank Routing Number',
              payeeDuplicate.bank_routing_number,
            )}
          </Grid>
          <p>
            <h3>Is this the same payee?</h3>
          </p>
        </div>
      </StandardModalYesNo>
    );
  };
  renderInfo = () => {
    const { payeeviewpage } = this.props;
    const { currentUser } = this.props;
    const { payee, loading, userData } = payeeviewpage;
    const disabledLabel = payee.deleted_at ? (
      <Label as="a" color="red" ribbon>
        Disabled
      </Label>
    ) : null;
    const statusLabel = (
      <Label as="a" color="blue" ribbon>
        {payee.status}
      </Label>
    );
    const enableButton = payee.deleted_at ? (
      <Button positive onClick={() => this.props.enablePayee(payee)}>
        Enable
      </Button>
    ) : null;
    const approveButton =
      payee.status === 'REQUESTED' ? (
        <Button positive onClick={() => this.props.checkPayee(payee)}>
          <Icon name="check circle" /> Approve
        </Button>
      ) : null;
    return (
      <Form size="huge" loading={loading}>
        {this.buildPayeeCheckModal()}
        {payee.deleted_at ? disabledLabel : statusLabel}
        {currentUser.user_type === 'super_admin' ? enableButton : null}
        {currentUser.user_type === 'super_admin' ? approveButton : null}
        <Grid className="tabGrid">
          <Grid.Row>
            <Grid.Column>
              <div className="profileLabel">
                <h4>Id</h4>
              </div>
              <div className="profileData">{payee.payee_id}</div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <div className="profileLabel">
                <h4>Name</h4>
              </div>
              <div className="profileData">{payee.name}</div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <div className="profileLabel">
                <h4>Address</h4>
              </div>
              <div className="profileData">{payee.address}</div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <div className="profileLabel">
                <h4>Bank Name</h4>
              </div>
              <div className="profileData">{payee.bank_name}</div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <div className="profileLabel">
                <h4>Bank Account Name</h4>
              </div>
              <div className="profileData">{payee.bank_account_name}</div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <div className="profileLabel">
                <h4>Bank Account</h4>
              </div>
              <div className="profileData">{payee.bank_account}</div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <div className="profileLabel">
                <h4>Bank Routing Number</h4>
              </div>
              <div className="profileData">{payee.bank_routing_number}</div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <div className="profileLabel">
                <h4>Payee Email</h4>
              </div>
              <div className="profileData">{payee.email}</div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <div className="profileLabel">
                <h4>Address</h4>
              </div>
              <div className="profileData">{payee.address}</div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <div className="profileLabel">
                <h4>2nd Address</h4>
              </div>
              <div className="profileData">{payee.address_2}</div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <div className="profileLabel">
                <h4>City</h4>
              </div>
              <div className="profileData">{payee.city}</div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <div className="profileLabel">
                <h4>State</h4>
              </div>
              <div className="profileData">{payee.state}</div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <div className="profileLabel">
                <h4>Zip Code</h4>
              </div>
              <div className="profileData">{payee.zip_code}</div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <MemberAgreementTable pageProps={this.props} />
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Divider />
        <h4>Assigned User</h4>
        <div className="listStyle">
          <UserListView list={userData} />
        </div>
        <Button onClick={() => this.props.history.goBack()} size="huge">
          Back
        </Button>
        {!payee.deleted_at ? (
          <Button
            color="green"
            onClick={() => this.props.setPayeeId(payee.id)}
            size="huge"
          >
            Edit
          </Button>
        ) : null}
      </Form>
    );
  };
  renderLoading = () => (
    <Segment>
      <Loader size="huge" active />
    </Segment>
  );

  render() {
    const { loading } = this.props.payeeviewpage;

    const renderView = loading ? this.renderLoading() : this.renderInfo();

    return (
      <ContainerWrapper>
        <CardBox>
          <h3>
            <FormattedMessage {...messages.title} />
          </h3>
          <Divider />
          {renderView}
        </CardBox>
      </ContainerWrapper>
    );
  }
}

PayeeViewPage.propTypes = {
  getPayee: PropTypes.func.isRequired,
  getUserPayee: PropTypes.func.isRequired,
  setPayeeId: PropTypes.func.isRequired,
  getPayeeAgreement: PropTypes.func.isRequired,
  enablePayee: PropTypes.func.isRequired,
  activateNotif: PropTypes.func.isRequired,
  resendPayeeAgreement: PropTypes.func.isRequired,
  match: PropTypes.any,
  history: PropTypes.any,
  payeeviewpage: PropTypes.any,
  currentUser: PropTypes.any,
  approvePayee: PropTypes.func.isRequired,
  checkPayee: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  payeeviewpage: makeSelectPayeeViewPage(),
  currentUser: makeSelectCurrentUser(),
});

const mapDispatchToProps = {
  getPayee,
  getUserPayee,
  setPayeeId,
  getPayeeAgreement,
  enablePayee,
  resendPayeeAgreement,
  activateNotif,
  approvePayee,
  checkPayee,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'payeeViewPage', reducer });
const withSaga = injectSaga({ key: 'payeeViewPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(PayeeViewPage);
