/*
 * PayeeViewPage Messages
 *
 * This contains all the text for the PayeeViewPage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.PayeeViewPage.header',
    defaultMessage: 'This is PayeeViewPage container !',
  },
  title: {
    id: 'app.containers.PayeeViewPage.title',
    defaultMessage: 'Payee Information',
  },
});
