import React from 'react';
import PropTypes from 'prop-types';
import { Table, Button } from 'semantic-ui-react';
import pdfImage from 'images/pdf.png';
import moment from 'moment';

const openInNewTab = (e, url) => {
  e.preventDefault();
  window.open(url);
};

const previewSpan = {
  display: 'inline',
  maring: 10,
};
const previewStyle = {
  display: 'inline',
  width: 30,
  height: 30,
  padding: 5,
};

const getActionButton = (record, pageProps) => {
  const { payeeviewpage } = pageProps;
  const { payee, isNotifTimeout } = payeeviewpage;
  console.log('isNotifTimeout', isNotifTimeout);
  if (!record) return null;
  if (!payee) return null;
  const data = { record, payee };
  if (record.is_accepted) {
    return (
      <span style={previewSpan}>
        <Button positive onClick={e => openInNewTab(e, record.agreement_file)}>
          <img alt="Preview" src={pdfImage} style={previewStyle} />
        </Button>
        <div>
          <b style={{ fontSize: 10 }}>Signed Terms and Agreement</b>
        </div>
      </span>
    );
  }
  if (isNotifTimeout) {
    const interval = setInterval(() => {
      const savedTime = moment(sessionStorage.getItem('emailSentTimeOut'));
      const thisTime = moment();
      if (thisTime > savedTime) {
        clearInterval(interval);
        pageProps.activateNotif();
      }
    }, 1000);
  }
  return (
    <Button
      positive
      disabled={isNotifTimeout}
      onClick={() => pageProps.resendPayeeAgreement(data)}
    >
      Resend Terms and Agreement
    </Button>
  );
};

const getPayeeAgreementsData = pageProps => {
  const { payeeAgreement } = pageProps.payeeviewpage;
  if (!payeeAgreement) return null;
  /* eslint-disable */
  let records = [];
  for (let i=0; i< payeeAgreement.length; i++) {
    const record = payeeAgreement[i];
    const recordRow = (
      <Table.Row key={i}>
        <Table.Cell>{record.member_name}</Table.Cell>
        <Table.Cell>{record && record.is_accepted ? <h3>Accepted</h3> : <h3>Not Accepted Yet</h3>}</Table.Cell>
        <Table.Cell>{getActionButton(record, pageProps)}</Table.Cell>
      </Table.Row>
    );
    records.push(recordRow);
  }
  /* eslint-enable */
  return records;
};

const MemberAgreementTable = ({ pageProps }) => (
  <div>
    <div className="profileLabel">
      <h4>Accepted Terms</h4>
    </div>
    <Table celled>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Name</Table.HeaderCell>
          <Table.HeaderCell>Status</Table.HeaderCell>
          <Table.HeaderCell>File</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>{getPayeeAgreementsData(pageProps)}</Table.Body>
    </Table>
  </div>
);

MemberAgreementTable.propTypes = {
  pageProps: PropTypes.any.isRequired,
};

export default MemberAgreementTable;
