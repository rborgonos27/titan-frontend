import { takeLatest, call, put, all } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { APP } from 'config';
import request from 'utils/request';
import { optionsFormData } from 'utils/options';
import { setPayeeId } from 'containers/App/actions';
import moment from 'moment';
import {
  GET_PAYEE,
  GET_USER_PAYEE,
  SET_PAYEE_ID,
  GET_PAYEE_AGREEMENT,
  ENABLE_PAYEE,
  RESEND_PAYEE_AGREEMENT,
  APPROVE_PAYEE,
  CHECK_PAYEE,
} from './constants';
import {
  getPayee,
  getPayeeSuccess,
  getPayeeFailure,
  getUserPayeeSuccess,
  getUserPayeeError,
  getPayeeAgreementSuccess,
  getPayeeAgreementError,
  enablePayeeSuccess,
  enablePayeeError,
  resendPayeeAgreementError,
  resendPayeeAgreementSuccess,
  approvePayee,
  approvePayeeSuccess,
  approvePayeeError,
  checkPayeeError,
  checkPayeeSuccess,
} from './actions';

export function* getRequest(requestData) {
  const API_URI = APP.API_URL;
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const requestResponse = yield call(
      request,
      `${API_URI}/payee/${requestData.data}/`,
      optionsFormData('GET', null, token),
    );
    if (requestResponse) {
      yield put(getPayeeSuccess(requestResponse));
    } else {
      yield put(getPayeeFailure(requestResponse));
    }
  } catch (error) {
    yield put(getPayeeFailure({ error }));
  }
}

export function* getPayeeUser(payee) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const payeeResponse = yield call(
      request,
      `${API_URI}/payee_user/?payee_id=${payee.data}`,
      optionsFormData('GET', null, token),
    );
    if (payeeResponse.success) {
      yield put(getUserPayeeSuccess(payeeResponse.data));
    } else {
      yield put(getUserPayeeSuccess(getUserPayeeError.message));
    }
  } catch (error) {
    yield put(getUserPayeeSuccess(error));
  }
}

export function* getPayeeAgreement(payee) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const payeeResponse = yield call(
      request,
      `${API_URI}/payee_agreement/?payee_id=${payee.data}`,
      optionsFormData('GET', null, token),
    );
    if (payeeResponse) {
      yield put(getPayeeAgreementSuccess(payeeResponse.results));
    } else {
      yield put(getPayeeAgreementError(payeeResponse));
    }
  } catch (error) {
    yield put(getPayeeAgreementError(error));
  }
}

export function* enablePayee(payee) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const formData = new FormData();
    formData.append('payee_id', payee.data.id);
    const payeeResponse = yield call(
      request,
      `${API_URI}/enable_payee/`,
      optionsFormData('PUT', formData, token),
    );
    if (payeeResponse.success) {
      yield put(enablePayeeSuccess(payeeResponse.data));
      yield put(getPayee(payee.data.id));
    } else {
      yield put(enablePayeeError(payeeResponse.message));
    }
  } catch (error) {
    yield put(enablePayeeError(error));
  }
}

export function* resendPayeeNotif(notif) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const requestData = notif.data;
    const { record, payee } = requestData;
    if (record.is_accepted === false) {
      const payeeNotif = new FormData();
      payeeNotif.append('email', payee.email);
      payeeNotif.append('payee_id', payee.id);
      payeeNotif.append('user_id', record.user_id);
      payeeNotif.append('member_name', `${record.member_name}`);
      payeeNotif.append('payee_name', payee.name);
      yield call(
        request,
        `${API_URI}/payee_notification/`,
        optionsFormData('POST', payeeNotif, token),
      );
      yield put(resendPayeeAgreementSuccess('Payee agreement sent.'));
      const emailSentTimeOut = moment()
        .add(2, 'minutes')
        .format(APP.DATE_TIME_FORMAT);
      sessionStorage.setItem('emailSentTimeOut', emailSentTimeOut);
    }
  } catch (error) {
    yield put(resendPayeeAgreementError(error));
  }
}

export function* runApprovePayee(payee) {
  try {
    const { data } = payee;
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const payeeFormData = new FormData();
    /* eslint-disable */
    const {
      name,
      bank_name,
      bank_account_name,
      bank_account,
      bank_routing_number,
      address,
      email,
      address_2,
      city,
      state,
      zip_code,
      currentUser,
    } = data;
    /* eslint-enable */
    payeeFormData.append('name', name);
    payeeFormData.append('bank_name', bank_name);
    payeeFormData.append('bank_account_name', bank_account_name);
    payeeFormData.append('bank_account', bank_account);
    payeeFormData.append('bank_routing_number', bank_routing_number);
    payeeFormData.append('email', email);
    payeeFormData.append('address', address);
    payeeFormData.append('address_2', address_2);
    payeeFormData.append('state', state);
    payeeFormData.append('status', 'APPROVED');
    payeeFormData.append('city', city);
    payeeFormData.append('zip_code', zip_code);
    const payeeResponse = yield call(
      request,
      `${API_URI}/payee/${data.id}/`,
      optionsFormData('PUT', payeeFormData, token),
    );
    console.log('payee response', payeeResponse);
    const payeeNotification = new FormData();
    payeeNotification.append('name', `Titan Admin`);
    payeeNotification.append('payee_id', payeeResponse.id);
    payeeNotification.append('payee_name', payeeResponse.name);
    yield call(
      request,
      `${API_URI}/approve/payee/notif`,
      optionsFormData('POST', payeeNotification, token),
    );
    yield put(approvePayeeSuccess(payeeResponse));
    yield put(getPayee(data.id));
  } catch (error) {
    yield put(approvePayeeError(error));
  }
}

export function* checkPayee(payee) {
  try {
    const { data } = payee;
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    /* eslint-disable */
    const { bank_account_name, bank_account, bank_routing_number, } = data;
    let queryString = `?bank_account_name=${bank_account_name}`;
    queryString = `${queryString}&bank_account=${bank_account}`;
    queryString = `${queryString}&bank_routing_number=${bank_routing_number}`;
    /* eslint-enable */
    const checkPayeeResponse = yield call(
      request,
      `${API_URI}/payee_duplicate/${queryString}`,
      optionsFormData('GET', null, token),
    );
    console.log(checkPayeeResponse);
    if (checkPayeeResponse.success) {
      yield put(checkPayeeSuccess(checkPayeeResponse.data));
    } else {
      yield put(approvePayee(data));
    }
  } catch (error) {
    yield put(checkPayeeError(error));
  }
}

export function* runSetPayeeId(payee) {
  yield put(setPayeeId(payee.data));
  yield put(push('/manage/payee'));
}

export function* requestWatcher() {
  yield takeLatest(GET_PAYEE, getRequest);
}

export function* getPayeeUserWatcher() {
  yield takeLatest(GET_USER_PAYEE, getPayeeUser);
}

export function* setPayeeIdWatcher() {
  yield takeLatest(SET_PAYEE_ID, runSetPayeeId);
}

export function* getPayeeAgreementWatcher() {
  yield takeLatest(GET_PAYEE_AGREEMENT, getPayeeAgreement);
}

export function* enablePayeeWatcher() {
  yield takeLatest(ENABLE_PAYEE, enablePayee);
}

export function* resendPayeeAgreementWatcher() {
  yield takeLatest(RESEND_PAYEE_AGREEMENT, resendPayeeNotif);
}

export function* approvePayeeWatcher() {
  yield takeLatest(APPROVE_PAYEE, runApprovePayee);
}

export function* checkPayeeWatcher() {
  yield takeLatest(CHECK_PAYEE, checkPayee);
}

export default function* rootSaga() {
  yield all([
    requestWatcher(),
    getPayeeUserWatcher(),
    setPayeeIdWatcher(),
    getPayeeAgreementWatcher(),
    enablePayeeWatcher(),
    resendPayeeAgreementWatcher(),
    approvePayeeWatcher(),
    checkPayeeWatcher(),
  ]);
}
