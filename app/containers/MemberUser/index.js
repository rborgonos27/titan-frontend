/**
 *
 * MemberUser
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import ContainerWrapper from 'components/ContainerWrapper';
import StandardModal from 'components/StandardModal';
import { DetailData, Title, HelpText, FormInput } from 'components/Utils';
import CardBox from 'components/CardBox';
import Gravatar from 'react-gravatar';
import {
  Label,
  Divider,
  Button,
  Table,
  Form,
  Grid,
  Header,
  Icon,
} from 'semantic-ui-react';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { generateRandomPassword } from 'utils/helper';
import makeSelectMemberUser, { makeSelectCurrentUser } from './selectors';
import {
  getMembers,
  addMember,
  disableMember,
  enableMember,
  updateMember,
} from './actions';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export const ADD = 'ADD';
export const EDIT = 'EDIT';
export const MAX_MEMBER = 4;

/* eslint-disable react/prefer-stateless-function */
export class MemberUser extends React.Component {
  componentDidMount() {
    const { currentUser } = this.props;
    this.props.getMembers(currentUser.id);
  }
  componentWillReceiveProps(props) {
    this.clearInput(props);
  }
  state = {
    passwordInput: '',
    emailInput: '',
    contactNo: '',
    firstName: '',
    lastName: '',
    /* eslint-disable */
    memberId: null,
    /* eslint-enable */
    openInput: false,
    openView: false,
    selectedUser: null,
    passwordInputEnable: true,
    mode: '',
  };
  clearInput = props => {
    this.setState({
      passwordInput: '',
      emailInput: '',
      contactNo: '',
      firstName: '',
      lastName: '',
      openInput: false,
      openDelete: false,
      openEnable: false,
    });
    console.log(props);
  };
  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  disableSubmitButton = () => {
    const { mode, emailInput, passwordInput, firstName, lastName } = this.state;
    if (mode === ADD)
      return (
        emailInput === '' ||
        passwordInput === '' ||
        firstName === '' ||
        lastName === ''
      );
    else if (mode === EDIT)
      return emailInput === '' || firstName === '' || lastName === '';
    return false;
  };
  disableAddButton = () => {
    const { membersData } = this.props.memberuser;
    return membersData.length >= MAX_MEMBER;
  };
  getOnContinueAction = () => {
    const { mode } = this.state;
    switch (mode) {
      case ADD:
        this.runAddMember();
        break;
      case EDIT:
        this.runUpdateMember();
        break;
      default:
        break;
    }
  };
  prepareAddMember = () => {
    const mode = ADD;
    const openInput = true;
    this.clearInput(null);
    const passwordInput = generateRandomPassword(8);
    const passwordInputEnable = true;
    this.setState({ openInput, mode, passwordInput, passwordInputEnable });
  };
  fillEditMemberData = member => {
    const memberId = member.id;
    const emailInput = member.email;
    const firstName = member.first_name;
    const lastName = member.last_name;
    const contactNo = member.primary_contact_number;
    const mode = EDIT;
    const openInput = true;
    const passwordInput = '';
    const passwordInputEnable = false;
    this.setState({
      emailInput,
      firstName,
      lastName,
      contactNo,
      mode,
      openInput,
      passwordInput,
      passwordInputEnable,
      /* eslint-disable */
      memberId,
      /* eslint-enable */
    });
  };
  handleInputNumber = e => {
    /* eslint-disable */
    const x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
    this.setState({ [e.target.name]: e.target.value });
    /* eslint-enable */
  };
  buildInputMember = () => {
    const {
      mode,
      openInput,
      emailInput,
      passwordInput,
      firstName,
      lastName,
      contactNo,
      passwordInputEnable,
    } = this.state;
    const { loading } = this.props.memberuser;
    return (
      <StandardModal
        headerText={mode === ADD ? 'Register User' : 'Update User'}
        open={openInput}
        onClose={() => this.setState({ openInput: false })}
        onActionText={mode === ADD ? 'Submit' : 'Update'}
        onContinue={() => this.getOnContinueAction()}
        btnDisabled={this.disableSubmitButton()}
      >
        <Form loading={loading}>
          <Grid>
            <Grid.Row>
              <Grid.Column width={8}>
                <FormInput
                  caption="Enter Email"
                  required
                  name="emailInput"
                  handleInput={this.handleInput}
                  placeholder="Enter Email"
                  value={emailInput}
                />
              </Grid.Column>
              <Grid.Column width={8}>
                <FormInput
                  caption="Enter Password"
                  readOnly={passwordInputEnable}
                  required={mode === ADD}
                  name="passwordInput"
                  handleInput={this.handleInput}
                  placeholder="Enter Password"
                  type="password"
                  value={passwordInput}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width={8}>
                <FormInput
                  caption="Enter First Name"
                  required
                  name="firstName"
                  handleInput={this.handleInput}
                  placeholder="Enter First Name"
                  value={firstName}
                />
              </Grid.Column>
              <Grid.Column width={8}>
                <FormInput
                  caption="Enter Last Name"
                  required
                  name="lastName"
                  handleInput={this.handleInput}
                  placeholder="Enter Last Name"
                  value={lastName}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width={8}>
                <FormInput
                  caption="Enter Contact No"
                  name="contactNo"
                  handleInput={this.handleInputNumber}
                  placeholder="Enter Last Name"
                  value={contactNo}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Form>
      </StandardModal>
    );
  };
  buildViewMember = () => {
    const { openView, selectedUser } = this.state;
    if (!selectedUser) return null;
    const { loading } = this.props.memberuser;
    return (
      <StandardModal
        headerText="Register User"
        open={openView}
        onClose={() => this.setState({ openView: false })}
        onActionText="Close"
        onContinue={() => this.setState({ openView: false })}
      >
        <Form loading={loading}>
          <Grid>
            <Grid.Row>
              <Grid.Column width={8}>
                <Form.Field>
                  <Label pointing="below">Enter Email</Label>
                  <DetailData>{selectedUser.email}</DetailData>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={8}>
                <Form.Field>
                  <Label pointing="below">Primary Contact No</Label>
                  <DetailData>{selectedUser.primary_contact_number}</DetailData>
                </Form.Field>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width={8}>
                <Form.Field>
                  <Label pointing="below">First Name</Label>
                  <DetailData>{selectedUser.first_name}</DetailData>
                </Form.Field>
              </Grid.Column>
              <Grid.Column width={8}>
                <Form.Field>
                  <Label pointing="below">Last Name</Label>
                  <DetailData>{selectedUser.last_name}</DetailData>
                </Form.Field>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Form>
      </StandardModal>
    );
  };
  buildModalEnable = () => {
    const { openEnable, selectedUser } = this.state;
    const { currentUser } = this.props;
    const { loading } = this.props.memberuser;
    return (
      <StandardModal
        headerText="Enable User"
        open={openEnable}
        onClose={() => this.setState({ openEnable: false })}
        onActionText="Enable"
        onContinue={() => {
          const data = {
            adminUserId: currentUser.id,
            userId: selectedUser.id,
          };
          this.props.enableMember(data);
        }}
      >
        <Form loading={loading}>
          <p style={{ fontSize: 14 }}>
            Are you sure you want to enable this record?
          </p>
        </Form>
      </StandardModal>
    );
  };
  buildModalDelete = () => {
    const { openDelete, selectedUser } = this.state;
    const { currentUser } = this.props;
    const { loading } = this.props.memberuser;
    return (
      <StandardModal
        headerText="Disable User"
        open={openDelete}
        onClose={() => this.setState({ openDelete: false })}
        onActionText="Disable"
        onContinue={() => {
          const data = {
            adminUserId: currentUser.id,
            userId: selectedUser.id,
          };
          this.props.disableMember(data);
        }}
      >
        <Form loading={loading}>
          <p style={{ fontSize: 14 }}>
            Are you sure you want to delete this record?
          </p>
        </Form>
      </StandardModal>
    );
  };
  displayData = selectedUser => {
    this.setState({ selectedUser, openView: true });
  };
  getMemberData = () => {
    const { membersData } = this.props.memberuser;
    if (!membersData) return null;
    const memberRowData = [];
    /* eslint-disable */
    for (let i = 0; i < membersData.length; i++) {
      const member = membersData[i];
      const isDeleted = member.deleted_at ? true : false;
      const row = (
        <Table.Row
          negative={isDeleted}
          key={i}
          onClick={() => this.displayData(member)}
        >
          <Table.Cell>
            <Header as='h4' image>
              {member.profile_picture ? (
                <img alt='' src={member.profile_picture} className="userPageImage" />
              ) : (
                <Gravatar
                  email={member.email}
                  size={25}
                  rating="pg"
                  default="identicon"
                  className="img-circle"
                />
              )}
            </Header>
            {`${member.first_name} ${member.last_name}`}
          </Table.Cell>
          <Table.Cell>{member.username}</Table.Cell>
          <Table.Cell>{member.email}</Table.Cell>
          <Table.Cell>
            {isDeleted ? null : (
              <Button
                size="small"
                onClick={e => {
                  e.preventDefault();
                  e.stopPropagation();
                  this.fillEditMemberData(member);
                }}
              >
                <Icon name="pencil" />
                Edit
              </Button>
            )}
            {isDeleted ? (
              <Button
                size="small"
                positive
                onClick={e => {
                  e.preventDefault();
                  e.stopPropagation();
                  this.setState({ selectedUser: member, openEnable: true });
                }}
              >
                <Icon name="undo" />
                Enable
              </Button>
            ) : (
              <Button
                size="small"
                negative
                onClick={e => {
                  e.preventDefault();
                  e.stopPropagation();
                  this.setState({ selectedUser: member, openDelete: true });
                }}
              >
                <Icon name="stop circle outline" />
                Disable
              </Button>
            )}

          </Table.Cell>
        </Table.Row>
      );
      memberRowData.push(row);
    }
    /* eslint-enable */
    return memberRowData;
  };
  runAddMember = () => {
    const data = this.state;
    const { currentUser } = this.props;
    data.currentUser = currentUser;
    this.props.addMember(data);
  };
  runUpdateMember = () => {
    const data = this.state;
    const { currentUser } = this.props;
    data.currentUser = currentUser;
    this.props.updateMember(data);
  };
  render() {
    return (
      <ContainerWrapper>
        <CardBox>
          {this.buildInputMember()}
          {this.buildViewMember()}
          {this.buildModalDelete()}
          {this.buildModalEnable()}
          <Title>
            <FormattedMessage {...messages.title} />
          </Title>
          <Divider />
          <Button
            disabled={this.disableAddButton()}
            positive
            onClick={() => this.prepareAddMember()}
          >
            <Icon name="add" />
            Add Member
          </Button>
          <HelpText>Note: You can only add 4 users.</HelpText>
          <Table striped selectable sortable celled>
            <Table.Header>
              <Table.Row>
                <Table.Cell>User Name</Table.Cell>
                <Table.Cell>Member Id</Table.Cell>
                <Table.Cell>Email</Table.Cell>
                <Table.Cell />
              </Table.Row>
            </Table.Header>
            <Table.Body>{this.getMemberData()}</Table.Body>
          </Table>
        </CardBox>
      </ContainerWrapper>
    );
  }
}

MemberUser.propTypes = {
  getMembers: PropTypes.func.isRequired,
  addMember: PropTypes.func.isRequired,
  updateMember: PropTypes.func.isRequired,
  memberuser: PropTypes.any.isRequired,
  disableMember: PropTypes.any.isRequired,
  enableMember: PropTypes.any.isRequired,
  currentUser: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  memberuser: makeSelectMemberUser(),
  currentUser: makeSelectCurrentUser(),
});

const mapDispatchToProps = {
  getMembers,
  addMember,
  disableMember,
  enableMember,
  updateMember,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'memberUser', reducer });
const withSaga = injectSaga({ key: 'memberUser', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(MemberUser);
