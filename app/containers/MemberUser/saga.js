import { all, takeLatest } from 'redux-saga/effects';
import {
  runGetMembers,
  addMember,
  disableMember,
  enableMember,
  updateMember,
} from './functions';
import {
  GET_MEMBERS,
  ADD_MEMBER,
  DISABLE_MEMBER,
  ENABLE_MEMBER,
  UPDATE_MEMBER,
} from './constants';

export function* getMembersWatcher() {
  yield takeLatest(GET_MEMBERS, runGetMembers);
}

export function* addMemberWatcher() {
  yield takeLatest(ADD_MEMBER, addMember);
}

export function* disableMemberWatcher() {
  yield takeLatest(DISABLE_MEMBER, disableMember);
}

export function* enableMemberWatcher() {
  yield takeLatest(ENABLE_MEMBER, enableMember);
}

export function* updateMemberWatcher() {
  yield takeLatest(UPDATE_MEMBER, updateMember);
}

export default function* rootSaga() {
  yield all([
    getMembersWatcher(),
    addMemberWatcher(),
    disableMemberWatcher(),
    enableMemberWatcher(),
    updateMemberWatcher(),
  ]);
}
