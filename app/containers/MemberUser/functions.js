import { call, put } from 'redux-saga/effects';
import request from 'utils/request';
import { APP } from 'config';
import { optionsFormData } from 'utils/options';
import {
  getMembersSuccess,
  getMembersError,
  addMemberSuccess,
  addMemberError,
  getMembers,
  disableMemberSuccess,
  disableMemberError,
  enableMemberSuccess,
  enableMemberError,
  updateMemberSuccess,
  updateMemberError,
} from './actions';

export function* runGetMembers(member) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const memberRequest = yield call(
      request,
      `${API_URI}/user_member/?user_id=${member.data}`,
      optionsFormData('GET', null, token),
    );
    if (memberRequest.success) {
      yield put(getMembersSuccess(memberRequest.data));
    } else {
      yield put(getMembersError(memberRequest.message));
    }
  } catch (error) {
    yield put(getMembersError(error));
  }
}

export function* addMember(member) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    console.log('member', member);
    const { data } = member;
    const {
      currentUser,
      emailInput,
      firstName,
      lastName,
      contactNo,
      passwordInput,
    } = data;
    const memberCall = yield call(
      request,
      `${API_URI}/user/${currentUser.id}/`,
      optionsFormData('GET', null, token),
    );
    const formData = new FormData();
    formData.append('email', emailInput);
    formData.append('username', '');
    formData.append('password', passwordInput);
    formData.append('first_name', firstName);
    formData.append('last_name', lastName);
    formData.append('primary_contact_name', firstName);
    formData.append('primary_contact_number', contactNo);
    formData.append('business_name', memberCall.business_name);
    formData.append(
      'principal_city_of_business',
      memberCall.principal_city_of_business,
    );
    formData.append('user_type', 'sub_member');
    formData.append('address', memberCall.address);
    formData.append('address_2', memberCall.address_2);
    formData.append('city', memberCall.city);
    formData.append('state', memberCall.state);
    formData.append('zip_code', memberCall.zip_code);
    formData.append('parent_user_id', memberCall.id);
    formData.append(
      'business_contact_number',
      memberCall.business_contact_number,
    );
    formData.append('is_first_time_member', true);
    const memberRequest = yield call(
      request,
      `${API_URI}/member_user/`,
      optionsFormData('POST', formData, token),
    );
    if (memberRequest.id) {
      yield put(getMembers(memberCall.id));
      yield put(addMemberSuccess(memberRequest.data));
      yield call(
        request,
        `${API_URI}/new/user/notif`,
        optionsFormData('POST', formData, token),
      );
    } else {
      yield put(addMemberError(memberRequest.message));
    }
  } catch (error) {
    yield put(addMemberError(error));
  }
}

export function* disableMember(member) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const { userId, adminUserId } = member.data;
    yield call(
      request,
      `${API_URI}/member_user/${userId}/`,
      optionsFormData('DELETE', null, token),
    );
    yield put(disableMemberSuccess(adminUserId));
    yield put(getMembers(adminUserId));
  } catch (error) {
    yield put(disableMemberError(error));
  }
}

export function* enableMember(member) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userForm = new FormData();
    const { userId, adminUserId } = member.data;
    userForm.append('user_id', userId);
    const userRequest = yield call(
      request,
      `${API_URI}/enable_user/`,
      optionsFormData('PUT', userForm, token),
    );
    if (userRequest.success) {
      yield put(enableMemberSuccess(userRequest));
      yield put(getMembers(adminUserId));
    } else {
      yield put(enableMemberError(userRequest.message));
    }
  } catch (error) {
    yield put(enableMemberError(error));
  }
}

export function* updateMember(member) {
  const API_URI = APP.API_URL;
  const token = yield JSON.parse(localStorage.getItem('token'));
  const { data } = member;
  const {
    currentUser,
    emailInput,
    firstName,
    lastName,
    contactNo,
    passwordInput,
    memberId,
  } = data;
  const memberCall = yield call(
    request,
    `${API_URI}/user/${memberId}/`,
    optionsFormData('GET', null, token),
  );
  const formData = new FormData();
  formData.append('email', emailInput);
  formData.append('username', memberCall.username);
  formData.append('first_name', firstName);
  formData.append('last_name', lastName);
  formData.append('primary_contact_name', firstName);
  formData.append('primary_contact_number', contactNo);
  formData.append('business_name', memberCall.business_name);
  formData.append('user_type', 'sub_member');
  formData.append('address', memberCall.address);
  formData.append('address_2', memberCall.address_2);
  formData.append('city', memberCall.city);
  formData.append('state', memberCall.state);
  formData.append('zip_code', memberCall.zip_code);
  formData.append('parent_user_id', currentUser.id);
  formData.append(
    'principal_city_of_business',
    memberCall.principal_city_of_business,
  );
  formData.append(
    'business_contact_number',
    memberCall.business_contact_number,
  );
  formData.append('is_first_time_member', memberCall.is_first_time_member);
  if (passwordInput === '') {
    const userRequest = yield call(
      request,
      `${API_URI}/user/account/${memberId}/`,
      optionsFormData('GET', null, token),
    );
    if (userRequest.id) {
      formData.append('password', userRequest.password);
      const memberRequest = yield call(
        request,
        `${API_URI}/member_user/${memberId}/`,
        optionsFormData('PUT', formData, token),
      );
      if (memberRequest.id) {
        yield put(updateMemberSuccess(memberRequest.data));
        yield put(getMembers(currentUser.id));
      } else {
        yield put(updateMemberError(memberRequest.message));
      }
    }
  } else {
    formData.append('password', passwordInput);
    const user = yield call(
      request,
      `${API_URI}/profile/user/${memberId}/`,
      optionsFormData('PUT', formData, token),
    );
    if (user.id) {
      yield put(updateMemberSuccess(user));
      yield put(getMembers(currentUser.id));
    } else {
      yield put(updateMemberError(user));
    }
  }
  yield call(
    request,
    `${API_URI}/update/user/notif`,
    optionsFormData('POST', formData, token),
  );
}
