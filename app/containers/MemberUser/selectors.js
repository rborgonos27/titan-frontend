import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the memberUser state domain
 */

const selectMemberUserDomain = state => state.get('memberUser', initialState);
const selectGlobal = state => state.get('global');

/**
 * Other specific selectors
 */

/**
 * Default selector used by MemberUser
 */

const makeSelectMemberUser = () =>
  createSelector(selectMemberUserDomain, substate => substate.toJS());

const makeSelectCurrentUser = () =>
  createSelector(selectGlobal, globalState => globalState.get('userData'));

export default makeSelectMemberUser;
export { selectMemberUserDomain, makeSelectCurrentUser };
