/*
 * MemberUser Messages
 *
 * This contains all the text for the MemberUser component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.MemberUser.header',
    defaultMessage: 'This is MemberUser container !',
  },
  title: {
    id: 'app.containers.MemberUser.title',
    defaultMessage: 'Manage Users',
  },
});
