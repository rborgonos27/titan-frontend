/*
 *
 * MemberUser constants
 *
 */

export const DEFAULT_ACTION = 'app/MemberUser/DEFAULT_ACTION';

export const GET_MEMBERS = 'app/MemberUser/GET_MEMBERS';
export const GET_MEMBERS_SUCCESS = 'app/MemberUser/GET_MEMBERS_SUCCESS';
export const GET_MEMBERS_ERROR = 'app/MemberUser/GET_MEMBERS_ERROR';

export const ADD_MEMBER = 'app/MemberUser/ADD_MEMBER';
export const ADD_MEMBER_SUCCESS = 'app/MemberUser/ADD_MEMBER_SUCCESS';
export const ADD_MEMBER_ERROR = 'app/MemberUser/ADD_MEMBER_ERROR';

export const DISABLE_MEMBER = 'app/MemberUser/DISABLE_MEMBER';
export const DISABLE_MEMBER_SUCCESS = 'app/MemberUser/DISABLE_MEMBER_SUCCESS';
export const DISABLE_MEMBER_ERROR = 'app/MemberUser/DISABLE_MEMBER_ERROR';

export const ENABLE_MEMBER = 'app/MemberUser/ENABLE_MEMBER';
export const ENABLE_MEMBER_SUCCESS = 'app/MemberUser/ENABLE_MEMBER_SUCCESS';
export const ENABLE_MEMBER_ERROR = 'app/MemberUser/ENABLE_MEMBER_ERROR';

export const UPDATE_MEMBER = 'app/MemberUser/UPDATE_MEMBER';
export const UPDATE_MEMBER_SUCCESS = 'app/MemberUser/UPDATE_MEMBER_SUCCESS';
export const UPDATE_MEMBER_ERROR = 'app/MemberUser/UPDATE_MEMBER_ERROR';
