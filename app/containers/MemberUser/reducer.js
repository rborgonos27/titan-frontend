/*
 *
 * MemberUser reducer
 *
 */

import { fromJS } from 'immutable';
import {
  GET_MEMBERS,
  GET_MEMBERS_SUCCESS,
  GET_MEMBERS_ERROR,
  ADD_MEMBER,
  ADD_MEMBER_SUCCESS,
  ADD_MEMBER_ERROR,
  DISABLE_MEMBER,
  DISABLE_MEMBER_SUCCESS,
  DISABLE_MEMBER_ERROR,
  ENABLE_MEMBER,
  ENABLE_MEMBER_SUCCESS,
  ENABLE_MEMBER_ERROR,
  UPDATE_MEMBER,
  UPDATE_MEMBER_SUCCESS,
  UPDATE_MEMBER_ERROR,
} from './constants';

export const initialState = fromJS({
  loading: false,
  error: null,
  membersData: [],
});

function memberUserReducer(state = initialState, action) {
  const { type, data, error } = action;
  switch (type) {
    case GET_MEMBERS:
      return state
        .set('loading', true)
        .set('error', null)
        .set('membersData', []);
    case GET_MEMBERS_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('membersData', data);
    case GET_MEMBERS_ERROR:
      return state
        .set('loading', false)
        .set('error', error)
        .set('membersData', []);
    case ADD_MEMBER:
      return state.set('loading', true).set('error', null);
    case ADD_MEMBER_SUCCESS:
      return state.set('loading', false).set('error', null);
    case ADD_MEMBER_ERROR:
      return state.set('loading', false).set('error', error);
    case DISABLE_MEMBER:
      return state.set('loading', true).set('error', null);
    case DISABLE_MEMBER_SUCCESS:
      return state.set('loading', false).set('error', null);
    case DISABLE_MEMBER_ERROR:
      return state.set('loading', false).set('error', null);
    case ENABLE_MEMBER:
      return state.set('loading', true).set('error', null);
    case ENABLE_MEMBER_SUCCESS:
      return state.set('loading', false).set('error', null);
    case ENABLE_MEMBER_ERROR:
      return state.set('loading', false).set('error', null);
    case UPDATE_MEMBER:
      return state.set('loading', true).set('error', null);
    case UPDATE_MEMBER_SUCCESS:
      return state.set('loading', false).set('error', null);
    case UPDATE_MEMBER_ERROR:
      return state.set('loading', false).set('error', error);
    default:
      return state;
  }
}

export default memberUserReducer;
