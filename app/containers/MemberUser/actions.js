/*
 *
 * MemberUser actions
 *
 */

import {
  GET_MEMBERS,
  GET_MEMBERS_ERROR,
  GET_MEMBERS_SUCCESS,
  ADD_MEMBER,
  ADD_MEMBER_ERROR,
  ADD_MEMBER_SUCCESS,
  DISABLE_MEMBER,
  DISABLE_MEMBER_SUCCESS,
  DISABLE_MEMBER_ERROR,
  ENABLE_MEMBER,
  ENABLE_MEMBER_SUCCESS,
  ENABLE_MEMBER_ERROR,
  UPDATE_MEMBER,
  UPDATE_MEMBER_SUCCESS,
  UPDATE_MEMBER_ERROR,
} from './constants';

export function defaultAction() {
  return {};
}

export function getMembers(data) {
  return {
    type: GET_MEMBERS,
    data,
  };
}

export function getMembersSuccess(data) {
  return {
    type: GET_MEMBERS_SUCCESS,
    data,
  };
}

export function getMembersError(error) {
  return {
    type: GET_MEMBERS_ERROR,
    error,
  };
}

export function addMember(data) {
  return {
    type: ADD_MEMBER,
    data,
  };
}

export function addMemberSuccess(data) {
  return {
    type: ADD_MEMBER_SUCCESS,
    data,
  };
}

export function addMemberError(error) {
  return {
    type: ADD_MEMBER_ERROR,
    error,
  };
}

export function disableMember(data) {
  return {
    type: DISABLE_MEMBER,
    data,
  };
}

export function disableMemberSuccess(data) {
  return {
    type: DISABLE_MEMBER_SUCCESS,
    data,
  };
}

export function disableMemberError(error) {
  return {
    type: DISABLE_MEMBER_ERROR,
    error,
  };
}

export function enableMember(data) {
  return {
    type: ENABLE_MEMBER,
    data,
  };
}

export function enableMemberSuccess(data) {
  return {
    type: ENABLE_MEMBER_SUCCESS,
    data,
  };
}

export function enableMemberError(error) {
  return {
    type: ENABLE_MEMBER_ERROR,
    error,
  };
}

export function updateMember(data) {
  return {
    type: UPDATE_MEMBER,
    data,
  };
}

export function updateMemberSuccess(data) {
  return {
    type: UPDATE_MEMBER_SUCCESS,
    data,
  };
}

export function updateMemberError(error) {
  return {
    type: UPDATE_MEMBER_ERROR,
    error,
  };
}
