import { fromJS } from 'immutable';
import memberUserReducer from '../reducer';

describe('memberUserReducer', () => {
  it('returns the initial state', () => {
    expect(memberUserReducer(undefined, {})).toEqual(fromJS({}));
  });
});
