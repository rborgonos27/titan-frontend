/*
 *
 * RequestViewPage constants
 *
 */

export const DEFAULT_ACTION = 'app/RequestViewPage/DEFAULT_ACTION';
export const GET_REQUEST = 'app/RequestViewPage/GET_REQUEST';
export const GET_REQUEST_SUCCESS = 'app/RequestViewPage/GET_REQUEST_SUCCESS';
export const GET_REQUEST_ERROR = 'app/RequestViewPage/GET_REQUEST_ERROR';
export const GET_BACKUP = 'app/RequestViewPage/GET_BACKUP';
export const GET_BACKUP_SUCCESS = 'app/RequestViewPage/GET_BACKUP_SUCCESS';
export const GET_BACKUP_ERROR = 'app/RequestViewPage/GET_BACKUP_ERROR';
export const GET_TIME = 'app/RequestViewPage/GET_TIME';
export const GET_TIME_SUCCESS = 'app/RequestViewPage/GET_TIME_SUCCESS';
export const GET_TIME_ERROR = 'app/RequestViewPage/GET_TIME_ERROR';

export const MODIFY_PAYMENT_DATE = 'app/RequestViewPage/MODIFY_PAYMENT_DATE';
export const MODIFY_PAYMENT_DATE_SUCCESS =
  'app/RequestViewPage/MODIFY_PAYMENT_DATE_SUCCESS';
export const MODIFY_PAYMENT_DATE_ERROR =
  'app/RequestViewPage/MODIFY_PAYMENT_DATE_ERROR';

export const MODIFY_PICKUP_DATE = 'app/RequestViewPage/MODIFY_PICKUP_DATE';
export const MODIFY_PICKUP_DATE_SUCCESS =
  'app/RequestViewPage/MODIFY_PICKUP_DATE_SUCCESS';
export const MODIFY_PICKUP_DATE_ERROR =
  'app/RequestViewPage/MODIFY_PICKUP_DATE_ERROR';

export const CANCEL_PAYMENT = 'app/RequestViewPage/CANCEL_PAYMENT';
export const CANCEL_PAYMENT_SUCCESS =
  'app/RequestViewPage/CANCEL_PAYMENT_SUCCESS';
export const CANCEL_PAYMENT_ERROR = 'app/RequestViewPage/CANCEL_PAYMENT_ERROR';

export const TRANSACTION_DATA = 'app/RequestViewPage/TRANSACTION_DATA';
export const TRANSACTION_DATA_SUCCESS =
  'app/RequestViewPage/TRANSACTION_DATA_SUCCESS';
export const TRANSACTION_DATA_FAILURE =
  'app/RequestViewPage/TRANSACTION_DATA_FAILURE';

export const ADMIN_ACTION = 'app/RequestViewPage/ADMIN_ACTION';
export const ADMIN_ACTION_SUCCESS = 'app/RequestViewPage/ADMIN_ACTION_SUCCESS';
export const ADMIN_ACTION_FAILURE = 'app/RequestViewPage/ADMIN_ACTION_FAILURE';

export const REQUEST_REVISION = 'app/RequestViewPage/REQUEST_REVISION';
export const REQUEST_REVISION_SUCCESS =
  'app/RequestViewPage/REQUEST_REVISION_SUCCESS';
export const REQUEST_REVISION_ERROR =
  'app/RequestViewPage/REQUEST_REVISION_ERROR';

export const GET_USER_FEE_TIER_RATE =
  'app/RequestViewPage/GET_USER_FEE_TIER_RATE';
export const GET_USER_FEE_TIER_RATE_SUCCESS =
  'app/RequestViewPage/GET_USER_FEE_TIER_RATE_SUCCESS';
export const GET_USER_FEE_TIER_RATE_ERROR =
  'app/RequestViewPage/GET_USER_FEE_TIER_RATE_ERROR';

export const GET_FEE_RATE = 'app/RequestViewPage/GET_FEE_RATE';
export const GET_FEE_RATE_SUCCESS = 'app/RequestViewPage/GET_FEE_RATE_SUCCESS';
export const GET_FEE_RATE_ERROR = 'app/RequestViewPage/GET_FEE_RATE_ERROR';

export const REVERT_TRANSACTION = 'app/RequestViewPage/REVERT_TRANSACTION';
export const REVERT_TRANSACTION_SUCCESS =
  'app/RequestViewPage/REVERT_TRANSACTION_SUCCESS';
export const REVERT_TRANSACTION_ERROR =
  'app/RequestViewPage/REVERT_TRANSACTION_ERROR';

export const RESEND_PAYEE_NOTIF = 'app/RequestViewPage/RESEND_PAYEE_NOTIF';
export const RESEND_PAYEE_NOTIF_SUCCESS =
  'app/RequestViewPage/RESEND_PAYEE_NOTIF_SUCCESS';
export const RESEND_PAYEE_NOTIF_ERROR =
  'app/RequestViewPage/RESEND_PAYEE_NOTIF_ERROR';
export const ACTIVATE_NOTIF = 'app/RequestViewPage/ACTIVATE_NOTIF';

export const GET_USER_PAYEE_AGREEMENT =
  'app/RequestViewPage/GET_USER_PAYEE_AGREEMENT';

export const GET_USER_PAYEE_AGREEMENT_SUCCESS =
  'app/RequestViewPage/GET_USER_PAYEE_AGREEMENT_SUCCESS';

export const GET_USER_PAYEE_AGREEMENT_ERROR =
  'app/RequestViewPage/GET_USER_PAYEE_AGREEMENT_ERROR';

export const FILE_UPLOAD = 'app/RequestViewPage/FILE_UPLOAD';
export const FILE_UPLOAD_SUCCESS = 'app/RequestViewPage/FILE_UPLOAD_SUCCESS';
export const FILE_UPLOAD_ERROR = 'app/RequestViewPage/FILE_UPLOAD_ERROR';

export const REMOVE_FILE = 'app/RequestViewPage/REMOVE_FILE';
export const REMOVE_FILE_SUCCESS = 'app/RequestViewPage/REMOVE_FILE_SUCCESS';
export const REMOVE_FILE_ERROR = 'app/RequestViewPage/REMOVE_FILE_ERROR';

export const UPDATE_REQUEST = 'app/RequestViewPage/UPDATE_REQUEST';
export const UPDATE_REQUEST_SUCCESS =
  'app/RequestViewPage/UPDATE_REQUEST_SUCCESS';
export const UPDATE_REQUEST_ERROR = 'app/RequestViewPage/UPDATE_REQUEST_ERROR';

export const GET_DELETED_BACKUP = 'app/RequestViewPage/GET_DELETED_BACKUP';
export const GET_DELETED_BACKUP_SUCCESS =
  'app/RequestViewPage/GET_DELETED_BACKUP_SUCCESS';
export const GET_DELETED_BACKUP_ERROR =
  'app/RequestViewPage/GET_DELETED_BACKUP_ERROR';
