/*
 *
 * RequestViewPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  GET_REQUEST,
  GET_REQUEST_SUCCESS,
  GET_REQUEST_ERROR,
  GET_BACKUP,
  GET_BACKUP_SUCCESS,
  GET_BACKUP_ERROR,
  GET_DELETED_BACKUP,
  GET_DELETED_BACKUP_SUCCESS,
  GET_DELETED_BACKUP_ERROR,
  GET_TIME,
  GET_TIME_SUCCESS,
  GET_TIME_ERROR,
  MODIFY_PAYMENT_DATE,
  MODIFY_PAYMENT_DATE_SUCCESS,
  MODIFY_PAYMENT_DATE_ERROR,
  MODIFY_PICKUP_DATE,
  MODIFY_PICKUP_DATE_SUCCESS,
  MODIFY_PICKUP_DATE_ERROR,
  CANCEL_PAYMENT,
  CANCEL_PAYMENT_SUCCESS,
  CANCEL_PAYMENT_ERROR,
  TRANSACTION_DATA,
  TRANSACTION_DATA_SUCCESS,
  TRANSACTION_DATA_FAILURE,
  ADMIN_ACTION,
  ADMIN_ACTION_SUCCESS,
  ADMIN_ACTION_FAILURE,
  REQUEST_REVISION,
  REQUEST_REVISION_SUCCESS,
  REQUEST_REVISION_ERROR,
  GET_USER_FEE_TIER_RATE,
  GET_USER_FEE_TIER_RATE_SUCCESS,
  GET_USER_FEE_TIER_RATE_ERROR,
  GET_FEE_RATE,
  GET_FEE_RATE_SUCCESS,
  GET_FEE_RATE_ERROR,
  REVERT_TRANSACTION,
  REVERT_TRANSACTION_ERROR,
  REVERT_TRANSACTION_SUCCESS,
  RESEND_PAYEE_NOTIF,
  RESEND_PAYEE_NOTIF_ERROR,
  RESEND_PAYEE_NOTIF_SUCCESS,
  ACTIVATE_NOTIF,
  GET_USER_PAYEE_AGREEMENT,
  GET_USER_PAYEE_AGREEMENT_ERROR,
  GET_USER_PAYEE_AGREEMENT_SUCCESS,
  FILE_UPLOAD,
  FILE_UPLOAD_SUCCESS,
  FILE_UPLOAD_ERROR,
  REMOVE_FILE,
  REMOVE_FILE_SUCCESS,
  REMOVE_FILE_ERROR,
  UPDATE_REQUEST,
  UPDATE_REQUEST_SUCCESS,
  UPDATE_REQUEST_ERROR,
} from './constants';

export const initialState = fromJS({
  loading: true,
  error: false,
  request: null,
  backup: [],
  backupLoading: true,
  backupError: false,
  currentTime: null,
  tableData: null,
  tableLoading: false,
  revisionData: null,
  revisionLoading: false,
  feeTierObject: null,
  feeObject: null,
  notif: null,
  isNotifTimeout: false,
  payeeAgreement: null,
  errorList: [],
  filesLoading: false,
  files: [],
  isUploaded: false,
  deletedBackup: [],
});

function requestViewPageReducer(state = initialState, action) {
  switch (action.type) {
    case GET_REQUEST:
      return state
        .set('loading', true)
        .set('request', null)
        .set('error', false);
    case GET_REQUEST_SUCCESS:
      return state
        .set('loading', false)
        .set('request', action.data)
        .set('error', false);
    case GET_REQUEST_ERROR:
      return state
        .set('loading', false)
        .set('request', null)
        .set('error', action.data.error);
    case GET_BACKUP:
      return state
        .set('backupLoading', true)
        .set('backup', [])
        .set('files', [])
        .set('backupError', false);
    case GET_BACKUP_SUCCESS:
      return state
        .set('backupLoading', false)
        .set('backup', action.data)
        .set('files', action.data)
        .set('backupError', false);
    case GET_BACKUP_ERROR:
      return state
        .set('backupLoading', false)
        .set('backup', {})
        .set('files', {})
        .set('backupError', action.data.error);
    case GET_DELETED_BACKUP:
      return state
        .set('backupLoading', true)
        .set('deletedBackup', [])
        .set('backupError', false);
    case GET_DELETED_BACKUP_SUCCESS:
      return state
        .set('backupLoading', false)
        .set('deletedBackup', action.data)
        .set('backupError', false);
    case GET_DELETED_BACKUP_ERROR:
      return state
        .set('backupLoading', false)
        .set('deletedBackup', [])
        .set('backupError', action.data.error);
    case GET_TIME:
      return state.set('curentTime', null).set('error', false);
    case GET_TIME_SUCCESS:
      return state
        .set('error', false)
        .set('currentTime', action.data.current_time);
    case GET_TIME_ERROR:
      return state.set('error', action.data.error).set('currentTime', null);
    case MODIFY_PAYMENT_DATE:
      return state
        .set('loading', true)
        .set('request', null)
        .set('error', false);
    case MODIFY_PAYMENT_DATE_SUCCESS:
      return state
        .set('loading', false)
        .set('request', action.data)
        .set('error', false);
    case MODIFY_PAYMENT_DATE_ERROR:
      return state
        .set('loading', false)
        .set('request', null)
        .set('error', action.data.error);
    case MODIFY_PICKUP_DATE:
      return state
        .set('loading', true)
        .set('request', null)
        .set('error', false);
    case MODIFY_PICKUP_DATE_SUCCESS:
      return state
        .set('loading', false)
        .set('request', action.data)
        .set('error', false);
    case MODIFY_PICKUP_DATE_ERROR:
      return state
        .set('loading', false)
        .set('request', null)
        .set('error', action.data.error);
    case CANCEL_PAYMENT:
      return state
        .set('loading', true)
        .set('request', null)
        .set('error', false);
    case CANCEL_PAYMENT_SUCCESS:
      return state
        .set('loading', false)
        .set('request', action.data)
        .set('error', false);
    case CANCEL_PAYMENT_ERROR:
      return state
        .set('loading', false)
        .set('request', null)
        .set('error', action.data.error);
    case TRANSACTION_DATA:
      return state
        .set('tableLoading', true)
        .set('tableData', null)
        .set('error', false);
    case TRANSACTION_DATA_SUCCESS:
      return state
        .set('tableLoading', false)
        .set('tableData', action.data)
        .set('error', false);
    case TRANSACTION_DATA_FAILURE:
      return state
        .set('tableLoading', false)
        .set('tableData', null)
        .set('error', action.data.error);
    case ADMIN_ACTION:
      return state
        .set('loading', true)
        .set('request', null)
        .set('error', false);
    case ADMIN_ACTION_SUCCESS:
      return state
        .set('loading', false)
        .set('request', action.data)
        .set('error', false);
    case ADMIN_ACTION_FAILURE:
      return state
        .set('loading', false)
        .set('request', null)
        .set('error', action.data.error);
    case REVERT_TRANSACTION:
      return state
        .set('loading', true)
        .set('request', null)
        .set('error', false);
    case REVERT_TRANSACTION_SUCCESS:
      return state
        .set('loading', false)
        .set('request', action.data)
        .set('error', false);
    case REVERT_TRANSACTION_ERROR:
      return state
        .set('loading', false)
        .set('request', null)
        .set('error', action.data.error);
    case REQUEST_REVISION:
      return state
        .set('revisionLoading', true)
        .set('revisionData', null)
        .set('error', false);
    case REQUEST_REVISION_SUCCESS:
      return state
        .set('revisionLoading', false)
        .set('revisionData', action.data)
        .set('error', false);
    case REQUEST_REVISION_ERROR:
      return state
        .set('revisionLoading', false)
        .set('revisionData', null)
        .set('error', action.data.error);
    case GET_USER_FEE_TIER_RATE:
      return state
        .set('loading', true)
        .set('error', null)
        .set('feeTierObject', null);
    case GET_USER_FEE_TIER_RATE_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('feeTierObject', action.data);
    case GET_USER_FEE_TIER_RATE_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('feeTierObject', null);
    case GET_FEE_RATE:
      return state
        .set('loading', true)
        .set('error', null)
        .set('feeObject', null);
    case GET_FEE_RATE_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('feeObject', action.data);
    case GET_FEE_RATE_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('feeObject', action.error);
    case RESEND_PAYEE_NOTIF:
      return state
        .set('loading', true)
        .set('error', action.error)
        .set('isNotifTimeout', false)
        .set('notif', null);
    case RESEND_PAYEE_NOTIF_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('isNotifTimeout', true)
        .set('notif', action.data);
    case RESEND_PAYEE_NOTIF_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('isNotifTimeout', false)
        .set('notif', null);
    case ACTIVATE_NOTIF:
      return state.set('isNotifTimeout', false);
    case GET_USER_PAYEE_AGREEMENT:
      return state
        .set('loading', true)
        .set('error', null)
        .set('payeeAgreement', null);
    case GET_USER_PAYEE_AGREEMENT_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('payeeAgreement', action.data);
    case GET_USER_PAYEE_AGREEMENT_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('payeeAgreement', null);
    case FILE_UPLOAD:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', [])
        .set('isUploaded', false)
        .set('filesLoading', true);
    case FILE_UPLOAD_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', action.data)
        .set('isUploaded', true)
        .set('filesLoading', false);
    case FILE_UPLOAD_ERROR:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', [])
        .set('filesError', action.data.error)
        .set('isUploaded', false)
        .set('filesLoading', false);
    case REMOVE_FILE:
      return state
        .set('filesLoading', true)
        .set('isUploaded', false)
        .set('error', null)
        .set('files', []);
    case REMOVE_FILE_SUCCESS:
      return state
        .set('filesLoading', false)
        .set('isUploaded', true)
        .set('error', null)
        .set('files', action.data);
    case REMOVE_FILE_ERROR:
      return state
        .set('filesLoading', false)
        .set('isUploaded', false)
        .set('error', action.error)
        .set('files', []);
    case UPDATE_REQUEST:
      return state.set('loading', true).set('error', null);
    case UPDATE_REQUEST_SUCCESS:
      return state.set('loading', false).set('error', null);
    case UPDATE_REQUEST_ERROR:
      return state.set('loading', false).set('error', action.error);
    default:
      return state;
  }
}

export default requestViewPageReducer;
