import { all, takeLatest, call, put } from 'redux-saga/effects';
import { APP } from 'config';
import request from 'utils/request';
import { optionsFormData } from 'utils/options';
import moment from 'moment';
import {
  GET_REQUEST,
  GET_BACKUP,
  GET_TIME,
  CANCEL_PAYMENT,
  MODIFY_PAYMENT_DATE,
  MODIFY_PICKUP_DATE,
  TRANSACTION_DATA,
  ADMIN_ACTION,
  REQUEST_REVISION,
  GET_USER_FEE_TIER_RATE,
  GET_FEE_RATE,
  REVERT_TRANSACTION,
  RESEND_PAYEE_NOTIF,
  GET_USER_PAYEE_AGREEMENT,
  FILE_UPLOAD,
  REMOVE_FILE,
  UPDATE_REQUEST,
  GET_DELETED_BACKUP,
} from './constants';
import {
  getRequestError,
  getRequestSuccess,
  getRequest,
  getFeeRate,
  getBackupSuccess,
  getBackupError,
  getTimeSuccess,
  getTimeError,
  cancelPaymentSuccess,
  cancelPaymentFailure,
  modifyPaymentDateSuccess,
  modifyPaymentDateError,
  modifyPickupDateError,
  modifyPickupDateSuccess,
  transactionData,
  transactionDataSuccess,
  transactionDataFailure,
  adminActionSuccess,
  adminActionFailure,
  requestRevision,
  requestRevisionSuccess,
  requestRevisionFailure,
  getUserFeeTierRateSuccess,
  getUserFeeTierRateError,
  getUserFeeTierRate,
  getFeeRateError,
  getFeeRateSuccess,
  revertTransactionSuccess,
  revertTransactionError,
  resendPayeeNotifSuccess,
  resendPayeeNotifError,
  getUserPayeeAgreementSuccess,
  getUserPayeeAgreementError,
  fileUploadSuccess,
  getBackup,
  removeFileError,
  removeFileSuccess,
  updateRequestError,
  updateRequestSuccess,
  getDeletedBackupError,
  getDeletedBackupSuccess,
  getDeletedBackup,
} from './actions';

export function* getRequestData(requestData) {
  const API_URI = APP.API_URL;
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const requestResponse = yield call(
      request,
      `${API_URI}/request/detail/${requestData.data}/`,
      optionsFormData('GET', null, token),
    );
    if (requestResponse.success) {
      yield put(getUserFeeTierRate(requestResponse.data.user.id));
      yield put(getFeeRate(requestResponse.data.user.id));
      yield put(getRequestSuccess(requestResponse.data));
    } else {
      yield put(getRequestSuccess(requestResponse));
    }
  } catch (error) {
    yield put(getRequestError({ error }));
  }
}

export function* runGetBackup(requestData) {
  const API_URI = APP.API_URL;
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const requestResponse = yield call(
      request,
      `${API_URI}/backup/?request_id=${requestData.data}`,
      optionsFormData('GET', null, token),
    );
    if (requestResponse.success) {
      yield put(getBackupSuccess(requestResponse.data));
    } else {
      yield put(getBackupError(requestResponse));
    }
  } catch (error) {
    yield put(getBackupError({ error }));
  }
}

export function* runGetDeletedBackup(requestData) {
  const API_URI = APP.API_URL;
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const requestResponse = yield call(
      request,
      `${API_URI}/backup/?request_id=${requestData.data}&show_deleted=1`,
      optionsFormData('GET', null, token),
    );
    if (requestResponse.success) {
      yield put(getDeletedBackupSuccess(requestResponse.data));
    } else {
      yield put(getDeletedBackupError(requestResponse));
    }
  } catch (error) {
    yield put(getDeletedBackupError({ error }));
  }
}

export function* getTime() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const serverTime = yield call(
      request,
      `${API_URI}/time/`,
      optionsFormData('GET', null, token),
    );
    if (serverTime.current_time) {
      yield put(getTimeSuccess(serverTime));
    } else {
      yield put(getTimeError(serverTime));
    }
  } catch (error) {
    yield put(getTimeError(error));
  }
}

export function* getUserFeeRate(user) {
  try {
    const API_URI = APP.API_URL;
    const { data } = user;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const feeRate = yield call(
      request,
      `${API_URI}/fee/?user_id=${data}`,
      optionsFormData('GET', null, token),
    );
    if (feeRate.results.length > 0) {
      yield put(getFeeRateSuccess(feeRate.results[0]));
    } else {
      yield put(getFeeRateError(feeRate));
    }
  } catch (error) {
    yield put(getFeeRateError(error));
  }
}

export function* cancelPayment(dataRequest) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const formData = new FormData();
    const requestData = dataRequest.data.request;
    const payAt = moment(requestData.due_at).format(APP.DATE_FORMAT);
    const pickUpAt = moment(requestData.pickup_at).format(APP.DATE_FORMAT);
    formData.append('type', requestData.type);
    formData.append('to', 'Anywhere');
    formData.append('status', 'CANCELLED');
    formData.append('amount', requestData.amount);
    formData.append('fee', requestData.fee);
    formData.append('due_at', `${payAt} 00:00`);
    formData.append('pickup_at', `${pickUpAt} 00:00`);
    formData.append('remarks', requestData.remarks);
    formData.append('payee_id', requestData.payee.id);
    formData.append('transaction_type', requestData.transaction_type);
    const requestResponse = yield call(
      request,
      `${API_URI}/request/${requestData.id}/`,
      optionsFormData('PUT', formData, token),
    );
    if (requestResponse.success) {
      yield put(cancelPaymentSuccess(requestResponse.data));
      yield put(transactionData(requestData.id));
      const notifForm = new FormData();
      notifForm.append('id', requestData.id);
      if (dataRequest.data.currentUser.user_type !== 'super_admin') {
        yield call(
          request,
          `${API_URI}/update/request/notif`,
          optionsFormData('POST', notifForm, token),
        );
      }
    } else {
      yield put(cancelPaymentFailure(requestResponse));
    }
  } catch (error) {
    yield put(cancelPaymentFailure(error));
  }
}

export function* modifyPaymentDate(data) {
  try {
    const { payAt } = data.data;
    const requestData = data.data.request;
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const paymentFormData = new FormData();
    paymentFormData.append('type', requestData.type);
    paymentFormData.append('to', 'Anywhere');
    paymentFormData.append('status', requestData.status);
    paymentFormData.append('amount', requestData.amount);
    paymentFormData.append('fee', requestData.fee);
    paymentFormData.append('due_at', `${payAt} 00:00`);
    paymentFormData.append('pickup_at', requestData.pickup_at);
    paymentFormData.append('transaction_type', requestData.transaction_type);
    paymentFormData.append('remarks', requestData.remarks);
    paymentFormData.append('payee_id', requestData.payee.id);

    const requestResponse = yield call(
      request,
      `${API_URI}/request/${requestData.id}/`,
      optionsFormData('PUT', paymentFormData, token),
    );
    if (requestResponse.success) {
      yield put(modifyPaymentDateSuccess(requestResponse.data));
      const notifForm = new FormData();
      notifForm.append('id', requestData.id);
      if (data.data.currentUser.user_type !== 'super_admin') {
        yield call(
          request,
          `${API_URI}/update/request/notif`,
          optionsFormData('POST', notifForm, token),
        );
      }
    } else {
      yield put(modifyPaymentDateError(requestResponse));
    }
  } catch (error) {
    yield put(modifyPaymentDateError(error));
  }
}

export function* modifyPickupDate(data) {
  try {
    const { pickupAt } = data.data;
    const requestData = data.data.request;
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const paymentFormData = new FormData();
    paymentFormData.append('type', requestData.type);
    paymentFormData.append('to', 'Anywhere');
    paymentFormData.append('status', requestData.status);
    paymentFormData.append('amount', requestData.amount);
    paymentFormData.append('fee', requestData.fee);
    paymentFormData.append('due_at', requestData.due_at);
    paymentFormData.append('pickup_at', `${pickupAt} 00:00`);
    paymentFormData.append('remarks', requestData.remarks);
    paymentFormData.append('transaction_type', requestData.transaction_type);
    paymentFormData.append('payee_id', requestData.payee.id);

    const requestResponse = yield call(
      request,
      `${API_URI}/request/${requestData.id}/`,
      optionsFormData('PUT', paymentFormData, token),
    );
    if (requestResponse.success) {
      yield put(modifyPickupDateSuccess(requestResponse.data));
      const notifForm = new FormData();
      notifForm.append('id', requestData.id);
      if (data.data.currentUser.user_type !== 'super_admin') {
        yield call(
          request,
          `${API_URI}/update/request/notif`,
          optionsFormData('POST', notifForm, token),
        );
      }
    } else {
      yield put(modifyPickupDateError(requestResponse));
    }
  } catch (error) {
    yield put(modifyPickupDateError({ error }));
  }
}

export function* transactionDataRequest(requestData) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const requestResponse = yield call(
      request,
      `${API_URI}/transaction/?request_id=${requestData.data}`,
      optionsFormData('GET', null, token),
    );
    if (requestResponse.results) {
      yield put(transactionDataSuccess(requestResponse));
    } else {
      yield put(transactionDataFailure(requestResponse));
    }
  } catch (error) {
    transactionDataFailure(error);
  }
}

export function* adminAction(data) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const requestData = data.data;
    const requestId = requestData.id;
    let requestStatus = 'PROCESSING';
    if (requestData.status === 'REQUESTED') {
      requestStatus = 'PROCESSING';
    } else if (requestData.status === 'PROCESSING') {
      requestStatus =
        requestData.transaction_type === 'ELECTRONIC'
          ? 'COMPLETED'
          : 'SCHEDULED';
    } else if (requestData.status === 'SCHEDULED') {
      requestStatus = 'COMPLETED';
    }
    const formData = new FormData();
    const payAt = moment(data.data.due_at).format('YYYY-MM-DD');
    formData.append('status', requestStatus);
    formData.append('to', 'Anywhere');
    formData.append('amount', data.data.amount);
    formData.append('type', data.data.type);
    formData.append('pay_at', payAt);
    formData.append('pickup_at', data.data.pickup_at);
    formData.append('fee', data.data.fee);
    formData.append('due_at', data.data.due_at);
    formData.append('remarks', data.data.remarks);
    formData.append('transaction_type', data.data.transaction_type);
    const endPoint = `${API_URI}/request/${requestId}/`;
    const processingRequest = yield call(
      request,
      endPoint,
      optionsFormData('PUT', formData, token),
    );
    // request status is processing
    if (data.data.type === 'PAYMENT' && requestStatus === 'PROCESSING') {
      // check if payee is accepts the T&A
      /* eslint-disable */
      const endPoint = `${API_URI}/request/detail/${requestId}/`;
      const getRequest = yield call(
        request,
        endPoint,
        optionsFormData('GET', null, token),
      );
      if (getRequest.success) {
        const { payee, user, payee_agreement } = getRequest.data;
        if (!payee_agreement) {
          const payeeAgreementForm = new FormData();
          payeeAgreementForm.append('payee_id', payee.id);
          payeeAgreementForm.append('user_id', user.id);
          payeeAgreementForm.append('remarks', 'for review and sign');
          payeeAgreementForm.append('is_accepted', false);
          yield call(
            request,
            `${API_URI}/payee_agreement/`,
            optionsFormData('POST', payeeAgreementForm, null),
          );
          /* eslint-enable */
          const payeeNotif = new FormData();
          payeeNotif.append('email', payee.email);
          payeeNotif.append('payee_id', payee.id);
          payeeNotif.append('user_id', user.id);
          payeeNotif.append('member_name', `${user.business_name}`);
          payeeNotif.append('payee_name', payee.name);
          yield call(
            request,
            `${API_URI}/payee_notification/`,
            optionsFormData('POST', payeeNotif, token),
          );
        }
      }
    }
    if (processingRequest.success) {
      yield put(adminActionSuccess(processingRequest.data));
      yield put(getRequest(requestId));
      yield put(transactionData(requestId));
      yield put(requestRevision(requestId));
    } else {
      yield put(adminActionFailure(processingRequest));
    }
  } catch (error) {
    yield put(adminActionFailure({ error }));
  }
}

export function* fileUpload(file) {
  try {
    const API_URI = APP.API_URL;
    const { files, requestId, requestType } = file.data;
    /* eslint-disable */
    for (const i in files) {
      const fileData = files[i];
      const token = yield JSON.parse(localStorage.getItem('token'));
      const formData = new FormData();
      formData.append('filename', fileData);
      formData.append('url', 'filename');
      formData.append('mime_type', fileData.type);
      formData.append('type', requestType);
      formData.append('resource_id', requestId);
      formData.append('remarks', '');
      formData.append(
        'expire_at',
        moment()
          .add(1, 'hours')
          .format('YYYY-MM-DD h:m:s'),
      );
      const fileResponse = yield call(
        request,
        `${API_URI}/file_request/`,
        optionsFormData('POST', formData, token),
      );
      if (fileResponse.filename) {
        yield put(fileUploadSuccess([]));
      }
    }
    yield put(getBackup(requestId));
    /* eslint-enable */
  } catch (error) {
    console.log(error);
  }
}

export function getDividedAmountOnTier(
  feeTierObject,
  totalDeposit,
  amount,
  requestType,
) {
  const firstFeeTier = feeTierObject[0];
  const secondFeeTier = feeTierObject[1];
  /* eslint-disable */
  const thirdfeeTier = feeTierObject[2];
  /* eslint-enable */
  let firstTierAmount = 0;
  let secondTierAmount = 0;
  let thirdTierAmount = 0;
  const amountParsed = parseFloat(amount);
  const totalDepositParsed = parseFloat(totalDeposit);
  const totalAmount = amountParsed + totalDepositParsed;
  const firstFeeTierThreshold =
    requestType === 'DEPOSIT'
      ? firstFeeTier.deposit_threshold
      : firstFeeTier.payment_threshold;
  const secondFeeTierThreshold =
    requestType === 'DEPOSIT'
      ? secondFeeTier.deposit_threshold
      : secondFeeTier.payment_threshold;
  if (totalDepositParsed > 0) {
    if (totalDepositParsed < firstFeeTierThreshold) {
      if (totalAmount > secondFeeTierThreshold) {
        firstTierAmount = firstFeeTierThreshold - totalDepositParsed;
        secondTierAmount = secondFeeTierThreshold - firstFeeTierThreshold;
        thirdTierAmount = amountParsed - firstTierAmount - secondTierAmount;
      } else if (totalAmount > firstFeeTierThreshold) {
        firstTierAmount = firstFeeTierThreshold - totalDepositParsed;
        secondTierAmount = amountParsed - firstTierAmount;
      } else {
        firstTierAmount = amountParsed;
      }
    } else if (totalDepositParsed < secondFeeTierThreshold) {
      if (totalAmount > secondFeeTierThreshold) {
        secondTierAmount = secondFeeTierThreshold - totalDepositParsed;
        thirdTierAmount = amountParsed - secondTierAmount;
      } else {
        secondTierAmount = amountParsed;
      }
    } else if (totalDepositParsed > secondFeeTierThreshold) {
      thirdTierAmount = amountParsed;
    }
    return { firstTierAmount, secondTierAmount, thirdTierAmount };
  }

  if (amountParsed < firstFeeTierThreshold) {
    firstTierAmount = amountParsed;
  } else if (amountParsed < secondFeeTierThreshold) {
    firstTierAmount = firstFeeTierThreshold;
    secondTierAmount = amountParsed - firstTierAmount;
  } else if (amountParsed > secondFeeTierThreshold) {
    firstTierAmount = firstFeeTierThreshold; // 600, 000
    secondTierAmount = secondFeeTierThreshold - firstTierAmount;
    thirdTierAmount = amountParsed - secondFeeTierThreshold; // 400, 000
  }
  return { firstTierAmount, secondTierAmount, thirdTierAmount };
}

export function getComputedFee(
  feeTierObject,
  totalTransactionAmount,
  amount,
  requestType,
) {
  const origAmount = amount.replace(/,/g, '').replace('$', '');
  const feeTotalTransactionAmount =
    parseFloat(origAmount) + parseFloat(totalTransactionAmount);
  const segregateAmountByTier = getDividedAmountOnTier(
    feeTierObject,
    totalTransactionAmount,
    amount,
    requestType,
  );

  const firstFeeTier = feeTierObject[0];
  const secondFeeTier = feeTierObject[1];
  const thirdFeeTier = feeTierObject[2];
  const {
    firstTierAmount,
    secondTierAmount,
    thirdTierAmount,
  } = segregateAmountByTier;
  let feePercent = 0;
  if (thirdTierAmount > 0) {
    feePercent =
      requestType === 'DEPOSIT'
        ? thirdFeeTier.deposit_rate
        : thirdFeeTier.payment_rate;
  } else if (secondTierAmount > 0) {
    feePercent =
      requestType === 'DEPOSIT'
        ? secondFeeTier.deposit_rate
        : secondFeeTier.payment_rate;
  } else {
    feePercent = firstFeeTier.deposit_rate;
  }
  const firstFeeRate =
    requestType === 'DEPOSIT'
      ? firstFeeTier.deposit_rate
      : firstFeeTier.payment_rate;
  const secondFeeRate =
    requestType === 'DEPOSIT'
      ? secondFeeTier.deposit_rate
      : secondFeeTier.payment_rate;
  const thirdFeeRate =
    requestType === 'DEPOSIT'
      ? thirdFeeTier.deposit_rate
      : thirdFeeTier.payment_rate;
  const firstTierFee = firstFeeRate * firstTierAmount;
  const secondTierFee = secondFeeRate * secondTierAmount;
  const thirdTierFee = thirdFeeRate * thirdTierAmount;
  const feeTotal = firstTierFee + secondTierFee + thirdTierFee;
  return { feePercent, feeTotal, feeTotalTransactionAmount };
}

export function* requestRevisionData(revision) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const requestResponse = yield call(
      request,
      `${API_URI}/requestrevisions/?request_id=${revision.data}`,
      optionsFormData('GET', null, token),
    );
    if (requestResponse.results) {
      yield put(requestRevisionSuccess(requestResponse));
    } else {
      yield put(requestRevisionFailure(requestResponse));
    }
  } catch (error) {
    requestRevisionFailure(error);
  }
}

export function* runGetUserFeeTierRate(user) {
  try {
    const API_URI = APP.API_URL;
    const { data } = user;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const feeRate = yield call(
      request,
      `${API_URI}/fee_tier/?user_id=${data}`,
      optionsFormData('GET', null, token),
    );
    if (feeRate.results.length > 0) {
      yield put(getUserFeeTierRateSuccess(feeRate.results));
    } else {
      yield put(getUserFeeTierRateError(feeRate));
    }
  } catch (error) {
    yield put(getUserFeeTierRateError(error));
  }
}

export function* runRevertTransaction(data) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const requestData = data.data;
    const requestId = requestData.id;

    let requestStatus = 'REQUESTED';
    if (requestData.status === 'COMPLETED') {
      requestStatus = 'SCHEDULED';
    } else if (requestData.status === 'SCHEDULED') {
      requestStatus = 'PROCESSING';
    } else if (
      requestData.status === 'PROCESSING' ||
      requestData.status === 'CANCELLED'
    ) {
      requestStatus = 'REQUESTED';
    }
    const formData = new FormData();
    formData.append('status', requestStatus);
    formData.append('pickup_at', data.data.pickup_at);
    formData.append('due_at', data.data.due_at);
    formData.append('remarks', data.data.remarks);
    const endPoint = `${API_URI}/revert_request/${requestId}/`;
    const processingRequest = yield call(
      request,
      endPoint,
      optionsFormData('PUT', formData, token),
    );
    if (processingRequest.success) {
      yield put(revertTransactionSuccess(processingRequest.data));
      yield put(transactionData(requestId));
      yield put(requestRevision(requestId));
    } else {
      yield put(revertTransactionError(processingRequest.message));
    }
  } catch (error) {
    yield put(revertTransactionError(error));
  }
}

export function* resendPayeeNotif(notif) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const requestData = notif.data;
    const { user } = requestData;
    if (
      requestData.payee_agreement &&
      requestData.payee_agreement.is_accepted === false
    ) {
      const payeeNotif = new FormData();
      payeeNotif.append('email', requestData.payee.email);
      payeeNotif.append('payee_id', requestData.payee.id);
      payeeNotif.append('user_id', user.id);
      payeeNotif.append('member_name', `${user.business_name}`);
      payeeNotif.append('payee_name', requestData.payee.name);
      yield call(
        request,
        `${API_URI}/payee_notification/`,
        optionsFormData('POST', payeeNotif, token),
      );
      yield put(resendPayeeNotifSuccess('Payee agreement sent.'));
      const emailSentTimeOut = moment()
        .add(2, 'minutes')
        .format(APP.DATE_TIME_FORMAT);
      sessionStorage.setItem('emailSentTimeOut', emailSentTimeOut);
    }
  } catch (error) {
    yield put(resendPayeeNotifError(error));
  }
}

export function* getUserPayeeAgreement(userPayee) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const { payeeId, userId } = userPayee.data;
    const payeeUserAgreement = yield call(
      request,
      `${API_URI}/payee_user_agreement/?user_id=${userId}&payee_id=${payeeId}`,
      optionsFormData('GET', null, token),
    );
    if (payeeUserAgreement.success) {
      yield put(getUserPayeeAgreementSuccess(payeeUserAgreement.data[0]));
    } else {
      yield put(getUserPayeeAgreementError('No Agreement'));
    }
  } catch (error) {
    yield getUserPayeeAgreementError(error);
  }
}

export function* removeFile(data) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const { fileId, requestId, backupRemarks } = data.data;
    yield call(
      request,
      `${API_URI}/file_request/${fileId}/?remarks=${backupRemarks}`,
      optionsFormData('DELETE', null, token),
    );
    yield put(removeFileSuccess([]));
    yield put(getBackup(requestId));
    yield put(getDeletedBackup(requestId));
  } catch (error) {
    yield put(removeFileError(error));
  }
}

export function* updateRequest(requestData) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const paymentFormData = new FormData();
    const {
      pickupAt,
      payAt,
      requestType,
      amount,
      feeObject,
      requestId,
      transactionType,
      isChecked,
      currentUser,
    } = requestData.data;
    const requestActualData = requestData.data.request;
    const actualAmount = amount.replace(/,/g, '').replace('$', '');
    console.log('request actual data', requestActualData);
    const fee =
      requestActualData.type === 'DEPOSIT'
        ? actualAmount * feeObject.deposit_flat_rate
        : actualAmount * feeObject.payment_flat_rate;
    paymentFormData.append('type', requestType);
    paymentFormData.append('to', 'Anywhere');
    paymentFormData.append('status', requestActualData.status);
    paymentFormData.append('update_status', 'UPDATE');
    paymentFormData.append('amount', actualAmount);
    paymentFormData.append('pickup_at', `${pickupAt} 00:00`);
    paymentFormData.append('due_at', `${payAt} 00:00`);
    paymentFormData.append('transaction_type', transactionType);
    if (isChecked) {
      paymentFormData.append(
        'waived_at',
        moment().format(APP.DATE_TIME_FORMAT),
      );
      paymentFormData.append('fee', 0);
      if (requestActualData.fee > 0) {
        paymentFormData.append(
          'remarks',
          `${currentUser.first_name} ${currentUser.username} waived the fee`,
        );
      } else {
        paymentFormData.append('remarks', requestActualData.remarks);
      }
    } else if (transactionType === 'ELECTRONIC') {
      paymentFormData.append('fee', 0);
      paymentFormData.append('remarks', requestActualData.remarks);
    } else {
      paymentFormData.append('fee', fee);
      paymentFormData.append('remarks', requestActualData.remarks);
    }

    const requestResponse = yield call(
      request,
      `${API_URI}/request/${requestId}/`,
      optionsFormData('PUT', paymentFormData, token),
    );
    yield put(updateRequestSuccess(requestResponse));
    yield put(getRequest(requestId));
    yield put(transactionData(requestId));
  } catch (error) {
    yield put(updateRequestError(error));
  }
}

export function* getTimeWatcher() {
  yield takeLatest(GET_TIME, getTime);
}

export function* requestWatcher() {
  yield takeLatest(GET_REQUEST, getRequestData);
}

export function* backupWatcher() {
  yield takeLatest(GET_BACKUP, runGetBackup);
}

export function* cancelPaymentWatcher() {
  yield takeLatest(CANCEL_PAYMENT, cancelPayment);
}

export function* modifyPaymentDateWatcher() {
  yield takeLatest(MODIFY_PAYMENT_DATE, modifyPaymentDate);
}

export function* modifyPickupDateWatcher() {
  yield takeLatest(MODIFY_PICKUP_DATE, modifyPickupDate);
}

export function* transactionDataWatcher() {
  yield takeLatest(TRANSACTION_DATA, transactionDataRequest);
}

export function* adminActionWatcher() {
  yield takeLatest(ADMIN_ACTION, adminAction);
}

export function* requestRevisionWatcher() {
  yield takeLatest(REQUEST_REVISION, requestRevisionData);
}

export function* getUserFeeTierRateWatcher() {
  yield takeLatest(GET_USER_FEE_TIER_RATE, runGetUserFeeTierRate);
}

export function* getUserFeeRateWatcher() {
  yield takeLatest(GET_FEE_RATE, getUserFeeRate);
}

export function* revertTransactionWatcher() {
  yield takeLatest(REVERT_TRANSACTION, runRevertTransaction);
}

export function* resendPayeeNotifWatcher() {
  yield takeLatest(RESEND_PAYEE_NOTIF, resendPayeeNotif);
}

export function* getUserPayeeAgreementWatcher() {
  yield takeLatest(GET_USER_PAYEE_AGREEMENT, getUserPayeeAgreement);
}

export function* fileUploadWatcher() {
  yield takeLatest(FILE_UPLOAD, fileUpload);
}

export function* removeFileWatcher() {
  yield takeLatest(REMOVE_FILE, removeFile);
}

export function* updateRequestWatcher() {
  yield takeLatest(UPDATE_REQUEST, updateRequest);
}

export function* getDeletedBackupWatcher() {
  yield takeLatest(GET_DELETED_BACKUP, runGetDeletedBackup);
}

export default function* rootSaga() {
  yield all([
    requestWatcher(),
    backupWatcher(),
    getTimeWatcher(),
    cancelPaymentWatcher(),
    modifyPaymentDateWatcher(),
    modifyPickupDateWatcher(),
    transactionDataWatcher(),
    adminActionWatcher(),
    requestRevisionWatcher(),
    getUserFeeTierRateWatcher(),
    getUserFeeRateWatcher(),
    revertTransactionWatcher(),
    resendPayeeNotifWatcher(),
    getUserPayeeAgreementWatcher(),
    fileUploadWatcher(),
    removeFileWatcher(),
    updateRequestWatcher(),
    getDeletedBackupWatcher(),
  ]);
}
