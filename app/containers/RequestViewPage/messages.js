/*
 * RequestViewPage Messages
 *
 * This contains all the text for the RequestViewPage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.RequestViewPage.header',
    defaultMessage: 'This is RequestViewPage container !',
  },
  title: {
    id: 'app.containers.RequestViewPage.title',
    defaultMessage: 'Request Details',
  },
  payment: {
    id: 'app.containers.RequestViewPage.payment',
    defaultMessage: 'Payment',
  },
  deposit: {
    id: 'app.containers.RequestViewPage.deposit',
    defaultMessage: 'Deposit',
  },
  details: {
    id: 'app.containers.RequestViewPage.details',
    defaultMessage: 'Detail',
  },
  status: {
    id: 'app.containers.RequestViewPage.status',
    defaultMessage: 'Status',
  },
  transactionHistory: {
    id: 'app.containers.RequestViewPage.transactionHistory',
    defaultMessage: 'Transaction History',
  },
  changeHistory: {
    id: 'app.containers.RequestViewPage.changeHistory',
    defaultMessage: 'Change Log',
  },
});
