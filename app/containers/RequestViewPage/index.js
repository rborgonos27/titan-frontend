/**
 *
 * RequestViewPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import ContainerWrapper from 'components/ContainerWrapper';
import StandardModal from 'components/StandardModal';
import Dropzone from 'react-dropzone';
import { Grid, Loader, Divider, Button, Dimmer } from 'semantic-ui-react';
import moment from 'moment';
import 'react-day-picker/lib/style.css';
import closeImage from 'images/close.png';
import { APP } from 'config';
import { getDateForward, getMaxPickupDate } from 'containers/SendPayment/dates';
import { SegmentLoader, RequiredText } from 'components/Utils';
import { formatMoney } from 'utils/formatMoney';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import pdfImage from 'images/pdf.png';
import fileIcon from 'images/file-icon.png';
import makeSelectRequestViewPage, {
  makeSelectCurrentUser,
  makeSelectFeeTierObject,
} from './selectors';
import {
  getRequest,
  getBackup,
  getTime,
  modifyPaymentDate,
  modifyPickupDate,
  cancelPayment,
  transactionData,
  adminAction,
  requestRevision,
  getUserFeeTierRate,
  getFeeRate,
  revertTransaction,
  resendPayeeNotif,
  activateNotif,
  getUserPayeeAgreement,
  fileUpload,
  removeFile,
  updateRequest,
  getDeletedBackup,
} from './actions';
import reducer from './reducer';
import saga from './saga';
import {
  PayeeDetailsModal,
  CancelPaymentModal,
  EditModal,
  DepositView,
  PaymentView,
  ChangeDateModal,
  DateWarningModal,
  RemarksModal,
  ConfirmDeleteModal,
} from './Components';

/* eslint-disable react/prefer-stateless-function */
export class RequestViewPage extends React.Component {
  componentDidMount() {
    const { currentUser } = this.props;
    const { match } = this.props;
    this.props.getRequest(match.params.id);
    this.props.getBackup(match.params.id);
    this.props.getDeletedBackup(match.params.id);
    this.props.getTime();
    this.props.transactionData(match.params.id);
    this.props.requestRevision(match.params.id);
    this.props.getFeeRate(currentUser.id);
  }
  /* eslint-disable */
  state = {
    openDetails: false,
    openPaymentDate: false,
    openPickUpDate: false,
    openBackup: false,
    openCancelPayment: false,
    openConfirmDate: false,
    payAt: '',
    pickupAt: '',
    serverTime: '',
    isOpenRemarks: false,
    transaction: '',
    remarks: '',
    amount: '',
    editRequestModal: false,
    transactionType: '',
    requestType: '',
    request: null,
    requestId: null,
    uploadedFiles: [],
    feeObject: null,
    belowDateWarning: false,
    removeFileObject: null,
    confirmDeleteModal: false,
    isBackupAltered: false,
    isChecked: false,
    backupRemarks: '',
  };
  /* eslint-enable */
  componentWillReceiveProps(nextProps) {
    const {
      currentTime,
      request,
      files,
      feeObject,
    } = nextProps.requestviewpage;
    this.setState({
      serverTime: currentTime,
    });
    if (request) {
      /* eslint-disable */
      this.setState({
        request,
        uploadedFiles: files,
        amount: request.amount,
        pickupAt: moment(request.pickup_at, APP.DATE_TIME_FORMAT).format(
          APP.DATE_FORMAT,
        ),
        payAt: moment(request.due_at, APP.DATE_TIME_FORMAT).format(
          APP.DATE_FORMAT,
        ),
        requestType: request.type,
        requestId: request.id,
        transactionType: request.transaction_type,
        isChecked: request.waived_at != null,
        openPaymentDate: false,
        openPickUpDate: false,
        confirmDeleteModal: false,
        feeObject,
      });
      /* eslint-enable */
    }
  }
  closeModal = modal => this.setState({ [modal]: false });
  handleInput = e => this.setState({ [e.target.name]: e.target.value });
  disableUpDownArrow = e => {
    /* eslint-disable */
    const restrictedKeys = [
      16,17,18,19,20,33,34,36,
      38,40,45,65,66,67,68,69,
      70,71,72,73,74,75,76,77,
      78,79,80,81,82,83,84,85,
      86,87,88,89,90,91,92,93,
      94,95,106,107,109,111,186,
      187,189,191,192,219,220,221,222,
    ];
    if (restrictedKeys.indexOf(e.keyCode) > -1) {
      e.preventDefault();
    }
  };
  openInNewTab = (e, url) => {
    e.preventDefault();
    window.open(url);
  };
  handleInputAmount = e => {
    const { amount } = this.state;
    let formattedAmount = formatMoney(amount);
    if (amount > 9999999) {
      alert("Can't accept deposit more than 10,000,000.00");
      formattedAmount = formatMoney(0);
    }
    this.setState({ [e.target.name]: `$ ${formattedAmount}` });
  };
  handleChangeDropDown = (e, { value }) => {
    this.setState({ transactionType: value });
  };
  updateRequest = () => {
    const { currentUser } = this.props;
    const data = this.state;
    data.currentUser = currentUser
    this.props.updateRequest(data);
    this.setState({ editRequestModal: false })
  };
  checkIfBelowDate = date => {
    const { serverTime } = this.state;
    const serverTimeMoment = moment(serverTime).format(APP.DATE_FORMAT);
    const selectedDate = moment(date).format(APP.DATE_FORMAT);
    return serverTimeMoment > selectedDate;
  };
  setPageState = object => this.setState(object);
  submitData = () => {
    const { remarks, transaction } = this.state;
    const { request, feeTierObject, feeObject } = this.props.requestviewpage;
    request.remarks = remarks;
    request.feeTierObject = feeTierObject;
    if (feeObject) {
      request.feeObject = feeObject;
    }
    if (transaction === 'process') {
      this.props.adminAction(request);
    } else if (transaction === 'revert') {
      this.props.revertTransaction(request);
    }
    this.setState({ remarks: '' });
    this.closeModal('isOpenRemarks');
  };
  handleCancelPayment = () => {
    const { request } = this.props.requestviewpage;
    const { currentUser } = this.props;
    const data = { request, currentUser };
    this.props.cancelPayment(data);
    this.closeModal('openCancelPayment');
  };
  openInNewTab = (e, url) => {
    e.preventDefault();
    window.open(url);
  };
  onImageDrop = files => {
    if (files.length > 0) {
      const { uploadedFiles, requestId, requestType } = this.state;
      const data = {
        files,
        requestId,
        requestType,
      };
      this.props.fileUpload(data);
      this.setState({
        isBackupAltered: true,
        uploadedFiles: [...uploadedFiles, ...files],
      });
    }
  };
  buildModalBackup = () => {
    const { openBackup,isBackupAltered } = this.state;
    const { requestviewpage, currentUser } = this.props;
    const {
      backup,
      files,
      filesLoading,
      request,
      deletedBackup } = requestviewpage;
    if (!request) return null;
    const titleStyle = {
      fontSize: '1em',
      textAlign: 'center',
      color: '#fc5109',
    };
    const previewStyle = {
      display: 'inline',
      width: 70,
      height: 70,
      padding: 5,
    };
    const previewSpan = {
      display: 'inline',
    };
    const closeButton = {
      position: 'absolute',
      width: '30px',
      height: '30px',
      objectFit: 'cover',
    };
    const buttonCaption = isBackupAltered ? 'CONFIRM' : 'CLOSE';
    return (
      <StandardModal
        headerText="Backup"
        open={openBackup}
        onClose={() => this.closeModal('openBackup')}
        onActionText={buttonCaption}
        onContinue={() => {
          this.setState({ isBackupAltered: !isBackupAltered });
          this.closeModal('openBackup');
        }}
      >
        <div>
          {/* eslint-disable */
            backup.length > 0 ?
              backup.map((file, key) => {
                const fileNameSplit = file.filename.split('/');
                const fileName = fileNameSplit[fileNameSplit.length - 1];
                return (
                  <span key={key} style={previewSpan}>
                      <Button onClick={(e) => this.openInNewTab(e, file.filename_url)}>
                        <img
                          alt="Preview"
                          key={key}
                          src={this.getFileSrc(file)}
                          style={previewStyle}
                        />
                      </Button>
                      <div>
                        <b style={{ fontSize: 13 }}>{fileName}</b>
                      </div>
                    </span>
                ) }) : (<p>No Uploaded Backup</p>)
            /* eslint-enable */}
        </div>
        <Divider />
        <div style={{ marginBottom: 20 }} />
        <div>
          {request.status !== 'COMPLETED' ? (
            <div>
              <span style={titleStyle}>Upload Backup</span>
              <div style={{ marginBottom: 20 }} />
              {filesLoading ? (
                <Dimmer active inverted>
                  <Loader size="massive">Loading</Loader>
                </Dimmer>
              ) : null}
              <Dropzone
                style={{ width: '100%' }}
                className=" col-lg-8 col-md-6 col-sm-6 col-xs-12 dropZone"
                onDrop={this.onImageDrop}
                multiple
                accept="image/*,application/pdf,
                          application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel,
                          application/doc,
                          application/ms-doc,
                          application/msword,
                          application/vnd.openxmlformats-officedocument.wordprocessingml.document,
                          application/vnd.ms-excel,
                          .csv
                          "
              >
                <p>Drop your files or click here to upload</p>
                {/* eslint-disable */
                  files.length > 0 && (
                    <div>
                      {files.map((file, key) => {
                        const { request } = this.props.requestviewpage;
                        if (!request) return null;
                        return (
                          <span key={key} style={previewSpan}>
                          {request.status === 'COMPLETED' ? null : (
                            <a onClick={(e) => {
                              const fileId = file.id;
                              const { requestId } = this.state;
                              e.preventDefault();
                              e.stopPropagation();
                              const removeFileObject = { fileId, requestId };
                              this.setState({ removeFileObject, confirmDeleteModal: true });
                            }
                            }>
                              <img
                                alt=""
                                style={closeButton}
                                key={key}
                                src={closeImage}
                              />
                            </a>
                          )}
                            <img
                              alt="Preview"
                              key={key}
                              src={this.getFileSrc(file)}
                              style={previewStyle}
                            />
                          <b style={{fontSize: 10}}>{file.name}</b>
                        </span>
                        );
                      })
                      }
                    </div>
                  )
                  /* eslint-enable */
                }
              </Dropzone>
            </div>
          ) : null}
          {deletedBackup.length > 0 ? (
            <div>
              <br />
              <br />
              <br />
              {currentUser.user_type === 'super_admin'
                ? this.getDeletedBackupView()
                : null}
              <br />
              <br />
            </div>
          ) : null}
        </div>
      </StandardModal>
    );
  };
  getDeletedBackupView = () => {
    const { requestviewpage } = this.props;
    const { deletedBackup } = requestviewpage;
    const titleStyle = {
      fontSize: '1em',
      textAlign: 'center',
      color: '#fc5109',
    };
    const previewStyle = {
      display: 'inline',
      width: 70,
      height: 70,
      padding: 5,
    };
    const previewSpan = {
      display: 'inline',
    };
    return (
      <div>
        <div style={{ marginBottom: 60 }} />
        <span style={titleStyle}>Deleted Backup</span>
        <Divider />
        {/* eslint-disable */
          deletedBackup.length > 0 ?
            deletedBackup.map((file, key) => {
              const fileNameSplit = file.filename.split('/');
              const fileName = fileNameSplit[fileNameSplit.length - 1];
              return (
                <span key={key} style={previewSpan}>
                      <Button onClick={(e) => this.openInNewTab(e, file.filename_url)}>
                        <img
                          alt="Preview"
                          key={key}
                          src={this.getFileSrc(file)}
                          style={previewStyle}
                        />
                      </Button>
                      <div>
                        <b style={{ fontSize: 13 }}>{fileName}</b>
                      </div>
                      <div>
                        {!file.remarks || file.remarks === '' ? null : <RequiredText>Reason: <b style={{ fontSize: 13 }}>{file.remarks}</b></RequiredText>}
                      </div>
                    </span>
              ) }) : (<p>No Deleted Backup</p>)
          /* eslint-enable */}
        <div style={{ marginBottom: 20 }} />
      </div>
    );
  };
  handleUpdatePickupAtValues = () => {
    const { serverTime } = this.state;
    const { request } = this.props.requestviewpage;
    const currentDate = moment(serverTime).format(APP.DATE_FORMAT);

    const payAt = moment(request.due_at).format(APP.DATE_FORMAT);
    const maxDateBasis = this.state.payAt !== '' ? this.state.payAt : payAt;
    const maxPickupDate = getMaxPickupDate(maxDateBasis, 3);

    // noOfWorkingDays
    const minPickupDate = getDateForward(2, currentDate);
    return { maxPickupDate, minPickupDate };
  };
  handleDayChangePayAt = (selectedDay, modifiers, dayPickerInput) => {
    const state = 'payAt';
    this.handleSetDateState(selectedDay, modifiers, dayPickerInput, state);
  };
  handleDayChangePickUpAt = (selectedDay, modifiers, dayPickerInput) => {
    const state = 'pickupAt';
    this.handleSetDateState(selectedDay, modifiers, dayPickerInput, state);
  };
  handleSetDateState = (selectedDay, modifiers, dayPickerInput, state) => {
    const selectedDateFromMoment = moment(selectedDay).format(APP.DATE_FORMAT);
    // const belowDateWarning = this.checkIfBelowDate(selectedDay); // disable checking of below date
    /* eslint-disable */
    this.setState({
      [state]: selectedDateFromMoment,
      // belowDateWarning, skip checking of below date
    });
    /* eslint-enable */
  };
  handleModifyPaymentDate = () => {
    const { openPaymentDate } = this.state;
    this.setState({
      openPaymentDate: !openPaymentDate,
      payAt: '',
      pickupAt: '',
    });
  };
  handleModifyPaymentDateSubmit = () => {
    const { request } = this.props.requestviewpage;
    const { payAt, pickupAt, openPaymentDate, openPickUpDate } = this.state;
    const { currentUser } = this.props;
    const data = { request, payAt, pickupAt, currentUser };
    if (openPaymentDate) {
      this.props.modifyPaymentDate(data);
    } else if (openPickUpDate) {
      this.props.modifyPickupDate(data);
    }
    /* eslint-disable */
    this.setState({ openConfirmDate: false });
    /* eslint-enable */
  };
  renderUpdateCancelButton = () => {
    let disableButton = true;
    const { payAt, pickupAt, openPaymentDate, openPickUpDate } = this.state;
    if (openPaymentDate && !openPickUpDate) {
      if (payAt !== '') {
        disableButton = false;
      }
    } else if (openPickUpDate && !openPaymentDate) {
      if (pickupAt !== '') {
        disableButton = false;
      }
    }
    return (
      <div>
        {/* eslint-disable */}
        <Button
          onClick={() => {
            this.setState({
              openPaymentDate: false,
              openPickUpDate: false,
            });
          }}
          negative
          size="tiny"
          icon="warning"
          labelPosition="right"
          content="Cancel"
        />
        <Button
          onClick={() => this.setState({ openConfirmDate: true })}
          color="orange"
          size="tiny"
          icon="checkmark"
          labelPosition="right"
          disabled={disableButton}
          content="Update"
        />
        {/* eslint-enable */}
      </div>
    );
  };
  formatDate = (date, format) => moment(date).format(format);
  getStatus = status => {
    let color = '#CD1010';
    if (status === 'REQUESTED') {
      color = '#CD1010';
    } else if (status === 'PROCESSING') {
      color = '#000089';
    } else if (status === 'SCHEDULED') {
      color = '#FFD639';
    } else if (status === 'COMPLETED') {
      color = '#037903';
    }
    return <span style={{ color }}>{status}</span>;
  };
  isAllowedAction = request => {
    if (request === 'COMPLETED') {
      return false;
    } else if (request === 'CANCELLED') {
      return false;
    }
    return true;
  };
  renderInfo = () => {
    const { request } = this.props.requestviewpage;
    if (!request) return null;
    return request.type === 'PAYMENT' ? (
      <PaymentView
        pageProps={this.props}
        pageState={this.state}
        handleDayChangePickUpAt={this.handleDayChangePickUpAt}
        handleDayChangePayAt={this.handleDayChangePayAt}
        setPageState={this.setPageState}
        formatDate={this.formatDate}
        isAllowedAction={this.isAllowedAction}
        submitTransaction={this.submitTransaction}
        renderChangeLog={this.renderChanegLog}
        handleModifyPaymentDate={this.handleModifyPaymentDate}
        renderUpdateCancelButton={this.renderUpdateCancelButton}
        handleUpdatePickupAtValues={this.handleUpdatePickupAtValues}
        getStatus={this.getStatus}
      />
    ) : (
      <DepositView
        pageProps={this.props}
        pageState={this.state}
        handleDayChangePickUpAt={this.handleDayChangePickUpAt}
        handleDayChangePayAt={this.handleDayChangePayAt}
        setPageState={this.setPageState}
        formatDate={this.formatDate}
        isAllowedAction={this.isAllowedAction}
        submitTransaction={this.submitTransaction}
        renderChangeLog={this.renderChanegLog}
        handleModifyPaymentDate={this.handleModifyPaymentDate}
        renderUpdateCancelButton={this.renderUpdateCancelButton}
        handleUpdatePickupAtValues={this.handleUpdatePickupAtValues}
        getStatus={this.getStatus}
      />
    );
  };
  renderLoading = () => <SegmentLoader />;
  renderChanegLog = data => {
    /*eslint-disable*/
    const { request } = this.props.requestviewpage;
    const { currentUser } = this.props;
    if ( currentUser.user_type !== 'super_admin') return null;
    if (!request) return null;
    let revisionData = [];
    revisionData.push(
      <Grid.Row key={-1}>
        <Grid.Column>
          <h4>Previous</h4>
        </Grid.Column>
        <Grid.Column>
          <h4>Current</h4>
        </Grid.Column>
      </Grid.Row>
    );
    let ctr = 0;
    const color = '#CD1010';
    const normalColor = '#000000';
    for(let i = 0; i < data.results.length; i++) {
      ctr = ctr + 1;
      const revision = data.results[i];
      let currentData = request;
      if (data.results.length === ctr) {
        currentData = request;
      } else if (data.results.length > 1) {
        currentData = data.results[i+1];
      }
      const revisionDueAt = moment(revision.due_at, APP.DATE_TIME_FORMAT).format(
        APP.DATE_DISPLAY_FORMAT,
      );
      const revisionPickupAt = moment(revision.pickup_at, APP.DATE_TIME_FORMAT).format(
        APP.DATE_DISPLAY_FORMAT,
      );

      const revisionUpdatedAt = moment(revision.updated_at, APP.DATE_TIME_FORMAT).format(
        `${APP.DATE_DISPLAY_FORMAT} HH:mm:ss`,
      );

      const currentDueAt = moment(currentData.due_at, APP.DATE_TIME_FORMAT).format(
        APP.DATE_DISPLAY_FORMAT,
      );
      const currentPickupAt = moment(currentData.pickup_at, APP.DATE_TIME_FORMAT).format(
        APP.DATE_DISPLAY_FORMAT,
      );
      const currentUpdatedAt = moment(currentData.updated_at, APP.DATE_TIME_FORMAT).format(
        `${APP.DATE_DISPLAY_FORMAT} HH:mm:ss`,
      );

      const requestDueAt = moment(request.due_at, APP.DATE_TIME_FORMAT).format(
        APP.DATE_DISPLAY_FORMAT,
      );
      const requestPickupAt = moment(request.pickup_at, APP.DATE_TIME_FORMAT).format(
        APP.DATE_DISPLAY_FORMAT,
      );

      const statusColor = (revision.status !== currentData.status) ? color : normalColor;
      const payeeColor = (revision.payee.id !== currentData.payee.id) ? color : normalColor;
      const payAtColor = (revision.due_at !== currentData.due_at) ? color : normalColor;
      const pickupAtColor = (revision.pickup_at !== currentData.pickup_at) ? color : normalColor;
      const feeColor = (revision.fee !== currentData.fee) ? color : normalColor;
      const gridRow = (
        <Grid.Row key={i}>
          <Grid.Column>
            <b>Status:</b> <span style={{ color: statusColor }}>{revision.status}</span> <br />
            <b>Payee:</b> <span style={{color: payeeColor}}>{revision.payee.name}</span> <br />
            <b>Pay At:</b> <span style={{color: payAtColor}}>{revisionDueAt}</span> <br />
            <b>Pickup At:</b> <span style={{color: pickupAtColor}}>{revisionPickupAt}</span> <br />
            <b>Fee:</b> <span style={{color: feeColor}}>$ {formatMoney(revision.fee)}</span> <br />
            <b>Updated At:</b> <span style={{color: pickupAtColor}}>{revisionPickupAt}</span> <br />
            <b>Updated By:</b> <span style={{color: pickupAtColor}}>{`${revision.last_updated_by.first_name} ${revision.last_updated_by.last_name}`}</span> <br />
          </Grid.Column>
          <Grid.Column>
            <b>Status:</b> <span style={{ color: statusColor }}>{currentData.status}</span> <br />
            <b>Payee:</b> <span style={{color: payeeColor}}>{currentData.payee.name}</span> <br />
            <b>Pay At:</b> <span style={{color: payAtColor}}>{currentDueAt}</span> <br />
            <b>Pickup At:</b> <span style={{color: pickupAtColor}}>{currentPickupAt}</span> <br />
            <b>Fee</b> <span style={{color: feeColor }}>$ {formatMoney(currentData.fee)}</span> <br />
            <b>Updated At:</b> <span style={{color: pickupAtColor}}>{currentUpdatedAt}</span> <br />
            <b>Updated By:</b> <span style={{color: pickupAtColor}}>{`${currentData.last_updated_by.first_name} ${currentData.last_updated_by.last_name}`}</span> <br />
          </Grid.Column>
        </Grid.Row>
      );
      revisionData.push(gridRow);
    }
    return <Grid columns={2}>{revisionData}</Grid>
    /* eslint-enable */
  };
  /* eslint-disable */
  submitTransaction = transaction =>
    this.setState({ transaction, isOpenRemarks: true });
  getFileSrc = file => {
    let src = null;
    if (file.mime_type === 'image/jpeg' || file.mime_type === 'image/png') {
      src = file.filename_url;
    } else if (file.mime_type === 'application/pdf') {
      src = pdfImage;
    } else {
      src = fileIcon;
    }
    return src;
  };
  /* eslint-enable */
  render() {
    const { loading } = this.props.requestviewpage;
    const renderView = loading ? this.renderLoading() : this.renderInfo();
    return (
      <ContainerWrapper>
        <PayeeDetailsModal
          pageState={this.state}
          pageProps={this.props}
          closeModal={this.closeModal}
        />
        <CancelPaymentModal
          pageState={this.state}
          pageProps={this.props}
          closeModal={this.closeModal}
          handleCancelPayment={this.handleCancelPayment}
        />
        <EditModal
          pageProps={this.props}
          pageState={this.state}
          handleInput={this.handleInput}
          handleInputAmount={this.handleInputAmount}
          handleChangeDropDown={this.handleChangeDropDown}
          disableUpDownArrow={this.disableUpDownArrow}
          onImageDrop={this.onImageDrop}
          getFileSrc={this.getFileSrc}
          handleDayChangePickUpAt={this.handleDayChangePickUpAt}
          handleDayChangePayAt={this.handleDayChangePayAt}
          updateRequest={this.updateRequest}
          setPageState={this.setPageState}
          formatDate={this.formatDate}
        />
        <ChangeDateModal
          pageState={this.state}
          handleModifyPaymentDateSubmit={this.handleModifyPaymentDateSubmit}
          closeModal={this.closeModal}
        />
        <DateWarningModal
          pageState={this.state}
          setPageState={this.setPageState}
          closeModal={this.closeModal}
        />
        <RemarksModal
          pageState={this.state}
          handleInput={this.handleInput}
          closeModal={this.closeModal}
          submitData={this.submitData}
        />
        <ConfirmDeleteModal
          pageState={this.state}
          setPageState={this.setPageState}
          pageProps={this.props}
          handleInput={this.handleInput}
        />
        {this.buildModalBackup()}
        <div className="row">
          <div className="col-lg-2 col-md-3 col-sm-3" />
          <div className="col-responsive col-lg-8 col-md-6 col-sm-6 col-xs-12">
            <div className="card-box service-mode m-t-30 m-b-30 p-l-r-30 content-box">
              {renderView}
            </div>
          </div>
        </div>
      </ContainerWrapper>
    );
  }
}

RequestViewPage.propTypes = {
  currentUser: PropTypes.any,
  feeTierObject: PropTypes.any,
  getRequest: PropTypes.func.isRequired,
  getTime: PropTypes.func.isRequired,
  match: PropTypes.any,
  requestviewpage: PropTypes.any,
  getBackup: PropTypes.func.isRequired,
  modifyPaymentDate: PropTypes.any,
  modifyPickupDate: PropTypes.func.isRequired,
  cancelPayment: PropTypes.func.isRequired,
  transactionData: PropTypes.func.isRequired,
  adminAction: PropTypes.func.isRequired,
  requestRevision: PropTypes.func.isRequired,
  getFeeRate: PropTypes.func.isRequired,
  revertTransaction: PropTypes.func.isRequired,
  fileUpload: PropTypes.func.isRequired,
  updateRequest: PropTypes.func.isRequired,
  getDeletedBackup: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  requestviewpage: makeSelectRequestViewPage(),
  currentUser: makeSelectCurrentUser(),
  feeTierObject: makeSelectFeeTierObject(),
});

const mapDispatchToProps = {
  getRequest,
  getBackup,
  getTime,
  modifyPaymentDate,
  modifyPickupDate,
  cancelPayment,
  transactionData,
  adminAction,
  requestRevision,
  getUserFeeTierRate,
  getFeeRate,
  revertTransaction,
  resendPayeeNotif,
  activateNotif,
  getUserPayeeAgreement,
  fileUpload,
  removeFile,
  updateRequest,
  getDeletedBackup,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'requestViewPage', reducer });
const withSaga = injectSaga({ key: 'requestViewPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(RequestViewPage);
