/*
 *
 * RequestViewPage actions
 *
 */

import {
  GET_REQUEST,
  GET_REQUEST_SUCCESS,
  GET_REQUEST_ERROR,
  GET_BACKUP,
  GET_BACKUP_SUCCESS,
  GET_BACKUP_ERROR,
  GET_DELETED_BACKUP,
  GET_DELETED_BACKUP_SUCCESS,
  GET_DELETED_BACKUP_ERROR,
  GET_TIME,
  GET_TIME_SUCCESS,
  GET_TIME_ERROR,
  MODIFY_PAYMENT_DATE,
  MODIFY_PAYMENT_DATE_SUCCESS,
  MODIFY_PAYMENT_DATE_ERROR,
  MODIFY_PICKUP_DATE,
  MODIFY_PICKUP_DATE_SUCCESS,
  MODIFY_PICKUP_DATE_ERROR,
  CANCEL_PAYMENT,
  CANCEL_PAYMENT_SUCCESS,
  CANCEL_PAYMENT_ERROR,
  TRANSACTION_DATA,
  TRANSACTION_DATA_SUCCESS,
  TRANSACTION_DATA_FAILURE,
  ADMIN_ACTION,
  ADMIN_ACTION_SUCCESS,
  ADMIN_ACTION_FAILURE,
  REQUEST_REVISION,
  REQUEST_REVISION_SUCCESS,
  REQUEST_REVISION_ERROR,
  GET_USER_FEE_TIER_RATE,
  GET_USER_FEE_TIER_RATE_SUCCESS,
  GET_USER_FEE_TIER_RATE_ERROR,
  GET_FEE_RATE,
  GET_FEE_RATE_SUCCESS,
  GET_FEE_RATE_ERROR,
  REVERT_TRANSACTION,
  REVERT_TRANSACTION_SUCCESS,
  REVERT_TRANSACTION_ERROR,
  RESEND_PAYEE_NOTIF,
  RESEND_PAYEE_NOTIF_SUCCESS,
  RESEND_PAYEE_NOTIF_ERROR,
  ACTIVATE_NOTIF,
  GET_USER_PAYEE_AGREEMENT,
  GET_USER_PAYEE_AGREEMENT_ERROR,
  GET_USER_PAYEE_AGREEMENT_SUCCESS,
  FILE_UPLOAD,
  FILE_UPLOAD_ERROR,
  FILE_UPLOAD_SUCCESS,
  REMOVE_FILE,
  REMOVE_FILE_ERROR,
  REMOVE_FILE_SUCCESS,
  UPDATE_REQUEST,
  UPDATE_REQUEST_ERROR,
  UPDATE_REQUEST_SUCCESS,
} from './constants';

export function getRequest(data) {
  return {
    type: GET_REQUEST,
    data,
  };
}

export function getRequestSuccess(data) {
  return {
    type: GET_REQUEST_SUCCESS,
    data,
  };
}

export function getRequestError(data) {
  return {
    type: GET_REQUEST_ERROR,
    data,
  };
}

export function getBackup(data) {
  return {
    type: GET_BACKUP,
    data,
  };
}

export function getBackupSuccess(data) {
  return {
    type: GET_BACKUP_SUCCESS,
    data,
  };
}

export function getBackupError(data) {
  return {
    type: GET_BACKUP_ERROR,
    data,
  };
}

export function getDeletedBackup(data) {
  return {
    type: GET_DELETED_BACKUP,
    data,
  };
}

export function getDeletedBackupSuccess(data) {
  return {
    type: GET_DELETED_BACKUP_SUCCESS,
    data,
  };
}

export function getDeletedBackupError(data) {
  return {
    type: GET_DELETED_BACKUP_ERROR,
    data,
  };
}

export function getTime() {
  return {
    type: GET_TIME,
  };
}

export function getTimeSuccess(data) {
  return {
    type: GET_TIME_SUCCESS,
    data,
  };
}

export function getTimeError(data) {
  return {
    type: GET_TIME_ERROR,
    data,
  };
}

export function modifyPaymentDate(data) {
  return {
    type: MODIFY_PAYMENT_DATE,
    data,
  };
}

export function modifyPaymentDateSuccess(data) {
  return {
    type: MODIFY_PAYMENT_DATE_SUCCESS,
    data,
  };
}

export function modifyPaymentDateError(data) {
  return {
    type: MODIFY_PAYMENT_DATE_ERROR,
    data,
  };
}

export function modifyPickupDate(data) {
  return {
    type: MODIFY_PICKUP_DATE,
    data,
  };
}

export function modifyPickupDateSuccess(data) {
  return {
    type: MODIFY_PICKUP_DATE_SUCCESS,
    data,
  };
}

export function modifyPickupDateError(data) {
  return {
    type: MODIFY_PICKUP_DATE_ERROR,
    data,
  };
}

export function cancelPayment(data) {
  return {
    type: CANCEL_PAYMENT,
    data,
  };
}

export function cancelPaymentSuccess(data) {
  return {
    type: CANCEL_PAYMENT_SUCCESS,
    data,
  };
}

export function cancelPaymentFailure(data) {
  return {
    type: CANCEL_PAYMENT_ERROR,
    data,
  };
}

export function transactionData(data) {
  return {
    type: TRANSACTION_DATA,
    data,
  };
}

export function transactionDataSuccess(data) {
  return {
    type: TRANSACTION_DATA_SUCCESS,
    data,
  };
}

export function transactionDataFailure(data) {
  return {
    type: TRANSACTION_DATA_FAILURE,
    data,
  };
}

export function adminAction(data) {
  return {
    type: ADMIN_ACTION,
    data,
  };
}

export function adminActionSuccess(data) {
  return {
    type: ADMIN_ACTION_SUCCESS,
    data,
  };
}

export function adminActionFailure(data) {
  return {
    type: ADMIN_ACTION_FAILURE,
    data,
  };
}

export function requestRevision(data) {
  return {
    type: REQUEST_REVISION,
    data,
  };
}

export function requestRevisionSuccess(data) {
  return {
    type: REQUEST_REVISION_SUCCESS,
    data,
  };
}

export function requestRevisionFailure(data) {
  return {
    type: REQUEST_REVISION_ERROR,
    data,
  };
}

export function getUserFeeTierRate(data) {
  return {
    type: GET_USER_FEE_TIER_RATE,
    data,
  };
}

export function getUserFeeTierRateSuccess(data) {
  return {
    type: GET_USER_FEE_TIER_RATE_SUCCESS,
    data,
  };
}

export function getUserFeeTierRateError(error) {
  return {
    type: GET_USER_FEE_TIER_RATE_ERROR,
    error,
  };
}

export function getFeeRate(data) {
  return {
    type: GET_FEE_RATE,
    data,
  };
}

export function getFeeRateSuccess(data) {
  return {
    type: GET_FEE_RATE_SUCCESS,
    data,
  };
}

export function getFeeRateError(error) {
  return {
    type: GET_FEE_RATE_ERROR,
    error,
  };
}

export function revertTransaction(data) {
  return {
    type: REVERT_TRANSACTION,
    data,
  };
}

export function revertTransactionSuccess(data) {
  return {
    type: REVERT_TRANSACTION_SUCCESS,
    data,
  };
}

export function revertTransactionError(error) {
  return {
    type: REVERT_TRANSACTION_ERROR,
    error,
  };
}

export function resendPayeeNotif(data) {
  return {
    type: RESEND_PAYEE_NOTIF,
    data,
  };
}

export function resendPayeeNotifSuccess(data) {
  return {
    type: RESEND_PAYEE_NOTIF_SUCCESS,
    data,
  };
}

export function resendPayeeNotifError(error) {
  return {
    type: RESEND_PAYEE_NOTIF_ERROR,
    error,
  };
}
export function activateNotif() {
  return {
    type: ACTIVATE_NOTIF,
  };
}

export function getUserPayeeAgreement(data) {
  return {
    type: GET_USER_PAYEE_AGREEMENT,
    data,
  };
}

export function getUserPayeeAgreementSuccess(data) {
  return {
    type: GET_USER_PAYEE_AGREEMENT_SUCCESS,
    data,
  };
}

export function getUserPayeeAgreementError(error) {
  return {
    type: GET_USER_PAYEE_AGREEMENT_ERROR,
    error,
  };
}

export function fileUpload(data) {
  return {
    type: FILE_UPLOAD,
    data,
  };
}

export function fileUploadSuccess(data) {
  return {
    type: FILE_UPLOAD_SUCCESS,
    data,
  };
}

export function fileUploadError(data) {
  return {
    type: FILE_UPLOAD_ERROR,
    data,
  };
}

export function removeFile(data) {
  return {
    type: REMOVE_FILE,
    data,
  };
}

export function removeFileSuccess(data) {
  return {
    type: REMOVE_FILE_SUCCESS,
    data,
  };
}

export function removeFileError(error) {
  return {
    type: REMOVE_FILE_ERROR,
    error,
  };
}

export function updateRequest(data) {
  return {
    type: UPDATE_REQUEST,
    data,
  };
}

export function updateRequestSuccess(data) {
  return {
    type: UPDATE_REQUEST_SUCCESS,
    data,
  };
}

export function updateRequestError(error) {
  return {
    type: UPDATE_REQUEST_ERROR,
    error,
  };
}
