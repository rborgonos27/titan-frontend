import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import DataTable from 'components/DataTable';
import { Divider, Button } from 'semantic-ui-react';
import { SegmentLoader } from 'components/Utils';
import moment from 'moment';
import 'react-day-picker/lib/style.css';
import messages from '../messages';

const AdminDetails = ({
  pageProps,
  submitTransaction,
  setPageState,
  isAllowedAction,
  renderChangeLog,
}) => {
  const { requestviewpage, currentUser } = pageProps;
  const {
    tableData,
    tableLoading,
    revisionData,
    revisionLoading,
    request,
    isNotifTimeout,
  } = requestviewpage;
  const disableButton =
    request.status === 'PROCESSING' &&
    request.type === 'PAYMENT' &&
    request.payee_agreement &&
    request.payee_agreement.is_accepted === false;
  const fields = [
    { name: 'type', display: 'Transaction Type', type: 'string' },
    { name: 'status', display: 'Status', type: 'string' },
    { name: 'remarks', display: 'Note', type: 'string' },
    { name: 'process_at', display: 'Timestamp', type: 'date' },
  ];
  const data = { fields, tableData };
  let buttonText = 'PROCESS';
  if (request.status === 'REQUESTED') {
    buttonText = 'PROCESS';
  } else if (request.status === 'PROCESSING') {
    buttonText =
      request.transaction_type === 'ELECTRONIC' ? 'COMPLETE' : 'SCHEDULE';
  } else if (request.status === 'SCHEDULED') {
    buttonText = 'COMPLETE';
  }
  let revertButtonText = 'REQUESTED';
  if (request.status === 'COMPLETED') {
    revertButtonText = 'SCHEDULED';
  } else if (request.status === 'SCHEDULED') {
    revertButtonText = 'PROCESSING';
  } else if (
    request.status === 'PROCESSING' ||
    request.status === 'CANCELLED'
  ) {
    revertButtonText = 'REQUESTED';
  }
  /* eslint-disable */
  const buttonRevert =
    request.status === 'REQUESTED' ? null : (
      <Button negative onClick={() => submitTransaction('revert')}>
        {`Revert to ${revertButtonText}`}
      </Button>
    );
  /* eslint-enable */
  const actionButton = isAllowedAction(request.status) ? (
    <Button
      disabled={disableButton}
      positive
      onClick={() => submitTransaction('process')}
    >
      {buttonText}
    </Button>
  ) : null;

  const editButton = isAllowedAction(request.status) ? (
    <Button
      disabled={disableButton}
      positive
      onClick={() => setPageState({ editRequestModal: true })}
    >
      EDIT
    </Button>
  ) : null;
  // const
  if (isNotifTimeout) {
    const interval = setInterval(() => {
      const savedTime = moment(sessionStorage.getItem('emailSentTimeOut'));
      const thisTime = moment();
      if (thisTime > savedTime) {
        clearInterval(interval);
        pageProps.activateNotif();
      }
    }, 1000);
  }

  const resendNotif =
    !isNotifTimeout && disableButton ? (
      <Button positive onClick={() => pageProps.resendPayeeNotif(request)}>
        Resend Payee Agreement
      </Button>
    ) : null;

  const proccessButton = (
    <div style={{ textAlign: 'right' }}>
      {buttonRevert}
      {editButton}
      {actionButton}
      {resendNotif}
    </div>
  );
  return (
    <div>
      {currentUser.user_type === 'super_admin' ? proccessButton : null}
      {!tableLoading && tableData.results.length > 0 ? (
        <div>
          <Divider />
          <h1>
            <span style={{ color: '#737272' }}>
              <FormattedMessage {...messages.transactionHistory} />
            </span>
          </h1>
        </div>
      ) : null}

      {tableLoading ? <SegmentLoader /> : <DataTable data={data} />}
      {!revisionLoading && revisionData.results.length > 0 && currentUser.user_type === 'super_admin' ? (
        <div>
          <Divider />

          <h1>
            <span style={{ color: '#737272' }}>
              <FormattedMessage {...messages.changeHistory} />
            </span>
          </h1>
          {renderChangeLog(revisionData)}
        </div>
      ) : null}
    </div>
  );
};

AdminDetails.propTypes = {
  pageProps: PropTypes.any.isRequired,
  renderChangeLog: PropTypes.func.isRequired,
  submitTransaction: PropTypes.func.isRequired,
  setPageState: PropTypes.func.isRequired,
  isAllowedAction: PropTypes.func.isRequired,
};

export default AdminDetails;
