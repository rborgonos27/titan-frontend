import React from 'react';
import PropTypes from 'prop-types';
import StandardModal from 'components/StandardModal';
import moment from 'moment';
import { APP } from 'config';

const CancelPaymentModal = ({
  pageState,
  pageProps,
  closeModal,
  handleCancelPayment,
}) => {
  const { openCancelPayment, serverTime } = pageState;
  const { currentUser } = pageProps;
  const { request } = pageProps.requestviewpage;
  if (!request) return null;
  const serverDateActual = moment(serverTime);
  const payAtDate = moment(request.due_at, APP.DATE_TIME_FORMAT);
  let daysDiff = payAtDate.diff(serverDateActual, 'days');
  const payAtDay = moment(request.due_at).format('ddd');
  /* eslint-disable */
  if (payAtDay === 'Mon') {
    daysDiff = daysDiff - 2;
  } else if (payAtDay === 'Tue') {
    daysDiff = daysDiff - 2;
  } else if (daysDiff > 5) {
    daysDiff = daysDiff - 2;
  }
  /* eslint-enable */
  let modalText =
    'You are cancelling this transaction. This payment request may not be reverse once confirmed. Thank you.';
  if (daysDiff < 2) {
    modalText =
      'Your payment date is less than 2 business days away. Please contact your Titan Officer to modify this transaction.';
  }
  let actionText = 'CONFIRM';
  let actionContinue = handleCancelPayment;
  if (currentUser.user_type === 'super_admin') {
    actionText = 'CONFIRM';
    actionContinue = handleCancelPayment;
    modalText =
      'You are cancelling this transaction. This payment request may not be reverse once confirmed. Thank you.';
  } else {
    actionText = daysDiff < 2 ? 'OK' : 'CONFIRM';
    actionContinue =
      daysDiff < 2
        ? () => closeModal('openCancelPayment')
        : handleCancelPayment;
  }
  return (
    <StandardModal
      headerText="Cancel Payment"
      open={openCancelPayment}
      onClose={() => closeModal('openCancelPayment')}
      onActionText={actionText}
      onContinue={actionContinue}
    >
      <div>{modalText}</div>
    </StandardModal>
  );
};

CancelPaymentModal.propTypes = {
  pageState: PropTypes.any.isRequired,
  pageProps: PropTypes.any.isRequired,
  closeModal: PropTypes.func.isRequired,
  handleCancelPayment: PropTypes.func.isRequired,
};

export default CancelPaymentModal;
