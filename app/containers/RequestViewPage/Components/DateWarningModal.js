import React from 'react';
import PropTypes from 'prop-types';
import StandardModal from 'components/StandardModal';

const DateWarningModal = ({ pageState, closeModal, setPageState }) => {
  const { belowDateWarning } = pageState;
  return (
    <StandardModal
      headerText="Reminder"
      open={belowDateWarning}
      onClose={() => closeModal('openConfirmDate')}
      onActionText="CLOSE"
      onContinue={() => setPageState({ belowDateWarning: false })}
    >
      <p>You selected previous date.</p>
    </StandardModal>
  );
};

DateWarningModal.propTypes = {
  pageState: PropTypes.any.isRequired,
  setPageState: PropTypes.any.isRequired,
  closeModal: PropTypes.func.isRequired,
};

export default DateWarningModal;
