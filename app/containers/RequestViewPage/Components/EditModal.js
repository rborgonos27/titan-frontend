import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { APP } from 'config';
import styled from 'styled-components';
import {
  Grid,
  Form,
  Input,
  Dropdown,
  Divider,
  Dimmer,
  Loader,
} from 'semantic-ui-react';
import StandardModal from 'components/StandardModal';
import Dropzone from 'react-dropzone';
import { getDateForward } from 'containers/SendPayment/dates';
import { DetailData } from 'components/Utils';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import closeImage from 'images/close.png';

const CheckBoxSection = styled.div`
  margin-left: -200px;
  margin-top: 5px;
  margin-bottom: 20px;
  font-size: 20px;
  color: #464646;
`;

const EditModal = ({
  pageProps,
  pageState,
  handleDayChangePickUpAt,
  handleDayChangePayAt,
  handleInput,
  handleInputAmount,
  handleChangeDropDown,
  disableUpDownArrow,
  onImageDrop,
  getFileSrc,
  formatDate,
  updateRequest,
  setPageState,
}) => {
  const { requestviewpage } = pageProps;
  const {
    editRequestModal,
    serverTime,
    amount,
    transactionType,
    payAt,
    pickupAt,
    isChecked,
  } = pageState;
  const { request, loading, files, filesLoading } = requestviewpage;
  if (!request) return null;
  const serverTimeMoment = moment(serverTime).format(APP.DATE_FORMAT);
  const serverTimeMomentMonth = moment(serverTime).format(
    APP.DATE_MONTH_FORMAT,
  );
  const serverTimeHourMoment = moment(serverTime).format('H');
  // 14 is 2pm in military time
  const daysForward = serverTimeHourMoment > 14 ? 8 : 7;
  const minPayDate = getDateForward(daysForward, serverTimeMoment);
  const minPayDateMonth = moment(minPayDate).format(APP.DATE_MONTH_FORMAT);
  const dueAtCurrentValue = moment(request.due_at, APP.DATE_TIME_FORMAT).format(
    APP.DATE_FORMAT,
  );
  const pickupAtCurrentValue = moment(
    request.pickup_at,
    'YYYY-MM-DD h:m:s',
  ).format(APP.DATE_FORMAT);
  const transactionTypeOption = [
    { key: 'PAPER', value: 'PAPER', text: 'PAPER' },
    { key: 'ELECTRONIC', value: 'ELECTRONIC', text: 'ELECTRONIC' },
  ];
  const previewStyle = {
    display: 'inline',
    width: 70,
    height: 70,
    padding: 5,
  };

  const previewSpan = {
    display: 'inline',
  };
  const closeButton = {
    position: 'absolute',
    width: '30px',
    height: '30px',
    objectFit: 'cover',
  };
  return (
    <StandardModal
      headerText="Modify Request"
      open={editRequestModal}
      onClose={() => setPageState({ editRequestModal: false })}
      onActionText="Update"
      onContinue={() => updateRequest()}
    >
      <Form loading={loading} className="formModal">
        <Grid stackable columns={2}>
          <Grid.Row>
            <Grid.Column>
              <DetailData>Due At</DetailData>
              <DayPickerInput
                value={payAt === '' ? dueAtCurrentValue : payAt}
                onDayChange={handleDayChangePayAt}
                onClick={() => {}}
                placeholder={APP.DATE_FORMAT}
                format={APP.DATE_FORMAT}
                initialMonth={new Date(minPayDateMonth)}
                formatDate={formatDate}
                inputProps={{ readOnly: true }}
                dayPickerProps={{
                  // fromMonth: new Date(minPayDateMonth),
                  // month: new Date(minPayDateMonth),
                  modifiers: {
                    disabled: [
                      {
                        daysOfWeek: [0, 6],
                      },
                      {
                        // before: new Date(minPayDate),
                      },
                    ],
                  },
                }}
              />
            </Grid.Column>
            <Grid.Column>
              <DetailData>Pickup At</DetailData>
              <DayPickerInput
                value={pickupAt === '' ? pickupAtCurrentValue : pickupAt}
                onDayChange={handleDayChangePickUpAt}
                placeholder={APP.DATE_FORMAT}
                format={APP.DATE_FORMAT}
                initialMonth={new Date(serverTimeMomentMonth)}
                formatDate={formatDate}
                inputProps={{
                  readOnly: true,
                }}
                dayPickerProps={{
                  // fromMonth: new Date(minPayDateMonth),
                  // month: new Date(serverTimeMomentMonth),
                  // toMonth: new Date(maxPickupMomentMonth),
                  modifiers: {
                    disabled: [
                      {
                        daysOfWeek: [0, 6],
                      },
                      {
                        // before: new Date(minPickupDate),
                      },
                      {
                        // after: new Date(maxPickupDate),
                      },
                    ],
                  },
                }}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <DetailData>Amount</DetailData>
              <Input
                value={amount}
                name="amount"
                onChange={handleInput}
                onBlur={handleInputAmount}
                fluid
                placeholder="Amount"
                autoComplete="off"
                onKeyDown={e => disableUpDownArrow(e)}
              />
            </Grid.Column>
            <Grid.Column>
              <DetailData>Transaction Type</DetailData>
              <Dropdown
                fluid
                value={transactionType}
                selection
                onChange={handleChangeDropDown}
                options={transactionTypeOption}
                placeholder="Transaction Type"
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              {/* eslint-disable */}
              <label className="container_checkbox">
                <CheckBoxSection>Waive Fee?</CheckBoxSection>
                <input
                  type="checkbox"
                  checked={isChecked}
                  onChange={() => setPageState({ isChecked: !isChecked })}
                />
                <span className="_checkmark" />
              </label>
              {/* eslint-enable */}
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Divider />
        <Grid>
          <Grid.Row>
            {filesLoading ? (
              <Dimmer active inverted>
                <Loader size="massive">Loading</Loader>
              </Dimmer>
            ) : null}
            <DetailData>Upload Backup</DetailData>
            <Dropzone
              style={{ width: '100%' }}
              className=" col-lg-8 col-md-6 col-sm-6 col-xs-12 dropZone"
              onDrop={onImageDrop}
              multiple
              accept="image/*,application/pdf,
                        application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel,
                        application/doc,
                        application/ms-doc,
                        application/msword,
                        application/vnd.openxmlformats-officedocument.wordprocessingml.document,
                        application/vnd.ms-excel,
                        .csv
                        "
            >
              <p>Drop your files or click here to upload</p>
              {/* eslint-disable */
                files.length > 0 && (
                  <div>
                    {files.map((file, key) => (
                      <span key={key} style={previewSpan}>
                                {request.status === 'COMPLETED' ? null : (
                                  <a onClick={(e) => {
                                    const fileId = file.id;
                                    const { requestId } = pageState;
                                    e.preventDefault();
                                    e.stopPropagation();
                                    const removeFileObject = { fileId, requestId };
                                    setPageState({ removeFileObject, confirmDeleteModal: true });
                                  }
                                  }>
                                    <img
                                      alt=""
                                      style={closeButton}
                                      key={key}
                                      src={closeImage}
                                    />
                                  </a>
                                )}
                                <img
                                  alt="Preview"
                                  key={key}
                                  src={getFileSrc(file)}
                                  style={previewStyle}
                                />
                                <b style={{fontSize: 10}}>{file.name}</b>
                              </span>
                    ))}
                  </div>
                )
                /* eslint-enable */
              }
            </Dropzone>
          </Grid.Row>
        </Grid>
      </Form>
    </StandardModal>
  );
};

EditModal.propTypes = {
  pageProps: PropTypes.any.isRequired,
  pageState: PropTypes.any.isRequired,
  handleInput: PropTypes.func.isRequired,
  handleInputAmount: PropTypes.func.isRequired,
  handleChangeDropDown: PropTypes.func.isRequired,
  disableUpDownArrow: PropTypes.func.isRequired,
  onImageDrop: PropTypes.func.isRequired,
  getFileSrc: PropTypes.func.isRequired,
  handleDayChangePickUpAt: PropTypes.func.isRequired,
  handleDayChangePayAt: PropTypes.func.isRequired,
  updateRequest: PropTypes.func.isRequired,
  setPageState: PropTypes.func.isRequired,
  formatDate: PropTypes.func.isRequired,
};

export default EditModal;
