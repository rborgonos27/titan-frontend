import React from 'react';
import PropTypes from 'prop-types';
import { getDateForward } from 'containers/SendPayment/dates';
import moment from 'moment';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import pdfImage from 'images/pdf.png';
import { Form, Divider, Grid, Button } from 'semantic-ui-react';
import { formatMoney } from 'utils/formatMoney';
import { DetailData, ALink } from 'components/Utils';
import { APP } from 'config';
import { FormattedMessage } from 'react-intl';
import messages from '../messages';
import AdminDetails from './AdminDetail';

const openInNewTab = (e, url) => {
  e.preventDefault();
  window.open(url);
};

const previewStyle = {
  display: 'inline',
  width: 30,
  height: 30,
  padding: 5,
};

const DepositView = ({
  pageProps,
  pageState,
  handleDayChangePickUpAt,
  handleDayChangePayAt,
  setPageState,
  formatDate,
  isAllowedAction,
  handleModifyPaymentDate,
  renderUpdateCancelButton,
  handleUpdatePickupAtValues,
  submitTransaction,
  renderChangeLog,
  getStatus,
}) => {
  const { requestviewpage, currentUser } = pageProps;
  const { openPaymentDate, openPickUpDate, serverTime } = pageState;
  const { request, loading } = requestviewpage;
  const serverTimeMoment = moment(serverTime).format(APP.DATE_FORMAT);
  const serverTimeMomentMonth = moment(serverTime).format(
    APP.DATE_MONTH_FORMAT,
  );
  const serverTimeHourMoment = moment(serverTime).format('H');
  // 14 is 2pm in military time
  const daysForward = serverTimeHourMoment > 14 ? 8 : 7;
  const minPayDate = getDateForward(daysForward, serverTimeMoment);
  const minPayDateMonth = moment(minPayDate).format(APP.DATE_MONTH_FORMAT);
  const { maxPickupDate } = handleUpdatePickupAtValues();
  const maxPickupMomentMonth = moment(maxPickupDate).format(
    APP.DATE_MONTH_FORMAT,
  );
  const dueAt = moment(request.due_at, APP.DATE_TIME_FORMAT).format(
    APP.DATE_DISPLAY_FORMAT,
  );
  const pickupAt = moment(request.pickup_at, APP.DATE_TIME_FORMAT).format(
    APP.DATE_DISPLAY_FORMAT,
  );
  const total = parseFloat(request.amount) - parseFloat(request.fee);
  const totalAmount = formatMoney(total);
  const dueAtCurrentValue = moment(request.due_at, APP.DATE_TIME_FORMAT).format(
    APP.DATE_FORMAT,
  );
  const pickupAtCurrentValue = moment(
    request.pickup_at,
    'YYYY-MM-DD h:m:s',
  ).format(APP.DATE_FORMAT);
  const serverDateActual = moment(serverTime);
  const payAtDate = moment(request.due_at, APP.DATE_TIME_FORMAT);
  const daysDiff = payAtDate.diff(serverDateActual, 'days');
  const renderDueAt = openPaymentDate ? (
    <DayPickerInput
      value={pageState.payAt === '' ? dueAtCurrentValue : pageState.payAt}
      onDayChange={handleDayChangePayAt}
      onClick={() => {}}
      placeholder={APP.DATE_FORMAT}
      format={APP.DATE_FORMAT}
      initialMonth={new Date(minPayDateMonth)}
      formatDate={formatDate}
      inputProps={{ readOnly: true }}
      dayPickerProps={{
        // fromMonth: new Date(minPayDateMonth),
        modifiers: {
          disabled: [
            {
              daysOfWeek: [0, 6],
            },
            {
              // before: new Date(minPayDate),
            },
          ],
        },
      }}
    />
  ) : (
    dueAt
  );
  const renderPickupAt = openPickUpDate ? (
    <DayPickerInput
      value={
        pageState.pickupAt === '' ? pickupAtCurrentValue : pageState.pickupAt
      }
      onDayChange={handleDayChangePickUpAt}
      placeholder={APP.DATE_FORMAT}
      format={APP.DATE_FORMAT}
      initialMonth={new Date(serverTimeMomentMonth)}
      formatDate={formatDate}
      inputProps={{
        readOnly: true,
      }}
      dayPickerProps={{
        fromMonth: new Date(serverTimeMomentMonth),
        toMonth: new Date(maxPickupMomentMonth),
        modifiers: {
          disabled: [
            {
              daysOfWeek: [0, 6],
            },
            {
              // before: new Date(minPickupDate),
            },
            {
              // after: new Date(maxPickupDate),
            },
          ],
        },
      }}
    />
  ) : (
    pickupAt
  );

  return (
    <Form size="huge" loading={loading}>
      <Grid stackable columns={2}>
        <Grid.Row>
          <Grid.Column>
            <h2>
              <span style={{ color: '#fc5109' }}>
                {request.type === 'DEPOSIT' ? (
                  <FormattedMessage {...messages.deposit} />
                ) : (
                  <FormattedMessage {...messages.payment} />
                )}
              </span>{' '}
              <span style={{ color: '#737272' }}>
                <FormattedMessage {...messages.details} />
              </span>
            </h2>
          </Grid.Column>
          <Grid.Column width={4}>
            <h2>{getStatus(request.status)}</h2>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div style={{ marginBottom: 20 }} />
          </Grid.Column>
          <Grid.Column>
            <div style={{ marginBottom: 20 }} />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            {request.type === 'DEPOSIT' ? 'Deposit Date' : 'Payment Date'}
            <DetailData>
              {renderDueAt}
              {daysDiff > 3 || currentUser.user_type === 'super_admin' ? (
                <span style={{ fontSize: 14, marginLeft: 10 }}>
                  {isAllowedAction(request.status) ? (
                    <ALink
                      onClick={handleModifyPaymentDate}
                      linkColor="#fc5109"
                    >
                      {openPaymentDate ? '' : 'Modify'}
                    </ALink>
                  ) : null}
                </span>
              ) : null}
              {openPaymentDate ? renderUpdateCancelButton() : null}
            </DetailData>
          </Grid.Column>
          <Grid.Column width={4}>
            Amount
            <span style={{ marginLeft: 10 }}>
              <ALink onClick={() => setPageState({ openBackup: true })}>
                See Backup
              </ALink>
            </span>
            <DetailData>
              <b>{`$ ${formatMoney(request.amount)}`}</b>
            </DetailData>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            Pickup Date
            <DetailData>
              {renderPickupAt}
              {daysDiff > 3 || currentUser.user_type === 'super_admin' ? (
                <span style={{ fontSize: 14, marginLeft: 10 }}>
                  {isAllowedAction(request.status) ? (
                    <ALink
                      onClick={() =>
                        setPageState({
                          openPickUpDate: !openPickUpDate,
                          pickupAt: '',
                        })
                      }
                      linkColor="#fc5109"
                    >
                      {currentUser.user_type !== 'super_admin' || openPickUpDate
                        ? ''
                        : 'Modify'}
                    </ALink>
                  ) : null}
                </span>
              ) : null}
              {openPickUpDate ? renderUpdateCancelButton() : null}
            </DetailData>
          </Grid.Column>
          <Grid.Column width={4}>
            Fee
            <DetailData>{`$ ${formatMoney(request.fee)}`}</DetailData>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            Transaction Id
            <DetailData>{request.request_id}</DetailData>
          </Grid.Column>
          <Grid.Column width={4}>
            Net Amount
            <DetailData>{`$ ${totalAmount}`}</DetailData>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            Transaction Type
            <DetailData>{request.transaction_type}</DetailData>
          </Grid.Column>
          <Grid.Column width={4}>
            <div>
              <ALink onClick={() => window.print()} linkColor="#fc5109">
                Print Detail
              </ALink>
            </div>
            <div>
              {isAllowedAction(request.status) ? (
                <ALink
                  onClick={() => setPageState({ openCancelPayment: true })}
                  linkColor="#fc5109"
                >
                  Cancel Deposit
                </ALink>
              ) : null}
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            {request.ticket ? (
              <span>
                <Button positive onClick={e => openInNewTab(e, request.ticket)}>
                  <img alt="Preview" src={pdfImage} style={previewStyle} />
                </Button>
                <div>
                  <b style={{ fontSize: 10 }}>Print Ticket</b>
                </div>
              </span>
            ) : null}
          </Grid.Column>
          <Grid.Column>
            <div style={{ marginBottom: 20 }} />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div style={{ marginBottom: 20 }} />
          </Grid.Column>
          <Grid.Column width={4}>
            <div style={{ marginBottom: 20 }} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <AdminDetails
        pageProps={pageProps}
        submitTransaction={submitTransaction}
        setPageState={setPageState}
        isAllowedAction={isAllowedAction}
        renderChangeLog={renderChangeLog}
      />
      <Divider />
      <Button onClick={() => pageProps.history.goBack()} size="huge">
        Back
      </Button>
    </Form>
  );
};

DepositView.propTypes = {
  pageProps: PropTypes.any.isRequired,
  pageState: PropTypes.any.isRequired,
  handleDayChangePickUpAt: PropTypes.func.isRequired,
  handleDayChangePayAt: PropTypes.func.isRequired,
  setPageState: PropTypes.func.isRequired,
  formatDate: PropTypes.func.isRequired,
  isAllowedAction: PropTypes.func.isRequired,
  submitTransaction: PropTypes.func.isRequired,
  renderChangeLog: PropTypes.func.isRequired,
  handleModifyPaymentDate: PropTypes.func.isRequired,
  renderUpdateCancelButton: PropTypes.func.isRequired,
  handleUpdatePickupAtValues: PropTypes.func.isRequired,
  getStatus: PropTypes.func.isRequired,
};

export default DepositView;
