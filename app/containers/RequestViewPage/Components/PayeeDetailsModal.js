import React from 'react';
import PropTypes from 'prop-types';
import StandardModal from 'components/StandardModal';
import { SegmentLoader } from 'components/Utils';
import PayeeDetails from './PayeeDetails';

const PayeeDetailsModal = ({ pageState, pageProps, closeModal }) => {
  const { openDetails } = pageState;
  const { requestviewpage } = pageProps;
  const { request, loading } = requestviewpage;
  if (!request) return null;

  const renderData = loading ? (
    <SegmentLoader />
  ) : (
    <PayeeDetails payee={request.payee} />
  );
  return (
    <StandardModal
      headerText="Payee Details"
      open={openDetails}
      onClose={() => closeModal('openDetails')}
      onActionText="OK"
      onContinue={() => closeModal('openDetails')}
    >
      <div style={{ textAlign: 'left' }}>{renderData}</div>
    </StandardModal>
  );
};

PayeeDetailsModal.propTypes = {
  pageState: PropTypes.any.isRequired,
  pageProps: PropTypes.any.isRequired,
  closeModal: PropTypes.func.isRequired,
};

export default PayeeDetailsModal;
