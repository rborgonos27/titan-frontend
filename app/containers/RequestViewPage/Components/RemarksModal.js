import React from 'react';
import PropTypes from 'prop-types';
import StandardModal from 'components/StandardModal';
import { Form, TextArea } from 'semantic-ui-react';

const RemarksModal = ({ pageState, handleInput, closeModal, submitData }) => {
  const { isOpenRemarks, transaction, remarks } = pageState;
  const isbuttonDisabled = transaction === 'revert' && remarks === '';
  return (
    <StandardModal
      headerText="Transaction Note"
      open={isOpenRemarks}
      onClose={() => closeModal('isOpenRemarks')}
      onActionText="SUBMIT"
      btnDisabled={isbuttonDisabled}
      onContinue={submitData}
    >
      <Form>
        <Form.Field>
          <TextArea
            name="remarks"
            onChange={handleInput}
            rows={3}
            placeholder="Input transaction notes."
            value={remarks}
          />
        </Form.Field>
      </Form>
    </StandardModal>
  );
};

RemarksModal.propTypes = {
  pageState: PropTypes.any.isRequired,
  handleInput: PropTypes.any.isRequired,
  closeModal: PropTypes.func.isRequired,
  submitData: PropTypes.func.isRequired,
};

export default RemarksModal;
