import React from 'react';
import PropTypes from 'prop-types';
import StandardModal from 'components/StandardModal';
import { TextArea } from 'semantic-ui-react';
import { RequiredText } from 'components/Utils/RequiredText';

const ConfirmDeleteModal = ({
  pageState,
  setPageState,
  pageProps,
  handleInput,
}) => {
  const { confirmDeleteModal, removeFileObject, backupRemarks } = pageState;
  const isBackupAltered = true;
  if (!removeFileObject) return null;
  const { fileId, requestId } = removeFileObject;
  const removeFileObjectWithRemarks = { fileId, requestId, backupRemarks };
  return (
    <StandardModal
      headerText="Confirm Delete"
      open={confirmDeleteModal}
      onClose={() =>
        setPageState({
          confirmDeleteModal: false,
          isBackupAltered: !isBackupAltered,
        })
      }
      onActionText="CONFIRM"
      onContinue={() => {
        setPageState({ isBackupAltered });
        pageProps.removeFile(removeFileObjectWithRemarks);
      }}
      btnDisabled={backupRemarks === ''}
    >
      <p>Are you sure you want to delete this file?</p>
      <p>
        {`Reason `}
        <RequiredText>*</RequiredText>
      </p>
      <p>
        <TextArea
          className="materialInput inputWidth"
          name="backupRemarks"
          placeholder="Input reason"
          onChange={handleInput}
          value={backupRemarks}
          line={3}
        />
      </p>
    </StandardModal>
  );
};

ConfirmDeleteModal.propTypes = {
  pageState: PropTypes.any.isRequired,
  pageProps: PropTypes.any.isRequired,
  setPageState: PropTypes.any.isRequired,
  handleInput: PropTypes.any.isRequired,
};

export default ConfirmDeleteModal;
