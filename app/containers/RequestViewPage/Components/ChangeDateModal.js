import React from 'react';
import PropTypes from 'prop-types';
import StandardModal from 'components/StandardModal';

const ChangeDateModal = ({
  pageState,
  closeModal,
  handleModifyPaymentDateSubmit,
}) => {
  const { openConfirmDate } = pageState;
  return (
    <StandardModal
      headerText="Confirm Change Date"
      open={openConfirmDate}
      onClose={() => closeModal('openConfirmDate')}
      onActionText="CONFIRM"
      onContinue={handleModifyPaymentDateSubmit}
    >
      <div>Are you sure you want to update date in this request?</div>
    </StandardModal>
  );
};

ChangeDateModal.propTypes = {
  pageState: PropTypes.any.isRequired,
  handleModifyPaymentDateSubmit: PropTypes.any.isRequired,
  closeModal: PropTypes.func.isRequired,
};

export default ChangeDateModal;
