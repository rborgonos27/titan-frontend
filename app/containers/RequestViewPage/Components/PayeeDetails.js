import React from 'react';
import PropTypes from 'prop-types';
import { Grid } from 'semantic-ui-react';

const PayeeDetails = ({ payee }) => {
  if (!payee) {
    return null;
  }
  return (
    <div style={{ margin: 10 }}>
      <Grid columns={2}>
        <Grid.Row>
          <Grid.Column>Address:</Grid.Column>
          <Grid.Column>
            <b>{payee.address}</b>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>Bank:</Grid.Column>
          <Grid.Column>
            <b>{payee.bank_name}</b>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>Bank Account:</Grid.Column>
          <Grid.Column>
            <b>{payee.bank_account_name}</b>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>Bank Account Number:</Grid.Column>
          <Grid.Column>
            <b>{payee.bank_account}</b>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>Bank Routing Number:</Grid.Column>
          <Grid.Column>
            <b>{payee.bank_routing_number}</b>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

PayeeDetails.propTypes = {
  payee: PropTypes.any.isRequired,
};

export default PayeeDetails;
