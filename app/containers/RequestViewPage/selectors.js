import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the requestViewPage state domain
 */
const selectGlobal = state => state.get('global');
const selectRequestViewPageDomain = state =>
  state.get('requestViewPage', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by RequestViewPage
 */

const makeSelectRequestViewPage = () =>
  createSelector(selectRequestViewPageDomain, substate => substate.toJS());

const makeSelectCurrentUser = () =>
  createSelector(selectGlobal, globalState => globalState.get('userData'));

const makeSelectFeeTierObject = () =>
  createSelector(selectGlobal, globalState => globalState.get('feeTierObject'));

export default makeSelectRequestViewPage;
export {
  selectRequestViewPageDomain,
  makeSelectCurrentUser,
  makeSelectFeeTierObject,
};
