import { fromJS } from 'immutable';
import requestViewPageReducer from '../reducer';

describe('requestViewPageReducer', () => {
  it('returns the initial state', () => {
    expect(requestViewPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
