import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the memberServicesPage state domain
 */

const selectMemberServicesPageDomain = state =>
  state.get('memberServicesPage', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by MemberServicesPage
 */

const makeSelectMemberServicesPage = () =>
  createSelector(selectMemberServicesPageDomain, substate => substate.toJS());

export default makeSelectMemberServicesPage;
export { selectMemberServicesPageDomain };
