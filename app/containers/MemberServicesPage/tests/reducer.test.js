import { fromJS } from 'immutable';
import memberServicesPageReducer from '../reducer';

describe('memberServicesPageReducer', () => {
  it('returns the initial state', () => {
    expect(memberServicesPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
