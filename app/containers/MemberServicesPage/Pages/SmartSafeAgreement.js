import React from 'react';
import titanLogo from 'images/titan-logo.png';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import PropTypes from 'prop-types';

/* eslint-disable */
import {
  B,
  SubHeader,
  TextBody,
  BodyWrapper,
  HeaderPlain,
  HeaderLeft,
  HeaderCenter,
  Img,
  A,
  BigHeader,
  HR,
} from './StyleUtils';

const SmartSafeAgreement = ({ setBack }) => (
  <ContainerWrapper>
    <CardBox>
      <Img src={titanLogo} />
      <A onClick={() => setBack()}>Back</A>
      <HR />
      <BodyWrapper>
        <HeaderCenter>Smartsafe Agreement</HeaderCenter>
        <TextBody>
          This SmartSafe Agreement (the “<B>Agreement</B>”) is a binding
          contract between you and us for your account and your relationship
          with us.
        </TextBody>
        <TextBody>
          This Agreement expresses and outlines the services, roles, and
          responsibilities of the parties. If additional locations are added to
          the scope of this Agreement, consistent terms and services will be
          maintained.
        </TextBody>
        <TextBody>
          <B>Term:</B> The initial term of this Agreement will begin on the date
          that you agree in writing (including email) to allow us to install a
          smart safe on your premises and shall continue until we remove the
          smart safe. Except as otherwise provided in the termination provisions
          of Section 10 or as otherwise specifically provided in this Agreement,
          it is expressly understood and agreed that there is no provision for
          early termination of this Agreement; provided, however, that (a)
          either Party may terminate this Agreement upon ten (10) days written
          notice to the other Party in the event of the bankruptcy or insolvency
          of the other Party.
        </TextBody>
        <TextBody>
          <B>Additional Safes and Equipment:</B> You acknowledge and agree that
          any safes or equipment added to this agreement at a later date shall
          be subject to like term periods and this Agreement shall automatically
          extend the term period to cover such additional safes or equipment.
        </TextBody>
        <br />
        <HeaderCenter>TERMS AND CONDITIONS</HeaderCenter>
        <ol>
          <li>
            <B>SERVICES and EQUIPMENT</B>
            <ol type='a'>
              <li>
                <B><u>Services.</u> "Services</B> include the transportation and cash management services described below.  The fees payable by you to us for Services are described in this Agreement.
                <ol type="(1)">
                  <li>
                    <u>Transportation Services.</u> We (throughout this Agreement, “we” or “us” includes our designated agent) agree to pick up, receive from, and/or deliver to you, or any designated agent, securely sealed or locked shipments which may contain any or all of the following: currency, coin, checks, securities, or other valuables. If the shipment container(s) received by us from you or your designated consignee, does not appear to be securely locked or sealed, we reserve the right to refuse to accept such container(s) and will not receive said container(s) from the you or your designated agent. If we accept sealed container(s), we will give you a receipt for said sealed container(s), transport and deliver such sealed container(s) to our designated processing facility. You agree that it will not conceal or misrepresent any material fact or circumstance concerning the property delivered to us pursuant to this Agreement.
                  </li>
                  <li>
                    <u>Cash Management Services</u> (as it may apply for the processing of manual drop deposits).  Upon transportation and delivery of the shipment container(s) to our facility, we will verify the currency and checks received if applicable pursuant to the procedures as outlined in Exhibit A. As part of the cash management services, we will notify you by email or telephone (using primary contact information shown on the member account unless otherwise stated) of the relevant shipment container(s) if our verification procedure indicates any of the following:
                    <ol type='a'>
                      <li>
                        Discrepancy in excess of $100.00; and/or
                      </li>
                      <li>
                        Fraud of any kind.
                      </li>
                    </ol>
                  </li>
                </ol>
                <TextBody>
                  Our verification procedure involves confirming that your deposit ticket(s) matches the fine count of the contents as performed by us.  Differences may include shortages, overages and/or counterfeit of any monies processed by us. We agree to be responsible for all verification errors discovered or claimed after seventy-two (72) hours have expired from the time of pickup.
                </TextBody>
              </li>
              <li>
                <B><u>Equipment.</u></B>  We agree to provide, and you agree to take possession of, a smartsafe (the “Equipment”) at each agreed upon location.
                <ol type="(1)">
                    <li>
                      <u>Inspection of Equipment.</u>  You shall initially inspect the Equipment within forty-eight (48) hours after delivery.  Unless you give written notice to us specifying any defect in or other proper objection to the Equipment, you agree that it shall be deemed that the Equipment is in good condition and repair and that you are satisfied with the Equipment. We shall at any and all times during your regular business hours have the right to enter into and upon the Location (defined as your location where the Equipment is located) to inspect the Equipment and observe its use.
                    </li>
                    <li>
                      <u>Suitability of Equipment.</u>  You acknowledge that (a) the Equipment described herein is of the type and kind suitable for your purpose and needs; and (b) you agree to provide us with forty-five (45) days prior written notice should it become necessary to move the Equipment to a different location.  You shall not move the Equipment to another location without our prior written consent, which shall not be unreasonably withheld or delayed.  You further agree to pay all costs associated with the relocation of such Equipment.
                    </li>
                    <li>
                      <u>Installation.</u>  You shall be responsible for any applicable permits or licenses, which may be required for the installation and/or operations of such Equipment.  Each piece of Equipment must be installed by or at the direction, or as approved by us.  You shall provide, at your own expense, and throughout the term of this Agreement: a dedicated, grounded electrical line and/or a dedicated phone line and/or data line, and any other necessary site preparation as may be required for appropriate installation and as needed to maintain the correct ongoing operation of the safe.
                    </li>
                    <li>
                      <u>Return or Repossession.</u> Upon the expiration or earlier termination of this Agreement, you shall return the Equipment to us in good repair, condition and working order, ordinary wear and tear resulting from proper use excepted, by delivering, or causing to be delivered, the Equipment at your cost and expense to such place as we shall specify within the city or county in which the same was delivered to you.  In the event of loss or damage to the Equipment, you shall, at our option: (a) place the same in good repair, condition and working order; or (b) replace the same with like Equipment in good repair, condition and working order; or (c) pay us the replacement cost of the Equipment.  No loss or damage to the Equipment or any part thereof shall impair any of your obligations under this Agreement, which shall continue in full force and effect through the term of the Agreement.  In the event that it becomes necessary, during, or at the end of the Agreement term for us to take possession of and/or be required to remove Equipment provided for herein from your premises, we shall be entitled to reasonable reimbursement from you, of all expenses associated with removal and/or repossession of such Equipment, and reasonable attorney’s fees, if incurred to obtain appropriate authority for removal and/or repossession.
                    </li>
                    <li>
                      <u>Access to Safe.</u> You shall provide us or our agent, vendor, contractor or other designee access to safe during normal business hours or a mutually agreeable time to perform maintenance and service to ensure proper functionality and security of safe.
                    </li>
                </ol>
              </li>
              <li>
                <B><u>Your Representations and Agreements.</u></B>  With regard to the Services, you acknowledge and agree that our count of all funds verified shall be deemed correct and final.  Further, you agrees that it will not conceal or misrepresent any material fact or circumstance concerning the property delivered to us pursuant to this Agreement.  Regarding the Equipment, you agree that title and right to possession of the Equipment shall at no time pass to you.  The Equipment is and shall remain personal property of our vendor, notwithstanding the fact that the Equipment may be affixed, attached to or imbedded in or upon real property or a building, whether by cement, bolts, or other means.  The Equipment must be firmly and securely attached to the concrete foundation.  You represent and guarantee to us that the physical location(s) where the Equipment is located shall at a minimum have a fully functional monitored premise alarm, including fire or smoke detection.  Each of your locations shall also have reasonable and customary security measures for its industry, including, but not limited to, fully functional door locks and video camera surveillance on the Equipment.  In the event of a loss and it is determined by us that you were negligent regarding your site security obligations and ensuring that the Equipment is securely attached to the concrete foundation, then any of our guarantee of funds shall become void and we shall have no further obligation to reimburse you for any resulting loss of funds or Equipment
              </li>
            </ol>
          </li>
          <li>
              <B>LIABILITY</B>
              <ol type="a">
                <li>
                  <B><u>General Liability.</u></B> We agree to assume liability for any loss, according to the terms of this Agreement of the shipment container(s) from the time we sign for and receive physical custody of the shipment container(s) or when currency are deposited into the Equipment through the validators. We are not responsible for any funds deposited into the manual drop safe if applicable. Our responsibility terminates when you or your designated consignee takes physical possession of the shipment container(s) and signs our receipt.  If it is impossible to complete the delivery, we shall be responsible for any loss until the shipment container(s) is returned to you and a signed receipt obtained.  Before currency are deposited into the Equipment, we assume no liability for any loss. Also, we shall incur no liability whatsoever for losses arising from the misuse, abuse, malfunction or destruction of the Equipment and/or bill validator(s) by you, your employees, directors, agents, contractors or assigns, or from loss(es) or destruction arising, in whole or part, from fraud, negligence, or willful or criminal misconduct on your part, your employees, contractors, agents, directors or assigns or from unexplained or ongoing patterns of loss, as defined or determined by us or destruction including but not limited to counterfeit bills placed into safe as deposits. We reserve the right to take any and all action as may be reasonably necessary to prevent money laundering to the extent permitted under law or as may be required by any regulatory body that may exert a right of control over us.
                  <TextBody>
                    UNDER NO CIRCUMSTANCES WILL WE BE LIABLE TO YOU FOR LOST PROFITS OR FOR ANY INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING FROM THE SUBJECT MATTER OR SERVICES OF THIS AGREEMENT, REGARDLESS OF THE TYPE OF CLAIM AND EVEN IF THAT PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; SUCH AS, BUT NOT LIMITED TO LOSS OF REVENUE, LOSS OF INTEREST, LOST DATA, DATA TRANSMISSION ERROR OR ANTICIPATED PROFITS OR LOST BUSINESS. EXCEPT FOR THE CARGO LIABILITY SPECIFIED WITHIN SECTION 2, IN NO EVENT SHALL OUR LIABILITY TO YOU ARISING OUT OF THIS AGREEMENT EXCEED THE AMOUNT REPRESENTED BY THAT PORTION OF THE SERVICE CHARGE CONTAINED HEREIN FOR THE SERVICE(S) PAID BY YOU FOR SUCH SERVICES.
                  </TextBody>
                </li>
              </ol>
          </li>
          <li>
            <B>CLAIM PROCEDURES</B>
            <TextBody>
              The following provisions shall control in the event of any loss or claim, notwithstanding anything to the contrary contained in this Agreement.
            </TextBody>
            <ol type='a'>
              <li>
                <B><u>Notification.</u></B>  In the event of a loss, you agree to notify us in writing within four (4) calendar days after the loss is discovered or should have been discovered in the exercise of due care.  Notwithstanding anything in this Agreement to the contrary, you agree that any loss shall be reported by you to us within forty-five (45) days after the pick-up by us of the securely sealed shipment container in connection with which the loss is asserted.  Unless such notice has been received by us within this forty-five (45) day period, such claim shall be deemed waived by you.  It is agreed that both parties will work together to determine the extent of the loss, and if possible, the cause of loss.
              </li>
              <li>
                <B><u>Check Reconstruction.</u></B> You shall retain sufficient information to allow reconstruction of item(s) in the event of a loss.  In addition, you agree you will cooperate and assist in reconstructing lost, damaged, or destroyed items constituting a part of any loss.  Our liability, unless otherwise stated in this Agreement, shall be the payment to you for the reasonable costs necessary to reconstruct the item(s), any necessary cost because of stop-payment procedures or reasonable costs associated with you providing information and assistance with recovery of loss. The term “Reconstruction” is defined to mean the identification of the item(s) only to the extent of determining the face amount of said item(s) and the identity of the maker or endorser of each or providing audit trail, foreign or internal network information, data, customer information or other relevant information to allow us to recover any and/or all item(s) or cash associated with loss. You agree in the event of a loss, that our liability shall be reduced by the face value of reconstructed or recovered item(s).
              </li>
              <li>
                <B><u>Proof.</u></B>  Upon our request, you will furnish a proof of loss to us or our insurance carrier.  Once reimbursement has been made to you, we and our insurer shall receive any and all of your rights and remedies of recovery.
              </li>
            </ol>
          </li>
          <li>
            <B>LIMITATIONS & FORCE MAJEURE</B>
            <ol type='a'>
              <li>
                <B><u>Limitations.</u></B>  You agree that we will not be liable for any loss caused by or resulting from shortages claimed in the contents of the sealed or locked shipment(s) regarding manual drop deposits, for indirect, consequential or incidental damages or losses, non-performance or delays.  Likewise, we shall not be liable to you for failure to render service if in our judgment the same may endanger the safety of your property or personnel or our vehicles or employees.
              </li>
              <li>
                <B><u>Force Majeure.</u></B> It is further agreed that we shall not be held accountable or liable for any damages or losses, whether controlled or uncontrolled, and whether such loss be direct or indirect, proximate or remote, or be in whole or in part caused by, contributed to, or aggravated by the peril(s) for which liability is assumed by us, resulting from:
                <ol type='(1)'>
                   <li>
                     hostile or warlike action in time of peace or war, including action hindering, combating or defending against an actual, impending or expected attack; (i) by any government or sovereign power (de jure or de facto) or (ii) by any agent of any such government, power authority or forces;
                   </li>
                   <li>
                     nuclear reaction, nuclear radiation, radioactive contamination or any weapon of war, insurrection, rebellion, revolution, civil war, acts of terrorism, usurped power, or action taken by governmental authority; seizure or destruction under quarantine or customs regulations; confiscation by order of any governmental or public authority; or risks of contraband or illegal transportation or trade; or
                   </li>
                   <li>
                     acts of God, strikes, labor disturbances, while shipments are being transported by aircraft (including air piracy, explosion, crash or other incident on board the aircraft), impostor pick-up or deliveries, or other conditions or circumstances beyond our reasonable control.
                   </li>
                </ol>
              </li>
              <li>
                <B><u>Ownership.</u></B>  You expressly understand and accept that ownership (title) to cash transported or stored by us shall never transfer to us.
              </li>
            </ol>
          </li>
          <li>
            <B>DISPUTES</B>
            <TextBody>
              You and we agree that except for non payment, any controversy or claim, including any claim of misrepresentation, arising out of or related to this Agreement, or the furnishing of any service by us to you, shall be settled by arbitration under the then current rules of the American Arbitration Association.  The arbitrator shall be chosen from a panel of persons knowledgeable in the fields of financial institution security operations and armored car services.  You and we agree to equally share in the cost and fees of this resolution process.  The decision and award of the arbitrator shall be final and binding.  Judgment upon the award so rendered may be entered in any court having jurisdiction thereof.  Any arbitration hereunder shall be held in California.
            </TextBody>
          </li>
          <li>
            <B>WARRANTIES & REPRESENTATIONS</B>
            <TextBody>
              You acknowledge that we are not the manufacturer of the Equipment and further agrees that any and all warranties on the Equipment are limited to those warranties extended by manufacturer. The complete details of such warranty are available from your service representative.   The remedy above shall be the EXCLUSIVE remedy in the event of a breach of this warranty or in the event of damages, action, demand or fee arising from malfunction or latent defect of said Equipment, and it is expressly agreed that neither party shall be liable for special, incidental, indirect or consequential damages arising out of, or in any way connected with this Agreement.  THIS LIMITED WARRANTY EXCLUDES ALL OTHER WARRANTIES; EXPRESS OR IMPLIED, INCLUDING WARRANTS FOR MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, WHETHER OR NOT PURPOSES OR SPECIFICATIONS ARE DESCRIBED HEREIN.  WE FURTHER DISCLAIM ANY DAMAGE OR LOSS OF PROPERTY OR VALUE CAUSED BY EQUIPMENT WHICH HAVE BEEN THE SUBJECT OF MISUSE, ABUSE, NEGLIGENCE, OR USED IN VIOLATION OF ANY PRODUCT MANUALS, INSTRUCTIONS OR WARNINGS, OR MODIFIED REPAIRED OR SERVICED BY PERSONS NOT AUTHORIZED BY US OR THE MANUFACTURER, OR IMPROPERLY RELOCATED.  ALL OF OUR OBLIGATIONS UNDER THIS AGREEMENT SHALL BE VOID IF YOU ARE IN BREACH OF ANY OF THE TERMS AND CONDITIONS OF THIS AGREEMENT.
            </TextBody>
          </li>
          <li>
            <B>INDEMNITY</B>
            <TextBody>
              You agree to indemnify, defend and hold us harmless from all claims, costs or expenses arising out of any third party’s threatened or actual claim, suit, demand, garnishment or seizure of any funds or property provided by you hereunder that is in our custody due to a claim, demand or suit against you by such third party or through governmental seizure.  We agree to give you prompt notice of any such claim, suit, demand or seizure and to provide you reasonable cooperation on the defense.
            </TextBody>
          </li>
          <li>
            <B>INSURANCE</B>
            <TextBody>
              With regard to the Equipment provided hereunder, you shall procure, maintain and pay for: (a) all risk insurance against loss of and damage to the Equipment for not less than the full replacement value of the Equipment, naming us as loss payee and (b) combined public liability and property damage insurance with limits as approved by us, naming us as an additional insured.  The insurance shall be in such form and with such company or companies as shall be reasonably acceptable to us, shall provide at least thirty (30) days advance written notice to us of any cancellation, change or modification, and shall provide primary and non-contributory coverage for the protection of you and us without regard to any other coverage carried by you or us protecting against similar risks.  You shall provide us with an original policy or certificate evidencing such insurance.  You hereby appoint us as your attorney in fact with power and authority to do all things, including, but not limited to, making claims, receiving payments and endorsing documents, checks or drafts necessary or advisable to secure payments due under any policy of insurance required under this Agreement.  If you fail to purchase such insurance as required under this provision, we shall have the right, at our sole option, to acquire same at your sole expense and cost.  In case of your failure to procure or maintain said insurance, as more fully outlined below, or to pay fees, assessments, charges and taxes, all as specified in this Agreement, we shall have the right, but shall not be obligated, to effect such insurance, or pay said fees, assignments, charges and taxes, as the case may be.  In that event, the cost thereof shall be repayable to us with the next invoice, and failure to repay the same shall carry with it the same consequences as failure to pay any amount(s) due for service provided hereunder.
            </TextBody>
          </li>
          <li>
            <B>DEFAULT</B>
            <ol type="a">
              <li>
                If (i) we fail to perform any material obligation under this Agreement and such failure continues for sixty (60) days after we receive written notice from you specifying in reasonable detail the nature of that failure or (ii) we becomes the subject of a proceeding under the U.S. Bankruptcy Code, you may terminate this Agreement by giving written notice to us. Upon expiration or termination of this Agreement for any reason, all Equipment or materials, as applicable, provided by either party to the other in connection with the expired or terminated Services will be made available to us at the location within five (5) business days after the effective date of expiration or termination.  The termination rights set forth in this Section are cumulative and are in addition to all other rights and remedies available to the parties.
              </li>
              <li>
                If you terminate this Agreement, or its Agreement with us for cash and/or armored car service(s) with regard to such Equipment, prior to the end of the term provided therein, or fails to pay any invoices or other amount herein, or if you fail to observe, keep or perform any other provision of this Agreement required to be observed, kept or performed by you, we shall have the right to exercise any one or more of the following remedies: (a) to take possession of the Equipment, without demand or notice, wherever same may be located, without any court order or other process of law; (d) to terminate this Agreement; or (e) to pursue any other remedy at law or in equity.  You hereby waive any and all damages occasioned by the removal and taking of possession of the Equipment.  Notwithstanding any repossession or any other action which we may take, you shall be and remain liable for the full performance of all of your obligations to be performed under this Agreement.  All of our remedies are cumulative, and may be exercised concurrently or separately.
              </li>
            </ol>
          </li>
          <li>
            <B>MISCELLANEOUS</B>
            <ol type="a">
              <li>
                <B><u>Security Filings</u>.</B>  You hereby agree to execute any and all documents requested by us to perfect our security interest in the Equipment.  If we consent to a change of Location of the Equipment, you agrees to execute any further documentation necessary due to such location.  All costs and expenses associated with filing of our security interest in the Equipment shall be at the sole expense of us.
              </li>
              <li>
                <B><u>Bankruptcy.</u></B>  If any proceeding under the Bankruptcy Act, as amended, is commenced by or against you, or if you are adjudged insolvent, or if you make any assignment for the benefit of his creditors, or if a writ of attachment or execution is levied on the Equipment and is not released or satisfied within ten (10) days thereafter, or if a receiver is appointed in any proceeding or action to which you are a party, any Equipment provided hereunder shall not be treated as your asset.  The Equipment is, and shall at all times be and remain, the sole and exclusive property of us; and you shall have no right, title or interest therein or thereto except as expressly set forth in this Agreement.
              </li>
              <li>
                <B><u>Non–Warranty Service Calls and Your Routine Maintenance.</u></B> You shall be responsible for all non-warranty E-Deposit costs associated with any repair or service call; including but not limited to the following non-warranty issues: abuse, damage to cassettes due to mishandling, tube/coin jams, bill jams, screen protector damage or replacement, network or phone line related problems, damage due to spillage or infestation, equipment resets, phone fixable problems, user programming problems, equipment upgrades, printer jams or printer issues related to incorrect paper type; along with any repairs or service call arising out of your negligence, willful misconduct, or failure to perform any material obligation within this Agreement or normal preventative maintenance by you.  You shall also be responsible for the cost of any consumable items such as printer tape, bill trays, printer paper, cleaning cards, and screen protectors.
              </li>
              <li>
                <B><u>Confidentiality.</u></B> Each party receiving information (the "Receiving Party") undertakes to retain in confidence the terms of this agreement and all other non-public information, technology, materials and know-how of the other party disclosed or acquired by the Receiving Party pursuant to or in connection with this Agreement which is either designated as proprietary and/or confidential or, by the nature of the circumstances surrounding disclosure, ought in good faith to be treated as proprietary and/or confidential ("Confidential Information"). Neither party shall use any Confidential Information with respect to which it is the Receiving Party for any purpose other than to carry out the activities contemplated by this agreement.  Each party agrees to use commercially reasonable efforts to protect Confidential Information of the other party, and in any event, to take precautions at least as great as those taken to protect its own confidential information of a similar nature.  Each party shall also notify the other promptly in writing in the event such party learns of any unauthorized use or disclosure of any Confidential Information that it has received from the other party, and will cooperate in good faith to remedy such occurrence to the extent reasonably possible.
              </li>
              <li>
                <B><u>Amendment.</u></B> We may change this Agreement at any time. We may add new terms. We may delete or amend existing terms. We ordinarily send you advance notice of an adverse change to this Agreement. However, we may make changes without prior notice unless otherwise required by law. We may, but do not have to, notify you of changes that we make for security reasons or that we believe are either beneficial or not adverse to you. When we change this Agreement, the then-current version of this Agreement supersedes all prior versions and governs your account. If you continue to use your account or keep it open, you are deemed to accept and agree to the change and are bound by the change. If you do not agree with a change, you may close your account as provided in this Agreement.
              </li>
              <li>
                <B><u>Entire Agreement.</u></B> This Agreement: (a) shall be governed by and construed in accordance with the laws of the State of California without reference to conflict of laws principles; (b) constitutes the entire agreement and understanding of the parties with respect to its subject matter, except that the terms of any agreement regarding confidential information of the parties shall be deemed to be a part of this Agreement; (c) and the terms and conditions including fees set forth in it shall be treated as confidential information; (d) is not for the benefit of any third party; (e) may not be amended except by a written instrument signed by both you and us; (f) may not be assigned by you without our prior written consent; (fg) may be assigned by us, provided that we shall furnish written notice of such assignment to you; (gh) shall be binding upon any assignees, and defined terms used in this Agreement to apply to either party shall be construed to refer to such party’s assignee; (hi) is the product of negotiation; (ij) is subject to a contractually agreed one (1) year statute of limitations on all claims or the minimum allowable by applicable law; (jk)shall not be deemed to have been drafted by either party; (kl) may be executed in multiple counterparts, all of the same agreement which when taken together shall constitute one and the same instrument; (lm) contains article and section headings which are for convenience of reference only and which shall not be deemed to alter or affect the meaning or interpretation of any provision of this Agreement; (mn) does not make either party the agent, fiduciary or partner of the other; (no) does not grant either party any authority to bind the other to any legal obligation; (op) does not intend to nor grant any rights to any third party and (q) shall remain valid and enforceable despite the holding of any specific provision to be invalid or unenforceable, except for such specific provision.  The waiver by either party of any rights arising out of this Agreement shall not cause a waiver of any other rights under this Agreement, at law or in equity.  Any and all correspondence regarding this Agreement shall be delivered via certified mail (return receipt requested) or verifiable third-party courier (return receipt requested).
              </li>
            </ol>
          </li>
        </ol>
        <HeaderCenter>
          EXHIBIT A
        </HeaderCenter>
        <HeaderCenter>
          SERVICE SPECIFICATIONS
        </HeaderCenter>
        <HeaderLeft>
          ITEMS THAT REQUIRE PREVENTIVE MAINTENANCE
        </HeaderLeft>
        <TextBody>
          <B>- Bill Acceptors</B> – Remove validators and routinely clean minimum once a month or more based on cash volumes.
        </TextBody>
        <HeaderLeft>
          Non-Warrantee Calls
        </HeaderLeft>
        <TextBody>
          A maintenance call to repair a smartsafe that is caused by you or damaged experienced at your location shall NOT be covered under our maintenance program.  Some examples could be: damage to the display screen, vandalism, coin or other items being placed into the cash validators or any other deliberate misuse of the smartsafe equipment will be at your expense.
        </TextBody>
        <TextBody>
          <B>Holiday Service:</B>  We agree to provide service as stated in the Agreement with the following holiday exceptions:  New Year’s Day, Martin Luther King Day, President’s Day, Memorial Day, Independence Day, Labor Day, Veterans Day, Columbus Day, Thanksgiving Day, Christmas Day, federal banking and any local applicable observed holiday.  Charges for service on such days will be mutually agreed upon in advance by the parties on a per pick-up basis, excluding Christmas Day.  We will not provide Christmas Day service.
        </TextBody>
        <HeaderLeft>
          Cash Management Services:
        </HeaderLeft>
        Deposit Processing
        <ul>
            <li>
                You validated deposits will be processed at our local vault.
            </li>
            <li>
                Your manual deposits will be delivered to us for processing.
            </li>
            <li>
                Your reports will indicate Equipment’s deposit totals segregated by validator contents. It shall include the end of day totals and bank deposit totals.
            </li>
        </ul>
      </BodyWrapper>
    </CardBox>
  </ContainerWrapper>
);

SmartSafeAgreement.propTypes = {
  setBack: PropTypes.func.isRequired,
};

export default SmartSafeAgreement;
