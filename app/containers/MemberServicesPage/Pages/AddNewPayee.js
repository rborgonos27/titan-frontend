import React from 'react';
import titanLogo from 'images/titan-logo.png';
import addPayeeFeature from 'images/add_payee.png';
import generalPayeeScreen from 'images/general_payee_screen.png';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import PropTypes from 'prop-types';
import { Table, Image } from 'semantic-ui-react';
import {
  B,
  TextBody,
  BodyWrapper,
  HeaderLeft,
  HeaderCenter,
  Img,
  A,
  HR,
  HeaderColored,
} from './StyleUtils';

const rowRedStyle = {
  backgroundColor: 'red',
  color: '#FFF',
  textAlign: 'center',
};
const AddNewPayee = ({ setBack }) => (
  <ContainerWrapper>
    <CardBox>
      <Img src={titanLogo} />
      <A onClick={() => setBack()}>Back</A>
      <HR />
      <BodyWrapper>
        <HeaderCenter>Adding New Payee</HeaderCenter>
        {/* eslint-disable */}
        <TextBody>
          Payee Recipients must be verified prior to making payments.  Once Payees are reviewed by Titan Vault via Athena Pay, the payee will appear in Member’s Payee list.  Below are instructions, requirements and best practices to ensure timely payments are made.
        </TextBody>
        <HeaderLeft>Document Content:</HeaderLeft>
        <TextBody>
          ❖	Add Payee Feature <br />
          ❖	General New Payee Screen <br />
          ❖	Required Payee Information <br />
          ❖	Submitting a New Payee Request <br />
        </TextBody>
        <HeaderColored><u>Add Payee Feature</u></HeaderColored>
        <TextBody>
          <Image src={addPayeeFeature} centered />
        </TextBody>
        <HeaderColored><u>General Payee Screen</u></HeaderColored>
        <TextBody>
          <Image src={generalPayeeScreen} centered />
        </TextBody>
        <HeaderColored><u>Payee Fields and Information Required</u></HeaderColored>
        <TextBody>
          The following information is required when adding a new payee.  Please have the information available when requesting a new payee.
        </TextBody>
        <ol>
          <li>Payee Name (matching invoice or contract)</li>
          <li>Bank Name</li>
          <li>Bank Account Number</li>
          <li>Bank Account Name (same or Payee Name or if differ please enter here)</li>
          <li>
            Payee Email
            <ul>
              <li><i>(where a terms of conditions for receiving payment from Titan Vault document will be automatically sent to be signed)</i></li>
            </ul>
          </li>
          <li>Address</li>
          <li>Address 2 (optional)</li>
          <li>City</li>
          <li>State</li>
          <li>Zip Code</li>
          <li>Bank Account Routing Number</li>
        </ol>
        <TextBody>
          <div style={{ marginRight: 50, marginLeft: 50 }}>
            <B>NOTE:</B> If bank information is unknown or not available, please enter “0000000000” under mandatory (*) fields. Please be inform this may delay approval and payment for this new payee.  A Titan Officer will be in contact if incomplete information.
          </div>
        </TextBody>
        <HeaderColored><u>Submitting a New Payee Request</u></HeaderColored>
        <TextBody>
          Adding new payees may take up to 2 businesses days to be available so please allocated extra time.  Payee will appear on the payment list upon approval.
        </TextBody>
        <TextBody>
          NOTE: Payee/Recipient will be required to accept TV’s terms and condition (permitting the acceptance of the payment) received via an email from Titan Vault at the first payment request from Member.
        </TextBody>
        <TextBody>
          <Table celled>
            <Table.Header>
              <Table.Row style={  rowRedStyle}>
                <Table.Cell>PAYMENTS REQUEST BEST PRACTICES & THINGS TO KNOW</Table.Cell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell>
                  <ul>
                    <li>Payee List provided during onboarding process is not automatically added to the list upon initial access.  Supporting documentation such as the contracts or invoices related to the payee must be provided in order for final approval.</li>
                    <li>Debit payment will be posted to TV account 24 hours after scheduled payment date.</li>
                    <li>When making Edits, Changes or Updates to the Payee detail, it will automatically lock the page and submit a notification to the Titan Officer.  The changes will be quickly reviewed to ensure the updates are reflected in our system.</li>
                    <li>Contact memberservices@titan-vault.com if you have any questions or special circumstances for the particular payee recipient.</li>
                  </ul>
                </Table.Cell>
              </Table.Row>
              <Table.Row style={rowRedStyle}>
                <Table.Cell>
                  EACH MEMBERS SCENARIO MAY BE DIFFER. PLEASE CONTACT MEMBERSERVICES@TITAN-VAULT.COM IF THERE ARE ANY QUESTIONS.
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </TextBody>
      </BodyWrapper>
    </CardBox>
  </ContainerWrapper>
);

AddNewPayee.propTypes = {
  setBack: PropTypes.func.isRequired,
};
export default AddNewPayee;
