import React from 'react';
import titanLogo from 'images/titan-logo.png';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import PropTypes from 'prop-types';
import { Table } from 'semantic-ui-react';
import {
  B,
  TextBody,
  BodyWrapper,
  HeaderLeft,
  HeaderCenter,
  Img,
  A,
  HR,
  HeaderColored,
} from './StyleUtils';

const rowRedStyle = {
  backgroundColor: 'red',
  color: '#FFF',
  textAlign: 'center',
};
const ElectronicDeposit = ({ setBack }) => (
  <ContainerWrapper>
    <CardBox>
      <Img src={titanLogo} />
      <A onClick={() => setBack()}>Back</A>
      <HR />
      <BodyWrapper>
        <HeaderCenter>Electronic Deposit Handling Procedures</HeaderCenter>
        {/* eslint-disable */}
        <TextBody>
          Guidelines for all members preparing electronic deposits to Titan Vault (“TV”) via our affiliate Athena Pay (“Athena”) by your payors using Titan Vault Member portal. Follow preparation requirements and best practices to ensure timely deposits to your account.
        </TextBody>
        <HeaderLeft>Document Content:</HeaderLeft>
        <TextBody>
          ❖	Scheduling Electronic Deposit <br />
            <ul>➢	Member Deposit Entry <br /></ul>
          ❖	Titan’s Deposit Review <br />
          ❖	Availability of Funds <br />
          ❖	Instructions for Payor to deposit into Member Account
        </TextBody>
        <HeaderColored><u>Scheduling Deposit</u></HeaderColored>
        <TextBody>
          <ol>
            <li>
              <B>Member Deposit Entry </B>(via TV portal)
              <ol type="a">
                <li>Member must initiate deposit request in Titan’s Members portal; completion of deposit amount and scheduled payment date must be completed in order to be processed. </li>
                <li>Choose <u>“Electronic”</u>   for the deposit type.</li>
                <li>Enter the <u>estimated deposit date</u> that funds may be deposited by your payor into your Titan Vault account.  Selecting Immediate means funds will be deposited same business day.</li>
                  <ol type="i">
                    <li>
                      <B>Note ALL final completion date will be updated based on actual day funds are received by Titan Vault.</B>
                    </li>
                  </ol>
                <li>Enter the <u>amount</u> to be deposited.</li>
                <li>Member provide supporting invoice or relevant documentation related to your intended payment deposit from the payor.
                  <B>Deposits may not be accepted if no supporting documentation is provided.  Please speak with a Titan Officer prior to scheduling deposit should you have any questions</B>
                </li>
              </ol>
            </li>
          </ol>
        </TextBody>
        <HeaderColored><u>Titan Deposit Review</u></HeaderColored>
        <TextBody>
          <ol>
            <li><B>Deposit Review Process </B>(via TV portal)</li>
            <ol type="a">
              <li>All deposit request made in TV will be reviewed by a Titan Officer.</li>
              <li>
                Deposit transaction detail will show the following statuses:
                <Table celled>
                  <Table.Header>
                    <Table.Row>
                      <Table.Cell>STATUS</Table.Cell>
                      <Table.Cell>DESCRIPTION</Table.Cell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell>
                        REQUESTED
                      </Table.Cell>
                      <Table.Cell>
                        Request submitted to Titan for review and approval
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>
                        PROCESSING
                      </Table.Cell>
                      <Table.Cell>
                        Titan reviewed and have approve request. Awaiting deposit from payor.
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>
                        COMPLETED
                      </Table.Cell>
                      <Table.Cell>
                        TV confirms funds have been received.
                        TV Credit to Member Titan Vault account.
                      </Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
              </li>
            </ol>
          </ol>
        </TextBody>
        <HeaderColored><u>Availability of Funds</u></HeaderColored>
        <TextBody>
          Members’ TV account will be credited immediately upon confirmation of funds received in Titan Vault’s Custodial account via affiliate Athena Pay, LLC.
        </TextBody>
        <HeaderColored><u>Instruction to Payor for all Incoming Deposit</u></HeaderColored>
        <TextBody>
          It is the Member’s responsibility to provide the following deposit instructions to all payors.  Details below must be included in the deposit memo to ensure deposit is credit to Member account timely.
        </TextBody>
        <HeaderLeft>
          Bank Deposit Detail
        </HeaderLeft>
        <TextBody>
          <ul>
            <ul>❏	Account Name:  Athena Pay</ul>
            <ul>❏	Account No: ---------------------</ul>
            <ul>❏	Routing No:  321177573</ul>
            <ul>❏	Bank Institution Name/ID: NB</ul>
            <ul>❏	Memo must include : “MEMBERS NAME” C/O TITAN “ (Eg. Green Farm c/o Titan – invoice 344563)</ul>
          </ul>

          <Table celled>
            <Table.Header>
              <Table.Row style={  rowRedStyle}>
                <Table.Cell>DEPOSIT BEST PRACTICES & THINGS TO KNOW</Table.Cell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell>
                  <ul>
                    <li>Providing Supporting document for deposits from Member’s payors to each deposit request is recommended to avoid delays.  Failure to do may result in a delay. Contact memberservices@titan-vault.com if you have any questions or require prior approval.</li>
                    <li>Member’s payors must include Member’s business name on the memo to ensure timely credit.</li>
                    <li>Electronic deposits will be credited to Titan Member immediately upon receipt of payment in Titan Vault’s affiliate partner Athena Pay.</li>
                    <li>TV/Athena Pay has multiple banking partners. Each Members has a designated banking partner working with TV.  Deposit instruction may differ for every TV MembeRs.</li>
                    <li>Bank Deposit detail may change; if such changes happen, TV will notify by providing written notice Members 30 days prior to any changes.</li>
                  </ul>
                </Table.Cell>
              </Table.Row>
              <Table.Row style={rowRedStyle}>
                <Table.Cell>
                  EACH MEMBERS WILL RECEIVE AN ELECTRONIC DEPOSIT UPON ACCOUNT OPENING. (EXAMPLE. APPENDIX A) NEXT PAGE
                  <br /> <br />
                  FWD TO YOUR PAYORS FOR ELECTRONIC DEPOSITS
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </TextBody>
        <br />
        <br />
        <HeaderCenter>
          <u>APPENDIX A - SAMPLE</u>
        </HeaderCenter>
        <HeaderCenter>DEPOSIT INSTRUCTIONS</HeaderCenter>
        <TextBody>
          <Table celled>
            <Table.Header>
              <Table.Row style={rowRedStyle}>
                <Table.Cell colspan="2">
                  ACH/WIRE INSTRUCTIONS
                </Table.Cell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell>
                  Account Name:
                </Table.Cell>
                <Table.Cell>
                  <B>Athena Pay</B>
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  Account Number
                </Table.Cell>
                <Table.Cell>
                  <B>83600010692010</B>
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  Routing Number
                </Table.Cell>
                <Table.Cell>
                  <B>321177573</B>
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  Bank Institution Name/ID
                </Table.Cell>
                <Table.Cell>
                  <B>NB</B>
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  <B>
                    Memo must include: PAY TO:
                  </B>
                </Table.Cell>
                <Table.Cell>
                  “<span style={{ backgroundColor: 'yelow'}}>MEMBERS NAME” C/O TITAN </span>“ <br /> <br />

                  (Eg. Green Farm c/o Titan – invoice 344563)
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
          <br />
          <br />
          <Table celled>
            <Table.Header>
              <Table.Row style={rowRedStyle}>
                <Table.Cell>
                  MEMBER ACCOUNT
                </Table.Cell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell>
                  GREEN FARM CO.
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>
                  83600010692010
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </TextBody>
      </BodyWrapper>
    </CardBox>
  </ContainerWrapper>
);

ElectronicDeposit.propTypes = {
  setBack: PropTypes.func.isRequired,
};
export default ElectronicDeposit;
