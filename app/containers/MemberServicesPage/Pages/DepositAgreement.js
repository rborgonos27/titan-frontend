import React from 'react';
import titanLogo from 'images/titan-logo.png';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import PropTypes from 'prop-types';

import {
  B,
  SubHeader,
  TextBody,
  BodyWrapper,
  HeaderPlain,
  HeaderLeft,
  HeaderCenter,
  Img,
  A,
  BigHeader,
  HR,
} from './StyleUtils';

const DepositAgreement = ({ setBack }) => (
  <ContainerWrapper>
    <CardBox>
      <Img src={titanLogo} />
      <A onClick={() => setBack()}>Back</A>
      <HR />
      <BodyWrapper>
        <HeaderCenter>Deposit Agreement and Disclosures</HeaderCenter>
        <TextBody>
          This <i>Deposit Agreement and Disclosures,</i> the applicable
          <i> Schedule of Fees</i>, the signature card and other account opening
          documents for your account are part of the binding contract between
          you and us for your deposit account and your deposit relationship with
          us. They contain the terms of our agreement with you. Please read all
          of these documents carefully.
        </TextBody>
        <TextBody>
          This <i>Deposit Agreement and Disclosures</i> also summarizes certain
          laws and regulations that apply to common transactions, provides some
          disclosures for deposit accounts required by federal law, and
          establishes terms that cover some transactions or situations that the
          law either does not cover or allows us to change by this contract. The
          <i> Schedule of Fees</i> lists our accounts and account fees.
        </TextBody>
        {/* eslint-disable */}
        <TextBody>
          When you complete our account opening documents, request an
          account, or keep your account open, you acknowledge that you have
          reviewed and understand the terms of this Agreement and you agree
          to be governed by these terms. You understand that these terms, as
          we may change or supplement them periodically, are a binding
          contract between you and us for your deposit account and your
          deposit relationship.
        </TextBody>
        <TextBody>
          Our deposit relationship with you is that of debtor and creditor.
          This Deposit Agreement and the deposit relationship does not
          create a fiduciary, quasi-fiduciary or special relationship
          between us. We owe you only a duty of ordinary care. Our internal
          policies and procedures are solely for our own purposes and do not
          impose on us a higher standard of care than otherwise would apply
          by law without such policies or procedures.
        </TextBody>
        <HeaderLeft>Changes to This Agreement</HeaderLeft>
        <TextBody>
          We may change this Agreement at any time. We may add new terms. We
          may delete or amend existing terms. We may add new accounts and
          services and discontinue existing accounts or services. We may
          convert existing accounts and services into new accounts and
          services.
        </TextBody>
        <TextBody>
          We ordinarily send you advance notice of an adverse change to this
          Agreement. However, we may make changes without prior notice
          unless otherwise required by law. We may, but do not have to,
          notify you of changes that we make for security reasons or that we
          believe are either beneficial or not adverse to you.
        </TextBody>
        <TextBody>
          When we change this Agreement, the then-current version of this
          Agreement supersedes all prior versions and governs your account.
        </TextBody>
        <TextBody>
          If you continue to use your account or keep it open, you are
          deemed to accept and agree to the change and are bound by the
          change. If you do not agree with a change, you may close your
          account as provided in this Agreement.
        </TextBody>
        <TextBody>
          See the <i>Notices, Statements and Other Communications</i> section for information about how we provide notice.
        </TextBody>
        <HeaderLeft>Closing an Account</HeaderLeft>
        <TextBody>
          You or we may close your account at any time without advance
          notice, except that we may require you to give us seven days
          advance notice when you intend to close your account by
          withdrawing your funds. Titan Vault may close your account or
          convert your account to another account type at its discretion due
          to excessive overdrafts.
        </TextBody>
        <TextBody>
          Sometimes after an account is closed, we receive a deposit for
          credit to the account or other item for payment from the account.
          If this happens, we may at our option and without any liability to
          you: either return the deposit, or other item; or we may reopen
          the account and accept the deposit, or other item for you, even if
          this overdraws your account.
        </TextBody>
        <TextBody>
          Sometimes after an account which had funds in it is closed, and
          while we are still holding the funds from the account, we receive
          a withdrawal request, or other item for payment from the account.
          We may refuse the withdrawal request and return the other item. We
          are not liable for any losses or damage that may result from
          refusing the withdrawal or dishonoring the other item, even if we
          are still holding funds that would cover the withdrawal or other
          item.
        </TextBody>
        <TextBody>
          When you ask us to close your account, we may continue to pay
          transactions as we receive them while we process your closure
          request. When we complete our closure process, we may close your
          account, even if your account has a balance and transactions
          you’ve told us about are still pending.
        </TextBody>
        <TextBody>
          If your account is overdrawn when closed, you agree to pay
          immediately all amounts you owe us. If your account had funds in
          it when closed, we may:
        </TextBody>
        <TextBody>
          <ul>
            <li>
              hold the funds for your pick up or to pay outstanding or
              expected items or claims; or
            </li>
            <li>deposit the funds in another of your accounts with us.</li>
          </ul>
        </TextBody>
        <TextBody>
          This Agreement continues to govern matters related to your account
          even after your account closes.
        </TextBody>
        {/* Explanation of Some Terms */}
        <HeaderLeft>Explanation of Some Terms</HeaderLeft>
        <HeaderPlain>Definitions</HeaderPlain>
        <TextBody>
          Please keep in mind the following definitions as you review the
          Agreement.
        </TextBody>
        <TextBody>
          <B>Titan Vault, we, us</B> and <B>our</B> mean Titan Vault, LLC.<br />
          <B>Financial Center</B> means a branch of Titan Vault.<br />
          <B>Business days - </B> our business days are Monday through
          Friday, excluding bank holidays. Hours of the business day for a
          financial center are available at that financial center.<br />
          <B>Item</B> includes all orders and instructions for the payment,
          transfer or withdrawal of funds from an account. As examples, item
          includes: an electronic transaction, draft, demand draft,
          pre-authorized payment, automatic transfer, online transfer or
          bill payment instruction, withdrawal slip, in-person transfer or
          withdrawal, cash ticket, deposit adjustment, or other order of
          instruction for the payment, transfer, or withdrawal of funds, or
          an image, digital image or a photocopy of any of the foregoing.
          Item may also include a cash-in ticket and a deposit adjustment.
          Item may also include a draft, warrant, or other item deposited to
          your account, including a deposited item that was returned unpaid.<br />
          <B>You</B> and <B>your</B> means each and every owner of the
          account and anyone else with the authority to deposit, withdraw,
          or exercise control over the funds in the account.
        </TextBody>
        <HeaderPlain>Headings and Interpretation</HeaderPlain>
        <TextBody>
          We include section and paragraph headings in this Agreement to
          help you find terms and provisions. The headings are for
          convenience or reference only. They do not limit the term or
          provision.<br />
          Unless it would be inconsistent to do so, words and phrases used
          in this document should be construed so the singular includes the
          plural and the plural includes the singular.<br />
          In some sections we give examples. The examples cover some, but
          not all, of the situations or items that are covered by the
          section.
        </TextBody>
        <HeaderLeft>Information About You and Your Account</HeaderLeft>
        <HeaderPlain>Information You Give Us</HeaderPlain>
        <TextBody>
          When you open a deposit account with us, you give us information
          about yourself and confirm that it is correct. We enter the
          information into our records. We may rely on that information
          until you notify us of a change and we have had a reasonable time
          to act on the new information.
        </TextBody>
        <HeaderPlain>Identification</HeaderPlain>
        <TextBody>
          Federal law, including the USA PATRIOT Act, requires all financial
          institutions to obtain, verify and record information that
          identifies each customer who opens an account with that financial
          institution.
        </TextBody>
        <TextBody>
          When you apply for an account, we will ask for your
          legal name, address, date of birth and your Tax Identification
          Number (TIN). We may require one or more forms of unexpired photo
          identification. We may validate the information you provide to us
          to ensure we have a reasonable assurance of your identity. We may
          contact you for additional information. If your account is funded
          before we verify your information, you may not have access to your
          funds. If we are not able to verify your identity to our
          satisfaction, we will not open your account or we may close the
          account if it was previously funded.
        </TextBody>
        <HeaderPlain>Sharing Information with Affiliates</HeaderPlain>
        <TextBody>
          <B>Accounts Held by Consumers</B> We may share information that we
          have about you and your accounts among the Titan Vault family of
          companies.<br />
          <B>Accounts Held by Businesses</B> We may share information about
          our experiences with you with Zeuss Technologies, Inc. and its
          subsidiaries and affiliated companies (“Titan Vault Affiliates”)
          and selected third parties. We may also share information that you
          have provided to us on applications or that we receive from
          outside sources among the Titan Vault Affiliates.
          <SubHeader>Consumer Reports and Other Inquiries</SubHeader>
          We may make any inquiries that we consider appropriate to help us
          verify your identity and determine if we should open, maintain,
          ollect or close your account. This may include verification of
          employment and consumer reports or other reports from account
          information services and other consumer reporting agencies. If you
          ask, we will tell you whether we requested such a report and, if
          we did request a report, we will tell you the name, address and
          telephone number of the reporting agency.
          <SubHeader>
            Disclosing Information About You and Your Account
          </SubHeader>
          We may disclose information about your accounts to consumer
          reporting agencies and to other persons or agencies who, in our
          judgment, have a legitimate purpose for obtaining information,
          including our affiliates.<br />
          For example, subject to any applicable financial privacy laws or
          other laws or regulations, we may provide information on you and
          your accounts:
          <ul>
            <li>to consumer reporting agencies;</li>
            <li>
              to anyone who we reasonably believe is conducting a legitimate
              credit inquiry, including inquiries to verify the existence or
              condition of an account for a third party such as a lender,
              merchant or consumer reporting agency;
            </li>
            <li>
              in response to any subpoena, summons, court or administrative
              order, or other legal process which we believe requires our
              compliance;
            </li>
            <li>
              in connection with the collection of indebtedness or to report
              losses incurred by us;
            </li>
            <li>
              in compliance with any agreement between us and a
              professional, regulatory or disciplinary body;
            </li>
            <li>in connection with potential sales of businesses;</li>
            <li>
              to service providers who help us meet your needs by assisting
              us in providing or offering our products or services; and
            </li>
            <li>
              to other third parties as is described in our publication U.S.
              Consumer Privacy Notice or as required under applicable law or
              regulation.
            </li>
          </ul>
          <SubHeader>
            Account Information Services/Consumer Reporting Agencies
          </SubHeader>
          If we close your account because of your unsatisfactory handling,
          we generally report to consumer reporting agencies such as
          ChexSystems, Inc. your name, address, Taxpayer Identification
          Number (TIN), driver’s license number and the date and reason we
          closed the account. The consumer reporting agency may supply this
          information to others. This may adversely impact your ability to
          establish an account at any financial institution for up to five
          years from the date of the report. We may report information about
          your account to credit bureaus. Late payments, missed payments or
          other defaults on your account may be reflected in your credit
          report.
          <SubHeader>
            Telephone Calls: Calling, Monitoring and Recording
          </SubHeader>
          When you give a telephone number directly to us, or place a
          telephone call to us, you authorize us to place calls to you at
          that number. You understand that a “telephone number” includes a
          cell phone number and “calls” include both telephone calls and
          text messages to or from your phone or cell phone. As examples, we
          may place calls to you about fraud alerts, deposit holds, and
          amounts you owe us (collection calls) on your account. When we
          place calls to you, we may use automatic dialers and artificial,
          text, or prerecorded messages. You authorize us to monitor, and to
          record, telephone conversations and other electronic
          communications you have with us and with our representatives for
          reasonable business purposes, including security and quality
          assurance. We will not remind you that we may be monitoring or
          recording a call at the outset of the call unless required by law
          to do so. You consent and agree in advance to these terms and
          conditions.
          <SubHeader>Release of Information</SubHeader>
          You can obtain information about your account by many methods,
          including at a financial center, by telephone, by mail and our
          website. We believe we have adopted reasonable security measures
          for each method, but we cannot ensure against unauthorized
          inquiries or intrusions. You agree that we are not responsible for
          the release of information to anyone who has gained possession of
          your authorization code or access device or who has learned your
          identifying characteristics such as personal identification number
          (PIN), account number or social security number, even if you have
          not authorized them to obtain the information.<br />
        </TextBody>
        <HeaderLeft>Account Ownership</HeaderLeft>
        <TextBody>
          <SubHeader>Some General Terms</SubHeader>
          When you open an account, we may rely on information you give us
          and we maintain in our records. We determine the type and
          ownership of the account from this information. When you ask us to
          make a change to this information or your account, and we agree to
          the change, the change is not effective until we have had a
          reasonable time to act on the new information. As an example, if
          you ask us to change the signers on your account, your requested
          change is not effective until we have a reasonable time to act on
          it. If we ask you to give us additional documents or information,
          and you do not do so promptly, we may close your account.<br />
          When we accept a deposit to an account or permit a withdrawal or
          payment from an account, we may rely upon the form of the account
          and the terms of this Agreement at the time we process the
          transaction. We do not have to inquire about the source or
          ownership of any funds we receive for deposit or about the
          application of any withdrawal or payment from an account. When we
          permit a withdrawal or payment from an account at the request of
          any signer, or the agent of any signer, in accordance with the
          terms of this Agreement, the withdrawal or payment is a complete
          release and discharge of us from all claims regarding the
          withdrawal or payment.<br />
          If you instruct us to open an account in the names of two or more
          people, and we do so, but later determine that one or more of them
          have not completed our account opening documents or other
          requirements, you agree to hold us harmless for reliance on your
          instruction. We may in our discretion for all purposes and
          circumstances (including determining ownership of the account
          following the death of any person in whose name the account was
          opened) either treat the account as being owned by all persons in
          whose names the account was opened or treat the account as being
          owned solely by the persons who have signed or completed our
          account opening documents or other requirements. If we treat the
          account as owned by all persons in whose names the account was
          opened, we may permit the non-signing person to withdraw funds or
          take other action on the account without any liability to you.<br />
          We may open an account without regard to whether you are married
          and without regard to whether the funds on deposit are your
          community or separate property. We may require you to close the
          account in order to remove a co-owner, terminate a joint ownership
          or change a payable on death or trust designation.
          <SubHeader>Some Basic Terms for Joint Accounts</SubHeader>
          If more than one person’s name appears in the title of an account
          without a fiduciary, beneficiary or other designation, then the
          account is a joint account. All persons whose names appear on the
          account are co-owners of the account, regardless of whose money is
          deposited in the account.<br />
          Each co-owner acts as the agent of each other co-owner. Each
          co-owner authorizes each other co-owner to operate the account
          without the consent or approval of any other co-owner. We may act
          and rely on the instructions of one co-owner without liability to
          any other co-owner. So as examples, one co-owner may without the
          consent or approval of the others: <br />
          <ul>
            <li>add additional persons as co-owners;</li>
            <li>
              deposit funds and withdraw or transfer part or all of the
              funds in the account;
            </li>
            <li>
              endorse for deposit to the joint account on behalf of any
              other co-owner an item payable to another co-owner;
            </li>
            <li>
              instruct us to stop payment on an item that another co-owner
              wrote on the account;
            </li>
            <li>
              draw upon an overdraft or other line of credit connected to
              the account;
            </li>
            <li>
              obtain information about the account, including transactions
              conducted by other co-owners;
            </li>
            <li>pledge the account as security for any debts; and</li>
            <li>close the account.</li>
          </ul>
          Each co-owner is jointly and severally liable to us for all fees,
          charges and other amounts owed to us on, and all costs, losses and
          liabilities related to, this Agreement or the account. Note that
          our right of setoff described in the Right of Setoff section of
          this Agreement applies to joint accounts.<br />
          All joint accounts are presumed to be joint accounts with the
          right of survivorship, unless the applicable state law does not
          permit this presumption or we have agreed with you in writing that
          the account is owned in another capacity.
          <B>
            Right of survivorship means that when a co-owner dies, the funds
            in the account belongs to the surviving co-owner(s),
          </B>
          subject to our right to charge the account for any amount the
          deceased co-owner or a surviving co-owner owes us. The rights of
          survivorship continue between surviving co-owners and we may pay
          the funds in the account to any surviving co-owner. The applicable
          state law may impose requirements that must be met to create a
          joint account with right of survivorship. You are solely
          responsible for meeting these requirements.<br />
          <SubHeader>
            Some Basic Terms for “Payable on Death” Accounts
          </SubHeader>
          For an individual or joint account, you may choose to make your
          account payable on your death to one or more payable on death
          (“POD”) beneficiaries. You can make your account a POD account by
          instructing us to list each POD beneficiary on the account and
          complying with the applicable state law. The applicable state law
          usually imposes requirements that must be met to create a POD
          account. As an example, you may have to include certain words or
          letters in the account title to create a POD account, such as:
          “payable on death,” “POD,” “in trust for,” “ITF,” “as trustee
          for,” “ATF,” “transfer on death,” “TOD,” or “Totten Trust.” You
          are solely responsible for meeting these requirements. We may
          treat an account which names a POD beneficiary as a POD account.
          However, if the applicable requirements are not met, we may treat
          your account as though there is no POD beneficiary.<br />
          During your lifetime, a POD account belongs to you. You may close
          the account, remove or add one or more POD beneficiaries, change
          the account type or ownership, and withdraw all or part of the
          funds in the account. When the account owner or last co-owner
          dies, we may pay any funds remaining in the account to the then
          surviving (if any) POD beneficiary(ies), subject to our right to
          charge the account for any amount a deceased owner, co-owner or
          POD beneficiary owes us. We may distribute the account balance,
          subject to any bank claims, to such beneficiaries payable to one
          or all surviving beneficiaries jointly, or payable individually,
          in equal shares, to each surviving beneficiary. A POD beneficiary
          does not acquire an interest in the account until after the death
          of the account owner or the last co-owner. A POD beneficiary may
          acquire an interest in the account at that time but only if the
          POD beneficiary is alive.
          <SubHeader>
            Some Basic Terms for Business and Other Non-Personal Accounts
          </SubHeader>
          If the account owner is a corporation, unincorporated association,
          limited liability company, limited liability partnership,
          fiduciary, partnership, sole proprietorship or other entity
          holding an account in any capacity other than an individual
          capacity, each person signing the signature card or completing
          other account opening requirements represents and agrees that
          they:
          <ul>
            <li>
              are fully authorized to execute all documents or otherwise
              complete our requirements in their stated capacity;
            </li>
            <li>
              have furnished all documents or other information necessary to
              demonstrate that authority; and
            </li>
            <li>
              will furnish other documents and complete other requirements
              as we may request from time to time.
            </li>
            <li>
              We may refuse to recognize any resolution affecting the
              account that is not on our form or that appears to us to be
              incomplete or improperly executed.
            </li>
          </ul>
          <SubHeader>Transfering Ownership</SubHeader>
          Your account is for your use only. It is non-transferable and
          non-negotiable. Ownership of your account is transferable only on
          our records with our consent.
          <ul>
            <li>
              You may not grant, transfer or assign any of your rights to
              your account without our written consent.
            </li>
            <li>
              Even if we consent, we may require that you close the account
              and that the new account owner open a new account in their
              name.
            </li>
            <li>
              We may refuse to acknowledge or accept your attempted pledge
              or assignment of your account or any interest in it, including
              a notice of security interest.
            </li>
          </ul>
        </TextBody>
        <HeaderLeft>Accounts</HeaderLeft>
        <TextBody>
          <SubHeader>Demand Deposit Accounts</SubHeader>
          The <i>Schedule of Fees</i> describes our business accounts and
          lists applicable fees. <br />
          Your account is a demand deposit account. All types of customers
          can open a demand deposit account. Most demand deposit accounts do
          not earn interest. We do not offer an interest bearing demand
          deposit account to customers.
        </TextBody>
        <HeaderLeft>
          Information About Fees and Charging Your Account
        </HeaderLeft>
        <TextBody>
          <B>Fees</B> You agree to pay for our services in accordance with
          the fees that apply to your account and your deposit relationship
          with us. <br />
          <B>Account Fees</B> Your account is subject to the fees described
          in the Schedule of Fees that applies to your account.
          <ul>
            <li>
              The <i>Business Schedule of Fees</i> lists account fees that
              apply to our business deposit accounts.
            </li>
            <li>
              The schedule that applies to your account is part of the
              binding contract between you and us.
            </li>
          </ul>
          Your account fees and terms may differ from those of other
          customers with the same type of account, based on our assessment
          of your overall relationship with us.<br />
          <B>Fees for Other Services</B> We also offer other services. You
          can get current information about these services and the fees that
          apply to them at a financial center or by calling us at the
          customer service number. We may occasionally list fees for some of
          these services in the Schedule of Fees. Fees for these services
          may vary. We may change these fees at any time without notice.<br />
          <B>How We Set Fees</B> We set our fees based on many factors, including
          the value we offer, our competitive position, deterrence of misuse
          of an account by our customers, consideration of profit and the safety
          and soundness of the storage facilities. We may also consider costs in
          setting fees, but we do not set our fees based only or primarily on
          the direct or overall costs and expenses associated with providing
          the particular account or service involved.<br />
          <B>Calculating Balances</B> When we calculate an account balance or
          combined balance to determine whether a fee applies to your account,
          we may use the balance that we determine is in each account.
          We may ignore funds subject to a hold of any type.
          If a loan or line of credit is linked, we may ignore
          each loan or line of credit that we determine is in default.<br />
          <SubHeader>Charging an Account</SubHeader>
          We may deduct fees, overdrafts and other amounts you owe us under this Agreement from
          your accounts with us or our affiliates. We may make these deductions at any time without
          prior notice to you or request from you. If there are not enough funds in your account
          to cover the amounts you owe us, we may overdraw your account, without being liable
          to you. You agree to pay immediately all fees, overdrafts and other amounts you owe us.<br />
          We may use deposits you or others make to your account (including deposits of payroll
          and government benefits) to pay fees, overdrafts and other amounts you owe us. <br />
          Some government payments (such as Social Security, Supplemental Security Income,
          Veterans and other federal or state benefits) may be protected from attachment,
          levy, garnishment or other legal process under federal or state law.
          If such protections would otherwise apply to deductions we make for amounts you
          owe us, to the extent that you may do so by contract, you waive these protections
          and agree that we may use these funds to pay fees, overdrafts and other amounts
          you owe us under this Agreement.<br />
          Please see the Right to Setoff section of the Agreement for more information.<br />
        </TextBody>
        <HeaderLeft>Insufficient Funds - Overdrafts and Returned Items</HeaderLeft>
        <TextBody>
          You can avoid fees for overdrafts and declined or returned items by making sure
          that your account always contains sufficient available funds to cover all of your transactions.
          <SubHeader>Overdrafts and Declined or Returned Items</SubHeader>
          When we determine that you do not have enough available funds in your account to cover an item,
          then we consider the item an insufficient funds item. Without notice to you, we either
          authorize or pay the insufficient funds item and overdraw your account (an overdraft item)
          or we decline or return the insufficient funds item without payment (a returned item).<br />
          We pay overdrafts at our discretion, which means we do not guarantee that we will always,
          or ever, authorize and pay them. If we overdraw your account to pay items on one or more occasions,
          we are not obligated to continue paying future insufficient funds items. We may pay all, some, or
          none of your overdrafts, without notice to you. If we do not authorize and pay an overdraft, then
          we decline or return the transaction unpaid.<br />
          The Schedule of Fees for your account explains when we charge you fees for overdrafts and for declined
          or returned items and the dollar amount of the fees. Please review the Schedule of Fees for your account carefully.<br />
          If we overdraw your account, you agree to repay us immediately, without notice or demand from us. We ordinarily
          use deposits you or others make to your account to pay overdrafts, fees and other amounts you owe us.<br />
          <SubHeader>Impact of Holds</SubHeader>
          Sometimes funds in your account are not available to cover your payments and other items. When we determine that
          funds in your account are subject to a hold, dispute, or legal process, then these funds are not available to cover your items. <br />
          It is important to know that your available funds may change between the time you authorize a transaction and when the transaction is paid.<br />
          A hold immediately reduces the amount of available funds in your account by the amount of the authorization request plus any fees.
          If, while the hold is in place, you do not have enough available funds in your account to cover other transactions you may have
          conducted, those items may overdraw your account or be returned unpaid. <br />
          <i>What are “items”?</i> Items include all orders and instructions for the payment,
          transfer, or withdrawal of funds from your account. As examples, item includes an ACH
          transaction, preauthorized payment, automatic transfer, transfer or bill payment instruction,
          transfer or withdrawal instruction. For more examples, please review the definition of items
          in the Explanation of Some Terms section. <br />
          <SubHeader>Posting Orders</SubHeader>
          We determine the order in which we process and post deposits and other credits and
          other items to your account. We may pay or authorize some items, and decline or return others,
          in any order we deem appropriate. When you do not have enough available funds to cover all of
          the items presented that day, some processing and posting orders can result in more insufficient
          funds items and more overdraft and returned item fees than other orders. We may choose our processing
          and posting orders regardless of whether additional fees result. <br />
          Please see the <i>Processing and Posting Orders</i> section for more information.
        </TextBody>
        <HeaderLeft>
          Restrictions and Responsibilities
        </HeaderLeft>
        <TextBody>
          You will not, directly or indirectly: reverse engineer, decompile, disassemble or otherwise attempt to
          discover the source code, object code or underlying structure, ideas, know-how or algorithms relevant
          to the software, documentation or data (“Software”); modify, translate, or create derivative works based
          on the Software (except to the extent expressly permitted by us); use the Software for the benefit of a
          third party; or remove any proprietary notices or labels. <br />
          You will be responsible for obtaining and maintaining any equipment and ancillary services needed to
          connect to, access or otherwise use the Software, including, without limitation, modems, hardware, servers,
          software, operating systems, networking, web servers and the like (collectively, “Equipment”).
          You will also be responsible for maintaining the security of the Equipment, and for all uses of the
          Equipment with or without your knowledge or consent.<br />
          You agree that you will (a) be responsible for the legality of any data you provide to us, and (b)
          use reasonable efforts to prevent unauthorized access to your accounts, and utilize best efforts to
          notify us promptly of any such unauthorized access or use of your account or personnel.<br />
          You agree that you will not (a) make any our service available to, or use any of our services for
          the benefit of, anyone other than you, (b) sell, resell, license, sublicense, distribute, rent or
          lease our Software, (c) intentionally use our Software to store or transmit malicious code,
          (d) intentionally disrupt the performance of our service, or (e) intentionally attempt to
          gain unauthorized access to our service. <br />
        </TextBody>
        <HeaderLeft>Non-Titan Products and/or Services</HeaderLeft>
        <TextBody>
          We (or third parties we use) may link to or interoperate with third-party products or services, including, for example, but not limited to, Quickbooks (“Non-Titan Products and/or Services”). Any acquisition by you of such Non-Titan Products and/or Services, and any exchange of data between you and any Non-Titan Products and/or Services, is solely between you and the applicable Non-Titan Products and/or Services provider. You acknowledge that access to Non-Titan Products and/or Services or other products or services is dependent on you acquisition of such Non-Titan Products and/or Services. We do not warrant or support Non-Titan Products and/or Services, whether or not they are designated by us as “certified” or otherwise.  Other than our warranty obligations or support obligations, we do not guarantee that our services will function or integrate with Non-Titan Products and/or Services.
        </TextBody>
        <TextBody>
          If you install or enable a Non-Titan Product and/or Service, you grant us permission to allow the provider of that Non-Titan Product and/or Service to link to and access your data as required for the interoperation of that Non-Titan Product and/or Service with our service. We are not responsible for any disclosure, modification, transmission or deletion of your data resulting from access by a Non-Titan Product and/or Service.
        </TextBody>
        <TextBody>
          Our services may contain features designed to interoperate with Non-Titan Products and/or Services. To use such features, you may be required to obtain access to Non-Titan Products and/or Services from their providers, and may be required to grant us access to your account(s) on the Non-Titan Products and/or Services. If the provider of a Non-Titan Product and/or Service ceases to make the such product or service available for interoperation with the corresponding product or service features on reasonable terms, we may cease providing related features without penalty.
        </TextBody>
        <TextBody>
          Your Data <br />
          You grant to us and our affiliates a license to access your data, networks and systems for the sole purpose of providing our services (including, but not limited to customer service, diagnostic and corrective actions for your benefit) to host, copy, transmit, analyze and display your data. You grant to us and our affiliates an irrevocable, worldwide royalty-free license to use data created from your data to the extent permitted by law.
        </TextBody>
        <TextBody>
          Upon your request, we will make your data available to you in a manner reasonable to us. After the termination of this Agreement, we will have no obligation to maintain or provide your data, and may in our sole discretion keep, delete or destroy all copies of your data in our systems or otherwise in our possession or control, unless legally prohibited.
        </TextBody>
        <HeaderLeft>
          Processing and Posting Orders
        </HeaderLeft>
        <TextBody>
          <SubHeader>Processing Transactions and Posting Orders</SubHeader>
          Posting transactions to your account impacts your account balance. Posting a credit increases your balance. Posting a debit or hold reduces your balance. Credits include deposits and credits we make. Holds include deposit holds, and holds related to cash withdrawals and electronic transfers. Debits include withdrawals, transfers, payments, and fees.
        </TextBody>
        <TextBody>
          We use automated systems to process transactions and then to post transactions to accounts. When we process multiple transactions for your account on the same day, you agree that we may in our discretion determine our posting orders for the transactions and that we may credit, authorize, accept, pay, decline or return credits, debits and holds in any order at our option.
        </TextBody>
        <TextBody>
          <SubHeader>
            Posting Orders
          </SubHeader>
        </TextBody>
        <TextBody>
          This section summarizes how we generally post some common transactions to your account.
        </TextBody>
        <TextBody>
          We group the different types of transactions into categories. We use several different categories for holds, credits, and debits. Most categories include more than one transaction type.
        </TextBody>
        <TextBody>
          Our automated systems assign each transaction received for that day to a category. We generally post all transactions within a category, using the posting order or orders that apply to that category, before we post any transactions assigned to the next category.
        </TextBody>
        <TextBody>
          We start with the balance in your account at the beginning of the business day, subtract holds from your balance, and make any adjustments from prior days. Next, we generally add credits to your balance and then subtract debits from your balance. Some, but not all, of our categories are shown below. For each debit category shown below, we list some common types of debits that we assign to the category and summarize how we generally post them within the category.
          <ul>
            <li>
                We add deposits and other credits to your balance.
            </li>
            <li>
                Then, we subtract from your balance in date and time order the types of debits listed in this paragraph, when our systems receive date and time information. If our systems do not receive date and time information, then we subtract the remaining debits in this category from your balance in order from the highest to lowest dollar amount.<br />
                Common debits in this category include: <br />
                - withdrawals <br />
                - one-time transafers; and <br />
                - wire transfer,
            </li>
            <li>
                Then, we subtract from your balance many other types of electronic debits in order from the highest to lowest dollar amount. These debits include: scheduled transfers, preauthorized or automatic payments that use your deposit account number (generally referred to as automated clearing house (ACH) debits), and bill payments.
            </li>
            <li>
                Then, we subtract from your balance most fees (such as monthly maintenance fees, overdraft fees, and returned item fees) in order from highest to lowest dollar amount. Some fees may show as “processing” until the next day or longer.
            </li>
          </ul>
        </TextBody>
        <TextBody>
            <SubHeader>Changing Posting Orders</SubHeader>
            You agree that we may determine in our discretion the orders in which we post transactions to your account.
        </TextBody>
        <TextBody>
            You agree that we may determine in our discretion the categories, the transactions within a category, the order among categories, and the posting orders within a category. We sometimes add or delete categories, change posting orders within categories and move transaction types among categories. You agree that we may in our discretion make these changes at any time without notice to you.
        </TextBody>
        <TextBody>
            <SubHeader>Posting Orders Determined at End of Day</SubHeader>
            We receive credits, debits and holds throughout the day. Regardless of when during the day we receive transactions for your account, you agree that we may treat them as if we received all transactions at the same time at the end of the business day.
        </TextBody>
        <TextBody>
            During the day, we show some transactions as processing. As an example, we show some transactions as processing on the Account Details screen in your account. Please note that transactions shown as processing have not been posted yet. The posting order for these transactions is determined at the end of the day, with the other transactions we receive for that day.
        </TextBody>
        <TextBody>
            You should note that often we do not receive debits on the same day that you conduct them.
        </TextBody>
        <TextBody>
            We generally post credits and debits to your account in a different order than the order in which you conduct them or we receive them.
        </TextBody>
        <TextBody>
            <SubHeader>Overdraft Fees</SubHeader>
            We generally determine at the time we post a debit to your account whether it creates an overdraft and whether an overdraft or returned item fee applies. You should note that sometimes we authorize a transaction at a time when you have enough available funds to cover it, but because other transactions post before it and reduce your balance, the transaction creates an overdraft when we post it to your account. You can avoid fees for overdrafts and returned items by making sure that your account always contains enough available funds to cover all of your transactions.
        </TextBody>
        <TextBody>
            When your account balance includes some funds that are subject to a hold, dispute or legal process, you should note that those funds are not available to cover your transactions.
        </TextBody>
        <TextBody>
            Our posting orders can impact the number of overdraft fees we charge you when you do not have enough available funds to cover all of your transactions. When several debits arrive the same business day for payment from your account and you do not have enough available funds in your account to cover all of the debits we receive for that day, you understand that some posting orders can result in more overdrafts, and more fees for overdraft items and returned items, than if we had used other posting orders. You agree that we may in our discretion choose our posting orders, and also change them from time to time, regardless of whether additional fees may result.
        </TextBody>
        <TextBody>
            When your account balance includes some funds that are not available at the time that we post a debit, and you do not have enough available funds in your account to cover the debit, the debit results in an overdraft and we generally charge you an overdraft item fee or returned item fee for the debit. You should note that we do not show holds, or distinguish between available and unavailable funds in your account balance, on your statement so when you review your statement later, it might appear that you had enough available funds in your account to cover a debit for which we charged you a fee.
        </TextBody>
        <TextBody>
            <SubHeader>
                Certain Transactions Made After Business Day Ends
            </SubHeader>
            During processing, we generally include in your account balance some transactions that you make after the business day cut-off, but before the end of the calendar day. These transactions are described below. This can impact fees that apply to your account. The credits can help you avoid overdrafts, returned items, and related fees. However, the debits can cause you to incur overdrafts, returned items, and related fees. You should note that we show these transactions on your statement as posting to your account on our next business day.
        </TextBody>
        <TextBody>
            <i>Credits.</i> We generally add to your account balance the following credits, when the transaction occurs after the cutoff time for the business day, but during the same calendar day:
            <ul>
                <li>Cash deposited at one of our financial centers, and</li>
                <li>Transfers to your account from another deposit account with us.</li>
            </ul>
            <i>Debits.</i> We generally subtract from your account balance the following debits, when the transaction occurs after the cutoff time for the business day, but during the same calendar day:
            <ul>
                <li>Cash withdrawals made at one of our financial centers, and</li>
                <li>Transfers from your account made.</li>
            </ul>
        </TextBody>
        <HeaderLeft>
            Processing Deposits and Cashed Items
        </HeaderLeft>
        <TextBody>
            We may forward deposits, cashed items and other transaction requests for an account to one of our processing centers. We may use the date that our processing center receives the transaction as the effective date of the transaction.
        </TextBody>
        <TextBody>
            <SubHeader>
                Cashing Items or Accepting Items for Deposit
            </SubHeader>
            We may accept, accept for collection only, refuse, or return all or part of any deposit. If we accept items for deposit to your account, you are responsible for the items if there is a subsequent problem with them.
            <ul>
                <li>
                    We may accept an item for deposit to your account from anyone. We do not have to question the authority of the person making the deposit.
                </li>
                <li>
                    If your account is overdrawn, we may use the deposit to pay the overdraft and any fees you owe us.
                </li>
                <li>
                    We may adjust your account for any deposit errors, even if you have already withdrawn all or part of the deposit, though we reserve the right not to do so in every case.
                </li>
                <li>
                    In receiving items for deposit or collection, we act only as your collecting agent and assume no responsibility beyond the exercise of ordinary care. We are not responsible for errors and delays made by others in the collection process.
                </li>
                <li>
                    We may assess a charge for processing cash in a deposit.
                </li>
                <li>
                    If you give us cash that we later determine to be counterfeit, we may charge your account for the amount we determine to be counterfeit.
                </li>
                <li>
                    We may require ID or impose other conditions before accepting a deposit.
                </li>
            </ul>
        </TextBody>
        <TextBody>
            <SubHeader>
                Deposit Preparation and Acceptance
            </SubHeader>
            When you make deposits through our financial centers, including lobby boxes, night depositories and other automated depositories, we may use the method of delivery to our branch or processing center to determine when we accept the deposit, when you receive credit for the deposit, and whether deposit fees apply.
        </TextBody>
        <TextBody>
            If we credit your account for a deposit and provide you with a receipt, we may use the amount shown on the deposit slip or otherwise specified by you. The amount of the credit is subject to subsequent verification by us and, after review, we may adjust your account for any errors, though we reserve the right not to do so in every case.
        </TextBody>
        <TextBody>
            Any of our employees or authorized agents may open and count any deposit that our authorized employee or authorized agent did not count in front of you, including coin deposits, cash deposits, and each deposit made through the mail, a lobby box, a night depository, or other automated depository. You agree not to dispute that employee or agent’s determination of the amount you delivered. The funds will be accepted for deposit after the counting has been completed and we have verified the amount, if we opt to do so. The funds will be made available to you in accordance with our funds availability schedule at that time.
        </TextBody>
        <TextBody>
            If you make your deposit through a mechanical or automated depository such as a night depository, you agree to exercise due care in opening, closing and properly securing the depository.
        </TextBody>
        <TextBody>
            If your deposit includes items that we do not accept for deposit, we may hold those items until claimed by you.
        </TextBody>
        <TextBody>
            <SubHeader>
                Deposit Error Correction
            </SubHeader>
            When we accept your deposits, we may provisionally credit your account. If later we determine that the amounts are incorrect, we may adjust (debit or credit) your account, though we reserve the right not to do so. We may not adjust the deposit unless you notify us of the error within one year of the date of the deposit.   After this notice period has passed without your bringing an error to our attention, the deposit amount will be considered finally settled. That is, if the actual amount deposited was less than the amount declared on the deposit slip, the difference will become your property and if the actual amount deposited was more than the amount declared on the deposit slip, the difference will become our property. We may change our standard adjustment amount from time to time without notice to you.
        </TextBody>
        <TextBody>
            <SubHeader>
                Identifying the Account for Your Deposit
            </SubHeader>
            You must correctly identify the account to which you want funds deposited. We may credit a deposit to an account based solely on the account number listed on the deposit slip or other instruction to credit an account.  You are responsible for any claim, cost, loss or damage caused by your failure to properly identify the account to which a deposit is made or intended to be made.
        </TextBody>
        <TextBody>
            <SubHeader>Overpayments and Reversals</SubHeader>
            If funds to which you are not entitled are deposited to your account by mistake or otherwise, we may deduct these funds from your account, even if this causes your account to become overdrawn. If the funds were transferred from your account, we may reverse the transfer. We can do this without giving you any prior notice or demand.
        </TextBody>
        <TextBody>
            <SubHeader>Returned Items</SubHeader>
            This section applies to items that you deposit or that we cash for you (a “cashed or deposited item”) and includes items drawn on us as well as items drawn on other financial institutions. You are responsible for returned items.
        </TextBody>
        <TextBody>
            If a cashed or deposited item is returned to us at any time for any reason by the bank on which it is drawn or any collecting bank, we may accept that return, pay the claiming party, and charge the item to your account without regard to whether we or the other bank finally paid the item or returned the item in accordance with any applicable midnight deadline or clearinghouse rule. We may also deduct from your account any interest you may have provisionally earned on the item.
        </TextBody>
        <TextBody>
            We may charge you a fee for each returned item. Different fees may apply to domestic and foreign items. We may debit your account for a returned item at any time on or after the day it is returned to us by electronic, automated clearinghouse (“ACH”) or other means or on the day we receive notice that the item is being returned to us - whichever is earlier.
        </TextBody>
        <TextBody>
            As an example: if an item deposited in your account has been paid by the bank on which it is drawn (including on us) and that item is later returned to us with a claim that the item was altered, forged, unauthorized, bears a forged or missing endorsement or should not have been paid for any reason, we may at our discretion charge the item against your account or place a hold on the amount of that item against your account until the claim is finally resolved. We may take these actions without prior notice to you and regardless of whether settlement with respect to such item is considered final.
        </TextBody>
        <TextBody>
            We are not obligated to question the truth of the facts that are asserted, to assess the timeliness of the claim, to take any action to recover payment of a returned item, or to assert any defense. We do not need to notify you in advance of our actions related to the claim. If you do not have sufficient available funds to cover a returned item, we may overdraw your account. We are not liable to you if there are insufficient funds to pay your items because we withdraw funds from your account or in any way restrict your access to funds due to a hold or debit to your account in connection with a returned item. You agree to repay immediately an overdraft caused by a return of a cashed or deposited item.
        </TextBody>
        <TextBody>
            You waive notice of dishonor and protest. You agree that we will have no obligation to notify you of any item that is being returned.
        </TextBody>
        <TextBody>
            <SubHeader>
                Third-Party Endorsements
            </SubHeader>
            We may require that items you want to deposit or cash be endorsed by all parties to whom the items are payable. We may require verification of any endorsement through either an endorsement guarantee or personal identification.
        </TextBody>
        <BigHeader>
            When Funds are Available for Withdrawal and Deposit Holds
        </BigHeader>
        <TextBody>
            Our general policy is to make funds from your cash and deposits available to you no later than the first business day after the day of your deposit. However, in some cases we place a hold on funds that you deposit. A hold results in a delay in the availability of these funds.
        </TextBody>
        <TextBody>
            When we place a hold, you will have to wait a few days before being able to use the funds. When we decide to place a hold at the time you make your deposit, the status of such hold will be apparent on your online account.
        </TextBody>
        <TextBody>
            <SubHeader>
                Your Ability to Withdraw Funds
            </SubHeader>
            Our general policy is to make funds from your cash and deposits available to you no later than the first business day after the day we receive your deposit. Once they are available, you can withdraw the funds in cash and we will use the funds to make that you authorize. For determining the availability of your deposits, every day is a business day, except Saturdays, Sundays, and federal holidays.
        </TextBody>
        <TextBody>
            <SubHeader>
                Longer Delays May Apply
            </SubHeader>
            In some cases, we will not make all of the funds that you deposit available to you by the first business day after the day of your deposit. If you need the funds from a deposit right away, you should ask us when the funds will be available.
        </TextBody>
        <TextBody>
            In addition, we may delay the availability of funds you deposit for a longer period under the following circumstances:
            <ul>
                <li>You have overdrawn your account repeatedly in the last six months.</li>
                <li>There is an emergency, such as failure of communications or computer equipment.</li>
            </ul>
        </TextBody>
        <BigHeader>
            Processing Withdrawals
        </BigHeader>
        <TextBody>
            <SubHeader>
                Items Resulting from Voluntary Disclosure
            </SubHeader>
            If you voluntarily disclose your account number to another person orally, electronically, in writing or by other means, you are deemed to authorize each item, including electronic debits, which result from your disclosure. We may pay these items and charge your account.
        </TextBody>
        <TextBody>
            <SubHeader>
                Large Cash Withdrawals
            </SubHeader>
            We may require reasonable advance notice for large cash withdrawals. We may also refuse to honor a request to withdraw funds in cash from your account at a financial center if we believe that the amount is unreasonably large or that honoring the request would cause us an undue hardship or security risk. We may require that such withdrawals be made at one of our cash vaults by an armored courier, acceptable to us and at your sole risk and expense. We are not responsible for providing for your security in such transactions.
        </TextBody>
        <TextBody>
            <SubHeader>
                Paying Items
            </SubHeader>
            We may debit your account for an item drawn on your account either on the day it is presented to us for payment, by electronic or other means.   If you do not have sufficient available funds to cover the item, we decide whether to return it or to pay it and overdraw your account.
        </TextBody>
        <TextBody>
            We may determine your balance and make our decision on an insufficient funds item at any time between our receipt of the item or notice and the time we must return the item. We are required to determine your account balance only once during this time period.
        </TextBody>
        <TextBody>
            <SubHeader>Unpaid Items</SubHeader>
            If we decide not to pay an item drawn on your account, we may return the original, an image or a copy of the item or we may send an electronic notice of return and keep either the original, an image or a copy of the item in our records. If we send an electronic notice of return, you agree that any person who receives that electronic notice may use it to make a claim against you to the same extent and with the same effect as if we had returned the original item.
        </TextBody>
        <BigHeader>
            Notices, Statements and Other Communications
        </BigHeader>
        <TextBody>
            <SubHeader>General Terms for Notices, Statements and Other Communications</SubHeader>
            Please review promptly all notices, statements and other communications we send you. In this section “communications” means all notices, statements and other communications we send you.
        </TextBody>
        <TextBody>
            We may provide communications in English. Many communications will be notices of change affecting your rights and obligations. If you have questions about any of them or difficulty reading English, please call us at the number for customer service.
        </TextBody>
        <TextBody>
            We may:
            <ul>
                <li>address communications to one account owner;</li>
                <li>provide communications in English;</li>
                <li>destroy communications that are sent to you and returned to us as being undeliverable, along with any accompanying items;</li>
                <li>authorize the Post Office or an agent to destroy communications, along with accompanying items, that the Post Office informs us are undeliverable; and</li>
                <li>stop sending communications to you until a new address is provided to us if one or more communications that we mail to you are returned to us as being undeliverable.</li>
            </ul>
        </TextBody>
        <TextBody>
            We are not responsible for communications, or for any accompanying items, lost while not in our possession.
        </TextBody>
        <TextBody>
            If we receive communications that we sent you at a financial center, they are deemed to have been delivered to you at the time that they are available to you at the financial center.
        </TextBody>
        <TextBody>
            <B>Electronic delivery of communications </B>We recommend that you receive your communications electronically. You can find your account statements, notices, and other eligible documents online within the statements and documents area of your account details page. Communications currently available for electronic delivery are listed in the statements and documents area on our website.
        </TextBody>
        <TextBody>
            <SubHeader>
                Notices
            </SubHeader>
            When we inform you of changes affecting your rights and obligations, we do so by delivering or otherwise making a notice available to you. In some cases, we may post a notice of a change on our website. Otherwise, we provide it to you electronically.
        </TextBody>
        <TextBody>
            If a notice of a change to this Agreement is returned to us as being undeliverable or if we stop sending notices or statements to you because we consider your account dormant or because notices or statements we previously sent you were returned to us as being undeliverable, you understand that the notices are available to you through our financial centers. You agree to that method of delivery and that changes covered in these notices are still effective and binding on you.
        </TextBody>
        <TextBody>
            A notice sent to any one owner is deemed notice to all account owners and is effective for all account owners.
        </TextBody>
        <TextBody>
            <SubHeader>
                Statements
            </SubHeader>
            We provide you with a single statement when there is activity on your account. When there is no activity on your account, we may choose not to provide a statement. You agree that you will download your statements electronically from our website. Statement cycles generally vary from 28 to 33 days and may end on different days during the month. A statement cycle can be shorter than monthly. As examples, a statement cycle may only be a few days in length for the first statement cycle after an account is opened or when a statement date is changed to link accounts for combined statements.
        </TextBody>
        <TextBody>
            <SubHeader>
                Your Address and Change of Address
            </SubHeader>
            We may send notices, statements and other communications regarding your account to you at the electronic or street address we have in our records for your account.
        </TextBody>
        <TextBody>
            You agree to notify us if you change your address. If the United States Post Office or one of its agents tells us that your address has changed:
            <ul>
                <li>
                    we may change your address on our records to the address specified by the Post Office; and
                </li>
                <li>
                    we may send notices, statements and other communications regarding your account to that new address.
                </li>
            </ul>
        </TextBody>
        <BigHeader>
            Reporting Problems
        </BigHeader>
        <TextBody>
            If you find that your records and ours disagree, if you suspect any problem or unauthorized transaction on your account or you do not receive a statement when expected, call us immediately at the number for customer service on your statement. If you fail to notify us in a timely manner, your rights may be limited.
        </TextBody>
        <TextBody>
            <SubHeader>
                Your Responsibility
            </SubHeader>
            You must exercise reasonable control over your statements, deposit slips, endorsement and signature stamps, Personal Identification Numbers and other access devices. It is your responsibility to keep them safe and secure and to promptly discover and report if any of them are missing in time to prevent misuse. You assume full responsibility for monitoring and reviewing the activity of your account, the work of your employees, agents and accountants, and any use they make of your account.
        </TextBody>
        <TextBody>
            We may deny a claim for losses due to forged, altered or unauthorized transactions, items or signatures if you do not guard against improper access to your statements, deposit slips, endorsement and signature stamps, and account information. We may also deny your claim if you do not monitor your account and report problems as provided in this section.
        </TextBody>
        <TextBody>
            <SubHeader>
                What Are Problems and Unauthorized Transactions
            </SubHeader>
            Problems and unauthorized transactions include suspected fraud; missing deposits; unauthorized electronic transfers; missing, withdrawal orders; other withdrawal orders bearing an unauthorized signature, endorsement or alteration; illegible images; and encoding errors made by you or us. This is not a complete list.
        </TextBody>
        <TextBody>
            <SubHeader>
                Reviewing Your Account Statements
            </SubHeader>
            Your review of your statements and other items is one of the best ways to help prevent the wrongful use of your account. You agree:
            <ul>
                <li>to review your statements and other items and reconcile them as soon as they are made available to you;</li>
                <li>that our statements provide sufficient information to determine the identification and authenticity of any transaction including without limit, whether any are forged, altered or unauthorized if the statement includes the item number, amount and the date the item posted to your account;</li>
                <li>to report any problems or unauthorized transactions as soon as possible; and</li>
                <li>that 60 days after we send a statement and any accompanying items (or otherwise make them available) is the maximum reasonable amount of time for you to review your statement or items and report any problem or unauthorized transaction related to a matter shown on the statement or items. There are exceptions to this 60-day period. For forged, unauthorized or missing endorsements, you must notify us within the period specified by the state law applicable to your account.</li>
            </ul>
        </TextBody>
        <TextBody>
            <SubHeader>
                We Are Not Liable If You Fail To Report Promptly
            </SubHeader>
            Except as otherwise expressly provided elsewhere in this agreement, if you fail to notify us in writing of suspected problems or unauthorized transactions within 60 days after we make your statement or items available to you, you agree that:
            <ul>
                <li>you may not make a claim against us relating to the unreported problems or unauthorized transactions, regardless of the care or lack of care we may have exercised in handling your account; and</li>
                <li>you may not bring any legal proceeding or action against us to recover any amount alleged to have been improperly paid out of your account.</li>
            </ul>
        </TextBody>
        <TextBody>
            Except as otherwise expressly provided elsewhere in this agreement, we are not liable to you for subsequent unauthorized transactions on your account by the same person if you fail to report an unauthorized transaction on your account within 30 days (or such lesser period as is specified in the state law applicable to your account) following the closing date of the statement containing information about the first unauthorized transaction.
        </TextBody>
        <TextBody>
            <SubHeader>
                Written Confirmation and Other Assistance
            </SubHeader>
            If you report to us that an unauthorized transaction has occurred on your account, we may require you to confirm your report in writing. We may also require that you give us a statement, under penalty of perjury, about the facts and circumstances relating to your report and provide such other information and proof as we may reasonably request.
        </TextBody>
        <TextBody>
            If you assert a claim regarding a problem, you must cooperate with us in the investigation and prosecution of your claim and any attempt to recover funds. You also agree to assist us in identifying and in seeking criminal and civil penalties against the person responsible. You must file reports and complaints with appropriate law enforcement authorities.
        </TextBody>
        <TextBody>
            If you fail or refuse to do these things, we will consider your failure or refusal to be your ratification of the defect in the statement or item, unauthorized transaction or other problem and your agreement that we can charge the full amount to your account.
        </TextBody>
        <TextBody>
            <SubHeader>
                Our Investigation and Maximum Liability
            </SubHeader>
            We may take a reasonable period of time to investigate the facts and circumstances surrounding any claimed loss. We do not have to provisionally credit your account while we investigate.
        </TextBody>
        <TextBody>
            Our maximum liability is the lesser of your actual damages proved or the amount of the missing deposit or the forgery, alteration or other unauthorized withdrawal, reduced in all cases by the amount of the loss that could have been avoided by your use of ordinary care.
        </TextBody>
        <TextBody>
            We are not liable to you for special or consequential losses or damages of any kind, including loss of profits and opportunity or for attorneys’ fees incurred by you.
        </TextBody>
        <TextBody>
            <SubHeader>Business Insurance</SubHeader>
            If your claim relates to a business account, you agree to pursue all rights you may have under any insurance coverage you maintain before making a claim against us in connection with any transaction involving your accounts. You will provide us with all reasonable information about your coverage, including the name of your insurance carrier, policy number, policy limits and applicable deductibles. Our liability is reduced by the amount of all insurance proceeds you receive or are entitled to receive. At our request, you agree to assign to us your rights under your insurance policy.
        </TextBody>
        <TextBody>
            <SubHeader>
                Opening a New Account
            </SubHeader>
            If you or we suspect that your account is or may be compromised, we may recommend that you close your account and open a new account. If there are any unauthorized transactions on your account, we recommend that you close your account and open a new one. If we recommend that you close your account and you do not do so, we are not liable to you for subsequent losses or damages on the account due to unauthorized transactions. When you open a new account, you are responsible for notifying any third parties that need to know your new account number.
        </TextBody>
        <BigHeader>
            Other Terms and Services
        </BigHeader>
        <TextBody>
            <SubHeader>
                Account Changes
            </SubHeader>
            You must notify us of any change to your name or address. If you do not provide notice of change of address, we may send notices, statements and other correspondence to you at the address maintained on our records for your account and you agree to indemnify us and hold us harmless for doing so.
        </TextBody>
        <TextBody>
            You agree to notify us in writing of any change in ownership or authorized signers of your account or if an owner or authorized signer on the account dies or is adjudicated incompetent.
        </TextBody>
        <TextBody>
            If there is more than one owner and/or authorized signer on the account, any one account holder or authorized signer may request the account be closed without consent of any other account holder or authorized signer. Further, any one account holder may request, and we may, at our option, permit removal of any account holder or authorized signer without consent of any other account holder or authorized signer on the account.
        </TextBody>
        <TextBody>
            You acknowledge that we may, but need not, require a new signature card to be completed before any change in ownership or authorized signers becomes effective and each time you open a new account, we may require a Taxpayer Identification Number certification(s). You also acknowledge that we may require you to close your account in the event of any change in ownership or change in the authorized signers.
        </TextBody>
        <TextBody>
            After we receive notice of a change and all documents we require regarding the change, we may take a reasonable period of time to act on and implement the change to your account.
        </TextBody>
        <TextBody>
            <SubHeader>
                Compliance
            </SubHeader>
            You agree to comply with applicable laws and regulations. You may not use your account or related services for any illegal transactions or activity, for example those prohibited by the Unlawful Internet Gambling Enforcement Act, 31 U.S.C. Section 5361 et. seq. You agree to indemnify us from every action, proceeding, claim, loss, cost and expense (including attorney’s fees) suffered or incurred by us due to any U.S. or foreign government entity seizing, freezing or otherwise asserting or causing us to assert control over any account or funds in an account of yours (or ours) when purportedly caused by, or arising out of, your action or inaction. This will apply whether or not such action is ultimately determined to be authorized under the laws of the U.S. or its territories, or of any foreign jurisdiction. We are not required to inquire or determine the authority of any action taken by the U.S. or foreign government entity prior to acceding to any legal process initiated by it.
        </TextBody>
        <TextBody>
            Please note that your agreement to comply with applicable laws and regulations includes United States economic sanctions laws and regulations, including regulations issued by the Office of Foreign Assets Control (OFAC) of the U.S. Department of the Treasury, and Executive Orders issued by the President of the United States.
        </TextBody>
        <TextBody>
            <SubHeader>
                Proper Authorization
            </SubHeader>
            You represent and warrant to us that (a) you are duly organized, validly existing and, to the extent applicable under the laws of the jurisdiction of your organization, in good standing under the laws of your jurisdiction of your organization, (b) you have all requisite corporate and partnership power and authority, as the case may be, to execute and deliver this Agreement and perform your obligations hereunder, (c) the execution and delivery of this Agreement and the consummation of all transactions contemplated thereby have been duly authorized by all necessary corporate and partnership action, as the case may be, and will not result in any violation of any applicable law or regulation, and (d) this Agreement has been duly executed and delivered by, and constitutes a legal, valid, binding and enforceable obligation.
        </TextBody>
        <TextBody>
            <SubHeader>
                Conflicting Demands and Disputes
            </SubHeader>
            We are not required to make payment from an account to a signer, a payee, a beneficiary of a trust account or Payable on Death (POD) account, or to any other person claiming an interest in any funds in the account:
            <ul>
                <li>if we have actual knowledge of, or otherwise believe in good faith that there may be a bona fide dispute between the signers, beneficiaries, payees, or other persons concerning their rights to the account proceeds or</li>
                <li>if we are otherwise uncertain as to who is entitled to the account funds.</li>
            </ul>
        </TextBody>
        <TextBody>
            We may notify all signers, beneficiaries, payees, and other persons claiming an interest in the account of the dispute or uncertainty without liability to you.
        </TextBody>
        <TextBody>
            We also may, at our option and without liability to you, take one or more of these actions:
            <ul>
                <li>continue to rely on current signature cards and other account documents;</li>
                <li>honor the competing claim upon receipt of evidence we deem satisfactory to justify such claim;</li>
                <li>freeze all or part of the funds until the dispute is resolved to our satisfaction;</li>
                <li>close the account and distribute the account balance, subject to any claims, to each claimant payable jointly, or payable individually in equal shares to each claimant;</li>
                <li>pay the funds into an appropriate court for resolution; or</li>
                <li>refuse to disburse any funds in the account to any person until such time as: all persons claiming an interest in the account consent in writing to a resolution of the dispute; or a court of proper jurisdiction authorizes or directs the payment; or the person with a conflicting claim withdraws his or her claim in writing.</li>
            </ul>
        </TextBody>
        <TextBody>
            You are liable for all expenses and fees we incur, including attorneys’ fees, and we may charge them to your account.
        </TextBody>
        <TextBody>
            <SubHeader>
                Death or Incompetence
            </SubHeader>
            You agree to notify us promptly if any owner or authorized signer on your account dies or is declared incompetent by a court. Until we receive a notice of death or incompetency, we may act with respect to any account or service as if all owners, signers or other persons are alive and competent and we will not be liable for any actions or inactions taken on that basis.
        </TextBody>
        <TextBody>
            If you give us instructions regarding your account, and you or another owner of the account subsequently dies or is declared incompetent, we may act on the instructions unless we receive written notice of death or incompetency prior to honoring such instructions.
        </TextBody>
        <TextBody>
            When we receive a notice that an owner has died or been declared incompetent, we may place a hold on your account and refuse to accept deposits or permit withdrawals. We may hold any funds in your account until we know the identity of the successor.
        </TextBody>
        <TextBody>
            If a deposit — including salary, pension, Social Security and Supplemental Security Income (SSI)
        </TextBody>
        <TextBody>
            - payable to the deceased owner is credited to the account after the date the deceased owner died, we may debit the account for the deposit and return it to the payer.
        </TextBody>
        <TextBody>
            We may accept and comply with court orders, and take direction from court appointed personal representatives, guardians, or conservators from states other than where your account was opened or where the account, property or records are held. We reserve the right to require U.S. court documents for customers who reside outside of the U.S. at time of incompetence or death.
        </TextBody>
        <TextBody>
            <SubHeader>
                Facsimile Signature
            </SubHeader>
            A facsimile signature can be a convenient method for signing or endorsing documents and other items. If you use a facsimile signature, you are responsible for any withdrawal from your account that bears or appears to us to bear a facsimile signature that resembles or purports to be the signature of a person authorized to withdraw funds. We will not be liable to you if use of the facsimile device (or similar device utilized to affix your signature) was unauthorized. You are responsible even if the size, or color of the facsimile signature is different from that of any signature previously presented to us. We may pay the withdrawal and may charge your account for it. You agree to reimburse us (and we may charge your account) for all claims, costs, losses and damages, including attorneys’ fees, that result from our payment of a withdrawal bearing either  a facsimile that resembles or purports to bear your signature or a facsimile that we believe you authorized.
        </TextBody>
        <TextBody>
            <SubHeader>“Freezing” Your Account</SubHeader>
            If we decide to close your account, we may freeze it. If we do this, we may in our discretion either accept or return deposits and other items that we receive after we freeze your account without being liable to you.
        </TextBody>
        <TextBody>
            If at any time we believe that your account may be subject to irregular, unauthorized, fraudulent or illegal activity, we may, in our discretion, freeze some or all of the funds in the account and in other accounts you maintain with us, without any liability to you, until such time as we are able to complete our investigation of the account and transactions. If we do freeze your account funds, we will provide notice to you as soon as reasonably possible. Notice may be made by mail or verbally or provided by other means such as via email or text alerts as permitted by law or by updated balance information. We may not provide this notice to you prior to freezing the account if we believe that such notice could result in a security risk to us or to the owner of the funds in the account.
        </TextBody>
        <TextBody>
            <SubHeader>Indemnification and Limitation of Liability</SubHeader>
            You agree to reimburse us for all claims, costs, losses and damages (including fees paid for collection) we may incur with respect to overdrafts or returned deposits in connection with your account.
        </TextBody>
        <TextBody>
            We are not liable to you for errors that do not result in a financial loss to you. We may take any action authorized or permitted by this Agreement without being liable to you, even if such action causes you to incur fees, expenses or damages.
        </TextBody>
        <TextBody>
            We are not liable to you for any claim, cost, loss or damage caused by an event that is beyond our reasonable control. In particular, we are not liable to you if circumstances beyond our reasonable control prevent us from, or delay us in, performing our obligations for a service, including acting on a payment order, crediting a funds transfer to your account, processing a transaction or crediting your account. Circumstances beyond our reasonable control include: a natural disaster; emergency conditions, such as fire, theft or labor dispute; a legal constraint or governmental action or  inaction; the breakdown or failure of our equipment for any reason, including a loss of electric power; the breakdown of any private or common carrier communication or transmission facilities, any time-sharing supplier or any mail or courier service; the potential violation of any guideline, rule or regulation of any government authority; suspension of payments by another bank; or your act, omission, negligence or fault.
        </TextBody>
        <TextBody>
            Except as limited by applicable law, we are not liable for special, incidental, exemplary, punitive or consequential losses or damages of any kind.
        </TextBody>
        <TextBody>
            Our liability for a claim will be limited to the face value of an item or transaction improperly dishonored or paid or the actual value of any deposits not properly credited or withdrawals not properly debited.
        </TextBody>
        <TextBody>
            You agree that the amount of any claim you have against us in connection with any account or transaction with us, whether brought as a warranty, negligence, wrongful dishonor or other action, is subject to reduction to the extent that: 1) negligence or failure to use reasonable care on your part, or on the part of any of your agents or employees, contributed to the loss which is the basis of your claim; and 2) damages could not be avoided by our use of ordinary care.
        </TextBody>
        <TextBody>
            Any loss recovery you obtain from third parties on a particular claim will reduce the amount of any obligations we may have to you on that claim and you will immediately notify us of any such recovery. You agree to pursue all rights you may have under any insurance policy you maintain in connection with any loss and to provide us information regarding coverage. Our liability will be reduced by the amount of any insurance proceeds you receive or are entitled to receive in connection with the loss. If we reimburse you for a loss covered by insurance, you agree to assign us your rights under the insurance to the extent of your reimbursement.
        </TextBody>
        <TextBody>
            <SubHeader>Legal Process – Subpoena and Levy</SubHeader>
            “Legal process” includes a writ of attachment, execution, garnishment, tax withholding order, levy, restraining order, subpoena, warrant, injunction, government agency request for information, search warrant, forfeiture or other similar order.
        </TextBody>
        <TextBody>
            We may accept and comply with legal process: served in person, by mail, by facsimile transmission, or by other means; or served at locations other than the location where the account, property or records are held. You direct us not to contest the legal process. We may, but are not required to, send a notice to you of the legal process. We do not send a notice if we believe the law prohibits us from doing so.
        </TextBody>
        <TextBody>
            We may hold and turn over funds or other property to the court or creditor as directed by the legal process, subject to our right of setoff and any security interest we have in the funds or other property. We do not pay interest on the funds during the period we hold them pursuant to legal process. If we hold or turn over funds, we may without any liability to you return items unpaid and refuse to permit withdrawals from your account.
        </TextBody>
        <TextBody>
            We may charge your account a fee for each legal process. You agree to pay us for fees and expenses (including administrative expenses) that we incur in responding to any legal process related to your account, such as expenses for research and copying of documents. The fees and expenses may include attorneys’ fees. We may deduct these fees and expenses from any of your accounts without prior notice to you.
        </TextBody>
        <TextBody>
            If the legal process directs us to release information about one or more, but not all, accounts that are reported on a combined statement, we may release the entire combined statement, even though other accounts reported on the combined statement are not covered by the legal process. If the legal process requests information about one or more, but not all, account owners or signers, we may release information about all co-owners or signers on the account, even though some of the other co-owners or signers are not covered by the legal process.
        </TextBody>
        <TextBody>
            We may produce documents held at, or provide access to property that is located in, any of our facilities or any facility operated by a third party on our behalf, even if the facility is not designated as the place to be searched in the legal process.
        </TextBody>
        <TextBody>
            We have no liability to you if we accept and comply with legal process as provided in this section or by law.
        </TextBody>
        <TextBody>
            <SubHeader>
            Multiple Signatures Not Required
            </SubHeader>
            We may act on the oral or written instructions of any one signer on the account. Each signer may make withdrawals, authorize payments, transfer funds, stop payments, obtain ancillary services (e.g., electronic fund transfer services or wire transfers), and otherwise give us instructions regarding your account. We may require written authorization for some actions.
        </TextBody>
        <TextBody>
            We do not assume a duty to enforce multiple signature requirements that you may agree upon among yourselves. If you indicate on your signature card or other account documents that more than one signature is required for withdrawal, this indication is for your own internal procedures and is not binding on us.
        </TextBody>
        <TextBody>
            We may disregard any instructions to permit withdrawals only upon more than one signature with respect to electronic fund transfers or other debit/withdrawal requests. We may pay out funds from your account if the item, or other withdrawal or transfer instruction is signed or approved by any one of the persons authorized to sign on the account. We are not liable to you if we do this.
        </TextBody>
        <TextBody>
            <SubHeader>Powers of Attorney/Appointment and Payment to Agents</SubHeader>
            You may decide to appoint someone to act for you as your agent or attorney-in-fact (“agent”) under a power of attorney. Please note that the form must be satisfactory to us in our discretion and unless prohibited by law, we may refuse, with or without cause, to honor powers of attorney that you grant to others.
        </TextBody>
        <TextBody>
            For our customers’ convenience we have a power of attorney form, which is available at many of our financial centers. If your state has a statutory form power of attorney, we also generally accept that form. We may, however, accept any form that we believe was executed by you and act on instructions we receive under that form without any liability to you. You agree to reimburse us for all claims, costs, losses and damages that we incur in accepting and acting on any power of attorney form that we believe you executed.
        </TextBody>
        <TextBody>
            We may pay any funds deposited in your account to your agent or upon the order of your agent. When we accept a power of attorney, we may continue to recognize the authority of your agent to act on your behalf without question until we receive written notice of revocation from you or notice of your death or incapacity and have had a reasonable time to act upon it. We will not be liable for action in accordance with the most current documentation if we have not received such notice.
        </TextBody>
        <TextBody>
            We may require a separate form for each agent and for each account for which you want to grant power of attorney. We may require your agent to present the original form and refuse to act on a copy. In some cases, we may require that your agent confirm in an affidavit that the power has not been revoked or terminated or that you register the power with the appropriate recording authorities. We may restrict the types or sizes of transactions we permit your agent to conduct.
        </TextBody>
        <TextBody>
            The authority of your agent to receive payments, transact on or otherwise make changes to your account generally terminates with your death or incapacity, unless the document creating such agency provides, in accordance with applicable law, that the agent’s powers continue in spite of your incapacity.
        </TextBody>
        <TextBody>
            <SubHeader>Records</SubHeader>
            We may in our discretion retain records in any form including, without limit, paper, film, digitalized or other electronic medium. If we are not able to produce the original or a copy of your signature card or any other document relating to your account or service, our records (including our electronic records) will be deemed conclusive. If there is a discrepancy between your records and our records, our records will be deemed conclusive.
        </TextBody>
        <TextBody>
            <SubHeader>Right of Setoff</SubHeader>
            We may take or setoff funds in any or all of your accounts with us and with our affiliates for direct, indirect and acquired obligations that you owe us, regardless of the source of funds in an account. This provision does not apply where otherwise prohibited by law. Your accounts include both accounts you own individually and accounts you own jointly with others. Our setoff rights are in addition to other rights we have under this Agreement to take or charge funds in your account for obligations you owe us.
        </TextBody>
        <TextBody>
            If the law imposes conditions or limits on our ability to take or setoff funds in your accounts, to the extent that you may do so by contract, you waive those conditions and limits and you authorize us to apply funds in any or all of your accounts with us and with our affiliates to obligations you owe us.
        </TextBody>
        <TextBody>
            We may use funds held in your joint accounts to repay obligations on which any account owner is liable, whether jointly with another or individually. We may use funds held in your individual accounts to repay your obligations to us, whether owed by you individually or jointly with another, including: obligations owed by you arising out of another joint account of which you are a joint owner, even if the obligations are not directly incurred by you; obligations on which you are secondarily liable; and any amounts for which we become liable to any governmental agency or department or any company as a result of recurring payments credited to any of your accounts after the death, legal incapacity or other termination of entitlement of the intended recipient of  such funds. If you are a sole proprietor, we may charge any of your accounts.
        </TextBody>
        <TextBody>
            If we take or setoff funds from a time deposit account, we may charge an early withdrawal penalty on the funds withdrawn.
        </TextBody>
        <TextBody>
            We may take or setoff funds from your account before we pay items drawn on the account. We are not liable to you for dishonoring items where our action results in insufficient funds in your account to pay your items.
        </TextBody>
        <TextBody>
            Some government payments may be protected from attachment, levy or other legal process under federal or state law. If such protections may apply, to the extent that you may do so by contract, you waive these protections and agree that we may take or setoff funds, including federal and state benefit payments, from your accounts to pay overdrafts, fees and other obligations you owe us.
        </TextBody>
        <TextBody>
            This section does not limit or reduce our rights under applicable law to charge or setoff funds in your accounts with us for direct, indirect and acquired obligations you owe us.
        </TextBody>
        <TextBody>
            <SubHeader>Sample of Your Signature</SubHeader>
            To determine the authenticity of your signature, we may refer to the signature card or other document upon which your signature appears. We may use an automated process to reproduce and retain your signature from an item upon which your signature appears.
        </TextBody>
        <TextBody>
            <SubHeader>Unclaimed Property – Accounts Presumed Abandoned or Inactive</SubHeader>
            State and federal law and our policy govern when accounts are considered abandoned. The applicable state law is generally the state listed in the address for your account statement.
        </TextBody>
        <TextBody>
            Your account is usually considered abandoned if you have not performed at least one of the following activities for the period of time specified in the applicable state’s unclaimed property law: made a deposit or withdrawal, written to us about the account, or otherwise shown an interest in the account, such as asking us to keep the account active. You usually need to perform the activity. Therefore, bank charges and interest payments, and automatic deposits and withdrawals, are usually not considered activity.
        </TextBody>
        <TextBody>
            We are required by the unclaimed property laws to turn over accounts considered abandoned to the applicable state. Before we turn over an abandoned account, we may send a notice to the address we currently show for the account statement. We may not send this notice if mail we previously sent to this address was returned. Unless prohibited by the applicable state law, we may charge to the account our costs and expenses of any notice, advertisement, payment and delivery of the account to the applicable state agency.
        </TextBody>
        <TextBody>
            After we turn the funds over to the state, we have no further liability to you for the funds and you must apply to the appropriate state agency to reclaim your funds.
        </TextBody>
        <TextBody>
            If we consider your account inactive, then (unless prohibited by federal law or the law of the state where we maintain your account) we may:
            <ul>
                <li>charge dormant account fees on the account in addition to regular monthly maintenance and other fees,</li>
                <li>stop sending statements,</li>
                <li>if the account received interest, stop paying interest on the account; and</li>
                <li>refuse to pay items drawn on or payable out of the account.</li>
            </ul>
        </TextBody>
        <TextBody>
            If you re-establish contact with us, we do not have to reimburse you for these fees and we are not liable to you for any interest that would otherwise have accrued on your account.
        </TextBody>
        <TextBody>
            <SubHeader>Verification of Transactions and Right to Reverse Transactions</SubHeader>
            Transactions, including those for which we provide a receipt, may be subject to subsequent verification and correction, though we reserve the right not to do so in every case. We may reverse or otherwise adjust any transaction (both credit and debit) that we believe we erroneously made to your account at any time without prior notice to you, if we opt to do so.
        </TextBody>
        <TextBody>
            <SubHeader>Waiver, Severability, and Change of Law by Agreement</SubHeader>
            <B>Waiver</B> We may delay or waive the enforcement of any of our rights under this Agreement without losing that right or any other right. No delay in enforcing our rights will affect your obligation to pay us fees and other amounts you owe us under this Agreement. If we waive a provision of this Agreement, the waiver applies only in the specific instance in which we decide to waive the provision and not to future situations or other provisions regardless of how similar they may be.
        </TextBody>
        <TextBody>
            <B>Severability</B> A determination that any part of this Agreement is invalid or unenforceable will not affect the remainder of this Agreement.
        </TextBody>
        <TextBody>
            <B>Change of Law by Agreement</B> If any part of this Agreement is inconsistent with any applicable law, then to the extent the law can be amended or waived by contract, you and we agree that this Agreement governs and that the law is amended or waived by this Agreement.
        </TextBody>
        <BigHeader>
            Online Services
        </BigHeader>
        <TextBody>
            We offer a variety of online services for use with your accounts.
        </TextBody>
        <TextBody>
            <SubHeader>Types of Online Services</SubHeader>
            <B>Payments, Credits, and Transfers</B> You can send or receive electronic transfers from or to your accounts. Electronic transfers may take various forms, such as:
            <ul>
                <li>Automatic electronic deposits to your account;</li>
                <li>Automatic one-time or repeating charges to your account for bill payments, sent by a merchant or other payee with your authorization.; and</li>
            </ul>
        </TextBody>
        <TextBody>
            <B>Online Services.</B> Online services are governed by a separate agreement. You receive the agreement for the service at the time you enroll. You can use these services with linked accounts to view your account information, make deposits, transfer funds between your accounts and to the accounts of others, and make payments from your account to third parties. You can enroll for these services on our website.
        </TextBody>
        <TextBody>
            <SubHeader>Online Disclosures</SubHeader>
            <B>Business deposit accounts</B> Our Schedule of Fees describes our business deposit accounts. Business deposit accounts are accounts that are established primarily for business purposes.
        </TextBody>
        <TextBody>
            When you open one of our business deposit accounts, you represent and agree to that you are establishing it primarily for business purposes. Provisions below that explain a consumer’s liability for unauthorized transfers do not apply to business deposit accounts, although as a matter of practice we generally follow the error resolution procedures described in this Online Disclosures section for business-purpose accounts. Please note that we are not required to follow these procedures for business accounts and that we may change our practice at any time without notice.
        </TextBody>
        <TextBody>
            <B>Contact in Event of Unauthorized Transfer; and Lost or Stolen Card, PIN or Other Code</B> <br />
            If you believe your PIN or other code is lost or stolen, or learned by an unauthorized person, or that someone has transferred or may transfer money from your account without your permission, notify us immediately by calling the number listed below.
        </TextBody>
        <TextBody>
            Telephone: 1-415-413-3916 <br />
            You can also write to us at: Titan Vault, 922 Oakes Street, East Palo Alto, CA 94303<br />
            You should also call the number or write to the address listed above if you believe a transfer has been made using the information from your account without your permission.<br />
            If unauthorized activity occurs, you agree to cooperate during the investigation and to complete a Fraud Claims Report or similar affidavit.<br />
        </TextBody>
        <TextBody>
            <B>Business Days</B> For purposes of these disclosures, our business days are Monday through Friday. Weekends and bank holidays are not included.
        </TextBody>
        <TextBody>
            <B>Documentation of Transfers</B> <br />
            Receipts You can usually get a receipt at the time you make any transfer to or from your account. You may not get a receipt for small dollar transactions.
        </TextBody>
        <TextBody>
            Transactions may be verified by us though we reserve the right not to do so in every case, so the receipt is not final and our records will control if there is a conflict.
        </TextBody>
        <TextBody>
            Periodic Statements We send you a monthly account statement unless there are no electronic fund transfers in a particular month. In any case, we send you a statement at least quarterly unless we consider your account inactive.
        </TextBody>
        <TextBody>
            <B>Liability for Failure to Make Transfers</B><br />
            If we do not complete a transfer to or from your account on time or in the correct amount according to our agreement with you, we will be liable for your losses or damages. However, there are some exceptions. We will not be liable, for instance:
            <ul>
                <li>If, through no fault of ours, you do not have enough money in your account to make the transfer.</li>
                <li>If circumstances beyond our control (such as power outages, equipment failures, fire or flood) prevent the transfer, despite reasonable precautions that we have taken.</li>
                <li>If the funds are subject to legal process or other encumbrance restricting the transfer.</li>
                <li>If we consider your account to be inactive or dormant.</li>
                <li>There may be other exceptions stated in our agreement with you or permitted by law.</li>
            </ul>
        </TextBody>
        <TextBody>
            <B>Confidentiality - Account Information Disclosure</B> We will disclose information to third parties about your account or transfers you make as stated in the Information about You and Your Account section near the front of this Agreement.
        </TextBody>
        <TextBody>
            <B>Fees</B><br />
            Other Fees For fees that apply to online services, please review the Schedule of Fees for your account and each agreement or disclosure that we provide to you for the specific electronic service, including the separate agreement for Online Services.
        </TextBody>
        <TextBody>
            <B>In Case of Errors or Questions about your Electronic Transfers You May Sign into Online Services to Report the Error Promptly, or</B> Call or write us at the telephone number or address below, as soon as you can, if you think your statement or receipt is wrong, or if you need more information about a transfer listed on the statement or receipt.
        </TextBody>
        <TextBody>
            Call us at (415) 413-3916 during normal Claims Department business hours or write us at Titan Vault, 922 Oak Street, East Palo Alto, CA 94303.
        </TextBody>
        <TextBody>
            We must hear from you NO LATER than 60 days after we sent you the FIRST statement on which the error or problem appeared. Please provide us with the following:
            <ul>
                <li>Tell us your name and account number;</li>
                <li>Describe the error or the transfer you are unsure about, and explain as clearly as you can why you believe it is an error or why you need more information;</li>
                <li>Tell us the dollar amount of the suspected error.</li>
            </ul>
        </TextBody>
        <TextBody>
            If you tell us orally, we may require that you send your complaint or question in writing within 10 business days.<br />
            We will determine whether an error occurred within 10 business days after we hear from you and will correct any error promptly. If we need more time, however, we may take up to 45 days to investigate your complaint or question. If we decide to do this, we will credit your account within 10 business days for the amount you think is in error, so that you will have the use of the money during the time it takes us to complete our investigation. If we ask you to put your complaint or question in writing and we do not receive it within 10 business days, we may not credit your account.<br />
            For errors involving new accounts, we may take up to 90 days (instead of 45) to investigate your complaint or question. For new accounts we may take up to 20 business days to credit your account for the amount you think is in error.<br />
            We will tell you the results within 3 business days after completing our investigation. If we decide that there was no error, we will send you a written explanation. You may ask for copies of the documents that we used in our investigation.<br />
            <B>Notice:</B> As part of the security system to help protect your account, we may use hidden cameras and other security devices to determine who is accessing your account. You consent to this.<br />
        </TextBody>
        <BigHeader>Funds Transfer Services</BigHeader>
        <TextBody>
            The following provisions apply to funds transfers you send or receive through us. We provide separate agreements to you that govern the terms of some funds transfer services, including separate agreements for online services, and funds transfers in the financial centers. If you have a specific agreement with us for these services, these provisions supplement that agreement to the extent these provisions are not inconsistent with the specific agreement.
        </TextBody>
        <TextBody>
            The Uniform Commercial Code includes provisions relating to funds transfers. These provisions define the following terms: funds transfer, payment order and beneficiary. These terms are used here as they are defined in Article 4A of the Uniform Commercial Code – Funds Transfers as adopted by the state whose law applies to the account for which the funds transfer service is provided.
        </TextBody>
        <TextBody>
            In general: A funds transfer is the process of carrying out payment orders that lead to paying a beneficiary. The payment order is the set of instructions given to us to transfer funds. The beneficiary is the person or business who receives the payment.
        </TextBody>
        <TextBody>
            In general, your and our rights and obligations under this Agreement are governed by and interpreted according to federal law and the law of the state where your account is located. Funds transfers to your account or funded from your account or otherwise funded by you may involve one or more funds transfer systems, including, without limitation, Fedwire or Clearing House Interbank Payments System (CHIPS). Accordingly, notwithstanding any choice of law that may be provided elsewhere in this agreement, such transfers will be governed by the rules of any funds transfer system through which the transfers are made, as amended from time to time, including, without limitation, Fedwire, the National Automated Clearing House Association, any regional association (each an “ACH”), and CHIPS. Funds transfers through Fedwire will be governed by, and subject to, Regulation J, Subpart B, and Uniform Commercial Code Article 4A incorporated by reference thereunder. Funds transfers through CHIPS are governed by, and subject to, CHIPS Rules and Administrative Procedures and by the laws of the State of New York, including Article 4-A of the New York Uniform Commercial Code.
        </TextBody>
        <TextBody>
            We may charge fees for sending or receiving a funds transfer. We may deduct our fees from your account or from the amount of the transfer. Other financial institutions involved in the funds transfer may also charge fees. For current fees, call us at the number for customer service.
        </TextBody>
        <TextBody>
            <SubHeader>
                Sending Funds Transfers
            </SubHeader>
            You may subscribe to certain services we offer or you may give us other instructions to pay money or have another bank pay money to a beneficiary. This Sending Funds Transfers section applies to wire transfers and transfers we make between your Titan Vault accounts. It does not apply to automated clearing house (ACH) system funds transfer services.
        </TextBody>
        <TextBody>
            You may give us payment orders for ACH system funds transfers only if you have a separate agreement with us for those services.
        </TextBody>
        <TextBody>
            <B>Cutoff Times for Payment Orders</B> We have cutoff times for processing payment orders. Cutoff times vary depending on the particular office of our bank and the type of payment order. We may treat payment orders we receive after a cutoff time as if received the next business day. We tell you our cutoff times upon request.<br />
            <B>Amending or Canceling Payment Orders</B> You may not amend or cancel a payment order after we receive it. If you ask us to do this, we may make a reasonable effort to act on your request. But we are not liable to you if, for any reason, a payment order is not amended or canceled. You agree to reimburse us for any costs, losses or damages that we incur in connection with your request to amend or cancel a payment order.<br />
            <B>Inconsistency of Name or Number</B> The beneficiary’s bank may make payment to the beneficiary based solely on the account or other identifying number, even if the name on the payment order differs from the name on the account. We or an intermediary bank may send a payment order to  an intermediary bank or beneficiary’s bank based solely on the bank identifying number, even if the payment order indicates a different bank name.<br />
            <B>Sending Payment Orders</B> We may select any intermediary bank, funds transfer system or means of transmittal to send your payment orders. Our selection may differ from that indicated in your instructions.<br />
            <B>Notice of Rejection</B> We may reject payment orders. We notify you of any rejection orally, electronically or in writing. If we send written notices by mail, we do so by the end of the next business day.<br />
            We are not liable to you for the rejection or obligated to pay you interest for the period before you receive timely notice of rejection.<br />
            <B>Errors or Questions About Your Payment Orders</B> We notify you about certain funds transfers by listing them on your account statement. In some cases, we also may notify you electronically, in writing or by a report produced through one of our information reporting services.<br />
            You must notify us at once if you think a funds transfer shown on your statement or notice is incorrect. You must send us written notice, including a statement of relevant facts, no later than 14 days after the date you receive the first notice or statement on which the problem or error appears.<br />
            If you fail to notify us within this 14-day period, we are not liable for any loss of interest because of an unauthorized or erroneous debit or because your statement or notice is incorrect. We are not required to compensate you, and we are not required to credit or adjust your account for any loss of interest or interest equivalent.<br />
        </TextBody>
        <TextBody>
            <SubHeader>Receiving Funds Transfers</SubHeader>
            We may receive instructions to pay funds to your account. We may receive funds transfers directly from the sender, through a funds transfer system or through some other communications system. This includes wire transfers, ACH transfers that may be sent through an ACH system or processed directly to an account with us, and transfers between Titan Vault accounts.<br />
            <B>ACH Provisional Payment Rule</B> Under ACH rules, funds transfers sent through an ACH are provisional and may be revoked prior to final settlement. You agree to these rules. If the funds transfer is revoked before final settlement, we may charge your account for the amount credited. The person who sent the payment order is considered not to have paid you. If this happens, we do not send a separate notice; we report the information on your account statement.<br />
            <B>Notice of Funds Transfer</B> We notify you that we have received funds transfers by listing them on your account statement. We provide statements to you by mail or through Online Services if you selected paperless delivery through Online Services for your deposit account documents. If you use one of our information reporting services, you may receive notice through that service.<br />
            We are not obligated to send you a separate notice of each incoming funds transfer. While we generally do not provide such separate notices, we may do so on occasion, in which case we send the notice within two business days after we credit your account.<br />
            We are not obligated to pay you interest for the period before you receive notice.<br />
            If you are expecting a funds transfer and want to find out if it has been credited to your account, call us at the number for customer service.<br />
            <B>Posting Your Customers’ Payments</B> We credit to your account electronic payments (such as bill payments) that we receive from your customers. If you do not apply a payment to an account of your customer, you must promptly return the payment to us.<br />
        </TextBody>
        <TextBody>
            <SubHeader>ACH Debits and Credits</SubHeader>
            From time to time, originators that you authorize may send automated clearing house (ACH) credits or debits for your account. For each ACH transaction, you agree that the transaction is subject to the National Automated Clearing House Association (NACHA) Operating Rules and any local ACH operating rules then in effect. You agree that we may rely on the representations and warranties contained in these operating rules and either credit or debit your account, as instructed by the originator of the ACH transaction.<br />
            You should be careful about giving someone your account number to help prevent unauthorized transactions on your account. You must notify us immediately of unauthorized activity.<br />
            <B>Business deposit accounts</B> You acknowledge and agree that if you request us to transmit an ACH return transaction in connection with any problem, including a claim of erroneous or unauthorized ACH debit posted to your account, the related originating depository financial institution has no obligation to accept that return transaction if the return request is not made within the applicable time frame set forth in the NACHA Operating Rules. We will respond to your reported problem and attempt to pursue your request with the originating depository financial institution as long as you report the problem to us in writing within 60 days after the statement first reflecting the transaction was mailed to you; however, we do not guarantee that we will be able to recover your funds if you notify us of the problem beyond NACHA time frames. In some cases, depending on the facts, your claim may not be honored and you could incur a loss.<br />
        </TextBody>
        <BigHeader>Tax Information</BigHeader>
        <TextBody>
            When you open an account, we are required to obtain — and each U.S. citizen or resident alien must give us — a certified U.S. Taxpayer Identification Number (TIN) and information regarding your backup withholding status. When you apply for an account, you certify that you have provided the correct TIN for the account holder and the correct backup withholding status.
        </TextBody>
        <TextBody>
            For individual accounts, the TIN is your Social Security Number (SSN). For individual accounts with more than one owner, we report taxpayer information for the person listed first in our records.
        </TextBody>
        <TextBody>
            Resident aliens who do not qualify for Social Security should provide their Individual Taxpayer Identification Number (ITIN). For other accounts, the TIN is the owner’s Employer Identification Number (EIN). If you do not give us a certified name and TIN, if the IRS notifies us that the name and TIN you gave us is incorrect, or if the IRS notifies us that you failed to report all your interest and dividends on your tax return, we are required to backup withhold at the current backup withholding rate on interest paid to your account and pay it to the IRS. In some cases, a state and local tax authority may also require that we pay state and local backup withholding on interest paid to your account when we are required to pay backup withholding to the IRS. Backup withholding is not an additional tax. If you are subject to backup withholding, we are required to report to you and to the IRS regardless of the amount of the interest payment. You may claim amounts withheld and paid to the IRS as a credit on your federal income tax return.
        </TextBody>
        <TextBody>
            If you are a certified nonresident alien individual, you are generally exempt from backup withholding on interest but may be subject to information reporting if you reside in a country in which we are required to report. If you are a certified foreign entity, you are generally exempt from backup withholding and information reporting for interest payments. Deposit interest income that is effectively connected with the conduct of a trade or business in the United States is subject to information reporting.
        </TextBody>
        <TextBody>
            You must renew your status as an exempt foreign person or entity prior to the end of the third calendar year following the year in which you last certified your status. If you fail to renew your status by the last day of the fourth calendar year, your interest payments will be subject to backup withholding. If you become a U.S. citizen or resident after opening your account, you must notify us within 30 days and provide us with your certified name and TIN.
        </TextBody>
        <TextBody>
            We comply with Foreign Account Tax Compliance Act (FATCA) as mandated by U.S. federal tax law. We will withhold on certain payments when required by such law.
        </TextBody>
        <TextBody>
            For more information or to determine how this information applies to you, consult your U.S. tax advisor.
        </TextBody>
        <BigHeader>
            Resolving Claims
        </BigHeader>
        <TextBody>
            If you and we are not able to resolve a claim ourselves, then you and we agree that the claim will be resolved as provided in this Resolving Claims section. This is a dispute resolution provision.
        </TextBody>
        <TextBody>
            Please read it carefully.
        </TextBody>
        <TextBody>
            <SubHeader>What does "Claim" Mean?</SubHeader>
            Claim means any claim, dispute or controversy (whether under a statute, in contract, tort, or otherwise and whether for money damages, penalties or declaratory or equitable relief) by either you or us against the other, or against the employees or agents of the other, arising from or relating in any way to this deposit agreement (including any renewals, extensions or modifications) or the deposit relationship between us.
        </TextBody>
        <TextBody>
            Claim does not include provisional or ancillary remedies from a court of competent jurisdiction, which either you or we may exercise without waiving the right to arbitration or reference.
        </TextBody>
        <TextBody>
            <B>How Claims on Personal Accounts will be Resolved</B>
            You and we both agree that all Claims relating to a personal account will be resolved in court by a judge without a jury, as permitted by law.
        </TextBody>
        <TextBody>
            <B>
                JURY TRIAL WAIVER FOR PERSONAL Accounts <br />
                FOR PERSONAL ACCOUNTS, AS PERMITTED BY LAW, YOU AND WE AGREE AND UNDERSTAND THAT YOU AND WE ARE BOTH GIVING UP THE RIGHT TO TRIAL BY JURY. THIS IS A JURY TRIAL WAIVER.
            </B>
        </TextBody>
        <TextBody>
            <B>How Claims on Business Accounts will be Resolved</B>
            You have the right to compel us at your option, and we have the right to compel you at our option, to resolve a Claim relating to a business account by binding arbitration. If neither you nor we decide to compel arbitration, then the Claim will be resolved in court by a judge without a jury, as permitted by law. There is an exception for Claims brought in a California state court. If a Claim relating to a business account is brought in a California state court, either you or we can seek to compel the other to have the Claim resolved by general reference to a judicial referee under California Code of Civil Procedure (C.C.P.) Section 638, as provided below. Both parties may also agree to resolve their disputes through judicial reference. The arbitration, judicial reference or trial by a judge will take place on an individual basis without resort to any form of class or representative action.
        </TextBody>
        <TextBody>
            <B>
                CLASS ACTION AND JURY TRIAL WAIVER FOR BUSINESS ACCOUNTS <br />
                FOR BUSINESS ACCOUNTS, YOU AND WE AGREE AND UNDERSTAND: (1) THAT YOU AND WE ARE BOTH GIVING UP THE RIGHT TO TRIAL BY JURY, AND (2) THAT THIS SECTION PRECLUDES YOU AND US FROM PARTICIPATING IN OR BEING REPRESENTED IN ANY CLASS OR REPRESENTATIVE ACTION OR JOINING OR CONSOLIDATING THE CLAIMS OF OTHER PERSONS. THIS IS A CLASS ACTION WAIVER AND JURY TRIAL WAIVER.
            </B>
        </TextBody>
        <TextBody>
            <SubHeader>Judicial Reference</SubHeader>
            A case sent to judicial reference is heard by a neutral individual (a “judicial referee”), but remains in the court system subject to the same rules of procedure, discovery and evidence and appeal as any court case. The judicial referee will be an active or retired judge or attorney with more than 10 years of experience, chosen by mutual agreement of you and us.
        </TextBody>
        <TextBody>
            If you and we are unable to agree on a judicial referee, then the judicial referee will be appointed according to the procedure for appointment of a referee under California C.C.P. Section 640.
        </TextBody>
        <TextBody>
            The judicial referee, sitting alone without a jury, will decide questions of law and fact and will resolve the Claim. This includes the applicability of this Resolving Claims section and the validity of the deposit agreement.
        </TextBody>
        <TextBody>
            Judicial reference will be governed by California C.C.P. Section 638 at seq. and the judicial referee will determine all issues in accordance with federal and California law and the California rules of evidence. The referee is empowered to provide all temporary or provisional remedies and rule on any motion that would be authorized in pretrial or trial proceedings in court, including motions for summary judgment or summary adjudication. The award that results from the decision of the referee will be entered as a judgment in the court that appointed the referee, in accordance with the provisions of California C.C.P. Sections 644(a) and 645. You and we both reserve the right to seek appellate review of any judgment or order to the same extent permitted in a court of law.
        </TextBody>
        <TextBody>
            <SubHeader>Arbitration</SubHeader>
            This section on arbitration applies to business accounts and is subject to the provisions of the Limitation and Non-Severability section below.
        </TextBody>
        <TextBody>
            Arbitration is a method of resolving disputes in front of one or more neutral individuals, instead of having a trial in court in front of a judge and/or jury. The arbitrator will be an active or retired judge or attorney with more than 10 years of experience, chosen by mutual agreement of you and us.
        </TextBody>
        <TextBody>
            If you and we are unable to agree on an arbitrator, then you agree to choose one of the following Administrators within 10 days of our written notice that an agreement cannot be reached.
            <ul>
                <li>JAMS Resolution Center 160 W Santa Clara St #1600, San Jose, CA 95113 www.jamsadr.com (408) 288-2240</li>
                <li>American Arbitration Association (“AAA”) One Sansome Street, Suite 1600, San Francisco, CA 94104 www.adr.org 800-778-7879</li>
            </ul>
        </TextBody>
        <TextBody>
            If you do not choose the Administrator on a timely basis, we will select the Administrator and the Administrator will select the arbitrator using the Administrator’s rules. If an Administrator cannot hear or refuses to hear the arbitration, then the arbitration will be handled by the alternative Administrator.
        </TextBody>
        <TextBody>
            The arbitrator, sitting alone without a jury, will decide questions of law and fact and will resolve the Claim. This includes the applicability of this Resolving Claims section and the validity of the deposit agreement, except that the arbitrator may not decide or resolve any Claim challenging the validity of the class action and jury trial waiver. The validity of the class action and jury trial waiver will be decided only by a judicial referee or a court.
        </TextBody>
        <TextBody>
            After a decision is given by an arbitrator, and where the amount of the Claim exceeds $200,000, either you or we can appeal the arbitrator’s decision to another arbitrator. If the amount of the Claim exceeds $1,000,000, either you or we can appeal the arbitrator’s decision to a panel of three arbitrators. No decision may be appealed under this paragraph, unless the arbitrator that heard the matter first makes a finding that the Claim could reasonably have exceeded either $200,000 or $1,000,000. Any arbitrator who hears an appeal under this paragraph will be selected according to the rules of the Administrator.
        </TextBody>
        <TextBody>
            The arbitration of any matter involves interstate commerce and is governed by the Federal Arbitration Act, 9 U.S.C. §§ 1 et seq. (the “FAA”). The arbitrator will follow applicable substantive law to the extent consistent with the FAA. The arbitrator will give effect to the applicable statutes of limitation and will dismiss barred claims. Arbitrations will be governed by the rules of the Administrator to the extent those rules do not conflict with this Resolving Claims section. In addition, you or we may submit a written request to the arbitrator to expand the scope of discovery normally allowable. At the timely request of either you or us, the arbitrator must provide a brief written explanation of the basis for the award.
        </TextBody>
        <TextBody>
            Judgment upon the award given by the arbitrator may be entered in any court having jurisdiction. The arbitrator’s decision is final and binding, except for any right of appeal provided by the FAA or under this Agreement.
        </TextBody>
        <TextBody>
            <SubHeader>Limitation and Non-Severability</SubHeader>
            <B>For both personal and business accounts.</B> Regardless of anything else in this Resolving Claims section, you and we both acknowledge and agree that the validity and effect of the class action and jury trial waiver for business accounts and the jury trial waiver for personal accounts may be determined only by a court or judicial referee and not by an arbitrator. You and we both have the right to appeal the limitation or invalidation of the waiver.
        </TextBody>
        <TextBody>
            <B>For business accounts. </B>Regardless of anything else in this Resolving Claims section, you and we both acknowledge and agree that the class action and jury trial waiver is material and essential to the arbitration of any disputes between you and us and is non-severable from the agreement to arbitrate Claims. If the class action and jury trial waiver is limited, voided or found unenforceable, then the agreement to arbitrate (except for this sentence) will be null and void with respect to such proceeding and this Resolving Claims section will be read as if the provisions regarding arbitration were not present. You and we both have the right to appeal the limitation or invalidation of the class action and jury trial waiver. You and we acknowledge and agree that under no circumstances will a class action be arbitrated.
        </TextBody>
        <TextBody>
            <SubHeader>Rules of Interpretation</SubHeader>
            Except as provided in the Limitation and Non-Severability section above, if any portion of this Resolving Claims section is determined to be invalid or unenforceable, it will not invalidate the remaining portions of this section. If there is a conflict or inconsistency between this Revolving Claims section and other terms of this deposit agreement or the applicable rules of the Administrator, this Resolving Claims section will govern. If there is any conflict between this Revolving Claims section and any other dispute provision (whether it be for arbitration, reference or any other form of dispute resolution), this Resolving Claims section will prevail for Claims arising out of this deposit agreement or transactions contemplated by this deposit agreement.
        </TextBody>
        <TextBody>
            <SubHeader>Jurisdiction and Venue</SubHeader>
            Any action or proceeding regarding your account or this deposit agreement must be brought in the state in which the financial center that maintains your account is located. You submit to the personal jurisdiction of that state. Note that any action or proceeding will be governed by and interpreted in accordance with the Governing Law section of this agreement.
        </TextBody>
        <TextBody>
          If a Claim is submitted to arbitration and the state where that financial center is located is not reasonably convenient for you, then you and we will attempt to agree on another location. If you and we are unable to agree on another location, then the location will be determined by the Administrator or arbitrator.
        </TextBody>
      </BodyWrapper>
    </CardBox>
  </ContainerWrapper>
);

DepositAgreement.propTypes = {
    setBack: PropTypes.func.isRequired,
};
export default DepositAgreement;
