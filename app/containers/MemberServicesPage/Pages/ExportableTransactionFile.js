import React from 'react';
import titanLogo from 'images/titan-logo.png';
import allTransaction from 'images/all-transaction.png';
import quickbooks from 'images/quickbooks.png';
import csvReport from 'images/csv-report.png';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import PropTypes from 'prop-types';
import { Image, Table } from 'semantic-ui-react';

import {
  B,
  TextBody,
  BodyWrapper,
  HeaderPlain,
  HeaderLeft,
  HeaderCenter,
  Img,
  A,
  HR,
  HeaderColored,
} from './StyleUtils';
/* eslint-disable */
const ExportableTransactionFile = ({ setBack }) => (
  <ContainerWrapper>
    <CardBox>
      <Img src={titanLogo} />
      <A onClick={() => setBack()}>Back</A>
      <HR />
      <BodyWrapper>
        <HeaderCenter>EXPORTABLE TRANSACTION FILE</HeaderCenter>
        <TextBody>
          Members can easily import their account transaction to most accounting systems (ie. Quickbooks, Zero,
          Intaact using exportable comma-separated values (“CSV”) files.
        </TextBody>
        <TextBody>
          CSV files are simple text files containing tabular data. Each field in the file is separated from the next by
          a comma. Most spreadsheets support this format, although you can create and edit CSV files with any
          text editor like Notepad. The most commonly viewed CSV format is MS Excel CSV
        </TextBody>
        <HeaderColored>Export CSV files</HeaderColored>
        <TextBody>
          <ol>
            <li>
              Sign in to the Member Dashboard (See Image 1.1)
            </li>
            <li>
              Scroll down and locate “All Transaction detail”
            </li>
            <li>
              Export Transactions you want using the filter
              <ul>
                <li>
                  By not selecting any specific filter, the file will include all transaction including open transactions.
                </li>
              </ul>
            </li>
            <li>
              Click the “Export to CSV” button
            </li>
          </ol>
        </TextBody>
        <TextBody>
          <div style={{ textAlign: 'center' }}>Image 1.1</div>
          <Image src={allTransaction} centered />
        </TextBody>
        <HeaderColored>File Location</HeaderColored>
        <TextBody>
          <ul>
            <li>
              File will automatically save to your local download drive.
            </li>
            <li>
              Locate, open, and edit the file as needed. Recommend to open file with MS Excel.  See Image 1.2
            </li>
          </ul>
        </TextBody>
        <TextBody>
          <div style={{ textAlign: 'center' }}>Image 1.2</div>
          <Image src={csvReport} centered />
        </TextBody>
        <HeaderLeft>
          File Output fields include:
        </HeaderLeft>
        <TextBody>
          <Table celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Titan Vault Export FIELD</Table.HeaderCell>
                <Table.HeaderCell>Description of Field</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell>Type</Table.Cell>
                <Table.Cell>Deposit or payment</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>member name</Table.Cell>
                <Table.Cell>TV Account Name</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>user name</Table.Cell>
                <Table.Cell>Authorized User </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>member_id</Table.Cell>
                <Table.Cell>TV Unique Login ID</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>transaction_id</Table.Cell>
                <Table.Cell>Record number</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Transaction_date</Table.Cell>
                <Table.Cell>Date of Transaction</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>sent_to</Table.Cell>
                <Table.Cell>Recipient Name</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>send_to_memo</Table.Cell>
                <Table.Cell>Description</Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </TextBody>
        <HeaderColored>
          <u>How to import transactions using Excel CSV files</u>
        </HeaderColored>
        <TextBody>
          <ul>
            <HeaderLeft> > Quickbooks-specific import rules:</HeaderLeft>
            <ul>
              QuickBooks Online can import CSV files that use either a 3-column or 4-column format. Titan-Vault CSV files will support Quickbooks’ 3-column format. (See Image 1.3)
            </ul>
            <br />
            <ul>
              <HeaderPlain>3 Columns</HeaderPlain>
              The 3-column template has columns for the Date, a Description of the transaction, and the Amount of the transaction.
              <div style={{ textAlign: 'center' }}>
                Image 1.2
                <Image src={quickbooks} centered />
              </div>
            </ul>
          </ul>
          <ul>
            <HeaderLeft> > Troubleshooting & Format Variance</HeaderLeft>
            <ul>
              <li>
                Your CSV file must be formatted correctly. Please ensure your system is set up to map Titan Vault’s data fields.  If you try to upload an incorrectly-formatted file, you will receive an error.
              </li>
              <li>
                Each bank formats its CSV files differently, so importing a CSV file may not be possible for every financial institution.  If your ERP Accounting software does not support CSV files, please contact MemberServics@titan-vault.com so we are aware and add that to our development roadmap.
              </li>
            </ul>
          </ul>
        </TextBody>
        <HeaderPlain>Formatting CSV files for import</HeaderPlain>
        <TextBody>
          If you experience any issues importing your CSV file, make sure your file doesn't break any of the following formatting restrictions:
          <ul>
            <li>
              Special characters are not allowed. Because of this restriction:
              <ul>
                <li>
                  Do not use characters such as <B>#, %,</B> and <B>&</B> anywhere in the file.
                </li>
                <li>
                  Do not use <B>( )</B> for negative amounts (use <B>-234</B>, not <B>(234)</B> for negative figures)
                </li>
                <li>
                  Do not include currency symbols (<B>$234</B> should be <B>234</B>)
                </li>
                <li>
                  Do not use commas to separate thousands (<B>2, 111</B> should be <B>2111</B>)
                </li>
              </ul>
            </li>
            <li>
              Zeros <B>(0)</B> are not allowed anywhere in the file (<B>$0</B> should be left blank)
            </li>
            <li>
              Correct any transactions that display an amount in the <B>Description</B> column
            </li>
            <li>
              Remove the word <B>amount</B> if it appears in the name of the <B>Credit</B> or <B>Debit</B> column (<B>Credit amount</B> and <B>Debit amount</B> are incorrect)
            </li>
            <li>
              <B>MAC</B> users must save the file as a <B>Windows CSV</B> file
            </li>
            <li>
              Correct and standardize the <B>Date</B> format to a single format (for example, <B>dd/mm/yyyy</B>)
            </li>
            <li>
              Some banks provide the day of the week and include it in the Date column (for example, <B>20/11/2018 TUE</B> in column A) which is not an acceptable format. To correct this, split the date and the day of the week in two separate columns.
              <ol>
                <li>Highlight the column that contains the date.</li>
                <li>Go to <B>Data</B> then select <B>Text to Columns.</B></li>
                <li>
                  Follow the Convert Text to Columns Wizard, choosing the following:
                  <ul>
                    <li>
                      Original data type: <B>Delimited</B>
                    </li>
                    <li>
                      Delimiters: <B>Space</B>
                    </li>
                    <li>
                      Column data format: <B>General</B>
                    </li>
                  </ul>
                </li>
                <li>
                  Select <B>Finish.</B>
                </li>
              </ol>
            </li>
          </ul>
        </TextBody>
      </BodyWrapper>
    </CardBox>
  </ContainerWrapper>
);

ExportableTransactionFile.propTypes = {
  setBack: PropTypes.func.isRequired,
};
export default ExportableTransactionFile;
