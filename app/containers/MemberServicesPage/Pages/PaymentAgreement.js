import React from 'react';
import titanLogo from 'images/titan-logo.png';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import PropTypes from 'prop-types';

import {
  B,
  SubHeader,
  TextBody,
  BodyWrapper,
  HeaderLeft,
  HeaderCenter,
  Img,
  A,
  HR,
} from './StyleUtils';

const PaymentAgreement = ({ setBack }) => (
  <ContainerWrapper>
    <CardBox>
      <Img src={titanLogo} />
      <A onClick={() => setBack()}>Back</A>
      <HR />
      <BodyWrapper>
        <HeaderCenter>
          PAYMENT AGENT AND NETWORK PARTICIPATION AGREEMENT
        </HeaderCenter>
        <TextBody>
          This Payment Agent and Network Participation Agreement (this
          “Agreement”) and the applicable Schedule of Fees are part of the
          binding contract between you and us (collectively referred to as the
          “Parties”) for your account and your relationship with us. This
          Agreement and the Schedule of Fees, along with the terms and
          conditions of the Deposit Agreement encompass all of the terms of our
          relationship with you. Please read all of these documents carefully.
          In referring to the Parties individually, this Agreement may refer to
          “you,” “your,” “your Company,” or “Payee” in reference to you and
          “us,” “we,” “our” or the Payment Agent in reference to Titan Vault,
          LLC or Athena Pay, LLC.
        </TextBody>
        <HeaderLeft>CERTAIN DEFINED TERMS</HeaderLeft>
        <TextBody>
          <B>“The Network”</B> means the universe of companies that have entered
          into a relationship with us, the Payment Agent, to accept payments
          into a custodial account on their behalf and disburse those payments
          to other network participants to whom those funds are owed.
        </TextBody>
        <TextBody>
          <B>“Payee”</B> means the provider of goods or services who is owed
          payment from the Payor.
        </TextBody>
        <TextBody>
          <B>“Payor”</B> means the recipient of goods or services, who owes
          payment to the Payee.
        </TextBody>
        <HeaderLeft>NATURE OF THE OBLIGATION</HeaderLeft>
        <TextBody>
          <B>Establishment of Account;</B> Deposits. We will establish a
          custodial deposit account (the “Custodial Account”) on your behalf
          inside Titan Vault or Athena Pay to receive, hold, and distribute the
          amounts contained therein to be disbursed to other Specified Network
          Participants pursuant to the provisions of this Agreement. You direct
          us to hold any funds delivered to us for deposit in the Custodial
          Account pursuant to this Agreement un-invested. You agree that there
          will not be any interest on amounts that are deposited in the Account.
        </TextBody>
        <TextBody>
          <B>Appointment of Payment Agent.</B> Solely with respect to the
          payment of money from one Network Participant, the Payor, to another
          Network Participant, the Payee, you appoint us as your agent to
          represent you and act on your behalf. You as Payee acknowledge and
          agree that payment of money from the Payor to us for delivery to you
          the Payee satisfies and discharges the Payor’s obligations to the
          Payee to the extent of such payment. You hereby agree that you will
          only deposit funds into the account when you have an existing payment
          obligation and that the deposit amount will only be used to satisfy
          that existing payment obligation and the amount will not exceed the
          amount of that payment obligation. The Parties accept such appointment
          pursuant to the terms and conditions set forth in this Agreement.
        </TextBody>
        <HeaderLeft>
          DISBURSEMENTS, TERMINATION AND RESIGNATION AND REMOVAL
        </HeaderLeft>
        <TextBody>
          <B>Disbursements of Funds in the Custodial Account.</B> We will only
          comply with written payment instructions received from you (the
          “Company Remittance Instructions”) with respect to the remittance of
          funds in the Custodial Account. As soon as practicable following our
          receipt of the Company Remittance Instructions, we shall remit funds
          in the Custodial Account to the Specified Network Participant in
          accordance with such Company Remittance Instructions. You agree that
          we may use affiliates (such as Athena Pay) to complete such
          remittances. You also agree that clicking “Pay” or another equivalent
          on Titan Vault or Athena Pay shall be considered a Company Remittance
          Instruction from you to us.
        </TextBody>
        <TextBody>
          <B>Termination of Agreement.</B> This Agreement may only be terminated
          by the written agreement of the parties hereto.
        </TextBody>
        <TextBody>
          <B>Resignation and Removal of Payment Agent.</B> We may resign from
          the performance of our duties hereunder at any time by giving thirty
          (30) days’ prior written notice to you or may be removed, with or
          without cause, by you at any time by giving thirty (30) days’ prior
          written notice to us. Such resignation or removal shall take effect
          upon the appointment of a successor Payment Agent by Company. Upon the
          acceptance in writing of any appointment as Payment Agent hereunder by
          a successor Payment Agent, such successor Payment Agent shall
          thereupon succeed to and become vested with all the rights, powers,
          privileges and duties of the retiring Payment Agent, and the retiring
          Payment Agent shall be discharged from its duties and obligations
          under this Agreement, but shall not be discharged from any liability
          for actions taken as Payment Agent under this Agreement, prior to such
          succession. After any retiring Payment Agent’s resignation or removal,
          the provisions of this Agreement shall inure to its benefit as to any
          actions taken or omitted to be taken by it while it was Payment Agent
          under this Agreement. The Payment Agent shall have the right to
          withhold an amount equal to any amount due and owing to the Payment
          Agent as of the date its resignation or removal becomes effective plus
          any costs and expenses the Payment Agent reasonably believes it will
          incur in connection with its resignation or removal hereunder.
        </TextBody>
        <HeaderLeft>RESPONSIBILITIES OF PAYMENT AGENT</HeaderLeft>
        <TextBody>
          <B>Payment Agent Responsibilities.</B> Our acceptance of our duties
          under this Agreement is subject to the following terms and conditions,
          which we all agree shall govern and control with respect to its
          rights, duties, liabilities and immunities:
          <ol type="i">
            <li>
              Except as to our due execution and delivery of this Agreement, we
              make no representation and have no responsibility as to the
              validity of this Agreement or of any other instrument referred to
              herein, or as to the correctness of any statement contained
              herein, and we shall not be required to inquire as to the
              performance of any obligation under any other agreement;
            </li>
            <li>
              We shall be protected in acting upon any written notice, request,
              waiver, consent, receipt or other paper or document, not only as
              to its due execution and the validity and effectiveness of its
              provisions, but also as to the truth of any information therein
              contained, which we in good faith believe to be genuine and what
              it purports to be;
            </li>
            <li>
              We may consult with competent and responsible legal counsel
              selected by us and we shall not be liable for any action taken or
              omitted by it in good faith in accordance with the advice of such
              counsel;
            </li>
            <li>
              You shall reimburse us for all reasonable expenses incurred by us
              in connection with our duties hereunder (other than taxes imposed
              in respect of the receipt of fees by the Payment Agent). You agree
              to indemnify, defend and hold us and our directors, employees,
              officers, agents, successors and assigns (collectively, the
              “Indemnified Parties”) harmless from and against any and all
              losses, claims, damages, liabilities and expenses including,
              without limitation, reasonable costs of attorney’s fees and
              expenses (collectively, “Damages”), incurred by us without gross
              negligence or willful misconduct on our part and arising out of or
              in connection with the performance by us of our duties under this
              Agreement. Such indemnity includes, without limitation, damages
              incurred in connection with any litigation arising from this
              Agreement or involving the subject matter hereof. The
              indemnification provisions contained in this section are in
              addition to any other rights any of the Indemnified Parties may
              have by law or otherwise and shall survive the termination of this
              Agreement or the resignation or removal of the Payment Agent;
            </li>
            <li>
              We shall have no duties or responsibilities except those expressly
              set forth herein, and we shall not be bound by any modification of
              this Agreement unless in writing and signed by all parties hereto
              or their respective successors in interest;
            </li>
            <li>
              We shall have no responsibility in respect of the validity or
              sufficiency of this Agreement or of the terms hereof;
            </li>
            <li>
              We shall be protected by you in acting upon any notice,
              resolution, request, consent, order, certificate, report, opinion,
              bond or other paper or document reasonably believed by it to be
              genuine and to have been signed and presented by the proper party
              or parties. Whenever we deem it necessary or desirable that a
              matter be proved or established prior to taking or suffering any
              action under this Agreement, such matter may be deemed
              conclusively proved and established by a certificate signed by
              Company, and such certificate shall be full warranty for any
              action taken or suffered in good faith under the provisions of
              this Agreement;
            </li>
            <li>
              We do not have any interest in the Custodial Account, but we are
              serving as Payment Agent only and having only possession thereof;
            </li>
            <li>
              WE SHALL NOT BE LIABLE, DIRECTLY OR INDIRECTLY, FOR ANY (I)
              DAMAGES, LOSSES OR EXPENSES ARISING OUT OF THE SERVICES PROVIDED
              HEREUNDER, OTHER THAN DAMAGES, LOSSES OR EXPENSES WHICH HAVE BEEN
              FINALLY ADJUDICATED TO HAVE DIRECTLY RESULTED FROM OUR GROSS
              NEGLIGENCE OR WILLFUL MISCONDUCT, OR (II) SPECIAL, INDIRECT OR
              CONSEQUENTIAL DAMAGES OR LOSSES OF ANY KIND WHATSOEVER (INCLUDING
              WITHOUT LIMITATION LOST PROFITS), EVEN IF WE HAVE BEEN ADVISED OF
              THE POSSIBILITY OF SUCH LOSSES OR DAMAGES AND REGARDLESS OF THE
              FORM OF ACTION;
            </li>
            <li>
              No provision of this Agreement shall require us to risk or advance
              our own funds or otherwise incur any financial liability or
              potential financial liability in the performance of its duties or
              the exercise of its rights under this Agreement; and
            </li>
            <li>
              If any portion of the funds in the Custodial Account shall be
              attached, garnished or levied upon by any court order, or the
              delivery thereof shall be stayed or enjoined by an order of a
              court, or any order, judgment or decree shall be made or entered
              by any court order affecting funds in the Account, we are hereby
              expressly authorized, in our sole discretion, to respond as it
              deems appropriate or to comply with all writs, orders or decrees
              so entered or issued, or which it is advised by legal counsel of
              our own choosing is binding upon it, whether with or without
              jurisdiction. If we obey or comply with any such writ, order or
              decree we shall not be liable to you or to any other person, firm
              or corporation, should, by reason of such compliance
              notwithstanding, such writ, order or decree be subsequently
              reversed, modified, annulled, set aside or vacated.
            </li>
          </ol>
        </TextBody>
        <TextBody>
          <B>
            <u>FEES AND EXPENSES.</u>
          </B>{' '}
          You shall compensate us for our services hereunder in accordance with
          the Schedule of Fees and, in addition, shall reimburse us for all
          costs and expenses, including reasonable attorneys’ fees, occasioned
          by any delay, controversy, litigation or event arising out of the
          transactions contemplated by this Agreement. All of the compensation
          and reimbursement obligations set forth in this Section shall be
          payable as soon as practicable after submission by us to you of an
          invoice detailing such expenses. You expressly allow us to deduct any
          fees, costs and expenses from any other Titan Vault or Athena Pay
          account that you control. The obligations of Company under this
          Section shall survive any termination of this Agreement and the
          resignation or removal of the Payment Agent.
        </TextBody>
        <HeaderLeft>MISCELLANEOUS</HeaderLeft>
        <TextBody>
          <B>Binding Agreement.</B> When you request to make a payment, you
          acknowledge that you have reviewed and understand the terms of this
          Agreement and you agree to be governed by these terms. You understand
          that these terms, as we may change or supplement them periodically,
          are a binding contract between you and us for your payment.
        </TextBody>
        <TextBody>
          <B>Modifications to This Agreement.</B> We may change this Agreement
          at any time. We may add new terms. We may delete or amend existing
          terms. We may add new services and discontinue existing services. If
          you continue to make payments, you are deemed to accept and agree to
          the change and are bound by the change.
        </TextBody>
        <TextBody>
          <B>Governing Law.</B> This Agreement shall be construed, performed and
          enforced in accordance with the laws of the State of California
          applicable to contracts made and to be performed in such State.
        </TextBody>
        <TextBody>
          <SubHeader>Other</SubHeader>
          <ol type="i">
            <li>
              This Agreement shall be binding upon and inure to the benefit of
              the parties and their successors. Neither this Agreement nor any
              right or interest hereunder may be assigned in whole or in part by
              any party, except as provided in Section entitled “Resignation and
              Removal of Payment Agent,” without the prior consent of the other
              parties.
            </li>
            <li>
              We agree that if any provision of this Agreement shall under any
              circumstances be deemed invalid or inoperative, this Agreement
              shall be construed and enforced accordingly, and the invalidity of
              any portion of this Agreement shall not affect the validity of the
              remainder hereof.
            </li>
            <li>
              No party to this Agreement is liable to any other party for losses
              due to, or if it is unable to perform its obligations under the
              terms of this Agreement because of, acts of God, fire, floods,
              earthquake, strikes, equipment or transmission failure, war, riot,
              nuclear accident, terror attack, computer piracy, cyber-terrorism,
              or other causes reasonably beyond its control.
            </li>
          </ol>
        </TextBody>
      </BodyWrapper>
    </CardBox>
  </ContainerWrapper>
);

PaymentAgreement.propTypes = {
  setBack: PropTypes.func.isRequired,
};
export default PaymentAgreement;
