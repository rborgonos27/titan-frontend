import React from 'react';
import titanLogo from 'images/titan-logo.png';
import paymentRequestPage from 'images/payment_request_page.png';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import PropTypes from 'prop-types';
import { Table, Image } from 'semantic-ui-react';
import {
  B,
  TextBody,
  BodyWrapper,
  HeaderLeft,
  HeaderCenter,
  Img,
  A,
  HR,
  HeaderColored,
} from './StyleUtils';

const rowRedStyle = {
  backgroundColor: 'red',
  color: '#FFF',
  textAlign: 'center',
};
const MakeDeposit = ({ setBack }) => (
  <ContainerWrapper>
    <CardBox>
      <Img src={titanLogo} />
      <A onClick={() => setBack()}>Back</A>
      <HR />
      <BodyWrapper>
        <HeaderCenter>How to Make a Payment</HeaderCenter>
        {/* eslint-disable */}
        <TextBody>
          Making a payment on the Titan Vault (“TV”)  platform is super simple. Preparing for deposits to Titan Vault (“TV”) Titan Vault Member portal. Follow preparation requirements and best practices to ensure timely deposits to your account.
        </TextBody>
        <HeaderLeft>Document Content:</HeaderLeft>
        <TextBody>
          ❖	Make Payment Screen <br />
          <ul>➢	Member Payment Entry <br /></ul>
          ❖	Titan’s Payment Status <br />
        </TextBody>
        <HeaderColored><u>Make Payment Screen</u></HeaderColored>
        <TextBody>
          {/*depositRequestPage*/}
          <Image src={paymentRequestPage} centered />
        </TextBody>
        <TextBody>
          <ol>
            <li><B>Member Payment Entry Fields </B>(via TV portal)</li>
            <ol type="a">
              <li><B>PAYEE</B> Select whether deposit will be made in cash (requires armor partner pickup) or electronic (refer to electronic deposit procedures.) Note: if checks are allowed, choose paper as the option.</li>
              <li><B>AMOUNT</B> Enter deposit amount.</li>
              <li>
                <B>UPLOAD BACKUP</B> Documentation supporting the deposit request  that will allow Titan Officer  to validate the payment need (ie. An invoice you’re paying, rental agreement, tax payment stub, payroll report)
                <ol type="i">
                  <li>
                    <B>Note:</B> <i>payment requests without documentation will not be processed. Please speak with a Titan Officer prior to scheduling payment should you have any questions.</i>
                  </li>
                </ol>
              </li>
              <li>
                <B>SCHEDULE PAYMENT</B> DATE Select date the payment must be made.  If it is a cash deposit is required prior to payment, please plan deposit at least 10 business days prior to scheduled payment date selected.  Payment can be made within 48 hours if request is approved prior to 2pm each business day.
              </li>
              <li>Member provide supporting invoice or relevant documentation related to your intended payment deposit from the payor. </li>
              <li>
                <B>MEMO</B> is optional, this detail will be sent to your payee recipient.
              </li>
            </ol>
          </ol>
        </TextBody>
        <HeaderColored><u>Titan Payment Status</u></HeaderColored>
        <ol>
          <li>
            <B>Payment Review Process</B> (via TV portal)
            <ol type="a">
              <li>All payment request made in TV will be reviewed by a Titan Officer.</li>
              <li>Members may see status in TV Members Dashboard at any time. </li>
              <li>Payment transaction details will show the following statuses:</li>
              <Table celled>
                <Table.Header>
                  <Table.Row>
                    <Table.Cell>STATUS</Table.Cell>
                    <Table.Cell>DESCRIPTION</Table.Cell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  <Table.Row>
                    <Table.Cell><B>REQUESTED</B></Table.Cell>
                    <Table.Cell>Request submitted to Titan for review and approval</Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell><B>PROCESSING</B></Table.Cell>
                    <Table.Cell>Titan reviewed and have approve request.</Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell><B>SCHEDULED</B></Table.Cell>
                    <Table.Cell>Titan has confirmed deposit cash pickup (if applicable) and remains until funds received by Titan via Athena.</Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell><B>COMPLETED</B></Table.Cell>
                    <Table.Cell>
                      TV confirms funds have been received. <br />
                      TV Credit to Member Titan Vault account.
                    </Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>
            </ol>
          </li>
        </ol>
        <HeaderColored><u>Payee Recipient List</u></HeaderColored>
        <TextBody>
          Members’ TV account will pre-approve all Member Payee/Recipient list. New Payee requests will require 48 hours to approve and appear on dropdown.
        </TextBody>
        <TextBody>
          <Table celled>
            <Table.Header>
              <Table.Row style={  rowRedStyle}>
                <Table.Cell>PAYMENTS REQUEST BEST PRACTICES & THINGS TO KNOW</Table.Cell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell>
                  <ul>
                    <li>Providing supporting documents  are mandatory for all payment request.  Contact memberservices@titan-vault.com if you have any questions or require prior approval.</li>
                    <li>Allow minimum 3 business days for payment requests to be made timely to Titan Vault.  If funds are insufficient in the account, please plan ahead prior 10 business days to payment date allowing deposits to be collected and processed with banking partner.</li>
                    <li>Debit payment will be posted to TV account 24 hours after scheduled payment date.</li>
                    <li>Fees will be calculate based on the final payment date.</li>
                  </ul>
                </Table.Cell>
              </Table.Row>
              <Table.Row style={rowRedStyle}>
                <Table.Cell>
                  EACH MEMBERS SCENARIO MAY BE DIFFER. PLEASE CONTACT MEMBERSERVICES@TITAN-VAULT.COM IF THERE ARE ANY QUESTIONS.
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </TextBody>
      </BodyWrapper>
    </CardBox>
  </ContainerWrapper>
);

MakeDeposit.propTypes = {
  setBack: PropTypes.func.isRequired,
};
export default MakeDeposit;
