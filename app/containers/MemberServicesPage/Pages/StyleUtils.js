import styled from 'styled-components';

const Img = styled.img`
  align: right;
  width: 45px;
  height: 45px;
  position: absolute;
  right: 0px;
  margin-right: 40px;
`;

const HeaderCenter = styled.h4`
  text-align: center;
  margin-bottom: 20px;
  font-weight: bold;
`;

const HeaderLeftNoUnderline = styled.h4`
  text-align: left;
  margin-bottom: 20px;
  font-weight: bold;
`;

const HeaderLeft = styled.h5`
  text-align: left;
  margin-bottom: 20px;
  margin-top: 20px;
  font-weight: bold;
  text-decoration: underline;
`;
const HeaderPlain = styled.h5`
  text-align: left;
  margin-bottom: 20px;
  margin-top: 20px;
  font-weight: bold;
`;
const HeaderColored = styled.h5`
  text-align: left;
  margin-bottom: 20px;
  margin-top: 20px;
  font-weight: bold;
  color: #fc5109;
`;

const BodyWrapper = styled.div`
  margin-top: 20px;
  padding: 30px;
  text-align: justify;
`;

const TextBody = styled.div`
  text-align: justify;
  margin-bottom: 10px;
`;

const SubHeader = styled.div`
  font-weight: bolder;
  font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  color: #505458;
  margin-bottom: 10px;
  margin-top: 10px;
`;

const B = styled.span`
  font-weight: bolder;
  font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  color: #505458;
`;

const A = styled.a`
  &:hover {
    text-decoration: underline;
    color: #000;
    cursor: pointer;
  }
  text-decoration: underline;
  color: #000;
  cursor: pointer;
`;

const BigHeader = styled.h2`
  text-decoration: underline;
`;

const HR = styled.hr`
  border: 1px solid #fc5109;
  margin-top: 45px;
`;

export {
  B,
  SubHeader,
  TextBody,
  BodyWrapper,
  HeaderPlain,
  HeaderLeft,
  HeaderCenter,
  Img,
  A,
  BigHeader,
  HeaderLeftNoUnderline,
  HR,
  HeaderColored,
};
