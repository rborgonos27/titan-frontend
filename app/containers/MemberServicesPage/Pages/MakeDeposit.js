import React from 'react';
import titanLogo from 'images/titan-logo.png';
import depositRequestPage from 'images/deposit_request_page.png';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import PropTypes from 'prop-types';
import { Table, Image } from 'semantic-ui-react';
import {
  B,
  TextBody,
  BodyWrapper,
  HeaderLeft,
  HeaderCenter,
  Img,
  A,
  HR,
  HeaderColored,
} from './StyleUtils';

const rowRedStyle = {
  backgroundColor: 'red',
  color: '#FFF',
  textAlign: 'center',
};
const MakeDeposit = ({ setBack }) => (
  <ContainerWrapper>
    <CardBox>
      <Img src={titanLogo} />
      <A onClick={() => setBack()}>Back</A>
      <HR />
      <BodyWrapper>
        <HeaderCenter>How To Create a Deposit Request</HeaderCenter>
        {/* eslint-disable */}
        <TextBody>
          Deposits request created on the Titan Vault (“TV”)  platform is super simple. Preparing for deposits to Titan Vault (“TV”) Titan Vault Member portal. Follow preparation requirements and best practices to ensure timely deposits to your account.
        </TextBody>
        <HeaderLeft>Document Content:</HeaderLeft>
        <TextBody>
          ❖	Deposit Request Screen <br />
          <ul>➢	Member Deposit Entry <br /></ul>
          ❖	Titan’s Deposit Status <br />
          ❖	Titan’s Availability of Funds <br />
          ❖	Instructions for Payor to deposit into Member Account
        </TextBody>
        <HeaderColored><u>Deposit Request Screen</u></HeaderColored>
        <TextBody>
          {/*depositRequestPage*/}
          <Image src={depositRequestPage} centered />
        </TextBody>
        <TextBody>
          <ol>
            <li><B>Member Deposit Entry Fields </B>(via TV portal)</li>
            <ol type="a">
              <li><B>PAPER/ELECTRONIC</B> Select whether deposit will be made in cash (requires armor partner pickup) or electronic (refer to electronic deposit procedures.) Note: if checks are allowed, choose paper as the option.</li>
              <li><B>AMOUNT</B> Enter deposit amount.</li>
              <li>
                <B>IMMEDIATE/SCHEDULED</B> Selecting immediate allows Members to make <i>immediate</i> electronic deposit from payor accounts.  This schedule will be rare.
                <ol type="i">
                  <li>
                    Select <B>scheduled</B> for most deposit allows time for review and approval from Titan Officers.
                  </li>
                </ol>
              </li>
              <li>
                <B>UPLOAD BACKUP</B> Documentation supporting the deposit request  that will allow Titan Officer  to validate the source of deposit (ie. An invoice paid by a customer of yours, refund check from tax authorities)
                <ol type="i">
                  <li>
                    <B>Note:</B> <i>deposit requests without documentation may delay or resulted in hold in deposit. Please speak with a Titan Officer prior to scheduling deposit should you have any questions.</i>
                  </li>
                </ol>
              </li>
              <li>
                <B>SCHEDULE DEPOSIT</B> DATE Selected “estimated” date deposit will be made.  If it is a cash deposit via armored car, select deposit date typically 3 business days from the schedule cash pick up date, we can adjust based on the pick up date if needed)
                <ol type="i">
                  <li>
                    <B>Note:</B> <i>Final deposit date will be updated via Titan once deposit is confirmed.</i>
                  </li>
                </ol>
              </li>
              <li>Member provide supporting invoice or relevant documentation related to your intended payment deposit from the payor. </li>
              <li>
                <B>RECURRING/ON-DEMAND</B> Selected recurring allows Titan to know an offline recurring pickup has been scheduled with Head of Cash Logistics.  Selecting ONDEMAND <B>is required</B> if recurring pickup has not been arranged.  Titan officer will be in contact
              </li>
              <li>
                <B>CASH PICKUP</B> DATE will be automatically selected 3 days prior to deposit date above.  Head of Cash Logistic will confirm pickup date request via email shortly after deposit request is approved.
              </li>
              <li>
                <B>MEMO</B> is optional for any additional notes Members would like to provide Titan Officer.
              </li>
            </ol>
          </ol>
        </TextBody>
        <HeaderColored><u>Titan Deposit Status</u></HeaderColored>
        <ol>
          <li>
            <B>Deposit Review Process</B> (via TV portal)
            <ol type="a">
              <li>All deposit request made in TV will be reviewed by a Titan Officer.</li>
              <li>Members may see status in TV Members Dashboard at any time.</li>
              <li>Deposit transaction details will show the following statuses:</li>
              <Table celled>
                <Table.Header>
                  <Table.Row>
                    <Table.Cell>STATUS</Table.Cell>
                    <Table.Cell>DESCRIPTION</Table.Cell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  <Table.Row>
                    <Table.Cell><B>REQUESTED</B></Table.Cell>
                    <Table.Cell>Request submitted to Titan for review and approval</Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell><B>PROCESSING</B></Table.Cell>
                    <Table.Cell>Titan reviewed and have approve request. Awaiting deposit from payor.</Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell><B>SCHEDULED</B></Table.Cell>
                    <Table.Cell>Titan has confirmed deposit cash pickup (if applicable) and remains until funds received by Titan via Athena.</Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell><B>COMPLETED</B></Table.Cell>
                    <Table.Cell>
                      TV confirms funds have been received. <br />
                      TV Credit to Member Titan Vault account.
                    </Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>
            </ol>
          </li>
        </ol>
        <HeaderColored><u>Availability of Funds</u></HeaderColored>
        <TextBody>
          Members’ TV account will be credited immediately upon confirmation of funds received in Titan Vault’s Custodial account via affiliate Athena Pay, LLC.
        </TextBody>
        <TextBody>
          <Table celled>
            <Table.Header>
              <Table.Row style={  rowRedStyle}>
                <Table.Cell>DEPOSIT BEST PRACTICES & THINGS TO KNOW</Table.Cell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell>
                  <ul>
                    <li>Providing Supporting document for deposits from Member’s payors to each deposit request is recommended to avoid delays.  Failure to do may result in a delay. Contact memberservices@titan-vault.com if you have any questions or require prior approval.</li>
                    <li>Allow 3-4 business days for cash deposits to be made available to Titan Vault.</li>
                  </ul>
                </Table.Cell>
              </Table.Row>
              <Table.Row style={rowRedStyle}>
                <Table.Cell>
                  EACH MEMBERS SCENARIO MAY BE DIFFER. PLEASE CONTACT MEMBERSERVICES@TITAN-VAULT.COM IF THERE ARE ANY QUESTIONS.
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </TextBody>
      </BodyWrapper>
    </CardBox>
  </ContainerWrapper>
);

MakeDeposit.propTypes = {
  setBack: PropTypes.func.isRequired,
};
export default MakeDeposit;
