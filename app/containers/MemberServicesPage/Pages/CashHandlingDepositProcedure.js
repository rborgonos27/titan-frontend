import React from 'react';
import titanLogo from 'images/titan-logo.png';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import PropTypes from 'prop-types';
import { Table } from 'semantic-ui-react';

import {
  B,
  TextBody,
  BodyWrapper,
  HeaderLeft,
  HeaderCenter,
  Img,
  A,
  HR,
  HeaderColored,
} from './StyleUtils';
/* eslint-disable */
const CashHandlingDepositProcedure = ({ setBack }) => (
  <ContainerWrapper>
    <CardBox>
      <Img src={titanLogo} />
      <A onClick={() => setBack()}>Back</A>
      <HR />
      <BodyWrapper>
        <HeaderCenter>Cash Deposit Handling Procedures</HeaderCenter>
        <TextBody>
          Guidelines for all members preparing cash deposits to Titan Vault (“TV”) after request has been processed using Titan Vault Member portal. Follow preparation requirements and best practices to ensure timely deposits to your account and processing payments.
        </TextBody>
        <HeaderLeft>
          Document Content:
        </HeaderLeft>
        <ul>
          <li>
            Scheduling Deposit
            <ul>
              <li>Member Deposit Entry</li>
              <li>Recurring Pickup</li>
              <li>On-demand Pickup</li>
            </ul>
          </li>
          <li>Titan’s Deposit Review</li>
          <li>Preparing Cash-in-transit (“CIT”) Deposit</li>
          <li>Preparing Smartsafe Deposit</li>
          <li>Currency Requirement</li>
        </ul>
        <HeaderColored>
          <u>Scheduling Deposit</u>
        </HeaderColored>
        <TextBody>
          <ol>
            <li>
              <B>Member Deposit Entry</B> (via TV portal)
              <ol type="a">
                <li>
                  Member must initiate deposit request in Titan’s Members portal; completion of deposit amount and scheduled payment date must be completed in order to be processed.
                </li>
              </ol>
            </li>
            <li>
              <B>Recurring Pickup</B>
              <ol type="a">
                <li>
                  For Recurring cash deposit pickups that has been scheduled either weekly, bi-weekly, or monthly.
                </li>
                <li>
                  Any cancellation or changes to a recurring pickup must be made at least 48 business hours in advance before 4:00pm.  Email cash@titan-vault.com for any changes to your regularly scheduled pickup.
                </li>
                <li>
                  Deposit requests made shall be picked up at the earliest regularly scheduled time. (ie. Member has a recurring Wednesday pickup with a deposit request made on Thursday; cash will be picked up following Wednesday.)
                </li>
              </ol>
            </li>
            <li>
              <B>On-demand Pickup</B>
              <ol type="a">
                <li>
                  For members without a recurring pickup schedule or require special one-off pickups.
                </li>
                <li>
                  All cash pickups must be requested with Titan by 4:00 pm at least five (5) business days before the scheduled payment date.
                </li>
                <li>
                  At least one business day prior to your pickup you will receive a conﬁrmation email from Armored Car Partner conﬁrming your pickup date is scheduled.
                  <br />
                  <Table celled>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell>Cash Pickup Day</Table.HeaderCell>
                        <Table.HeaderCell>
                          Request by 4:00pm
                          Three (3) Business days before
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                          Receive confirmation of confirm pickup at least one (1) business day before
                        </Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>
                      <Table.Row>
                        <Table.Cell>Monday</Table.Cell>
                        <Table.Cell>Thursday</Table.Cell>
                        <Table.Cell>Friday</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Tuesday</Table.Cell>
                        <Table.Cell>Friday</Table.Cell>
                        <Table.Cell>Monday</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Wednesday</Table.Cell>
                        <Table.Cell>Monday</Table.Cell>
                        <Table.Cell>Tuesday</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Thursday</Table.Cell>
                        <Table.Cell>Tuesday</Table.Cell>
                        <Table.Cell>Wednesday</Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>Friday</Table.Cell>
                        <Table.Cell>Wednesday</Table.Cell>
                        <Table.Cell>Thursday</Table.Cell>
                      </Table.Row>
                    </Table.Body>
                  </Table>
                </li>
              </ol>
            </li>
          </ol>
        </TextBody>
        <HeaderColored>
          <u>Titan Deposit Review</u>
        </HeaderColored>
        <TextBody>
          <ol>
            <li>
              Member Deposit Entry (via TV portal)
              <ol type="a">
                <li>All deposit request made in TV will be reviewed by a Titan Officer.</li>
                <li>
                  Deposit transaction detail will show the following statuses:
                  <ol type="i">
                    <li>Requested - Member clicks “confirm” deposit request</li>
                    <li>Processing - Titan reviewed and have approve request</li>
                    <li><B>Scheduled - Cash pickup scheduled</B></li>
                    <li>Completed - TV receipt of deposit and credited to Member account.</li>
                  </ol>
                </li>
              </ol>
            </li>
          </ol>
        </TextBody>
        <HeaderColored>
          <u>Preparing Cash-in-Transit CIT deposit</u>
        </HeaderColored>
        <TextBody>
          Member making cash note deposit should follow the guidelines below. <br /><br />
          <ul>
            Items needed:
          </ul>
          <ul>❏	Titan Printed Currency Deposit Ticket (“Said to Contain” Amount)</ul>
          <ul>❏	Rubber bands or similar currency straps if available</ul>
          <ul>❏	Clear plastic tamper-proof deposit bag (Starter supply provided)</ul>
          <ul>❏	Permanent marker, black or blue only</ul>
        </TextBody>
        <TextBody>
          <ol>
            <li>
              <B>Currency Deposit Ticket</B>
              <ol type="i">
                <li>Print at least one (1)“Deposit Ticket” directly from Deposit Request via TV portal.  See Exhibit 1</li>
                <li>Place the Deposit Ticket in the deposit bag(s.)</li>
                <li>If your deposit requires multiple deposit bags, please make sure the deposit ticket is placed in each bag.</li>
                <li>Important: Be sure to indicate on each deposit bag, Bag____ of _____.</li>
              </ol>
            </li>
            <li>
              <B>Currency Deposit Bag</B>
              <ol type="a">
                <li>A starter deposit kit will be provided upon final approval of New Member Account.</li>
                <li>Only clear plastic bags may be used for CIT deposits. White or opaque deposit bags are not acceptable.</li>
                <li>
                  The deposit bag must be securely sealed with a tamper-proof seal so that any unauthorized access is easily detected, and the bag must be free of holes and tears.
                  <ol type="i">
                    <li>
                      If a bag had holes or tears the armored car partner can refuse to accept the deposit
                    </li>
                  </ol>
                </li>
                <li>
                  A single deposit bag may include various denominations.
                </li>
                <li>
                  Each deposit bag must indicate on the front of the bag: (Written w permanent marker)
                  <ol type="i">
                    <li>TO: HARDCAR</li>
                    <li>“[Member Name]” via Athena Pay</li>
                    <li>Account Number  (Athena Pay’s #)</li>
                    <li>Dollar amount in the deposit bag - Amount shown on your deposit ticket.</li>
                    <li>Date of scheduled cash pickup</li>
                    <li>Bag____ of _____</li>
                  </ol>
                </li>
                <li>
                  Use as few bags as possible for easier processing.
                </li>
              </ol>
            </li>
          </ol>
        </TextBody>
        <HeaderColored>
          <u>Preparing Smartsafe deposit</u>
        </HeaderColored>
        <TextBody>
          Details to be provided at a later time.
        </TextBody>
        <HeaderColored>
          <u>Currency Requirements</u>
        </HeaderColored>
        <TextBody>
          <ol>
            <li>
              <B>Currency within a strap: All denomination</B>
              <ol type="a">
                <li>All notes in a deposit must be clearly more than 50% of a note to receive credit.</li>
                <li>Unﬁt currency, currency that is not suitable for further circulation due to its physical condition (torn, dirty, limp, worn or defaced), should not be included in your regular deposit.</li>
                <li>Notes should be straightened, and all corners and edges should be aligned.</li>
                <li>Notes within the strap may be packaged without regard to direction or facing.</li>
              </ol>
            </li>
            <li>
              <B>Straps</B>
              <ol type="a">
                <li>A strap is a package of 100 notes of the same denomination.</li>
                <li>All straps must have only one band/rubber band around them.</li>
                <li>Include only U.S. currency. Under no circumstances should coin or other valuables be included.</li>
                <li>Currency must be strapped and bundled with rubber bands/currency straps and separated by denomination.</li>
              </ol>
            </li>
            <li>
              <B>Bundles</B>
              <ol type="a">
                <li>A bundle consists of 1,000 notes of the same denomination in ten equal straps of 100 notes each.</li>
                <li>Straps must be bound together with two tight-ﬁttng rubber bands.</li>
                <li>Denomination $1-$20; deposits must contain full bundles.</li>
                <li>Denomination $50-$100; deposits must be in full straps or bundles</li>
              </ol>
              <TextBody>
                A strap is 100 notes.<br />
                A bundle is 1,000 notes in ten equal straps of 100 notes.<br />
                Currency must be deposited in the following denominations and increments:<br />
                <Table celled>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>Denomination</Table.HeaderCell>
                      <Table.HeaderCell>
                        Standard Strap
                        (100 notes)
                        Dollar Amount
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        Standard Bundle
                        (1,000 notes/10 strap)
                        Dollar Amount
                      </Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell>Ones - $1</Table.Cell>
                      <Table.Cell>N/A</Table.Cell>
                      <Table.Cell>$1,000</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Twos - $2</Table.Cell>
                      <Table.Cell>N/A</Table.Cell>
                      <Table.Cell>$2,000</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Fives - $5</Table.Cell>
                      <Table.Cell>N/A</Table.Cell>
                      <Table.Cell>$5,000</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Tens - $10</Table.Cell>
                      <Table.Cell>N/A</Table.Cell>
                      <Table.Cell>$10,000</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Twenties - $20</Table.Cell>
                      <Table.Cell>N/A</Table.Cell>
                      <Table.Cell>$20,000</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Fifties - $50</Table.Cell>
                      <Table.Cell>$5,000</Table.Cell>
                      <Table.Cell>$50,000</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell>Hundred - $100</Table.Cell>
                      <Table.Cell>$10,000</Table.Cell>
                      <Table.Cell>$100,000</Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
              </TextBody>
              <TextBody>
                <ol>
                  If Armored Car Partner receives incomplete straps or bundles (cash that cannot be strapped or bundled into the above increments) that cash will be subtracted from your total deposit, and returned.
                </ol>
              </TextBody>
              <TextBody>
                <ol>
                  Example : Hardcar receives a stated amount of $100,000 in $20s (5 bundles of $20s). After verifying the amount there is actually $99,960 in $20s; the bundle is short $40 in $20s. $80,000 will be deposited and $19,960 will be returned, because $19,960 is not a complete bundle.
                </ol>
                <Table celled>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>What is  NOT ACCEPTABLE  in your deposit</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell>
                        <ul>
                          <li>No paper clips</li>
                          <li>No staples</li>
                          <li>No post-its</li>
                          <li>No coins</li>
                          <li>No checks</li>
                          <li>No counterfeit or altered notes. Counterfeit notes will be conﬁscated and subtracted from your deposit and may result in a portion of your deposit being returned. Armored Car Partner is not responsible for reimbursing conﬁscated counterfeit notes.</li>
                        </ul>
                      </Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
              </TextBody>
              <TextBody>
                <Table celled>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>What is  NOT ACCEPTABLE  in your deposit </Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell>
                        <ul>
                          ➢	Deposits must be ready for pickup by the  me arranged. Armored Car Partner Agents will wait no more than ten (10) minutes for a pickup.
                        </ul>
                        <ul>
                          ➢	Upon pickup the Armored Car Partner Agent will provide you a receipt. Armored Car Partner Agents are only authorized to sign Armored Car Partner documents.
                        </ul>
                      </Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
              </TextBody>
            </li>
          </ol>
        </TextBody>
      </BodyWrapper>
    </CardBox>
  </ContainerWrapper>
);

CashHandlingDepositProcedure.propTypes = {
  setBack: PropTypes.func.isRequired,
};
export default CashHandlingDepositProcedure;
