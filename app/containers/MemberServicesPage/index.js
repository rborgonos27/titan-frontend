/**
 *
 * MemberServicesPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import { Divider } from 'semantic-ui-react';
import styled from 'styled-components';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import {
  DepositAgreement,
  PaymentAgreement,
  SmartSafeAgreement,
  ExportableTransactionFile,
  CashHandlingDepositProcedure,
  ElectronicDeposit,
  MakeDeposit,
  MakePayment,
  AddNewPayee,
} from './Pages';
import makeSelectMemberServicesPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

const A = styled.a`
  &:hover {
    text-decoration: underline;
    color: #000;
    cursor: pointer;
  }
  text-decoration: underline;
  color: #000;
  cursor: pointer;
`;

const Header = styled.h4`
  color: #f25218;
  font-weight: bold;
`;

const MAKE_DEPOSIT = 'make_deposit';
const SEND_PAYMENT = 'send_payment';
const SCHEDULE_PICKUP = 'schedule_pickup';
const EXPORTABLE_TRANSACTION_FILE = 'exportable_transaction_file';
const CASH_HANDLING_DEPOSIT = 'cash_handling_deposit_procedures';
const DEPOSIT_AGREEMENT = 'deposit_agreement';
const PAYMENT_AGREEMENT = 'payment_agreement';
const SMART_SAFE_AGREEMENT = 'smart_safe_agreement';
const MAIN = 'main';
const ELECTRONIC_DEPOSIT = 'electronic_deposit';
const ADDING_NEW_PAYEE = 'adding_new_payee';
/* eslint-disable react/prefer-stateless-function */
export class MemberServicesPage extends React.Component {
  state = {
    display: 'main',
  };
  createLink = (name, title) => (
    <li>
      <A onClick={() => this.setPage(name)}>{title}</A>
    </li>
  );
  buildMain = () => (
    <ContainerWrapper>
      <CardBox>
        <h3>
          <FormattedMessage {...messages.header} />
        </h3>
        <Divider />
        <Header>How To</Header>
        <ul>
          {this.createLink(MAKE_DEPOSIT, 'Make Deposit')}
          {this.createLink(SEND_PAYMENT, 'Send Payment')}
          {this.createLink(SCHEDULE_PICKUP, 'Schedule Pickup')}
          {this.createLink(
            EXPORTABLE_TRANSACTION_FILE,
            'Exportable Transaction File',
          )}
          {this.createLink(
            CASH_HANDLING_DEPOSIT,
            'CIT Cash Handling Deposit Procedures',
          )}
          {this.createLink(
            ELECTRONIC_DEPOSIT,
            'Electronic Deposit Handling Procedures',
          )}
          {this.createLink(ADDING_NEW_PAYEE, 'Adding New Payee')}
        </ul>
        <Header>Terms of Service</Header>
        <ul>
          {this.createLink(DEPOSIT_AGREEMENT, 'Deposit Agreement')}
          {this.createLink(PAYMENT_AGREEMENT, 'Payment Agent Agreement')}
          {this.createLink(SMART_SAFE_AGREEMENT, 'SmartSafe Agreement')}
        </ul>
      </CardBox>
    </ContainerWrapper>
  );
  buildDepositAgreement = () => (
    <DepositAgreement setBack={() => this.setPage(MAIN)} />
  );
  buildPaymentAgreement = () => (
    <PaymentAgreement setBack={() => this.setPage(MAIN)} />
  );
  buildSmartSafeAgreement = () => (
    <SmartSafeAgreement setBack={() => this.setPage(MAIN)} />
  );
  buildExportableTransactionFile = () => (
    <ExportableTransactionFile setBack={() => this.setPage(MAIN)} />
  );
  buildCashHandlingDepositProcedure = () => (
    <CashHandlingDepositProcedure setBack={() => this.setPage(MAIN)} />
  );
  buildElectronicDepositFile = () => (
    <ElectronicDeposit setBack={() => this.setPage(MAIN)} />
  );
  buildMakeDepositFile = () => (
    <MakeDeposit setBack={() => this.setPage(MAIN)} />
  );
  buildMakePaymenetFile = () => (
    <MakePayment setBack={() => this.setPage(MAIN)} />
  );
  buildAddNewPayeeFile = () => (
    <AddNewPayee setBack={() => this.setPage(MAIN)} />
  );
  setPage = display => this.setState({ display });
  manageDisplay = () => {
    const { display } = this.state;
    switch (display) {
      case MAIN:
        return this.buildMain();
      case DEPOSIT_AGREEMENT:
        return this.buildDepositAgreement();
      case PAYMENT_AGREEMENT:
        return this.buildPaymentAgreement();
      case SMART_SAFE_AGREEMENT:
        return this.buildSmartSafeAgreement();
      case EXPORTABLE_TRANSACTION_FILE:
        return this.buildExportableTransactionFile();
      case CASH_HANDLING_DEPOSIT:
        return this.buildCashHandlingDepositProcedure();
      case ELECTRONIC_DEPOSIT:
        return this.buildElectronicDepositFile();
      case MAKE_DEPOSIT:
        return this.buildMakeDepositFile();
      case SEND_PAYMENT:
        return this.buildMakePaymenetFile();
      case ADDING_NEW_PAYEE:
        return this.buildAddNewPayeeFile();
      default:
        return this.buildMain();
    }
  };
  render() {
    return this.manageDisplay();
  }
}

MemberServicesPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  memberservicespage: makeSelectMemberServicesPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'memberServicesPage', reducer });
const withSaga = injectSaga({ key: 'memberServicesPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(MemberServicesPage);
