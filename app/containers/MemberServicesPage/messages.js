/*
 * MemberServicesPage Messages
 *
 * This contains all the text for the MemberServicesPage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.MemberServicesPage.header',
    defaultMessage: 'Members Services',
  },
});
