/**
 *
 * DashBoard
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Form, TextArea } from 'semantic-ui-react';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { getParams } from 'utils/helper';
import StandardModal from 'components/StandardModal';
import { getCurrentUser } from 'containers/App/actions';
import moment from 'moment';
import { APP } from 'config';

import makeSelectDashBoard, { makeSelectCurrentUser } from './selectors';
import reducer from './reducer';
import saga from './saga';
import {
  getAdminRequest,
  getMemberRequest,
  processRequest,
  viewRequest,
  pageRequest,
  memberPageRequest,
  updateCredentials,
  filterRequest,
  getPayee,
  updateFirstTimeMember,
  proceedToDashboard,
  getUserFeeRate,
  getUserFeeTierRate,
  getWallet,
  getTopRequest,
  getMemberUsers,
  setNewMemberData,
  getNextMemberPage,
  filterMemberRequest,
  exportCSV,
  memberBalance,
  memberBalancesPage,
} from './actions';
import {
  AdminView,
  MemberView,
  TempMemberView,
  TempSubMemberView,
} from './View';

const DATE_FORMAT_FOR_REACT = `DD/MM/YYYY`;
/* eslint-disable react/prefer-stateless-function */
export class DashBoard extends React.Component {
  /* eslint-disable */
  state = {
    isOpen: false,
    remarks: '',
    request: null,
    column: null,
    direction: null,
    status: 'ALL',
    search: '',
    password: '',
    confirmPassword: '',
    isFilterRan: false,
    onFilterOpen: false,
    credentialsError: '',
    filterType: 'ALL',
    filterStatus: 'ALL',
    filterPeriod: '',
    filterDeliveryMethod: 'ANY',
    filterDate: '',
    filterDateFrom: moment().format(APP.DATE_FORMAT),
    filterDateTo: moment().format(APP.DATE_FORMAT),
    filterDateFromDisplay: moment().format(DATE_FORMAT_FOR_REACT),
    filterDateToDisplay: moment().format(DATE_FORMAT_FOR_REACT),
    filterPayee: '',
    filterCategory: 'ALL',
    filterUser: null,
    payeeData: [],
    userName: '',
    isErrorMessageOpen: false,
    showReadyNotification: false,
    profileImage: null,
    isRender: false,
    userMemberOption: [],
  };
  /* eslint-enable */
  componentWillMount() {
    const { currentUser, dashboard } = this.props;
    const { nextUrl, previousUrl } = dashboard;
    /* eslint-disable */
    let parameter = null;
    if (nextUrl || previousUrl) {
      const url = nextUrl ? nextUrl : previousUrl;
      const params = !url ? {limit: 0, offset: 0} : getParams(url);
      const {limit, offset} = params;
      const currentPage = nextUrl ? offset / limit : (parseInt(offset) + (parseInt(limit) * 2)) / limit;
      parameter = {
        limit,
        offset: (currentPage - 1) * limit,
        filterParam: { stateData: this.state },
      };
      /* eslint-enable */
    }
    this.props.getCurrentUser();
    if (currentUser.user_type === 'super_admin') {
      this.props.getPayee();
      if (nextUrl || previousUrl) {
        this.props.pageRequest(parameter);
      } else {
        this.props.getAdminRequest();
      }
      this.props.memberBalance();
    } else if (
      currentUser.user_type === 'member' ||
      currentUser.user_type === 'sub_member'
    ) {
      this.props.getPayee();
      if (nextUrl || previousUrl) {
        this.props.memberPageRequest(parameter);
      } else {
        this.props.getMemberRequest();
      }
    }
  }
  componentDidMount() {
    const { currentUser } = this.props;
    const { id } = currentUser;
    if (
      currentUser.user_type === 'member' ||
      currentUser.user_type === 'sub_member'
    ) {
      this.props.getUserFeeRate(id);
      this.props.getUserFeeTierRate(id);
      this.props.getWallet();
      this.props.getTopRequest();
    } else {
      this.props.getMemberUsers();
    }
  }
  componentWillReceiveProps(nextProps) {
    const {
      credentialsError,
      showReadyNotification,
      membersData,
      newMemberData,
    } = nextProps.dashboard;
    /* eslint-disable */
    this.setState({ credentialsError, isErrorMessageOpen: credentialsError !== null, showReadyNotification });
    if (membersData && newMemberData) {
      this.setMembersOption(membersData.results);
      if (membersData.next) {
        this.props.getNextMemberPage(membersData.next);
      }
    }
    /* eslint-enable */
  }
  handleInput = e => this.setState({ [e.target.name]: e.target.value });
  clearInput = () => this.setState({ search: '' });
  /* eslint-disable */
  setCredentialsError = credentialsError => this.setState({ credentialsError });
  /* eslint-enable */
  close = () => this.setState({ isOpen: false, remarks: '' });
  processMemberRequest = request => {
    this.setState({ request, isOpen: true });
  };
  setMembersOption = results => {
    /* eslint-disable */
    const { userMemberOption } = this.state;
    let userMemberNewSet = [];
    let memberOptionFromResponse = [];
    for(let i = 0; i < results.length; i++) {
      const data = results[i];
      const userObject = {
        key: data.id,
        value: data.id,
        text: `${data.business_name}`,
      }
      memberOptionFromResponse.push(userObject);
    }
    if (userMemberOption.length > 0) {
      userMemberNewSet = [...userMemberOption, ...memberOptionFromResponse];
    } else {
      userMemberNewSet = memberOptionFromResponse;
    }
    this.setState({userMemberOption: userMemberNewSet});
    this.props.setNewMemberData(false);
    /* eslint-enable */
  };
  resetFilter = () => {
    /* eslint-disable */
    this.setState({
      filterType: 'ALL',
      filterStatus: 'ALL',
      filterDeliveryMethod: 'ANY',
      filterDate: '',
      filterDateFrom: moment().format(APP.DATE_FORMAT),
      filterDateTo: moment().format(APP.DATE_FORMAT),
      filterPayee: '',
      filterCategory: 'ALL',
      filterPeriod: '',
      filterUser: null,
    });
    const { currentUser } = this.props;
    if (currentUser.user_type === 'member') {
      this.props.getMemberRequest();
    } else {
      this.props.getAdminRequest();
    }

    /* eslint-enable */
  };

  handleDateFromChange = date => {
    console.log('date from', date);
    if (moment(date).isValid() === true) {
      const filterDateFrom = moment(date).format(APP.DATE_FORMAT);
      console.log('date from', filterDateFrom);
      const filterDateFromDisplay = moment(date).format('DD/MM/YYYY');
      this.setState({ filterDateFrom, filterDateFromDisplay });
    } else {
      const filterDateFrom = moment().format(APP.DATE_FORMAT);
      this.setState({ filterDateFrom });
    }
  };
  handleDateToChange = date => {
    console.log('date to', date);
    if (moment(date).isValid() === true) {
      const filterDateTo = moment(date).format(APP.DATE_FORMAT);
      const filterDateToDisplay = moment(date).format('DD/MM/YYYY');
      this.setState({ filterDateTo, filterDateToDisplay });
    } else {
      const filterDateTo = moment().format(APP.DATE_FORMAT);
      this.setState({ filterDateTo });
    }
  };
  setFilterState = (stateName, stateValue) =>
    this.setState({ [stateName]: stateValue });
  submitData = () => {
    const { request, remarks } = this.state;
    request.remarks = remarks;
    this.props.processRequest(request);
    this.setState({ remarks: '' });
    this.close();
  };
  applyFilter = () => {
    const { currentUser } = this.props;
    if (currentUser.user_type === 'member') {
      this.props.filterMemberRequest(this.state);
    } else {
      this.props.filterRequest(this.state);
    }
  };
  exportCSV = () => {
    const { currentUser } = this.props;
    const stateData = this.state;
    stateData.currentUser = currentUser;
    this.props.exportCSV(stateData);
  };
  handleChangeDropDown = (e, { value }) => this.setState({ status: value });
  buildModal = () => {
    const { isOpen, remarks } = this.state;
    return (
      <StandardModal
        headerText="Transaction Note"
        open={isOpen}
        onClose={this.close}
        onActionText="SUBMIT"
        onContinue={this.submitData}
      >
        <Form>
          <Form.Field>
            <TextArea
              name="remarks"
              onChange={this.handleInput}
              rows={3}
              placeholder="Input transaction notes."
              value={remarks}
            />
          </Form.Field>
        </Form>
      </StandardModal>
    );
  };
  setColumnState = (column, direction) => this.setState({ column, direction });
  manageDiplay = () => {
    const { currentUser, dashboard } = this.props;
    const { userMemberOption } = this.state;
    const {
      transactionData,
      loading,
      pageNumber,
      nextUrl,
      previousUrl,
      payeeData,
      userUpdatedData,
      wallet,
      topFiveRequest,
      memberBalances,
      memberBalancePageNumber,
      memberBalancePageNextUrl,
      memberBalancePagePreviousUrl,
    } = dashboard;
    const userType = currentUser.user_type;
    const isFirstTimeMember = currentUser.is_first_time_member;
    const { column, direction, search, status, isRender } = this.state;
    /* eslint-disable */
    const memberUrl = memberBalancePageNextUrl ? memberBalancePageNextUrl : memberBalancePagePreviousUrl;
    const memberUrlParams = !memberUrl ? { memberUrlLimit: 0, memberUrlLimitOffset: 0 } : getParams(memberUrl);
    const memberUrlLimit = memberUrlParams.limit ? memberUrlParams.limit : 0;
    const memberUrlLimitOffset = memberUrlParams.offset ? memberUrlParams.offset : 0;
    const memberUrlNumberOfPage = memberUrlLimit > 0 ? Math.ceil(memberBalancePageNumber / memberUrlLimit) : 0;
    const memberUrlCurrentPage = memberBalancePageNextUrl
      ? memberUrlLimitOffset / memberUrlLimit
      : (parseInt(memberUrlLimitOffset) + parseInt(memberUrlLimit) * 2) / memberUrlLimit;

    const url = nextUrl ? nextUrl : previousUrl;
    const params = !url ? { limit: 0, offset: 0 } : getParams(url);
    const { limit, offset } = params;
    const numberOfPage = limit > 0 ? Math.ceil(pageNumber / limit) : 0;
    const currentPage = nextUrl
      ? offset / limit
      : (parseInt(offset) + parseInt(limit) * 2) / limit;
    /* eslint-enable */
    const data = {
      transactionData,
      pageNumber,
      nextUrl,
      previousUrl,
      limit,
      column,
      direction,
      status,
      numberOfPage,
      currentPage,
      search,
      currentUser,
      setState: this.setColumnState,
      onInputChange: this.handleInput,
      onDropDownChange: this.handleChangeDropDown,
      clearInput: this.clearInput,
      isRender,
      clearFilter: () => this.setState({ status: '' }),
      memberUrlNumberOfPage,
      memberUrlCurrentPage,
      memberBalancePageNextUrl,
      memberBalancePagePreviousUrl,
      memberUrlLimit,
      memberBalancesPage,
    };

    let userView = null;
    const filterParam = {
      setFilterState: this.setFilterState,
      resetFilter: this.resetFilter,
      applyFilter: this.applyFilter,
      stateData: this.state,
      payeeData,
      exportCSV: this.exportCSV,
      userMemberOption,
      handleDateFromChange: this.handleDateFromChange,
      handleDateToChange: this.handleDateToChange,
    };
    switch (userType) {
      case 'super_admin':
        userView = (
          <AdminView
            data={data}
            memberBalances={memberBalances}
            loading={loading}
            memberBalancesPage={this.props.memberBalancesPage}
            processRequest={this.processMemberRequest}
            viewRequest={this.props.viewRequest}
            pageAction={this.props.pageRequest}
            filterParam={filterParam}
          />
        );
        break;
      case 'member':
        userView = isFirstTimeMember ? (
          <TempMemberView
            user={currentUser}
            handleInput={this.handleInput}
            userInput={this.state}
            action={this.props.updateFirstTimeMember}
            loading={loading}
            setCredentialsError={this.setCredentialsError}
            setState={this.setFilterState}
            startDashboard={() =>
              this.props.proceedToDashboard(userUpdatedData)
            }
          />
        ) : (
          <MemberView
            data={data}
            loading={loading}
            viewRequest={this.props.viewRequest}
            pageAction={this.props.memberPageRequest}
            wallet={wallet}
            topRequest={topFiveRequest}
            filterParam={filterParam}
          />
        );
        break;
      case 'sub_member':
        userView = isFirstTimeMember ? (
          <TempSubMemberView
            user={currentUser}
            handleInput={this.handleInput}
            userInput={this.state}
            action={this.props.updateFirstTimeMember}
            loading={loading}
            setCredentialsError={this.setCredentialsError}
            setState={this.setFilterState}
            startDashboard={() =>
              this.props.proceedToDashboard(userUpdatedData)
            }
          />
        ) : (
          <MemberView
            data={data}
            loading={loading}
            viewRequest={this.props.viewRequest}
            pageAction={this.props.memberPageRequest}
            wallet={wallet}
            topRequest={topFiveRequest}
            filterParam={filterParam}
          />
        );
        break;
      case 'temporary':
        userView = (
          <TempMemberView
            user={currentUser}
            handleInput={this.handleInput}
            userInput={this.state}
            action={this.props.updateCredentials}
            loading={loading}
            setCredentialsError={this.setCredentialsError}
            setState={this.setFilterState}
            startDashboard={() =>
              this.props.proceedToDashboard(userUpdatedData)
            }
          />
        );
        break;
      default:
        userView = <div>No View Available</div>;
        break;
    }
    return userView;
  };
  render() {
    return (
      <div>
        {this.buildModal()}
        {this.manageDiplay()}
      </div>
    );
  }
}

DashBoard.propTypes = {
  currentUser: PropTypes.any,
  getAdminRequest: PropTypes.func.isRequired,
  getMemberRequest: PropTypes.func.isRequired,
  processRequest: PropTypes.func.isRequired,
  viewRequest: PropTypes.func.isRequired,
  dashboard: PropTypes.any,
  pageRequest: PropTypes.func.isRequired,
  memberPageRequest: PropTypes.func.isRequired,
  updateCredentials: PropTypes.func.isRequired,
  filterRequest: PropTypes.func.isRequired,
  getPayee: PropTypes.func.isRequired,
  updateFirstTimeMember: PropTypes.func.isRequired,
  proceedToDashboard: PropTypes.func.isRequired,
  getCurrentUser: PropTypes.func.isRequired,
  getUserFeeRate: PropTypes.func.isRequired,
  getUserFeeTierRate: PropTypes.func.isRequired,
  getWallet: PropTypes.func.isRequired,
  getTopRequest: PropTypes.func.isRequired,
  getMemberUsers: PropTypes.func.isRequired,
  setNewMemberData: PropTypes.func.isRequired,
  getNextMemberPage: PropTypes.func.isRequired,
  filterMemberRequest: PropTypes.func.isRequired,
  exportCSV: PropTypes.func.isRequired,
  memberBalance: PropTypes.func.isRequired,
  memberBalancesPage: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  dashboard: makeSelectDashBoard(),
  currentUser: makeSelectCurrentUser(),
});

const mapDispatchToProps = {
  getAdminRequest,
  getMemberRequest,
  processRequest,
  viewRequest,
  pageRequest,
  memberPageRequest,
  updateCredentials,
  filterRequest,
  getPayee,
  updateFirstTimeMember,
  proceedToDashboard,
  getCurrentUser,
  getUserFeeRate,
  getUserFeeTierRate,
  getWallet,
  getTopRequest,
  getMemberUsers,
  setNewMemberData,
  getNextMemberPage,
  filterMemberRequest,
  exportCSV,
  memberBalance,
  memberBalancesPage,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'dashBoard', reducer });
const withSaga = injectSaga({ key: 'dashBoard', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(DashBoard);
