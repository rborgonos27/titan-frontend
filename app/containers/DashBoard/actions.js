/*
 *
 * DashBoard actions
 *
 */

import {
  GET_ADMIN_REQUEST,
  GET_ADMIN_SUCCESS,
  GET_ADMIN_FAILED,
  GET_MEMBER_REQUEST,
  GET_MEMBER_SUCCESS,
  GET_MEMBER_FAILED,
  PROCESS_REQUEST,
  PROCESS_REQUEST_SUCCESS,
  PROCESS_REQUEST_FAILED,
  VIEW_REQUEST,
  PAGE_REQUEST,
  PAGE_REQUEST_SUCCESS,
  PAGE_REQUEST_FAILED,
  MEMBER_PAGE_REQUEST,
  MEMBER_PAGE_REQUEST_SUCCESS,
  MEMBER_PAGE_REQUEST_FAILED,
  UPDATE_CREDENTIALS,
  UPDATE_CREDENTIALS_SUCCESS,
  UPDATE_CREDENTIALS_FAILURE,
  FILTER_REQUEST,
  FILTER_REQUEST_SUCCESS,
  FILTER_REQUEST_FAILED,
  GET_PAYEE,
  GET_PAYEE_SUCCESS,
  GET_PAYEE_FAILED,
  UPDATE_FIRST_TIME_MEMBER,
  UPDATE_FIRST_TIME_MEMBER_SUCCESS,
  UPDATE_FIRST_TIME_MEMBER_ERROR,
  PROCEED_TO_DASHBOARD,
  GET_USER_FEE_RATE,
  GET_USER_FEE_RATE_ERROR,
  GET_USER_FEE_TIER_RATE,
  GET_USER_FEE_TIER_RATE_ERROR,
  GET_WALLET,
  GET_WALLET_SUCCESS,
  GET_WALLET_ERROR,
  GET_TOP_REQUEST,
  GET_TOP_REQUEST_SUCCESS,
  GET_TOP_REQUEST_ERROR,
  GET_MEMBER_USERS,
  GET_MEMBER_USERS_SUCCESS,
  GET_MEMBER_USERS_ERROR,
  SET_NEW_MEMBER_DATA,
  GET_NEXT_MEMBER_PAGE,
  FILTER_MEMBER_REQUEST,
  FILTER_MEMBER_REQUEST_SUCCESS,
  FILTER_MEMBER_REQUEST_FAILED,
  EXPORT_CSV,
  EXPORT_CSV_ERROR,
  EXPORT_CSV_SUCCESS,
  MEMBER_BALANCE,
  MEMBER_BALANCE_ERROR,
  MEMBER_BALANCE_SUCCESS,
  MEMBER_BALANCE_PAGE,
  MEMBER_BALANCE_PAGE_ERROR,
  MEMBER_BALANCE_PAGE_SUCCESS,
} from './constants';

export function getAdminRequest(data) {
  return {
    type: GET_ADMIN_REQUEST,
    data,
  };
}

export function getMemberRequest(data) {
  return {
    type: GET_MEMBER_REQUEST,
    data,
  };
}

export function getAdminRequestSuccess(data) {
  return {
    type: GET_ADMIN_SUCCESS,
    data,
  };
}

export function getAdminMemberSuccess(data) {
  return {
    type: GET_MEMBER_SUCCESS,
    data,
  };
}

export function getAdminMemberFailed(data) {
  return {
    type: GET_MEMBER_FAILED,
    data,
  };
}

export function getAdminRequestFailed(data) {
  return {
    type: GET_ADMIN_FAILED,
    data,
  };
}

export function processRequest(data) {
  return {
    type: PROCESS_REQUEST,
    data,
  };
}

export function processRequestSuccess(data) {
  return {
    type: PROCESS_REQUEST_SUCCESS,
    data,
  };
}

export function processRequestFailed(data) {
  return {
    type: PROCESS_REQUEST_FAILED,
    data,
  };
}

export function viewRequest(data) {
  return {
    type: VIEW_REQUEST,
    data,
  };
}

export function pageRequest(data) {
  return {
    type: PAGE_REQUEST,
    data,
  };
}

export function pageRequestSuccess(data) {
  return {
    type: PAGE_REQUEST_SUCCESS,
    data,
  };
}

export function pageRequestFailed(data) {
  return {
    type: PAGE_REQUEST_FAILED,
    data,
  };
}

export function memberPageRequest(data) {
  return {
    type: MEMBER_PAGE_REQUEST,
    data,
  };
}

export function memberPageRequestSuccess(data) {
  return {
    type: MEMBER_PAGE_REQUEST_SUCCESS,
    data,
  };
}

export function memberPageRequestFailed(data) {
  return {
    type: MEMBER_PAGE_REQUEST_FAILED,
    data,
  };
}

export function updateCredentials(data) {
  return {
    type: UPDATE_CREDENTIALS,
    data,
  };
}

export function updateCreadentialsSuccess(data) {
  return {
    type: UPDATE_CREDENTIALS_SUCCESS,
    data,
  };
}

export function updateCredentialsError(error) {
  return {
    type: UPDATE_CREDENTIALS_FAILURE,
    error,
  };
}

export function filterRequest(data) {
  return {
    type: FILTER_REQUEST,
    data,
  };
}

export function filterRequestSucess(data) {
  return {
    type: FILTER_REQUEST_SUCCESS,
    data,
  };
}

export function filterRequestFailed(error) {
  return {
    type: FILTER_REQUEST_FAILED,
    error,
  };
}

export function filterMemberRequest(data) {
  return {
    type: FILTER_MEMBER_REQUEST,
    data,
  };
}

export function filterMemberRequestSucess(data) {
  return {
    type: FILTER_MEMBER_REQUEST_SUCCESS,
    data,
  };
}

export function filterMemberRequestFailed(error) {
  return {
    type: FILTER_MEMBER_REQUEST_FAILED,
    error,
  };
}

export function getPayee() {
  return {
    type: GET_PAYEE,
  };
}

export function getPayeeSuccess(data) {
  return {
    type: GET_PAYEE_SUCCESS,
    data,
  };
}

export function getPayeeFailed(error) {
  return {
    type: GET_PAYEE_FAILED,
    error,
  };
}

export function updateFirstTimeMember(data) {
  return {
    type: UPDATE_FIRST_TIME_MEMBER,
    data,
  };
}

export function updateFirstTimeMemberSuccess(data) {
  return {
    type: UPDATE_FIRST_TIME_MEMBER_SUCCESS,
    data,
  };
}

export function updateFirstTimeMemberError(error) {
  return {
    type: UPDATE_FIRST_TIME_MEMBER_ERROR,
    error,
  };
}

export function proceedToDashboard(data) {
  return {
    type: PROCEED_TO_DASHBOARD,
    data,
  };
}

export function getUserFeeRate(data) {
  return {
    type: GET_USER_FEE_RATE,
    data,
  };
}

export function getUserFeeRateError(error) {
  return {
    type: GET_USER_FEE_RATE_ERROR,
    error,
  };
}

export function getUserFeeTierRate(data) {
  return {
    type: GET_USER_FEE_TIER_RATE,
    data,
  };
}

export function getUserFeeTierRateError(error) {
  return {
    type: GET_USER_FEE_TIER_RATE_ERROR,
    error,
  };
}

export function getWallet() {
  return {
    type: GET_WALLET,
  };
}

export function getWalletSuccess(data) {
  return {
    type: GET_WALLET_SUCCESS,
    data,
  };
}

export function getWalletError(error) {
  return {
    type: GET_WALLET_ERROR,
    error,
  };
}

export function getTopRequest() {
  return {
    type: GET_TOP_REQUEST,
  };
}

export function getTopRequestSuccess(data) {
  return {
    type: GET_TOP_REQUEST_SUCCESS,
    data,
  };
}

export function getTopRequestError(error) {
  return {
    type: GET_TOP_REQUEST_ERROR,
    error,
  };
}

export function getMemberUsers() {
  return {
    type: GET_MEMBER_USERS,
  };
}

export function getMemberUsersSuccess(data) {
  return {
    type: GET_MEMBER_USERS_SUCCESS,
    data,
  };
}

export function getMemberUsersError(error) {
  return {
    type: GET_MEMBER_USERS_ERROR,
    error,
  };
}

export function setNewMemberData(data) {
  return {
    type: SET_NEW_MEMBER_DATA,
    data,
  };
}

export function getNextMemberPage(data) {
  return {
    type: GET_NEXT_MEMBER_PAGE,
    data,
  };
}

export function exportCSV(data) {
  return {
    type: EXPORT_CSV,
    data,
  };
}

export function exportCSVSuccess(data) {
  return {
    type: EXPORT_CSV_SUCCESS,
    data,
  };
}

export function exportCSVError(error) {
  return {
    type: EXPORT_CSV_ERROR,
    error,
  };
}

export function memberBalance() {
  return { type: MEMBER_BALANCE };
}

export function memberBalanceSuccess(data) {
  return {
    type: MEMBER_BALANCE_SUCCESS,
    data,
  };
}

export function memberBalanceError(error) {
  return {
    type: MEMBER_BALANCE_ERROR,
    error,
  };
}

export function memberBalancesPage(data) {
  return {
    type: MEMBER_BALANCE_PAGE,
    data,
  };
}

export function memberBalancesPageSuccess(data) {
  return {
    type: MEMBER_BALANCE_PAGE_SUCCESS,
    data,
  };
}

export function memberBalancesPageError(error) {
  return {
    type: MEMBER_BALANCE_PAGE_ERROR,
    error,
  };
}
