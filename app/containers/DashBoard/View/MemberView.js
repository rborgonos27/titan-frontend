import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import CardBox from 'components/CardBox';
import { Loading } from 'components/Utils';
import { Table, Pagination, Input } from 'semantic-ui-react';
import ContainerWrapper from 'components/ContainerWrapper';
// import depositImage from 'themes/assets/images/deposit.png';
// import paymentImage from 'themes/assets/images/payment.png';
import moment from 'moment';
import ProgressBar from 'progressbar.js';
import { formatMoney } from 'utils/formatMoney';
import ChartPolar from 'components/ChartPolar';
import miniDepositImage from 'images/deposit.png';
import miniPaymentImage from 'images/payment.png';
import {
  handleSort,
  handlePagination,
  getMemberData,
  getTopFiveDetails,
  buildMemberAdvanceFilter,
  buildExportToCsv,
} from './Utils';

const createGauge = (selector, gaugeValue) => {
  const guageMeter = document.querySelector(`#${selector}`);
  const guage = new ProgressBar.Circle(guageMeter, {
    strokeWidth: 6,
    color: '#000000',
    trailColor: '#eee',
    trailWidth: 1,
    easing: 'easeInOut',
    duration: 1400,
    svgStyle: null,
    text: {
      value: '',
      alignToBottom: false,
    },
    from: { color: '#FFEA82' },
    to: { color: '#ff4400' },
    // Set default step function for all animate calls
    /* eslint-disable */
    step: (state, barCircle) => {
      barCircle.path.setAttribute('stroke', state.color);
      const value = Math.round(barCircle.value() * 100);
      if (value === 0) {
        barCircle.setText('');
      } else {
        barCircle.setText(`${value} %`);
      }
      barCircle.text.style.fontSize = '18px';
    },
    /* eslint-enable */
  });
  guage.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
  guage.text.style.fontSize = '3rem';

  guage.animate(gaugeValue);
};

const MemberView = ({
  loading,
  data,
  viewRequest,
  pageAction,
  wallet,
  topRequest,
  filterParam,
}) => {
  let availableBalance = 0;
  let pendingDeposit = 0;
  let pendingPayment = 0;
  let totalAvailable = 0;
  if (wallet) {
    availableBalance = wallet[0].balance;
    pendingDeposit = wallet[1].balance;
    pendingPayment = wallet[2].balance;
  }
  totalAvailable = availableBalance + pendingDeposit - pendingPayment;
  const availableBalanceActual = parseFloat(availableBalance) - parseFloat(pendingPayment);
  const vaultAmount = parseFloat(availableBalance);
  const {
    column,
    direction,
    onInputChange,
    search,
    // status,
    clearFilter,
    currentUser,
  } = data;
  const wrapper = document.querySelector('#smartSafe');
  if (wrapper && topRequest) {
    const chartGuage = document.querySelector('svg');
    if (chartGuage) {
      const progressiveBoxText = document.querySelector('.progressbar-text');
      if (progressiveBoxText) {
        progressiveBoxText.remove();
      }
      chartGuage.remove();
    }
    createGauge('smartSafe', 0.65);
  }
  const dateJoined = moment(currentUser.date_joined).format('D MMM YYYY');
  // const optionStatus = [
  //   {
  //     key: 0,
  //     text: 'ALL',
  //     value: 'ALL',
  //   },
  //   {
  //     key: 1,
  //     text: 'REQUESTED',
  //     value: 'REQUESTED',
  //   },
  //   {
  //     key: 2,
  //     text: 'PROCESSING',
  //     value: 'PROCESSING',
  //   },
  //   {
  //     key: 3,
  //     text: 'SCHEDULED',
  //     value: 'SCHEDULED',
  //   },
  //   {
  //     key: 4,
  //     text: 'COMPLETED',
  //     value: 'COMPLETED',
  //   },
  //   {
  //     key: 5,
  //     text: 'DEPOSIT',
  //     value: 'DEPOSIT',
  //   },
  //   {
  //     key: 6,
  //     text: 'PAYMENT',
  //     value: 'PAYMENT',
  //   },
  // ];
  return (
    <ContainerWrapper>
      <div className="row">
        <NavLink to="/deposit" className="col-md-6">
          <div className="card-box-dashboard send-payment text-center payment-deposit-border">
            <br />
            <br />
            <img alt="" src={miniDepositImage} />
            <h4>Make Deposit</h4>
            <br />
            <br />
            {/* <p /><button
              className="btn btn-payment waves-effect waves-light"
              type="submit"
              to="/deposit"
            >
              <strong>Next</strong>
            </button> */}
          </div>
        </NavLink>
        <NavLink to="/payment" className="col-md-6">
          <div className="card-box-dashboard send-payment text-center payment-deposit-border">
            <br />
            <br />
            <img alt="" src={miniPaymentImage} />
            <h4>Send Payment</h4>
            <br />
            <br />
            {/* eslint-disable */
            /*<p /> <button
              className="btn btn-payment waves-effect waves-light"
              type="submit"
            >
              <strong>Next</strong>
            </button>*/
            /* eslint-enable */}
          </div>
        </NavLink>
        <div className="col-lg-2" />
      </div>
      <div className="row">
        <div className="col-lg-12 member-title">
          <h3>Member Overview</h3>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <div className="col-lg-6">
            <CardBox>
              {/* <h4 className="m-t-0 header-title">
                <b>Transactions</b>
              </h4> */}
              <div className="row">
                <div className="col-lg-12">
                  <div className="col-lg-6">
                    <div className="chartStyle">
                      <ChartPolar
                        actualAmount={availableBalanceActual}
                        pendingPayment={pendingPayment}
                        pendingDeposit={pendingDeposit}
                      />
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <p style={{ marginTop: 63, marginLeft: 20 }} />
                    <div style={{ textAlign: 'center' }}>
                      <h2>$ {formatMoney(totalAvailable)}</h2>
                      Total Transactions
                    </div>
                  </div>
                </div>
              </div>
              <br />
              <br />
              <div className="row">
                <div className="col-lg-12">
                  <div className="col-lg-4">
                    <h4>$ {formatMoney(availableBalanceActual)}</h4>
                    <b style={{ fontSize: 30, color: '#32CD32' }}>&#8226;</b>
                    &nbsp; Available Now
                  </div>
                  <div className="col-lg-4">
                    <h4>$ {formatMoney(pendingDeposit)}</h4>
                    <b style={{ fontSize: 30, color: '#faad41' }}>&#8226;</b>
                    &nbsp; Pending Deposit
                  </div>
                  <div className="col-lg-4">
                    <h4>$ {formatMoney(pendingPayment)}</h4>
                    <b style={{ fontSize: 30, color: '#f25218' }}>&#8226;</b>
                    &nbsp; Pending Payment
                  </div>
                </div>
              </div>
            </CardBox>
          </div>
          <div className="col-lg-6">
            <CardBox>
              <h4 className="m-t-0 header-title">
                <b>RECENT TRANSACTION</b>
              </h4>
              {getTopFiveDetails(topRequest, viewRequest)}
            </CardBox>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <div className="col-lg-4">
            <h3>Vault Amount</h3>
            <div className="card-box-dashboard text-center p-t-40 p-b-40">
              <div className="m-t-0 m-b-20 widget-link">
                <i className="widget-wallet icon-wallet icon-dashboard-widget orange" />
              </div>
              <h1 className="m-t-0 m-b-20">
                <b>$ {formatMoney(vaultAmount)}</b>
              </h1>
            </div>
          </div>
          {/* <div className="col-lg-4">
            <h3>Smart Safe</h3>
            <div className="card-box-dashboard text-center p-t-40 p-b-40">
              <div className="col-lg-12">
                <div className="col-lg-6">
                  <div id="smartSafe" />
                  <h5>Capacity</h5>
                </div>
                <div className="col-lg-6">
                  <br />
                  <br />
                  <h4>$ {formatMoney(pendingDeposit)}</h4>
                </div>
              </div>
            </div>
          </div> */}
          <div className="col-lg-4">
            <h3>Member Since</h3>
            <div className="card-box-dashboard text-center p-t-40 p-b-40">
              <div className="m-t-0 m-b-20 widget-link">
                <i className="widget-wallet icon-user icon-dashboard-widget orange" />
              </div>
              <h2 className="m-t-0 m-b-20">
                <b>{dateJoined}</b>
              </h2>
              {/* <div>Member since</div> */}
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <CardBox>
            <h4 className="m-t-0 header-title">
              <b>All Transactions</b>
            </h4>
            <div className="p-20">
              <Input
                onChange={onInputChange}
                name="search"
                value={search}
                onKeyDown={e => {
                  if (e.keyCode === 13) {
                    clearFilter();
                    const pageData = {
                      search,
                      filterParam,
                    };
                    pageAction(pageData);
                  }
                }}
                icon="search"
                placeholder="Search..."
              />
              <span style={{ marginRight: 10 }} />
              {/* <Dropdown
                value={status}
                onChange={(e, { value }) =>
                  onChangeData(e, { value }, data, pageAction)
                }
                options={optionStatus}
                text="Filter"
                icon="filter"
                floating
                labeled
                button
                className="icon"
              /> */}
              {buildMemberAdvanceFilter(filterParam)}
              {buildExportToCsv(filterParam)}
              <Loading loading={loading} />
              <Table striped selectable sortable celled>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell
                      sorted={column === 'due_at' ? direction : null}
                      onClick={() => handleSort('due_at', data, pageAction)}
                    >
                      Transaction Date
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'type' ? direction : null}
                      onClick={() => handleSort('type', data, pageAction)}
                    >
                      Type
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'amount' ? direction : null}
                      onClick={() => handleSort('amount', data, pageAction)}
                    >
                      Amount
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'payee__name' ? direction : null}
                      onClick={() =>
                        handleSort('payee__name', data, pageAction)
                      }
                    >
                      Send To
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'fee' ? direction : null}
                      onClick={() => handleSort('fee', data, pageAction)}
                    >
                      Fee
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'amount' ? direction : null}
                      onClick={() => handleSort('amount', data, pageAction)}
                    >
                      Net Total
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'status' ? direction : null}
                      onClick={() => handleSort('status', data, pageAction)}
                    >
                      Status
                    </Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  {getMemberData(data.transactionData, viewRequest)}
                </Table.Body>
              </Table>
              {data.numberOfPage > 0 ? (
                <Pagination
                  defaultActivePage={data.currentPage}
                  totalPages={data.numberOfPage}
                  onPageChange={(e, pageData) =>
                    handlePagination(e, pageData, pageAction, data, filterParam)
                  }
                />
              ) : null}
            </div>
            <br />
            <br />
            <br />
          </CardBox>
        </div>
      </div>
    </ContainerWrapper>
  );
};

MemberView.propTypes = {
  loading: PropTypes.bool,
  data: PropTypes.any,
  viewRequest: PropTypes.func,
  pageAction: PropTypes.func,
  wallet: PropTypes.any,
  topRequest: PropTypes.any,
  filterParam: PropTypes.any,
};

export default MemberView;
