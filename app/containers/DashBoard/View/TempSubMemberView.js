import React from 'react';
import PropTypes from 'prop-types';
import CardBox from 'components/CardBox';
import { RequiredText, DetailData, HelpText } from 'components/Utils';
import {
  Segment,
  Button,
  Divider,
  Form,
  Input,
  Message,
} from 'semantic-ui-react';
import Gravatar from 'react-gravatar';
import { validatePassword, buildErrorModal, buildSuccessModal } from './Utils';

const readURL = file => {
  const reader = new FileReader();

  reader.onload = e => {
    document.querySelector('#previewImg').src = e.target.result;
  };

  reader.readAsDataURL(file);
};

const TempSubMemberView = ({
  user,
  action,
  handleInput,
  loading,
  userInput,
  setCredentialsError,
  setState,
  startDashboard,
}) => {
  const {
    password,
    confirmPassword,
    credentialsError,
    userName,
    isErrorMessageOpen,
    showReadyNotification,
    profileImage,
  } = userInput;
  const userData = {
    user,
    userInput,
  };
  // const preview = profileImage ? profileImage.preview : null;
  const isValidPassword = validatePassword(password);
  const isValidConfirmPassword = validatePassword(confirmPassword);
  const disableButton = isValidPassword && password === confirmPassword;
  const iconStyle = {
    color: 'green',
    name: 'check',
    link: true,
    size: 'large',
  };
  const showCheckIcon = isValidPassword ? iconStyle : null;
  const showCheckIconConfirmPassword = isValidConfirmPassword
    ? iconStyle
    : null;
  const errorMessage = credentialsError ? (
    <Message error>{credentialsError}</Message>
  ) : null;

  return (
    <Segment className="segmentStyle" color="orange">
      <h3>Update your login information.</h3>
      <Divider />
      {buildErrorModal(isErrorMessageOpen, setState, credentialsError)}
      {buildSuccessModal(showReadyNotification, startDashboard)}
      <CardBox>
        {errorMessage}
        {user.is_first_time_member ? (
          <div className="profileImageDiv">
            {profileImage ? (
              <img
                alt="profile"
                className="previewImg"
                id="previewImg"
                src={profileImage.name}
              />
            ) : (
              <Gravatar
                email={user.email}
                size={150}
                rating="pg"
                default="identicon"
                className="img-circle"
              />
            )}
            <div className="profileImageUpload">
              <Button
                onClick={() => {
                  document.querySelector('#profilePicture').click();
                }}
              >
                Upload Profile Picture
              </Button>
              <input
                type="file"
                id="profilePicture"
                name="file"
                encType="multipart/form-data"
                accept="image/*"
                onChange={e => {
                  const file = e.target.files[0];
                  setState('profileImage', file);
                  readURL(file);
                }}
              />
            </div>
          </div>
        ) : null}
        <Form loading={loading} centered>
          <Form.Field>
            <h4 className="p-t-10 headerClass">Name</h4>
            <DetailData>{user.first_name}</DetailData>
          </Form.Field>
          <Form.Field>
            <h4 className="p-t-10 headerClass">Email</h4>
            <DetailData>{user.email}</DetailData>
          </Form.Field>
          {user.is_first_time_member ? (
            <Form.Field>
              <h4 className="p-t-10 headerClass">
                Enter Username
                <RequiredText>*</RequiredText>
              </h4>
              <Input
                name="userName"
                type="text"
                value={userName}
                onChange={handleInput}
                placeholder="Username..."
              />
            </Form.Field>
          ) : null}
          <Form.Field>
            <h4 className="p-t-10 headerClass">
              Enter Password
              <RequiredText>*</RequiredText>
            </h4>
            <HelpText>
              Minimum 8 characters, at least one upper case and 1 numeric and a
              special character @#%$&!.
            </HelpText>
            <Input
              name="password"
              type="password"
              icon={showCheckIcon}
              value={password}
              onChange={handleInput}
              placeholder="Password..."
            />
          </Form.Field>
          <Form.Field>
            <h4 className="p-t-10 headerClass">
              Confirm Password
              <RequiredText>*</RequiredText>
            </h4>
            <Input
              name="confirmPassword"
              type="password"
              icon={showCheckIconConfirmPassword}
              value={confirmPassword}
              onChange={handleInput}
              placeholder="Confirm Password..."
            />
          </Form.Field>
          <Form.Field>
            <Button
              className="buttonSubmit"
              fluid
              size="massive"
              color="orange"
              disabled={!disableButton}
              onClick={e => {
                e.preventDefault();
                if (password !== confirmPassword) {
                  setCredentialsError('Password not matched.');
                } else {
                  setState('', false);
                  action(userData);
                }
              }}
            >
              Submit
            </Button>
          </Form.Field>
        </Form>
      </CardBox>
    </Segment>
  );
};

TempSubMemberView.propTypes = {
  loading: PropTypes.bool,
  user: PropTypes.any,
  action: PropTypes.func,
  handleInput: PropTypes.func,
  userInput: PropTypes.any,
  setCredentialsError: PropTypes.func,
  setState: PropTypes.func,
  startDashboard: PropTypes.any,
};

export default TempSubMemberView;
