import React from 'react';
import PropTypes from 'prop-types';
import CardBox from 'components/CardBox';
import { Loading } from 'components/Utils';
import { Table, Pagination, Input, Divider } from 'semantic-ui-react';
import ContainerWrapper from 'components/ContainerWrapper';
import {
  // onChangeData,
  handleSort,
  getData,
  handlePagination,
  handleMemberBalancesPagination,
  buildAdminAdvanceFilter,
  buildAdminFilterView,
  buildExportToCsv,
  getMemberBalance,
} from './Utils';

const AdminView = ({
  loading,
  data,
  processRequest,
  viewRequest,
  pageAction,
  memberBalancesPage,
  filterParam,
  memberBalances,
}) => {
  const { column, direction, onInputChange, search, clearFilter } = data;
  const { currentPage, numberOfPage } = data;
  const { memberUrlNumberOfPage, memberUrlCurrentPage } = data;
  /*
  const optionStatus = [
    {
      key: 0,
      text: 'ALL',
      value: 'ALL',
    },
    {
      key: 1,
      text: 'REQUESTED',
      value: 'REQUESTED',
    },
    {
      key: 2,
      text: 'PROCESSING',
      value: 'PROCESSING',
    },
    {
      key: 3,
      text: 'SCHEDULED',
      value: 'SCHEDULED',
    },
    {
      key: 4,
      text: 'COMPLETED',
      value: 'COMPLETED',
    },
    {
      key: 5,
      text: 'CANCELLED',
      value: 'CANCELLED',
    },
  ];
  */
  return (
    <ContainerWrapper>
      <div className="row">
        <div className="col-lg-12">
          <CardBox>
            <h4 className="m-t-0 header-title">
              <b>Requests for Approval</b>
            </h4>
            <div className="p-20">
              <Input
                onChange={onInputChange}
                name="search"
                value={search}
                onKeyDown={e => {
                  if (e.keyCode === 13) {
                    clearFilter();
                    const pageData = {
                      search,
                      filterParam,
                    };
                    pageAction(pageData);
                  }
                }}
                icon="search"
                placeholder="Search..."
              />
              <span style={{ marginRight: 10 }} />
              {buildAdminAdvanceFilter(filterParam)}
              {buildExportToCsv(filterParam)}
              {buildAdminFilterView(filterParam)}
              {/*
              <Dropdown
                value={status}
                onChange={(e, { value }) =>
                  onChangeData(e, { value }, data, pageAction)
                }
                options={optionStatus}
                text="Filter Status"
                icon="filter"
                floating
                labeled
                button
                className="icon"
              />
              */}
              <Loading loading={loading} />
              <Table striped selectable sortable celled>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell
                      sorted={column === 'type' ? direction : null}
                      onClick={() =>
                        handleSort('type', data, pageAction, filterParam)
                      }
                    >
                      Type
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'request_id' ? direction : null}
                      onClick={() =>
                        handleSort('request_id', data, pageAction, filterParam)
                      }
                    >
                      Transaction Id
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        column === 'user__business_name' ? direction : null
                      }
                      onClick={() =>
                        handleSort(
                          'user__business_name',
                          data,
                          pageAction,
                          filterParam,
                        )
                      }
                    >
                      Member Name
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'user__first_name' ? direction : null}
                      onClick={() =>
                        handleSort(
                          'user__first_name',
                          data,
                          pageAction,
                          filterParam,
                        )
                      }
                    >
                      User Name
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'user__email' ? direction : null}
                      onClick={() =>
                        handleSort('user__email', data, pageAction, filterParam)
                      }
                    >
                      Member Id
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'due_at' ? direction : null}
                      onClick={() =>
                        handleSort('due_at', data, pageAction, filterParam)
                      }
                    >
                      Transaction Date
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'pickup_at' ? direction : null}
                      onClick={() =>
                        handleSort('pickup_at', data, pageAction, filterParam)
                      }
                    >
                      Cash Pickup Date
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'payee__name' ? direction : null}
                      onClick={() =>
                        handleSort('payee__name', data, pageAction, filterParam)
                      }
                    >
                      Payee
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'amount' ? direction : null}
                      onClick={() =>
                        handleSort('amount', data, pageAction, filterParam)
                      }
                    >
                      Amount
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'status' ? direction : null}
                      onClick={() =>
                        handleSort('status', data, pageAction, filterParam)
                      }
                    >
                      Status
                    </Table.HeaderCell>
                    <Table.HeaderCell>Action</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  {getData(data.transactionData, processRequest, viewRequest)}
                </Table.Body>
              </Table>
              {data.numberOfPage > 0 ? (
                <Pagination
                  defaultActivePage={currentPage}
                  totalPages={numberOfPage}
                  onPageChange={(e, pageData) =>
                    handlePagination(e, pageData, pageAction, data, filterParam)
                  }
                />
              ) : null}
            </div>
            <Divider />
            <h4 className="m-t-0 header-title">
              <b>Member Balances</b>
            </h4>
            <div className="p-20">
              <Table striped selectable sortable celled>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell
                      sorted={column === 'business_name' ? direction : null}
                      onClick={() =>
                        handleSort('type', data, pageAction, filterParam)
                      }
                    >
                      Member Name
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'user__first_name' ? direction : null}
                      onClick={() =>
                        handleSort(
                          'user__first_name',
                          data,
                          pageAction,
                          filterParam,
                        )
                      }
                    >
                      Balance
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'user__email' ? direction : null}
                      onClick={() =>
                        handleSort('user__email', data, pageAction, filterParam)
                      }
                    >
                      Pending Deposit
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'user__email' ? direction : null}
                      onClick={() =>
                        handleSort('user__email', data, pageAction, filterParam)
                      }
                    >
                      Pending Payment
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={column === 'due_at' ? direction : null}
                      onClick={() =>
                        handleSort('due_at', data, pageAction, filterParam)
                      }
                    >
                      Fee
                    </Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>{getMemberBalance(memberBalances)}</Table.Body>
              </Table>
              {memberUrlNumberOfPage > 0 ? (
                <Pagination
                  activePage={memberUrlCurrentPage}
                  totalPages={memberUrlNumberOfPage}
                  onPageChange={(e, pageData) =>
                    handleMemberBalancesPagination(
                      e,
                      pageData,
                      memberBalancesPage,
                      data,
                      filterParam,
                    )
                  }
                />
              ) : null}
            </div>
          </CardBox>
        </div>
      </div>
    </ContainerWrapper>
  );
};

AdminView.propTypes = {
  loading: PropTypes.bool,
  data: PropTypes.any,
  processRequest: PropTypes.func,
  memberBalances: PropTypes.any,
  viewRequest: PropTypes.func,
  pageAction: PropTypes.func,
  filterParam: PropTypes.any,
  memberBalancesPage: PropTypes.func,
};

export default AdminView;
