import React from 'react';
import moment from 'moment';
import { formatMoney } from 'utils/formatMoney';
import {
  Icon,
  Popup,
  Grid,
  Button,
  Dropdown,
  Table,
  Header,
  Image,
} from 'semantic-ui-react';
import { APP } from 'config';
import DatePicker from 'react-datepicker';
import StandardModal from 'components/StandardModal';
import 'react-datepicker/dist/react-datepicker.css';
import { ALink } from 'components/Utils';
import CheckImage from 'images/check.png';
import CrossImage from 'images/cross.png';
import RequestedImage from 'images/requested.png';
import ProcessingImage from 'images/processing.png';

const onChangeData = (e, { value }, data, pageAction) => {
  const { onDropDownChange, clearInput } = data;
  onDropDownChange(e, { value });
  const pageData = {
    searchStatus: value === 'ALL' ? null : value,
  };
  clearInput();
  pageAction(pageData);
};

const handleSort = (clickedColumn, data, pageAction, filterParam) => {
  const { column, direction, setState, search, status } = data;
  if (column !== clickedColumn) {
    setState(clickedColumn, 'ascending');
    const pageData = {
      ordering: clickedColumn,
      search,
      searchStatus: status !== 'ALL' ? status : null,
      filterParam,
    };
    pageAction(pageData);
  } else if (column === clickedColumn) {
    setState(
      clickedColumn,
      direction === 'ascending' ? 'descending' : 'ascending',
    );
    const directionSymbol = direction === 'ascending' ? '-' : '';
    const pageData = {
      ordering: `${directionSymbol}${clickedColumn}`,
      search,
      searchStatus: status !== 'ALL' ? status : null,
      filterParam,
    };
    pageAction(pageData);
  }
};

const getMemberBalance = memberBalances => {
  /* eslint-disable */
  let rows = [];
  for (const key in memberBalances) {
    const balance = memberBalances[key];

    const rowData = (
      <Table.Row key={key}>
        <Table.Cell>
          {`${balance.business_name}`}
        </Table.Cell>
        <Table.Cell>
          {`${formatMoney(balance.wallet_info[0].balance)}`}
        </Table.Cell>
        <Table.Cell>
          {`${formatMoney(balance.wallet_info[1].balance)}`}
        </Table.Cell>
        <Table.Cell>
          {`${formatMoney(balance.wallet_info[2].balance)}`}
        </Table.Cell>
        <Table.Cell>
          {`${formatMoney(balance.member_fee)}`}
        </Table.Cell>
      </Table.Row>
    );

    rows.push(rowData);
  }
  /* eslint-enable */
  return rows;
};

const getData = (data, processRequest, viewRequest) => {
  /* eslint-disable */
  let rows = [];
  for (const key in data) {
    const request = data[key];
    const momentPaymentAt = moment(request.due_at, APP.DATE_TIME_FORMAT);
    const currentDate = moment();
    const diffDate = momentPaymentAt.diff(currentDate, 'days');
    const dueAt = momentPaymentAt.format(APP.DATE_DISPLAY_FORMAT);
    const pickupAt = moment(request.pickup_at, APP.DATE_TIME_FORMAT).format(
      APP.DATE_DISPLAY_FORMAT,
    );
    let buttonText = 'PROCESS';
    if (request.status === 'REQUESTED') {
      buttonText = 'PROCESS';
    } else if (request.status === 'PROCESSING') {
      buttonText = 'SCHEDULE';
    } else if (request.status === 'SCHEDULED') {
      buttonText = 'COMPLETE';
    } else if (request.status === 'COMPLETED') {
      buttonText = 'COMPLETED';
    } else if (request.status === 'CANCELLED') {
      buttonText = 'CANCELLED';
    }

    const payee = request.payee.name.toUpperCase();
    const payeeDisplay = request.type === 'PAYMENT' ? payee : 'N/A';
    const rowData = (
      <Table.Row key={key} error={(request.status !== 'COMPLETED') && diffDate < 3} id={request.id} onClick={() => viewRequest(request.id)}>
        <Table.Cell>
          {`${request.type}`}
        </Table.Cell>
        <Table.Cell>
          {`${request.request_id}`}
        </Table.Cell>
        <Table.Cell>
          {`${request.user.business_name}`}
        </Table.Cell>
        <Table.Cell>
          {`${request.user.first_name}
            ${request.user.last_name}`}
        </Table.Cell>
        <Table.Cell>{request.user.username}</Table.Cell>
        <Table.Cell>{dueAt}</Table.Cell>
        <Table.Cell>{pickupAt}</Table.Cell>

        <Table.Cell>{payeeDisplay}</Table.Cell>
        <Table.Cell>{formatMoney(request.amount)}</Table.Cell>
        <Table.Cell>{request.status}</Table.Cell>
        <Table.Cell>
          <Button
            positive
            onClick={e => {
              e.preventDefault();
              e.stopPropagation();
              processRequest(request);
            }}
            disabled={request.status === 'COMPLETED' || request.status === 'CANCELLED'}
          >
            {/*<Icon name="checkmark" />*/}
            {buttonText}
          </Button>
        </Table.Cell>
      </Table.Row>
    );

    rows.push(rowData);
  }
  /* eslint-enable */
  return rows;
};

const handlePagination = (e, data, pageAction, appData, filterParam = null) => {
  const { search, status, limit } = appData;
  const offset = (data.activePage - 1) * limit;
  const pageData = {
    limit,
    offset,
    searchStatus: status !== 'ALL' ? status : null,
    search,
    filterParam,
  };
  pageAction(pageData);
};

const handleMemberBalancesPagination = (
  e,
  data,
  pageAction,
  appData,
  filterParam = null,
) => {
  const { search, status } = appData;
  const limit = appData.memberUrlLimit;
  const offset = (data.activePage - 1) * limit;
  const pageData = {
    limit,
    offset,
    searchStatus: status !== 'ALL' ? status : null,
    search,
    filterParam,
  };
  pageAction(pageData);
};

const getMemberData = (data, viewRequest) => {
  /* eslint-disable */
  let rows = [];
  for (const key in data) {
    const request = data[key];
    const paymentDate = moment(request.due_at, APP.DATE_TIME_FORMAT).format(
      APP.DATE_DISPLAY_FORMAT,
    );
    let totalTransaction  = 0;
    if (request.type === 'DEPOSIT') {
      totalTransaction = parseFloat(request.amount) - parseFloat(request.fee);
    } else if (request.type === 'PAYMENT') {
      totalTransaction = parseFloat(request.amount) + parseFloat(request.fee);
    }

    const payee = request.payee.name.toUpperCase();
    const totalAmount = formatMoney(totalTransaction);
    const paymentAmount = formatMoney(request.amount);
    const feeAmount = formatMoney(request.fee);
    const payeeDisplay = request.type === 'PAYMENT' ? payee : 'N/A';
    const rowData = (
      <Table.Row id={key} key={key} onClick={() => viewRequest(request.id)}>
        <Table.Cell>{paymentDate}</Table.Cell>
        <Table.Cell>{request.type}</Table.Cell>
        <Table.Cell>{`$ ${paymentAmount}`}</Table.Cell>

        <Table.Cell>{payeeDisplay}</Table.Cell>
        <Table.Cell>{`$ ${feeAmount}`}</Table.Cell>
        <Table.Cell>{`$ ${totalAmount}`}</Table.Cell>
        <Table.Cell>{request.status}</Table.Cell>
      </Table.Row>
    );
    rows.push(rowData);
  }
  /* eslint-enable */
  return rows;
};

const createDropDownSearchFilter = (
  dropdownName,
  dropdownValue,
  options,
  setFilterState,
) => (
  <Dropdown
    value={dropdownValue}
    options={options}
    name={dropdownName}
    placeholder="Select User..."
    onChange={(e, { name, value }) => {
      setFilterState(name, value);
    }}
    selection
    fluid
    search
  />
);
const createDropdownFilter = (
  dropdownName,
  dropdownValue,
  options,
  setFilterState,
) => (
  <Dropdown
    value={dropdownValue}
    options={options}
    name={dropdownName}
    onChange={(e, { name, value }) => {
      setFilterState(name, value);
    }}
    selection
  />
);
const buildMemberAdvanceFilter = filterParam => {
  const typeOption = [
    {
      key: 0,
      text: 'All Transactions',
      value: 'ALL',
    },
    {
      key: 1,
      text: 'Deposit',
      value: 'DEPOSIT',
    },
    {
      key: 2,
      text: 'Payment',
      value: 'PAYMENT',
    },
  ];
  const statusOption = [
    {
      key: 0,
      text: 'ALL',
      value: 'ALL',
    },
    {
      key: 1,
      text: 'REQUESTED',
      value: 'REQUESTED',
    },
    {
      key: 2,
      text: 'PROCESSING',
      value: 'PROCESSING',
    },
    {
      key: 3,
      text: 'SCHEDULED',
      value: 'SCHEDULED',
    },
    {
      key: 4,
      text: 'COMPLETED',
      value: 'COMPLETED',
    },
    {
      key: 5,
      text: 'CANCELLED',
      value: 'CANCELLED',
    },
  ];
  const filterPeriodOption = [
    {
      key: 0,
      text: 'All',
      value: '',
    },
    {
      key: 1,
      text: 'Custom',
      value: 'custom',
    },
    {
      key: 2,
      text: 'Today',
      value: 'today',
    },
    {
      key: 3,
      text: 'Next 3 days',
      value: 'next_3_days',
    },
    {
      key: 4,
      text: 'This Week',
      value: 'this_week',
    },
    {
      key: 5,
      text: 'This Month',
      value: 'this_month',
    },
    {
      key: 6,
      text: 'This Quarter',
      value: 'this_quarter',
    },
    {
      key: 7,
      text: 'Last 30 days',
      value: 'last_30_days',
    },
    {
      key: 8,
      text: 'Last 90 days',
      value: 'last_90_days',
    },
    {
      key: 9,
      text: 'Last Month',
      value: 'last_month',
    },
    {
      key: 10,
      text: 'Last Quarter',
      value: 'last_quarter',
    },
    {
      key: 11,
      text: 'Last Year',
      value: 'last_year',
    },
    {
      key: 12,
      text: 'All dates',
      value: 'last_dates',
    },
  ];
  const dateOption = [
    {
      key: 0,
      text: 'Created Date',
      value: 'created_date',
    },
    {
      key: 1,
      text: 'Payment Date',
      value: 'payment_date',
    },
    {
      key: 2,
      text: 'Cash Pickup Date',
      value: 'pickup_date',
    },
  ];
  const {
    stateData,
    setFilterState,
    resetFilter,
    applyFilter,
    payeeData,
    handleDateFromChange,
    handleDateToChange,
  } = filterParam;
  const {
    filterType,
    filterStatus,
    filterPeriod,
    filterDate,
    filterDateFrom,
    filterDateTo,
    filterPayee,
    onFilterOpen,
  } = stateData;
  return (
    <Popup
      position="bottom left"
      wide
      flowing
      className="advanceFilterPopup"
      open={onFilterOpen}
      trigger={
        <Button onClick={() => setFilterState('onFilterOpen', true)}>
          <Icon name="filter" /> Filter
        </Button>
      }
      on="click"
    >
      <Grid columns="equal" className="advanceFilterGrid">
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterLabel">Category</div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterComponent">
              {createDropdownFilter(
                'filterType',
                filterType,
                typeOption,
                setFilterState,
              )}
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterLabel">Status</div>
          </Grid.Column>
          <Grid.Column>
            <div className="advanceFilterLabel">Date Type</div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterComponent">
              {createDropdownFilter(
                'filterStatus',
                filterStatus,
                statusOption,
                setFilterState,
              )}
            </div>
          </Grid.Column>
          <Grid.Column>
            <div className="advanceFilterComponent">
              {createDropdownFilter(
                'filterDate',
                filterDate,
                dateOption,
                setFilterState,
              )}
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterLabel">Period</div>
          </Grid.Column>
          <Grid.Column>
            <Grid>
              <Grid.Column width={8}>
                {filterPeriod === 'custom' ? (
                  <div className="advanceFilterLabel">From</div>
                ) : null}
              </Grid.Column>
              <Grid.Column width={8}>
                {filterPeriod === 'custom' ? (
                  <div className="advanceFilterLabel">To</div>
                ) : null}
              </Grid.Column>
            </Grid>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterComponent">
              {createDropdownFilter(
                'filterPeriod',
                filterPeriod,
                filterPeriodOption,
                setFilterState,
              )}
            </div>
          </Grid.Column>
          <Grid.Column>
            <Grid>
              <Grid.Column width={8}>
                <div className="advanceFilterComponent">
                  {filterPeriod === 'custom' ? (
                    <DatePicker
                      className="advanceFilterDatePicker"
                      calendarClassName="advanceFilterCalendar"
                      dateFormat="MM/DD/YYYY"
                      selected={new Date(filterDateFrom)}
                      onChange={handleDateFromChange}
                    />
                  ) : null}
                </div>
              </Grid.Column>
              <Grid.Column width={8}>
                <div className="advanceFilterComponent">
                  {filterPeriod === 'custom' ? (
                    <DatePicker
                      className="advanceFilterDatePicker"
                      calendarClassName="advanceFilterCalendar"
                      dateFormat="MM/DD/YYYY"
                      selected={new Date(filterDateTo)}
                      onChange={handleDateToChange}
                    />
                  ) : null}
                </div>
              </Grid.Column>
            </Grid>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterLabel">Payee</div>
          </Grid.Column>
          <Grid.Column>&nbsp;</Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterComponent">
              {createDropdownFilter(
                'filterPayee',
                filterPayee,
                payeeData,
                setFilterState,
              )}
            </div>
          </Grid.Column>
          <Grid.Column>&nbsp;</Grid.Column>
        </Grid.Row>
        {/*
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterLabel">Category</div>
          </Grid.Column>
          <Grid.Column>&nbsp;</Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={4}>
            <div className="advanceFilterComponent">
              {createDropdownFilter(
                'filterCategory',
                filterCategory,
                typeOption,
                setFilterState,
              )}
            </div>
          </Grid.Column>
          <Grid.Column width={4}>&nbsp;</Grid.Column>
        </Grid.Row>
        */}
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterComponent">
              <Button
                onClick={() => {
                  resetFilter();
                  setFilterState('onFilterOpen', false);
                  setFilterState('isFilterRan', false);
                }}
                color="grey"
              >
                Reset
              </Button>
              <Button
                onClick={() => setFilterState('onFilterOpen', false)}
                color="grey"
              >
                Close
              </Button>
            </div>
          </Grid.Column>
          <Grid.Column>
            <div className="advanceFilterApply">
              <Button
                onClick={() => {
                  setFilterState('onFilterOpen', false);
                  setFilterState('isFilterRan', true);
                  applyFilter();
                }}
                color="green"
              >
                Apply
              </Button>
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Popup>
  );
};
const buildAdminAdvanceFilter = filterParam => {
  const typeOption = [
    {
      key: 0,
      text: 'All Transactions',
      value: 'ALL',
    },
    {
      key: 1,
      text: 'Deposit',
      value: 'DEPOSIT',
    },
    {
      key: 2,
      text: 'Payment',
      value: 'PAYMENT',
    },
  ];
  const statusOption = [
    {
      key: 0,
      text: 'ALL',
      value: 'ALL',
    },
    {
      key: 1,
      text: 'REQUESTED',
      value: 'REQUESTED',
    },
    {
      key: 2,
      text: 'PROCESSING',
      value: 'PROCESSING',
    },
    {
      key: 3,
      text: 'SCHEDULED',
      value: 'SCHEDULED',
    },
    {
      key: 4,
      text: 'COMPLETED',
      value: 'COMPLETED',
    },
    {
      key: 5,
      text: 'CANCELLED',
      value: 'CANCELLED',
    },
  ];
  const filterPeriodOption = [
    {
      key: 0,
      text: 'All',
      value: '',
    },
    {
      key: 1,
      text: 'Custom',
      value: 'custom',
    },
    {
      key: 2,
      text: 'Today',
      value: 'today',
    },
    {
      key: 3,
      text: 'Next 3 days',
      value: 'next_3_days',
    },
    {
      key: 4,
      text: 'This Week',
      value: 'this_week',
    },
    {
      key: 5,
      text: 'This Month',
      value: 'this_month',
    },
    {
      key: 6,
      text: 'This Quarter',
      value: 'this_quarter',
    },
    {
      key: 7,
      text: 'Last 30 days',
      value: 'last_30_days',
    },
    {
      key: 8,
      text: 'Last 90 days',
      value: 'last_90_days',
    },
    {
      key: 9,
      text: 'Last Month',
      value: 'last_month',
    },
    {
      key: 10,
      text: 'Last Quarter',
      value: 'last_quarter',
    },
    {
      key: 11,
      text: 'Last Year',
      value: 'last_year',
    },
    {
      key: 12,
      text: 'All dates',
      value: 'last_dates',
    },
  ];
  const dateOption = [
    {
      key: 0,
      text: 'Created Date',
      value: 'created_date',
    },
    {
      key: 1,
      text: 'Payment Date',
      value: 'payment_date',
    },
    {
      key: 2,
      text: 'Cash Pickup Date',
      value: 'pickup_date',
    },
  ];
  const {
    stateData,
    setFilterState,
    resetFilter,
    applyFilter,
    payeeData,
    handleDateFromChange,
    handleDateToChange,
    userMemberOption,
  } = filterParam;
  const {
    filterType,
    filterStatus,
    filterPeriod,
    filterDate,
    filterDateFrom,
    filterDateTo,
    filterPayee,
    onFilterOpen,
    filterUser,
  } = stateData;
  return (
    <Popup
      position="bottom left"
      wide
      flowing
      className="advanceFilterPopup"
      open={onFilterOpen}
      trigger={
        <Button onClick={() => setFilterState('onFilterOpen', true)}>
          <Icon name="filter" /> Filter
        </Button>
      }
      on="click"
    >
      <Grid columns="equal" className="advanceFilterGrid">
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterLabel">Category</div>
          </Grid.Column>
          <Grid.Column>
            <div className="advanceFilterLabel">Member</div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterComponent">
              {createDropdownFilter(
                'filterType',
                filterType,
                typeOption,
                setFilterState,
              )}
            </div>
          </Grid.Column>
          <Grid.Column>
            <div className="advanceFilterComponent">
              {createDropDownSearchFilter(
                'filterUser',
                filterUser,
                userMemberOption,
                setFilterState,
              )}
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterLabel">Status</div>
          </Grid.Column>
          <Grid.Column>
            <div className="advanceFilterLabel">Date Type</div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterComponent">
              {createDropdownFilter(
                'filterStatus',
                filterStatus,
                statusOption,
                setFilterState,
              )}
            </div>
          </Grid.Column>
          <Grid.Column>
            <div className="advanceFilterComponent">
              {createDropdownFilter(
                'filterDate',
                filterDate,
                dateOption,
                setFilterState,
              )}
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterLabel">Period</div>
          </Grid.Column>
          <Grid.Column>
            <Grid>
              <Grid.Column width={8}>
                {filterPeriod === 'custom' ? (
                  <div className="advanceFilterLabel">From</div>
                ) : null}
              </Grid.Column>
              <Grid.Column width={8}>
                {filterPeriod === 'custom' ? (
                  <div className="advanceFilterLabel">To</div>
                ) : null}
              </Grid.Column>
            </Grid>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterComponent">
              {createDropdownFilter(
                'filterPeriod',
                filterPeriod,
                filterPeriodOption,
                setFilterState,
              )}
            </div>
          </Grid.Column>
          <Grid.Column>
            <Grid>
              <Grid.Column width={8}>
                <div className="advanceFilterComponent">
                  {filterPeriod === 'custom' ? (
                    <DatePicker
                      className="advanceFilterDatePicker"
                      calendarClassName="advanceFilterCalendar"
                      dateFormat="yyyy-MM-dd"
                      selected={new Date(filterDateFrom)}
                      onChange={handleDateFromChange}
                    />
                  ) : null}
                </div>
              </Grid.Column>
              <Grid.Column width={8}>
                <div className="advanceFilterComponent">
                  {filterPeriod === 'custom' ? (
                    <DatePicker
                      className="advanceFilterDatePicker"
                      calendarClassName="advanceFilterCalendar"
                      dateFormat="yyyy-MM-dd"
                      selected={new Date(filterDateTo)}
                      onChange={handleDateToChange}
                    />
                  ) : null}
                </div>
              </Grid.Column>
            </Grid>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterLabel">Payee</div>
          </Grid.Column>
          <Grid.Column>&nbsp;</Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterComponent">
              {createDropdownFilter(
                'filterPayee',
                filterPayee,
                payeeData,
                setFilterState,
              )}
            </div>
          </Grid.Column>
          <Grid.Column>&nbsp;</Grid.Column>
        </Grid.Row>
        {/*
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterLabel">Category</div>
          </Grid.Column>
          <Grid.Column>&nbsp;</Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={4}>
            <div className="advanceFilterComponent">
              {createDropdownFilter(
                'filterCategory',
                filterCategory,
                typeOption,
                setFilterState,
              )}
            </div>
          </Grid.Column>
          <Grid.Column width={4}>&nbsp;</Grid.Column>
        </Grid.Row>
        */}
        <Grid.Row>
          <Grid.Column>
            <div className="advanceFilterComponent">
              <Button
                onClick={() => {
                  resetFilter();
                  setFilterState('onFilterOpen', false);
                  setFilterState('isFilterRan', false);
                }}
                color="grey"
              >
                Reset
              </Button>
              <Button
                onClick={() => setFilterState('onFilterOpen', false)}
                color="grey"
              >
                Close
              </Button>
            </div>
          </Grid.Column>
          <Grid.Column>
            <div className="advanceFilterApply">
              <Button
                onClick={() => {
                  setFilterState('onFilterOpen', false);
                  setFilterState('isFilterRan', true);
                  applyFilter();
                }}
                color="green"
              >
                Apply
              </Button>
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Popup>
  );
};

const buildAdminFilterView = filterParam => {
  const { stateData, resetFilter, setFilterState, payeeData } = filterParam;
  const {
    filterType,
    filterStatus,
    filterPeriod,
    filterDate,
    filterDateFrom,
    filterDateTo,
    filterPayee,
    isFilterRan,
  } = stateData;
  let transactions = '';
  if (filterType !== 'ALL') {
    transactions = filterType === 'DEPOSIT' ? 'Deposit / ' : 'Payment / ';
  }
  let status = '';
  if (filterStatus !== 'ALL') {
    switch (filterStatus) {
      case 'REQUESTED':
        status = 'Requested / ';
        break;
      case 'PROCESSING':
        status = 'Processing / ';
        break;
      case 'SCHEDULED':
        status = 'Scheduled / ';
        break;
      case 'COMPLETED':
        status = 'Completed / ';
        break;
      case 'CANCELLED':
        status = 'Cancelled / ';
        break;
      default:
        status = 'Requested / ';
        break;
    }
  }
  let period = '';
  if (filterPeriod !== 'custom') {
    switch (filterPeriod) {
      case 'today':
        period = 'Today / ';
        break;
      case 'next_3_days':
        period = 'Next 3 Days / ';
        break;
      case 'this_week':
        period = 'This Week / ';
        break;
      case 'this_month':
        period = 'This Month / ';
        break;
      case 'this_quarter':
        period = 'This Quarter / ';
        break;
      case 'last_30_days':
        period = 'Last 30 Days / ';
        break;
      case 'last_90_days':
        period = 'Last 90 Days / ';
        break;
      case 'last_month':
        period = 'Last Month / ';
        break;
      case 'last_quarter':
        period = 'Last Quarter / ';
        break;
      case 'last_year':
        period = 'Last Year / ';
        break;
      case 'all_dates':
        period = 'All Dates / ';
        break;
      default:
        period = 'Today / ';
        break;
    }
  }
  let dateFilter = '';
  if (filterDate === 'payment_date') {
    dateFilter = 'Payment Date / ';
  } else if (filterDate === 'pickup_date') {
    dateFilter = 'Pickup Date / ';
  } else if (filterDate === 'created_date') {
    dateFilter = 'Created Date / ';
  }

  let dateRange = '';
  if (filterPeriod === 'custom') {
    const from = moment(filterDateFrom).format(APP.DATE_DISPLAY_FORMAT);
    const to = moment(filterDateTo).format(APP.DATE_DISPLAY_FORMAT);
    dateRange = `${from} to ${to} / `;
  }
  let payeeName = '';
  /* eslint-disable */
  if (payeeData) {
    for (let i = 0; i < payeeData.length; i++) {
      const payeeObject = payeeData[i];
      if (payeeObject.value === filterPayee) {
        payeeName = `${payeeObject.text} / `;
      }
    }
  }
  /* eslint-enable */
  return isFilterRan ? (
    <span>
      {`${transactions}
      ${status}
      ${period}
      ${dateFilter}
      ${dateRange}
      ${payeeName}`}
      <ALink
        onClick={() => {
          resetFilter();
          setFilterState('onFilterOpen', false);
          setFilterState('isFilterRan', false);
        }}
      >
        Clear/View All
      </ALink>
    </span>
  ) : null;
};
const buildExportToCsv = filterParam => (
  <Button onClick={() => filterParam.exportCSV()}>
    <Icon name="file excel" />
    Export to CSV
  </Button>
);

const validatePassword = passwordText => {
  /* eslint-disable */
  const passwordRegEx = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$');
  const result = passwordRegEx.test(passwordText);
  return result;
  /* eslint-enable */
};

const buildErrorModal = (isOpen, onClickAction, content) => (
  <StandardModal
    onContinue={() => onClickAction('isErrorMessageOpen', false)}
    onClose={() => onClickAction('isErrorMessageOpen', false)}
    open={isOpen}
    headerText="Something went wrong."
    onActionText="Close"
  >
    <div>{content}</div>
  </StandardModal>
);

const buildSuccessModal = (isOpen, onClickAction) => (
  <StandardModal
    onContinue={() => onClickAction()}
    onClose={() => onClickAction()}
    open={isOpen}
    headerText="Congratulations"
    onActionText="Continue to Dashboard"
  >
    <div>
      Congratulations! You have successfully created your User Id and Password.
      Please use this moving forward to access your Titan Vault account. Thank
      you
    </div>
  </StandardModal>
);

const getTopFiveDetails = (topRequest, viewRequest) => {
  /* eslint-disable */
  if (!topRequest) return null;
  let topRequestDetails = [];
  for(let i = 0; i < topRequest.length; i++) {
    const request = topRequest[i];
    // const payAtTime = moment(request.updated_at).format("HH:mm");
    // const payAtDay = moment(request.updated_at).format(APP.DATE_DISPLAY_FORMAT);
    let imageToSet = CheckImage;
    if (request.status === 'PROCESSING') {
      imageToSet = ProcessingImage;
    } else if (request.status === 'REQUESTED') {
      imageToSet = RequestedImage;
    } else if (request.status === 'CANCELLED') {
      imageToSet = CrossImage;
    }
    let netAmount = 0;
    if (request.type === 'DEPOSIT') {
      netAmount = parseFloat(request.amount) - parseFloat(request.fee);
    } else {
      netAmount = parseFloat(request.amount) + parseFloat(request.fee);
    }
    const row = (
      <Table.Row id={request.id} key={i} onClick={() => viewRequest(request.id)}>
          <Table.Cell>
            <Header as="h5" image>
              <Image src={imageToSet} rounded size="small" />
              <Header.Content>
                <h6>{request.status}</h6>
                {/* <Header.Subheader>{payAtDay} at {payAtTime}</Header.Subheader> */}
              </Header.Content>
            </Header>
          </Table.Cell>
          <Table.Cell><h6>{request.type}</h6></Table.Cell>
          <Table.Cell>{formatMoney(request.amount)}</Table.Cell>
          <Table.Cell>{`(${formatMoney(request.fee)})`}</Table.Cell>
          <Table.Cell>{formatMoney(netAmount)}</Table.Cell>
        </Table.Row>
    );
    topRequestDetails.push(row);
  }
  /* eslint-enable */
  return (
    <Table basic="very" celled structured>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>
            <h5>Status</h5>
          </Table.HeaderCell>
          <Table.HeaderCell>
            <h5>Type</h5>
          </Table.HeaderCell>
          <Table.HeaderCell>
            <h5>Gross</h5>
          </Table.HeaderCell>
          <Table.HeaderCell>
            <h5>Fee</h5>
          </Table.HeaderCell>
          <Table.HeaderCell>
            <h5>Amount</h5>
          </Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>{topRequestDetails}</Table.Body>
    </Table>
  );
};

export {
  onChangeData,
  handleSort,
  getData,
  handlePagination,
  getMemberData,
  buildAdminAdvanceFilter,
  validatePassword,
  buildErrorModal,
  buildSuccessModal,
  buildAdminFilterView,
  getTopFiveDetails,
  buildMemberAdvanceFilter,
  buildExportToCsv,
  getMemberBalance,
  handleMemberBalancesPagination,
};
