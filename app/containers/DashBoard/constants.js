/*
 *
 * DashBoard constants
 *
 */

export const DEFAULT_ACTION = 'app/DashBoard/DEFAULT_ACTION';
export const GET_ADMIN_REQUEST = 'app/DashBoard/GET_ADMIN_REQUEST';
export const GET_ADMIN_SUCCESS = 'app/DashBoard/GET_ADMIN_SUCCESS';
export const GET_ADMIN_FAILED = 'app/DashBoard/GET_ADMIN_FAILED';

export const GET_MEMBER_REQUEST = 'app/DashBoard/GET_MEMBER_REQUEST';
export const GET_MEMBER_SUCCESS = 'app/DashBoard/GET_MEMBER_SUCCESS';
export const GET_MEMBER_FAILED = 'app/DashBoard/GET_MEMBER_FAILED';

export const PROCESS_REQUEST = 'app/DashBoard/PROCESS_REQUEST';
export const PROCESS_REQUEST_SUCCESS = 'app/DashBoard/PROCESS_REQUEST_SUCCESS';
export const PROCESS_REQUEST_FAILED = 'app/DashBoard/PROCESS_REQUEST_FAILED';

export const VIEW_REQUEST = 'app/DashBoard/VIEW_REQUEST';
export const VIEW_REQUEST_SUCCESS = 'app/DashBoard/VIEW_REQUEST_SUCCESS';
export const VIEW_REQUEST_ERROR = 'app/DashBoard/VIEW_REQUEST_ERROR';

export const PAGE_REQUEST = 'app/DashBoard/PAGE_REQUEST';
export const PAGE_REQUEST_SUCCESS = 'app/DashBoard/PAGE_REQUEST_SUCCESS';
export const PAGE_REQUEST_FAILED = 'app/DashBoard/PAGE_REQMEMBER_UEST_FAILED';

export const MEMBER_PAGE_REQUEST = 'app/DashBoard/MEMBER_PAGE_REQUEST';
export const MEMBER_PAGE_REQUEST_SUCCESS =
  'app/DashBoard/MEMBER_PAGE_REQUEST_SUCCESS';
export const MEMBER_PAGE_REQUEST_FAILED =
  'app/DashBoard/MEMBER_PAGE_REQUEST_FAILED';

export const UPDATE_CREDENTIALS = 'app/DashBoard/UPDATE_CREDENTIALS';
export const UPDATE_CREDENTIALS_SUCCESS =
  'app/DashBoard/UPDATE_CREDENTIALS_SUCCESS';
export const UPDATE_CREDENTIALS_FAILURE =
  'app/DashBoard/UPDATE_CREDENTIALS_FAILURE';

export const FILTER_REQUEST = 'app/DashBoard/FILTER_REQUEST';
export const FILTER_REQUEST_SUCCESS = 'app/DashBoard/FILTER_REQUEST_SUCCESS';
export const FILTER_REQUEST_FAILED = 'app/DashBoard/FILTER_REQUEST_FAILED';

export const GET_PAYEE = 'app/DashBoard/GET_PAYEE';
export const GET_PAYEE_SUCCESS = 'app/DashBoard/GET_PAYEE_SUCCESS';
export const GET_PAYEE_FAILED = 'app/DashBoard/GET_PAYEE_FAILED';

export const UPDATE_FIRST_TIME_MEMBER =
  'app/DashBoard/UPDATE_FIRST_TIME_MEMBER';
export const UPDATE_FIRST_TIME_MEMBER_SUCCESS =
  'app/DashBoard/UPDATE_FIRST_TIME_MEMBER_SUCCESS';
export const UPDATE_FIRST_TIME_MEMBER_ERROR =
  'app/DashBoard/UPDATE_FIRST_TIME_MEMBER_ERROR';

export const PROCEED_TO_DASHBOARD = 'app/DashBoard/PROCEED_TO_DASHBOARD';

export const GET_USER_FEE_RATE = 'app/DashBoard/GET_USER_FEE_RATE';
export const GET_USER_FEE_RATE_ERROR = 'app/DashBoard/GET_USER_FEE_RATE_ERROR';

export const GET_USER_FEE_TIER_RATE = 'app/DashBoard/GET_USER_FEE_TIER_RATE';
export const GET_USER_FEE_TIER_RATE_ERROR =
  'app/DashBoard/GET_USER_FEE_TIER_RATE_ERROR';

export const GET_WALLET = 'app/DashBoard/GET_WALLET';
export const GET_WALLET_SUCCESS = 'app/DashBoard/GET_WALLET_SUCCESS';
export const GET_WALLET_ERROR = 'app/DashBoard/GET_WALLET_ERROR';

export const GET_TOP_REQUEST = 'app/Dashboard/GET_TOP_REQUEST';
export const GET_TOP_REQUEST_SUCCESS = 'app/Dashboard/GET_TOP_REQUEST_SUCCESS';
export const GET_TOP_REQUEST_ERROR = 'app/Dashboard/GET_TOP_REQUEST_ERROR';

export const GET_MEMBER_USERS = 'app/Dashboard/GET_MEMBER_USERS';
export const GET_MEMBER_USERS_SUCCESS =
  'app/Dashboard/GET_MEMBER_USERS_SUCCESS';
export const GET_MEMBER_USERS_ERROR = 'app/Dashboard/GET_MEMBER_USERS_ERROR';

export const SET_NEW_MEMBER_DATA = 'app/Dashboard/SET_NEW_MEMBER_DATA';
export const GET_NEXT_MEMBER_PAGE = 'app/Dashboard/GET_NEXT_MEMBER_PAGE';

export const FILTER_MEMBER_REQUEST = 'app/DashBoard/FILTER_MEMBER_REQUEST';
export const FILTER_MEMBER_REQUEST_SUCCESS =
  'app/DashBoard/FILTER_MEMBER_REQUEST_SUCCESS';
export const FILTER_MEMBER_REQUEST_FAILED =
  'app/DashBoard/FILTER_MEMBER_REQUEST_FAILED';

export const EXPORT_CSV = 'app/DashBoard/EXPORT_CSV';
export const EXPORT_CSV_SUCCESS = 'app/DashBoard/EXPORT_CSV_SUCCESS';
export const EXPORT_CSV_ERROR = 'app/DashBoard/EXPORT_ERROR';

export const MEMBER_BALANCE = 'app/DashBoard/MEMBER_BALANCE';
export const MEMBER_BALANCE_SUCCESS = 'app/DashBoard/MEMBER_BALANCE_SUCCESS';
export const MEMBER_BALANCE_ERROR = 'app/DashBoard/MEMBER_BALANCE_ERROR';

export const MEMBER_BALANCE_PAGE = 'app/DashBoard/MEMBER_BALANCE_PAGE';
export const MEMBER_BALANCE_PAGE_SUCCESS =
  'app/DashBoard/MEMBER_BALANCE_PAGE_SUCCESS';
export const MEMBER_BALANCE_PAGE_ERROR =
  'app/DashBoard/MEMBER_BALANCE_PAGE_ERROR';
