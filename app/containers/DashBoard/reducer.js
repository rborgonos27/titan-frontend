/*
 *
 * DashBoard reducer
 *
 */

import { fromJS } from 'immutable';
import {
  GET_ADMIN_REQUEST,
  GET_ADMIN_SUCCESS,
  GET_ADMIN_FAILED,
  GET_MEMBER_REQUEST,
  GET_MEMBER_SUCCESS,
  GET_MEMBER_FAILED,
  PROCESS_REQUEST,
  PROCESS_REQUEST_SUCCESS,
  PROCESS_REQUEST_FAILED,
  PAGE_REQUEST,
  PAGE_REQUEST_SUCCESS,
  PAGE_REQUEST_FAILED,
  MEMBER_PAGE_REQUEST,
  MEMBER_PAGE_REQUEST_SUCCESS,
  MEMBER_PAGE_REQUEST_FAILED,
  UPDATE_CREDENTIALS,
  UPDATE_CREDENTIALS_SUCCESS,
  UPDATE_CREDENTIALS_FAILURE,
  FILTER_REQUEST,
  FILTER_REQUEST_SUCCESS,
  FILTER_REQUEST_FAILED,
  GET_PAYEE,
  GET_PAYEE_SUCCESS,
  GET_PAYEE_FAILED,
  UPDATE_FIRST_TIME_MEMBER,
  UPDATE_FIRST_TIME_MEMBER_SUCCESS,
  UPDATE_FIRST_TIME_MEMBER_ERROR,
  GET_USER_FEE_RATE,
  GET_USER_FEE_RATE_ERROR,
  GET_WALLET,
  GET_WALLET_SUCCESS,
  GET_WALLET_ERROR,
  GET_TOP_REQUEST,
  GET_TOP_REQUEST_ERROR,
  GET_TOP_REQUEST_SUCCESS,
  GET_MEMBER_USERS,
  GET_MEMBER_USERS_ERROR,
  GET_MEMBER_USERS_SUCCESS,
  SET_NEW_MEMBER_DATA,
  GET_NEXT_MEMBER_PAGE,
  FILTER_MEMBER_REQUEST,
  FILTER_MEMBER_REQUEST_SUCCESS,
  FILTER_MEMBER_REQUEST_FAILED,
  EXPORT_CSV,
  EXPORT_CSV_SUCCESS,
  EXPORT_CSV_ERROR,
  MEMBER_BALANCE,
  MEMBER_BALANCE_ERROR,
  MEMBER_BALANCE_SUCCESS,
  MEMBER_BALANCE_PAGE,
  MEMBER_BALANCE_PAGE_SUCCESS,
  MEMBER_BALANCE_PAGE_ERROR,
} from './constants';

export const initialState = fromJS({
  loading: false,
  transactionData: [],
  pageNumber: 0,
  nextUrl: null,
  previousUrl: null,
  error: null,
  credentialsError: null,
  payeeData: [],
  userUpdatedData: null,
  showReadyNotification: false,
  currentUser: null,
  feeObject: null,
  wallet: null,
  topFiveError: null,
  topFiveRequest: null,
  topFiveLoading: false,
  membersData: null,
  membersLoading: false,
  newMemberData: false,
  memberBalances: [],
  memberBalancesLoading: false,
  memberBalancePageNumber: 0,
  memberBalancePageNextUrl: null,
  memberBalancePagePreviousUrl: null,
});

function dashBoardReducer(state = initialState, action) {
  switch (action.type) {
    case GET_ADMIN_REQUEST:
      return state.set('loading', true);
    case GET_MEMBER_REQUEST:
      return state.set('loading', true);
    case GET_ADMIN_SUCCESS:
      return state
        .set('loading', false)
        .set('pageNumber', action.data.count)
        .set('nextUrl', action.data.next)
        .set('previousUrl', action.data.previous)
        .set('transactionData', action.data.results);
    case GET_MEMBER_SUCCESS:
      return state
        .set('loading', false)
        .set('pageNumber', action.data.count)
        .set('nextUrl', action.data.next)
        .set('previousUrl', action.data.previous)
        .set('transactionData', action.data.results);
    case GET_ADMIN_FAILED:
      return state
        .set('loading', false)
        .set('transactionData', [])
        .set('error', action.error);
    case GET_MEMBER_FAILED:
      return state
        .set('loading', false)
        .set('transactionData', [])
        .set('error', action.error);
    case PROCESS_REQUEST:
      return state.set('loading', true);
    case PROCESS_REQUEST_SUCCESS:
      return state.set('loading', false);
    case PROCESS_REQUEST_FAILED:
      return state.set('loading', false);
    case PAGE_REQUEST:
      return state.set('loading', true);
    case PAGE_REQUEST_SUCCESS:
      return state
        .set('loading', false)
        .set('pageNumber', action.data.count)
        .set('nextUrl', action.data.next)
        .set('previousUrl', action.data.previous)
        .set('transactionData', action.data.results);
    case PAGE_REQUEST_FAILED:
      return state
        .set('loading', false)
        .set('transactionData', [])
        .set('error', action.error);
    case MEMBER_PAGE_REQUEST:
      return state.set('loading', true);
    case MEMBER_PAGE_REQUEST_SUCCESS:
      return state
        .set('loading', false)
        .set('pageNumber', action.data.count)
        .set('nextUrl', action.data.next)
        .set('previousUrl', action.data.previous)
        .set('transactionData', action.data.results);
    case MEMBER_PAGE_REQUEST_FAILED:
      return state
        .set('loading', false)
        .set('transactionData', [])
        .set('error', action.error);
    case UPDATE_CREDENTIALS:
      return state.set('loading', true).set('credentialsError', null);
    case UPDATE_CREDENTIALS_SUCCESS:
      return state.set('loading', false).set('credentialsError', null);
    case UPDATE_CREDENTIALS_FAILURE:
      return state.set('loading', false).set('credentialsError', action.error);
    case FILTER_REQUEST:
      return state.set('loading', true);
    case FILTER_REQUEST_SUCCESS:
      return state
        .set('loading', false)
        .set('pageNumber', action.data.count)
        .set('nextUrl', action.data.next)
        .set('previousUrl', action.data.previous)
        .set('transactionData', action.data.results);
    case FILTER_REQUEST_FAILED:
      return state
        .set('loading', false)
        .set('transactionData', [])
        .set('error', action.error);
    case FILTER_MEMBER_REQUEST:
      return state.set('loading', true);
    case FILTER_MEMBER_REQUEST_SUCCESS:
      return state
        .set('loading', false)
        .set('pageNumber', action.data.count)
        .set('nextUrl', action.data.next)
        .set('previousUrl', action.data.previous)
        .set('transactionData', action.data.results);
    case FILTER_MEMBER_REQUEST_FAILED:
      return state
        .set('loading', false)
        .set('transactionData', [])
        .set('error', action.error);
    case GET_PAYEE:
      return state.set('loading', true);
    case GET_PAYEE_SUCCESS:
      return state.set('loading', false).set('payeeData', action.data);
    case GET_PAYEE_FAILED:
      return state
        .set('loading', false)
        .set('payeeData', [])
        .set('error', action.error);
    case UPDATE_FIRST_TIME_MEMBER:
      return state
        .set('loading', true)
        .set('showReadyNotification', false)
        .set('userUpdatedData', null)
        .set('credentialsError', null);
    case UPDATE_FIRST_TIME_MEMBER_SUCCESS:
      return state
        .set('loading', false)
        .set('userUpdatedData', action.data)
        .set('showReadyNotification', true)
        .set('credentialsError', null);
    case UPDATE_FIRST_TIME_MEMBER_ERROR:
      return state
        .set('loading', false)
        .set('userUpdatedData', null)
        .set('showReadyNotification', false)
        .set('credentialsError', action.error);
    case GET_USER_FEE_RATE:
      return state
        .set('loading', false)
        .set('error', null)
        .set('feeObject', null);
    case GET_USER_FEE_RATE_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('feeObject', null);
    case GET_WALLET:
      return state
        .set('loading', true)
        .set('error', null)
        .set('wallet', null);
    case GET_WALLET_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('wallet', action.data);
    case GET_WALLET_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('wallet', null);
    case GET_TOP_REQUEST:
      return state
        .set('topFiveLoading', true)
        .set('topFiveError', null)
        .set('topFiveRequest', null);
    case GET_TOP_REQUEST_SUCCESS:
      return state
        .set('topFiveLoading', false)
        .set('topFiveError', null)
        .set('topFiveRequest', action.data);
    case GET_TOP_REQUEST_ERROR:
      return state
        .set('topFiveLoading', false)
        .set('topFiveError', action.error)
        .set('topFiveRequest', null);
    case GET_MEMBER_USERS:
      return state
        .set('membersLoading', true)
        .set('membersData', null)
        .set('newMemberData', false)
        .set('error', null);
    case GET_NEXT_MEMBER_PAGE:
      return state
        .set('membersLoading', true)
        .set('membersData', null)
        .set('newMemberData', false)
        .set('error', null);
    case GET_MEMBER_USERS_SUCCESS:
      return state
        .set('membersLoading', false)
        .set('membersData', action.data)
        .set('newMemberData', true)
        .set('error', null);
    case GET_MEMBER_USERS_ERROR:
      return state
        .set('membersLoading', false)
        .set('membersData', null)
        .set('newMemberData', false)
        .set('error', action.error);
    case SET_NEW_MEMBER_DATA:
      return state.set('newMemberData', action.data);
    case EXPORT_CSV:
      return state.set('loading', true).set('error', null);
    case EXPORT_CSV_SUCCESS:
      return state.set('loading', false).set('error', null);
    case EXPORT_CSV_ERROR:
      return state.set('loading', false).set('error', action.error);
    case MEMBER_BALANCE:
      return state
        .set('memberBalancesLoading', true)
        .set('memberBalances', [])
        .set('error', null);
    case MEMBER_BALANCE_SUCCESS:
      return state
        .set('memberBalancesLoading', false)
        .set('memberBalances', action.data.results)
        .set('memberBalancePageNumber', action.data.count)
        .set('memberBalancePageNextUrl', action.data.next)
        .set('memberBalancePagePreviousUrl', action.data.previous)
        .set('error', null);
    case MEMBER_BALANCE_ERROR:
      return state
        .set('memberBalancesLoading', false)
        .set('memberBalances', [])
        .set('memberBalancePageNumber', 0)
        .set('memberBalancePageNextUrl', null)
        .set('memberBalancePagePreviousUrl', null)
        .set('error', action.error);
    case MEMBER_BALANCE_PAGE:
      return state.set('memberBalancesLoading', true).set('error', null);
    case MEMBER_BALANCE_PAGE_SUCCESS:
      return state
        .set('memberBalances', action.data.results)
        .set('memberBalancePageNumber', action.data.count)
        .set('memberBalancePageNextUrl', action.data.next)
        .set('memberBalancePagePreviousUrl', action.data.previous)
        .set('error', null);
    case MEMBER_BALANCE_PAGE_ERROR:
      return state.set('error', action.error);
    default:
      return state;
  }
}

export default dashBoardReducer;
