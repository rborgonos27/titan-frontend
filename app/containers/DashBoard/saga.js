import { call, put, takeLatest, all } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import ReactGA from 'react-ga';
import moment from 'moment';
import { APP, GA } from 'config';
import request, { getBlob, download } from 'utils/request';
import { optionsFormData } from 'utils/options';
import {
  loginSuccessAction,
  getCurrentUser,
  getUserFeeRateSuccess,
  getUserFeeTierRateSuccess,
} from 'containers/App/actions';
import {
  GET_ADMIN_REQUEST,
  GET_MEMBER_REQUEST,
  PROCESS_REQUEST,
  VIEW_REQUEST,
  PAGE_REQUEST,
  MEMBER_PAGE_REQUEST,
  UPDATE_CREDENTIALS,
  FILTER_REQUEST,
  GET_PAYEE,
  UPDATE_FIRST_TIME_MEMBER,
  PROCEED_TO_DASHBOARD,
  GET_USER_FEE_RATE,
  GET_USER_FEE_TIER_RATE,
  GET_WALLET,
  GET_TOP_REQUEST,
  GET_MEMBER_USERS,
  GET_NEXT_MEMBER_PAGE,
  FILTER_MEMBER_REQUEST,
  EXPORT_CSV,
  MEMBER_BALANCE,
  MEMBER_BALANCE_PAGE,
} from './constants';
import {
  getAdminRequestSuccess,
  getAdminRequestFailed,
  getAdminMemberSuccess,
  getAdminMemberFailed,
  processRequestSuccess,
  processRequestFailed,
  getMemberRequest,
  getAdminRequest,
  pageRequestSuccess,
  pageRequestFailed,
  memberPageRequestSuccess,
  memberPageRequestFailed,
  updateCredentialsError,
  updateCreadentialsSuccess,
  filterRequestSucess,
  filterRequestFailed,
  getPayeeSuccess,
  getPayeeFailed,
  updateFirstTimeMemberError,
  updateFirstTimeMemberSuccess,
  getUserFeeRateError,
  getUserFeeTierRateError,
  getWalletError,
  getWalletSuccess,
  getTopRequestSuccess,
  getTopRequestError,
  getMemberUsersError,
  getMemberUsersSuccess,
  filterMemberRequestFailed,
  filterMemberRequestSucess,
  exportCSVError,
  exportCSVSuccess,
  memberBalanceError,
  memberBalanceSuccess,
  memberBalancesPageSuccess,
  memberBalancesPageError,
} from './actions';

export function* getAdminData() {
  const API_URI = APP.API_URL;
  ReactGA.initialize(GA.TRACKING_ID);
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    ReactGA.event({
      category: 'Dashboard',
      action: `Fetching transaction requests`,
      label: 'Admin Record',
    });
    const transactions = yield call(
      request,
      `${API_URI}/admin/request/`,
      optionsFormData('GET', null, token),
    );
    if (transactions.results) {
      ReactGA.event({
        category: 'Dashboard',
        action: `Successfully fetched the requests`,
        label: 'Admin Record Success',
      });
      yield put(getAdminRequestSuccess(transactions));
    } else {
      ReactGA.event({
        category: 'Dashboard',
        action: `Failed fetching the requests ${JSON.stringify(transactions)}`,
        label: 'Admin Record Failed',
      });
      yield put(getAdminRequestFailed(transactions.data));
    }
  } catch (error) {
    ReactGA.event({
      category: 'Dashboard',
      action: `Failed fetching the requests ${JSON.stringify(error)}`,
      label: 'Admin Record Failed',
    });
    yield put(getAdminRequestFailed(error));
  }
}

export function* getMemberdata() {
  const API_URI = APP.API_URL;
  ReactGA.initialize(GA.TRACKING_ID);
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    ReactGA.event({
      category: 'Dashboard',
      action: `Fetching transaction requests`,
      label: 'Member Record',
    });
    const transactions = yield call(
      request,
      `${API_URI}/request/`,
      optionsFormData('GET', null, token),
    );
    if (transactions.results) {
      ReactGA.event({
        category: 'Dashboard',
        action: `Successfully fetched the requests`,
        label: 'Member Record Success',
      });
      console.log('transactions', transactions);
      yield put(getAdminMemberSuccess(transactions));
    } else {
      ReactGA.event({
        category: 'Dashboard',
        action: `Failed fetching the requests ${JSON.stringify(transactions)}`,
        label: 'Member Record Failed',
      });
      yield put(getAdminMemberFailed(transactions.data));
    }
  } catch (error) {
    ReactGA.event({
      category: 'Dashboard',
      action: `Failed fetching the requests ${JSON.stringify(error)}`,
      label: 'Member Record Failed',
    });
    yield put(getAdminMemberFailed(error));
  }
}

export function* processRequest(data) {
  const API_URI = APP.API_URL;
  ReactGA.initialize(GA.TRACKING_ID);
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const requestData = data.data;
    const requestId = requestData.id;
    let requestStatus = 'PROCESSING';
    if (requestData.status === 'REQUESTED') {
      requestStatus = 'PROCESSING';
    } else if (requestData.status === 'PROCESSING') {
      requestStatus = 'SCHEDULED';
    } else if (requestData.status === 'SCHEDULED') {
      requestStatus = 'COMPLETED';
    }
    ReactGA.event({
      category: 'Dashboard',
      action: `Processing Request to ${requestStatus} in ${requestData.id}`,
      label: 'Admin Record',
    });
    const formData = new FormData();
    formData.append('status', requestStatus);
    formData.append('to', 'Anywhere');
    formData.append('amount', data.data.amount);
    formData.append('pay_at', data.data.pay_at);
    formData.append('pickup_at', data.data.pickup_at);
    formData.append('due_at', data.data.due_at);
    formData.append('remarks', data.data.remarks);
    formData.append('fee', data.data.fee);
    formData.append('transaction_type', data.data.transaction_type);
    formData.append('type', data.data.type);
    const endPoint = `${API_URI}/request/${requestId}/`;
    const processingRequest = yield call(
      request,
      endPoint,
      optionsFormData('PUT', formData, token),
    );
    if (data.data.type === 'PAYMENT' && requestStatus === 'PROCESSING') {
      // check if payee is accepts the T&A
      const { payee, user } = data.data;
      if (requestData.payee_agreement === null) {
        const payeeAgreementForm = new FormData();
        payeeAgreementForm.append('payee_id', payee.id);
        payeeAgreementForm.append('user_id', user.id);
        payeeAgreementForm.append('remarks', 'for review and sign');
        payeeAgreementForm.append('is_accepted', false);
        yield call(
          request,
          `${API_URI}/payee_agreement/`,
          optionsFormData('POST', payeeAgreementForm, null),
        );

        const payeeNotif = new FormData();
        payeeNotif.append('email', payee.email);
        payeeNotif.append('payee_id', payee.id);
        payeeNotif.append('user_id', user.id);
        payeeNotif.append('member_name', `${user.business_name}`);
        payeeNotif.append('payee_name', payee.name);
        yield call(
          request,
          `${API_URI}/payee_notification/`,
          optionsFormData('POST', payeeNotif, token),
        );
      }
    }
    if (processingRequest.success) {
      yield put(
        processRequestSuccess(processRequestSuccess(processingRequest)),
      );
      ReactGA.event({
        category: 'Dashboard',
        action: `Successful Processing Request to ${requestStatus} in ${requestId}`,
        label: 'Admin Record Success',
      });
      yield put(getAdminRequest());
    } else {
      yield put(processRequestSuccess(processRequestFailed(processingRequest)));
      ReactGA.event({
        category: 'Dashboard',
        action: `Failed Processing Request ${JSON.stringify(
          processingRequest,
        )}`,
        label: 'Admin Record Failed',
      });
    }
  } catch (error) {
    ReactGA.event({
      category: 'Dashboard',
      action: `Failed Processing Request ${JSON.stringify(error)}`,
      label: 'Admin Record Failed',
    });
    yield put(processRequestSuccess(processRequestFailed(error)));
  }
}

export function* viewRequest(requestData) {
  yield put(push(`/request/${requestData.data}`));
}

export function* pageRequest(pageData) {
  const API_URI = APP.API_URL;
  ReactGA.initialize(GA.TRACKING_ID);
  const {
    limit,
    offset,
    search,
    ordering,
    searchStatus,
    filterParam,
  } = pageData.data;
  const {
    filterType,
    filterStatus,
    // filterDeliveryMethod,
    filterDate,
    filterDateFrom,
    filterDateTo,
    filterPayee,
    isFilterRan,
    filterPeriod,
    // filterCategory,
  } = filterParam.stateData;
  let queryString = '';
  let separator = queryString === '' ? '?' : '&';
  queryString = limit
    ? `${queryString}${separator}limit=${limit}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = offset
    ? `${queryString}${separator}offset=${offset}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = search
    ? `${queryString}${separator}search=${search}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = searchStatus
    ? `${queryString}${separator}search=${searchStatus}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = ordering
    ? `${queryString}${separator}ordering=${ordering}`
    : queryString;
  if (isFilterRan) {
    queryString =
      filterType && filterType !== 'ALL'
        ? `${queryString}${separator}type=${filterType}`
        : queryString;
    separator = queryString === '' ? '?' : '&';
    queryString =
      filterStatus && filterStatus !== 'ALL'
        ? `${queryString}${separator}status=${filterStatus}`
        : queryString;
    // separator = queryString === '' ? '?' : '&';
    // queryString = filterDeliveryMethod
    //   ? `${queryString}${separator}delivery_method=${filterDeliveryMethod}`
    //   : queryString;
    separator = queryString === '' ? '?' : '&';
    queryString = filterDate
      ? `${queryString}${separator}date=${filterDate}`
      : queryString;
    separator = queryString === '' ? '?' : '&';
    queryString = filterDateFrom
      ? `${queryString}${separator}from=${filterDateFrom}`
      : queryString;
    separator = queryString === '' ? '?' : '&';
    queryString = filterDateTo
      ? `${queryString}${separator}to=${filterDateTo}`
      : queryString;
    separator = queryString === '' ? '?' : '&';
    queryString =
      filterPayee && filterPayee !== ''
        ? `${queryString}${separator}payee_id=${filterPayee}`
        : queryString;
    queryString =
      filterPeriod && filterPeriod !== ''
        ? `${queryString}${separator}period=${filterPeriod}`
        : queryString;
  }
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const transactions = yield call(
      request,
      `${API_URI}/admin/request/${queryString}`,
      optionsFormData('GET', null, token),
    );
    ReactGA.event({
      category: 'Dashboard',
      action: `Running Filter and Sort`,
      label: 'Admin Record',
    });
    if (transactions.results) {
      yield put(pageRequestSuccess(transactions));
      ReactGA.event({
        category: 'Dashboard',
        action: `Successfully Running Filter and Sort`,
        label: 'Admin Record Success',
      });
    } else {
      ReactGA.event({
        category: 'Dashboard',
        action: `Failed Running Filter and Sort ${JSON.stringify(
          transactions,
        )}`,
        label: 'Admin Record Failed',
      });
      yield put(pageRequestFailed(transactions));
    }
  } catch (error) {
    ReactGA.event({
      category: 'Dashboard',
      action: `Failed Running Filter and Sort ${JSON.stringify(error)}`,
      label: 'Admin Record Failed',
    });
    yield put(pageRequestFailed(error));
  }
}

export function* memberPageRequest(pageData) {
  const API_URI = APP.API_URL;
  const {
    limit,
    offset,
    search,
    ordering,
    searchStatus,
    filterParam,
  } = pageData.data;
  let queryString = '';
  let separator = queryString === '' ? '?' : '&';
  queryString = limit
    ? `${queryString}${separator}limit=${limit}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = offset
    ? `${queryString}${separator}offset=${offset}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = search
    ? `${queryString}${separator}search=${search}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = searchStatus
    ? `${queryString}${separator}search=${searchStatus}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = ordering
    ? `${queryString}${separator}ordering=${ordering}`
    : queryString;
  const {
    filterType,
    filterStatus,
    // filterDeliveryMethod,
    filterDate,
    filterDateFrom,
    filterDateTo,
    filterPayee,
    isFilterRan,
    filterPeriod,
    // filterCategory,
  } = filterParam.stateData;
  if (isFilterRan) {
    queryString =
      filterType && filterType !== 'ALL'
        ? `${queryString}${separator}type=${filterType}`
        : queryString;
    separator = queryString === '' ? '?' : '&';
    queryString =
      filterStatus && filterStatus !== 'ALL'
        ? `${queryString}${separator}status=${filterStatus}`
        : queryString;
    // separator = queryString === '' ? '?' : '&';
    // queryString = filterDeliveryMethod
    //   ? `${queryString}${separator}delivery_method=${filterDeliveryMethod}`
    //   : queryString;
    separator = queryString === '' ? '?' : '&';
    queryString = filterDate
      ? `${queryString}${separator}date=${filterDate}`
      : queryString;
    separator = queryString === '' ? '?' : '&';
    queryString = filterDateFrom
      ? `${queryString}${separator}from=${filterDateFrom}`
      : queryString;
    separator = queryString === '' ? '?' : '&';
    queryString = filterDateTo
      ? `${queryString}${separator}to=${filterDateTo}`
      : queryString;
    separator = queryString === '' ? '?' : '&';
    queryString =
      filterPayee && filterPayee !== ''
        ? `${queryString}${separator}payee_id=${filterPayee}`
        : queryString;
    queryString =
      filterPeriod && filterPeriod !== ''
        ? `${queryString}${separator}period=${filterPeriod}`
        : queryString;
  }
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const transactions = yield call(
      request,
      `${API_URI}/request/${queryString}`,
      optionsFormData('GET', null, token),
    );
    if (transactions.results) {
      yield put(memberPageRequestSuccess(transactions));
    } else {
      yield put(memberPageRequestFailed(transactions));
    }
  } catch (error) {
    yield put(memberPageRequestFailed(error));
  }
}

export function* updateCredentials(credential) {
  try {
    const API_URI = APP.API_URL;
    const { user, userInput } = credential.data;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userRequest = yield call(
      request,
      `${API_URI}/user/${user.id}/`,
      optionsFormData('GET', null, token),
    );

    if (userRequest.id) {
      const formData = new FormData();
      formData.append('email', userRequest.email);
      formData.append('username', userRequest.username);
      formData.append('password', userInput.password);
      formData.append('first_name', userRequest.first_name);
      formData.append('last_name', userRequest.last_name);
      formData.append('primary_contact_name', userRequest.primary_contact_name);
      formData.append('is_first_time_member', userRequest.is_first_time_member);
      formData.append(
        'primary_contact_number',
        userRequest.primary_contact_number,
      );
      formData.append('business_name', userRequest.business_name);
      formData.append(
        'principal_city_of_business',
        userRequest.principal_city_of_business,
      );
      formData.append('user_type', userRequest.user_type);
      const userUpdateRequest = yield call(
        request,
        `${API_URI}/profile/user/${user.id}/`,
        optionsFormData('PUT', formData, token),
      );
      if (userUpdateRequest.id) {
        yield put(updateCreadentialsSuccess(userUpdateRequest));
        yield localStorage.setItem(
          'userData',
          JSON.stringify(userUpdateRequest),
        );
        yield put(push('/temp/user'));
      } else {
        yield put(updateCredentialsError(userUpdateRequest.message));
      }
    }
  } catch (error) {
    yield put(updateCredentialsError(error));
  }
}

export function* updateFirstTimeMember(credential) {
  try {
    const API_URI = APP.API_URL;
    const { user, userInput } = credential.data;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userRequest = yield call(
      request,
      `${API_URI}/user/${user.id}/`,
      optionsFormData('GET', null, token),
    );

    if (userRequest.id) {
      const formData = new FormData();
      formData.append('email', userRequest.email);
      formData.append('username', userInput.userName);
      formData.append('password', userInput.password);
      formData.append('first_name', userRequest.first_name);
      formData.append('last_name', userRequest.last_name);
      formData.append('primary_contact_name', userRequest.primary_contact_name);
      formData.append(
        'is_first_time_member',
        !userRequest.is_first_time_member,
      );
      formData.append(
        'primary_contact_number',
        userRequest.primary_contact_number,
      );
      formData.append('business_name', userRequest.business_name);
      formData.append(
        'principal_city_of_business',
        userRequest.principal_city_of_business,
      );
      formData.append('user_type', userRequest.user_type);
      const userUpdateRequest = yield call(
        request,
        `${API_URI}/profile/user/${user.id}/`,
        optionsFormData('PUT', formData, token),
      );
      if (userUpdateRequest.id) {
        if (userInput.profileImage) {
          const formDataFile = new FormData();
          formDataFile.append('filename', userInput.profileImage);
          formDataFile.append('url', 'filename');
          formDataFile.append('mime_type', userInput.profileImage.type);
          formDataFile.append('resource_id', userUpdateRequest.id);
          formDataFile.append('type', 'USER');
          formDataFile.append(
            'expire_at',
            moment()
              .add(1, 'hours')
              .format('YYYY-MM-DD h:m:s'),
          );
          ReactGA.event({
            category: 'User',
            action: `Update user credentials`,
            label: 'Update user credentials',
          });
          yield call(
            request,
            `${API_URI}/user_file/`,
            optionsFormData('POST', formDataFile, token),
          );
          yield put(getCurrentUser());
        }
        yield put(updateFirstTimeMemberSuccess(userUpdateRequest));
      } else {
        yield put(updateFirstTimeMemberError(userUpdateRequest.message));
      }
    }
  } catch (error) {
    yield put(updateFirstTimeMemberError(error));
  }
}

export function* filterRequest(pageData) {
  const API_URI = APP.API_URL;
  const {
    filterType,
    filterStatus,
    // filterDeliveryMethod,
    filterDate,
    filterDateFrom,
    filterDateTo,
    filterPayee,
    filterPeriod,
    filterUser,
    // filterCategory,
  } = pageData.data;
  let queryString = '';
  let separator = queryString === '' ? '?' : '&';
  queryString =
    filterType && filterType !== 'ALL'
      ? `${queryString}${separator}type=${filterType}`
      : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString =
    filterStatus && filterStatus !== 'ALL'
      ? `${queryString}${separator}status=${filterStatus}`
      : queryString;
  // separator = queryString === '' ? '?' : '&';
  // queryString = filterDeliveryMethod
  //   ? `${queryString}${separator}delivery_method=${filterDeliveryMethod}`
  //   : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = filterDate
    ? `${queryString}${separator}date=${filterDate}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = filterDateFrom
    ? `${queryString}${separator}from=${filterDateFrom}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = filterDateTo
    ? `${queryString}${separator}to=${filterDateTo}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString =
    filterPayee && filterPayee !== ''
      ? `${queryString}${separator}payee_id=${filterPayee}`
      : queryString;
  queryString =
    filterPeriod && filterPeriod !== ''
      ? `${queryString}${separator}period=${filterPeriod}`
      : queryString;
  queryString =
    filterUser && filterUser !== ''
      ? `${queryString}${separator}user_id=${filterUser}`
      : queryString;
  // separator = queryString === '' ? '?' : '&';
  // queryString = filterCategory
  //   ? `${queryString}${separator}category=${filterCategory}`
  //   : queryString;
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const transactions = yield call(
      request,
      `${API_URI}/admin/request/${queryString}`,
      optionsFormData('GET', null, token),
    );
    if (transactions.results) {
      yield put(filterRequestSucess(transactions));
    } else {
      yield put(filterRequestFailed(transactions));
    }
  } catch (error) {
    yield put(filterRequestFailed(error));
  }
}

export function* filterMemberRequest(pageData) {
  const API_URI = APP.API_URL;
  const {
    filterType,
    filterStatus,
    // filterDeliveryMethod,
    filterDate,
    filterDateFrom,
    filterDateTo,
    filterPayee,
    filterPeriod,
    // filterCategory,
  } = pageData.data;
  let queryString = '';
  let separator = queryString === '' ? '?' : '&';
  queryString =
    filterType && filterType !== 'ALL'
      ? `${queryString}${separator}type=${filterType}`
      : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString =
    filterStatus && filterStatus !== 'ALL'
      ? `${queryString}${separator}status=${filterStatus}`
      : queryString;
  // separator = queryString === '' ? '?' : '&';
  // queryString = filterDeliveryMethod
  //   ? `${queryString}${separator}delivery_method=${filterDeliveryMethod}`
  //   : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = filterDate
    ? `${queryString}${separator}date=${filterDate}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = filterDateFrom
    ? `${queryString}${separator}from=${filterDateFrom}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = filterDateTo
    ? `${queryString}${separator}to=${filterDateTo}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString =
    filterPayee && filterPayee !== ''
      ? `${queryString}${separator}payee_id=${filterPayee}`
      : queryString;
  queryString =
    filterPeriod && filterPeriod !== ''
      ? `${queryString}${separator}period=${filterPeriod}`
      : queryString;
  // separator = queryString === '' ? '?' : '&';
  // queryString = filterCategory
  //   ? `${queryString}${separator}category=${filterCategory}`
  //   : queryString;
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const transactions = yield call(
      request,
      `${API_URI}/request/${queryString}`,
      optionsFormData('GET', null, token),
    );
    if (transactions.results) {
      yield put(filterMemberRequestSucess(transactions));
    } else {
      yield put(filterMemberRequestFailed(transactions));
    }
  } catch (error) {
    yield put(filterMemberRequestFailed(error));
  }
}

export function* getPayee() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const payee = yield call(
      request,
      `${API_URI}/payee/?page_size=1000`,
      optionsFormData('GET', null, token),
    );
    /* eslint-disable */
    if (payee.results) {
      const { results } = payee;
      let dropdownData = [];
      for (const i in results) {
        const resultsData = results[i];
        const dataResult = {
          key: i,
          text: resultsData.name,
          value: resultsData.id,
        };
        dropdownData.push(dataResult);
      }
      yield put(getPayeeSuccess(dropdownData));
      /* eslint-enable */
    } else {
      yield put(getPayeeFailed(payee));
    }
  } catch (error) {
    yield put(getPayeeFailed(error));
  }
}

export function* proceedToDashboard(userData) {
  const { data } = userData;
  const token = yield JSON.parse(localStorage.getItem('token'));
  yield localStorage.setItem('userData', JSON.stringify(data));
  yield put(getMemberRequest(data.id));
  yield put(getCurrentUser());
  yield put(
    loginSuccessAction({
      token,
      userData: data,
      redirect: '/',
    }),
  );
}

export function* getUserFeeRate(user) {
  try {
    const API_URI = APP.API_URL;
    const { data } = user;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const feeRate = yield call(
      request,
      `${API_URI}/fee/?user_id=${data}`,
      optionsFormData('GET', null, token),
    );
    if (feeRate.results.length > 0) {
      yield put(getUserFeeRateSuccess(feeRate.results[0]));
    } else {
      yield put(getUserFeeRateError(feeRate));
    }
  } catch (error) {
    yield put(getUserFeeRateError(error));
  }
}

export function* getUserFeeTierRate(user) {
  try {
    const API_URI = APP.API_URL;
    const { data } = user;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const feeRate = yield call(
      request,
      `${API_URI}/fee_tier/?user_id=${data}`,
      optionsFormData('GET', null, token),
    );
    if (feeRate.results.length > 0) {
      yield put(getUserFeeTierRateSuccess(feeRate.results));
    } else {
      yield put(getUserFeeTierRateError(feeRate));
    }
  } catch (error) {
    yield put(getUserFeeTierRateError(error));
  }
}

export function* getWallet() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const walletResponse = yield call(
      request,
      `${API_URI}/wallet/`,
      optionsFormData('GET', null, token),
    );
    if (walletResponse.success) {
      yield put(getWalletSuccess(walletResponse.data));
    } else {
      yield put(getWalletError(walletResponse.message));
    }
  } catch (error) {
    yield put(getWalletError(error));
  }
}

export function* getTopRequest() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const topRequest = yield call(
      request,
      `${API_URI}/top_request/`,
      optionsFormData('GET', null, token),
    );
    if (topRequest.results) {
      yield put(getTopRequestSuccess(topRequest.results));
    } else {
      yield put(getTopRequestError(topRequest));
    }
  } catch (error) {
    yield put(getTopRequestError(error));
  }
}

export function* getMemberUsers() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userMembers = yield call(
      request,
      `${API_URI}/user/?content=member`,
      optionsFormData('GET', null, token),
    );
    if (userMembers.results) {
      yield put(getMemberUsersSuccess(userMembers));
    } else {
      yield put(getMemberUsersError(userMembers));
    }
  } catch (error) {
    yield put(getMemberUsersError(error));
  }
}

export function* getNextMemberPage(nextPage) {
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userMembers = yield call(
      request,
      `${nextPage.data}`,
      optionsFormData('GET', null, token),
    );
    if (userMembers.results) {
      yield put(getMemberUsersSuccess(userMembers));
    } else {
      yield put(getMemberUsersError(userMembers));
    }
  } catch (error) {
    yield put(getMemberUsersError(error));
  }
}

export function* exportCSV(filterData) {
  const API_URI = APP.API_URL;
  ReactGA.initialize(GA.TRACKING_ID);
  const {
    filterType,
    filterStatus,
    filterDate,
    filterDateFrom,
    filterDateTo,
    filterPayee,
    filterPeriod,
    filterUser,
    currentUser,
  } = filterData.data;
  let queryString = '';
  let separator = queryString === '' ? '?' : '&';
  queryString =
    filterType && filterType !== 'ALL'
      ? `${queryString}${separator}type=${filterType}`
      : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString =
    filterStatus && filterStatus !== 'ALL'
      ? `${queryString}${separator}status=${filterStatus}`
      : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = filterDate
    ? `${queryString}${separator}date=${filterDate}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = filterDateFrom
    ? `${queryString}${separator}from=${filterDateFrom}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = filterDateTo
    ? `${queryString}${separator}to=${filterDateTo}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString =
    filterPayee && filterPayee !== ''
      ? `${queryString}${separator}payee_id=${filterPayee}`
      : queryString;
  queryString =
    filterPeriod && filterPeriod !== ''
      ? `${queryString}${separator}period=${filterPeriod}`
      : queryString;
  queryString = `${queryString}${separator}user_report=${
    currentUser.user_type
  }`;
  if (currentUser.user_type === 'member') {
    queryString = `${queryString}${separator}user_id=${currentUser.id}`;
  } else if (currentUser.user_type === 'sub_member') {
    queryString = `${queryString}${separator}user_id=${currentUser.id}`;
  } else {
    queryString =
      filterUser && filterUser !== ''
        ? `${queryString}${separator}user_id=${filterUser}`
        : queryString;
  }

  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const options = {
      headers: {
        Accept: 'application/json',
        Authorization: `JWT ${token}`,
      },
      method: 'GET',
    };
    ReactGA.event({
      category: 'Dashboard',
      action: `Exporting Request`,
      label: 'Exporting Request',
    });
    const result = yield call(
      getBlob,
      `${API_URI}/csv_request/${queryString}`,
      options,
    );
    if (!result.errors) {
      download(result, `report${moment().format('YYYY_MM-DD')}.csv`);
      yield put(exportCSVSuccess('success'));
    }
  } catch (error) {
    console.log(error);
    ReactGA.event({
      category: 'Dashboard',
      action: `Failed Running Filter and Sort ${JSON.stringify(error)}`,
      label: 'Admin Record Failed',
    });
    yield put(exportCSVError(error));
  }
}

export function* runMemberBalances() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const requestData = yield call(
      request,
      `${API_URI}/member_wallet/`,
      optionsFormData('GET', null, token),
    );
    if (requestData.results) {
      yield put(memberBalanceSuccess(requestData));
    } else {
      yield put(memberBalanceError(requestData));
    }
  } catch (error) {
    yield put(memberBalanceError(error));
  }
}

export function* memberBalancesPageAction(pageData) {
  const API_URI = APP.API_URL;
  const { limit, offset, search, ordering, searchStatus } = pageData.data;
  let queryString = '';
  let separator = queryString === '' ? '?' : '&';
  queryString = limit
    ? `${queryString}${separator}limit=${limit}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = offset
    ? `${queryString}${separator}offset=${offset}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = search
    ? `${queryString}${separator}search=${search}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = searchStatus
    ? `${queryString}${separator}search=${searchStatus}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = ordering
    ? `${queryString}${separator}ordering=${ordering}`
    : queryString;
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const transactions = yield call(
      request,
      `${API_URI}/member_wallet/${queryString}`,
      optionsFormData('GET', null, token),
    );
    if (transactions.results) {
      ReactGA.event({
        category: 'Payee',
        action: `Payee Page Action (pagination, sort and filter) success`,
        label: 'Add Payee Page Action Success',
      });
      yield put(memberBalancesPageSuccess(transactions));
    } else {
      yield put(memberBalancesPageError(transactions));
    }
  } catch (error) {
    yield put(memberBalancesPageError(error));
  }
}

export function* adminWatcher() {
  yield takeLatest(GET_ADMIN_REQUEST, getAdminData);
}

export function* memberWatcher() {
  yield takeLatest(GET_MEMBER_REQUEST, getMemberdata);
}

export function* processWatcher() {
  yield takeLatest(PROCESS_REQUEST, processRequest);
}

export function* viewRequestWatcher() {
  yield takeLatest(VIEW_REQUEST, viewRequest);
}

export function* pageRequestWatcher() {
  yield takeLatest(PAGE_REQUEST, pageRequest);
}

export function* memberPageRequestWatcher() {
  yield takeLatest(MEMBER_PAGE_REQUEST, memberPageRequest);
}

export function* updateCredentialsWatcher() {
  yield takeLatest(UPDATE_CREDENTIALS, updateCredentials);
}

export function* filterRequestWatcher() {
  yield takeLatest(FILTER_REQUEST, filterRequest);
}

export function* getPayeeWatcher() {
  yield takeLatest(GET_PAYEE, getPayee);
}

export function* updateFirstTimeMemberWatcher() {
  yield takeLatest(UPDATE_FIRST_TIME_MEMBER, updateFirstTimeMember);
}

export function* proceedToDashboardWatcher() {
  yield takeLatest(PROCEED_TO_DASHBOARD, proceedToDashboard);
}

export function* getUserFeeRateWatcher() {
  yield takeLatest(GET_USER_FEE_RATE, getUserFeeRate);
}

export function* getUserFeeTierRateWatcher() {
  yield takeLatest(GET_USER_FEE_TIER_RATE, getUserFeeTierRate);
}

export function* getWalletWatcher() {
  yield takeLatest(GET_WALLET, getWallet);
}

export function* getTOpRequestWatcher() {
  yield takeLatest(GET_TOP_REQUEST, getTopRequest);
}

export function* getMemberUsersWatcher() {
  yield takeLatest(GET_MEMBER_USERS, getMemberUsers);
}

export function* getMemberNextPageWatcher() {
  yield takeLatest(GET_NEXT_MEMBER_PAGE, getNextMemberPage);
}

export function* filterMemberRequestWatcher() {
  yield takeLatest(FILTER_MEMBER_REQUEST, filterMemberRequest);
}

export function* exportCSVWatcher() {
  yield takeLatest(EXPORT_CSV, exportCSV);
}

export function* memberBalanceWatcher() {
  yield takeLatest(MEMBER_BALANCE, runMemberBalances);
}

export function* memberBalancePageWatcher() {
  yield takeLatest(MEMBER_BALANCE_PAGE, memberBalancesPageAction);
}

export default function* rootSaga() {
  yield all([
    adminWatcher(),
    memberWatcher(),
    processWatcher(),
    viewRequestWatcher(),
    pageRequestWatcher(),
    memberPageRequestWatcher(),
    updateCredentialsWatcher(),
    filterRequestWatcher(),
    getPayeeWatcher(),
    updateFirstTimeMemberWatcher(),
    proceedToDashboardWatcher(),
    getUserFeeRateWatcher(),
    getUserFeeTierRateWatcher(),
    getWalletWatcher(),
    getTOpRequestWatcher(),
    getMemberUsersWatcher(),
    getMemberNextPageWatcher(),
    filterMemberRequestWatcher(),
    exportCSVWatcher(),
    memberBalanceWatcher(),
    memberBalancePageWatcher(),
  ]);
}
