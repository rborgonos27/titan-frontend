import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the dashBoard state domain
 */
const selectGlobal = state => state.get('global');
const selectDashBoardDomain = state => state.get('dashBoard', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by DashBoard
 */

const makeSelectDashBoard = () =>
  createSelector(selectDashBoardDomain, substate => substate.toJS());

const makeSelectCurrentUser = () =>
  createSelector(selectGlobal, globalState => globalState.get('userData'));

export default makeSelectDashBoard;
export { selectDashBoardDomain, makeSelectCurrentUser };
