/**
 *
 * AccountPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import moment from 'moment';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import {
  Image,
  Table,
  Header as SHeader,
  Loader,
  Button,
  Divider,
} from 'semantic-ui-react';

import pdf from 'images/pdf.png';
import {
  getTransactionHistory,
  getAccountSummary,
  getStatement,
} from './actions';
import makeSelectAccountPage, { makeSelectCurrentUser } from './selectors';
import reducer from './reducer';
import saga from './saga';

/* eslint-disable react/prefer-stateless-function */
export class AccountPage extends React.Component {
  componentWillMount() {
    const { currentUser } = this.props;
    this.props.getStatement(currentUser);
  }
  state = {
    page: 'details',
    pdfFile: null,
  };
  setPdfFile = link => this.setState({ pdfFile: link, page: 'view' });
  getDetailView = () => {
    const { statements } = this.props.accountpage;
    if (!statements) return null;
    const { results } = statements;
    const records = [];
    /* eslint-disable */
    for (let i = 0; i < results.length; i++) {
      const statement = results[i];
      const cutoff = moment(statement.cutoff_at).format("MMM, YYYY");
      console.log('cutoff', cutoff);
      const row = (
        <Table.Row key={i}>
          <Table.Cell>
            <SHeader as="h4" image>
              <Image src={pdf} rounded size="mini" />
              <SHeader.Content>
                <a onClick={() => this.setPdfFile(statement.statement_file)}>
                  {cutoff}
                </a>
                <SHeader.Subheader>Report</SHeader.Subheader>
              </SHeader.Content>
            </SHeader>
          </Table.Cell>
        </Table.Row>
      );
      records.push(row);
    }
    /* eslint-enable */
    return (
      <Table basic="very" celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell />
          </Table.Row>
        </Table.Header>
        <Table.Body>{records}</Table.Body>
      </Table>
    );
  };
  getFileView = () => {
    const { pdfFile } = this.state;
    return (
      <div>
        <Button positive onClick={() => this.setState({ page: 'details' })}>
          Back
        </Button>
        <Divider />
        <embed
          src={`${pdfFile}#navpanes=0`}
          width="100%"
          height="900"
          type="application/pdf"
        />
      </div>
    );
  };
  manageView = () => {
    const { page } = this.state;
    switch (page) {
      case 'details':
        return this.getDetailView();
      case 'view':
        return this.getFileView();
      default:
        return this.getDetailView();
    }
  };
  getContent = () => (
    <CardBox>
      <h3>Member Statement</h3>
      {this.manageView()}
    </CardBox>
  );
  render() {
    const { loading } = this.props.accountpage;
    const content = loading ? (
      <Loader active size="massive" />
    ) : (
      this.getContent()
    );
    return <ContainerWrapper>{content}</ContainerWrapper>;
  }
}

AccountPage.propTypes = {
  getStatement: PropTypes.func.isRequired,
  accountpage: PropTypes.any,
  currentUser: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  accountpage: makeSelectAccountPage(),
  currentUser: makeSelectCurrentUser(),
});

const mapDispatchToProps = {
  getTransactionHistory,
  getAccountSummary,
  getStatement,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'accountPage', reducer });
const withSaga = injectSaga({ key: 'accountPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AccountPage);
