/*
 *
 * AccountPage actions
 *
 */

import {
  GET_TRANSACTION_HISTORY,
  GET_TRANSACTION_HISTORY_SUCCESS,
  GET_TRANSACTION_HISTORY_ERROR,
  GET_ACCOUNT_SUMMARY,
  GET_ACCOUNT_SUMMARY_ERROR,
  GET_ACCOUNT_SUMMARY_SUCCESS,
  GET_STATEMENT,
  GET_STATEMENT_SUCCESS,
  GET_STATEMENT_ERROR,
} from './constants';

export function getTransactionHistory() {
  return {
    type: GET_TRANSACTION_HISTORY,
  };
}

export function getTransactionHistorySuccess(data) {
  return {
    type: GET_TRANSACTION_HISTORY_SUCCESS,
    data,
  };
}

export function getTransactionHistoryError(error) {
  return {
    type: GET_TRANSACTION_HISTORY_ERROR,
    error,
  };
}

export function getAccountSummary() {
  return {
    type: GET_ACCOUNT_SUMMARY,
  };
}

export function getAccountSummarySuccess(data) {
  return {
    type: GET_ACCOUNT_SUMMARY_SUCCESS,
    data,
  };
}

export function getAccountSummaryError(error) {
  return {
    type: GET_ACCOUNT_SUMMARY_ERROR,
    error,
  };
}

export function getStatement(data) {
  return {
    type: GET_STATEMENT,
    data,
  };
}

export function getStatementSuccess(data) {
  return {
    type: GET_STATEMENT_SUCCESS,
    data,
  };
}

export function getStatementError(error) {
  return {
    type: GET_STATEMENT_ERROR,
    error,
  };
}
