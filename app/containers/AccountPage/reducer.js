/*
 *
 * AccountPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  GET_TRANSACTION_HISTORY,
  GET_TRANSACTION_HISTORY_SUCCESS,
  GET_TRANSACTION_HISTORY_ERROR,
  GET_ACCOUNT_SUMMARY,
  GET_ACCOUNT_SUMMARY_ERROR,
  GET_ACCOUNT_SUMMARY_SUCCESS,
  GET_STATEMENT,
  GET_STATEMENT_ERROR,
  GET_STATEMENT_SUCCESS,
} from './constants';

export const initialState = fromJS({
  loading: false,
  error: null,
  errorList: [],
  transactionData: [],
  accountSummary: null,
  statements: null,
});

function accountPageReducer(state = initialState, action) {
  switch (action.type) {
    case GET_TRANSACTION_HISTORY:
      return state
        .set('loading', true)
        .set('error', null)
        .set('transactionData', []);
    case GET_TRANSACTION_HISTORY_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('transactionData', action.data);
    case GET_TRANSACTION_HISTORY_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('transactionData', []);
    case GET_ACCOUNT_SUMMARY:
      return state
        .set('loading', true)
        .set('error', null)
        .set('accountSummary', null);
    case GET_ACCOUNT_SUMMARY_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('accountSummary', null);
    case GET_ACCOUNT_SUMMARY_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('accountSummary', action.data);
    case GET_STATEMENT:
      return state
        .set('loading', true)
        .set('error', null)
        .set('statements', null);
    case GET_STATEMENT_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('statements', action.data);
    case GET_STATEMENT_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('statements', null);
    default:
      return state;
  }
}

export default accountPageReducer;
