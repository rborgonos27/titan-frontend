/*
 *
 * AccountPage constants
 *
 */

export const DEFAULT_ACTION = 'app/AccountPage/DEFAULT_ACTION';

export const GET_TRANSACTION_HISTORY =
  'app/AccountPage/GET_TRANSACTION_HISTORY';
export const GET_TRANSACTION_HISTORY_SUCCESS =
  'app/AccountPage/GET_TRANSACTION_HISTORY_SUCCESS';
export const GET_TRANSACTION_HISTORY_ERROR =
  'app/AccountPage/GET_TRANSACTION_HISTORY_ERROR';

export const GET_ACCOUNT_SUMMARY = 'app/AccountPage/GET_ACCOUNT_SUMMARY';
export const GET_ACCOUNT_SUMMARY_SUCCESS =
  'app/AccountPage/GET_ACCOUNT_SUMMARY_SUCCESS';
export const GET_ACCOUNT_SUMMARY_ERROR =
  'app/AccountPage/GET_ACCOUNT_SUMMARY_ERROR';

export const GET_STATEMENT = 'app/AccountPage/GET_STATEMENT';
export const GET_STATEMENT_SUCCESS = 'app/AccountPage/GET_STATEMENT_SUCCESS';
export const GET_STATEMENT_ERROR = 'app/AccountPage/GET_STATEMENT_ERROR';
