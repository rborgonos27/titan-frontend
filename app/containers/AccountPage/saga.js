import { all, takeLatest, call, put } from 'redux-saga/effects';
import request from 'utils/request';
import { APP } from 'config';
import moment from 'moment';
import {
  GET_TRANSACTION_HISTORY,
  GET_ACCOUNT_SUMMARY,
  GET_STATEMENT,
} from './constants';
import {
  getTransactionHistorySuccess,
  getTransactionHistoryError,
  getAccountSummaryError,
  getAccountSummarySuccess,
  getStatementError,
  getStatementSuccess,
} from './actions';
import { optionsFormData } from '../../utils/options';

export function* getTransactionHistory() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const transactionRequest = yield call(
      request,
      `${API_URI}/request_history/`,
      optionsFormData('GET', null, token),
    );
    if (transactionRequest) {
      yield put(getTransactionHistorySuccess(transactionRequest));
    } else {
      yield put(getTransactionHistoryError(transactionRequest));
    }
  } catch (error) {
    yield put(getTransactionHistoryError(error));
  }
}

export function* getAccountSummary() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const accountSummary = yield call(
      request,
      `${API_URI}/account_summary/`,
      optionsFormData('GET', null, token),
    );
    if (accountSummary) {
      yield put(getAccountSummarySuccess(accountSummary));
    } else {
      yield put(getAccountSummaryError(accountSummary));
    }
  } catch (error) {
    yield put(getAccountSummaryError(error));
  }
}

export function* getStatement(statementData) {
  try {
    const API_URI = APP.API_URL;
    const DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';
    const token = yield JSON.parse(localStorage.getItem('token'));
    console.log(statementData.data);
    const currentUser = yield call(
      request,
      `${API_URI}/current_user/`,
      optionsFormData('GET', null, token),
    );
    const userDateJoined = currentUser.date_joined;
    const { data } = statementData;
    const dateJoined = moment(userDateJoined);
    // console.log(dateJoined, dateJoined.format('D'), moment().format('D'),  dateJoined.format('M'));
    /* eslint-disable */
    const cutOffMonth = parseInt(dateJoined.format('M')) === parseInt(moment().format('M')) ? parseInt(moment().format('M')) + 1 : parseInt(moment().format('M'));
    /* eslint-enable */
    const cuttOffObject = moment(`
      ${moment().format('YYYY')}-${cutOffMonth}-${dateJoined.format('D')}
    `);
    const cutOffDate = `${cuttOffObject.format(DATE_FORMAT)}`;
    console.log(cuttOffObject, cutOffDate);
    const formData = new FormData();
    formData.append('user_id', data.id);
    formData.append('cutoff_at', cutOffDate);
    yield call(
      request,
      `${API_URI}/generate_statement/`,
      optionsFormData('POST', formData, token),
    );
    const accountSummary = yield call(
      request,
      `${API_URI}/member_statement/?cut_off=${cutOffDate}`,
      optionsFormData('GET', null, token),
    );
    if (accountSummary) {
      yield put(getStatementSuccess(accountSummary));
    } else {
      yield put(getStatementError(accountSummary));
    }
  } catch (error) {
    yield put(getStatementError(error));
  }
}

export function* getTransactionHistoryWatcher() {
  yield takeLatest(GET_TRANSACTION_HISTORY, getTransactionHistory);
}

export function* getAccountSummaryWatcher() {
  yield takeLatest(GET_ACCOUNT_SUMMARY, getAccountSummary);
}

export function* getStatementWatcher() {
  yield takeLatest(GET_STATEMENT, getStatement);
}

export default function* rootSaga() {
  yield all([
    getTransactionHistoryWatcher(),
    getAccountSummaryWatcher(),
    getStatementWatcher(),
  ]);
}
