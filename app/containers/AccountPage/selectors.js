import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the accountPage state domain
 */

const selectAccountPageDomain = state => state.get('accountPage', initialState);
const selectGlobal = state => state.get('global');

/**
 * Other specific selectors
 */

/**
 * Default selector used by AccountPage
 */

const makeSelectAccountPage = () =>
  createSelector(selectAccountPageDomain, substate => substate.toJS());

const makeSelectCurrentUser = () => createSelector(
  selectGlobal,
  globalState => globalState.get('userData')
);

export default makeSelectAccountPage;
export { selectAccountPageDomain, makeSelectCurrentUser };
