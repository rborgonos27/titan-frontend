import { fromJS } from 'immutable';
import payeeTermsReducer from '../reducer';

describe('payeeTermsReducer', () => {
  it('returns the initial state', () => {
    expect(payeeTermsReducer(undefined, {})).toEqual(fromJS({}));
  });
});
