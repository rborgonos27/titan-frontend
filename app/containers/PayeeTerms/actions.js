/*
 *
 * PayeeTerms actions
 *
 */

import {
  UPLOAD_FILE,
  UPLOAD_FILE_SUCCESS,
  UPLOAD_FILE_ERROR,
  UPLOAD_SIGNATURE,
  UPLOAD_SIGNATURE_ERROR,
  UPLOAD_SIGNATURE_SUCCESS,
  VERIFY_TERMS,
  VERIFY_TERMS_ERROR,
  VERIFY_TERMS_SUCCESS,
} from './constants';

export function uploadFile(data) {
  return {
    type: UPLOAD_FILE,
    data,
  };
}

export function uploadFileSuccess(data) {
  return {
    type: UPLOAD_FILE_SUCCESS,
    data,
  };
}

export function uploadFileError(data) {
  return {
    type: UPLOAD_FILE_ERROR,
    data,
  };
}

export function uploadSignature(data) {
  return {
    type: UPLOAD_SIGNATURE,
    data,
  };
}

export function uploadSignatureSuccess(data) {
  return {
    type: UPLOAD_SIGNATURE_SUCCESS,
    data,
  };
}

export function uploadSignatureError(error) {
  return {
    type: UPLOAD_SIGNATURE_ERROR,
    error,
  };
}

export function verifyTerms(data) {
  return {
    type: VERIFY_TERMS,
    data,
  };
}

export function verifyTermsSuccess(data) {
  return {
    type: VERIFY_TERMS_SUCCESS,
    data,
  };
}

export function verifyTermsError(error) {
  return {
    type: VERIFY_TERMS_ERROR,
    error,
  };
}
