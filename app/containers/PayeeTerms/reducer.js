/*
 *
 * PayeeTerms reducer
 *
 */

import { fromJS } from 'immutable';
import {
  UPLOAD_FILE,
  UPLOAD_FILE_SUCCESS,
  UPLOAD_FILE_ERROR,
  UPLOAD_SIGNATURE,
  UPLOAD_SIGNATURE_SUCCESS,
  UPLOAD_SIGNATURE_ERROR,
  VERIFY_TERMS,
  VERIFY_TERMS_SUCCESS,
  VERIFY_TERMS_ERROR,
} from './constants';

export const initialState = fromJS({
  loading: false,
  data: null,
  error: null,
  payeeData: null,
  isDone: false,
  isAccepted: false,
});

function payeeTermsReducer(state = initialState, action) {
  switch (action.type) {
    case UPLOAD_FILE:
      return state
        .set('loading', true)
        .set('data', null)
        .set('error', null);
    case UPLOAD_FILE_ERROR:
      return state
        .set('loading', false)
        .set('data', null)
        .set('error', action.error);
    case UPLOAD_FILE_SUCCESS:
      return state
        .set('loading', false)
        .set('data', action.data)
        .set('error', null);
    case UPLOAD_SIGNATURE:
      return state
        .set('loading', true)
        .set('data', null)
        .set('isDone', false)
        .set('error', null);
    case UPLOAD_SIGNATURE_SUCCESS:
      return state
        .set('loading', false)
        .set('isDone', true)
        .set('data', action.data)
        .set('error', null);
    case UPLOAD_SIGNATURE_ERROR:
      return state
        .set('loading', false)
        .set('isDone', false)
        .set('data', null)
        .set('error', action.error);
    case VERIFY_TERMS:
      return state
        .set('loading', true)
        .set('isAccepted', false)
        .set('error', null);
    case VERIFY_TERMS_SUCCESS:
      return state
        .set('loading', false)
        .set('isAccepted', action.data.is_accepted)
        .set('error', null);
    case VERIFY_TERMS_ERROR:
      return state
        .set('loading', false)
        .set('isAccepted', false)
        .set('error', action.error.message);
    default:
      return state; // -haaj2zknwaq79q.cirggx0niovz.ap-southeast-1.rds.amazonaws.com -ukKX4X -pWdg7hWY4rE3o ebdb
  }
}

export default payeeTermsReducer;
