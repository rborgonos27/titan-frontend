/**
 *
 * PayeeTerms
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Segment, Checkbox, Button, Loader, Dimmer, Message } from 'semantic-ui-react';

import titanLogo from 'images/titan-logo.png';
import pdfImage from 'images/pdf.png';
import CardBox from 'components/CardBox';
import StandardModal from 'components/StandardModal';
import SignatureCanvas from 'react-signature-canvas'

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectPayeeTerms from './selectors';
import reducer from './reducer';
import { uploadFile, uploadSignature, verifyTerms } from './actions';
import saga from './saga';
import {
  B,
  TextBody,
  BodyWrapper,
  HeaderLeft,
  HeaderCenter,
  Img,
  ScrollBody,
  ThankYou,
} from './style';

/* eslint-disable react/prefer-stateless-function */
export class PayeeTerms extends React.Component {
  state = {
    isChecked: false,
    isDisabled: true,
    page: 'TERMS',
    pdfFile: null,
    payeeId: null,
    userId: null,
    openModal: false,
    imgSign: null,
    submitSignature: false,
    isAccepted: false,
    error: null,
    screenWidth: 0,
  };
  componentWillMount() {
    sessionStorage.clear();
    localStorage.clear();
  }
  componentDidMount() {
    sessionStorage.clear();
    localStorage.clear();
    const pdf = document.querySelector('#pdf');
    pdf.addEventListener('scroll', this.onScrollEmbed, false);
    const { match } = this.props;
    /* eslint-disable */
    const beforeDecode = match.params.id.split("'");
    const decodedValue = atob(beforeDecode[1]);
    const splitedValue = decodedValue.split(':');
    const payeeId = splitedValue[0];
    const userId = splitedValue[1];
    this.setState({ payeeId, userId });
    this.props.verifyTerms({ payeeId, userId });
    window.addEventListener("resize", this.resize);
    this.resize();
    /* eslint-enable */
  }
  componentWillReceiveProps(nextProps) {
    const { isDone, error, isAccepted } = nextProps.payeeterms;
    if (isDone) {
      this.setState({ page: 'COMPLETED' });
    }
    if (error) {
      this.setState({ error });
    }
    this.setState({
      isAccepted,
    });
  }
  resize = () => {
    this.setState({ screenWidth: window.innerWidth });
  };
  onScrollEmbed = () => {
    const pdf = document.querySelector('#pdf');
    const allowance = pdf.scrollHeight * 0.06;
    const threshold = pdf.scrollHeight - allowance;
    // console.log(allowance, threshold, pdf.scrollTop + pdf.clientHeight);
    if (pdf.clientHeight + pdf.scrollTop > threshold) {
      this.setState({ isDisabled: false });
    }
  };
  toggle = () => {
    const { isChecked } = this.state;
    this.setState({ isChecked: !isChecked });
  };
  openInNewTab = (e, url) => {
    e.preventDefault();
    window.open(url);
  };
  dataURLtoFile = (dataurl, filename) => {
    /* eslint-disable */
    let arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type:mime });
    /* eslint-enable */
  };
  signTerms = () => {
    let imgSign = this.signCanvas.getTrimmedCanvas().toDataURL('image/jpg');
    imgSign = this.dataURLtoFile(imgSign, 'signeddata.jpg');
    const data = this.state;
    this.setState({ openModal: false, imgSign });
    data.imgSign = imgSign;
    this.props.uploadSignature(data);
  };
  clearSign = () => {
    this.signCanvas.clear();
    this.setState({ submitSignature: false });
  };
  buildModal = () => {
    const { openModal, screenWidth } = this.state;
    let canvasWidth = 500;
    if (screenWidth <= 500) {
      canvasWidth = 250;
    } else if (screenWidth <= 620) {
      canvasWidth = 300;
    }
    return (
      <StandardModal
        headerText="Sign Here"
        open={openModal}
        onClose={() => this.setState({ openModal: false, submitSignature: false })}
        onActionText="CONFIRM"
        btnDisabled={!this.state.submitSignature}
        onContinue={() => this.signTerms()}
      >
        <div style={{ textAlign: 'center' }}>
          <SignatureCanvas
            penColor="black"
            onEnd={() => { this.setState({ submitSignature: true }) }}
            canvasProps={{ width: canvasWidth, height: 200, className: 'sigCanvas' }}
            ref={refs => { this.signCanvas = refs } } />
          <div>
            <Button positive onClick={() => this.clearSign()}>
              Clear
            </Button>
          </div>
        </div>
      </StandardModal>
    );
  };
  terms = () => {
    const { isDisabled, isChecked } = this.state;
    return (
      <div>
        {/* eslint-disable */}
        <ScrollBody id="pdf">
              <HeaderCenter>
                PAYMENT AGENT AND NETWORK PARTICIPATION AGREEMENT - PAYEE
              </HeaderCenter>
              <BodyWrapper>
                <TextBody>
                  This Payment Agent and Network Participation Agreement (this “Agreement”) is between you and us
                  (collectively referred to as the “Parties”) for your account and your relationship with us. This Agreement
                  encompass all of the terms of our relationship with you. Please read this document carefully. In referring
                  to the Parties individually, this Agreement may refer to “you,” “your,” “your Company,” or “Payee” in
                  reference to you and “us,” “we,” “our” or the Payment Agent in reference to Titan Vault, LLC or Athena
                  Pay, LLC.
                </TextBody>
                <HeaderLeft>
                  CERTAIN DEFINED TERMS
                </HeaderLeft>
                <TextBody>
                  <B>“The Network”</B> means the universe of companies that have entered into a relationship with us, the
                  Payment Agent, to accept payments into a custodial account on their behalf and disburse those
                  payments to other network participants to whom those funds are owed.
                </TextBody>
                <TextBody>
                  <B>“Payee”</B> means the provider of goods or services who is owed payment from the Payor.
                </TextBody>
                <TextBody>
                  <B>“Payor”</B> means the recipient of goods or services, who owes payment to the Payee.
                </TextBody>
                <HeaderLeft>
                  NATURE OF THE OBLIGATION
                </HeaderLeft>
                <TextBody>
                  <B>Appointment of Payment Agent.</B> Solely with respect to the payment of money from one Network
                  Participant, the Payor, to another Network Participant, the Payee, you appoint us as your agent to
                  represent you and act on your behalf. You as Payee acknowledge and agree that payment of money
                  from the Payor to us for delivery to you the Payee satisfies and discharges the Payor’s obligations to the
                  Payee to the extent of such payment. The Parties accept such appointment pursuant to the terms and
                  conditions set forth in this Agreement.
                </TextBody>
                <HeaderLeft>
                  DISBURSEMENTS, TERMINATION AND RESIGNATION AND REMOVAL
                </HeaderLeft>
                <TextBody>
                  <B>Disbursements of Funds in the Custodial Account.</B> We will only comply with written payment
                  instructions received from you (the “Company Remittance Instructions”) with respect to the remittance
                  of funds. As soon as practicable following our receipt of the Company Remittance Instructions, we shall
                  remit funds to you in accordance with such Company Remittance Instructions. You agree that we may
                  use affiliates (such as Athena Pay) to complete such remittances. You also agree that any payment
                  authorized shall be considered a Company Remittance Instruction from you to us.
                </TextBody>
                <TextBody>
                  <B>Termination of Agreement.</B> This Agreement may only be terminated by the written agreement of the
                  parties hereto.
                </TextBody>
                <TextBody>
                  <B>Resignation and Removal of Payment Agent.</B> We may resign from the performance of our duties
                  hereunder at any time by giving thirty (30) days’ prior written notice to you or may be removed, with or
                  without cause, by you at any time by giving thirty (30) days’ prior written notice to us.
                </TextBody>
                <HeaderLeft>
                  RESPONSIBILITIES OF PAYMENT AGENT
                </HeaderLeft>
                <TextBody>
                  <B>Payment Agent Responsibilities.</B> Our acceptance of our duties under this Agreement is subject to the
                  following terms and conditions, which we all agree shall govern and control with respect to its rights,
                  duties, liabilities and immunities:

                  <ol type="i">
                    <li>
                      Except as to our due execution and delivery of this Agreement, we make no representation
                      and have no responsibility as to the validity of this Agreement or of any other instrument
                      referred to herein, or as to the correctness of any statement contained herein, and we shall
                      not be required to inquire as to the performance of any obligation under any other
                      agreement;
                    </li>
                    <li>
                      We shall be protected in acting upon any written notice, request, waiver, consent, receipt or
                      other paper or document, not only as to its due execution and the validity and effectiveness
                      of its provisions, but also as to the truth of any information therein contained, which we in
                      good faith believe to be genuine and what it purports to be;
                    </li>
                    <li>
                      We may consult with competent and responsible legal counsel selected by us and we shall
                      not be liable for any action taken or omitted by it in good faith in accordance with the advice
                      of such counsel;
                    </li>
                    <li>
                      You shall reimburse us for all reasonable expenses incurred by us in connection with our
                      duties hereunder (other than taxes imposed in respect of the receipt of fees by the Payment
                      Agent). You agree to indemnify, defend and hold us and our directors, employees, officers,
                      agents, successors and assigns (collectively, the “Indemnified Parties”) harmless from and
                      against any and all losses, claims, damages, liabilities and expenses including, without
                      limitation, reasonable costs of attorney’s fees and expenses (collectively, “Damages”),
                      incurred by us without gross negligence or willful misconduct on our part and arising out of
                      or in connection with the performance by us of our duties under this Agreement. Such
                      indemnity includes, without limitation, damages incurred in connection with any litigation
                      arising from this Agreement or involving the subject matter hereof. The indemnification
                      provisions contained in this section are in addition to any other rights any of the Indemnified
                      Parties may have by law or otherwise and shall survive the termination of this Agreement or
                      the resignation or removal of the Payment Agent;
                    </li>
                    <li>
                      We shall have no duties or responsibilities except those expressly set forth herein, and we
                      shall not be bound by any modification of this Agreement unless in writing and signed by all
                      parties hereto or their respective successors in interest;
                    </li>
                    <li>
                      We shall have no responsibility in respect of the validity or sufficiency of this Agreement or of
                      the terms hereof;
                    </li>
                    <li>
                      We shall be protected by you in acting upon any notice, resolution, request, consent, order,
                      certificate, report, opinion, bond or other paper or document reasonably believed by it to be
                      genuine and to have been signed and presented by the proper party or parties. Whenever we
                      deem it necessary or desirable that a matter be proved or established prior to taking or
                      suffering any action under this Agreement, such matter may be deemed conclusively proved
                      and established by a certificate signed by Company, and such certificate shall be full warranty
                      for any action taken or suffered in good faith under the provisions of this Agreement;
                    </li>
                    <li>
                      We do not have any interest in the Custodial Account, but we are serving as Payment Agent
                      only and having only possession thereof;
                    </li>
                    <li>
                      WE SHALL NOT BE LIABLE, DIRECTLY OR INDIRECTLY, FOR ANY (I) DAMAGES, LOSSES OR
                      EXPENSES ARISING OUT OF THE SERVICES PROVIDED HEREUNDER, OTHER THAN DAMAGES,
                      LOSSES OR EXPENSES WHICH HAVE BEEN FINALLY ADJUDICATED TO HAVE DIRECTLY
                      RESULTED FROM OUR GROSS NEGLIGENCE OR WILLFUL MISCONDUCT, OR (II) SPECIAL,
                      INDIRECT OR CONSEQUENTIAL DAMAGES OR LOSSES OF ANY KIND WHATSOEVER
                      (INCLUDING WITHOUT LIMITATION LOST PROFITS), EVEN IF WE HAVE BEEN ADVISED OF THE
                      POSSIBILITY OF SUCH LOSSES OR DAMAGES AND REGARDLESS OF THE FORM OF ACTION;
                    </li>
                    <li>
                      No provision of this Agreement shall require us to risk or advance our own funds or
                      otherwise incur any financial liability or potential financial liability in the performance of its
                      duties or the exercise of its rights under this Agreement; and
                    </li>
                    <li>
                      If any portion of the funds in the Custodial Account shall be attached, garnished or levied
                      upon by any court order, or the delivery thereof shall be stayed or enjoined by an order of a
                      court, or any order, judgment or decree shall be made or entered by any court order
                      affecting funds in the Account, we are hereby expressly authorized, in our sole discretion, to
                      respond as it deems appropriate or to comply with all writs, orders or decrees so entered or
                      issued, or which it is advised by legal counsel of our own choosing is binding upon it, whether
                      with or without jurisdiction. If we obey or comply with any such writ, order or decree we
                      shall not be liable to you or to any other person, firm or corporation, should, by reason of
                      such compliance notwithstanding, such writ, order or decree be subsequently reversed,
                      modified, annulled, set aside or vacated.
                    </li>
                  </ol>
                </TextBody>
                <TextBody>
                  <B>KNOW YOUR CUSTOMER.</B> Federal law, including the USA PATRIOT Act, requires all financial institutions
                  to obtain, verify and record information that identifies each customer who opens an account with that
                  financial institution. You agree that you will provide us with your legal name, address, and your Tax
                  Identification Number (TIN). You agree that you will provide us with unexpired photo identification if
                  requested. We may validate the information you provide to us to ensure we have a reasonable
                  assurance of your identity. We may contact you for additional information. If we are not able to verify
                  your identity to our satisfaction, you agree that our payment obligation is abrogated.
                </TextBody>
                <HeaderLeft>
                  MISCELLANEOUS
                </HeaderLeft>
                <TextBody>
                  <B>Binding Agreement.</B> When you request to make a payment, you acknowledge that you have reviewed
                  and understand the terms of this Agreement and you agree to be governed by these terms. You
                  understand that these terms, as we may change or supplement them periodically, are a binding contract
                  between you and us for your payment.
                </TextBody>
                <TextBody>
                  <B>Modifications to This Agreement.</B> We may change this Agreement at any time. We may add new terms.
                  We may delete or amend existing terms. We may add new services and discontinue existing services. If
                  you continue to make payments, you are deemed to accept and agree to the change and are bound by
                  the change.
                </TextBody>
                <TextBody>
                  <B>Governing Law.</B> This Agreement shall be construed, performed and enforced in accordance with the
                  laws of the State of California applicable to contracts made and to be performed in such State.
                  <HeaderLeft>
                    Other
                  </HeaderLeft>

                  <ol type="i">
                    <li>
                      This Agreement shall be binding upon and inure to the benefit of the parties and their
                      successors. Neither this Agreement nor any right or interest hereunder may be assigned in
                      whole or in part by any party without the prior consent of the other parties.
                    </li>
                    <li>
                      We agree that if any provision of this Agreement shall under any circumstances be deemed
                      invalid or inoperative, this Agreement shall be construed and enforced accordingly, and the
                      invalidity of any portion of this Agreement shall not affect the validity of the remainder
                      hereof.
                    </li>
                    <li>
                      No party to this Agreement is liable to any other party for losses due to, or if it is unable to
                      perform its obligations under the terms of this Agreement because of, acts of God, fire,
                      floods, earthquake, strikes, equipment or transmission failure, war, riot, nuclear accident,
                      terror attack, computer piracy, cyber-terrorism, or other causes reasonably beyond its
                      control.
                    </li>
                  </ol>
                  <br />
                  <br />
                  <br />
                  <br />
                </TextBody>
              </BodyWrapper>
        </ScrollBody>
        {/* eslint-enable */}
        <div>
          <label className="container_checkbox">
            Accept Terms & Agreement
            <input type="checkbox" checked={isChecked} disabled={isDisabled} onChange={this.toggle} />
              <span className="_checkmark"></span>
          </label>
        </div>
        <div>
          <Button
            floated="right"
            color="orange"
            disabled={!isChecked}
            onClick={() => this.setState({ openModal: true })}
          >
            Continue
          </Button>
        </div>
      </div>
    );
  };
  nextStep = () => {
    const previewSpan = {
      display: 'inline',
    };
    const previewStyle = {
      display: 'inline',
      width: 70,
      height: 70,
      padding: 5,
    };
    return (
      <div style={{ color: '#000' }}>
        <BodyWrapper>
          <TextBody>
            Before we process the payment request, a signed Terms and Agreement
            is needed. We provided a link below where you can download the Terms and Agreement Document.
          </TextBody>
          <TextBody>
            <B>Instructions:</B>
            <ol>
              <li>
                Download the Terms & conditions file.
                <div>
                  <span style={previewSpan}>
                    <Button
                      onClick={e =>
                        this.openInNewTab(
                          e,
                          'https://s3-us-west-2.amazonaws.com/dev.titan-vault.com/file/backup/VwZYJUvAnpHpGhaJNKVF.pdf',
                        )
                      }
                    >
                      <img alt="Preview" src={pdfImage} style={previewStyle} />
                    </Button>
                    <div>
                      <b style={{ fontSize: 13 }}>Download this File</b>
                    </div>
                  </span>
                </div>
              </li>
              <li>Open PDF file using Adobe Acrobat Pro DC.</li>
              <li>
                Sign using the App.
              </li>
              <li>
                Upload the signed document here.
                <div className="profileImageUpload">
                  <Button
                    onClick={() => {
                      document.querySelector('#profilePicture').click();
                    }}
                  >
                    Upload signed Terms & Agreement File
                  </Button>
                  <input
                    type="file"
                    id="profilePicture"
                    name="file"
                    encType="multipart/form-data"
                    accept="application/pdf"
                    onChange={e => {
                      /* eslint-disable */
                      const file = e.target.files[0];
                      this.setState({ pdfFile: file });
                      // this.readURL(file);
                      /* eslint-enable */
                    }}
                  />
                </div>
              </li>
            </ol>
          </TextBody>
          <div>
            <Button
              floated="right"
              color="orange"
              disabled={this.state.pdfFile === null}
              onClick={() => {
                this.props.uploadFile(this.state);
                this.setState({ page: 'COMPLETED' });
              }}
            >
              Complete
            </Button>
            <Button positive onClick={() => this.setState({ openModal: true })}>
              Sign
            </Button>
          </div>
        </BodyWrapper>
      </div>
    );
  };
  thankyou = () => {
    const { isAccepted } = this.state;
    return (
      <ThankYou>
        {isAccepted ? <Message positive>Payee Terms already accepted.</Message> : null}
        <h1>
          Great! You are now able to receive payment from Athena Pay via Titan
          Vault scheduled.
        </h1>
        <br />

        <h1>Your Titan-Athena Payments Team.</h1>
      </ThankYou>
    );
  };
  managePage = () => {
    const { page, isAccepted } = this.state;
    if (isAccepted) {
      return this.thankyou();
    }
    switch (page) {
      case 'TERMS':
        return this.terms();
      case 'NEXT_STEP':
        return this.nextStep();
      case 'COMPLETED':
        return this.thankyou();
      default:
        return this.terms();
    }
  };
  render() {
    const { loading } = this.props.payeeterms;
    return (
      <div style={{ background: '#CCC' }}>
        <Segment className="segmentStyleTerms" color="orange">
          <div className="terms-card-box">
            <Img src={titanLogo} />
            <br />
            <br />
            {loading ? (
              <Dimmer active>
                <Loader size="small">Loading</Loader>
              </Dimmer>
            ) : null}
            {this.buildModal()}
            {this.managePage()}
          </div>
        </Segment>
      </div>
    );
  }
}

PayeeTerms.propTypes = {
  match: PropTypes.any,
  payeeterms: PropTypes.any,
  uploadFile: PropTypes.func.isRequired,
  uploadSignature: PropTypes.func.isRequired,
  verifyTerms: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  payeeterms: makeSelectPayeeTerms(),
});

const mapDispatchToProps = { uploadFile, uploadSignature, verifyTerms };

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'payeeTerms', reducer });
const withSaga = injectSaga({ key: 'payeeTerms', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(PayeeTerms);
