import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the payeeTerms state domain
 */

const selectPayeeTermsDomain = state => state.get('payeeTerms', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by PayeeTerms
 */

const makeSelectPayeeTerms = () =>
  createSelector(selectPayeeTermsDomain, substate => substate.toJS());

export default makeSelectPayeeTerms;
export { selectPayeeTermsDomain };
