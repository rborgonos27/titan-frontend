/*
 *
 * PayeeTerms constants
 *
 */

export const DEFAULT_ACTION = 'app/PayeeTerms/DEFAULT_ACTION';
export const UPLOAD_FILE = 'app/PayeeTerms/UPLOAD_FILE';
export const UPLOAD_FILE_SUCCESS = 'app/PayeeTerms/UPLOAD_FILE_SUCCESS';
export const UPLOAD_FILE_ERROR = 'app/PayeeTerms/UPLOAD_FILE_ERROR';

export const UPLOAD_SIGNATURE = 'app/PayeeTerms/UPLOAD_SIGNATURE';
export const UPLOAD_SIGNATURE_SUCCESS =
  'app/PayeeTerms/UPLOAD_SIGNATURE_SUCCESS';
export const UPLOAD_SIGNATURE_ERROR = 'app/PayeeTerms/UPLOAD_SIGNATURE_ERROR';

export const VERIFY_TERMS = 'app/PayeeTerms/VERIFY_TERMS';
export const VERIFY_TERMS_ERROR = 'app/PayeeTerms/VERIFY_TERMS_ERROR';
export const VERIFY_TERMS_SUCCESS = 'app/PayeeTerms/VERIFY_TERMS_SUCCESS';
