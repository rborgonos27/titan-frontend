import { all, takeLatest, call, put } from 'redux-saga/effects';
import request from 'utils/request';
import { APP } from 'config';
import moment from 'moment';
import {
  UPLOAD_FILE,
  UPLOAD_SIGNATURE,
  VERIFY_TERMS,
} from './constants';
import {
  uploadFileError,
  uploadFileSuccess,
  uploadSignatureError,
  uploadSignatureSuccess,
  verifyTermsError,
  verifyTermsSuccess,
} from './actions';
import { optionsFormData } from '../../utils/options';

export function* uploadFunction(termsData) {
  try {
    const API_URI = APP.API_URL;
    const payeeAgreement = yield call(
      request,
      `${API_URI}/payee_agreement/?payee_id=${termsData.data.payeeId}`,
      optionsFormData('GET', null, null),
    );
    if (payeeAgreement.results) {
      const formDataFile = new FormData();
      formDataFile.append('filename', termsData.data.pdfFile);
      formDataFile.append('url', 'filename');
      formDataFile.append('mime_type', termsData.data.pdfFile.type);
      formDataFile.append('resource_id', termsData.data.payeeId);
      formDataFile.append('type', 'PAYEE');
      formDataFile.append('payee_agreement_id', payeeAgreement.results[0].id);
      formDataFile.append(
        'expire_at',
        moment()
          .add(1, 'hours')
          .format('YYYY-MM-DD h:m:s'),
      );
      const payeeFile = yield call(
        request,
        `${API_URI}/payee_file/`,
        optionsFormData('POST', formDataFile, null),
      );
      if (payeeFile) {
        yield put(uploadFileSuccess(payeeFile));
        // const formData = new FormData();
        // formData.append('payee_id', termsData.data.payeeId);
        // formData.append('remarks', 'Signed');
        // formData.append('is_accepted', true);
      }
    }
  } catch (error) {
    yield put(uploadFileError(error));
  }
}

export function* uploadSignature(termsData) {
  try {
    console.log(termsData.data);
    const API_URI = APP.API_URL;
    const payeeAgreement = yield call(
      request,
      `${API_URI}/payee_user_agreement/?payee_id=${termsData.data.payeeId}&user_id=${termsData.data.userId}`,
      optionsFormData('GET', null, null),
    );
    if (payeeAgreement.success) {
      const formDataFile = new FormData();
      formDataFile.append('file', termsData.data.imgSign);
      formDataFile.append('payee_id', termsData.data.payeeId);
      formDataFile.append('member_name', payeeAgreement.data.member_name);
      // formDataFile.append('mime_type', termsData.data.pdfFile.type);
      // formDataFile.append('resource_id', termsData.data.payeeId);
      // formDataFile.append('type', 'PAYEE');
      formDataFile.append('payee_agreement_id', payeeAgreement.data.id);
      // formDataFile.append(
      //   'expire_at',
      //   moment()
      //     .add(1, 'hours')
      //     .format('YYYY-MM-DD h:m:s'),
      // );
      const payeeFile = yield call(
        request,
        `${API_URI}/upload_signature/`,
        optionsFormData('POST', formDataFile, null),
      );
      if (payeeFile) {
        console.log(payeeFile);
        yield put(uploadSignatureSuccess(payeeFile));
        // const formData = new FormData();
        // formData.append('payee_id', termsData.data.payeeId);
        // formData.append('remarks', 'Signed');
        // formData.append('is_accepted', true);
      }
    }
  } catch (error) {
    yield put(uploadSignatureError(error));
  }
}

export function* verifyTerms(payeeData) {
  try {
    const API_URI = APP.API_URL;
    const verifyTermsResponse = yield call(
      request,
      `${API_URI}/payee_view/?payee_id=${payeeData.data.payeeId}&user_id=${payeeData.data.userId}`,
      optionsFormData('GET', null, null),
    );
    if (verifyTermsResponse.success) {
      yield put(verifyTermsSuccess(verifyTermsResponse.data));
    } else {
      yield put(verifyTermsError(verifyTermsResponse.message));
    }
  } catch (error) {
    yield put(verifyTermsError(error));
  }
}

export function* uploadFileWatcher() {
  yield takeLatest(UPLOAD_FILE, uploadFunction);
}

export function* uploadSignatureWatcher() {
  yield takeLatest(UPLOAD_SIGNATURE, uploadSignature);
}

export function* verifyTermsWatcher() {
  yield takeLatest(VERIFY_TERMS, verifyTerms);
}

export default function* rootSaga() {
  yield all([
    uploadFileWatcher(),
    uploadSignatureWatcher(),
    verifyTermsWatcher(),
  ]);
}
