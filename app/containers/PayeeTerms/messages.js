/*
 * PayeeTerms Messages
 *
 * This contains all the text for the PayeeTerms component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.PayeeTerms.header',
    defaultMessage: 'This is PayeeTerms container !',
  },
});
