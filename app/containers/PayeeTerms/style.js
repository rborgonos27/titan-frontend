import styled from 'styled-components';

const ScrollBody = styled.div`
  color: #000;
  padding: 20px;
  border: 1px solid #cccccc;
  width: 100%;
  height: 700px;
  overflow: scroll;
`;

const ThankYou = styled.div`
  height: 700px;
`;

const Img = styled.img`
  align: left;
  width: 45px;
  height: 45px;
  right: 0px;
  margin-right: 40px;
`;

const HeaderCenter = styled.h4`
  text-align: center;
  margin-bottom: 10px;
  font-weight: bold;
`;

const HeaderLeftNoUnderline = styled.h4`
  text-align: left;
  margin-bottom: 20px;
  font-weight: bold;
`;

const HeaderLeft = styled.h5`
  text-align: left;
  margin-bottom: 20px;
  margin-top: 20px;
  font-weight: bold;
  text-decoration: underline;
`;
const HeaderPlain = styled.h5`
  text-align: left;
  margin-bottom: 20px;
  margin-top: 20px;
  font-weight: bold;
`;

const BodyWrapper = styled.div`
  margin-top: 10px;
  padding: 0px;
  text-align: justify;
`;

const TextBody = styled.div`
  text-align: justify;
  margin-bottom: 10px;
`;

const SubHeader = styled.div`
  font-weight: bolder;
  font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  color: #505458;
  margin-bottom: 10px;
  margin-top: 10px;
`;

const B = styled.span`
  font-weight: bolder;
  font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  color: #505458;
`;

const A = styled.a`
  &:hover {
    text-decoration: underline;
    color: #000;
    cursor: pointer;
  }
  text-decoration: underline;
  color: #000;
  cursor: pointer;
`;

const BigHeader = styled.h2`
  text-decoration: underline;
`;

export {
  B,
  SubHeader,
  TextBody,
  BodyWrapper,
  HeaderPlain,
  HeaderLeft,
  HeaderCenter,
  Img,
  A,
  BigHeader,
  HeaderLeftNoUnderline,
  ScrollBody,
  ThankYou,
};
