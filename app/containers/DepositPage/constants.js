/*
 *
 * DepositPage constants
 *
 */

export const DEFAULT_ACTION = 'app/DepositPage/DEFAULT_ACTION';
export const ON_RESET = 'app/DepositPage/ON_RESET';

export const ON_SUBMIT = 'app/DepositPage/ON_SUBMIT';
export const ON_SUBMIT_SUCCESS = 'app/DepositPage/ON_SUBMIT_SUCCESS';
export const ON_SUBMIT_ERROR = 'app/DepositPage/ON_SUBMIT_ERROR';

export const FILE_UPLOAD = 'app/DepositPage/FILE_UPLOAD';
export const FILE_UPLOAD_SUCCESS = 'app/DepositPage/FILE_UPLOAD_SUCCESS';
export const FILE_UPLOAD_ERROR = 'app/DepositPage/FILE_UPLOAD_ERROR';

export const GET_TIME = 'app/DepositPage/GET_TIME';
export const GET_TIME_SUCCESS = 'app/DepositPage/GET_TIME_SUCCESS';
export const GET_TIME_ERROR = 'app/DepositPage/GET_TIME_ERROR';

export const GET_DEPOSIT_ACCOUNT = 'app/DepositPage/GET_DEPOSIT_ACCOUNT';
export const GET_DEPOSIT_ACCOUNT_SUCCESS =
  'app/DepositPage/GET_DEPOSIT_ACCOUNT_SUCCESS';
export const GET_DEPOSIT_ACCOUNT_ERROR =
  'app/DepositPage/GET_DEPOSIT_ACCOUNT_ERROR';

export const GET_FEE_TRANSACTION = 'app/DepositPage/GET_FEE_TRANSACTION';
export const GET_FEE_TRANSACTION_SUCCESS =
  'app/DepositPage/GET_FEE_TRANSACTION_SUCCESS';
export const GET_FEE_TRANSACTION_ERROR =
  'app/DepositPage/GET_FEE_TRANSACTION_ERROR';

export const GET_USER_FEE_RATE = 'app/DepositPage/GET_USER_FEE_RATE';
export const GET_USER_FEE_RATE_SUCCESS =
  'app/DepositPage/GET_USER_FEE_RATE_SUCCESS';
export const GET_USER_FEE_RATE_ERROR =
  'app/DepositPage/GET_USER_FEE_RATE_ERROR';

export const GET_USER_FEE_TIER_RATE = 'app/DepositPage/GET_USER_FEE_RATE';
export const GET_USER_FEE_TIER_RATE_SUCCESS =
  'app/DepositPage/GET_USER_FEE_TIER_RATE_SUCCESS';
export const GET_USER_FEE_TIER_RATE_ERROR =
  'app/DepositPage/GET_USER_FEE_TIER_RATE_ERROR';

export const SEND_LOG = 'app/DepositPage/SEND_LOG';
export const SEND_LOG_SUCCESS = 'app/DepositPage/SEND_LOG_SUCCESS';
export const SEND_LOG_ERROR = 'app/DepositPage/SEND_LOG_ERROR';

export const COMPUTE_FEE = 'app/DepositPage/COMPUTE_FEE';
export const COMPUTE_FEE_SUCCESS = 'app/DepositPage/COMPUTE_FEE_SUCCESS';
export const COMPUTE_FEE_ERROR = 'app/DepositPage/COMPUTE_FEE_ERROR';

export const REMOVE_FILE = 'app/DepositPage/REMOVE_FILE';
export const REMOVE_FILE_SUCCESS = 'app/DepositPage/REMOVE_FILE_SUCCESS';
export const REMOVE_FILE_ERROR = 'app/DepositPage/REMOVE_FILE_ERROR';
