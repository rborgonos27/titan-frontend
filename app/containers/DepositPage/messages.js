/*
 * DepositPage Messages
 *
 * This contains all the text for the DepositPage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.DepositPage.header',
    defaultMessage: 'This is DepositPage container !',
  },
  depositDateText: {
    id: 'app.containers.DepositPage.depositDateText',
    defaultMessage:
      'Calendar is based on US, Pacific Standard time (PST.)' +
      ' Deposits made after 2pm PST will be processed the following business day.' +
      ' Deposit that fall on a weekend or bank holiday will be processed the next business day',
  },
  pickupDepositText: {
    id: 'app.containers.DepositPage.pickupDepositText',
    defaultMessage:
      'Selecting “Recurring” means this Cash deposit will be picked' +
      ' up on the next upcoming regularly scheduled day pre-arranged with the armored car.' +
      ' Selecting “On-demand” is deposits without a recurring pickup schedule or require special one-off pickups.' +
      ' On-demand pickup must be scheduled at minimum three (3) days from deposit date.' +
      ' Please choose a later deposit date if you need more time prepping your deposit.',
  },
});
