export const getTier = (tierObjects, totalTransactionAmount) => {
    let currentTier = null;
    for (let i = 0; i < tierObjects.length; i++) {
        currentTier = tierObjects[i];
        if (!currentTier.deposit_threshold) {
            break;
        }
        if (currentTier.deposit_threshold >= totalTransactionAmount) {
            break;
        }
    }
    return currentTier;
}

export const getTierAmount = (tierObjects, depositAmount, currentTotal) => { // 300k, 0
    const nextTier = getTier(tierObjects, currentTotal)
    const resultTier = [];
    let temp = depositAmount // 300 000
    if (nextTier.tier_no == tierObjects.length) {
        resultTier.push({ tier: nextTier, amount: temp });
        return resultTier;
    }

    console.log(nextTier.deposit_threshold);
    let remainingAmount = nextTier.deposit_threshold - currentTotal; // 400 000 - 0 = 400,000

    for (let i = nextTier.tier_no - 1; i < tierObjects.length; i++) {
        const currentTier = tierObjects[i];

        let difference = 0;
        if (i == 0) {
            difference = currentTier.deposit_threshold;
        } else {
            difference = currentTier.deposit_threshold - tierObjects[i-1].deposit_threshold; // 800,000 - 500,000 = 300,000
        }        
        
        
        if (remainingAmount > 0) {
            resultTier.push({ tier: nextTier, amount: remainingAmount }); // 500, 000
            temp = temp - remainingAmount;
            remainingAmount = 0;
            continue;
        }

        if (temp > difference) {
            resultTier.push({ tier: currentTier, amount: difference });
            temp = temp - difference;
        } else {
            resultTier.push({ tier: currentTier, amount: temp });
            break;
        }


        if (currentTier.tier_no == tierObjects.length) {
            resultTier.push({ tier: currentTier, amount: temp });
            break;
        }
    }

    return resultTier;
}

