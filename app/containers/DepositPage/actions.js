/*
 *
 * DepositPage actions
 *
 */

import {
  ON_SUBMIT,
  ON_SUBMIT_SUCCESS,
  ON_SUBMIT_ERROR,
  FILE_UPLOAD,
  FILE_UPLOAD_ERROR,
  FILE_UPLOAD_SUCCESS,
  GET_TIME,
  GET_TIME_SUCCESS,
  GET_TIME_ERROR,
  ON_RESET,
  GET_DEPOSIT_ACCOUNT,
  GET_DEPOSIT_ACCOUNT_SUCCESS,
  GET_DEPOSIT_ACCOUNT_ERROR,
  GET_FEE_TRANSACTION,
  GET_FEE_TRANSACTION_SUCCESS,
  GET_FEE_TRANSACTION_ERROR,
  GET_USER_FEE_RATE,
  GET_USER_FEE_RATE_ERROR,
  GET_USER_FEE_RATE_SUCCESS,
  GET_USER_FEE_TIER_RATE,
  GET_USER_FEE_TIER_RATE_SUCCESS,
  GET_USER_FEE_TIER_RATE_ERROR,
  SEND_LOG,
  SEND_LOG_ERROR,
  SEND_LOG_SUCCESS,
  COMPUTE_FEE,
  COMPUTE_FEE_SUCCESS,
  COMPUTE_FEE_ERROR,
  REMOVE_FILE,
  REMOVE_FILE_SUCCESS,
  REMOVE_FILE_ERROR,
} from './constants';

export function onReset() {
  return {
    type: ON_RESET,
  };
}
export function onSubmit(data) {
  return {
    type: ON_SUBMIT,
    data,
  };
}

export function onSubmitSuccess(data) {
  return {
    type: ON_SUBMIT_SUCCESS,
    data,
  };
}

export function onSubmitError(error) {
  return {
    type: ON_SUBMIT_ERROR,
    error,
  };
}

export function fileUpload(data) {
  return {
    type: FILE_UPLOAD,
    data,
  };
}

export function fileUploadSuccess(data) {
  return {
    type: FILE_UPLOAD_SUCCESS,
    data,
  };
}

export function fileUploadError(data) {
  return {
    type: FILE_UPLOAD_ERROR,
    data,
  };
}

export function getTime() {
  return {
    type: GET_TIME,
  };
}

export function getTimeSuccess(data) {
  return {
    type: GET_TIME_SUCCESS,
    data,
  };
}

export function getTimeError(data) {
  return {
    type: GET_TIME_ERROR,
    data,
  };
}

export function getDepositAccount() {
  return {
    type: GET_DEPOSIT_ACCOUNT,
  };
}

export function getDepositAccountSuccess(data) {
  return {
    type: GET_DEPOSIT_ACCOUNT_SUCCESS,
    data,
  };
}

export function getDepositAccountError(error) {
  return {
    type: GET_DEPOSIT_ACCOUNT_ERROR,
    error,
  };
}

export function getFeeTransaction(data) {
  return {
    type: GET_FEE_TRANSACTION,
    data,
  };
}

export function getFeeTransactionSuccess(data) {
  return {
    type: GET_FEE_TRANSACTION_SUCCESS,
    data,
  };
}

export function getFeeTransactionError(error) {
  return {
    type: GET_FEE_TRANSACTION_ERROR,
    error,
  };
}

export function getUserFeeRate(data) {
  return {
    type: GET_USER_FEE_RATE,
    data,
  };
}

export function getUserFeeRateSuccess(data) {
  return {
    type: GET_USER_FEE_RATE_SUCCESS,
    data,
  };
}

export function getUserFeeRateError(error) {
  return {
    type: GET_USER_FEE_RATE_ERROR,
    error,
  };
}

export function getUserFeeTierRate(data) {
  return {
    type: GET_USER_FEE_TIER_RATE,
    data,
  };
}

export function getUserFeeTierRateSuccess(data) {
  return {
    type: GET_USER_FEE_TIER_RATE_SUCCESS,
    data,
  };
}

export function getUserFeeTierRateError(error) {
  return {
    type: GET_USER_FEE_TIER_RATE_ERROR,
    error,
  };
}

export function sendLog(data) {
  return {
    type: SEND_LOG,
    data,
  };
}

export function sendLogSuccess(data) {
  return {
    type: SEND_LOG_SUCCESS,
    data,
  };
}

export function sendLogError(error) {
  return {
    type: SEND_LOG_ERROR,
    error,
  };
}

export function computeFee(data) {
  return {
    type: COMPUTE_FEE,
    data,
  };
}

export function computeFeeError(error) {
  return {
    type: COMPUTE_FEE_ERROR,
    error,
  };
}

export function computeFeeSuccess(data) {
  return {
    type: COMPUTE_FEE_SUCCESS,
    data,
  };
}

export function removeFile(data) {
  return {
    type: REMOVE_FILE,
    data,
  };
}

export function removeFileSuccess(data) {
  return {
    type: REMOVE_FILE_SUCCESS,
    data,
  };
}

export function removeFileError(error) {
  return {
    type: REMOVE_FILE_ERROR,
    error,
  };
}
