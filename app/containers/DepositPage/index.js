/**
 *
 * DepositPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import moment from 'moment';
import ContainerWrapper from 'components/ContainerWrapper';
import { HelpText, RequiredText } from 'components/Utils';
import {
  Form,
  TextArea,
  Button,
  Header,
  Grid,
  Dimmer,
  Loader,
} from 'semantic-ui-react';
import { formatMoney } from 'utils/formatMoney';
import Dropzone from 'react-dropzone';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { APP } from 'config';
import StandardModal from 'components/StandardModal';
import pdfImage from 'images/pdf.png';
import closeImage from 'images/close.png';
import fileIcon from 'images/file-icon.png';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectDepositPage, {
  makeSelectPayeeDepositAccount,
  makeSelectCurrentUser,
} from './selectors';
import {
  fileUpload,
  onSubmit,
  getTime,
  getDepositAccount,
  getFeeTransaction,
  getUserFeeRate,
  getUserFeeTierRate,
  sendLog,
  computeFee,
  removeFile,
} from './actions';
import reducer from './reducer';
import {
  getDateForward,
  getMaxPickupDate,
} from '../../containers/SendPayment/dates';
import saga from './saga';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export class DepositPage extends React.Component {
  state = {
    amount: '',
    error: null,
    fee: 0,
    payee: null,
    pay_at: '',
    pickup_at: '',
    remarks: '',
    depositDate: '',
    pickupDate: '',
    serverTime: '',
    uploadedFiles: sessionStorage.getItem('deposit_images')
      ? JSON.parse(sessionStorage.getItem('deposit_images'))
      : [],
    isScheduled: false,
    transactionType: 'PAPER',
    openConfirmModal: false,
    feeAmount: null,
    currentDate: null,
    currentUser: null,
    cancelModal: false,
    pickupType: 'ON-DEMAND',
  };
  componentWillMount() {
    // this.props.onReset();
    this.props.getTime();
    this.props.getDepositAccount();
  }
  componentDidMount() {
    const wrapper = document.querySelector('#wrapper');
    wrapper.className = 'enlarged forced';
    const { currentUser } = this.props;
    this.props.getFeeTransaction({ userId: currentUser.id });
    /* eslint-disable */
    this.setState({ currentUser });
    /* eslint-enable */
    this.props.getUserFeeRate(currentUser.id);
    this.props.getUserFeeTierRate(currentUser.id);
  }
  componentWillReceiveProps(nextProps) {
    const serverTime = nextProps.depositpage.currentTime;
    const payee = nextProps.payeeDepositAccount;
    const feeAmount = nextProps.depositpage.computedFee;
    // const { pay_at, pickup_at } = this.state;
    if (serverTime) {
      this.setState({ serverTime });
      const currentDate = moment(serverTime).format(APP.DATE_FORMAT);
      this.setState({ currentDate });
      /* eslint-disable */
      // if (!pay_at || !pickup_at) {
      //   this.setState({ pay_at: currentDate, pickup_at: currentDate });
      // }
      /* eslint-enable */
    }
    if (nextProps.depositpage.isUploaded) {
      this.setState({
        uploadedFiles: sessionStorage.getItem('deposit_images')
          ? JSON.parse(sessionStorage.getItem('deposit_images'))
          : [],
      });
    }
    if (payee) {
      this.setState({ payee });
    }
    if (feeAmount) {
      this.setState({ feeAmount });
    }
  }
  handleClickDate = (id, mindate) => {
    this.setState({ [id]: mindate });
  };
  handleChange = (event, { name = undefined, value }) => {
    this.setState({ showPickupDate: false });
    /* eslint-disable */
    if (this.state.hasOwnProperty(name)) {
      this.setState({ [name]: value });
      if (name === 'pay_at') {
        this.setState({ pickup_at: '' });
      }
    }
    /* eslint-enable */

    const { serverTime } = this.state;
    const currentDate = moment(serverTime).format(APP.DATE_FORMAT);
    const maxPickupDate =
      this.state.pay_at !== ''
        ? getMaxPickupDate(this.state.pay_at, 3)
        : currentDate;

    // noOfWorkingDays
    const minPickupDate =
      this.state.pay_at !== '' ? getDateForward(2, currentDate) : currentDate;
    this.setState({ maxPickupDate, minPickupDate, showPickupDate: true });
  };
  handleChangeDropDown = (e, { value }) => {
    this.setState({ payee: value });
    sessionStorage.setItem(`deposit_payee`, value);
  };
  handleUpdatePickupAtValues = () => {
    const { serverTime } = this.state;
    const currentDate = moment(serverTime).format(APP.DATE_FORMAT);
    const maxPickupDate =
      this.state.pay_at !== ''
        ? getMaxPickupDate(this.state.pay_at, 3)
        : currentDate;
    // noOfWorkingDays
    const minPickupDate = getDateForward(3, currentDate);
    return { maxPickupDate, minPickupDate };
  };
  setDateForImmdiateTransactions = isScheduled => {
    const { serverTime } = this.state;
    const serverTimeMoment = moment(serverTime).format(APP.DATE_FORMAT);
    const serverTimeHourMoment = moment(serverTime).format('H');
    const daysForward = serverTimeHourMoment > 14 ? 8 : 7;
    const minPayDate = getDateForward(daysForward, serverTimeMoment);
    if (!isScheduled) {
      const { minPickupDate } = this.handleUpdatePickupAtValues();
      this.setState({ pickup_at: minPickupDate, pay_at: minPayDate });
    } else {
      this.setState({ pickupType: 'RECURRING', pickup_at: '', pay_at: '' });
    }
  };
  onBlurPickupAt = () => {
    this.getTransactionFee();
  };
  handleDayChangePayAt = (selectedDay, modifiers, dayPickerInput) => {
    const state = 'pay_at';
    this.handleSetDateState(selectedDay, modifiers, dayPickerInput, state);
    const { minPickupDate } = this.handleUpdatePickupAtValues();
    this.setState({ pickup_at: minPickupDate, isPickUpAtDisabled: false });
  };
  handleDayChangePickUpAt = (selectedDay, modifiers, dayPickerInput) => {
    const state = 'pickup_at';
    this.handleSetDateState(selectedDay, modifiers, dayPickerInput, state);
  };
  handleSetDateState = (selectedDay, modifiers, dayPickerInput, state) => {
    const input = dayPickerInput.getInput();
    const selectedDateFromMoment = moment(selectedDay).format(APP.DATE_FORMAT);
    this.setState({
      [state]: selectedDateFromMoment,
      isEmpty: !input.value.trim(),
      isDisabled: modifiers.disabled === true,
    });
    sessionStorage.setItem(`deposit_${state}`, selectedDateFromMoment);
  };
  handleCheckBox = (e, { name }) => {
    this.setState({
      [name]: !this.state[name],
    });
  };
  formatDate = (date, format) => moment(date).format(format);
  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.setDateForImmdiateTransactions();
    sessionStorage.setItem(`deposit_${e.target.name}`, e.target.value);
  };
  handleInputAmount = e => {
    const { amount } = this.state;
    let formattedAmount = formatMoney(amount);
    if (amount > 9999999) {
      alert("Can't accept deposit more than 10,000,000.00");
      formattedAmount = formatMoney(0);
    }
    this.setState({ [e.target.name]: `$ ${formattedAmount}` });
  };
  onSelectText = e => {
    const formattedAmount = this.state.amount
      .replace(/,/g, '')
      .replace('$', '');
    this.setState({ [e.target.name]: formattedAmount });
  };
  onSubmitForm = e => {
    e.preventDefault();
    /* eslint-disable */
    let errors = [];
    const { payee, pay_at, pickup_at, amount, uploadedFiles } = this.state;
    if (amount.trim() === '' || amount === null) {
      errors.push('Amount is required.');
    }
    if (pay_at === '') {
      errors.push('Schedule payment date is required.');
    }
    if (pickup_at == '') {
      errors.push('Cash pickup date  is required.');
    }
    if (!payee) {
      errors.push('Payee is required.');
    }
    if (uploadedFiles.length === 0) {
      errors.push('Please upload at least 1 backup.');
    }

    if (errors.length > 0) {
      this.setState({ open: false, errorList: errors });
    } else {
      this.setState({ open: true, errorList: [] });
    }
    /* eslint-enable */
  };
  onImageDrop = files => {
    this.props.fileUpload(files);
    const { uploadedFiles } = this.state;
    this.setState({
      uploadedFiles: [...uploadedFiles, ...files],
    });
    this.setDateForImmdiateTransactions();
  };
  submitData = () => {
    const depositData = this.state;
    depositData.feeAmount = this.getFee();
    this.props.onSubmit(depositData);
  };
  disableUpDownArrow = e => {
    /* eslint-disable */
    const restrictedKeys = [
      16,17,18,19,20,33,34,36,
      38,40,45,65,66,67,68,69,
      70,71,72,73,74,75,76,77,
      78,79,80,81,82,83,84,85,
      86,87,88,89,90,91,92,93,
      94,95,106,107,109,111,186,
      187,189,191,192,219,220,221,222,
    ];
    if (restrictedKeys.indexOf(e.keyCode) > -1) {
      e.preventDefault();
    }
  };
  closeOpenModal = stateName => {
    this.setState({ [stateName]: !this.state[stateName] })
  };
  getTransactionFee = () => {
    const payAtData = this.state.pay_at;
    const pickUpAt = moment(payAtData, APP.DATE_TIME_FORMAT).format(
      APP.DATE_FORMAT,
    );
    const { currentUser } = this.props;
    this.props.getFeeTransaction({ userId: currentUser.id, date: payAtData });
  };
  getFee = () => {
    const { feeObject } = this.props.depositpage;
    const { transactionType, amount } = this.state;
    if (!feeObject)
      return 0;
    if (transactionType === 'ELECTRONIC')
      return 0;
    const origAmount = amount.replace(/,/g, '').replace('$', '');
    if (feeObject.fee_setting === 'flat_rate') {
      // get flat rate
      return parseFloat(origAmount) * parseFloat(feeObject.deposit_flat_rate);
    } else if (feeObject.fee_setting === 'fee_tier_v1') {
      return this.getComputedFee();
    } else if (feeObject.fee_setting === 'fee_tier_v2') {
      // get 2nd fee tier
      return this.getMinimumVolumeFee();
    }
  }
  getMinimumVolumeFee = () => {
    const { depositpage } = this.props;
    const { feeTierObject, transactionFee } = depositpage;
    const { amount } = this.state;
    let feeTotal = 0;
    if (transactionFee && feeTierObject) {
      const origAmount = amount.replace(/,/g, '').replace('$', '');
      const segregateAmountByTier = this.divideAmountOnMinimumVoulue(
        origAmount,
        transactionFee.total_transaction_amount,
      );
      /* eslint-disable */
      const belowMinimumTier = feeTierObject[0];
      const aboveMinimumTier = feeTierObject[1];

      const { belowMinimum, aboveMinimum } = segregateAmountByTier;

      const belowMinimumAmount = belowMinimumTier.deposit_rate * belowMinimum;
      const aboveMinimumAmount = aboveMinimumTier.deposit_rate * aboveMinimum;

      feeTotal = belowMinimumAmount + aboveMinimumAmount;
      /* eslint-enable */
      // const logData = {
      //   feeTotal,
      //   firstTierAmount,
      //   secondTierAmount,
      //   thirdTierAmount,
      //   firstTierFee,
      //   secondTierFee,
      //   thirdTierFee,
      //   transactionFee,
      //   feeTierObject,
      //   inputAmount: origAmount,
      // };
      // if (this.state.openConfirmModal) {
      //   this.props.sendLog(logData);
      // }
    }
    return feeTotal;
  };
  buildConfirmModal = () => {
    const {
      openConfirmModal,
      amount,
      transactionType,
      pickupType,
      isScheduled,
    } = this.state;
    // const pickuAtData = this.state.pickup_at;
    const payAtData = this.state.pay_at;
    // const pickUpAt = moment(pickuAtData, APP.DATE_TIME_FORMAT).format(
    //   APP.DATE_DISPLAY_FORMAT,
    // );
    const payAt = moment(payAtData, APP.DATE_TIME_FORMAT).format(
      APP.DATE_DISPLAY_FORMAT,
    );
    const origAmount = amount.replace(/,/g, '').replace('$', '');
    const feeAmount = this.getFee();
    const totalAmount = parseFloat(origAmount) - parseFloat(feeAmount);

    const { loading } = this.props.depositpage;
    return (
      <StandardModal
        headerText="Proceed this Request?"
        open={openConfirmModal}
        onClose={() => this.closeOpenModal('openConfirmModal')}
        onActionText="CONFIRM"
        onContinue={this.submitData}
        btnDisabled={loading}
      >
        <div style={{ textAlign: 'left' }}>
          <Header style={{ textAlign: 'center', marginBottom: 20 }}>
            Summary
          </Header>
          <Grid size="large" centered divided columns={2}>
            <Grid.Column textAlign="left">
              <Header as="h3">Amount</Header>
              <p className="confirmText">{amount}</p>
              <Header as="h3">Deposit Date</Header>
              <p className="confirmText">{payAt}</p>
              <Header as="h3">Pickup Date</Header>
              <p className="confirmText">
                Titan Officer will confirm cash pickup shortly.
              </p>
              <Header as="h3">Transaction Type</Header>
              <p className="confirmText">{transactionType}</p>
            </Grid.Column>
            <Grid.Column textAlign="left">
              <Header as="h3">Fee</Header>
              <p className="confirmText">$ {formatMoney(feeAmount)}</p>
              <Header as="h3">Net Deposit</Header>
              <p className="confirmText">$ {formatMoney(totalAmount)}</p>
              {transactionType === 'PAPER' ? (
                <p className="confirmText">
                  Final Fees will be calculated based on completed transactions
                  for the current period.
                </p>
              ) : (
                <p className="confirmText">
                  Fee is free for Electronic Transactions.
                </p>
              )}
              {isScheduled ? (
                <div>
                  <Header as="h3">Pickup Type</Header>
                  <p className="confirmText">{pickupType}</p>
                </div>
              ) : null}
            </Grid.Column>
          </Grid>
        </div>
      </StandardModal>
    );
  };
  enableSubmitButton = () => {
    /* eslint-disable */
    let errors = [];
    const { pay_at, pickup_at, amount, uploadedFiles } = this.state;
    if (amount.trim() === '' || amount === null || amount === 0 || amount === parseFloat(0).toFixed(2)) {
      errors.push('Amount is required.');
    }
    if (pay_at === '') {
      errors.push('Schedule payment date is required.');
    }
    if (pickup_at === '') {
      errors.push('Cash pickup date  is required.');
    }
    // if (uploadedFiles.length === 0) {
    //   errors.push('Please upload at least 1 backup.');
    // }
    return errors.length > 0;
    /* eslint-enable */
  };
  buildDepositAtDayPicker = (minPayDateMonth, minPayDate) => {
    const { isScheduled } = this.state;

    return isScheduled ? (
      <Form.Field>
        <h4 className="p-t-10 headerClass">
          Schedule Deposit
          <RequiredText>*</RequiredText>
        </h4>
        <DayPickerInput
          className="materialInput"
          value={this.state.pay_at}
          onDayChange={this.handleDayChangePayAt}
          placeholder={APP.DATE_FORMAT}
          format={APP.DATE_FORMAT}
          initialMonth={new Date(minPayDateMonth)}
          formatDate={this.formatDate}
          inputProps={{ readOnly: true }}
          dayPickerProps={{
            fromMonth: new Date(minPayDateMonth),
            modifiers: {
              disabled: [
                {
                  daysOfWeek: [0, 6],
                },
                {
                  before: new Date(minPayDate),
                },
              ],
            },
          }}
        />
        <HelpText>
          <FormattedMessage {...messages.depositDateText} />
        </HelpText>
        <div className="contentSeparator" />
      </Form.Field>
    ) : null;
  };
  buildPickUpDayPicker = (
    serverTimeMomentMonth,
    maxPickupMomentMonth,
    minPickupMomentMonth,
    minPickupDate,
    maxPickupDate,
  ) => {
    const { isScheduled } = this.state;
    return isScheduled ? (
      <Form.Field>
        <h4 className="p-t-10 headerClass">
          Cash Pickup
          <RequiredText>*</RequiredText>
        </h4>
        <DayPickerInput
          className="materialInput"
          value={this.state.pickup_at}
          onDayChange={this.handleDayChangePickUpAt}
          placeholder={APP.DATE_FORMAT}
          format={APP.DATE_FORMAT}
          initialMonth={new Date(minPickupMomentMonth)}
          formatDate={this.formatDate}
          inputProps={{
            readOnly: true,
            disabled: this.state.pay_at === '',
          }}
          dayPickerProps={{
            // month: new Date(minPickupMomentMonth),
            fromMonth: new Date(minPickupMomentMonth),
            toMonth: new Date(maxPickupMomentMonth),
            modifiers: {
              disabled: [
                {
                  daysOfWeek: [0, 6],
                },
                {
                  before: new Date(minPickupDate),
                },
                {
                  after: new Date(maxPickupDate),
                },
              ],
            },
          }}
        />
        <HelpText>
          <FormattedMessage {...messages.pickupDepositText} />
        </HelpText>
        <div className="contentSeparator" />
      </Form.Field>
    ) : null;
  };

  switchToggle = (e, type) => {
    const tablinks = document.querySelectorAll('.tablink');
    /* eslint-disable */
    for (let i = 0; i < tablinks.length; i++) {
      const tablink = tablinks[i];
      tablink.className = tablink.className.replace(' tab-active', '');
    }
    e.currentTarget.className += ' tab-active';
    /* eslint-enable */
    const isScheduled = type === 'Scheduled';
    // if (!isScheduled) {
    //   // const { currentDate } = this.state;
    //   this.setState({ isScheduled, pay_at: '', pickup_at: '' });
    // } else {
    this.setState({ isScheduled });
    this.setDateForImmdiateTransactions(isScheduled);
    // }
  };
  switchTransactionToggle = (e, type) => {
    const tablinks = document.querySelectorAll('.tablinksTransaction');
    /* eslint-disable */
    for (let i = 0; i < tablinks.length; i++) {
      const tablink = tablinks[i];
      tablink.className = tablink.className.replace(' tab-active', '');
    }
    /* eslint-enable */
    e.currentTarget.className += ' tab-active';
    this.setState({ transactionType: type });
  };
  switchPickupToggle = (e, type) => {
    const tablinks = document.querySelectorAll('.tablinksPickup');
    /* eslint-disable */
    for (let i = 0; i < tablinks.length; i++) {
      const tablink = tablinks[i];
      tablink.className = tablink.className.replace(' tab-active', '');
    }
    /* eslint-enable */
    e.currentTarget.className += ' tab-active';
    this.setState({ pickupType: type });
  };
  divideAmountOnMinimumVoulue = (amount, totalDeposit) => {
    const { feeTierObject } = this.props.depositpage;
    const firstFeeTier = feeTierObject[0];
    const secondFeeTier = feeTierObject[1];
    let belowMinimum = 0;
    let aboveMinimum = 0;
    const amountParsed = parseFloat(amount);
    const totalDepositParsed = parseFloat(totalDeposit);
    const totalAmount = amountParsed + totalDepositParsed;
    if (totalDepositParsed > 0) {
      if (totalDepositParsed <= firstFeeTier.deposit_threshold) {
        if (totalAmount > firstFeeTier.deposit_threshold) {
          belowMinimum = firstFeeTier.deposit_threshold - totalDepositParsed;
          aboveMinimum = amountParsed - belowMinimum;
        } else {
          aboveMinimum = amountParsed;
        }
      } else {
        aboveMinimum = amountParsed;
      }
      return { belowMinimum, aboveMinimum };
    }
    if (amountParsed <= firstFeeTier.deposit_threshold) {
      belowMinimum = amountParsed;
    } else if (amountParsed > secondFeeTier.deposit_threshold) {
      belowMinimum = firstFeeTier.deposit_threshold;
      aboveMinimum = amountParsed - belowMinimum;
    }
    return { belowMinimum, aboveMinimum };
  };
  divideAmountOnTier = (amount, totalDeposit) => {
    const { feeTierObject } = this.props.depositpage;
    const firstFeeTier = feeTierObject[0];
    const secondFeeTier = feeTierObject[1];
    /* eslint-disable */
    const thirdfeeTier = feeTierObject[2];
    /* eslint-enable */
    let firstTierAmount = 0;
    let secondTierAmount = 0;
    let thirdTierAmount = 0;
    const amountParsed = parseFloat(amount);
    const totalDepositParsed = parseFloat(totalDeposit);
    const totalAmount = amountParsed + totalDepositParsed;
    if (totalDepositParsed > 0) {
      if (totalDepositParsed <= firstFeeTier.deposit_threshold) {
        if (totalAmount > secondFeeTier.deposit_threshold) {
          firstTierAmount = firstFeeTier.deposit_threshold - totalDepositParsed;
          secondTierAmount =
            secondFeeTier.deposit_threshold - firstFeeTier.deposit_threshold;
          thirdTierAmount = amountParsed - firstTierAmount - secondTierAmount;
        } else if (totalAmount > firstFeeTier.deposit_threshold) {
          firstTierAmount = firstFeeTier.deposit_threshold - totalDepositParsed;
          secondTierAmount = amountParsed - firstTierAmount;
        } else {
          firstTierAmount = amountParsed;
        }
      } else if (totalDepositParsed <= secondFeeTier.deposit_threshold) {
        if (totalAmount > secondFeeTier.deposit_threshold) {
          secondTierAmount =
            secondFeeTier.deposit_threshold - totalDepositParsed;
          thirdTierAmount = amountParsed - secondTierAmount;
        } else {
          secondTierAmount = amountParsed;
        }
      } else if (totalDepositParsed > secondFeeTier.deposit_threshold) {
        thirdTierAmount = amountParsed;
      }
      return { firstTierAmount, secondTierAmount, thirdTierAmount };
    }
    if (amountParsed <= firstFeeTier.deposit_threshold) {
      firstTierAmount = amountParsed;
    } else if (amountParsed <= secondFeeTier.deposit_threshold) {
      firstTierAmount = firstFeeTier.deposit_threshold;
      secondTierAmount = amountParsed - firstTierAmount;
    } else if (amountParsed > secondFeeTier.deposit_threshold) {
      firstTierAmount = firstFeeTier.deposit_threshold; // 600, 000
      secondTierAmount = secondFeeTier.deposit_threshold - firstTierAmount;
      thirdTierAmount = amountParsed - secondFeeTier.deposit_threshold; // 400, 000
    }
    return { firstTierAmount, secondTierAmount, thirdTierAmount };
  };
  getComputedFee = () => {
    const { depositpage } = this.props;
    const { feeTierObject, transactionFee } = depositpage;
    const { amount } = this.state;
    let feeTotal = 0;
    if (transactionFee && feeTierObject) {
      const origAmount = amount.replace(/,/g, '').replace('$', '');
      const segregateAmountByTier = this.divideAmountOnTier(
        origAmount,
        transactionFee.total_transaction_amount,
      );
      /* eslint-disable */
      const firstFeeTier = feeTierObject[0];
      const secondFeeTier = feeTierObject[1];
      const thirdFeeTier = feeTierObject[2];

      const { firstTierAmount, secondTierAmount, thirdTierAmount } = segregateAmountByTier;

      const firstTierFee = firstFeeTier.deposit_rate * firstTierAmount;
      const secondTierFee = secondFeeTier.deposit_rate * secondTierAmount;
      const thirdTierFee = thirdFeeTier.deposit_rate * thirdTierAmount;

      feeTotal = firstTierFee + secondTierFee + thirdTierFee;
      /* eslint-enable */
      const logData = {
        feeTotal,
        firstTierAmount,
        secondTierAmount,
        thirdTierAmount,
        firstTierFee,
        secondTierFee,
        thirdTierFee,
        transactionFee,
        feeTierObject,
        inputAmount: origAmount,
      };
      if (this.state.openConfirmModal) {
        this.props.sendLog(logData);
      }
    }
    return feeTotal;
  };
  getFileSrc = file => {
    let src = null;
    if (file.type === 'image/jpeg' || file.type === 'image/png') {
      src = file.preview;
    } else if (file.type === 'application/pdf') {
      src = pdfImage;
    } else {
      src = fileIcon;
    }
    return src;
  };
  buildCancelModal = () => (
    <StandardModal
      headerText="Cancel Request"
      open={this.state.cancelModal}
      onClose={() => {
        this.setState({ cancelModal: false });
      }}
      onActionText="Cancel"
      onContinue={() => {
        sessionStorage.removeItem('deposit_images');
        this.setState({ cancelModal: false });
        this.props.history.goBack();
      }}
    >
      <div style={{ textAlign: 'center' }}>
        Cancel this request? By clicking confirm, the changes will be discarded.
      </div>
    </StandardModal>
  );
  cancelRequest = () => {
    this.setState({ cancelModal: true });
  };
  render() {
    const { remarks, serverTime } = this.state;
    const { loading } = this.props.depositpage;
    const serverTimeMoment = moment(serverTime).format(APP.DATE_FORMAT);
    const serverTimeHourMoment = moment(serverTime).format('H');
    const daysForward = serverTimeHourMoment > 14 ? 8 : 7;
    const minPayDate = getDateForward(daysForward, serverTimeMoment);
    const minPayDateMonth = moment(minPayDate).format(APP.DATE_MONTH_FORMAT);

    const serverTimeMomentMonth = moment(serverTime).format(
      APP.DATE_MONTH_FORMAT,
    );
    const { maxPickupDate, minPickupDate } = this.handleUpdatePickupAtValues();
    const minPickupMomentMonth = moment(minPickupDate).format(
      APP.DATE_MONTH_FORMAT,
    );
    const maxPickupMomentMonth = moment(maxPickupDate).format(
      APP.DATE_MONTH_FORMAT,
    );

    const previewStyle = {
      display: 'inline',
      width: 70,
      height: 70,
      padding: 5,
    };
    const previewSpan = {
      display: 'inline',
    };
    const closeButton = {
      position: 'absolute',
      width: '30px',
      height: '30px',
      objectFit: 'cover',
    };
    const enableSubmit = this.enableSubmitButton();
    return (
      <ContainerWrapper>
        {this.buildConfirmModal()}
        {this.buildCancelModal()}
        <div className="row">
          <div className="pull-right" style={{ marginRight: 50 }}>
            <Button circular onClick={() => this.cancelRequest()}>
              X
            </Button>
          </div>
          <div className="col-lg-2 col-md-3 col-sm-3" />
          <div className=" col-lg-8 col-md-6 col-sm-6 col-xs-12">
            <div className="card-box service-mode m-t-30 m-b-30 p-l-r-30 content-box contentBox">
              <Form size="huge" autoComplete="off" loading={loading}>
                <Form.Field>
                  <div className="service-mode-button w3-bar w3-black">
                    <button
                      className="w3-bar-item w3-button tablinksTransaction tab-active left-tab"
                      onClick={e => this.switchTransactionToggle(e, 'PAPER')}
                    >
                      Paper
                    </button>
                    <button
                      className="w3-bar-item w3-button tablinksTransaction right-tab"
                      onClick={e =>
                        this.switchTransactionToggle(e, 'ELECTRONIC')
                      }
                    >
                      Electronic
                    </button>
                  </div>
                </Form.Field>
                <Form.Field>
                  <h4 className="p-t-10 headerClass">
                    Enter Amount
                    <RequiredText>*</RequiredText>
                  </h4>
                  <input
                    className="materialInput"
                    type="text"
                    step="0.00"
                    min="0"
                    name="amount"
                    id="amount"
                    value={this.state.amount}
                    placeholder="Amount"
                    autoComplete="off"
                    onChange={this.handleInput}
                    onSelect={this.onSelectText}
                    onBlur={this.handleInputAmount}
                    onKeyDown={e => this.disableUpDownArrow(e)}
                  />
                  <div className="contentSeparator" />
                </Form.Field>
                <Form.Field>
                  <div className="service-mode-button w3-bar w3-black">
                    <button
                      className="w3-bar-item w3-button tablink tab-active left-tab"
                      onClick={e => this.switchToggle(e, 'Immediate')}
                    >
                      Immediate
                    </button>
                    <button
                      className="w3-bar-item w3-button tablink right-tab"
                      onClick={e => this.switchToggle(e, 'Scheduled')}
                    >
                      Scheduled
                    </button>
                  </div>
                </Form.Field>
                <Form.Field>
                  <h4 className="p-t-10 headerClass">
                    Upload Backup
                    <RequiredText>*</RequiredText>
                  </h4>
                  <div className="uploadFile">
                    {this.props.depositpage.filesLoading ? (
                      <Dimmer active inverted>
                        <Loader size="massive">Loading</Loader>
                      </Dimmer>
                    ) : null}
                    <Dropzone
                      className=" col-lg-8 col-md-6 col-sm-6 col-xs-12 dropZone"
                      onDrop={this.onImageDrop}
                      multiple
                      accept="image/*,application/pdf,
                      application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel,
                      application/doc,
                      application/ms-doc,
                      application/msword,
                      application/vnd.openxmlformats-officedocument.wordprocessingml.document,
                      application/vnd.ms-excel,
                      .csv
                      "
                    >
                      <p>Drop your files or click here to upload</p>
                      {/* eslint-disable */
                        this.state.uploadedFiles.length > 0 && (
                        <div>
                          {this.state.uploadedFiles.map((file, key) => (
                            <span key={key} style={previewSpan}>
                              <a onClick={(e) => {
                                e.preventDefault();
                                e.stopPropagation();
                                this.props.removeFile(file.id);
                              }
                              }>
                                <img
                                alt=""
                                style={closeButton}
                                key={key}
                                src={closeImage}
                                />
                              </a>
                              <img
                                alt="Preview"
                                key={key}
                                src={this.getFileSrc(file)}
                                style={previewStyle}
                              />
                              <b style={{fontSize: 10}}>{file.name}</b>
                            </span>
                          ))}
                        </div>
                      )
                        /* eslint-enable */}
                    </Dropzone>
                  </div>
                  <div className="clearfix" />
                  <div className="contentSeparator" />
                </Form.Field>
                {this.buildDepositAtDayPicker(minPayDateMonth, minPayDate)}
                {/*eslint-disable*/
                    this.state.pay_at !== ''
                      ? this.buildPickUpDayPicker(
                          serverTimeMomentMonth,
                          maxPickupMomentMonth,
                          minPickupMomentMonth,
                          minPickupDate,
                          maxPickupDate,
                        )
                          : null
                /* eslint-enable */
                }
                {this.state.isScheduled && this.state.pay_at !== '' ? (
                  <Form.Field>
                    <div className="service-mode-button w3-bar w3-black">
                      <button
                        className="w3-bar-item w3-button tablinksPickup tab-active left-tab"
                        onClick={e => this.switchPickupToggle(e, 'RECURRING')}
                      >
                        Recurring
                      </button>
                      <button
                        className="w3-bar-item w3-button tablinksPickup right-tab"
                        onClick={e => this.switchPickupToggle(e, 'ON-DEMAND')}
                      >
                        On-demand
                      </button>
                    </div>
                  </Form.Field>
                ) : null}
                <Form.Field>
                  <h4 className="p-t-10 headerClass">Memo</h4>
                  <TextArea
                    className="materialInput"
                    name="remarks"
                    onChange={this.handleInput}
                    value={remarks}
                    rows={3}
                    placeholder="Note to your payee."
                  />
                  <div className="contentSeparator" />
                </Form.Field>
                <Button
                  className="buttonSubmit"
                  fluid
                  size="massive"
                  color="orange"
                  disabled={enableSubmit}
                  onClick={() => {
                    this.onBlurPickupAt();
                    this.closeOpenModal('openConfirmModal');
                  }}
                >
                  Submit
                </Button>
                <Button
                  className="buttonSubmitCancel"
                  fluid
                  size="massive"
                  onClick={() => this.cancelRequest()}
                >
                  Cancel
                </Button>
              </Form>
            </div>
          </div>
        </div>
      </ContainerWrapper>
    );
  }
}

DepositPage.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  fileUpload: PropTypes.func.isRequired,
  getTime: PropTypes.func.isRequired,
  depositpage: PropTypes.any.isRequired,
  history: PropTypes.any.isRequired,
  getDepositAccount: PropTypes.func.isRequired,
  getFeeTransaction: PropTypes.func.isRequired,
  payeeDepositAccount: PropTypes.object,
  currentUser: PropTypes.object,
  feeObject: PropTypes.object,
  getUserFeeRate: PropTypes.func.isRequired,
  getUserFeeTierRate: PropTypes.func.isRequired,
  sendLog: PropTypes.func.isRequired,
  computeFee: PropTypes.func.isRequired,
  removeFile: PropTypes.func.isRequired,
  // onReset: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  depositpage: makeSelectDepositPage(),
  payeeDepositAccount: makeSelectPayeeDepositAccount(),
  currentUser: makeSelectCurrentUser(),
  // feeObject: makeSelectFeeObject(),
});

const mapDispatchToProps = {
  fileUpload,
  getTime,
  onSubmit,
  getDepositAccount,
  getFeeTransaction,
  getUserFeeRate,
  getUserFeeTierRate,
  sendLog,
  computeFee,
  removeFile,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'depositPage', reducer });
const withSaga = injectSaga({ key: 'depositPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(DepositPage);
