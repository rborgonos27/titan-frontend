import { fromJS } from 'immutable';
import depositPageReducer from '../reducer';

describe('depositPageReducer', () => {
  it('returns the initial state', () => {
    expect(depositPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
