import { getTierAmount, getTier } from '../computation';
export const tierObjects = [
    {
        deposit_rate: 0.0440,
        deposit_threshold: 400000,
        id: 739,
        payment_rate: 0.0490,
        payment_threshold: 500000,
        tier_no: 1,
    },
    {
        deposit_rate: 0.0360,
        deposit_threshold: 800000,
        id: 740,
        payment_rate: 0.0390,
        payment_threshold: 1000000,
        tier_no: 2,
    },
    {
        deposit_rate: 0.0290,
        deposit_threshold: null,
        id: 741,
        payment_rate: 0.0290,
        payment_threshold: null,
        tier_no: 3,
    },
];

test('get_tier_input_450000', () => {
    const totalTransactionAmount = 450000;
    const currentTier = getTier(tierObjects, totalTransactionAmount);
    expect(currentTier.tier_no).toBe(2);
});

test('get_tier_input_390000', () => {
    const totalTransactionAmount = 390000;
    const currentTier = getTier(tierObjects, totalTransactionAmount);
    expect(currentTier.tier_no).toBe(1);
});

test('get_tier_input_500000', () => {
    const totalTransactionAmount = 500000;
    const currentTier = getTier(tierObjects, totalTransactionAmount);
    expect(currentTier.tier_no).toBe(2);
});

test('get_tier_input_400000', () => {
    const totalTransactionAmount = 400000;
    const currentTier = getTier(tierObjects, totalTransactionAmount);
    expect(currentTier.tier_no).toBe(1);
});

test('get_tier_amount_has_current_amount', () => {
    const depositAmount = 300000;
    const currentAmount = 300000;
    const tieredAmount = getTierAmount(tierObjects, depositAmount, currentAmount);
    expect(tieredAmount.length).toBe(2);
    expect(tieredAmount[0].amount).toBe(100000);
    expect(tieredAmount[1].amount).toBe(200000);
    console.log(tieredAmount);
});

test('get_tier_amount_has_current_amount_should_compute_to_last_tier', () => {
    const depositAmount = 300000;
    const currentAmount = 1300000;
    const tieredAmount = getTierAmount(tierObjects, depositAmount, currentAmount);
    expect(tieredAmount.length).toBe(3);
    expect(tieredAmount[0].amount).toBe(100000);
    expect(tieredAmount[1].amount).toBe(400000);
    expect(tieredAmount[2].amount).toBe(900000);
    console.log(tieredAmount);
});

test('get_tier_amount_no_current_amount', () => {
    const depositAmount = 300000;
    const currentAmount = 0;
    const tieredAmount = getTierAmount(tierObjects, depositAmount, currentAmount);
    console.log(tieredAmount);
    expect(tieredAmount.length).toBe(1);
    expect(tieredAmount[0].amount).toBe(300000);
});

test('get_tier_amount_has_current_amount_should_move_to_last_tier', () => {
    const depositAmount = 1200000;
    const currentAmount = 200000;
    const tieredAmount = getTierAmount(tierObjects, depositAmount, currentAmount);
    console.log(tieredAmount);
    expect(tieredAmount.length).toBe(3);
    expect(tieredAmount[0].amount).toBe(200000);
    expect(tieredAmount[1].amount).toBe(300000);
    expect(tieredAmount[2].amount).toBe(700000);
});
