import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the depositPage state domain
 */

const selectDepositPageDomain = state => state.get('depositPage', initialState);
const selectGlobal = state => state.get('global');

/**
 * Other specific selectors
 */

/**
 * Default selector used by DepositPage
 */

const makeSelectDepositPage = () =>
  createSelector(selectDepositPageDomain, substate => substate.toJS());

const makeSelectPayeeDepositAccount = () =>
  createSelector(selectDepositPageDomain, state =>
    state.get('payeeDepositAccount'),
  );

const makeSelectCurrentUser = () =>
  createSelector(selectGlobal, globalState => globalState.get('userData'));

const makeSelectFeeObject = () =>
  createSelector(selectGlobal, globalState => globalState.get('feeObject'));

const makeSelectFeeTierObject = () =>
  createSelector(selectGlobal, globalState => globalState.get('feeTierObject'));

export default makeSelectDepositPage;
export {
  selectDepositPageDomain,
  makeSelectPayeeDepositAccount,
  makeSelectCurrentUser,
  makeSelectFeeObject,
  makeSelectFeeTierObject,
};
