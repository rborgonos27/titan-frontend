import { all, call, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import ReactGA from 'react-ga';
import request from 'utils/request';
import { APP, GA } from 'config';
import moment from 'moment';
import {
  GET_TIME,
  FILE_UPLOAD,
  ON_SUBMIT,
  GET_DEPOSIT_ACCOUNT,
  GET_FEE_TRANSACTION,
  GET_USER_FEE_RATE,
  GET_USER_FEE_TIER_RATE,
  SEND_LOG,
  COMPUTE_FEE,
  REMOVE_FILE,
} from './constants';
import {
  getTimeSuccess,
  getTimeError,
  onSubmitSuccess,
  onSubmitError,
  getDepositAccountSuccess,
  getDepositAccountError,
  getFeeTransactionError,
  getFeeTransactionSuccess,
  getUserFeeRateSuccess,
  getUserFeeRateError,
  getUserFeeTierRateSuccess,
  getUserFeeTierRateError,
  computeFeeError,
  computeFeeSuccess,
  fileUploadSuccess,
  removeFileError,
  removeFileSuccess,
  sendLogError,
} from './actions';
import { optionsFormData } from '../../utils/options';

export function* getTime() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const serverTime = yield call(
      request,
      `${API_URI}/time/`,
      optionsFormData('GET', null, token),
    );
    if (serverTime.current_time) {
      yield put(getTimeSuccess(serverTime));
    } else {
      yield put(getTimeError(serverTime));
    }
  } catch (error) {
    yield put(getTimeError(error));
  }
}

export function* submit(data) {
  const API_URI = APP.API_URL;
  ReactGA.initialize(GA.TRACKING_ID);
  try {
    const formData = new FormData();
    const amount = data.data.amount.replace(/,/g, '').replace('$', '');
    formData.append('type', 'DEPOSIT');
    formData.append('to', 'Anywhere');
    formData.append('amount', amount);
    formData.append('fee', data.data.feeAmount.toFixed(2));
    formData.append('due_at', `${data.data.pay_at} 00:00`);
    formData.append('pickup_at', `${data.data.pickup_at} 00:00`);
    formData.append('remarks', data.data.remarks);
    formData.append('payee_id', data.data.payee.id);
    formData.append('transaction_type', data.data.transactionType);
    formData.append('pickup_type', data.data.pickupType);
    const token = yield JSON.parse(localStorage.getItem('token'));
    ReactGA.event({
      category: 'Request',
      action: `Creating Deposit Request`,
      label: 'Deposit Request',
    });
    const depositResponse = yield call(
      request,
      `${API_URI}/request/`,
      optionsFormData('POST', formData, token),
    );
    if (depositResponse.success) {
      // remove session
      ReactGA.event({
        category: 'Request',
        action: `Deposit Request Created`,
        label: 'Deposit Request Success',
      });
      // yield sessionStorage.removeItem('deposit_amount');
      // yield sessionStorage.removeItem('deposit_payee');
      yield sessionStorage.removeItem('deposit_images');
      // yield sessionStorage.removeItem('deposit_pay_at');
      // yield sessionStorage.removeItem('deposit_pickup_at');
      // yield sessionStorage.removeItem('deposit_remarks');
      const formDataNotif = new FormData();
      formDataNotif.append(
        'name',
        `${data.data.currentUser.first_name} ${
          data.data.currentUser.last_name
        }`,
      );
      formDataNotif.append('amount', data.data.amount);
      formDataNotif.append('type', 'DEPOSIT');
      formDataNotif.append(
        'date',
        moment(data.data.pay_at).format(APP.DATE_DISPLAY_FORMAT),
      );
      formDataNotif.append('request_id', depositResponse.data.id);
      yield call(
        request,
        `${API_URI}/new/request/notif`,
        optionsFormData('POST', formDataNotif, token),
      );
      yield put(onSubmitSuccess(depositResponse.data));
      yield put(push('/'));
    }
  } catch (error) {
    ReactGA.event({
      category: 'Request',
      action: `Deposit Request Failed ${JSON.stringify(error)}`,
      label: 'Deposit Request Error',
    });
    yield put(onSubmitError({ error }));
  }
}

export function* fileUpload(file) {
  try {
    const API_URI = APP.API_URL;
    ReactGA.initialize(GA.TRACKING_ID);
    /* eslint-disable */
    for (const i in file.data) {
      const fileData = file.data[i];
      const token = yield JSON.parse(localStorage.getItem('token'));
      const formData = new FormData();
      formData.append('filename', fileData);
      formData.append('url', 'filename');
      formData.append('mime_type', fileData.type);
      formData.append('type', 'DEPOSIT');
      formData.append('remarks', '');
      formData.append(
        'expire_at',
        moment()
          .add(1, 'hours')
          .format('YYYY-MM-DD h:m:s'),
      );
      ReactGA.event({
        category: 'Request',
        action: `Upload backup file`,
        label: 'Deposit Request Upload Backup',
      });
      const fileResponse = yield call(
        request,
        `${API_URI}/file/`,
        optionsFormData('POST', formData, token),
      );
      if (fileResponse.filename) {
        let fileStoredSession = yield JSON.parse(
          sessionStorage.getItem('deposit_images'),
        );
        if (!fileStoredSession) {
          fileStoredSession = [];
        }
        const fileSession = {
          id: fileResponse.id,
          preview: fileResponse.filename,
          type: fileResponse.mime_type,
          name: fileData.name,
        };
        ReactGA.event({
          category: 'Request',
          action: `Upload backup file success`,
          label: 'Deposit Request Upload Backup Success',
        });
        fileStoredSession.push(fileSession);
        yield sessionStorage.setItem(
          'deposit_images',
          JSON.stringify(fileStoredSession),
        );
        yield put(fileUploadSuccess([]));
      } else {
        ReactGA.event({
          category: 'Request',
          action: `Upload backup file failed`,
          label: 'Deposit Request Upload Backup Failed',
        });
      }
    }
    /* eslint-enable */
  } catch (error) {
    ReactGA.event({
      category: 'Request',
      action: `Upload backup file failed ${error}`,
      label: 'Deposit Request Upload Backup Failed',
    });
  }
}

export function* getDepositAccount() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const payeeDepositAccount = yield call(
      request,
      `${API_URI}/payee_deposit_account/`,
      optionsFormData('GET', null, token),
    );
    if (payeeDepositAccount.id) {
      yield put(getDepositAccountSuccess(payeeDepositAccount));
    } else {
      yield put(getDepositAccountError(payeeDepositAccount));
    }
  } catch (error) {
    yield put(getDepositAccountError(error));
  }
}

export function* getFeeTransaction(feeData) {
  try {
    const { userId, date } = feeData.data;
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const dateparam = date ? `&date=${date}` : ``;
    const feeTransaction = yield call(
      request,
      `${API_URI}/fee_transaction_user/?user_id=${userId}&type=deposit${dateparam}`,
      optionsFormData('GET', null, token),
    );
    if (feeTransaction.id) {
      yield put(getFeeTransactionSuccess(feeTransaction));
    }
    if (feeTransaction.message) {
      const transaction = {
        id: null,
        fee_transaction_id: null,
        message: feeTransaction.message,
        type: 'deposit',
        fee_percent: 0,
        transaction_amount: 0,
        total_transaction_amount: 0,
        fee_amount: 0,
      };
      yield put(getFeeTransactionSuccess(transaction));
    }
  } catch (error) {
    yield put(getFeeTransactionError(error));
  }
}

export function* getUserFeeRate(user) {
  try {
    const API_URI = APP.API_URL;
    const { data } = user;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const feeRate = yield call(
      request,
      `${API_URI}/fee/?user_id=${data}`,
      optionsFormData('GET', null, token),
    );
    if (feeRate.results.length > 0) {
      yield put(getUserFeeRateSuccess(feeRate.results[0]));
    } else {
      yield put(getUserFeeRateError(feeRate));
    }
  } catch (error) {
    yield put(getUserFeeRateError(error));
  }
}

export function* getUserFeeTierRate(user) {
  try {
    const API_URI = APP.API_URL;
    const { data } = user;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const feeRate = yield call(
      request,
      `${API_URI}/fee_tier/?user_id=${data}`,
      optionsFormData('GET', null, token),
    );
    if (feeRate.results.length > 0) {
      yield put(getUserFeeTierRateSuccess(feeRate.results));
    } else {
      yield put(getUserFeeTierRateError(feeRate));
    }
  } catch (error) {
    yield put(getUserFeeTierRateError(error));
  }
}

export function* sendLog(data) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const formData = new FormData();
    const {
      feeTotal,
      firstTierAmount,
      secondTierAmount,
      thirdTierAmount,
      firstTierFee,
      secondTierFee,
      thirdTierFee,
      transactionFee,
      feeTierObject,
      inputAmount,
    } = data.data;

    const firstFeeTier = feeTierObject[0];
    const secondFeeTier = feeTierObject[1];
    const thirdFeeTier = feeTierObject[2];
    formData.append('type', 'DEPOSIT');
    formData.append('transaction_amount', feeTotal);
    formData.append(
      'total_transaction_amount',
      transactionFee.total_transaction_amount,
    );

    formData.append('input_amount', inputAmount);
    formData.append('first_tier_amount', firstTierAmount);
    formData.append('second_tier_amount', secondTierAmount);
    formData.append('third_tier_amount', thirdTierAmount);

    formData.append('first_tier_fee', firstTierFee);
    formData.append('second_tier_fee', secondTierFee);
    formData.append('third_tier_fee', thirdTierFee);

    formData.append('first_tier_rate', firstFeeTier.deposit_rate);
    formData.append('second_tier_rate', secondFeeTier.deposit_rate);
    formData.append('third_tier_rate', thirdFeeTier.deposit_rate);

    formData.append('first_tier_threshold', firstFeeTier.deposit_threshold);
    formData.append('second_tier_threshold', secondFeeTier.deposit_threshold);
    formData.append('third_tier_threshold', thirdFeeTier.deposit_threshold);
    yield call(
      request,
      `${API_URI}/log/`,
      optionsFormData('POST', formData, token),
    );
  } catch (error) {
    yield put(sendLogError(error));
  }
}

export function* computeFee(fee) {
  try {
    const API_URI = APP.API_URL;
    const { userId, date, amount } = fee.data;
    const amountParsed = amount.replace(/,/g, '').replace('$', '');
    const token = yield JSON.parse(localStorage.getItem('token'));
    const formData = new FormData();
    formData.append('user_id', userId);
    formData.append('type', 'deposit');
    formData.append('date', date);
    formData.append('amount', amountParsed);
    const computedFee = yield call(
      request,
      `${API_URI}/compute_fee/`,
      optionsFormData('POST', formData, token),
    );
    if (computedFee.fee) {
      yield put(computeFeeSuccess(computedFee.fee));
    } else {
      yield put(computeFeeError(computedFee));
    }
  } catch (error) {
    yield put(computeFeeError(error));
  }
}

export function* removeFile(data) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    /* eslint-disable */
    const removeFileResponse = yield call(
      request,
      `${API_URI}/user_file/${data.data}/`,
      optionsFormData('DELETE', null, token),
    );
    /* eslint-enable */
    let fileStoredSession = yield JSON.parse(
      sessionStorage.getItem('deposit_images'),
    );
    if (!fileStoredSession) {
      fileStoredSession = [];
    }
    ReactGA.event({
      category: 'Request',
      action: `remove backup file success`,
      label: 'Payment Request Remove Backup Success',
    });
    if (fileStoredSession.length === 1) {
      fileStoredSession = [];
    } else {
      fileStoredSession = removeFileOnArray(fileStoredSession, data.data);
    }
    yield sessionStorage.setItem(
      'deposit_images',
      JSON.stringify(fileStoredSession),
    );
    yield put(removeFileSuccess([]));
  } catch (error) {
    yield put(removeFileError(error));
  }
}

export function removeFileOnArray(files, id) {
  /* eslint-disable */
  for(let i = 0; i < files.length; i ++) {
    const file = files[i];
    if (file.id === id) {
      files.splice(i, 1);
    }
  };
  return files;
}

export function* getTimeWatcher() {
  yield takeLatest(GET_TIME, getTime);
}

export function* fileUploadWatcher() {
  yield takeLatest(FILE_UPLOAD, fileUpload);
}
export function* submitWatcher() {
  yield takeLatest(ON_SUBMIT, submit);
}

export function* getDepositAccountWatcher() {
  yield takeLatest(GET_DEPOSIT_ACCOUNT, getDepositAccount);
}

export function* getFeeTransactionWatcher() {
  yield takeLatest(GET_FEE_TRANSACTION, getFeeTransaction);
}

export function* getUserFeeRateWatcher() {
  yield takeLatest(GET_USER_FEE_RATE, getUserFeeRate);
}

export function* getUserFeeTierRateWatcher() {
  yield takeLatest(GET_USER_FEE_TIER_RATE, getUserFeeTierRate);
}

export function* sendLogWatcher() {
  yield takeLatest(SEND_LOG, sendLog);
}

export function* computeFeeWatcher() {
  yield takeLatest(COMPUTE_FEE, computeFee);
}

export function* removeFileWatcher() {
  yield takeLatest(REMOVE_FILE, removeFile);
}

export default function* rootSaga() {
  yield all([
    getTimeWatcher(),
    fileUploadWatcher(),
    submitWatcher(),
    getDepositAccountWatcher(),
    getFeeTransactionWatcher(),
    getUserFeeRateWatcher(),
    getUserFeeTierRateWatcher(),
    sendLogWatcher(),
    computeFeeWatcher(),
    removeFileWatcher(),
  ]);
}
