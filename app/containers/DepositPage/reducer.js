/*
 *
 * DepositPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  ON_SUBMIT,
  ON_SUBMIT_SUCCESS,
  ON_SUBMIT_ERROR,
  FILE_UPLOAD,
  FILE_UPLOAD_SUCCESS,
  FILE_UPLOAD_ERROR,
  GET_TIME,
  GET_TIME_SUCCESS,
  GET_TIME_ERROR,
  ON_RESET,
  GET_DEPOSIT_ACCOUNT,
  GET_DEPOSIT_ACCOUNT_SUCCESS,
  GET_DEPOSIT_ACCOUNT_ERROR,
  GET_FEE_TRANSACTION,
  GET_FEE_TRANSACTION_SUCCESS,
  GET_FEE_TRANSACTION_ERROR,
  GET_USER_FEE_RATE,
  GET_USER_FEE_RATE_SUCCESS,
  GET_USER_FEE_RATE_ERROR,
  GET_USER_FEE_TIER_RATE,
  GET_USER_FEE_TIER_RATE_SUCCESS,
  GET_USER_FEE_TIER_RATE_ERROR,
  COMPUTE_FEE,
  COMPUTE_FEE_ERROR,
  COMPUTE_FEE_SUCCESS,
  REMOVE_FILE,
  REMOVE_FILE_SUCCESS,
  REMOVE_FILE_ERROR,
} from './constants';

export const initialState = fromJS({
  loading: false,
  data: null,
  error: null,
  errorList: [],
  filesLoading: false,
  files: [],
  currentTime: null,
  option: null,
  payeeLoaded: false,
  payeeDepositAccount: null,
  feeTransaction: null,
  feeObject: null,
  feeTierObject: null,
  computedFee: null,
  isUploaded: false,
});

function depositPageReducer(state = initialState, action) {
  switch (action.type) {
    case ON_SUBMIT:
      return state
        .set('loading', true)
        .set('error', null)
        .set('errorList', []);
    case ON_SUBMIT_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', []);
    case ON_SUBMIT_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('errorList', []);
    case FILE_UPLOAD:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', [])
        .set('filesLoading', true)
        .set('isUploaded', false);
    case FILE_UPLOAD_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', action.data)
        .set('filesLoading', false)
        .set('isUploaded', true);
    case FILE_UPLOAD_ERROR:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', [])
        .set('filesError', action.data.error)
        .set('filesLoading', false)
        .set('isUploaded', false);
    case REMOVE_FILE:
      return state
        .set('filesLoading', true)
        .set('isUploaded', false)
        .set('error', null)
        .set('files', null);
    case REMOVE_FILE_SUCCESS:
      return state
        .set('filesLoading', false)
        .set('isUploaded', true)
        .set('error', null)
        .set('files', action.data);
    case REMOVE_FILE_ERROR:
      return state
        .set('filesLoading', false)
        .set('isUploaded', false)
        .set('error', action.error)
        .set('files', null);
    case GET_TIME:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', [])
        .set('filesError', null)
        .set('filesLoading', false)
        .set('currentTime', null);
    case GET_TIME_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', [])
        .set('filesError', null)
        .set('filesLoading', false)
        .set('currentTime', action.data.current_time);
    case GET_TIME_ERROR:
      return state
        .set('loading', false)
        .set('error', 'Error getting time.')
        .set('errorList', [])
        .set('files', [])
        .set('filesError', null)
        .set('filesLoading', false)
        .set('currentTime', null);
    case ON_RESET:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', []);
    case GET_DEPOSIT_ACCOUNT:
      return state
        .set('loading', true)
        .set('error', null)
        .set('payeeDepositAccount', null);
    case GET_DEPOSIT_ACCOUNT_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('payeeDepositAccount', action.data);
    case GET_DEPOSIT_ACCOUNT_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('payeeDepositAccount', null);
    case GET_FEE_TRANSACTION:
      return state
        .set('loading', true)
        .set('error', null)
        .set('transactionFee', null);
    case GET_FEE_TRANSACTION_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('transactionFee', action.data);
    case GET_FEE_TRANSACTION_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('transactionFee', null);
    case GET_USER_FEE_RATE:
      return state
        .set('loading', true)
        .set('error', null)
        .set('feeObject', null);
    case GET_USER_FEE_RATE_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('feeObject', action.data);
    case GET_USER_FEE_RATE_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('feeObject', null);
    case GET_USER_FEE_TIER_RATE:
      return state
        .set('loading', true)
        .set('error', null)
        .set('feeTierObject', null);
    case GET_USER_FEE_TIER_RATE_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('feeTierObject', action.data);
    case GET_USER_FEE_TIER_RATE_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('feeTierObject', null);
    case COMPUTE_FEE:
      return state
        .set('loading', true)
        .set('error', null)
        .set('computedFee', null);
    case COMPUTE_FEE_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('computedFee', action.data);
    case COMPUTE_FEE_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('computedFee', null);
    default:
      return state;
  }
}

export default depositPageReducer;
