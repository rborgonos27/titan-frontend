import { put, takeLatest, all } from 'redux-saga/effects';
import { logoutAction } from 'containers/App/actions';
import { ON_NEW_USER } from './constants';

export function* onNewUser(user) {
  yield put(logoutAction());
  yield localStorage.setItem('username', user.data);
}

export function* onNewUserWatcher() {
  yield takeLatest(ON_NEW_USER, onNewUser);
}
export default function* rootSaga() {
  yield all([onNewUserWatcher()]);
}
