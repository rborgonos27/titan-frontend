import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the loginRedirect state domain
 */

const selectLoginRedirectDomain = state =>
  state.get('loginRedirect', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by LoginRedirect
 */

const makeSelectLoginRedirect = () =>
  createSelector(selectLoginRedirectDomain, substate => substate.toJS());

export default makeSelectLoginRedirect;
export { selectLoginRedirectDomain };
