/*
 *
 * LoginRedirect actions
 *
 */

import { ON_NEW_USER } from './constants';

export function onNewUser(data) {
  return {
    type: ON_NEW_USER,
    data,
  };
}
