import { fromJS } from 'immutable';
import loginRedirectReducer from '../reducer';

describe('loginRedirectReducer', () => {
  it('returns the initial state', () => {
    expect(loginRedirectReducer(undefined, {})).toEqual(fromJS({}));
  });
});
