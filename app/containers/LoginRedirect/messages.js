/*
 * LoginRedirect Messages
 *
 * This contains all the text for the LoginRedirect component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.LoginRedirect.header',
    defaultMessage: 'Redirecting...',
  },
});
