/**
 *
 * LoginRedirect
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { decodeUrlValue } from 'utils/helper';
import makeSelectLoginRedirect from './selectors';
import reducer from './reducer';
import saga from './saga';
import { onNewUser } from './actions';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export class LoginRedirect extends React.Component {
  componentWillMount() {
    const { match } = this.props;
    const decodedValue = decodeUrlValue(match.params.id);
    this.props.onNewUser(decodedValue);
  }
  render() {
    return (
      <div>
        <FormattedMessage {...messages.header} />
      </div>
    );
  }
}

LoginRedirect.propTypes = {
  onNewUser: PropTypes.func.isRequired,
  match: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  loginredirect: makeSelectLoginRedirect(),
});

const mapDispatchToProps = { onNewUser };

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'loginRedirect', reducer });
const withSaga = injectSaga({ key: 'loginRedirect', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(LoginRedirect);
