/*
 *
 * LoginRedirect reducer
 *
 */

import { fromJS } from 'immutable';
import { ON_NEW_USER } from './constants';

export const initialState = fromJS({});

function loginRedirectReducer(state = initialState, action) {
  switch (action.type) {
    case ON_NEW_USER:
      return state;
    default:
      return state;
  }
}

export default loginRedirectReducer;
