/**
 *
 * Asynchronously loads the component for PayeeViewPage
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
