/**
 *
 * PrivacyPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import titanLogo from 'images/titan-logo.png';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import {
  B,
  SubHeader,
  TextBody,
  BodyWrapper,
  HeaderPlain,
  HeaderLeft,
  HeaderCenter,
  Img,
  A,
  BigHeader,
  HeaderLeftNoUnderline,
} from '../MemberServicesPage/Pages/StyleUtils';

/* eslint-disable react/prefer-stateless-function */
export class PrivacyPage extends React.Component {
  render() {
    return (
      <ContainerWrapper>
        <CardBox>
          <Img src={titanLogo} />
          <BodyWrapper>
            <HeaderLeftNoUnderline>
              Titan Vault Online Privacy Notice
            </HeaderLeftNoUnderline>
            <TextBody>
              This Online Privacy Notice (Notice) applies to this Titan Vault online interface (i.e., website or mobile application) and any Titan Vault affiliate or subsidiary online interface that links to this Notice, (each, a Site, and, collectively, Sites).  The term "Titan Vault" or "we" or "us" or "our" in this Notice refers to affiliates or subsidiaries of Titan Vault, LLC that link to this Notice. This Notice describes how Sites may collect, use and share information from or about you, and explains how information may be collected and used for advertising purposes.
            </TextBody>
            <TextBody>
              The terms and conditions of this Notice control, and by using the Site, you agree to the terms and conditions of this Notice.
            </TextBody>
            <SubHeader>
              Collecting and Using Information <br />
              Personal Information We Collect Online
            </SubHeader>
            <TextBody>
              “Personal Information” means personally identifiable information such as information you provide via forms, surveys, applications or other online fields including name, postal or email addresses, telephone, fax or mobile numbers, or account numbers.
            </TextBody>
            <SubHeader>
              How We Use Personal Information
            </SubHeader>
            <TextBody>
              We may use Personal Information:
              <ul>
                <li>
                  to respond to your inquiries and fulfill your requests;
                </li>
                <li>
                  to inform you about important information regarding the Site, products or services for which you apply or may be interested in applying for, or in which you are already enrolled, changes to terms, conditions, and policies and/or other administrative information;
                </li>
                <li>
                  to deliver marketing communications that we believe may be of interest to you;
                </li>
                <li>
                  to personalize your experience on the Site;
                </li>
                <li>
                  to verify your identity and/or location (or the identity or location of your representative or agent) in order to allow access to your accounts, conduct online transactions and to maintain measures aimed at preventing fraud and protecting the security of account and Personal Information;
                </li>
                <li>
                  to allow you to participate in surveys and other forms of market research, sweepstakes, contests and similar promotions and to administer these activities. Some of these activities have additional rules, which may contain additional information about how Personal Information is used and shared;
                </li>
                <li>
                  to allow you to use some Site financial planning tools. Information that you enter into one of these planning tools may be stored for future access and use.;
                </li>
                <li>
                  collected through aggregation services such as My Portfolio® and My Financial Picture® in order to consolidate your financial account information at one online location; understand what product or service may be of interest to you; and present you with offers;
                </li>
                <li>
                  for business purposes, including data analysis, audits, developing and improving products and services, enhancing the Site, identifying usage trends and determining the effectiveness of promotional campaigns;
                </li>
                <li>
                  for risk control, for fraud detection and prevention, to comply with laws and regulations, and to comply with other legal process and law enforcement requirements;
                </li>
                <li>
                  to allow you to utilize features within our Sites by granting us access to information from your device such as contact lists, or geo-location when you request certain services.
                </li>

              </ul>
            </TextBody>
            <TextBody>
              <SubHeader>
                Other Information We Collect Online
              </SubHeader>
              Other Information is any information other than Personal Information that does not reveal your specific identity or does not directly relate to an individual, such as browser information, information collected through cookies, pixel tags and other technologies, demographic information, other information provided by you such as your date of birth or household income, and aggregated and de-identified data.
            </TextBody>
            <TextBody>
              <SubHeader>
                How We Collect and Use Other Information
              </SubHeader>
              We and our third-party service providers may collect and use Other Information in a variety of ways, including:
              <ul>
                <li>
                  Through your browser or device: Certain information is collected by most browsers and/or through your device, such as your Media Access Control (MAC) address, device type, screen resolution, operating system version and internet browser type and version. We use this information to ensure Sites function properly, for fraud detection and prevention, and security purposes.
                </li>
                <li>
                  Using cookies: Cookies are pieces of information stored directly on the device you are using. Cookies we use do not contain or capture unencrypted Personal Information. Cookies allow us to collect information such as browser type, time spent on the Site, pages visited, language preferences, and your relationship with us. We use the information for security purposes, to facilitate navigation, to display information more effectively, to personalize/tailor your experience while engaging with us, and to recognize your device to allow your use of our online products and services. We collect statistical information about the usage of the Site in order to continually improve the design and functionality, to monitor responses to our advertisements and content, to understand how account holders and visitors use the Site and to assist us with resolving questions regarding the Site. We also utilize cookies for advertising purposes. Please see the Advertising section below for more information.
                </li>
                <li>
                  You can refuse to accept these cookies and most devices and browsers offer their own privacy settings for cookies. You will need to manage your cookie settings for each device and browser you use. However, if you do not accept these cookies, you may experience some inconvenience in your use of the Site and some online products and services. For example, we will not be able to recognize your device and you will need to answer a challenge question each time you log on. You also may not receive tailored advertising or other offers from us that may be relevant to your interests and needs.
                </li>
                <li>
                  Other technologies including pixel tags, web beacons, and clear GIFs: These may be used in connection with some Site pages, downloadable mobile applications and HTML-formatted email messages to measure the effectiveness of our communications, the success of our marketing campaigns, to compile statistics about usage and response rates, to personalize/tailor your experience while engaging with us online and offline, for fraud detection and prevention, for security purposes, for advertising, and to assist us in resolving account holders’ questions regarding use of our Site. Please see our Advertising section below for more information regarding our use of other technologies.
                </li>
                <li>
                  Flash objects: As part of our solutions for online authentication, we use Flash objects (sometimes referred to as “Local Shared Objects”) to help us recognize you and your device when you come back to the Site. For our pages running Adobe® Flash® content (demos and tutorials with moving content), we use Flash objects to determine your browser type and version of Adobe Flash in order for you to view the content. We do not use Flash objects for any online behavioral advertising purpose. Deleting cookies does not delete Flash objects. You can learn more about Flash objects - including how to control and disable them - through the Adobe interface. If you choose to disable Flash objects from our Site, then you may not be able to access and use all or part of the Site or benefit from the information and services offered.
                </li>
                <li>
                  IP Address: Your IP Address is a number that is automatically assigned to the device that you are using by your Internet Service Provider (ISP). An IP Address is identified and logged automatically in our server log files whenever a user visits the Site, along with the time of the visit and the page(s) that were visited. Collecting IP Addresses is standard practice on the internet and is done automatically by many web sites. We use IP Addresses for purposes such as calculating Site usage levels, helping diagnose server problems, to personalize/tailor your experience while engaging with us online and offline, for compliance and security purposes, for advertising, and administering the Site. Please see the Advertising section below for more information.
                </li>
                <li>
                  Aggregated and De-identified Data: Aggregated and De-identified Data is data that we may create or compile from various sources, including but not limited to accounts and transactions.  This information, which does not identify individual account holders, may be used for our business purposes, which may include offering products or services, research, marketing or analyzing market trends, and other purposes consistent with applicable laws.
                </li>
              </ul>
            </TextBody>
            <TextBody>
              <SubHeader>
                Additional Information
              </SubHeader>
              <SubHeader>
                Advertising
              </SubHeader>
              Titan Vault may advertise online (e.g., pages within our Sites and mobile apps through managed social media presences, and on other sites and mobile apps not affiliated with Titan Vault) and offline (e.g., direct marketing). In order to understand how advertising performs, we may collect certain information on our Sites and other sites and mobile apps through our advertising service providers using cookies, IP addresses, and other technologies. The collected information may include the number of page visits, pages viewed on our Sites, search engine referrals, browsing activities over time and across other sites following your visit to one of our Sites or apps, and responses to advertisements and promotions on the Sites and on sites and apps where we advertise.
            </TextBody>
            <TextBody>
              Titan Vault uses information described in this Notice to help advertise our products and services, in a variety of ways. We use such information to:
              <ul>
                <li>
                  Present tailored ads, to you including:
                  <ul>
                    <li>
                      Banner ads and splash ads that appear as you sign on or off of your online accounts on our Sites, within mobile banking and other mobility applications
                    </li>
                    <li>
                      E-mail, postal mail, and telemarketing, and,
                    </li>
                    <li>
                      On other sites and mobile apps not affiliated with Titan Vault;
                    </li>
                  </ul>
                </li>
                <li>
                  Analyze the effectiveness of our ads; and
                </li>
                <li>
                  Determine whether you might be interested in new products or services
                </li>
              </ul>
            </TextBody>
            <TextBody>
              <SubHeader>
                Important Reminder
              </SubHeader>
              In order for online behavioral advertising opt outs from our Sites and on other sites to work on your device, your browser must be set to accept cookies. If you delete cookies, buy a new device, access our Site or other sites from a different device, login under a different screen name, or change web browsers, you will need to opt-out again. If your browser has scripting disabled, you do not need to opt out, as online behavioral advertising technology does not work when scripting is disabled. Please check your browser's security settings to validate whether scripting is active or disabled.
            </TextBody>
            <TextBody>
              <SubHeader>
                Mobile Applications
              </SubHeader>
              Titan Vault’s Mobile Applications ("Applications") allow you to access your account balances and holdings, make deposits, transfers and pay bills on your mobile device.  This Notice applies to any Personal Information or Other Information that we may collect through the Applications.
            </TextBody>
            <TextBody>
              <SubHeader>
                Linking to other sites
              </SubHeader>
              We may provide links to third party sites, such as credit bureaus, service providers or merchants. If you follow links to sites not affiliated or controlled by Titan Vault, you should review their privacy and security policies and other terms and conditions, as they may be different from those of our Sites. Titan Vault does not guarantee and is not responsible for the privacy or security of these sites, including the accuracy, completeness, or reliability of their information.
            </TextBody>
            <TextBody>
              <SubHeader>
                Social media sites
              </SubHeader>
              Titan Vault provides experiences on social media platforms including, but not limited to, Facebook®, Twitter®, YouTube® and LinkedIn® that enable online sharing and collaboration among users who have registered to use them. Any content you post on official Titan Vault managed social media pages, such as pictures, information, opinions, or any Personal Information that you make available to other participants on these social platforms, is subject to the Terms of Use and Privacy Policies of those respective platforms. Please refer to them to better understand your rights and obligations with regard to such content.  In addition, please note that when visiting any official Titan Vault social media pages, you are also subject to Titan Vault’s Privacy Notices.
            </TextBody>
            <TextBody>
              <SubHeader>
                Security
              </SubHeader>
              To protect Personal Information from unauthorized access and use, we use security measures that comply with applicable federal and state laws.  These measures may include device safeguards and secured files and buildings as well as oversight of our third party service providers to ensure information remains confidential and secure.  
            </TextBody>
            <TextBody>
              <SubHeader>
                Using other aggregation websites
              </SubHeader>
              Other companies offer aggregation websites and services that allow you to consolidate your financial account information from different sources (such as your accounts with us or with other financial institutions) so that you can view all your account information at one online location. To do this, an aggregation provider may request access to Personal Information, such as financial information, usernames and passwords. You should use caution and ensure that the aggregator company has appropriate policies and practices to protect the privacy and security of any information you provide or to which they are gaining access. We are not responsible for the use or disclosure of any Personal Information accessed by any company or person to whom you provide your Site username and password.
            </TextBody>
            <TextBody>
              If you provide your Site username, password or other information about your accounts with us to an aggregation website, we will consider that you have authorized all transactions or actions initiated by an aggregation website using access information you provide, whether or not you were aware of a specific transaction or action.  If you decide to revoke the authority you have given to an aggregation website, we strongly recommend that you change your password for the Site to ensure that the aggregation website cannot continue to access your account.
            </TextBody>
            <TextBody>
              <SubHeader>
                Making sure information is accurate
              </SubHeader>
              Keeping your account information accurate and up to date is very important. If your account information is incomplete, inaccurate or not current, please use the Contact Us option on our Site, or call or write to us at the telephone numbers or appropriate address for changes listed on your account statements, records, online or other account materials. 
            </TextBody>
            <TextBody>
              Updates to this Privacy Notice <br />
              This U.S. Online Privacy Notice is subject to change. Please review it periodically. If we make changes to the U.S. Online Privacy Notice, we will revise the “Last Updated” date at the top of this Notice. Any changes to this Notice will become effective when we post the revised Notice on the Site. Your use of the Site following these changes means that you accept the revised Notice.
            </TextBody>
          </BodyWrapper>
        </CardBox>
      </ContainerWrapper>
    );
  }
}

PrivacyPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps,
);

export default compose(withConnect)(PrivacyPage);
