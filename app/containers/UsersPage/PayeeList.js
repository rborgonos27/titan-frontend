import React from 'react';
import PropTypes from 'prop-types';
import { Button, List } from 'semantic-ui-react';
import Gravatar from 'react-gravatar';

const PayeeList = ({ list, buttonAction, action, isView }) => {
  /* eslint-disable */
  let listRow = [];
  for (let i = 0; i<list.length; i++) {
    const payee = list[i];
    listRow.push(
      <List.Item id={i} key={i}>
        {!isView ? (
          <List.Content floated="right">
            <Button color={action === 'add' ? 'green' : 'red'} onClick={() => buttonAction(payee.id)}>
              { action === 'add' ? 'Add' : 'Remove' }
            </Button>
          </List.Content>
        ) : null}
        <List.Content floated="left">
          <Gravatar
            email={payee.email}
            size={25}
            rating="pg"
            default="identicon"
            className="img-circle"
          />
        </List.Content>
        <List.Content>
          <List.Header>{`${payee.name}`}</List.Header>
        </List.Content>
      </List.Item>
    )
  }
  /* eslint-enable */
  return (
    <List divided verticalAlign="middle">
      {listRow}
    </List>
  );
};

PayeeList.propTypes = {
  list: PropTypes.any.isRequired,
  buttonAction: PropTypes.func.isRequired,
  action: PropTypes.any.isRequired,
  isView: PropTypes.bool,
};

export default PayeeList;
