import { all, call, put, takeLatest } from 'redux-saga/effects';
import request, { getBlob, download } from 'utils/request';
import { APP } from 'config';
import { optionsFormData } from 'utils/options';
import moment from 'moment';
import {
  GET_USER,
  ADD_USER,
  EDIT_USER,
  DELETE_USER,
  PAGE_ACTION,
  GET_FEE_RATES,
  UPDATE_FEE_TIER,
  GET_STATE_CITIES,
  ENABLE_USER,
  FORGOT_PASSWORD,
  EXPORT_CSV,
  GET_PAYEE_USER,
  SEARCH_PAYEE,
  ADD_PAYEE,
  REMOVE_PAYEE,
  GET_USER_MEMBER,
  DISABLE_SUB_MEMBER,
  ADD_SUB_MEMBER,
  ENABLE_SUB_MEMBER,
  UPDATE_SUB_MEMBER,
} from './constants';
import {
  getUser,
  getUserSuccess,
  getUserError,
  addUserSuccess,
  addUserError,
  editUserSuccess,
  editUserError,
  deleteUserSuccess,
  deleteUserError,
  pageActionSuccess,
  pageActionError,
  getFeeRatesSuccess,
  getFeeRatesError,
  updateFeeTierSuccess,
  updateFeeTierError,
  getStateCitiesError,
  getStateCitiesSuccess,
  enableUserError,
  enableUserSuccess,
  forgotPasswordSuccess,
  forgotPasswordError,
  exportCSVSuccess,
  exportCSVError,
  getUserPayee,
  getUserPayeeSuccess,
  getUserPayeeError,
  searchPayee,
  searchPayeeSuccess,
  searchPayeeError,
  addPayeeSuccess,
  addPayeeError,
  removePayeeSuccess,
  removePayeeError,
  getUserMember,
  getUserMemberSuccess,
  getUserMemberError,
  disableSubMemberSuccess,
  disableSubMemberError,
  addSubMemberSuccess,
  addSubMemberError,
  enableSubMemberSuccess,
  enableSubMemberError,
  updateSubMemberSuccess,
  updateSubMemberError,
} from './actions';

export function* getUserData() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const user = yield call(
      request,
      `${API_URI}/admin/user/detail/`,
      optionsFormData('GET', null, token),
    );
    if (user.results) {
      yield put(getUserSuccess(user));
    } else {
      yield put(getUserError(user));
    }
  } catch (error) {
    yield put(getUserError(error));
  }
}

export function* addUser(userData) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const {
      email,
      passwordInput,
      firstName,
      lastName,
      username,
      primaryContactName,
      primaryContactNumber,
      businessName,
      principalCityOfBusiness,
      userType,
      address,
      address2,
      city,
      state,
      zipCode,
      businessContactNumber,
    } = userData.data;
    const formData = new FormData();
    formData.append('email', email);
    formData.append('username', username);
    formData.append('password', passwordInput);
    formData.append('first_name', firstName);
    formData.append('last_name', lastName);
    formData.append('primary_contact_name', primaryContactName);
    formData.append('primary_contact_number', primaryContactNumber);
    formData.append('business_name', businessName);
    formData.append('principal_city_of_business', principalCityOfBusiness);
    formData.append('user_type', userType);
    formData.append('address', address || '');
    formData.append('address_2', address2 || '');
    formData.append('city', city || '');
    formData.append('state', state || '');
    formData.append('zip_code', zipCode || '');
    formData.append('business_contact_number', businessContactNumber || '');
    formData.append('is_first_time_member', userType === 'member');
    const user = yield call(
      request,
      `${API_URI}/user/`,
      optionsFormData('POST', formData, token),
    );
    if (user.id) {
      yield put(addUserSuccess(user));
      yield put(getUser());
      yield call(
        request,
        `${API_URI}/new/user/notif`,
        optionsFormData('POST', formData, token),
      );
    } else {
      /* eslint-disable */
      let errorList = [];
      for (const key in user) {
        if (user.hasOwnProperty(key)) {
          errorList.push(user[key]);
        }
      }
      yield put(addUserError(errorList));
      /* eslint-enable */
    }
  } catch (error) {
    yield put(addUserError(error));
  }
}

export function* pageAction(pageData) {
  const API_URI = APP.API_URL;
  const { limit, offset, search, ordering, searchStatus } = pageData.data;
  let queryString = '';
  let separator = queryString === '' ? '?' : '&';
  queryString = limit
    ? `${queryString}${separator}limit=${limit}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = offset
    ? `${queryString}${separator}offset=${offset}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = search
    ? `${queryString}${separator}search=${search}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = searchStatus
    ? `${queryString}${separator}search=${searchStatus}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = ordering
    ? `${queryString}${separator}ordering=${ordering}`
    : queryString;
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const transactions = yield call(
      request,
      `${API_URI}/admin/user/detail/${queryString}`,
      optionsFormData('GET', null, token),
    );
    if (transactions.results) {
      yield put(pageActionSuccess(transactions));
    } else {
      yield put(pageActionError(transactions));
    }
  } catch (error) {
    yield put(pageActionError(error));
  }
}

export function* editUser(userData) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const {
      userId,
      email,
      passwordInput,
      firstName,
      lastName,
      userName,
      primaryContactName,
      primaryContactNumber,
      businessName,
      principalCityOfBusiness,
      userType,
      address,
      address2,
      city,
      state,
      zipCode,
      businessContactNumber,
      previousUserType,
      locationId,
      // depositRate,
      // depositThreshold,
      // payrollPaymentRate,
      // paymentRate,
      // paymentThreshold,
      // taxPaymentRate,
    } = userData.data;
    const formData = new FormData();
    const isFirstTimeMember =
      userType === 'member' && previousUserType === 'temporary';
    formData.append('email', email);
    formData.append('username', userName);
    formData.append('first_name', firstName);
    formData.append('last_name', lastName);
    formData.append('primary_contact_name', primaryContactName);
    formData.append('primary_contact_number', primaryContactNumber);
    formData.append('business_name', businessName);
    formData.append('principal_city_of_business', principalCityOfBusiness);
    formData.append('address', address);
    formData.append('address_2', address2 || '');
    formData.append('city', city || '');
    formData.append('state', state || '');
    formData.append('zip_code', zipCode || '');
    formData.append('business_contact_number', businessContactNumber);
    formData.append('user_type', userType);
    formData.append('is_first_time_member', isFirstTimeMember);
    if (locationId) {
      formData.append('location_id', locationId);
    }
    if (passwordInput === '') {
      const userRequest = yield call(
        request,
        `${API_URI}/user/account/${userId}/`,
        optionsFormData('GET', null, token),
      );
      if (userRequest.id) {
        formData.append('password', userRequest.password);
        const user = yield call(
          request,
          `${API_URI}/user/${userId}/`,
          optionsFormData('PUT', formData, token),
        );
        if (user.id) {
          yield put(editUserSuccess(userData));
          yield put(getUser());
        } else {
          yield put(editUserError(userData));
        }
      }
    } else {
      formData.append('password', passwordInput);
      const user = yield call(
        request,
        `${API_URI}/profile/user/${userId}/`,
        optionsFormData('PUT', formData, token),
      );
      if (user.id) {
        yield put(editUserSuccess(userData));
        yield put(getUser());
      } else {
        yield put(editUserError(userData));
      }
    }
    // if (payrollPaymentRate && taxPaymentRate) {
    //   const formDataFee = new FormData();
    //   const payrollPayment = parseFloat(payrollPaymentRate) / 100;
    //   const taxPayment = parseFloat(taxPaymentRate) / 100;
    //   const depositFlat = parseFloat(depositFlatRate) / 100;
    //   const paymentFlat = parseFloat(paymentFlatRate) / 100;
    //   formDataFee.append('payroll_payment_rate', payrollPayment.toFixed(4));
    //   formDataFee.append('tax_payment_rate', taxPayment.toFixed(4));
    //   formDataFee.append('payment_flat_rate', paymentFlat.toFixed(4));
    //   formDataFee.append('deposit_flat_rate', depositFlat.toFixed(4));
    //   formDataFee.append('fee_setting', feeSetting);
    //   formDataFee.append('user_id', userId);
    //   const fee = yield call(
    //     request,
    //     `${API_URI}/fee/${feeId}/`,
    //     optionsFormData('PUT', formDataFee, token),
    //   );
    //   console.log('fee can edit', fee, tierId, isEditTier);
    //   if (tierId) {
    //     console.log('can edit', isEditTier);
    //     /* eslint-disable */
    //     for (let i = 0; i < feeTier.length; i++) {
    //       const tier = feeTier[i];
    //       const feeTierForm = new FormData();
    //       const depositRate = tier.deposit_rate / 100;
    //       const paymentRate = tier.payment_rate / 100;
    //       feeTierForm.append('tier_no', tier.tier_no);
    //       feeTierForm.append('deposit_rate', depositRate.toFixed(4));
    //       if (tier.deposit_threshold) {
    //         feeTierForm.append('deposit_threshold', tier.deposit_threshold);
    //       }
    //       feeTierForm.append('payment_rate', paymentRate.toFixed(4));
    //       if (tier.payment_threshold) {
    //         feeTierForm.append('payment_threshold', tier.payment_threshold);
    //       }
    //       feeTierForm.append('user_id', userId);
    //       /*
    //       tier_no,
    //       deposit_rate,
    //       deposit_threshold,
    //       payment_rate,
    //       payment_threshold,
    //       */
    //       const feeTierResponse = yield call(
    //         request,
    //         `${API_URI}/fee_tier/${tier.id}/`,
    //         optionsFormData('PUT', feeTierForm, token),
    //       );
    //       console.log('response', feeTierResponse);
    //     }
    //     /* eslint-enable */
    //   }
    // }
    if (isFirstTimeMember) {
      yield call(
        request,
        `${API_URI}/member/user/notif`,
        optionsFormData('POST', formData, token),
      );
    }
  } catch (error) {
    console.log('error data', error);
    yield put(editUserError(error));
  }
}

export function* deleteUser(userData) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const { userId } = userData.data;
    yield call(
      request,
      `${API_URI}/user/${userId}/`,
      optionsFormData('DELETE', null, token),
    );
    yield put(deleteUserSuccess(userData));
    yield put(getUser());
  } catch (error) {
    yield put(deleteUserError(error));
  }
}

export function* getFeeRates(rate) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const { data } = rate;
    const feeObject = {};
    const feeResponse = yield call(
      request,
      `${API_URI}/fee/?user_id=${data}`,
      optionsFormData('GET', null, token),
    );
    /* eslint-disable */
    if (feeResponse.results.length > 0) {
      feeObject.fee = feeResponse.results[0];
    } else {
      yield put(getFeeRatesError(feeResponse));
    }
    const feeTierResponse = yield call(
      request,
      `${API_URI}/fee_tier/?user_id=${data}`,
      optionsFormData('GET', null, token),
    );
    if (feeTierResponse.results.length > 0) {
      feeObject.feeTier = feeTierResponse.results;
    } else {
      yield put(getFeeRatesError(feeTierResponse));
    }
    /* eslint-enable */
    yield put(getFeeRatesSuccess(feeObject));
  } catch (error) {
    yield put(getFeeRatesError(error));
  }
}

export function* updateFeeTier(feeData) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const {
      userId,
      payrollPaymentRate,
      taxPaymentRate,
      feeId,
      feeTier,
      tierId,
      isEditTier,
      feeSetting,
      paymentFlatRate,
      depositFlatRate,
    } = feeData.data;

    if (payrollPaymentRate && taxPaymentRate) {
      const formDataFee = new FormData();
      const payrollPayment = parseFloat(payrollPaymentRate) / 100;
      const taxPayment = parseFloat(taxPaymentRate) / 100;
      const depositFlat = parseFloat(depositFlatRate) / 100;
      const paymentFlat = parseFloat(paymentFlatRate) / 100;
      formDataFee.append('payroll_payment_rate', payrollPayment.toFixed(4));
      formDataFee.append('tax_payment_rate', taxPayment.toFixed(4));
      formDataFee.append('payment_flat_rate', paymentFlat.toFixed(4));
      formDataFee.append('deposit_flat_rate', depositFlat.toFixed(4));
      formDataFee.append('fee_setting', feeSetting);
      formDataFee.append('user_id', userId);
      const fee = yield call(
        request,
        `${API_URI}/fee/${feeId}/`,
        optionsFormData('PUT', formDataFee, token),
      );
      console.log('fee can edit', fee, tierId, isEditTier);
      if (tierId) {
        console.log('can edit', isEditTier);
        /* eslint-disable */
        for (let i = 0; i < feeTier.length; i++) {
          const tier = feeTier[i];
          const feeTierForm = new FormData();
          const depositRate = tier.deposit_rate / 100;
          const paymentRate = tier.payment_rate / 100;
          feeTierForm.append('tier_no', tier.tier_no);
          feeTierForm.append('deposit_rate', depositRate.toFixed(4));
          if (tier.deposit_threshold) {
            feeTierForm.append('deposit_threshold', tier.deposit_threshold);
          }
          feeTierForm.append('payment_rate', paymentRate.toFixed(4));
          if (tier.payment_threshold) {
            feeTierForm.append('payment_threshold', tier.payment_threshold);
          }
          feeTierForm.append('user_id', userId);
          /*
          tier_no,
          deposit_rate,
          deposit_threshold,
          payment_rate,
          payment_threshold,
          */
          const feeTierResponse = yield call(
            request,
            `${API_URI}/fee_tier/${tier.id}/`,
            optionsFormData('PUT', feeTierForm, token),
          );
          yield put(updateFeeTierSuccess(feeTierResponse));
        }
        /* eslint-enable */
      }
    }
  } catch (error) {
    yield put(updateFeeTierError(error));
  }
}

export function* getStateCities() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const stateCities = yield call(
      request,
      `${API_URI}/state_cities/`,
      optionsFormData('GET', null, token),
    );
    if (stateCities.data) {
      yield put(getStateCitiesSuccess(stateCities.data));
    } else {
      yield put(getStateCitiesSuccess(stateCities.error));
    }
  } catch (error) {
    yield put(getStateCitiesError(error));
  }
}

export function* enableUser(userData) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userForm = new FormData();
    const { data } = userData;
    userForm.append('user_id', data);
    const userRequest = yield call(
      request,
      `${API_URI}/enable_user/`,
      optionsFormData('PUT', userForm, token),
    );
    if (userRequest.success) {
      yield put(enableUserSuccess(userRequest));
      yield put(getUser());
    } else {
      yield put(enableUserError(userRequest.message));
    }
  } catch (error) {
    yield put(enableUserError(error));
  }
}

export function* forgotPassword(info) {
  try {
    const API_URI = APP.API_URL;
    const resetPasswordData = new FormData();
    resetPasswordData.append('username_or_email', info.data);
    const loginResponse = yield call(
      request,
      `${API_URI}/forgot_password/`,
      optionsFormData('POST', resetPasswordData),
    );
    if (loginResponse) {
      yield put(forgotPasswordSuccess(loginResponse));
    } else {
      yield put(forgotPasswordError(loginResponse));
    }
  } catch (error) {
    yield put(forgotPasswordError(error));
  }
}

export function* exportCSV() {
  const API_URI = APP.API_URL;
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const options = {
      headers: {
        Accept: 'application/json',
        Authorization: `JWT ${token}`,
      },
      method: 'GET',
    };
    const result = yield call(getBlob, `${API_URI}/csv_user/`, options);
    if (!result.errors) {
      download(result, `member_records_${moment().format('YYYY_MM-DD')}.csv`);
      yield put(exportCSVSuccess('success'));
    }
  } catch (error) {
    yield put(exportCSVError(error));
  }
}

export function* runGetUserPayee(userData) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const payeeResponse = yield call(
      request,
      `${API_URI}/payee_user/?user_id=${userData.data}`,
      optionsFormData('GET', null, token),
    );
    if (payeeResponse.success) {
      const userDataPayee = [];
      /* eslint-disable */
      for (let i = 0; i < payeeResponse.data.length; i++) {
        const userPayeeData = payeeResponse.data[i];
        userDataPayee.push(userPayeeData.payee);
      }
      yield put(getUserPayeeSuccess(userDataPayee));
      /* eslint-enable */
    } else {
      yield put(getUserPayeeError(payeeResponse.message));
    }
  } catch (error) {
    yield put(getUserPayeeError(error));
  }
}

export function* runSearchPayee(payeeData) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const payeeDetails = yield call(
      request,
      `${API_URI}/payee_details/?search=${payeeData.data}`,
      optionsFormData('GET', null, token),
    );
    if (payeeDetails.results) {
      const payeeList = [];
      /* eslint-disable */
      for (let i = 0; i < payeeDetails.results.length; i++) {
        const payee = payeeDetails.results[i];
        payeeList.push(payee);
      }
      yield put(searchPayeeSuccess(payeeList));
      /* eslint-enable */
    } else {
      yield put(searchPayeeError('Payee Not Found'));
    }
  } catch (error) {
    yield put(searchPayeeError(error));
  }
}

export function* addPayee(payeeData) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userPayeeFormData = new FormData();
    const { payeeId, userId, searchInput } = payeeData.data;
    userPayeeFormData.append('user_id', userId);
    userPayeeFormData.append('payee_id', payeeId);
    const userPayeeResponse = yield call(
      request,
      `${API_URI}/user_payee/`,
      optionsFormData('POST', userPayeeFormData, token),
    );
    if (userPayeeResponse.success) {
      yield put(getUserPayee(userId));
      if (searchInput !== '') {
        yield put(searchPayee(searchInput));
      }
      yield put(addPayeeSuccess('Success'));
    }
  } catch (error) {
    yield put(addPayeeError(error));
  }
}

export function* removePayee(payeeData) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const { payeeId, userId, searchInput } = payeeData.data;
    const userPayeeResponse = yield call(
      request,
      `${API_URI}/payee_user/?payee_id=${payeeId}&user_id=${userId}`,
      optionsFormData('DELETE', null, token),
    );
    if (userPayeeResponse.success) {
      yield put(getUserPayee(userId));
      if (searchInput !== '') {
        yield put(searchPayee(searchInput));
      }
      yield put(removePayeeSuccess('Success'));
    }
  } catch (error) {
    yield put(removePayeeError(error));
  }
}

export function* runGetUserMember(userMemberData) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userMembers = yield call(
      request,
      `${API_URI}/user_member/?user_id=${userMemberData.data}`,
      optionsFormData('GET', null, token),
    );
    if (userMembers.success) {
      const userDataPayee = [];
      /* eslint-disable */
      for (let i = 0; i < userMembers.data.length; i++) {
        const userMemberData = userMembers.data[i];
        userDataPayee.push(userMemberData);
      }
      yield put(getUserMemberSuccess(userDataPayee));
      /* eslint-enable */
    } else {
      yield put(getUserMemberError(userMembers.message));
    }
  } catch (error) {
    yield put(getUserMemberError(error));
  }
}

export function* disableSubMember(member) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const { userId, adminUserId } = member.data;
    yield call(
      request,
      `${API_URI}/member_user/${userId}/`,
      optionsFormData('DELETE', null, token),
    );
    yield put(disableSubMemberSuccess(adminUserId));
    yield put(getUserMember(adminUserId));
  } catch (error) {
    yield put(disableSubMemberError(error));
  }
}

export function* addSubMember(member) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    console.log('member', member);
    const { data } = member;
    const {
      userId,
      subMemberEmailInput,
      subMemberFirstName,
      subMemberLastName,
      subMemberContactNo,
      subMemberPasswordInput,
    } = data;
    const memberCall = yield call(
      request,
      `${API_URI}/user/${userId}/`,
      optionsFormData('GET', null, token),
    );
    const formData = new FormData();
    formData.append('email', subMemberEmailInput);
    formData.append('username', '');
    formData.append('password', subMemberPasswordInput);
    formData.append('first_name', subMemberFirstName);
    formData.append('last_name', subMemberLastName);
    formData.append('primary_contact_name', subMemberFirstName);
    formData.append('primary_contact_number', subMemberContactNo);
    formData.append('business_name', memberCall.business_name);
    formData.append(
      'principal_city_of_business',
      memberCall.principal_city_of_business,
    );
    formData.append('user_type', 'sub_member');
    formData.append('address', memberCall.address);
    formData.append('address_2', memberCall.address_2);
    formData.append('city', memberCall.city);
    formData.append('state', memberCall.state);
    formData.append('zip_code', memberCall.zip_code);
    formData.append('parent_user_id', memberCall.id);
    formData.append(
      'business_contact_number',
      memberCall.business_contact_number,
    );
    formData.append('is_first_time_member', true);
    const memberRequest = yield call(
      request,
      `${API_URI}/member_user/`,
      optionsFormData('POST', formData, token),
    );
    if (memberRequest.id) {
      yield put(getUserMember(memberCall.id));
      yield put(addSubMemberSuccess(memberRequest.data));
      yield call(
        request,
        `${API_URI}/new/user/notif`,
        optionsFormData('POST', formData, token),
      );
    } else {
      yield put(addSubMemberError(memberRequest.message));
    }
  } catch (error) {
    yield put(addSubMemberError(error));
  }
}

export function* updateSubMember(member) {
  const API_URI = APP.API_URL;
  const token = yield JSON.parse(localStorage.getItem('token'));
  const { data } = member;
  const {
    subMemberEmailInput,
    subMemberFirstName,
    subMemberLastName,
    subMemberContactNo,
    subMemberPasswordInput,
    memberId,
    userId,
  } = data;
  const memberCall = yield call(
    request,
    `${API_URI}/user/${memberId}/`,
    optionsFormData('GET', null, token),
  );
  const formData = new FormData();
  formData.append('email', subMemberEmailInput);
  formData.append('username', memberCall.username);
  formData.append('first_name', subMemberFirstName);
  formData.append('last_name', subMemberLastName);
  formData.append('primary_contact_name', subMemberFirstName);
  formData.append('primary_contact_number', subMemberContactNo);
  formData.append('business_name', memberCall.business_name);
  formData.append('user_type', 'sub_member');
  formData.append('address', memberCall.address);
  formData.append('address_2', memberCall.address_2);
  formData.append('city', memberCall.city);
  formData.append('state', memberCall.state);
  formData.append('zip_code', memberCall.zip_code);
  formData.append('parent_user_id', userId);
  formData.append(
    'principal_city_of_business',
    memberCall.principal_city_of_business,
  );
  formData.append(
    'business_contact_number',
    memberCall.business_contact_number,
  );
  formData.append('is_first_time_member', memberCall.is_first_time_member);
  if (subMemberPasswordInput === '') {
    const userRequest = yield call(
      request,
      `${API_URI}/user/account/${memberId}/`,
      optionsFormData('GET', null, token),
    );
    if (userRequest.id) {
      formData.append('password', userRequest.password);
      const memberRequest = yield call(
        request,
        `${API_URI}/member_user/${memberId}/`,
        optionsFormData('PUT', formData, token),
      );
      if (memberRequest.id) {
        yield put(updateSubMemberSuccess(memberRequest.data));
        yield put(getUserMember(userId));
      } else {
        yield put(updateSubMemberError(memberRequest.message));
      }
    }
  } else {
    formData.append('password', subMemberPasswordInput);
    const user = yield call(
      request,
      `${API_URI}/profile/user/${memberId}/`,
      optionsFormData('PUT', formData, token),
    );
    if (user.id) {
      yield put(updateSubMemberSuccess(user));
      yield put(getUserMember(userId));
    } else {
      yield put(updateSubMemberError(user));
    }
  }
  yield call(
    request,
    `${API_URI}/update/user/notif`,
    optionsFormData('POST', formData, token),
  );
}

export function* enableSubMember(member) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userForm = new FormData();
    const { userId, adminUserId } = member.data;
    userForm.append('user_id', userId);
    const userRequest = yield call(
      request,
      `${API_URI}/enable_user/`,
      optionsFormData('PUT', userForm, token),
    );
    if (userRequest.success) {
      yield put(enableSubMemberSuccess(userRequest));
      yield put(getUserMember(adminUserId));
    } else {
      yield put(enableSubMemberError(userRequest.message));
    }
  } catch (error) {
    yield put(enableSubMemberError(error));
  }
}

export function* getUserWatcher() {
  yield takeLatest(GET_USER, getUserData);
}

export function* addUserWatcher() {
  yield takeLatest(ADD_USER, addUser);
}

export function* pageActionWatcher() {
  yield takeLatest(PAGE_ACTION, pageAction);
}

export function* editUserWatcher() {
  yield takeLatest(EDIT_USER, editUser);
}

export function* deleteUserWatcher() {
  yield takeLatest(DELETE_USER, deleteUser);
}

export function* getFeeRatesWatcher() {
  yield takeLatest(GET_FEE_RATES, getFeeRates);
}

export function* updateFeeTierWatcher() {
  yield takeLatest(UPDATE_FEE_TIER, updateFeeTier);
}

export function* getStateCitiesWatcher() {
  yield takeLatest(GET_STATE_CITIES, getStateCities);
}

export function* enableUserWatcher() {
  yield takeLatest(ENABLE_USER, enableUser);
}

export function* forgotPasswordWatcher() {
  yield takeLatest(FORGOT_PASSWORD, forgotPassword);
}

export function* exportCSVWatcher() {
  yield takeLatest(EXPORT_CSV, exportCSV);
}

export function* getPayeeUserWatcher() {
  yield takeLatest(GET_PAYEE_USER, runGetUserPayee);
}

export function* searchPayeeWatcher() {
  yield takeLatest(SEARCH_PAYEE, runSearchPayee);
}

export function* addPayeeWatcher() {
  yield takeLatest(ADD_PAYEE, addPayee);
}

export function* removePayeeWatcher() {
  yield takeLatest(REMOVE_PAYEE, removePayee);
}

export function* getMemberUserWatcher() {
  yield takeLatest(GET_USER_MEMBER, runGetUserMember);
}

export function* disableSubMemberWatcher() {
  yield takeLatest(DISABLE_SUB_MEMBER, disableSubMember);
}

export function* addSubMemberWatcher() {
  yield takeLatest(ADD_SUB_MEMBER, addSubMember);
}

export function* enableSubMemberWatcher() {
  yield takeLatest(ENABLE_SUB_MEMBER, enableSubMember);
}

export function* updateSubMemberWatcher() {
  yield takeLatest(UPDATE_SUB_MEMBER, updateSubMember);
}

export default function* rootSaga() {
  yield all([
    getUserWatcher(),
    addUserWatcher(),
    pageActionWatcher(),
    editUserWatcher(),
    deleteUserWatcher(),
    getFeeRatesWatcher(),
    updateFeeTierWatcher(),
    getStateCitiesWatcher(),
    enableUserWatcher(),
    forgotPasswordWatcher(),
    exportCSVWatcher(),
    getPayeeUserWatcher(),
    searchPayeeWatcher(),
    addPayeeWatcher(),
    removePayeeWatcher(),
    getMemberUserWatcher(),
    disableSubMemberWatcher(),
    addSubMemberWatcher(),
    enableSubMemberWatcher(),
    updateSubMemberWatcher(),
  ]);
}
