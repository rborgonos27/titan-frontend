/*
 *
 * UsersPage constants
 *
 */

export const GET_USER = 'app/UsersPage/GET_USER';
export const GET_USER_SUCCESS = 'app/UsersPage/GET_USER_SUCCESS';
export const GET_USER_ERROR = 'app/UsersPage/GET_USER_ERROR';

export const ADD_USER = 'app/UsersPage/ADD_USER';
export const ADD_USER_SUCCESS = 'app/UsersPage/ADD_USER_SUCCESS';
export const ADD_USER_ERROR = 'app/UsersPage/ADD_USER_ERROR';

export const EDIT_USER = 'app/UsersPage/EDIT_USER';
export const EDIT_USER_SUCCESS = 'app/UsersPage/EDIT_USER_SUCCESS';
export const EDIT_USER_ERROR = 'app/UsersPage/EDIT_USER_ERROR';

export const DELETE_USER = 'app/UsersPage/DELETE_USER';
export const DELETE_USER_SUCCESS = 'app/UsersPage/DELETE_USER_SUCCESS';
export const DELETE_USER_ERROR = 'app/UsersPage/DELETE_USER_ERROR';

export const PAGE_ACTION = 'app/UsersPage/PAGE_ACTION';
export const PAGE_ACTION_SUCCESS = 'app/UsersPage/PAGE_ACTION_SUCCESS';
export const PAGE_ACTION_ERROR = 'app/UsersPage/PAGE_ACTION_ERROR';

export const GET_FEE_RATES = 'app/UsersPage/GET_FEE_RATES';
export const GET_FEE_RATES_SUCCESS = 'app/UsersPage/GET_FEE_RATES_SUCCESS';
export const GET_FEE_RATES_ERROR = 'app/UsersPage/GET_FEE_RATES_ERROR';

export const UPDATE_FEE_TIER = 'app/UsersPage/UPDATE_FEE_TIER';
export const UPDATE_FEE_TIER_SUCCESS = 'app/UsersPage/UPDATE_FEE_TIER_SUCCESS';
export const UPDATE_FEE_TIER_ERROR = 'app/UsersPage/UPDATE_FEE_TIER_ERROR';

export const GET_STATE_CITIES = 'app/UsersPage/GET_STATE_CITIES';
export const GET_STATE_CITIES_ERROR = 'app/UsersPage/GET_STATE_CITIES_ERROR';
export const GET_STATE_CITIES_SUCCESS =
  'app/UsersPage/GET_STATE_CITIES_SUCCESS';

export const ENABLE_USER = 'app/UsersPage/ENABLE_USER';
export const ENABLE_USER_ERROR = 'app/UsersPage/ENABLE_USER_ERROR';
export const ENABLE_USER_SUCCESS = 'app/UsersPage/ENABLE_USER_SUCCESS';

export const FORGOT_PASSWORD = 'app/UsersPage/FORGOT_PASSWORD';
export const FORGOT_PASSWORD_ERROR = 'app/UsersPage/FORGOT_PASSWORD_ERROR';
export const FORGOT_PASSWORD_SUCCESS = 'app/UsersPage/FORGOT_PASSWORD_SUCCESS';

export const EXPORT_CSV = 'app/UsersPage/EXPORT_CSV';
export const EXPORT_CSV_SUCCESS = 'app/UsersPage/EXPORT_CSV_SUCCESS';
export const EXPORT_CSV_ERROR = 'app/UsersPage/EXPORT_CSV_ERROR';

export const GET_PAYEE_USER = 'app/UsersPage/GET_PAYEE_USER';
export const GET_PAYEE_USER_SUCCESS = 'app/UsersPage/GET_PAYEE_USER_SUCCESS';
export const GET_PAYEE_USER_ERROR = 'app/UsersPage/GET_PAYEE_USER_ERROR';

export const SEARCH_PAYEE = 'app/UsersPage/SEARCH_PAYEE';
export const SEARCH_PAYEE_SUCCESS = 'app/UsersPage/SEARCH_PAYEE_SUCCESS';
export const SEARCH_PAYEE_ERROR = 'app/UsersPage/SEARCH_PAYEE_ERROR';

export const ADD_PAYEE = 'app/UsersPage/ADD_PAYEE';
export const ADD_PAYEE_SUCCESS = 'app/UsersPage/ADD_PAYEE_SUCCESS';
export const ADD_PAYEE_ERROR = 'app/UsersPage/ADD_PAYEE_ERROR';

export const REMOVE_PAYEE = 'app/UsersPage/REMOVE_PAYEE';
export const REMOVE_PAYEE_SUCCESS = 'app/UsersPage/REMOVE_PAYEE_SUCCESS';
export const REMOVE_PAYEE_ERROR = 'app/UsersPage/REMOVE_PAYEE_ERROR';

export const GET_USER_MEMBER = 'app/UsersPage/GET_USER_MEMBER';
export const GET_USER_MEMBER_SUCCESS = 'app/UsersPage/GET_USER_MEMBER_SUCCESS';
export const GET_USER_MEMBER_ERROR = 'app/UsersPage/GET_USER_MEMBER_ERROR';

export const ADD_SUB_MEMBER = 'app/UsersPage/ADD_SUB_MEMBER';
export const ADD_SUB_MEMBER_SUCCESS = 'app/UsersPage/ADD_SUB_MEMBER_SUCCESS';
export const ADD_SUB_MEMBER_ERROR = 'app/UsersPage/ADD_SUB_MEMBER_ERROR';

export const DISABLE_SUB_MEMBER = 'app/UsersPage/DISABLE_SUB_MEMBER';
export const DISABLE_SUB_MEMBER_SUCCESS =
  'app/UsersPage/DISABLE_SUB_MEMBER_SUCCESS';
export const DISABLE_SUB_MEMBER_ERROR =
  'app/UsersPage/DISABLE_SUB_MEMBER_ERROR';

export const ENABLE_SUB_MEMBER = 'app/UsersPage/ENABLE_SUB_MEMBER';
export const ENABLE_SUB_MEMBER_SUCCESS =
  'app/UsersPage/ENABLE_SUB_MEMBER_SUCCESS';
export const ENABLE_SUB_MEMBER_ERROR = 'app/UsersPage/ENABLE_SUB_MEMBER_ERROR';

export const UPDATE_SUB_MEMBER = 'app/UsersPage/UPDATE_SUB_MEMBER';
export const UPDATE_SUB_MEMBER_SUCCESS =
  'app/UsersPage/UPDATE_SUB_MEMBER_SUCCESS';
export const UPDATE_SUB_MEMBER_ERROR =
  'app/UsersPage/UPDATE_SUB_MEMBER_ERROR';
