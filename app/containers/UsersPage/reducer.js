/*
 *
 * UsersPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  GET_USER,
  GET_USER_SUCCESS,
  GET_USER_ERROR,
  ADD_USER,
  ADD_USER_SUCCESS,
  ADD_USER_ERROR,
  EDIT_USER,
  EDIT_USER_SUCCESS,
  EDIT_USER_ERROR,
  DELETE_USER,
  DELETE_USER_SUCCESS,
  DELETE_USER_ERROR,
  PAGE_ACTION,
  PAGE_ACTION_SUCCESS,
  PAGE_ACTION_ERROR,
  GET_FEE_RATES,
  GET_FEE_RATES_SUCCESS,
  GET_FEE_RATES_ERROR,
  UPDATE_FEE_TIER,
  UPDATE_FEE_TIER_SUCCESS,
  UPDATE_FEE_TIER_ERROR,
  GET_STATE_CITIES,
  GET_STATE_CITIES_SUCCESS,
  GET_STATE_CITIES_ERROR,
  ENABLE_USER,
  ENABLE_USER_SUCCESS,
  ENABLE_USER_ERROR,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_ERROR,
  EXPORT_CSV,
  EXPORT_CSV_SUCCESS,
  EXPORT_CSV_ERROR,
  GET_PAYEE_USER,
  GET_PAYEE_USER_SUCCESS,
  GET_PAYEE_USER_ERROR,
  SEARCH_PAYEE,
  SEARCH_PAYEE_SUCCESS,
  SEARCH_PAYEE_ERROR,
  ADD_PAYEE,
  ADD_PAYEE_SUCCESS,
  ADD_PAYEE_ERROR,
  REMOVE_PAYEE,
  REMOVE_PAYEE_SUCCESS,
  REMOVE_PAYEE_ERROR,
  GET_USER_MEMBER,
  GET_USER_MEMBER_ERROR,
  GET_USER_MEMBER_SUCCESS,
  DISABLE_SUB_MEMBER,
  DISABLE_SUB_MEMBER_SUCCESS,
  DISABLE_SUB_MEMBER_ERROR,
  ENABLE_SUB_MEMBER,
  ENABLE_SUB_MEMBER_SUCCESS,
  ENABLE_SUB_MEMBER_ERROR,
  ADD_SUB_MEMBER,
  ADD_SUB_MEMBER_SUCCESS,
  ADD_SUB_MEMBER_ERROR,
  UPDATE_SUB_MEMBER,
  UPDATE_SUB_MEMBER_SUCCESS,
  UPDATE_SUB_MEMBER_ERROR,
} from './constants';

export const initialState = fromJS({
  loading: false,
  error: false,
  userData: [],
  formLoading: false,
  pageNumber: 0,
  count: null,
  nextUrl: null,
  previousUrl: null,
  feeObject: null,
  transactionSuccess: null,
  hasFee: false,
  stateCities: null,
  payeeUser: [],
  payeeUserSearchResult: [],
  payeeUserLoading: false,
  payeeUserError: null,
  payeeLoading: false,
  userMembers: [],
});

function usersPageReducer(state = initialState, action) {
  switch (action.type) {
    case GET_USER:
      return state
        .set('loading', true)
        .set('userData', [])
        .set('error', false);
    case GET_USER_SUCCESS:
      return state
        .set('loading', false)
        .set('userData', action.data.results)
        .set('pageNumber', action.data.count)
        .set('nextUrl', action.data.next)
        .set('previousUrl', action.data.previous)
        .set('count', action.data.count)
        .set('error', false);
    case GET_USER_ERROR:
      return state
        .set('loading', false)
        .set('userData', [])
        .set('error', action.error);
    case ADD_USER:
      return state
        .set('formLoading', true)
        .set('hasFee', false)
        .set('error', false)
        .set('transactionSuccess', null);
    case ADD_USER_SUCCESS:
      return state
        .set('formLoading', false)
        .set('error', false)
        .set('transactionSuccess', true);
    case ADD_USER_ERROR:
      return state
        .set('formLoading', false)
        .set('error', action.error)
        .set('transactionSuccess', null);
    case EDIT_USER:
      return state
        .set('formLoading', true)
        .set('hasFee', false)
        .set('error', false)
        .set('transactionSuccess', null);
    case EDIT_USER_SUCCESS:
      return state
        .set('formLoading', false)
        .set('error', false)
        .set('transactionSuccess', true);
    case EDIT_USER_ERROR:
      return state
        .set('formLoading', false)
        .set('error', action.error)
        .set('transactionSuccess', null);
    case UPDATE_FEE_TIER:
      return state
        .set('formLoading', true)
        .set('hasFee', false)
        .set('error', false)
        .set('transactionSuccess', null);
    case UPDATE_FEE_TIER_SUCCESS:
      return state
        .set('formLoading', false)
        .set('error', false)
        .set('transactionSuccess', true);
    case UPDATE_FEE_TIER_ERROR:
      return state
        .set('formLoading', false)
        .set('error', action.error)
        .set('transactionSuccess', null);
    case DELETE_USER:
      return state
        .set('transactionSuccess', null)
        .set('formLoading', true)
        .set('error', false);
    case DELETE_USER_SUCCESS:
      return state
        .set('transactionSuccess', true)
        .set('formLoading', false)
        .set('error', false);
    case DELETE_USER_ERROR:
      return state.set('formLoading', false).set('error', action.error);
    case PAGE_ACTION:
      return state
        .set('loading', true)
        .set('userData', [])
        .set('error', false);
    case PAGE_ACTION_SUCCESS:
      return state
        .set('loading', false)
        .set('userData', action.data.results)
        .set('pageNumber', action.data.count)
        .set('nextUrl', action.data.next)
        .set('previousUrl', action.data.previous)
        .set('error', false);
    case PAGE_ACTION_ERROR:
      return state
        .set('loading', false)
        .set('userData', [])
        .set('error', action.error);
    case GET_FEE_RATES:
      return state
        .set('hasFee', false)
        .set('transactionSuccess', null)
        .set('error', false)
        .set('feeObject', null);
    case GET_FEE_RATES_SUCCESS:
      return state
        .set('hasFee', true)
        .set('error', false)
        .set('feeObject', action.data);
    case GET_FEE_RATES_ERROR:
      return state
        .set('hasFee', false)
        .set('error', action.error)
        .set('feeObject', null);
    case GET_STATE_CITIES:
      return state
        .set('loading', true)
        .set('error', null)
        .set('stateCities', null);
    case GET_STATE_CITIES_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('stateCities', action.data);
    case GET_STATE_CITIES_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('stateCities', null);
    case ENABLE_USER:
      return state.set('loading', true).set('error', null);
    case ENABLE_USER_SUCCESS:
      return state
        .set('loading', false)
        .set('transactionSuccess', true)
        .set('error', null);
    case ENABLE_USER_ERROR:
      return state.set('loading', false).set('error', action.error);
    case FORGOT_PASSWORD:
      return state.set('loading', true).set('error', null);
    case FORGOT_PASSWORD_SUCCESS:
      return state
        .set('loading', false)
        .set('transactionSuccess', true)
        .set('error', null);
    case FORGOT_PASSWORD_ERROR:
      return state.set('loading', false).set('error', action.error);
    case EXPORT_CSV:
      return state.set('loading', true).set('error', null);
    case EXPORT_CSV_SUCCESS:
      return state.set('loading', false).set('error', null);
    case EXPORT_CSV_ERROR:
      return state.set('loading', false).set('error', action.error);
    case GET_PAYEE_USER:
      return state
        .set('loading', true)
        .set('payeeUserSearchResult', [])
        .set('payeeUser', [])
        .set('error', null);
    case GET_PAYEE_USER_SUCCESS:
      return state
        .set('loading', false)
        .set('payeeUser', action.data)
        .set('error', null);
    case GET_PAYEE_USER_ERROR:
      return state
        .set('loading', false)
        .set('payeeUser', [])
        .set('error', action.error);
    case SEARCH_PAYEE:
      return state
        .set('payeeUserLoading', true)
        .set('payeeUserSearchResult', [])
        .set('payeeUserError', null);
    case SEARCH_PAYEE_SUCCESS:
      return state
        .set('payeeUserLoading', false)
        .set('payeeUserSearchResult', action.data)
        .set('payeeUserError', null);
    case SEARCH_PAYEE_ERROR:
      return state
        .set('payeeUserLoading', false)
        .set('payeeUserSearchResult', [])
        .set('payeeUserError', action.error);
    case ADD_PAYEE:
      return state.set('payeeLoading', true);
    case ADD_PAYEE_SUCCESS:
      return state.set('payeeLoading', false);
    case ADD_PAYEE_ERROR:
      return state.set('payeeLoading', false);
    case REMOVE_PAYEE:
      return state.set('payeeLoading', true);
    case REMOVE_PAYEE_SUCCESS:
      return state.set('payeeLoading', false);
    case REMOVE_PAYEE_ERROR:
      return state.set('payeeLoading', false);
    case GET_USER_MEMBER:
      return state
        .set('userMembers', [])
        .set('loading', true)
        .set('error', null);
    case GET_USER_MEMBER_SUCCESS:
      return state
        .set('userMembers', action.data)
        .set('loading', false)
        .set('error', null);
    case GET_USER_MEMBER_ERROR:
      return state
        .set('userMembers', [])
        .set('loading', false)
        .set('error', action.error);
    case DISABLE_SUB_MEMBER:
      return state.set('loading', true).set('error', null);
    case DISABLE_SUB_MEMBER_SUCCESS:
      return state.set('loading', false).set('error', null);
    case DISABLE_SUB_MEMBER_ERROR:
      return state.set('loading', false).set('error', null);
    case ADD_SUB_MEMBER:
      return state.set('loading', true).set('error', null);
    case ADD_SUB_MEMBER_SUCCESS:
      return state.set('loading', false).set('error', null);
    case ADD_SUB_MEMBER_ERROR:
      return state.set('loading', false).set('error', action.error);
    case ENABLE_SUB_MEMBER:
      return state.set('loading', true).set('error', null);
    case ENABLE_SUB_MEMBER_SUCCESS:
      return state.set('loading', false).set('error', null);
    case ENABLE_SUB_MEMBER_ERROR:
      return state.set('loading', false).set('error', null);
    case UPDATE_SUB_MEMBER:
      return state.set('loading', true).set('error', null);
    case UPDATE_SUB_MEMBER_SUCCESS:
      return state.set('loading', false).set('error', null);
    case UPDATE_SUB_MEMBER_ERROR:
      return state.set('loading', false).set('error', action.error);
    default:
      return state;
  }
}

export default usersPageReducer;
