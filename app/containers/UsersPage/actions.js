/*
 *
 * UsersPage actions
 *
 */

import {
  GET_USER,
  GET_USER_SUCCESS,
  GET_USER_ERROR,
  ADD_USER,
  ADD_USER_SUCCESS,
  ADD_USER_ERROR,
  PAGE_ACTION,
  PAGE_ACTION_SUCCESS,
  PAGE_ACTION_ERROR,
  EDIT_USER,
  EDIT_USER_SUCCESS,
  EDIT_USER_ERROR,
  DELETE_USER,
  DELETE_USER_SUCCESS,
  DELETE_USER_ERROR,
  GET_FEE_RATES,
  GET_FEE_RATES_SUCCESS,
  GET_FEE_RATES_ERROR,
  UPDATE_FEE_TIER,
  UPDATE_FEE_TIER_SUCCESS,
  UPDATE_FEE_TIER_ERROR,
  GET_STATE_CITIES,
  GET_STATE_CITIES_ERROR,
  GET_STATE_CITIES_SUCCESS,
  ENABLE_USER,
  ENABLE_USER_SUCCESS,
  ENABLE_USER_ERROR,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_ERROR,
  EXPORT_CSV,
  EXPORT_CSV_SUCCESS,
  EXPORT_CSV_ERROR,
  GET_PAYEE_USER,
  GET_PAYEE_USER_SUCCESS,
  GET_PAYEE_USER_ERROR,
  SEARCH_PAYEE,
  SEARCH_PAYEE_SUCCESS,
  SEARCH_PAYEE_ERROR,
  ADD_PAYEE,
  ADD_PAYEE_SUCCESS,
  ADD_PAYEE_ERROR,
  REMOVE_PAYEE,
  REMOVE_PAYEE_SUCCESS,
  REMOVE_PAYEE_ERROR,
  GET_USER_MEMBER,
  GET_USER_MEMBER_SUCCESS,
  GET_USER_MEMBER_ERROR,
  DISABLE_SUB_MEMBER,
  DISABLE_SUB_MEMBER_SUCCESS,
  DISABLE_SUB_MEMBER_ERROR,
  ENABLE_SUB_MEMBER,
  ENABLE_SUB_MEMBER_SUCCESS,
  ENABLE_SUB_MEMBER_ERROR,
  ADD_SUB_MEMBER,
  ADD_SUB_MEMBER_SUCCESS,
  ADD_SUB_MEMBER_ERROR,
  UPDATE_SUB_MEMBER,
  UPDATE_SUB_MEMBER_SUCCESS,
  UPDATE_SUB_MEMBER_ERROR,
} from './constants';

export function getUser() {
  return {
    type: GET_USER,
  };
}

export function getUserSuccess(data) {
  return {
    type: GET_USER_SUCCESS,
    data,
  };
}

export function getUserError(error) {
  return {
    type: GET_USER_ERROR,
    error,
  };
}

export function addUser(data) {
  return {
    type: ADD_USER,
    data,
  };
}

export function addUserSuccess(data) {
  return {
    type: ADD_USER_SUCCESS,
    data,
  };
}

export function addUserError(error) {
  return {
    type: ADD_USER_ERROR,
    error,
  };
}

export function pageAction(data) {
  return {
    type: PAGE_ACTION,
    data,
  };
}

export function pageActionSuccess(data) {
  return {
    type: PAGE_ACTION_SUCCESS,
    data,
  };
}

export function pageActionError(error) {
  return {
    type: PAGE_ACTION_ERROR,
    error,
  };
}

export function editUser(data) {
  return {
    type: EDIT_USER,
    data,
  };
}

export function editUserSuccess(data) {
  return {
    type: EDIT_USER_SUCCESS,
    data,
  };
}

export function editUserError(error) {
  return {
    type: EDIT_USER_ERROR,
    error,
  };
}

export function deleteUser(data) {
  return {
    type: DELETE_USER,
    data,
  };
}

export function deleteUserSuccess(data) {
  return {
    type: DELETE_USER_SUCCESS,
    data,
  };
}

export function deleteUserError(error) {
  return {
    type: DELETE_USER_ERROR,
    error,
  };
}

export function getFeeRates(data) {
  return {
    type: GET_FEE_RATES,
    data,
  };
}

export function getFeeRatesSuccess(data) {
  return {
    type: GET_FEE_RATES_SUCCESS,
    data,
  };
}

export function getFeeRatesError(error) {
  return {
    type: GET_FEE_RATES_ERROR,
    error,
  };
}

export function updateFeeTier(data) {
  return {
    type: UPDATE_FEE_TIER,
    data,
  };
}

export function updateFeeTierSuccess(data) {
  return {
    type: UPDATE_FEE_TIER_SUCCESS,
    data,
  };
}

export function updateFeeTierError(error) {
  return {
    type: UPDATE_FEE_TIER_ERROR,
    error,
  };
}

export function getStateCities() {
  return {
    type: GET_STATE_CITIES,
  };
}

export function getStateCitiesSuccess(data) {
  return {
    type: GET_STATE_CITIES_SUCCESS,
    data,
  };
}

export function getStateCitiesError(error) {
  return {
    type: GET_STATE_CITIES_ERROR,
    error,
  };
}

export function enableUser(data) {
  return {
    type: ENABLE_USER,
    data,
  };
}

export function enableUserSuccess(data) {
  return {
    type: ENABLE_USER_SUCCESS,
    data,
  };
}

export function enableUserError(error) {
  return {
    type: ENABLE_USER_ERROR,
    error,
  };
}

export function forgotPassword(data) {
  return {
    type: FORGOT_PASSWORD,
    data,
  };
}

export function forgotPasswordSuccess(data) {
  return {
    type: FORGOT_PASSWORD_SUCCESS,
    data,
  };
}

export function forgotPasswordError(error) {
  return {
    type: FORGOT_PASSWORD_ERROR,
    error,
  };
}

export function exportCSV() {
  return {
    type: EXPORT_CSV,
  };
}

export function exportCSVSuccess(data) {
  return {
    type: EXPORT_CSV_SUCCESS,
    data,
  };
}

export function exportCSVError(error) {
  return {
    type: EXPORT_CSV_ERROR,
    error,
  };
}

export function getUserPayee(data) {
  return {
    type: GET_PAYEE_USER,
    data,
  };
}

export function getUserPayeeSuccess(data) {
  return {
    type: GET_PAYEE_USER_SUCCESS,
    data,
  };
}

export function getUserPayeeError(error) {
  return {
    type: GET_PAYEE_USER_ERROR,
    error,
  };
}

export function searchPayee(data) {
  return {
    type: SEARCH_PAYEE,
    data,
  };
}

export function searchPayeeSuccess(data) {
  return {
    type: SEARCH_PAYEE_SUCCESS,
    data,
  };
}

export function searchPayeeError(error) {
  return {
    type: SEARCH_PAYEE_ERROR,
    error,
  };
}

export function addPayee(data) {
  return {
    type: ADD_PAYEE,
    data,
  };
}

export function addPayeeSuccess(data) {
  return {
    type: ADD_PAYEE_SUCCESS,
    data,
  };
}

export function addPayeeError(error) {
  return {
    type: ADD_PAYEE_ERROR,
    error,
  };
}

export function removePayee(data) {
  return {
    type: REMOVE_PAYEE,
    data,
  };
}

export function removePayeeSuccess(data) {
  return {
    type: REMOVE_PAYEE_SUCCESS,
    data,
  };
}

export function removePayeeError(error) {
  return {
    type: REMOVE_PAYEE_ERROR,
    error,
  };
}

export function getUserMember(data) {
  return {
    type: GET_USER_MEMBER,
    data,
  };
}

export function getUserMemberSuccess(data) {
  return {
    type: GET_USER_MEMBER_SUCCESS,
    data,
  };
}

export function getUserMemberError(error) {
  return {
    type: GET_USER_MEMBER_ERROR,
    error,
  };
}

export function disableSubMember(data) {
  return {
    type: DISABLE_SUB_MEMBER,
    data,
  };
}

export function disableSubMemberSuccess(data) {
  return {
    type: DISABLE_SUB_MEMBER_SUCCESS,
    data,
  };
}

export function disableSubMemberError(error) {
  return {
    type: DISABLE_SUB_MEMBER_ERROR,
    error,
  };
}

export function enableSubMember(data) {
  return {
    type: ENABLE_SUB_MEMBER,
    data,
  };
}

export function enableSubMemberSuccess(data) {
  return {
    type: ENABLE_SUB_MEMBER_SUCCESS,
    data,
  };
}

export function enableSubMemberError(error) {
  return {
    type: ENABLE_SUB_MEMBER_ERROR,
    error,
  };
}

export function addSubMember(data) {
  return {
    type: ADD_SUB_MEMBER,
    data,
  };
}

export function addSubMemberSuccess(data) {
  return {
    type: ADD_SUB_MEMBER_SUCCESS,
    data,
  };
}

export function addSubMemberError(error) {
  return {
    type: ADD_SUB_MEMBER_ERROR,
    error,
  };
}

export function updateSubMember(data) {
  return {
    type: UPDATE_SUB_MEMBER,
    data,
  };
}

export function updateSubMemberSuccess(data) {
  return {
    type: UPDATE_SUB_MEMBER_SUCCESS,
    data,
  };
}

export function updateSubMemberError(error) {
  return {
    type: UPDATE_SUB_MEMBER_ERROR,
    error,
  };
}
