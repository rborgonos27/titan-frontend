import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the usersPage state domain
 */

const selectUsersPageDomain = state => state.get('usersPage', initialState);
const selectGlobal = state => state.get('global');

/**
 * Other specific selectors
 */

/**
 * Default selector used by UsersPage
 */

const makeSelectUsersPage = () =>
  createSelector(selectUsersPageDomain, substate => substate.toJS());

const makeSelectFeeObject = () =>
  createSelector(selectUsersPageDomain, substate => substate.get('feeObject'));

const makeSelectCurrentUser = () =>
  createSelector(selectGlobal, globalState => globalState.get('userData'));

export default makeSelectUsersPage;
export { selectUsersPageDomain, makeSelectFeeObject, makeSelectCurrentUser };
