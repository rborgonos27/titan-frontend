/**
 *
 * UsersPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import ContainerWrapper from 'components/ContainerWrapper';
import SimpleModal from 'components/SimpleModal';
import CardBox from 'components/CardBox';
import { Loading, RequiredText, HelpText, FormInput } from 'components/Utils';
import Gravatar from 'react-gravatar';
import {
  Table,
  Tab,
  Button,
  Icon,
  Popup,
  Modal,
  Form,
  Label,
  Input,
  Grid,
  Dropdown,
  Pagination,
  Header,
  TextArea,
  Message,
  Divider,
} from 'semantic-ui-react';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { getParams } from 'utils/helper';
import { formatMoney } from 'utils/formatMoney';
import StandardModal from 'components/StandardModal';
import {
  getUser,
  addUser,
  editUser,
  deleteUser,
  pageAction,
  getFeeRates,
  updateFeeTier,
  getStateCities,
  enableUser,
  forgotPassword,
  exportCSV,
  getUserPayee,
  searchPayee,
  addPayee,
  removePayee,
  getUserMember,
  disableSubMember,
  addSubMember,
  enableSubMember,
  updateSubMember,
} from './actions';
import PayeeList from './PayeeList';
import makeSelectUsersPage, {
  makeSelectFeeObject,
  makeSelectCurrentUser,
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export const ADD = 'ADD';
export const EDIT = 'EDIT';
/* eslint-disable react/prefer-stateless-function */
export class UsersPage extends React.Component {
  state = {
    mode: '',
    openInputModal: false,
    openModalDelete: false,
    openModalView: false,
    locationId: '',
    openModalRestore: false,
    openModalResetPassword: false,
    actionName: '',
    recordAction: '',
    userId: '',
    email: '',
    passwordInput: '',
    address: '',
    address2: '',
    city: '',
    state: '',
    zipCode: '',
    businessContactNumber: '',
    firstName: '',
    lastName: '',
    username: '',
    primaryContactName: '',
    primaryContactNumber: '',
    businessName: '',
    principalCityOfBusiness: '',
    userType: 'temporary',
    memberAccountNumber: '',
    memberUserId: '',
    column: null,
    direction: null,
    search: '',
    previousUserType: '',
    isFirstTimeUser: false,
    depositRate: null,
    depositThreshold: null,
    paymentRate: null,
    paymentThreshold: null,
    taxPaymentRate: null,
    payrollPaymentRate: null,
    feeObject: null,
    feeTier: [],
    openRates: false,
    openModalAddTier: false,
    openModalEditTier: false,
    tempPaymentRate: 0,
    tempPaymentTreshold: 0,
    tempDepositRate: 0,
    tempDepositTreshold: 0,
    tierNo: null,
    tierId: null,
    feeId: null,
    editTierFee: false,
    tier1DepositThreshold: 0,
    tier1DepositRate: 0,
    tier1PaymentThreshold: 0,
    tier1PaymentRate: 0,
    tier2DepositThreshold: 0,
    tier2DepositRate: 0,
    tier2PaymentThreshold: 0,
    tier2PaymentRate: 0,
    tier3DepositThreshold: 0,
    tier3DepositRate: 0,
    tier3PaymentThreshold: 0,
    tier3PaymentRate: 0,
    tier4DepositThreshold: 0,
    tier4DepositRate: 0,
    tier4PaymentThreshold: 0,
    tier4PaymentRate: 0,
    tier5DepositThreshold: 0,
    tier5DepositRate: 0,
    tier5PaymentThreshold: 0,
    tier5PaymentRate: 0,
    depositFlatRate: 0,
    paymentFlatRate: 0,
    feeSetting: 'flat_rate',
    isEditTier: false,
    isEditFee: false,
    errorModal: false,
    deletedAt: null,
    searchInput: '',
    openSubUserDelete: false,
    openSubUserEnable: false,
    selectedSubUser: null,
    openSubMemberInput: false,
    openSubMemberEnable: false,
    subMemberEmailInput: '',
    subMemberPasswordInput: '',
    subMemberFirstName: '',
    subMemberLastName: '',
    subMemberContactNo: '',
  };
  componentDidMount() {
    const { currentUser } = this.props;
    if (currentUser.user_type === 'member') {
      this.props.history.push('/');
    }
    this.props.getStateCities();
    this.props.getUser();
  }
  componentWillReceiveProps(nextProps) {
    const {
      transactionSuccess,
      hasFee,
      feeObject,
      error,
    } = nextProps.userspage;
    if (error) {
      this.setState({ errorModal: true });
    }
    this.setState({ openSubUserDelete: false });
    this.clearSubUserInput(nextProps);
    if (transactionSuccess) {
      this.setState({
        openModalView: false,
        openInputModal: false,
        openModalDelete: false,
        openModalRestore: false,
        openModalResetPassword: false,
      });
    }
    if (feeObject) {
      const { feeTier } = feeObject;
      if (feeTier) {
        this.setState({
          feeTier,
          editTierFee: false,
          tier1DepositThreshold: feeTier[0].deposit_threshold,
          tier1DepositRate: (feeTier[0].deposit_rate * 100).toFixed(2),
          tier1PaymentThreshold: feeTier[0].payment_threshold,
          tier1PaymentRate: (feeTier[0].payment_rate * 100).toFixed(2),
          tier2DepositThreshold: feeTier[1].deposit_threshold,
          tier2DepositRate: (feeTier[1].deposit_rate * 100).toFixed(2),
          tier2PaymentThreshold: feeTier[1].payment_threshold,
          tier2PaymentRate: (feeTier[1].payment_rate * 100).toFixed(2),
          tier3DepositThreshold: feeTier[2].deposit_threshold,
          tier3DepositRate: (feeTier[2].deposit_rate * 100).toFixed(2),
          tier3PaymentThreshold: feeTier[2].payment_threshold,
          tier3PaymentRate: (feeTier[2].payment_rate * 100).toFixed(2),
          tier4DepositThreshold: feeTier[3].deposit_threshold,
          tier4DepositRate: (feeTier[3].deposit_rate * 100).toFixed(2),
          tier4PaymentThreshold: feeTier[3].payment_threshold,
          tier4PaymentRate: (feeTier[3].payment_rate * 100).toFixed(2),
          tier5DepositThreshold: feeTier[4].deposit_threshold,
          tier5DepositRate: (feeTier[4].deposit_rate * 100).toFixed(2),
          tier5PaymentThreshold: feeTier[4].payment_threshold,
          tier5PaymentRate: (feeTier[4].payment_rate * 100).toFixed(2),
        });
      }
    }
    if (hasFee) {
      this.setState({
        feeObject,
        payrollPaymentRate: (feeObject.fee.payroll_payment_rate * 100).toFixed(
          2,
        ),
        taxPaymentRate: (feeObject.fee.tax_payment_rate * 100).toFixed(2),
        depositFlatRate: (feeObject.fee.deposit_flat_rate * 100).toFixed(2),
        paymentFlatRate: (feeObject.fee.payment_flat_rate * 100).toFixed(2),
        feeSetting: feeObject.fee.fee_setting,
        feeId: feeObject.fee.id,
      });
    }
  }
  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  disableAddButton = () => {
    const { userMembers } = this.props.userspage;
    return userMembers.length >= 4;
  };
  getOnContinueAction = () => {
    const { mode } = this.state;
    switch (mode) {
      case ADD:
        this.runAddMember();
        break;
      case EDIT:
        this.runUpdateMember();
        break;
      default:
        break;
    }
  };
  runAddMember = () => {
    const data = this.state;
    this.props.addSubMember(data);
  };
  runUpdateMember = () => {
    const data = this.state;
    this.props.updateSubMember(data);
  };
  getOnContinueAction = () => {
    const { mode } = this.state;
    switch (mode) {
      case ADD:
        this.runAddMember();
        break;
      case EDIT:
        this.runUpdateMember();
        break;
      default:
        break;
    }
  };
  buildInputSubMember = () => {
    const {
      openSubMemberInput,
      subMemberEmailInput,
      subMemberPasswordInput,
      subMemberFirstName,
      subMemberLastName,
      subMemberContactNo,
      mode,
    } = this.state;
    const { loading } = this.props.userspage;
    return (
      <Modal open={openSubMemberInput}>
        <Modal.Header>
          {mode === ADD ? 'Register User' : 'Update User'}
        </Modal.Header>
        <Modal.Content>
          <Form loading={loading}>
            <Grid>
              <Grid.Row>
                <Grid.Column width={8}>
                  <FormInput
                    caption="Enter Email"
                    required
                    name="subMemberEmailInput"
                    handleInput={this.handleInput}
                    placeholder="Enter Email"
                    value={subMemberEmailInput}
                  />
                </Grid.Column>
                <Grid.Column width={8}>
                  <FormInput
                    caption="Enter Password"
                    required={mode === ADD}
                    name="subMemberPasswordInput"
                    handleInput={this.handleInput}
                    placeholder="Enter Password"
                    type="password"
                    value={subMemberPasswordInput}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={8}>
                  <FormInput
                    caption="Enter First Name"
                    required
                    name="subMemberFirstName"
                    handleInput={this.handleInput}
                    placeholder="Enter First Name"
                    value={subMemberFirstName}
                  />
                </Grid.Column>
                <Grid.Column width={8}>
                  <FormInput
                    caption="Enter Last Name"
                    required
                    name="subMemberLastName"
                    handleInput={this.handleInput}
                    placeholder="Enter Last Name"
                    value={subMemberLastName}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={8}>
                  <FormInput
                    caption="Enter Contact No"
                    name="subMemberContactNo"
                    handleInput={this.handleInputNumber}
                    placeholder="Enter Last Name"
                    value={subMemberContactNo}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button
            negative
            onClick={() => this.setState({ openSubMemberInput: false })}
          >
            Cancel
          </Button>
          <Button
            color="orange"
            onClick={() => this.getOnContinueAction()}
            disabled={this.disableSubMemberSubmitButton()}
          >
            Submit
          </Button>
        </Modal.Actions>
      </Modal>
    );
  };
  disableSubMemberSubmitButton = () => {
    const {
      subMemberEmailInput,
      subMemberpasswordInput,
      subMemberfirstName,
      subMemberLastName,
      mode,
    } = this.state;
    if (mode === ADD)
      return (
        subMemberEmailInput === '' ||
        subMemberpasswordInput === '' ||
        subMemberfirstName === '' ||
        subMemberLastName === ''
      );
    else if (mode === EDIT)
      return (
        subMemberEmailInput === '' ||
        subMemberfirstName === '' ||
        subMemberLastName === ''
      );
    return false;
  };
  handleInputNumber = e => {
    /* eslint-disable */
    const x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
    this.setState({ [e.target.name]: e.target.value });
    /* eslint-enable */
  };
  prepareAddMember = () => {
    const mode = ADD;
    const openSubMemberInput = true;
    this.clearSubUserInput(null);
    this.setState({ openSubMemberInput, mode });
  };
  updateFeeTier = () => {
    const {
      feeTier,
      tier1DepositThreshold,
      tier1DepositRate,
      tier1PaymentThreshold,
      tier1PaymentRate,
      tier2DepositThreshold,
      tier2DepositRate,
      tier2PaymentThreshold,
      tier2PaymentRate,
      tier3DepositThreshold,
      tier3DepositRate,
      tier3PaymentThreshold,
      tier3PaymentRate,
      tier4DepositThreshold,
      tier4DepositRate,
      tier4PaymentThreshold,
      tier4PaymentRate,
      tier5DepositThreshold,
      tier5DepositRate,
      tier5PaymentThreshold,
      tier5PaymentRate,
    } = this.state;
    feeTier[0].deposit_threshold = tier1DepositThreshold;
    feeTier[0].deposit_rate = tier1DepositRate;
    feeTier[0].payment_threshold = tier1PaymentThreshold;
    feeTier[0].payment_rate = tier1PaymentRate;
    feeTier[1].deposit_threshold = tier2DepositThreshold;
    feeTier[1].deposit_rate = tier2DepositRate;
    feeTier[1].payment_threshold = tier2PaymentThreshold;
    feeTier[1].payment_rate = tier2PaymentRate;
    feeTier[2].deposit_threshold = tier3DepositThreshold;
    feeTier[2].deposit_rate = tier3DepositRate;
    feeTier[2].payment_threshold = tier3PaymentThreshold;
    feeTier[2].payment_rate = tier3PaymentRate;
    feeTier[3].deposit_threshold = tier4DepositThreshold;
    feeTier[3].deposit_rate = tier4DepositRate;
    feeTier[3].payment_threshold = tier4PaymentThreshold;
    feeTier[3].payment_rate = tier4PaymentRate;
    feeTier[4].deposit_threshold = tier5DepositThreshold;
    feeTier[4].deposit_rate = tier5DepositRate;
    feeTier[4].payment_threshold = tier5PaymentThreshold;
    feeTier[4].payment_rate = tier5PaymentRate;
    const isEditTier = this.checkIfEditFeeTier(feeTier);
    this.setState({ feeTier, isEditTier });
  };
  checkIfEditFeeTier = feeTier => {
    const { feeObject } = this.props.userspage;
    const originalFeeTiers = feeObject.feeTier;
    /* eslint-disable */
    for (let i = 0; i < originalFeeTiers.length; i++) {
      const origFeeTier = originalFeeTiers[i];
      const newFeeTIer = feeTier[i];
      if (origFeeTier.payment_threshold !== newFeeTIer.payment_threshold) {
        return true;
      }
      if (origFeeTier.payment_rate !== newFeeTIer.payment_rate) {
        return true;
      }
      if (origFeeTier.deposit_rate !== newFeeTIer.deposit_rate) {
        return true;
      }
      if (origFeeTier.deposit_threshold !== newFeeTIer.deposit_threshold) {
        return true;
      }
    }
    /* eslint-enable */
    return false;
  };
  displayData = user => {
    this.props.getFeeRates(user.id);
    this.fillStateData(user);
    this.props.getUserPayee(user.id);
    this.props.getUserMember(user.id);
    this.setState({ actionName: 'User Details', openModalView: true });
  };
  getTierData = () => {
    const { feeTier } = this.state;
    /* eslint-disable */
    const row = [];
    let previousDepositThreshold = 0;
    let previousPaymentThreshold = 0;
    for (const key in feeTier) {
      const feeData = feeTier[key];
      const depositRate = feeData.deposit_rate * 100;
      const paymentRate = feeData.payment_rate * 100;
      const depositThreshold = feeData.deposit_threshold ? `$ ${formatMoney(feeData.deposit_threshold)}` : ' above';
      const paymentThreshold = feeData.payment_threshold ? `$ ${formatMoney(feeData.payment_threshold)}` : ' above';
      const rowData = (
        <Table.Row key={key} id={feeData.id}>
          <Table.Cell style={{ textAlign: 'center' }}>
            {depositRate.toFixed(2)} %
          </Table.Cell>
          <Table.Cell style={{ textAlign: 'left' }}>
          {`$ ${formatMoney(previousDepositThreshold)} <= ${depositThreshold}`}
          </Table.Cell>
          <Table.Cell style={{ textAlign: 'center' }}>
            {paymentRate.toFixed(2)} %
          </Table.Cell>
          <Table.Cell style={{ textAlign: 'left' }}>
          {`$ ${formatMoney(previousPaymentThreshold)} <= ${paymentThreshold}`}
          </Table.Cell>
        </Table.Row>
      );
      row.push(rowData);
      previousDepositThreshold = feeData.deposit_threshold;
      previousPaymentThreshold = feeData.payment_threshold;
    }
    return row;
    /* eslint-enable */
  };
  getTierViewData = () => {
    const { feeObject } = this.props;
    if (!feeObject) return row;
    /* eslint-disable */
    const row = [];
    const { feeTier } = feeObject;
    let previousDepositThreshold = 0;
    let previousPaymentThreshold = 0;
    for (const key in feeTier) {
      const feeData = feeTier[key];
      if (feeData.type === 'v2')
        continue;
      const depositRate = feeData.deposit_rate * 100;
      const paymentRate = feeData.payment_rate * 100;
      const depositThreshold = feeData.deposit_threshold ? `$ ${formatMoney(feeData.deposit_threshold)}` : ' above';
      const paymentThreshold = feeData.payment_threshold ? `$ ${formatMoney(feeData.payment_threshold)}` : ' above';
      const rowData = (
        <Table.Row key={key} id={feeData.id} onClick={() => {}}>
          <Table.Cell style={{ textAlign: 'center' }}>
            {depositRate.toFixed(2)} %
          </Table.Cell>
          <Table.Cell style={{ textAlign: 'center' }}>
          {`$ ${formatMoney(previousDepositThreshold)} <= ${depositThreshold}`}
          </Table.Cell>
          <Table.Cell style={{ textAlign: 'center' }}>
            {paymentRate.toFixed(2)} %
          </Table.Cell>
          <Table.Cell style={{ textAlign: 'center' }}>
          {`$ ${formatMoney(previousPaymentThreshold)} <= ${paymentThreshold}`}
          </Table.Cell>
        </Table.Row>
      )
      row.push(rowData);
      previousDepositThreshold = feeData.deposit_threshold;
      previousPaymentThreshold = feeData.payment_threshold;
    }
    return row;
    /* eslint-enable */
  };
  getTierViewDataV2 = () => {
    const { feeObject } = this.props;
    if (!feeObject) return row;
    /* eslint-disable */
    const row = [];
    const { feeTier } = feeObject;
    let previousDepositThreshold = 0;
    let previousPaymentThreshold = 0;
    for (const key in feeTier) {
      const feeData = feeTier[key];
      if (feeData.type === 'v1')
        continue;
      const depositRate = feeData.deposit_rate * 100;
      const paymentRate = feeData.payment_rate * 100;
      const depositThreshold = feeData.deposit_threshold ? `$ ${formatMoney(feeData.deposit_threshold)}` : ' above';
      const paymentThreshold = feeData.payment_threshold ? `$ ${formatMoney(feeData.payment_threshold)}` : ' above';
      const rowData = (
        <Table.Row key={key} id={feeData.id} onClick={() => {}}>
          <Table.Cell style={{ textAlign: 'center' }}>
            {depositRate.toFixed(2)} %
          </Table.Cell>
          <Table.Cell style={{ textAlign: 'center' }}>
          {`$ ${formatMoney(previousDepositThreshold)} <= ${depositThreshold}`}
          </Table.Cell>
          <Table.Cell style={{ textAlign: 'center' }}>
            {paymentRate.toFixed(2)} %
          </Table.Cell>
          <Table.Cell style={{ textAlign: 'center' }}>
          {`$ ${formatMoney(previousPaymentThreshold)} <= ${paymentThreshold}`}
          </Table.Cell>
        </Table.Row>
      )
      row.push(rowData);
      previousDepositThreshold = feeData.deposit_threshold;
      previousPaymentThreshold = feeData.payment_threshold;
    }
    return row;
    /* eslint-enable */
  };
  getUserData = () => {
    const { userData } = this.props.userspage;
    /* eslint-disable */
    let rows = [];
    for (const key in userData) {
      const user = userData[key];
      let userType = 'Administrator';
      switch (user.user_type) {
        case 'super_admin':
          userType = 'Administrator';
          break;
        case 'temporary':
          userType = 'Temporary Member';
          break;
        case 'member':
          userType = 'Member';
          break;
        default:
          userType = 'Administrator';
          break;
      }
      const isDeleted = user.deleted_at ? true : false;
      const rowData = (
        <Table.Row
          negative={isDeleted}
          key={user.id}
          id={user.id}
          onClick={() => this.displayData(user)}
        >
          <Table.Cell>
            {userType}
          </Table.Cell>
          <Table.Cell>
            <Header as='h4' image>
              {user.profile_picture ? (
                  <img alt='' src={user.profile_picture} className="userPageImage" />
                ) : (
                  <Gravatar
                    email={user.email}
                    size={25}
                    rating="pg"
                    default="identicon"
                    className="img-circle"
                  />
                )}
            </Header>
            {`${user.last_name}, ${user.first_name}`}
          </Table.Cell>
          <Table.Cell>{user.username}</Table.Cell>
          <Table.Cell>{user.business_name}</Table.Cell>
          <Table.Cell>
            {(!user.deleted_at) ? (
              <Popup
                trigger={
                  <Button
                    positive
                    onClick={e => {
                      e.preventDefault();
                      e.stopPropagation();
                      this.editRecord(user);
                      this.props.getFeeRates(user.id);
                      this.props.getUserPayee(user.id);
                      this.props.getUserMember(user.id);
                    }}
                  >
                    <Icon name="pencil alternate" />
                  </Button>
                }
                content="Edit this record."
              />
            ) : null}
            {(user.deleted_at) ? (
              <Popup
                trigger={
                  <Button
                    onClick={e => {
                      e.preventDefault();
                      e.stopPropagation();
                      this.enableRecord(user);
                    }}
                  >
                    <Icon name="redo" />
                  </Button>
                }
                content="Enable User."
              />
            ) : (
              <Popup
                trigger={
                  <Button
                    negative
                    onClick={e => {
                      e.preventDefault();
                      e.stopPropagation();
                      this.deleteRecord(user);
                    }}
                  >
                    <Icon name="stop circle" />
                  </Button>
                }
                content="Delete this record."
              />
            )}
          </Table.Cell>
        </Table.Row>
      );
      /* eslint-enable */
      rows.push(rowData);
    }
    return rows;
  };
  addRecord = () => {
    this.clearFormState();
    this.setState({
      openInputModal: true,
      recordAction: 'ADD_RECORD',
      actionName: 'Register User',
    });
  };
  clearFormState = () => {
    this.setState({
      openInputModal: false,
      // openModalDelete: false,
      email: '',
      passwordInput: '',
      firstName: '',
      lastName: '',
      primaryContactName: '',
      primaryContactNumber: '',
      address: '',
      address2: '',
      zipCode: '',
      state: '',
      city: '',
      businessContactNumber: '',
      businessName: '',
      principalCityOfBusiness: '',
      locationId: '',
    });
  };
  fillStateData = data => {
    this.setState({
      userId: data.id,
      email: data.email,
      userType: data.user_type,
      previousUserType: data.user_type,
      userName: data.username,
      firstName: data.first_name,
      lastName: data.last_name,
      primaryContactName: data.primary_contact_name,
      primaryContactNumber: data.primary_contact_number,
      businessName: data.business_name,
      address: data.address,
      address2: data.address_2 ? data.address_2 : '',
      state: this.sentenceCase(data.state),
      city: this.sentenceCase(data.city),
      zipCode: data.zip_code,
      businessContactNumber: data.business_contact_number,
      principalCityOfBusiness: data.principal_city_of_business,
      isFirstTimeUser: data.is_first_time_member,
      deletedAt: data.deleted_at,
      openModalRestore: false,
      memberUserId: data.username,
      memberAccountNumber: data.member_account_id,
      locationId: data.location_id,
    });
  };
  enableRecord = data => {
    this.clearFormState();
    this.fillStateData(data);
    this.setState({
      openModalRestore: true,
      recordAction: 'RESTORE_RECORD',
      actionName: 'Reactivate User',
    });
  };
  resendPasswordLink = data => {
    this.clearFormState();
    this.fillStateData(data);
    this.setState({
      openModalResetPassword: true,
      recordAction: 'RESEND_PASSWORD',
      actionName: 'Resend Password Link',
    });
  };
  editRecord = data => {
    this.clearFormState();
    this.fillStateData(data);
    this.setState({
      openInputModal: true,
      recordAction: 'EDIT_RECORD',
      actionName: 'Edit User',
    });
  };
  deleteRecord = data => {
    this.fillStateData(data);
    this.setState({
      openModalDelete: true,
      recordAction: 'DELETE_RECORD',
      actionName: `Delete ${data.last_name}, ${data.first_name}?`,
    });
  };
  closeModal = () => {
    const { recordAction } = this.state;
    if (recordAction === 'DELETE_RECORD') {
      this.setState({ openModalDelete: false });
    } else {
      this.setState({ openInputModal: false, editTierFee: false });
    }
  };
  submitRecord = () => {
    const { recordAction } = this.state;
    if (recordAction === 'ADD_RECORD') {
      this.props.addUser(this.state);
    } else if (recordAction === 'EDIT_RECORD') {
      this.props.editUser(this.state);
    } else if (recordAction === 'DELETE_RECORD') {
      this.props.deleteUser(this.state);
    }
  };
  submitFeeSettings = () => {
    this.updateFeeTier();
    this.props.updateFeeTier(this.state);
  };
  buildErrorModal = () => {
    const { errorModal } = this.state;
    const { error } = this.props.userspage;
    return (
      <Modal size="tiny" open={errorModal}>
        <Modal.Header>Titan Vault Error</Modal.Header>
        <Modal.Content>
          <Message negative>{error}</Message>
        </Modal.Content>
        <Modal.Actions>
          <Button
            onClick={() => this.setState({ errorModal: false })}
            negative
            icon="warning"
            labelPosition="right"
            content="Got It"
          />
        </Modal.Actions>
      </Modal>
    );
  };
  buildModalDelete = () => {
    const { openModalDelete, actionName } = this.state;
    const { formLoading } = this.props.userspage;
    return (
      <SimpleModal
        open={openModalDelete}
        actionName={actionName}
        cancelAction={() => this.closeModal()}
        submitAction={() => this.submitRecord()}
      >
        <Form loading={formLoading}>
          <p style={{ fontSize: 14 }}>
            Are you sure you want to delete this record?
          </p>
        </Form>
      </SimpleModal>
    );
  };
  buildModalRestore = () => {
    const { openModalRestore, actionName, userId } = this.state;
    const { formLoading } = this.props.userspage;
    return (
      <SimpleModal
        open={openModalRestore}
        actionName={actionName}
        cancelAction={() => this.setState({ openModalRestore: false })}
        submitAction={() => this.props.enableUser(userId)}
      >
        <Form loading={formLoading}>
          <p style={{ fontSize: 14 }}>
            Are you sure you want to restore this record?
          </p>
        </Form>
      </SimpleModal>
    );
  };
  buildModalResendPassword = () => {
    const { openModalResetPassword, actionName, email } = this.state;
    const { formLoading } = this.props.userspage;
    return (
      <Modal size="tiny" open={openModalResetPassword}>
        <Modal.Header>{actionName}</Modal.Header>
        <Modal.Content>
          <Form loading={formLoading}>
            <p style={{ fontSize: 14 }}>
              Are you sure you want to resend password link?
            </p>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button
            onClick={() => this.setState({ openModalResetPassword: false })}
            negative
            icon="warning"
            labelPosition="right"
            content="Cancel"
          />
          <Button
            onClick={() => {
              this.props.forgotPassword(email);
            }}
            color="orange"
            icon="checkmark"
            labelPosition="right"
            content="Submit"
          />
        </Modal.Actions>
      </Modal>
    );
  };
  handleChangeDropDown = (e, { value }) => {
    this.setState({ userType: value });
  };
  handleChangeStateDropDown = (e, { value }) => {
    this.setState({ state: value });
  };
  handleChangeCityDropDown = (e, { value }) => {
    this.setState({ city: value });
  };
  disableSubmitEdit = () =>
    this.state.email === '' ||
    this.state.firstName === '' ||
    this.state.lastName === '' ||
    this.state.primaryContactName === '' ||
    this.state.primaryContactNumber === '' ||
    this.state.businessName === '' ||
    this.state.principalCityOfBusiness === '';
  disableSubmit = () =>
    this.disableSubmitEdit() || this.state.passwordInput === '';
  closeModalTier = () => this.setState({ openModalEditTier: false });
  fillModalTier = data => {
    const depositRate = data.deposit_rate * 100;
    const paymentRate = data.payment_rate * 100;
    this.setState({
      tierId: data.id,
      tierNo: data.tier_no,
      depositRate: depositRate.toFixed(2),
      depositThreshold: data.deposit_threshold,
      paymentRate: paymentRate.toFixed(2),
      paymentThreshold: data.payment_threshold,
      openModalEditTier: true,
    });
  };
  buildModalEditTier = () => {
    const { openModalEditTier, formLoading } = this.state;
    const textStyle = { fontSize: '1.2em' };
    const depositThresholdInput = this.state.depositThreshold ? (
      <div>
        <Header as="h3">Deposit Threshold</Header>
        <p>
          <Input
            style={textStyle}
            name="depositThreshold"
            value={this.state.depositThreshold}
            onChange={this.handleInput}
            autoComplete="off"
            type="number"
            step={0}
            placeholder="Deposit Threshold..."
          />
        </p>
      </div>
    ) : null;
    const paymentThresholdInput = this.state.paymentThreshold ? (
      <div>
        <Header as="h3">Payment Threshold</Header>
        <p>
          <Input
            style={textStyle}
            name="paymentThreshold"
            value={this.state.paymentThreshold}
            onChange={this.handleInput}
            autoComplete="off"
            type="number"
            step={0}
            placeholder="Payment Threshold..."
          />
        </p>
      </div>
    ) : null;
    return (
      <Modal size="tiny" open={openModalEditTier}>
        <Modal.Header>Update Tier</Modal.Header>
        <Modal.Content>
          <Form loading={formLoading}>
            <Grid size="large" centered divided columns={2}>
              <Grid.Column textAlign="left">
                <Header as="h3">Deposit Rate</Header>
                <p>
                  <Input
                    style={textStyle}
                    name="depositRate"
                    value={this.state.depositRate}
                    onChange={this.handleInput}
                    autoComplete="off"
                    type="number"
                    step={0}
                    placeholder="Deposit Rate..."
                  />
                </p>
                {depositThresholdInput}
              </Grid.Column>
              <Grid.Column textAlign="left">
                <Header as="h3">Payment Rate</Header>
                <p>
                  <Input
                    style={textStyle}
                    name="paymentRate"
                    value={this.state.paymentRate}
                    onChange={this.handleInput}
                    autoComplete="off"
                    type="number"
                    step={0}
                    placeholder="Payment Rate..."
                  />
                </p>
                {paymentThresholdInput}
              </Grid.Column>
            </Grid>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button
            onClick={() => this.closeModalTier()}
            negative
            icon="warning"
            labelPosition="right"
            content="Cancel"
          />
          <Button
            onClick={() => {
              const {
                feeTier,
                tierNo,
                tierId,
                depositRate,
                depositThreshold,
                paymentRate,
                paymentThreshold,
              } = this.state;
              const depositRatePercent = depositRate / 100;
              const paymentRatePercent = paymentRate / 100;
              const newFeeTier = [];
              /* eslint-disable */
              for (let i = 0; i < feeTier.length; i++) {
                const feeData = feeTier[i];
                if (feeData.tier_no === tierNo) {
                  const newTier = {
                    id: tierId,
                    tier_no: tierNo,
                    deposit_rate: depositRatePercent.toFixed(4),
                    deposit_threshold: depositThreshold,
                    payment_rate: paymentRatePercent.toFixed(4),
                    payment_threshold: paymentThreshold,
                  };
                  newFeeTier.push(newTier);
                } else {
                  newFeeTier.push(feeData);
                }
              }
              /* eslint-enable */
              this.setState({ feeTier: newFeeTier, openModalEditTier: false });
            }}
            color="orange"
            icon="checkmark"
            labelPosition="right"
            content="Save"
          />
        </Modal.Actions>
      </Modal>
    );
  };
  buildModalAddTier = () => {
    const { openModalAddTier, formLoading } = this.state;
    return (
      <Modal size="tiny" open={openModalAddTier}>
        <Modal.Header>Add Tier</Modal.Header>
        <Modal.Content>
          <Form loading={formLoading}>
            <Grid size="large" centered divided columns={2}>
              <Grid.Column textAlign="left">
                <Header as="h3">Deposit Rate</Header>
                <p>
                  <Input
                    name="depositRate"
                    value={this.state.depositRate}
                    onChange={this.handleInput}
                    autoComplete="off"
                    type="number"
                    step={0}
                    placeholder="Deposit Rate..."
                  />
                </p>
                <Header as="h3">Deposit Threshold</Header>
                <p>
                  <Input
                    name="depositThreshold"
                    value={this.state.depositThreshold}
                    onChange={this.handleInput}
                    autoComplete="off"
                    type="number"
                    step={0}
                    placeholder="Deposit Threshold..."
                  />
                </p>
              </Grid.Column>
              <Grid.Column textAlign="left">
                <Header as="h3">Payment Rate</Header>
                <p>
                  <Input
                    name="paymentRate"
                    value={this.state.paymentRate}
                    onChange={this.handleInput}
                    autoComplete="off"
                    type="number"
                    step={0}
                    placeholder="Payment Rate..."
                  />
                </p>
                <Header as="h3">Payment Threshold</Header>
                <p>
                  <Input
                    name="paymentThreshold"
                    value={this.state.paymentThreshold}
                    onChange={this.handleInput}
                    autoComplete="off"
                    type="number"
                    step={0}
                    placeholder="Payment Threshold..."
                  />
                </p>
              </Grid.Column>
            </Grid>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button
            onClick={() => this.closeModalTier()}
            negative
            icon="warning"
            labelPosition="right"
            content="Cancel"
          />
          <Button
            onClick={() => {
              const {
                feeTier,
                depositRate,
                depositThreshold,
                paymentRate,
                paymentThreshold,
              } = this.state;
              const depositRatePercent = depositRate / 100;
              const paymentRatePercent = paymentRate / 100;
              const newTier = {
                id: 0,
                tier_no: 4,
                deposit_rate: depositRatePercent.toFixed(4),
                deposit_threshold: depositThreshold,
                payment_rate: paymentRatePercent.toFixed(4),
                payment_threshold: paymentThreshold,
              };
              feeTier.push(newTier);
              this.setState({ feeTier, openModalAddTier: false });
            }}
            color="orange"
            icon="checkmark"
            labelPosition="right"
            content="Add"
          />
        </Modal.Actions>
      </Modal>
    );
  };
  handleRadioChange = (e, { value }) => this.setState({ feeSetting: value });
  buildModalInput = () => {
    const {
      openInputModal,
      recordAction,
      actionName,
      userType,
      email,
      passwordInput,
      firstName,
      lastName,
      primaryContactName,
      primaryContactNumber,
      businessName,
      principalCityOfBusiness,
      businessContactNumber,
      address,
      address2,
      city,
      state,
      zipCode,
      taxPaymentRate,
      payrollPaymentRate,
      editTierFee,
      tier1DepositThreshold,
      tier1DepositRate,
      tier1PaymentThreshold,
      tier1PaymentRate,
      tier2DepositThreshold,
      tier2DepositRate,
      tier2PaymentThreshold,
      tier2PaymentRate,
      tier3DepositRate,
      tier3PaymentRate,
      tier4DepositThreshold,
      tier4DepositRate,
      tier4PaymentThreshold,
      tier4PaymentRate,
      tier5DepositRate,
      tier5PaymentRate,
      depositFlatRate,
      paymentFlatRate,
      memberAccountNumber,
      memberUserId,
      locationId,
    } = this.state;
    const { error } = this.props.userspage;
    const errorMessage = error ? <Message error>{error}</Message> : null;
    const inputStyle = { width: 150 };
    const cellStyle = { textAlign: 'left' };
    const tier1DepositInput = editTierFee ? (
      <Input
        style={inputStyle}
        name="tier1DepositRate"
        value={tier1DepositRate}
        onChange={this.handleInput}
        autoComplete="off"
        type="number"
        step={0.01}
        placeholder="Deposit Rate..."
      />
    ) : (
      `${parseFloat(tier1DepositRate).toFixed(2)} %`
    );
    const tier2DepositInput = editTierFee ? (
      <Input
        style={inputStyle}
        name="tier2DepositRate"
        value={tier2DepositRate}
        onChange={this.handleInput}
        autoComplete="off"
        type="number"
        step={0.01}
        placeholder="Deposit Rate..."
      />
    ) : (
      `${parseFloat(tier2DepositRate).toFixed(2)} %`
    );
    const tier3DepositInput = editTierFee ? (
      <Input
        style={inputStyle}
        name="tier3DepositRate"
        value={tier3DepositRate}
        onChange={this.handleInput}
        autoComplete="off"
        type="number"
        step={0.01}
        placeholder="Deposit Rate..."
      />
    ) : (
      `${parseFloat(tier3DepositRate).toFixed(2)} %`
    );
    const tier1PaymentInput = editTierFee ? (
      <Input
        style={inputStyle}
        name="tier1PaymentRate"
        value={tier1PaymentRate}
        onChange={this.handleInput}
        autoComplete="off"
        type="number"
        step={0.01}
        placeholder="Deposit Rate..."
      />
    ) : (
      `${parseFloat(tier1PaymentRate).toFixed(2)} %`
    );
    const tier2PaymentInput = editTierFee ? (
      <Input
        style={inputStyle}
        name="tier2PaymentRate"
        value={tier2PaymentRate}
        onChange={this.handleInput}
        autoComplete="off"
        type="number"
        step={0.01}
        placeholder="Deposit Rate..."
      />
    ) : (
      `${parseFloat(tier2PaymentRate).toFixed(2)} %`
    );
    const tier3PaymentInput = editTierFee ? (
      <Input
        style={inputStyle}
        name="tier3PaymentRate"
        value={tier3PaymentRate}
        onChange={this.handleInput}
        autoComplete="off"
        type="number"
        step={0.01}
        placeholder="Deposit Rate..."
      />
    ) : (
      `${parseFloat(tier3PaymentRate).toFixed(2)} %`
    );
    const depositThresholdInput = editTierFee ? (
      <Grid>
        <Grid.Column width={5}>{`$ 0 >= `}</Grid.Column>
        <Grid.Column width={7}>
          <Input
            style={inputStyle}
            name="tier1DepositThreshold"
            value={tier1DepositThreshold}
            onChange={this.handleInput}
            autoComplete="off"
            type="number"
            step={0.01}
            placeholder="Deposit Rate..."
          />
        </Grid.Column>
      </Grid>
    ) : (
      `$ 0 >= $ ${formatMoney(tier1DepositThreshold)}`
    );
    const deposit2ThresholdInput = editTierFee ? (
      <Grid>
        <Grid.Column width={5}>
          {`$ ${formatMoney(tier1DepositThreshold)} >= `}
        </Grid.Column>
        <Grid.Column width={7}>
          <Input
            style={inputStyle}
            name="tier2DepositThreshold"
            value={tier2DepositThreshold}
            onChange={this.handleInput}
            autoComplete="off"
            type="number"
            step={0.01}
            placeholder="Deposit Rate..."
          />
        </Grid.Column>
      </Grid>
    ) : (
      `$ ${formatMoney(tier1DepositThreshold)} >= $ ${formatMoney(
        tier2DepositThreshold,
      )}`
    );
    const paymentThresholdInput = editTierFee ? (
      <Grid>
        <Grid.Column width={5}>{`$ 0 >= `}</Grid.Column>
        <Grid.Column width={7}>
          <Input
            style={inputStyle}
            name="tier1PaymentThreshold"
            value={tier1PaymentThreshold}
            onChange={this.handleInput}
            autoComplete="off"
            type="number"
            step={0.01}
            placeholder="Deposit Rate..."
          />
        </Grid.Column>
      </Grid>
    ) : (
      `$ 0 >= $ ${formatMoney(tier1PaymentThreshold)}`
    );
    const payment2ThresholdInput = editTierFee ? (
      <Grid>
        <Grid.Column width={5}>
          {`$ ${formatMoney(tier1PaymentThreshold)} >= `}
        </Grid.Column>
        <Grid.Column width={7}>
          <Input
            style={inputStyle}
            name="tier2PaymentThreshold"
            value={tier2PaymentThreshold}
            onChange={this.handleInput}
            autoComplete="off"
            type="number"
            step={0.01}
            placeholder="Deposit Rate..."
          />
        </Grid.Column>
      </Grid>
    ) : (
      `$ ${formatMoney(tier1PaymentThreshold)} >= $ ${formatMoney(
        tier2PaymentThreshold,
      )}`
    );
    const tier4DepositInput = editTierFee ? (
      <Input
        style={inputStyle}
        name="tier4DepositRate"
        value={tier4DepositRate}
        onChange={this.handleInput}
        autoComplete="off"
        type="number"
        step={0.01}
        placeholder="Deposit Rate..."
      />
    ) : (
      `${parseFloat(tier4DepositRate).toFixed(2)} %`
    );
    const deposit4ThresholdInput = editTierFee ? (
      <Grid>
        <Grid.Column width={5}>{`$ 0 >= `}</Grid.Column>
        <Grid.Column width={7}>
          <Input
            style={inputStyle}
            name="tier4DepositThreshold"
            value={tier4DepositThreshold}
            onChange={this.handleInput}
            autoComplete="off"
            type="number"
            step={0.01}
            placeholder="Deposit Rate..."
          />
        </Grid.Column>
      </Grid>
    ) : (
      `$ 0 >= $ ${formatMoney(tier4DepositThreshold)}`
    );
    const tier4PaymentInput = editTierFee ? (
      <Input
        style={inputStyle}
        name="tier4PaymentRate"
        value={tier4PaymentRate}
        onChange={this.handleInput}
        autoComplete="off"
        type="number"
        step={0.01}
        placeholder="Deposit Rate..."
      />
    ) : (
      `${parseFloat(tier4PaymentRate).toFixed(2)} %`
    );
    const payment4ThresholdInput = editTierFee ? (
      <Grid>
        <Grid.Column width={5}>{`$ 0 >= `}</Grid.Column>
        <Grid.Column width={7}>
          <Input
            style={inputStyle}
            name="tier4PaymentThreshold"
            value={tier4PaymentThreshold}
            onChange={this.handleInput}
            autoComplete="off"
            type="number"
            step={0.01}
            placeholder="Deposit Rate..."
          />
        </Grid.Column>
      </Grid>
    ) : (
      `$ 0 >= $ ${formatMoney(tier4PaymentThreshold)}`
    );
    const tier5DepositInput = editTierFee ? (
      <Input
        style={inputStyle}
        name="tier5DepositRate"
        value={tier5DepositRate}
        onChange={this.handleInput}
        autoComplete="off"
        type="number"
        step={0.01}
        placeholder="Deposit Rate..."
      />
    ) : (
      `${parseFloat(tier5DepositRate).toFixed(2)} %`
    );
    const tier5PaymentInput = editTierFee ? (
      <Input
        style={inputStyle}
        name="tier5PaymentRate"
        value={tier5PaymentRate}
        onChange={this.handleInput}
        autoComplete="off"
        type="number"
        step={0.01}
        placeholder="Deposit Rate..."
      />
    ) : (
      `${parseFloat(tier5PaymentRate).toFixed(2)} %`
    );
    const { formLoading } = this.props.userspage;
    const userTypes = [
      {
        key: 0,
        text: 'Administrator',
        value: 'super_admin',
      },
      {
        key: 1,
        text: 'Temporary Member',
        value: 'temporary',
      },
      {
        key: 2,
        text: 'Member',
        value: 'member',
      },
    ];
    const disableSubmitButton =
      recordAction === 'EDIT_RECORD'
        ? this.disableSubmitEdit()
        : this.disableSubmit();
    const feeForm = (
      <Form loading={formLoading}>
        <Grid column={2} divided>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Tax Payment Rate (%)</Label>
                <RequiredText>*</RequiredText>
                <Input
                  name="taxPaymentRate"
                  value={taxPaymentRate}
                  onChange={this.handleInput}
                  type="number"
                  autoComplete="off"
                  placeholder="Tax Payment Rate..."
                />
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Payroll Payment Rate (%)</Label>
                <RequiredText>*</RequiredText>
                <Input
                  name="payrollPaymentRate"
                  value={payrollPaymentRate}
                  onChange={this.handleInput}
                  type="number"
                  autoComplete="off"
                  placeholder="Payroll Payment Rate.."
                />
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <Button
                primary
                onClick={() => {
                  this.setState({
                    editTierFee: !editTierFee,
                    tierId: true,
                  });
                }}
              >
                <Icon name="pencil" /> Edit Tier Fee
              </Button>
              <RequiredText>
                CLick the Edit Tier Fee Button to update values.
              </RequiredText>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <br />
        <br />
        <div>
          <h4>Three Tiers</h4>
          <Divider />
          <span
            style={{
              color: this.state.feeSetting === 'fee_tier_v1' ? '#0A0' : '#A00',
              fontWeight: 'bold',
              fontSize: 18,
            }}
          >
            {this.state.feeSetting === 'fee_tier_v1' ? 'Active' : 'Inactive'}
          </span>
          {editTierFee ? (
            <div>
              <br />
              <Button
                positive
                disabled={this.state.feeSetting === 'fee_tier_v1'}
                onClick={() => this.setState({ feeSetting: 'fee_tier_v1' })}
              >
                Set Three Tiers
              </Button>
            </div>
          ) : null}
        </div>
        <Table style={{ fontSize: '1.5em' }} striped celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Deposit Rate</Table.HeaderCell>
              <Table.HeaderCell>Deposit Threshold</Table.HeaderCell>
              <Table.HeaderCell>Payment Rate</Table.HeaderCell>
              <Table.HeaderCell>Payment Threshold</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row>
              <Table.Cell style={cellStyle}>{tier1DepositInput}</Table.Cell>
              <Table.Cell style={cellStyle}>{depositThresholdInput}</Table.Cell>
              <Table.Cell style={cellStyle}>{tier1PaymentInput}</Table.Cell>
              <Table.Cell style={cellStyle}>{paymentThresholdInput}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell style={cellStyle}>{tier2DepositInput}</Table.Cell>
              <Table.Cell style={cellStyle}>
                {deposit2ThresholdInput}
              </Table.Cell>
              <Table.Cell style={cellStyle}>{tier2PaymentInput}</Table.Cell>
              <Table.Cell style={cellStyle}>
                {payment2ThresholdInput}
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell style={cellStyle}>{tier3DepositInput}</Table.Cell>
              <Table.Cell style={cellStyle}>
                {`$ ${formatMoney(tier2DepositThreshold)} and above `}
              </Table.Cell>
              <Table.Cell style={cellStyle}>{tier3PaymentInput}</Table.Cell>
              <Table.Cell style={cellStyle}>
                {`$ ${formatMoney(tier2PaymentThreshold)} and above `}
              </Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
        <br />
        <div>
          <div>
            <h4>Minimum Volume</h4>
            <Divider />
            <span
              style={{
                color:
                  this.state.feeSetting === 'fee_tier_v2' ? '#0A0' : '#A00',
                fontWeight: 'bold',
                fontSize: 18,
              }}
            >
              {this.state.feeSetting === 'fee_tier_v2' ? 'Active' : 'Inactive'}
            </span>
            <br />
            {editTierFee ? (
              <div>
                <br />
                <Button
                  positive
                  disabled={this.state.feeSetting === 'fee_tier_v2'}
                  onClick={() => this.setState({ feeSetting: 'fee_tier_v2' })}
                >
                  Set Minimum Volume
                </Button>
              </div>
            ) : null}
          </div>
          <br />
          <br />
          <Table style={{ fontSize: '1.5em' }} striped celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Deposit Rate</Table.HeaderCell>
                <Table.HeaderCell>Deposit Threshold</Table.HeaderCell>
                <Table.HeaderCell>Payment Rate</Table.HeaderCell>
                <Table.HeaderCell>Payment Threshold</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell style={cellStyle}>{tier4DepositInput}</Table.Cell>
                <Table.Cell style={cellStyle}>
                  {deposit4ThresholdInput}
                </Table.Cell>
                <Table.Cell style={cellStyle}>{tier4PaymentInput}</Table.Cell>
                <Table.Cell style={cellStyle}>
                  {payment4ThresholdInput}
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell style={cellStyle}>{tier5DepositInput}</Table.Cell>
                <Table.Cell style={cellStyle}>
                  {`$ ${formatMoney(tier4DepositThreshold)} and above `}
                </Table.Cell>
                <Table.Cell style={cellStyle}>{tier5PaymentInput}</Table.Cell>
                <Table.Cell style={cellStyle}>
                  {`$ ${formatMoney(tier4PaymentThreshold)} and above `}
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </div>
        <br />
        <div>
          <h4>Fee Flat Rate</h4>
          <Divider />
          <span
            style={{
              color: this.state.feeSetting === 'flat_rate' ? '#0A0' : '#A00',
              fontWeight: 'bold',
              fontSize: 18,
            }}
          >
            {this.state.feeSetting === 'flat_rate' ? 'Active' : 'Inactive'}
          </span>
          <br />
          {editTierFee ? (
            <div>
              <br />
              <Button
                positive
                disabled={this.state.feeSetting === 'flat_rate'}
                onClick={() => this.setState({ feeSetting: 'flat_rate' })}
              >
                Set Flat Rate
              </Button>
            </div>
          ) : null}
        </div>
        <br />
        <br />
        <Grid column={2} divided>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Deposit Flat Rate (%)</Label>
                <RequiredText>*</RequiredText>
                <Input
                  name="depositFlatRate"
                  value={depositFlatRate}
                  onChange={this.handleInput}
                  type="number"
                  autoComplete="off"
                  placeholder="Deposit Flat Rate..."
                />
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Payment Flat Rate (%) </Label>
                <RequiredText>*</RequiredText>
                <Input
                  name="paymentFlatRate"
                  value={paymentFlatRate}
                  onChange={this.handleInput}
                  type="number"
                  autoComplete="off"
                  placeholder="Payment Flat Rate.."
                />
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <div style={{ width: '100%', textAlign: 'left', padding: 10 }}>
              <Button
                onClick={() => this.submitFeeSettings()}
                color="orange"
                icon="checkmark"
                labelPosition="right"
                disabled={!editTierFee}
                content="Submit"
                style={{ float: 'right' }}
              />
              <Button
                onClick={() => this.closeModal()}
                negative
                icon="warning"
                labelPosition="right"
                content="Cancel"
                style={{ float: 'right' }}
              />
            </div>
          </Grid.Row>
        </Grid>
      </Form>
    );
    const inputForm = (
      <Form loading={formLoading}>
        <Grid column={2} divided>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Member Account Number</Label>
                <div className="userViewField">{memberAccountNumber}</div>
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">User Id</Label>
                <div className="userViewField">{memberUserId}</div>
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Select user type</Label>
                <RequiredText>*</RequiredText>
                <Dropdown
                  value={userType}
                  className="userTypesClass"
                  onChange={this.handleChangeDropDown}
                  options={userTypes}
                  placeholder="Choose User Type"
                  selection
                  fluid
                />
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Enter email</Label>
                <RequiredText>*</RequiredText>
                <Input
                  name="email"
                  type="email"
                  value={email}
                  autoComplete="off"
                  onChange={this.handleInput}
                  placeholder="Email..."
                />
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Enter first name</Label>
                <RequiredText>*</RequiredText>
                <Input
                  name="firstName"
                  value={firstName}
                  onChange={this.handleInput}
                  autoComplete="off"
                  placeholder="First Name..."
                />
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Enter password</Label>
                <RequiredText>*</RequiredText>
                <Input
                  name="passwordInput"
                  type="password"
                  value={passwordInput}
                  autoComplete="off"
                  onChange={this.handleInput}
                  placeholder="Password..."
                />
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Enter last name</Label>
                <RequiredText>*</RequiredText>
                <Input
                  name="lastName"
                  value={lastName}
                  onChange={this.handleInput}
                  autoComplete="off"
                  placeholder="Last Name..."
                />
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Enter business name</Label>
                <RequiredText>*</RequiredText>
                <Input
                  name="businessName"
                  value={businessName}
                  onChange={this.handleInput}
                  autoComplete="off"
                  placeholder="Business Name..."
                />
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Enter primary contact name</Label>
                <RequiredText>*</RequiredText>
                <Input
                  name="primaryContactName"
                  value={primaryContactName}
                  onChange={this.handleInput}
                  autoComplete="off"
                  placeholder="Primary Contact Name..."
                />
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Enter principal city of business</Label>
                <RequiredText>*</RequiredText>
                <Input
                  name="principalCityOfBusiness"
                  value={principalCityOfBusiness}
                  onChange={this.handleInput}
                  autoComplete="off"
                  placeholder="Principal City of Business..."
                />
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Enter primary contact number</Label>
                <RequiredText>*</RequiredText>
                <Input
                  name="primaryContactNumber"
                  value={primaryContactNumber}
                  onChange={this.handleInputNumber}
                  autoComplete="off"
                  placeholder="Primary Contact Number..."
                />
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Enter business contact number</Label>
                <Input
                  name="businessContactNumber"
                  value={businessContactNumber}
                  onChange={this.handleInputNumber}
                  autoComplete="off"
                  placeholder="Business Contact Number..."
                />
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Enter Business Address</Label>
                <TextArea
                  name="address"
                  onChange={this.handleInput}
                  rows={3}
                  placeholder="Business Address..."
                  autoComplete="off"
                  value={address}
                />
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Enter Business Address 2</Label>
                <TextArea
                  name="address2"
                  onChange={this.handleInput}
                  rows={3}
                  placeholder="Business Address..."
                  autoComplete="off"
                  value={address2}
                />
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">State</Label>
                <Dropdown
                  options={this.getState()}
                  placeholder="Choose State"
                  className="userTypesClass"
                  search
                  selection
                  fluid
                  autocomplete="off"
                  value={state}
                  onChange={this.handleChangeStateDropDown}
                />
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">City</Label>
                <Dropdown
                  options={this.getCities()}
                  placeholder="Choose City"
                  className="userTypesClass"
                  search
                  selection
                  fluid
                  autocomplete="off"
                  value={city}
                  onChange={this.handleChangeCityDropDown}
                />
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Zip Code</Label>
                <Input
                  name="zipCode"
                  value={zipCode}
                  onChange={this.handleInput}
                  autoComplete="off"
                  placeholder="Input zip code"
                />
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              {recordAction === 'EDIT_RECORD' ? (
                <Form.Field>
                  <Label pointing="below">Location Id</Label>
                  <Input
                    name="locationId"
                    value={locationId}
                    onChange={this.handleInput}
                    autoComplete="off"
                    placeholder="Input LocationId"
                  />
                </Form.Field>
              ) : null}
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <div style={{ width: '100%', textAlign: 'left', padding: 10 }}>
              <Button
                onClick={() => this.submitRecord()}
                color="orange"
                icon="checkmark"
                labelPosition="right"
                disabled={disableSubmitButton}
                content="Submit"
                style={{ float: 'right' }}
              />
              <Button
                onClick={() => this.closeModal()}
                negative
                icon="warning"
                labelPosition="right"
                content="Cancel"
                style={{ float: 'right' }}
              />
            </div>
          </Grid.Row>
        </Grid>
      </Form>
    );
    const panes =
      recordAction === 'EDIT_RECORD'
        ? this.getEditGeneralTab(inputForm, feeForm)
        : this.getGeneralTab(inputForm);
    return (
      <Modal size="large" open={openInputModal}>
        <Modal.Header>{actionName}</Modal.Header>
        <Modal.Content>
          {errorMessage}
          <Tab panes={panes} />
        </Modal.Content>
      </Modal>
    );
  };
  buildModalEnableSubMember = () => {
    const { openSubMemberEnable, selectedSubUser, userId } = this.state;
    const { loading } = this.props.userspage;
    return (
      <SimpleModal
        open={openSubMemberEnable}
        actionName="Enable User"
        cancelAction={() => this.setState({ openSubMemberEnable: false })}
        submitAction={() => {
          const data = {
            adminUserId: userId,
            userId: selectedSubUser.id,
          };
          this.props.enableSubMember(data);
        }}
      >
        <Form loading={loading}>
          <p style={{ fontSize: 14 }}>
            Are you sure you want to enable this record?
          </p>
        </Form>
      </SimpleModal>
    );
  };
  buildModalDeleteSubUser = () => {
    const { openSubUserDelete, selectedSubUser, userId } = this.state;
    const { loading } = this.props.userspage;
    return (
      <SimpleModal
        open={openSubUserDelete}
        actionName="Disable Sub User"
        cancelAction={() => this.setState({ openSubUserDelete: false })}
        submitAction={() => {
          const data = {
            adminUserId: userId,
            userId: selectedSubUser.id,
          };
          this.props.disableSubMember(data);
        }}
      >
        <Form loading={loading}>
          <p style={{ fontSize: 14 }}>
            Are you sure you want to delete this record?
          </p>
        </Form>
      </SimpleModal>
    );
  };
  fillEditMemberData = member => {
    const memberId = member.id;
    const subMemberEmailInput = member.email;
    const subMemberFirstName = member.first_name;
    const subMemberLastName = member.last_name;
    const subMemberContactNo = member.primary_contact_number;
    const mode = EDIT;
    const openSubMemberInput = true;
    this.setState({
      subMemberEmailInput,
      subMemberFirstName,
      subMemberLastName,
      subMemberContactNo,
      mode,
      openSubMemberInput,
      /* eslint-disable */
      memberId,
      /* eslint-enable */
    });
  };
  getUserMembersData = () => {
    const { userMembers } = this.props.userspage;
    const { openModalView } = this.state;
    /* eslint-disable */
    const usersData = [];
    for (let i = 0; i < userMembers.length; i++) {
      const userMember = userMembers[i];
      const isDeleted = userMember.deleted_at ? true : false;
      const row = (
        <Table.Row negative={isDeleted} key={i}>
          <Table.Cell>
            <Header as='h4' image>
              {userMember.profile_picture ? (
                <img alt='' src={userMember.profile_picture} className="userPageImage" />
              ) : (
                <Gravatar
                  email={userMember.email}
                  size={25}
                  rating="pg"
                  default="identicon"
                  className="img-circle"
                />
              )}
            </Header>
            {`${userMember.first_name} ${userMember.last_name}`}
            </Table.Cell>
          <Table.Cell>{`${userMember.username}`}</Table.Cell>
          <Table.Cell>{`${userMember.email}`}</Table.Cell>
          {!openModalView ? (
            <Table.Cell>
              {isDeleted ? null : (
                <Button
                  size="small"
                  onClick={e => {
                    e.preventDefault();
                    e.stopPropagation();
                    this.fillEditMemberData(userMember);
                  }}
                >
                  <Icon name="pencil" />
                  Edit
                </Button>
              )}
              {isDeleted ? (
                <Button
                  size="small"
                  positive
                  onClick={e => {
                    e.preventDefault();
                    e.stopPropagation();
                    this.setState({ selectedSubUser: userMember, openSubMemberEnable: true });
                  }}
                >
                  Enable
                </Button>
              ) : (
                <Button
                  size="small"
                  negative
                  onClick={e => {
                    e.preventDefault();
                    e.stopPropagation();
                    this.setState({ selectedSubUser: userMember, openSubUserDelete: true });
                  }}
                >
                  Disable
                </Button>
              )}
            </Table.Cell>
          ) : null}

        </Table.Row>
      )
      usersData.push(row);
    }
    /* eslint-enable */
    return (
      <div>
        {!openModalView ? (
          <div>
            <Button
              disabled={this.disableAddButton()}
              positive
              onClick={() => this.prepareAddMember()}
            >
              Add User
            </Button>
            <HelpText>Note: You can only add 4 users.</HelpText>
          </div>
        ) : null}
        <Table striped selectable sortable celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>User Name</Table.HeaderCell>
              <Table.HeaderCell>Member Id</Table.HeaderCell>
              <Table.HeaderCell>Email</Table.HeaderCell>
              {!openModalView ? <Table.HeaderCell /> : null}
            </Table.Row>
          </Table.Header>
          <Table.Body>{usersData}</Table.Body>
        </Table>
        {!openModalView ? (
          <Grid>
            <Grid.Row>
              <div style={{ width: '100%', textAlign: 'left', padding: 10 }}>
                <Button
                  onClick={() => this.closeModal()}
                  negative
                  icon="warning"
                  labelPosition="right"
                  content="Cancel"
                  style={{ float: 'right' }}
                />
              </div>
            </Grid.Row>
          </Grid>
        ) : null}
      </div>
    );
  };
  getEditGeneralTab = (inputForm, feeForm) => [
    {
      menuItem: 'General',
      render: () => <Tab.Pane>{inputForm}</Tab.Pane>,
    },
    {
      menuItem: 'Fee Rates',
      render: () => <Tab.Pane>{feeForm}</Tab.Pane>,
    },
    {
      menuItem: 'Member User',
      render: () => <Tab.Pane>{this.getUserMembersData()}</Tab.Pane>,
    },
    {
      menuItem: 'Payee',
      render: () => <Tab.Pane>{this.getUserPayee()}</Tab.Pane>,
    },
  ];
  getGeneralTab = inputForm => [
    {
      menuItem: 'General',
      render: () => <Tab.Pane>{inputForm}</Tab.Pane>,
    },
  ];
  validateFee = () => {
    const {
      depositRate,
      depositThreshold,
      paymentRate,
      paymentThreshold,
      payrollPaymentRate,
      taxPaymentRate,
    } = this.state;

    return (
      !depositRate ||
      !depositThreshold ||
      !paymentRate ||
      !paymentThreshold ||
      !payrollPaymentRate ||
      !taxPaymentRate
    );
  };
  resetRate = () => {
    this.setState({
      depositRate: null,
      depositThreshold: null,
      paymentRate: null,
      paymentThreshold: null,
      payrollPaymentRate: null,
      taxPaymentRate: null,
      openRates: false,
    });
  };
  buildModalFee = () => (
    <StandardModal
      headerText="Fee Rates"
      open={this.state.openRates}
      onClose={() => this.resetRate()}
      onActionText="Save"
      onContinue={() => this.setState({ openRates: false })}
      btnDisabled={this.validateFee()}
    >
      <div style={{ textAlign: 'left' }}>
        <Grid size="large" centered divided columns={2}>
          <Grid.Column textAlign="left">
            <Header as="h3">Deposit Rate</Header>
            <p>
              <Input
                name="depositRate"
                value={this.state.depositRate}
                onChange={this.handleInput}
                autoComplete="off"
                type="number"
                step={0}
                placeholder="Deposit Rate..."
              />
            </p>
            <Header as="h3">Deposit Threshold</Header>
            <p>
              <Input
                name="depositThreshold"
                value={this.state.depositThreshold}
                onChange={this.handleInput}
                autoComplete="off"
                type="number"
                step={0}
                placeholder="Deposit Threshold..."
              />
            </p>
            <Header as="h3">Payroll Payment Rate</Header>
            <p>
              <Input
                name="payrollPaymentRate"
                value={this.state.payrollPaymentRate}
                onChange={this.handleInput}
                autoComplete="off"
                type="number"
                step={0}
                placeholder="Payroll Payment Rate..."
              />
            </p>
          </Grid.Column>
          <Grid.Column textAlign="left">
            <Header as="h3">Payment Rate</Header>
            <p>
              <Input
                name="paymentRate"
                value={this.state.paymentRate}
                onChange={this.handleInput}
                autoComplete="off"
                type="number"
                step={0}
                placeholder="Payment Rate..."
              />
            </p>
            <Header as="h3">Payment Threshold</Header>
            <p>
              <Input
                name="paymentThreshold"
                value={this.state.paymentThreshold}
                onChange={this.handleInput}
                autoComplete="off"
                type="number"
                step={0}
                placeholder="Payment Threshold..."
              />
            </p>
            <Header as="h3">Tax Payment Rate (%)</Header>
            <p>
              <Input
                name="taxPaymentRate"
                value={this.state.taxPaymentRate}
                onChange={this.handleInput}
                autoComplete="off"
                type="number"
                step={0}
                placeholder="Tax Payment Rate..."
              />
            </p>
          </Grid.Column>
        </Grid>
      </div>
    </StandardModal>
  );
  getPayeeByFilter = e => {
    const { searchInput } = this.state;
    if (e.keyCode === 13) {
      this.props.searchPayee(searchInput);
    }
  };
  getUserPayeeNotSelected = () => {
    const { payeeUser, payeeUserSearchResult } = this.props.userspage;
    const listPayee = [];
    /* eslint-disable */
    for (let i = 0; i < payeeUserSearchResult.length; i++) {
      const payeeResult = payeeUserSearchResult[i];
      let isExists = false;
      for (let x = 0; x < payeeUser.length; x++) {
        const payee = payeeUser[x];
        if (payee.id === payeeResult.id) {
          isExists = true;
          break;
        }
      }
      if (!isExists) {
        listPayee.push(payeeResult);
      }
    }
    /* eslint-enable */
    return listPayee;
  };
  getUserPayee = () => {
    const {
      payeeUser,
      payeeUserLoading,
      payeeUserSearchResult,
      payeeLoading,
    } = this.props.userspage;
    const { searchInput, openModalView } = this.state;
    if (payeeLoading) {
      return (
        <div style={{ margin: 30 }}>
          <Loading loading={payeeLoading} />
        </div>
      );
    }
    if (!payeeUser) return null;
    const payeeList = payeeUserSearchResult ? (
      <PayeeList
        list={this.getUserPayeeNotSelected()}
        buttonAction={this.addUserPayee}
        action="add"
      />
    ) : null;
    return (
      <div>
        {!openModalView ? (
          <div>
            <NavLink to="/manage/payee">
              <Button positive icon="icon">
                Add Payee
              </Button>
            </NavLink>
            <Divider />
            <Input
              onChange={this.handleInput}
              type="text"
              name="searchInput"
              id="searchInput"
              size="large"
              fluid
              value={searchInput}
              placeholder="Search Member..."
              icon="search"
              onKeyDown={this.getPayeeByFilter}
            />
          </div>
        ) : null}
        {payeeUserLoading ? (
          <div style={{ margin: 30 }}>
            <Loading loading={payeeUserLoading} />
          </div>
        ) : (
          <div>
            <h3>Available Payee</h3>
            <Divider />
            {payeeUserSearchResult.length > 0 ? null : (
              <div style={{ color: '#A00', textAlign: 'center' }}>
                No Payee Available
              </div>
            )}
            {payeeList}
          </div>
        )}
        <h3>Assigned Payee</h3>
        <Divider />
        {payeeUser.length > 0 ? null : (
          <div style={{ color: '#A00', textAlign: 'center' }}>
            No Payee Available
          </div>
        )}
        <PayeeList
          list={payeeUser}
          buttonAction={this.removeUserPayee}
          action="remove"
          isView={openModalView}
        />
        {!openModalView ? (
          <Grid>
            <Grid.Row>
              <div style={{ width: '100%', textAlign: 'left', padding: 10 }}>
                <Button
                  onClick={() => this.closeModal()}
                  negative
                  icon="warning"
                  labelPosition="right"
                  content="Cancel"
                  style={{ float: 'right' }}
                />
              </div>
            </Grid.Row>
          </Grid>
        ) : null}
      </div>
    );
  };
  addUserPayee = payeeId => {
    const { userId, searchInput } = this.state;
    const userData = { userId, payeeId, searchInput };
    this.props.addPayee(userData);
  };
  removeUserPayee = payeeId => {
    const { userId, searchInput } = this.state;
    const userData = { userId, payeeId, searchInput };
    this.props.removePayee(userData);
  };
  buildModalView = () => {
    const {
      openModalView,
      actionName,
      userType,
      email,
      firstName,
      lastName,
      primaryContactName,
      primaryContactNumber,
      businessName,
      principalCityOfBusiness,
      businessContactNumber,
      address,
      address2,
      city,
      state,
      zipCode,
      taxPaymentRate,
      payrollPaymentRate,
      paymentFlatRate,
      depositFlatRate,
      deletedAt,
      memberAccountNumber,
      memberUserId,
      locationId,
    } = this.state;
    const { formLoading } = this.props.userspage;
    const disabledLabel = deletedAt ? (
      <Label as="a" color="red" ribbon>
        Inactive
      </Label>
    ) : null;
    const formView = (
      <Form loading={formLoading}>
        {disabledLabel}
        <Grid column={2}>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Member Account Number</Label>
                <div className="userViewField">{memberAccountNumber}</div>
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">User Id</Label>
                <div className="userViewField">{memberUserId}</div>
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">User Type</Label>
                <div className="userViewField">
                  <b>
                    {/* eslint-disable */
                      userType === 'super_admin'
                        ? 'Super Admin'
                          : userType === 'member'
                          ? 'Member' : 'Temporary User'
                      /* eslint-enable */}
                  </b>
                </div>
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Email</Label>
                <div className="userViewField">
                  <b>{email}</b>
                </div>
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">First Name</Label>
                <div className="userViewField">
                  <b>{firstName}</b>
                </div>
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Enter business name</Label>
                <div className="userViewField">
                  <b>{businessName}</b>
                </div>
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Last Name</Label>
                <div className="userViewField">
                  <b>{lastName}</b>
                </div>
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Principal City of Business</Label>
                <div className="userViewField">
                  <b>{principalCityOfBusiness}</b>
                </div>
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Primary Contact Name</Label>
                <div className="userViewField">
                  <b>{primaryContactName}</b>
                </div>
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Business Contact Number</Label>
                <div className="userViewField">
                  <b>{businessContactNumber}</b>
                </div>
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Contact Number</Label>
                <div className="userViewField">
                  <b>{primaryContactNumber}</b>
                </div>
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Address</Label>
                <div className="userViewField">
                  <b>{address}</b>
                </div>
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Address 2</Label>
                <div className="userViewField">
                  <b>{address2}</b>
                </div>
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">City</Label>
                <div className="userViewField">
                  <b>{state}</b>
                </div>
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">State</Label>
                <div className="userViewField">
                  <b>{city}</b>
                </div>
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Zip Code</Label>
                <div className="userViewField">
                  <b>{zipCode}</b>
                </div>
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Location Id</Label>
                <div className="userViewField">
                  <b>{locationId}</b>
                </div>
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Form>
    );
    const feeForm = (
      <Form loading={formLoading}>
        <Grid column={2} divided>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Tax Payment Rate (%)</Label>
                <span style={{ fontSize: '1.5em', padding: 5 }}>
                  {taxPaymentRate} %
                </span>
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Payroll Payment Rate (%) </Label>
                <span style={{ fontSize: '1.5em', padding: 5 }}>
                  {payrollPaymentRate} %
                </span>
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <h4>Three Tiers</h4>
        <div>
          <Divider />
          <span
            style={{
              color: this.state.feeSetting === 'fee_tier_v1' ? '#0A0' : '#A00',
              fontWeight: 'bold',
              fontSize: 18,
            }}
          >
            {this.state.feeSetting === 'fee_tier_v1' ? 'Active' : 'Inactive'}
          </span>
          <br />
        </div>
        <Table style={{ fontSize: '1.5em' }} striped selectable celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Deposit Rate</Table.HeaderCell>
              <Table.HeaderCell>Deposit Threshold</Table.HeaderCell>
              <Table.HeaderCell>Payment Rate</Table.HeaderCell>
              <Table.HeaderCell>Payment Threshold</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>{this.getTierViewData()}</Table.Body>
        </Table>
        <br />
        <h4>Minimum Volume</h4>
        <div>
          <Divider />
          <span
            style={{
              color: this.state.feeSetting === 'fee_tier_v2' ? '#0A0' : '#A00',
              fontWeight: 'bold',
              fontSize: 18,
            }}
          >
            {this.state.feeSetting === 'fee_tier_v2' ? 'Active' : 'Inactive'}
          </span>
          <br />
        </div>
        <Table style={{ fontSize: '1.5em' }} striped selectable celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Deposit Rate</Table.HeaderCell>
              <Table.HeaderCell>Deposit Threshold</Table.HeaderCell>
              <Table.HeaderCell>Payment Rate</Table.HeaderCell>
              <Table.HeaderCell>Payment Threshold</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>{this.getTierViewDataV2()}</Table.Body>
        </Table>
        <br />
        <h4>Fee Flat Rate</h4>
        <div>
          <Divider />
          <span
            style={{
              color: this.state.feeSetting === 'flat_rate' ? '#0A0' : '#A00',
              fontWeight: 'bold',
              fontSize: 18,
            }}
          >
            {this.state.feeSetting === 'flat_rate' ? 'Active' : 'Inactive'}
          </span>
          <br />
        </div>
        <Grid column={2} divided>
          <Grid.Row>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Deposit Flat Rate (%)</Label>
                <span style={{ fontSize: '1.5em', padding: 5 }}>
                  {depositFlatRate} %
                </span>
              </Form.Field>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form.Field>
                <Label pointing="below">Payment Flat Rate (%) </Label>
                <span style={{ fontSize: '1.5em', padding: 5 }}>
                  {paymentFlatRate} %
                </span>
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Form>
    );
    const userPayee = this.getUserPayee();
    const panes = [
      { menuItem: 'General', render: () => <Tab.Pane>{formView}</Tab.Pane> },
      { menuItem: 'Fee Rates', render: () => <Tab.Pane>{feeForm}</Tab.Pane> },
      {
        menuItem: 'Member User',
        render: () => <Tab.Pane>{this.getUserMembersData()}</Tab.Pane>,
      },
      { menuItem: 'Payee', render: () => <Tab.Pane>{userPayee}</Tab.Pane> },
    ];
    return (
      <Modal size="large" open={openModalView}>
        <Modal.Header>{actionName}</Modal.Header>
        <Modal.Content>
          <Tab panes={panes} />
        </Modal.Content>
        <Modal.Actions>
          {deletedAt ? (
            <Button
              onClick={() => this.setState({ openModalRestore: true })}
              positive
              icon="redo"
              labelPosition="right"
              content="Reactivate"
            />
          ) : (
            <Button
              onClick={() => this.setState({ openModalResetPassword: true })}
              positive
              icon="mail"
              labelPosition="right"
              content="Send Reset Password Link"
            />
          )}
          <Button
            onClick={() => this.setState({ openModalView: false })}
            negative
            icon="warning"
            labelPosition="right"
            content="Close"
          />
        </Modal.Actions>
      </Modal>
    );
  };
  clearSubUserInput = props => {
    this.setState({
      openSubUserDelete: false,
      openSubMemberEnable: false,
      openSubMemberInput: false,
      subMemberEmailInput: '',
      subMemberPasswordInput: '',
      subMemberFirstName: '',
      subMemberLastName: '',
      subMemberContactNo: '',
    });
  };
  setColumnState = (column, direction) => this.setState({ column, direction });
  handleSort = clickedColumn => {
    const { column, direction, search } = this.state;
    if (column !== clickedColumn) {
      this.setColumnState(clickedColumn, 'ascending');
      const pageData = {
        ordering: clickedColumn,
        search,
      };
      this.props.pageAction(pageData);
    } else if (column === clickedColumn) {
      this.setColumnState(
        clickedColumn,
        direction === 'ascending' ? 'descending' : 'ascending',
      );
      const directionSymbol = direction === 'ascending' ? '-' : '';
      const pageData = {
        ordering: `${directionSymbol}${clickedColumn}`,
        search,
      };
      this.props.pageAction(pageData);
    }
  };
  handlePagination = (e, data, limit) => {
    const { search } = this.state;
    const offset = (data.activePage - 1) * limit;
    const pageData = {
      limit,
      offset,
      search,
    };
    this.props.pageAction(pageData);
  };
  getPagination = (numberOfPage, currentPage, limit) =>
    numberOfPage > 0 ? (
      <Pagination
        activePage={currentPage}
        totalPages={numberOfPage}
        onPageChange={(e, pageData) =>
          this.handlePagination(e, pageData, limit)
        }
      />
    ) : null;
  getState = () => {
    const { stateCities } = this.props.userspage;
    const states = [];
    if (!stateCities) return [];
    if (stateCities) {
      const stateKeys = Object.keys(stateCities);
      /* eslint-disable */
      for (let i = 0; i < stateKeys.length; i++) {
        const state = {
          key: stateKeys[i],
          value: stateKeys[i],
          text: stateKeys[i],
        };
        states.push(state);
      }
      /* eslint-enable */
    }
    return states;
  };
  /* eslint-disable */
  sentenceCase = (str) => {
    if ((str===null) || (str===''))
      return false;
    else
      str = str.toString();

    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  };
  /* eslint-enable */
  getCities = () => {
    const { state } = this.state;
    const { stateCities } = this.props.userspage;
    if (!stateCities) return [];
    if (state === '') return [];
    const citiesFromState = stateCities[state];
    if (!citiesFromState) return [];
    /* eslint-disable */
    const cities = [];
    for (let i = 0; i < citiesFromState.length; i++) {
      const cityState = this.sentenceCase(citiesFromState[i]);
      const city = {
        key: cityState,
        value: cityState,
        text: cityState,
      };
      cities.push(city);
    }
    /* eslint-enable */
    return cities;
  };
  shouldComponentUpdate(nextProps, nextState) {
    console.log('shouldComponentUpdate props', nextProps);
    console.log('shouldComponentUpdate state', nextState);
    return true;
  }
  render() {
    const {
      loading,
      pageNumber,
      nextUrl,
      previousUrl,
      error,
      count,
    } = this.props.userspage;
    console.log('logs', this.props.userspage, this.state);
    const { column, direction } = this.state;
    /* eslint-disable */
    const url = nextUrl ? nextUrl : previousUrl;
    const params = !url ? { limit: 0, offset: 0 } : getParams(url);
    const { limit, offset } = params;
    const numberOfPage = limit > 0 ? Math.ceil(pageNumber / limit) : 0;
    let currentPage = nextUrl
      ? offset / limit
      : (parseInt(offset) + parseInt(limit) * 2) / limit;
    currentPage = !offset ? Math.floor(count/limit) + 1 : currentPage;
    const errorMessage = error ? (
      <Message error>{error}</Message>
    ) : null;
    /* eslint-enable */
    return (
      <ContainerWrapper>
        {this.buildModalInput()}
        {this.buildModalDelete()}
        {this.buildModalView()}
        {this.buildModalFee()}
        {this.buildModalAddTier()}
        {this.buildModalEditTier()}
        {this.buildErrorModal()}
        {this.buildModalRestore()}
        {this.buildModalResendPassword()}
        {this.buildModalDeleteSubUser()}
        {this.buildInputSubMember()}
        {this.buildModalEnableSubMember()}
        <div className="row">
          <div className="col-lg-12">
            <CardBox>
              <h4 className="m-t-0 header-title">
                <b>
                  <FormattedMessage {...messages.title} />
                </b>
              </h4>
              <div className="p-20">
                <Loading loading={loading} />
                <Button positive onClick={() => this.addRecord()}>
                  <Icon name="plus" />
                  Register User
                </Button>
                <Button onClick={() => this.props.exportCSV()}>
                  <Icon name="file excel" />
                  Export to CSV
                </Button>
                {errorMessage}
                <Table striped selectable sortable celled>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell
                        sorted={column === 'user_type' ? direction : null}
                        onClick={() => this.handleSort('user_type')}
                      >
                        User Type
                      </Table.HeaderCell>
                      <Table.HeaderCell
                        sorted={column === 'last_name' ? direction : null}
                        onClick={() => this.handleSort('last_name')}
                      >
                        User Name
                      </Table.HeaderCell>
                      <Table.HeaderCell
                        sorted={column === 'username' ? direction : null}
                        onClick={() => this.handleSort('username')}
                      >
                        Member Id
                      </Table.HeaderCell>
                      <Table.HeaderCell
                        sorted={column === 'business_name' ? direction : null}
                        onClick={() => this.handleSort('business_name')}
                      >
                        Member Name
                      </Table.HeaderCell>
                      <Table.HeaderCell>Action</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>{this.getUserData()}</Table.Body>
                </Table>
                {this.getPagination(numberOfPage, currentPage, limit)}
              </div>
            </CardBox>
          </div>
        </div>
      </ContainerWrapper>
    );
  }
}

UsersPage.propTypes = {
  getUser: PropTypes.func.isRequired,
  addUser: PropTypes.func.isRequired,
  editUser: PropTypes.func.isRequired,
  deleteUser: PropTypes.func.isRequired,
  pageAction: PropTypes.func.isRequired,
  getFeeRates: PropTypes.func.isRequired,
  updateFeeTier: PropTypes.func.isRequired,
  getStateCities: PropTypes.func.isRequired,
  enableUser: PropTypes.func.isRequired,
  forgotPassword: PropTypes.func.isRequired,
  exportCSV: PropTypes.func.isRequired,
  getUserPayee: PropTypes.func.isRequired,
  getUserMember: PropTypes.func.isRequired,
  searchPayee: PropTypes.func.isRequired,
  addPayee: PropTypes.func.isRequired,
  disableSubMember: PropTypes.func.isRequired,
  addSubMember: PropTypes.func.isRequired,
  enableSubMember: PropTypes.func.isRequired,
  updateSubMember: PropTypes.func.isRequired,
  removePayee: PropTypes.func.isRequired,
  userspage: PropTypes.any,
  feeObject: PropTypes.any,
  currentUser: PropTypes.any,
  history: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  userspage: makeSelectUsersPage(),
  feeObject: makeSelectFeeObject(),
  currentUser: makeSelectCurrentUser(),
});

const mapDispatchToProps = {
  getUser,
  addUser,
  editUser,
  deleteUser,
  pageAction,
  getFeeRates,
  updateFeeTier,
  getStateCities,
  enableUser,
  forgotPassword,
  exportCSV,
  getUserPayee,
  searchPayee,
  addPayee,
  removePayee,
  getUserMember,
  disableSubMember,
  addSubMember,
  enableSubMember,
  updateSubMember,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'usersPage', reducer });
const withSaga = injectSaga({ key: 'usersPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(UsersPage);
