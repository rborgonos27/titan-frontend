/*
 *
 * PayeePage constants
 *
 */

export const GET_PAYEE = 'app/PayeePage/GET_PAYEE';
export const GET_PAYEE_SUCCESS = 'app/PayeePage/GET_PAYEE_SUCCESS';
export const GET_PAYEE_FAILURE = 'app/PayeePage/GET_PAYEE_FAILURE';

export const ADD_PAYEE = 'app/PayeePage/ADD_PAYEE';
export const ADD_PAYEE_SUCCESS = 'app/PayeePage/ADD_PAYEE_SUCCESS';
export const ADD_PAYEE_FAILURE = 'app/PayeePage/ADD_PAYEE_FAILURE';

export const EDIT_PAYEE = 'app/PayeePage/EDIT_PAYEE';
export const EDIT_PAYEE_SUCCESS = 'app/PayeePage/EDIT_PAYEE_SUCCESS';
export const EDIT_PAYEE_FAILURE = 'app/PayeePage/EDIT_PAYEE_FAILURE';

export const DELETE_PAYEE = 'app/PayeePage/DELETE_PAYEE';
export const DELETE_PAYEE_SUCCESS = 'app/PayeePage/DELETE_PAYEE_SUCCESS';
export const DELETE_PAYEE_FAILURE = 'app/PayeePage/DELETE_PAYEE_FAILURE';

export const PAGE_ACTION = 'app/PayeePage/PAGE_ACTION';
export const PAGE_ACTION_SUCCESS = 'app/PayeePage/PAGE_ACTION_SUCCESS';
export const PAGE_ACTION_FAILURE = 'app/PayeePage/PAGE_ACTION_FAILURE';

export const VIEW_PAYEE = 'app/PayeePage/VIEW_PAYEE';

export const REGISTER_PAYEE = 'app/PayeePage/REGISTER_PAYEE';
export const SET_PAYEE_ID = 'app/PayeePage/SET_PAYEE_ID';

export const ENABLE_PAYEE = 'app/PayeePage/ENABLE_PAYEE';
export const ENABLE_PAYEE_SUCCESS = 'app/PayeePage/ENABLE_PAYEE_SUCCESS';
export const ENABLE_PAYEE_ERROR = 'app/PayeePage/ENABLE_PAYEE_ERROR';
