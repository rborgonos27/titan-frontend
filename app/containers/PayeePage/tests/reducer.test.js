import { fromJS } from 'immutable';
import payeePageReducer from '../reducer';

describe('payeePageReducer', () => {
  it('returns the initial state', () => {
    expect(payeePageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
