import { all, call, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import ReactGA from 'react-ga';
import request from 'utils/request';
import { APP, GA } from 'config';
import { optionsFormData } from 'utils/options';
import { setPayeeId } from 'containers/App/actions';
import { onReset } from 'containers/ManagePayeePage/actions';
import {
  GET_PAYEE,
  ADD_PAYEE,
  EDIT_PAYEE,
  DELETE_PAYEE,
  VIEW_PAYEE,
  REGISTER_PAYEE,
  PAGE_ACTION,
  SET_PAYEE_ID,
  ENABLE_PAYEE,
} from './constants';
import {
  getPayee,
  getPayeeSuccess,
  getPayeeFailure,
  addPayeeSuccess,
  addPayeeFailure,
  editPayeeSuccess,
  editPayeeFailure,
  deletePayeeSuccess,
  deletePayeeFailure,
  pageActionSuccess,
  pageActionFailure,
  enablePayeeError,
  enablePayeeSuccess,
} from './actions';

export function* getPayeeSaga() {
  try {
    const API_URI = APP.API_URL;
    ReactGA.initialize(GA.TRACKING_ID);
    const token = yield JSON.parse(localStorage.getItem('token'));
    ReactGA.event({
      category: 'Payee',
      action: `Getting Payee List`,
      label: 'Payee List',
    });
    const payee = yield call(
      request,
      `${API_URI}/payee_details/`,
      optionsFormData('GET', null, token),
    );
    if (payee.results) {
      ReactGA.event({
        category: 'Payee',
        action: `Getting Payee List Success`,
        label: 'Payee List Success',
      });
      yield put(getPayeeSuccess(payee));
    } else {
      ReactGA.event({
        category: 'Payee',
        action: `Getting Payee List Error ${JSON.stringify(payee)}`,
        label: 'Payee List Error',
      });
      yield put(getPayeeFailure(payee));
    }
  } catch (error) {
    ReactGA.event({
      category: 'Payee',
      action: `Getting Payee List Error ${JSON.stringify(error)}`,
      label: 'Payee List Error',
    });
    yield put(getPayeeFailure(error));
  }
}

export function* addPayee(data) {
  try {
    const API_URI = APP.API_URL;
    ReactGA.initialize(GA.TRACKING_ID);
    const formData = new FormData();
    formData.append('name', data.data.payee_name);
    formData.append('bank_name', data.data.bank_name);
    formData.append('bank_account_name', data.data.bank_account_name);
    formData.append('bank_account', data.data.bank_account);
    formData.append('bank_routing_number', data.data.bank_routing_number);
    formData.append('address', data.data.address);
    const token = yield JSON.parse(localStorage.getItem('token'));
    ReactGA.event({
      category: 'Payee',
      action: `Adding payee`,
      label: 'Add Payee',
    });
    const payeeResponse = yield call(
      request,
      `${API_URI}/payee/`,
      optionsFormData('POST', formData, token),
    );
    if (payeeResponse.name) {
      yield put(addPayeeSuccess(payeeResponse));
      yield put(getPayee());
      ReactGA.event({
        category: 'Payee',
        action: `Adding payee success`,
        label: 'Add Payee Success',
      });
      yield sessionStorage.removeItem('payee_address');
      yield sessionStorage.removeItem('payee_bank_account');
      yield sessionStorage.removeItem('payee_bank_account_name');
      yield sessionStorage.removeItem('payee_bank_name');
      yield sessionStorage.removeItem('payee_bank_routing_number');
      yield sessionStorage.removeItem('payee_payee_name');
    } else {
      ReactGA.event({
        category: 'Payee',
        action: `Adding payee error ${JSON.stringify(payeeResponse)}`,
        label: 'Add Payee Error',
      });
      yield put(addPayeeFailure(payeeResponse));
    }
  } catch (error) {
    ReactGA.event({
      category: 'Payee',
      action: `Adding payee error ${JSON.stringify(error)}`,
      label: 'Add Payee Error',
    });
    yield put(addPayeeFailure(error));
  }
}

export function* pageAction(pageData) {
  const API_URI = APP.API_URL;
  ReactGA.initialize(GA.TRACKING_ID);
  const { limit, offset, search, ordering, searchStatus } = pageData.data;
  let queryString = '';
  let separator = queryString === '' ? '?' : '&';
  queryString = limit
    ? `${queryString}${separator}limit=${limit}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = offset
    ? `${queryString}${separator}offset=${offset}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = search
    ? `${queryString}${separator}search=${search}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = searchStatus
    ? `${queryString}${separator}search=${searchStatus}`
    : queryString;
  separator = queryString === '' ? '?' : '&';
  queryString = ordering
    ? `${queryString}${separator}ordering=${ordering}`
    : queryString;
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const transactions = yield call(
      request,
      `${API_URI}/payee_details/${queryString}`,
      optionsFormData('GET', null, token),
    );
    ReactGA.event({
      category: 'Payee',
      action: `Payee Page Action (pagination, sort and filter)`,
      label: 'Add Payee Page Action',
    });
    if (transactions.results) {
      ReactGA.event({
        category: 'Payee',
        action: `Payee Page Action (pagination, sort and filter) success`,
        label: 'Add Payee Page Action Success',
      });
      yield put(pageActionSuccess(transactions));
    } else {
      ReactGA.event({
        category: 'Payee',
        action: `Payee Page Action (pagination, sort and filter) error ${JSON.stringify(
          transactions,
        )}`,
        label: 'Add Payee Page Action Error',
      });
      yield put(pageActionFailure(transactions));
    }
  } catch (error) {
    ReactGA.event({
      category: 'Payee',
      action: `Payee Page Action (pagination, sort and filter) error ${JSON.stringify(
        error,
      )}`,
      label: 'Add Payee Page Action Error',
    });
    yield put(pageActionFailure(error));
  }
}

export function* editPayee(data) {
  try {
    const API_URI = APP.API_URL;
    ReactGA.initialize(GA.TRACKING_ID);
    const formData = new FormData();
    formData.append('name', data.data.payee_name);
    formData.append('bank_name', data.data.bank_name);
    formData.append('bank_account_name', data.data.bank_account_name);
    formData.append('bank_account', data.data.bank_account);
    formData.append('bank_routing_number', data.data.bank_routing_number);
    formData.append('address', data.data.address);
    const token = yield JSON.parse(localStorage.getItem('token'));
    const payeeResponse = yield call(
      request,
      `${API_URI}/payee/${data.data.payee_id}/`,
      optionsFormData('PUT', formData, token),
    );
    ReactGA.event({
      category: 'Payee',
      action: `Editing Payee`,
      label: 'Edit Payee',
    });
    if (payeeResponse.name) {
      yield put(editPayeeSuccess(payeeResponse));
      yield put(getPayee());
      ReactGA.event({
        category: 'Payee',
        action: `Editing Payee Success`,
        label: 'Edit Payee Success',
      });
      yield sessionStorage.removeItem('payee_address');
      yield sessionStorage.removeItem('payee_bank_account');
      yield sessionStorage.removeItem('payee_bank_account_name');
      yield sessionStorage.removeItem('payee_bank_name');
      yield sessionStorage.removeItem('payee_bank_routing_number');
      yield sessionStorage.removeItem('payee_payee_name');
    } else {
      yield put(editPayeeFailure(payeeResponse));
      ReactGA.event({
        category: 'Payee',
        action: `Editing Payee Error ${JSON.stringify(payeeResponse)}`,
        label: 'Edit Payee Error',
      });
    }
  } catch (error) {
    yield put(editPayeeFailure(error));
    ReactGA.event({
      category: 'Payee',
      action: `Editing Payee Error ${JSON.stringify(error)}`,
      label: 'Edit Payee Error',
    });
  }
}

export function* deletePayee(data) {
  try {
    const API_URI = APP.API_URL;
    ReactGA.initialize(GA.TRACKING_ID);
    const token = yield JSON.parse(localStorage.getItem('token'));
    ReactGA.event({
      category: 'Payee',
      action: `Deleting Payee`,
      label: 'Delete Payee',
    });
    const payeeResponse = yield call(
      request,
      `${API_URI}/payee/${data.data.payee_id}/`,
      optionsFormData('DELETE', null, token),
    );
    if (payeeResponse.success) {
      ReactGA.event({
        category: 'Payee',
        action: `Success Deleting Payee`,
        label: 'Delete Payee Success',
      });
      yield put(deletePayeeSuccess(payeeResponse));
      yield put(editPayeeSuccess(payeeResponse));
      yield put(getPayee());
    } else {
      ReactGA.event({
        category: 'Payee',
        action: `Error Deleting Payee ${JSON.stringify(payeeResponse)}`,
        label: 'Delete Payee Error',
      });
      yield put(deletePayeeFailure(payeeResponse));
    }
  } catch (error) {
    ReactGA.event({
      category: 'Payee',
      action: `Error Deleting Payee ${JSON.stringify(error)}`,
      label: 'Delete Payee Error',
    });
    yield put(deletePayeeFailure(error));
  }
}

export function* viewPayee(requestData) {
  yield put(push(`/payee/${requestData.data}`));
}

export function* registerPayee() {
  yield put(setPayeeId(null));
  yield put(onReset());
  yield put(push('/manage/payee'));
}

export function* runSetPayeeId(payee) {
  yield put(setPayeeId(payee.data));
  yield put(push('/manage/payee'));
}

export function* enablePayee(payee) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const formData = new FormData();
    formData.append('payee_id', payee.data.id);
    const payeeResponse = yield call(
      request,
      `${API_URI}/enable_payee/`,
      optionsFormData('PUT', formData, token),
    );
    if (payeeResponse.success) {
      yield put(enablePayeeSuccess(payeeResponse.data));
      yield put(getPayee());
    } else {
      yield put(enablePayeeError(payeeResponse.message));
    }
  } catch (error) {
    yield put(enablePayeeError(error));
  }
}

export function* getPayeeWatcher() {
  yield takeLatest(GET_PAYEE, getPayeeSaga);
}

export function* addPayeeWatcher() {
  yield takeLatest(ADD_PAYEE, addPayee);
}

export function* editPayeeWatcher() {
  yield takeLatest(EDIT_PAYEE, editPayee);
}

export function* deletePayeeWatcher() {
  yield takeLatest(DELETE_PAYEE, deletePayee);
}

export function* viewRequestWatcher() {
  yield takeLatest(VIEW_PAYEE, viewPayee);
}

export function* registerPayeeWatcher() {
  yield takeLatest(REGISTER_PAYEE, registerPayee);
}

export function* pageActionWatcher() {
  yield takeLatest(PAGE_ACTION, pageAction);
}

export function* setPayeeIdWatcher() {
  yield takeLatest(SET_PAYEE_ID, runSetPayeeId);
}

export function* enablePayeeWatcher() {
  yield takeLatest(ENABLE_PAYEE, enablePayee);
}

export default function* rootSaga() {
  yield all([
    getPayeeWatcher(),
    addPayeeWatcher(),
    editPayeeWatcher(),
    deletePayeeWatcher(),
    viewRequestWatcher(),
    registerPayeeWatcher(),
    pageActionWatcher(),
    setPayeeIdWatcher(),
    enablePayeeWatcher(),
  ]);
}
