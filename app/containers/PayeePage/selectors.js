import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the payeePage state domain
 */

const selectPayeePageDomain = state => state.get('payeePage', initialState);
const selectGlobal = state => state.get('global');

/**
 * Other specific selectors
 */

/**
 * Default selector used by PayeePage
 */

const makeSelectPayeePage = () =>
  createSelector(selectPayeePageDomain, substate => substate.toJS());

const makeSelectCurrentUser = () =>
  createSelector(selectGlobal, globalState => globalState.get('userData'));

export default makeSelectPayeePage;
export { selectPayeePageDomain, makeSelectCurrentUser };
