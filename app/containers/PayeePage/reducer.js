/*
 *
 * PayeePage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  GET_PAYEE,
  GET_PAYEE_SUCCESS,
  GET_PAYEE_FAILURE,
  ADD_PAYEE,
  ADD_PAYEE_SUCCESS,
  ADD_PAYEE_FAILURE,
  EDIT_PAYEE,
  EDIT_PAYEE_SUCCESS,
  EDIT_PAYEE_FAILURE,
  DELETE_PAYEE,
  DELETE_PAYEE_SUCCESS,
  DELETE_PAYEE_FAILURE,
  PAGE_ACTION,
  PAGE_ACTION_SUCCESS,
  PAGE_ACTION_FAILURE,
  ENABLE_PAYEE,
  ENABLE_PAYEE_SUCCESS,
  ENABLE_PAYEE_ERROR,
} from './constants';

export const initialState = fromJS({
  loading: false,
  data: [],
  error: null,
  errorList: [],
  formLoading: false,
  payeeData: null,
  pageNumber: 0,
  nextUrl: null,
  previousUrl: null,
});

function payeePageReducer(state = initialState, action) {
  switch (action.type) {
    case GET_PAYEE:
      return state
        .set('loading', true)
        .set('error', null)
        .set('errorList', []);
    case GET_PAYEE_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('pageNumber', action.data.count)
        .set('data', action.data.results)
        .set('nextUrl', action.data.next)
        .set('previousUrl', action.data.previous);
    case GET_PAYEE_FAILURE:
      return state
        .set('loading', false)
        .set('error', action.data.error)
        .set('errorList', []);
    case ADD_PAYEE:
      return state
        .set('formLoading', true)
        .set('error', null)
        .set('errorList', [])
        .set('payeeData', null);
    case ADD_PAYEE_SUCCESS:
      return state
        .set('formLoading', false)
        .set('error', null)
        .set('errorList', [])
        .set('payeeData', action.data);
    case ADD_PAYEE_FAILURE:
      return state
        .set('formLoading', false)
        .set('error', action.data.error)
        .set('errorList', [])
        .set('payeeData', null);
    case EDIT_PAYEE:
      return state
        .set('formLoading', true)
        .set('error', null)
        .set('errorList', [])
        .set('payeeData', null);
    case EDIT_PAYEE_SUCCESS:
      return state
        .set('formLoading', false)
        .set('error', null)
        .set('errorList', [])
        .set('payeeData', action.data);
    case EDIT_PAYEE_FAILURE:
      return state
        .set('formLoading', false)
        .set('error', action.data.error)
        .set('errorList', [])
        .set('payeeData', null);
    case DELETE_PAYEE:
      return state
        .set('formLoading', true)
        .set('error', null)
        .set('errorList', [])
        .set('payeeData', null);
    case DELETE_PAYEE_SUCCESS:
      return state
        .set('formLoading', false)
        .set('error', null)
        .set('errorList', [])
        .set('payeeData', action.data);
    case DELETE_PAYEE_FAILURE:
      return state
        .set('formLoading', false)
        .set('error', action.data.error)
        .set('errorList', [])
        .set('payeeData', null);
    case PAGE_ACTION:
      return state
        .set('loading', true)
        .set('error', null)
        .set('errorList', []);
    case PAGE_ACTION_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('pageNumber', action.data.count)
        .set('data', action.data.results)
        .set('nextUrl', action.data.next)
        .set('previousUrl', action.data.previous);
    case PAGE_ACTION_FAILURE:
      return state
        .set('loading', false)
        .set('error', action.data.error)
        .set('errorList', []);
    case ENABLE_PAYEE:
      return state.set('loading', true).set('error', null);
    case ENABLE_PAYEE_SUCCESS:
      return state.set('loading', false).set('error', null);
    case ENABLE_PAYEE_ERROR:
      return state.set('loading', false).set('error', action.error);
    default:
      return state;
  }
}

export default payeePageReducer;
