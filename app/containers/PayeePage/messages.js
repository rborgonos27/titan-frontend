/*
 * PayeePage Messages
 *
 * This contains all the text for the PayeePage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.PayeePage.header',
    defaultMessage: 'This is PayeePage container !',
  },
});
