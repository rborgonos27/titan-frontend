/*
 *
 * PayeePage actions
 *
 */

import {
  GET_PAYEE,
  GET_PAYEE_SUCCESS,
  GET_PAYEE_FAILURE,
  ADD_PAYEE,
  ADD_PAYEE_SUCCESS,
  ADD_PAYEE_FAILURE,
  EDIT_PAYEE,
  EDIT_PAYEE_SUCCESS,
  EDIT_PAYEE_FAILURE,
  DELETE_PAYEE,
  DELETE_PAYEE_SUCCESS,
  DELETE_PAYEE_FAILURE,
  VIEW_PAYEE,
  REGISTER_PAYEE,
  PAGE_ACTION,
  PAGE_ACTION_SUCCESS,
  PAGE_ACTION_FAILURE,
  SET_PAYEE_ID,
  ENABLE_PAYEE,
  ENABLE_PAYEE_SUCCESS,
  ENABLE_PAYEE_ERROR,
} from './constants';

export function getPayee() {
  return {
    type: GET_PAYEE,
  };
}

export function getPayeeSuccess(data) {
  return {
    type: GET_PAYEE_SUCCESS,
    data,
  };
}

export function getPayeeFailure(data) {
  return {
    type: GET_PAYEE_FAILURE,
    data,
  };
}

export function addPayee(data) {
  return {
    type: ADD_PAYEE,
    data,
  };
}

export function addPayeeSuccess(data) {
  return {
    type: ADD_PAYEE_SUCCESS,
    data,
  };
}

export function addPayeeFailure(data) {
  return {
    type: ADD_PAYEE_FAILURE,
    data,
  };
}

export function editPayee(data) {
  return {
    type: EDIT_PAYEE,
    data,
  };
}

export function editPayeeSuccess(data) {
  return {
    type: EDIT_PAYEE_SUCCESS,
    data,
  };
}

export function editPayeeFailure(data) {
  return {
    type: EDIT_PAYEE_FAILURE,
    data,
  };
}

export function deletePayee(data) {
  return {
    type: DELETE_PAYEE,
    data,
  };
}

export function deletePayeeSuccess(data) {
  return {
    type: DELETE_PAYEE_SUCCESS,
    data,
  };
}

export function deletePayeeFailure(data) {
  return {
    type: DELETE_PAYEE_FAILURE,
    data,
  };
}

export function viewPayee(data) {
  return {
    type: VIEW_PAYEE,
    data,
  };
}
export function registerPayee() {
  return {
    type: REGISTER_PAYEE,
  };
}

export function pageAction(data) {
  return {
    type: PAGE_ACTION,
    data,
  };
}

export function pageActionSuccess(data) {
  return {
    type: PAGE_ACTION_SUCCESS,
    data,
  };
}

export function pageActionFailure(error) {
  return {
    type: PAGE_ACTION_FAILURE,
    error,
  };
}

export function setPayeeId(data) {
  return {
    type: SET_PAYEE_ID,
    data,
  };
}

export function enablePayee(data) {
  return {
    type: ENABLE_PAYEE,
    data,
  };
}

export function enablePayeeSuccess(data) {
  return {
    type: ENABLE_PAYEE_SUCCESS,
    data,
  };
}

export function enablePayeeError(error) {
  return {
    type: ENABLE_PAYEE_ERROR,
    error,
  };
}
