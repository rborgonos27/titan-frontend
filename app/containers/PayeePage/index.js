/**
 *
 * PayeePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import {
  Table,
  Icon,
  Button,
  Popup,
  Modal,
  Form,
  Label,
  Input,
  TextArea,
  Pagination,
} from 'semantic-ui-react';
import { Loading } from 'components/Utils';
import ContainerWrapper from 'components/ContainerWrapper';
import CardBox from 'components/CardBox';
import { getParams } from 'utils/helper';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectPayeePage, { makeSelectCurrentUser } from './selectors';
import reducer from './reducer';
import saga from './saga';
import {
  getPayee,
  addPayee,
  editPayee,
  deletePayee,
  viewPayee,
  registerPayee,
  pageAction,
  setPayeeId,
  enablePayee,
} from './actions';
// import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export class PayeePage extends React.Component {
  state = {
    openModal: false,
    openModalDelete: false,
    openModalEnable: false,
    payee_id: '',
    recordAction: '',
    actionName: '',
    payee_name: '',
    bank_name: '',
    bank_account: '',
    bank_account_name: '',
    address: '',
    bank_routing_number: '',
    payee: null,
  };
  componentWillMount() {
    /* eslint-disable */
    const { currentUser } = this.props;
    // if (currentUser.user_type === 'sub_member') {
    //   this.props.history.push('/');
    // }
    this.props.getPayee();
    /* eslint-enable */
  }
  componentWillReceiveProps(nextProps) {
    const { payeeData } = nextProps.payeepage;
    if (payeeData) {
      this.clearFormState();
    }
    if (this.state.recordAction === 'DELETE_RECORD' && !payeeData) {
      this.clearFormState();
    }
  }
  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value });
    sessionStorage.setItem(`payee_${e.target.name}`, e.target.value);
  };
  submitRecord = () => {
    const { recordAction } = this.state;
    if (recordAction === 'ADD_RECORD') {
      this.props.addPayee(this.state);
    } else if (recordAction === 'EDIT_RECORD') {
      this.props.editPayee(this.state);
    } else if (recordAction === 'DELETE_RECORD') {
      this.props.deletePayee(this.state);
    }
  };
  clearFormState = () => {
    this.setState({
      openModal: false,
      openModalDelete: false,
      payee_name: '',
      bank_name: '',
      bank_account: '',
      bank_account_name: '',
      address: '',
      bank_routing_number: '',
    });
  };
  getData = data => {
    /* eslint-disable */
    const { currentUser } = this.props;
    console.log('current User', currentUser);
    let rows = [];
    for (const key in data) {
      const payee = data[key];
      const isDeleted = payee.deleted_at;
      const rowData = (
        <Table.Row  negative={isDeleted} key={key} id={payee.id} onClick={() => this.props.viewPayee(payee.id)}>
          <Table.Cell>
            {payee.status === 'REQUESTED' ? <Label ribbon color="red">Requested</Label> : null}
            {payee.name}
          </Table.Cell>
          <Table.Cell>{payee.bank_name}</Table.Cell>
          <Table.Cell>{payee.bank_account_name}</Table.Cell>
          <Table.Cell>{payee.bank_routing_number}</Table.Cell>
          {currentUser.user_type === 'super_admin' ? (
            <Table.Cell>
              {(!payee.deleted_at) ? (
                <Popup
                  trigger={
                    <Button
                      positive
                      onClick={e => {
                        e.preventDefault();
                        e.stopPropagation();
                        this.props.setPayeeId(payee.id);
                      }}
                    >
                      <Icon name="pencil alternate" />
                    </Button>
                  }
                  content="Edit this record."
                />
              ) : null}
              {(payee.deleted_at) ? (
                <Popup
                  trigger={
                    <Button
                      onClick={e => {
                        e.preventDefault();
                        e.stopPropagation();
                        this.enableRecord(payee);
                      }}
                    >
                      <Icon name="redo" />
                    </Button>
                  }
                  content="Enable Payee."
                />
              ) : (
                <Popup
                  trigger={
                    <Button
                      negative
                      onClick={e => {
                        e.preventDefault();
                        e.stopPropagation();
                        this.deleteRecord(payee);
                      }}
                    >
                      <Icon name="stop circle" />
                    </Button>
                  }
                  content="Disable Payee."
                />
              )}
            </Table.Cell>
          ) : null}
        </Table.Row>
      );
      rows.push(rowData);
    }
    return rows;
    /* eslint-enable */
  };
  fillStateData = data => {
    this.setState({
      payee_id: data.id,
      payee_name: data.name,
      bank_name: data.bank_name,
      bank_account: data.bank_account,
      bank_account_name: data.bank_account_name,
      address: data.address,
      bank_routing_number: data.bank_routing_number,
    });
  };
  buildModalInput = () => {
    const { openModal, actionName } = this.state;
    const { formLoading } = this.props.payeepage;
    return (
      <Modal size="small" open={openModal}>
        <Modal.Header>{actionName}</Modal.Header>
        <Modal.Content>
          <Form loading={formLoading}>
            <Form.Field>
              <Label pointing="below">Please enter payee name</Label>
              <Input
                name="payee_name"
                value={this.state.payee_name}
                onChange={this.handleInput}
                placeholder="Payee Name..."
              />
            </Form.Field>
            <Form.Field>
              <Label pointing="below">Please enter bank name</Label>
              <Input
                name="bank_name"
                value={this.state.bank_name}
                onChange={this.handleInput}
                placeholder="Bank Name..."
              />
            </Form.Field>
            <Form.Field>
              <Label pointing="below">Please enter bank account name</Label>
              <Input
                name="bank_account_name"
                value={this.state.bank_account_name}
                onChange={this.handleInput}
                placeholder="Bank Account..."
              />
            </Form.Field>
            <Form.Field>
              <Label pointing="below">Please enter bank account number</Label>
              <Input
                name="bank_account"
                value={this.state.bank_account}
                onChange={this.handleInput}
                placeholder="Bank Account Number..."
              />
            </Form.Field>
            <Form.Field>
              <Label pointing="below">Please enter address</Label>
              <TextArea
                name="address"
                value={this.state.address}
                onChange={this.handleInput}
                row={3}
                placeholder="Address..."
              />
            </Form.Field>
            <Form.Field>
              <Label pointing="below">Please enter bank routing number</Label>
              <Input
                name="bank_routing_number"
                value={this.state.bank_routing_number}
                onChange={this.handleInput}
                placeholder="Bank routing number..."
              />
            </Form.Field>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button
            onClick={() => this.closeModal()}
            negative
            icon="warning"
            labelPosition="right"
            content="Cancel"
          />
          <Button
            onClick={() => this.submitRecord()}
            color="orange"
            icon="checkmark"
            labelPosition="right"
            content="Submit"
            disabled={this.disableSubmit()}
          />
        </Modal.Actions>
      </Modal>
    );
  };
  disableSubmit = () =>
    this.state.payee_name === '' ||
    this.state.bank_name === '' ||
    this.state.bank_account === '' ||
    this.state.bank_account_name === '' ||
    this.state.address === '' ||
    this.state.bank_routing_number === '';
  buildModalDelete = () => {
    const { openModalDelete, actionName } = this.state;
    const { formLoading } = this.props.payeepage;
    return (
      <Modal size="tiny" open={openModalDelete}>
        <Modal.Header>{actionName}</Modal.Header>
        <Modal.Content>
          <Form loading={formLoading}>
            <p style={{ fontSize: 14 }}>
              Are you sure you want to delete this record?
            </p>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button
            onClick={() => this.closeModal()}
            negative
            icon="warning"
            labelPosition="right"
            content="Cancel"
          />
          <Button
            onClick={() => this.submitRecord()}
            color="orange"
            icon="checkmark"
            labelPosition="right"
            content="Submit"
          />
        </Modal.Actions>
      </Modal>
    );
  };
  buildModalEnable = () => {
    const { openModalEnable, actionName, payee } = this.state;
    const { formLoading } = this.props.payeepage;
    return (
      <Modal size="tiny" open={openModalEnable}>
        <Modal.Header>{actionName}</Modal.Header>
        <Modal.Content>
          <Form loading={formLoading}>
            <p style={{ fontSize: 14 }}>
              Are you sure you want to enable this record?
            </p>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button
            onClick={() => this.setState({ openModalEnable: false })}
            negative
            icon="warning"
            labelPosition="right"
            content="Cancel"
          />
          <Button
            onClick={() => {
              this.props.enablePayee(payee);
              this.setState({ openModalEnable: false });
            }}
            color="orange"
            icon="checkmark"
            labelPosition="right"
            content="Submit"
          />
        </Modal.Actions>
      </Modal>
    );
  };
  addRecord = () => {
    this.clearFormState();
    this.setState({
      openModal: true,
      recordAction: 'ADD_RECORD',
      actionName: 'Add Payee',
    });
  };
  editRecord = data => {
    this.fillStateData(data);
    this.setState({
      openModal: true,
      recordAction: 'EDIT_RECORD',
      actionName: 'Edit Payee',
    });
  };
  deleteRecord = data => {
    this.fillStateData(data);
    this.setState({
      openModalDelete: true,
      recordAction: 'DELETE_RECORD',
      actionName: `Delete ${data.name}?`,
    });
  };
  enableRecord = data => {
    this.setState({
      payee: data,
      openModalEnable: true,
      recordAction: 'ENABLE_RECORD',
      actionName: `Enable ${data.name}?`,
    });
  };
  closeModal = () => {
    const { recordAction } = this.state;
    if (recordAction === 'DELETE_RECORD') {
      this.setState({ openModalDelete: false });
    } else {
      this.setState({ openModal: false });
    }
  };
  handlePagination = (e, data, limit) => {
    const { search } = this.state;
    const offset = (data.activePage - 1) * limit;
    const pageData = {
      limit,
      offset,
      search,
    };
    this.props.pageAction(pageData);
  };
  render() {
    const {
      loading,
      data,
      pageNumber,
      nextUrl,
      previousUrl,
    } = this.props.payeepage;
    const { currentUser } = this.props;
    /* eslint-disable */
    const { column, direction } = this.state;
    const url = nextUrl ? nextUrl : previousUrl;
    const params = !url ? { limit: 0, offset: 0 } : getParams(url);
    const { limit, offset } = params;
    const numberOfPage = limit > 0 ? Math.ceil(pageNumber / limit) : 0;
    const currentPage = nextUrl
      ? offset / limit
      : (parseInt(offset) + parseInt(limit) * 2) / limit;
    return (
      <ContainerWrapper>
        <div className="row">
          {this.buildModalInput()}
          {this.buildModalDelete()}
          {this.buildModalEnable()}
          <div className="col-lg-12">
            <CardBox>
              <h4 className="m-t-0 header-title">
                <b>Payees</b>
              </h4>
              <div className="p-20">
                <Loading loading={loading} />
                <Button positive onClick={() => this.props.registerPayee()}>
                  <Icon name="plus" />
                  Add Payee
                </Button>
                <Table striped selectable>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>Payee Name</Table.HeaderCell>
                      <Table.HeaderCell>Bank Name</Table.HeaderCell>
                      <Table.HeaderCell>Bank Account Name</Table.HeaderCell>
                      <Table.HeaderCell>Bank Routing Number</Table.HeaderCell>
                      {currentUser.user_type === 'super_admin' ? (
                        <Table.HeaderCell>Action</Table.HeaderCell>
                      ) : null}
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>{this.getData(data)}</Table.Body>
                </Table>
                {numberOfPage > 0 ? (
                  <Pagination
                    activePage={currentPage}
                    totalPages={numberOfPage}
                    onPageChange={(e, pageData) =>
                      this.handlePagination(e, pageData, limit)
                    }
                  />
                ) : null}
              </div>
            </CardBox>
          </div>
        </div>
      </ContainerWrapper>
    );
    /* eslint-enable */
  }
}

PayeePage.propTypes = {
  payeepage: PropTypes.any.isRequired,
  getPayee: PropTypes.func.isRequired,
  addPayee: PropTypes.func.isRequired,
  editPayee: PropTypes.func.isRequired,
  deletePayee: PropTypes.func.isRequired,
  viewPayee: PropTypes.func.isRequired,
  registerPayee: PropTypes.func.isRequired,
  pageAction: PropTypes.func.isRequired,
  setPayeeId: PropTypes.func.isRequired,
  enablePayee: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  payeepage: makeSelectPayeePage(),
  currentUser: makeSelectCurrentUser(),
});

const mapDispatchToProps = {
  getPayee,
  addPayee,
  editPayee,
  deletePayee,
  viewPayee,
  registerPayee,
  pageAction,
  setPayeeId,
  enablePayee,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'payeePage', reducer });
const withSaga = injectSaga({ key: 'payeePage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(PayeePage);
