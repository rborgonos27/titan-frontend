/*
 *
 * ResetPasswordPage actions
 *
 */

import {
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_ERROR,
  REDIRECT,
} from './constants';

export function resetPassword(data) {
  return {
    type: RESET_PASSWORD,
    data,
  };
}

export function resetPasswordSuccess(data) {
  return {
    type: RESET_PASSWORD_SUCCESS,
    data,
  };
}

export function resetPasswordError(error) {
  return {
    type: RESET_PASSWORD_ERROR,
    error,
  };
}

export function redirectLogin() {
  return {
    type: REDIRECT,
  };
}
