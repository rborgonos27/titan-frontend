/*
 *
 * ResetPasswordPage constants
 *
 */

export const RESET_PASSWORD = 'app/ResetPasswordPage/RESET_PASSWORD';
export const RESET_PASSWORD_SUCCESS =
  'app/ResetPasswordPage/RESET_PASSWORD_SUCCESS';
export const RESET_PASSWORD_ERROR =
  'app/ResetPasswordPage/RESET_PASSWORD_ERROR';

export const REDIRECT = 'app/ResetPasswordPage/REDIRECT';
