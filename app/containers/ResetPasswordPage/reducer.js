/*
 *
 * ResetPasswordPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_ERROR,
} from './constants';

export const initialState = fromJS({
  error: null,
  loading: false,
  isSuccess: false,
  message: '',
});

function resetPasswordPageReducer(state = initialState, action) {
  switch (action.type) {
    case RESET_PASSWORD:
      return state
        .set('loading', true)
        .set('error', null)
        .set('isSuccess', false)
        .set('message', '');
    case RESET_PASSWORD_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('isSuccess', true)
        .set('message', action.data);
    case RESET_PASSWORD_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('isSuccess', false)
        .set('message', '');
    default:
      return state;
  }
}

export default resetPasswordPageReducer;
