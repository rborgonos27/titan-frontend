import { all, takeLatest, call, put } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { APP } from 'config';
import request from 'utils/request';
import { optionsFormData } from 'utils/options';

import { resetPasswordSuccess, resetPasswordError } from './actions';
import { RESET_PASSWORD, REDIRECT } from './constants';

export function* resetPassword(info) {
  try {
    const API_URI = APP.API_URL;
    const { userId, passwordText } = info.data;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const resetPasswordData = new FormData();
    resetPasswordData.append('user_id', userId);
    resetPasswordData.append('password', passwordText);
    const resetPasswordRequest = yield call(
      request,
      `${API_URI}/reset_password/`,
      optionsFormData('PUT', resetPasswordData, token),
    );
    if (resetPasswordRequest.success) {
      yield put(resetPasswordSuccess(resetPasswordRequest.message));
    } else {
      yield put(resetPasswordError(resetPasswordRequest.message));
    }
  } catch (error) {
    yield put(resetPasswordError(error));
  }
}

export function* redirectPage() {
  yield put(push('/login'));
}

export function* resetPasswordWatcher() {
  yield takeLatest(RESET_PASSWORD, resetPassword);
}

export function* redirectWatcher() {
  yield takeLatest(REDIRECT, redirectPage);
}
// Individual exports for testing
export default function* rootSaga() {
  yield all([resetPasswordWatcher(), redirectWatcher()]);
}
