/**
 *
 * ResetPasswordPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import CardBox from 'components/CardBox';
import { RequiredText, HelpText } from 'components/Utils';
import {
  Segment,
  Button,
  Divider,
  Form,
  Input,
  Message,
} from 'semantic-ui-react';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectResetPasswordPage from './selectors';
import { resetPassword, redirectLogin } from './actions';
import reducer from './reducer';
import saga from './saga';

/* eslint-disable react/prefer-stateless-function */
export class ResetPasswordPage extends React.Component {
  state = {
    /* eslint-disable  */
    userId: null,
    passwordText: '',
    confirmPasswordText: '',
    /* eslint-enable  */
  };
  componentDidMount() {
    const { match } = this.props;
    /* eslint-disable */
    const beforeDecode = match.params.id.split("'");
    const userId = atob(beforeDecode[1]);
    this.setState({ userId });
    /* eslint-enable */
  }
  componentWillReceiveProps(nextProps) {
    const { isSuccess } = nextProps.resetpasswordpage;
    if (isSuccess) {
      setTimeout(() => this.props.redirectLogin(), 3000);
    }
  }
  validatePassword = passwordText => {
    /* eslint-disable */
    const passwordRegEx = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$');
    const result = passwordRegEx.test(passwordText);
    return result;
    /* eslint-enable */
  };
  handleInput = e => this.setState({ [e.target.name]: e.target.value });
  render() {
    const { passwordText, confirmPasswordText } = this.state;
    const isValidPassword = this.validatePassword(passwordText);
    const isValidConfirmPassword = this.validatePassword(confirmPasswordText);
    const { message, loading } = this.props.resetpasswordpage;
    const disableButton =
      isValidPassword && passwordText === confirmPasswordText;
    const iconStyle = {
      color: 'green',
      name: 'check',
      link: true,
      size: 'large',
    };
    const showCheckIcon = isValidPassword ? iconStyle : null;
    const showCheckIconConfirmPassword = isValidConfirmPassword
      ? iconStyle
      : null;
    return (
      <Segment className="segmentStyle" color="orange">
        <h3>Reset Password.</h3>
        <Divider />
        <CardBox>
          <Form loading={loading} centered>
            {message !== '' ? <Message positive>{message}</Message> : null}
            <Form.Field>
              <h4 className="p-t-10 headerClass">
                Enter Password
                <RequiredText>*</RequiredText>
              </h4>
              <HelpText>
                Minimum 8 characters, at least one upper case and 1 numeric and
                a special character @#%$&!.
              </HelpText>
              <Input
                name="passwordText"
                type="password"
                icon={showCheckIcon}
                value={passwordText}
                onChange={this.handleInput}
                placeholder="Password..."
              />
            </Form.Field>
            <Form.Field>
              <h4 className="p-t-10 headerClass">
                Confirm Password
                <RequiredText>*</RequiredText>
              </h4>
              <Input
                name="confirmPasswordText"
                type="password"
                icon={showCheckIconConfirmPassword}
                value={confirmPasswordText}
                onChange={this.handleInput}
                placeholder="Confirm Password..."
              />
            </Form.Field>
            <Form.Field>
              <Button
                className="buttonSubmit"
                fluid
                size="massive"
                color="orange"
                disabled={!disableButton}
                onClick={() => this.props.resetPassword(this.state)}
              >
                Submit
              </Button>
            </Form.Field>
          </Form>
        </CardBox>
      </Segment>
    );
  }
}

ResetPasswordPage.propTypes = {
  match: PropTypes.any,
  resetpasswordpage: PropTypes.any,
  resetPassword: PropTypes.func.isRequired,
  redirectLogin: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  resetpasswordpage: makeSelectResetPasswordPage(),
});

const mapDispatchToProps = { resetPassword, redirectLogin };

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'resetPasswordPage', reducer });
const withSaga = injectSaga({ key: 'resetPasswordPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ResetPasswordPage);
