import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the sendPayment state domain
 */

const selectSendPaymentDomain = state => state.get('sendPayment', initialState);
const selectGlobal = state => state.get('global');

/**
 * Other specific selectors
 */

/**
 * Default selector used by SendPayment
 */

const makeSelectSendPayment = () =>
  createSelector(selectSendPaymentDomain, substate => substate.toJS());

const makeSelectCurrentUser = () =>
  createSelector(selectGlobal, globalState => globalState.get('userData'));

const makeSelectFeeObject = () =>
  createSelector(selectGlobal, globalState => globalState.get('feeObject'));

export default makeSelectSendPayment;
export { selectSendPaymentDomain, makeSelectCurrentUser, makeSelectFeeObject };
