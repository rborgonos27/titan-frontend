/*
 *
 * SendPayment constants
 *
 */

export const DEFAULT_ACTION = 'app/SendPayment/DEFAULT_ACTION';
export const ON_SUBMIT = 'app/SendPayment/ON_SUBMIT';
export const ON_SUCCESS = 'app/SendPayment/ON_SUCCESS';
export const ON_FAILED = 'app/SendPayment/ON_FAILED';
export const ON_RESET = 'app/SendPayment/ON_RESET';

export const FILE_UPLOAD = 'app/SendPayment/FILE_UPLOAD';
export const FILE_UPLOAD_SUCCESS = 'app/SendPayment/FILE_UPLOAD_SUCCESS';
export const FILE_UPLOAD_ERROR = 'app/SendPayment/FILE_UPLOAD_ERROR';

export const GET_TIME = 'app/SendPayment/GET_TIME';
export const GET_TIME_SUCCESS = 'app/SendPayment/GET_TIME_SUCCESS';
export const GET_TIME_ERROR = 'app/SendPayment/GET_TIME_ERROR';

export const GET_PAYEE = 'app/SendPayment/GET_PAYEE';
export const GET_PAYEE_SUCCESS = 'app/SendPayment/GET_PAYEE_SUCCESS';
export const GET_PAYEE_ERROR = 'app/SendPayment/GET_PAYEE_ERROR';

export const GET_MEMBER_PAYEE = 'app/SendPayment/GET_MEMBER_PAYEE';
export const GET_MEMBER_PAYEE_SUCCESS =
  'app/SendPayment/GET_MEMBER_PAYEE_SUCCESS';
export const GET_MEMBER_PAYEE_ERROR = 'app/SendPayment/GET_MEMBER_PAYEE_ERROR';

export const GET_USER_FEE_RATE = 'app/SendPayment/GET_USER_FEE_RATE';
export const GET_USER_FEE_RATE_SUCCESS =
  'app/SendPayment/GET_USER_FEE_RATE_SUCCESS';
export const GET_USER_FEE_RATE_ERROR =
  'app/SendPayment/GET_USER_FEE_RATE_ERROR';

export const GET_USER_FEE_TIER_RATE = 'app/SendPayment/GET_USER_FEE_TIER_RATE';
export const GET_USER_FEE_TIER_RATE_SUCCESS =
  'app/SendPayment/GET_USER_FEE_TIER_RATE_SUCCESS';
export const GET_USER_FEE_TIER_RATE_ERROR =
  'app/SendPayment/GET_USER_FEE_TIER_RATE_ERROR';

export const GET_FEE_TRANSACTION = 'app/SendPayment/GET_FEE_TRANSACTION';
export const GET_FEE_TRANSACTION_SUCCESS =
  'app/SendPayment/GET_FEE_TRANSACTION_SUCCESS';
export const GET_FEE_TRANSACTION_ERROR =
  'app/SendPayment/GET_FEE_TRANSACTION_ERROR';

export const SEND_LOG = 'app/SendPayment/SEND_LOG';
export const SEND_LOG_SUCCESS = 'app/SendPayment/SEND_LOG_SUCCESS';
export const SEND_LOG_ERROR = 'app/SendPayment/SEND_LOG_ERROR';

export const GET_WALLET = 'app/SendPayment/GET_WALLET';
export const GET_WALLET_SUCCESS = 'app/SendPayment/GET_WALLET_SUCCESS';
export const GET_WALLET_ERROR = 'app/SendPayment/GET_WALLET_ERROR';

export const REMOVE_FILE = 'app/SendPayment/REMOVE_FILE';
export const REMOVE_FILE_SUCCESS = 'app/SendPayment/REMOVE_FILE_SUCCESS';
export const REMOVE_FILE_ERROR = 'app/SendPayment/REMOVE_FILE_ERROR';
