/**
 *
 * Asynchronously loads the component for SendPayment
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
