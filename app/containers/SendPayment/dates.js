import moment from 'moment';

const dateFormat = 'YYYY-MM-DD';
const getDateForward = (days, serverDate) => {
  const date = moment(serverDate);
  return date.add(days, 'day').format(dateFormat);
};

const getMaxPickupDate = (dateString, day) => {
  const date = moment(dateString);
  return date.subtract(day, 'day').format(dateFormat);
};

const calculateBusinessDays = (firstDate, secondDate) => {
  let day1 = moment(firstDate);
  let day2 = moment(secondDate);
  let adjust = 0;
  if (day1.dayOfYear() === day2.dayOfYear() && day1.year() === day2.year()) {
    return 0;
  }
  if (day2.isBefore(day1)) {
    day2 = moment(firstDate);
    day1 = moment(secondDate);
  }
  if (day1.day() === 6) {
    day1.day(8);
  } else if (day1.day() === 0) {
    day1.day(1);
  }
  if (day2.day() === 6) {
    day2.day(5);
  } else if (day2.day() === 0) {
    day2.day(-2);
  }

  const day1Week = day1.week();
  let day2Week = day2.week();

  if (day1Week !== day2Week) {
    if (day2Week < day1Week) {
      day2Week += day1Week;
    }
    adjust = -2 * (day2Week - day1Week);
  }

  return day2.diff(day1, 'days') + adjust;
};

const getAllDatesByDay = day => {
  const date = moment()
    .startOf('year')
    .day(day);
  if (date.date() > 7) date.add(7, 'd');
  const year = date.year();
  const dateList = [];
  while (year === date.year()) {
    dateList.push(date.format(dateFormat).toString());
    date.add(7, 'd');
  }

  return dateList;
};

export {
  getAllDatesByDay,
  getDateForward,
  getMaxPickupDate,
  calculateBusinessDays,
};
