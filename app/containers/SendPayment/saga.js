import { all, call, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import ReactGA from 'react-ga';
import request from 'utils/request';
import { APP, GA } from 'config';
import moment from 'moment';
// import { formatMoney } from 'utils/formatMoney';
import {
  ON_SUBMIT,
  FILE_UPLOAD,
  GET_TIME,
  GET_PAYEE,
  GET_MEMBER_PAYEE,
  GET_USER_FEE_RATE,
  GET_USER_FEE_TIER_RATE,
  GET_FEE_TRANSACTION,
  SEND_LOG,
  GET_WALLET,
  REMOVE_FILE,
} from './constants';
import {
  onSuccess,
  onFailed,
  getTimeSuccess,
  getTimeError,
  getPayeeSuccess,
  getPayeeError,
  getUserFeeRateError,
  getUserFeeRateSuccess,
  getUserFeeTierRateError,
  getUserFeeTierRateSuccess,
  getFeeTransactionSuccess,
  getFeeTransactionError,
  getWalletSuccess,
  getWalletError,
  removeFileError,
  removeFileSuccess,
  fileUploadSuccess,
} from './actions';
import { optionsFormData } from '../../utils/options';

export function* submit(data) {
  const API_URI = APP.API_URL;
  ReactGA.initialize(GA.TRACKING_ID);
  try {
    const formData = new FormData();
    const amount = data.data.amount.replace(/,/g, '').replace('$', '');
    formData.append('type', 'PAYMENT');
    formData.append('to', 'Anywhere');
    formData.append('amount', amount);
    formData.append('fee', data.data.fee.toFixed(2));
    formData.append('due_at', `${data.data.pay_at} 00:00`);
    formData.append('pickup_at', `${data.data.pickup_at} 00:00`);
    formData.append('remarks', data.data.remarks);
    formData.append('payee_id', data.data.payee);
    formData.append('transaction_type', data.data.transactionType);
    formData.append('pickup_type', 'ON-DEMAND');
    const token = yield JSON.parse(localStorage.getItem('token'));
    ReactGA.event({
      category: 'Request',
      action: `Creating Payment Request`,
      label: 'Payment Request',
    });
    const paymentResponse = yield call(
      request,
      `${API_URI}/request/`,
      optionsFormData('POST', formData, token),
    );
    if (paymentResponse.success) {
      // remove session
      ReactGA.event({
        category: 'Request',
        action: `Payment Request Created`,
        label: 'Payment Request Success',
      });
      yield sessionStorage.removeItem('payment_amount');
      yield sessionStorage.removeItem('payment_payee');
      yield sessionStorage.removeItem('payment_images');
      yield sessionStorage.removeItem('payment_pay_at');
      yield sessionStorage.removeItem('payment_pickup_at');
      yield sessionStorage.removeItem('payment_remarks');
      const formDataNotif = new FormData();
      formDataNotif.append(
        'name',
        `${data.data.currentUser.first_name} ${
          data.data.currentUser.last_name
        }`,
      );
      formDataNotif.append('amount', data.data.amount);
      formDataNotif.append('type', 'PAYMENT');
      formDataNotif.append(
        'date',
        moment(data.data.pay_at).format(APP.DATE_DISPLAY_FORMAT),
      );
      formDataNotif.append('request_id', paymentResponse.data.id);
      yield call(
        request,
        `${API_URI}/new/request/notif`,
        optionsFormData('POST', formDataNotif, token),
      );
      yield put(onSuccess(paymentResponse.data));
      yield put(push('/'));
    }
  } catch (error) {
    ReactGA.event({
      category: 'Request',
      action: `Payment Request Failed ${JSON.stringify(error)}`,
      label: 'Payment Request Error',
    });
    yield put(onFailed({ error }));
  }
}

export function* fileUpload(file) {
  try {
    const API_URI = APP.API_URL;
    ReactGA.initialize(GA.TRACKING_ID);
    /* eslint-disable */
    for (const i in file.data) {
      const fileData = file.data[i];
      const token = yield JSON.parse(localStorage.getItem('token'));
      const formData = new FormData();
      formData.append('filename', fileData);
      formData.append('url', 'filename');
      formData.append('mime_type', fileData.type);
      formData.append('type', 'PAYMENT');
      formData.append('remarks', '');
      formData.append(
        'expire_at',
        moment()
          .add(1, 'hours')
          .format('YYYY-MM-DD h:m:s'),
      );
      ReactGA.event({
        category: 'Request',
        action: `Upload backup file`,
        label: 'Payment Request Upload Backup',
      });
      const fileResponse = yield call(
        request,
        `${API_URI}/file/`,
        optionsFormData('POST', formData, token),
      );
      if (fileResponse.filename) {
        let fileStoredSession = yield JSON.parse(
          sessionStorage.getItem('payment_images'),
        );
        if (!fileStoredSession) {
          fileStoredSession = [];
        }
        const fileSession = {
          id: fileResponse.id,
          preview: fileResponse.filename,
          type: fileResponse.mime_type,
          name: fileData.name,
        };
        ReactGA.event({
          category: 'Request',
          action: `Upload backup file success`,
          label: 'Payment Request Upload Backup Success',
        });
        fileStoredSession.push(fileSession);
        yield sessionStorage.setItem(
          'payment_images',
          JSON.stringify(fileStoredSession),
        );
        yield put(fileUploadSuccess([]));
      } else {
        ReactGA.event({
          category: 'Request',
          action: `Upload backup file failed`,
          label: 'Payment Request Upload Backup Failed',
        });
      }
    }
    /* eslint-enable */
  } catch (error) {
    ReactGA.event({
      category: 'Request',
      action: `Upload backup file failed ${error}`,
      label: 'Payment Request Upload Backup Failed',
    });
  }
}

export function* getTime() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const serverTime = yield call(
      request,
      `${API_URI}/time/`,
      optionsFormData('GET', null, token),
    );
    if (serverTime.current_time) {
      yield put(getTimeSuccess(serverTime));
    } else {
      yield put(getTimeError(serverTime));
    }
  } catch (error) {
    yield put(getTimeError(error));
  }
}

export function* getPayee() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const payee = yield call(
      request,
      `${API_URI}/payee/`,
      optionsFormData('GET', null, token),
    );
    if (payee.results) {
      /* eslint-disable */
      const { results } = payee;
      let dropdownData = [];
      for (const i in results) {
        const resultsData = results[i];
        const dataResult = {
          key: i,
          text: resultsData.name,
          value: resultsData.id,
        };
        dropdownData.push(dataResult);
      }
      yield put(getPayeeSuccess(dropdownData));
      /* eslint-enable */
    } else {
      yield put(getPayeeError(payee));
    }
  } catch (error) {
    yield put(getPayeeError(error));
  }
}

export function* getPayeeMember() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const payee = yield call(
      request,
      `${API_URI}/payee_user/`,
      optionsFormData('GET', null, token),
    );
    if (payee.success) {
      /* eslint-disable */
      const { data } = payee;
      let dropdownData = [];
      for (const i in data) {
        const resultsData = data[i].payee;
        if (!resultsData.deleted_at) {
          const dataResult = {
            key: i,
            text: resultsData.name,
            value: resultsData.id,
          };
          dropdownData.push(dataResult);
        }
      }
      yield put(getPayeeSuccess(dropdownData));
      /* eslint-enable */
    } else {
      yield put(getPayeeError(payee));
    }
  } catch (error) {
    yield put(getPayeeError(error));
  }
}

export function* getUserFeeRate(user) {
  try {
    const API_URI = APP.API_URL;
    const { data } = user;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const feeRate = yield call(
      request,
      `${API_URI}/fee/?user_id=${data}`,
      optionsFormData('GET', null, token),
    );
    if (feeRate.results.length > 0) {
      yield put(getUserFeeRateSuccess(feeRate.results[0]));
    } else {
      yield put(getUserFeeRateError(feeRate));
    }
  } catch (error) {
    yield put(getUserFeeRateError(error));
  }
}

export function* getUserFeeTierRate(user) {
  try {
    const API_URI = APP.API_URL;
    const { data } = user;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const feeRate = yield call(
      request,
      `${API_URI}/fee_tier/?user_id=${data}`,
      optionsFormData('GET', null, token),
    );
    if (feeRate.results.length > 0) {
      yield put(getUserFeeTierRateSuccess(feeRate.results));
    } else {
      yield put(getUserFeeTierRateError(feeRate));
    }
  } catch (error) {
    yield put(getUserFeeTierRateError(error));
  }
}

export function* getFeeTransaction(feeData) {
  try {
    const { userId, date } = feeData.data;
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const dateparam = date ? `&date=${date}` : ``;
    const feeTransaction = yield call(
      request,
      `${API_URI}/fee_transaction_user/?user_id=${userId}&type=payment${dateparam}`,
      optionsFormData('GET', null, token),
    );
    if (feeTransaction.id) {
      yield put(getFeeTransactionSuccess(feeTransaction));
    }
    if (feeTransaction.message) {
      const transaction = {
        id: null,
        fee_transaction_id: null,
        message: feeTransaction.message,
        type: 'deposit',
        fee_percent: 0,
        transaction_amount: 0,
        total_transaction_amount: 0,
        fee_amount: 0,
      };
      yield put(getFeeTransactionSuccess(transaction));
    }
  } catch (error) {
    yield put(getFeeTransactionError(error));
  }
}

export function* sendLog(data) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const formData = new FormData();
    const {
      feeTotal,
      firstTierAmount,
      secondTierAmount,
      thirdTierAmount,
      firstTierFee,
      secondTierFee,
      thirdTierFee,
      transactionFee,
      feeTierObject,
      inputAmount,
    } = data.data;

    const firstFeeTier = feeTierObject[0];
    const secondFeeTier = feeTierObject[1];
    const thirdFeeTier = feeTierObject[2];
    formData.append('type', 'PAYMENT');
    formData.append('transaction_amount', feeTotal);
    formData.append(
      'total_transaction_amount',
      transactionFee.total_transaction_amount,
    );

    formData.append('input_amount', inputAmount);
    formData.append('first_tier_amount', firstTierAmount);
    formData.append('second_tier_amount', secondTierAmount);
    formData.append('third_tier_amount', thirdTierAmount);

    formData.append('first_tier_fee', firstTierFee);
    formData.append('second_tier_fee', secondTierFee);
    formData.append('third_tier_fee', thirdTierFee);

    formData.append('first_tier_rate', firstFeeTier.payment_rate);
    formData.append('second_tier_rate', secondFeeTier.payment_rate);
    formData.append('third_tier_rate', thirdFeeTier.payment_rate);

    formData.append('first_tier_threshold', firstFeeTier.payment_threshold);
    formData.append('second_tier_threshold', secondFeeTier.payment_threshold);
    formData.append('third_tier_threshold', thirdFeeTier.payment_threshold);

    yield call(
      request,
      `${API_URI}/log/`,
      optionsFormData('POST', formData, token),
    );
  } catch (error) {
    console.log(error);
  }
}

export function* removeFile(data) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    yield call(
      request,
      `${API_URI}/user_file/${data.data}/`,
      optionsFormData('DELETE', null, token),
    );
    let fileStoredSession = yield JSON.parse(
      sessionStorage.getItem('payment_images'),
    );
    if (!fileStoredSession) {
      fileStoredSession = [];
    }
    ReactGA.event({
      category: 'Request',
      action: `remove backup file success`,
      label: 'Payment Request Remove Backup Success',
    });
    if (fileStoredSession.length === 1) {
      fileStoredSession = [];
    } else {
      fileStoredSession = removeFileOnArray(fileStoredSession, data.data);
    }
    yield sessionStorage.setItem(
      'payment_images',
      JSON.stringify(fileStoredSession),
    );
    yield put(removeFileSuccess([]));
  } catch (error) {
    yield put(removeFileError(error));
  }
}

export function removeFileOnArray(files, id) {
  /* eslint-disable */
  for(let i = 0; i < files.length; i ++) {
    const file = files[i];
    if (file.id === id) {
      files.splice(i, 1);
    }
  };
  return files;
}

export function* getWallet() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const walletResponse = yield call(
      request,
      `${API_URI}/wallet/`,
      optionsFormData('GET', null, token),
    );
    if (walletResponse.success) {
      yield put(getWalletSuccess(walletResponse.data));
    } else {
      yield put(getWalletError(walletResponse.message));
    }
  } catch (error) {
    yield put(getWalletError(error));
  }
}

export function* submitWatcher() {
  yield takeLatest(ON_SUBMIT, submit);
}

export function* fileUploadWather() {
  yield takeLatest(FILE_UPLOAD, fileUpload);
}

export function* getTimeWatcher() {
  yield takeLatest(GET_TIME, getTime);
}

export function* getPayeeWatcher() {
  yield takeLatest(GET_PAYEE, getPayee);
}

export function* getPayeeMemberWatcher() {
  yield takeLatest(GET_MEMBER_PAYEE, getPayeeMember);
}

export function* getUserFeeRateWatcher() {
  yield takeLatest(GET_USER_FEE_RATE, getUserFeeRate);
}

export function* getUserFeeTierRateWatcher() {
  yield takeLatest(GET_USER_FEE_TIER_RATE, getUserFeeTierRate);
}

export function* getFeeTransactionWatcher() {
  yield takeLatest(GET_FEE_TRANSACTION, getFeeTransaction);
}

export function* sendLogWatcher() {
  yield takeLatest(SEND_LOG, sendLog);
}

export function* getWalletWatcher() {
  yield takeLatest(GET_WALLET, getWallet);
}

export function* removeFileWatcher() {
  yield takeLatest(REMOVE_FILE, removeFile);
}

export default function* rootSaga() {
  yield all([
    submitWatcher(),
    fileUploadWather(),
    getTimeWatcher(),
    getPayeeWatcher(),
    getPayeeMemberWatcher(),
    getUserFeeRateWatcher(),
    getUserFeeTierRateWatcher(),
    getFeeTransactionWatcher(),
    sendLogWatcher(),
    getWalletWatcher(),
    removeFileWatcher(),
  ]);
}
