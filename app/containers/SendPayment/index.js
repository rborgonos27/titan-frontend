/**
 *
 * SendPayment
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import moment from 'moment';
import {
  Form,
  TextArea,
  Button,
  Modal,
  Header,
  Grid,
  Message,
  List,
  Dropdown,
  Dimmer,
  Loader,
} from 'semantic-ui-react';
import { formatMoney } from 'utils/formatMoney';
import Dropzone from 'react-dropzone';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { APP } from 'config';
import StandardModal from 'components/StandardModal';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import ContainerWrapper from 'components/ContainerWrapper';
import pdfImage from 'images/pdf.png';
import closeImage from 'images/close.png';
import fileIcon from 'images/file-icon.png';
import { HelpText, RequiredText } from 'components/Utils';

import makeSelectSendPayment, { makeSelectCurrentUser } from './selectors';
import {
  onSubmit,
  onReset,
  fileUpload,
  getTime,
  getPayee,
  getMemberPayee,
  getUserFeeRate,
  getUserFeeTierRate,
  getFeeTransaction,
  sendLog,
  getWallet,
  removeFile,
} from './actions';
import reducer from './reducer';
import saga from './saga';
import { getDateForward, getMaxPickupDate } from './dates';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export class SendPayment extends React.Component {
  /* eslint-disable */
  state = {
    currentUser: null,
    openNoPayeeModal: false,
    pay_at: sessionStorage.getItem('payment_pay_at')
      ? sessionStorage.getItem('payment_pay_at')
      : '',
    pickup_at: sessionStorage.getItem('payment_pickup_at')
      ? sessionStorage.getItem('payment_pickup_at')
      : '',
    remarks: sessionStorage.getItem('payment_remarks')
      ? sessionStorage.getItem('payment_remarks')
      : '',
    vendor: 2,
    fee: parseFloat(0).toFixed(2),
    amount: sessionStorage.getItem('payment_amount')
      ? parseFloat(sessionStorage.getItem('payment_amount')).toFixed(2)
      : '',
    open: false,
    dimmer: false,
    loading: false,
    error: null,
    errorList: [],
    maxPickupDate: '',
    disabledPickupDate: true,
    showPickupDate: false,
    uploadedFiles: sessionStorage.getItem('payment_images')
      ? JSON.parse(sessionStorage.getItem('payment_images'))
      : [],
    selectedDay: undefined,
    isEmpty: true,
    isDisabled: false,
    isPickUpAtDisabled: true,
    openWarning: false,
    serverTime: '',
    payee: null,
    optionsPayee: [],
    amountError: '',
    isNotEnoughBalance: false,
    transactionType: 'PAPER',
    cancelModal: false,
  };
  /* eslint-enable */
  componentWillMount() {
    this.props.onReset();
    this.props.getTime();
    this.props.getWallet();
    const { currentUser } = this.props;
    this.props.getUserFeeRate(currentUser.id);
  }
  componentDidMount() {
    const wrapper = document.querySelector('#wrapper'); // eslint-disable-line no-alert
    wrapper.className = 'enlarged forced';
    const { currentUser } = this.props;
    /* eslint-disable */
    this.setState({ currentUser });
    if (currentUser.user_type === 'member' || currentUser.user_type === 'sub_member') {
      this.props.getMemberPayee();
    } else {
      this.props.getPayee();
    }
    // this.props.getUserFeeRate(currentUser.id);
    this.props.getUserFeeTierRate(currentUser.id);
    this.props.getFeeTransaction({ userId: currentUser.id });
    /* eslint-enable */
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      serverTime: nextProps.sendpayment.currentTime,
      optionsPayee: nextProps.sendpayment.option,
      payee: sessionStorage.getItem('payment_payee')
        ? sessionStorage.getItem('payment_payee')
        : null,
    });
    if (nextProps.sendpayment.isUploaded) {
      this.setState({
        uploadedFiles: sessionStorage.getItem('payment_images')
          ? JSON.parse(sessionStorage.getItem('payment_images'))
          : [],
      });
    }
    const { payeeLoaded, option } = nextProps.sendpayment;
    if (payeeLoaded && option.length < 1) {
      this.openShowModal('openNoPayeeModal', true);
    }
  }
  buildNotEnoughBalance = () => (
    <StandardModal
      headerText="Insufficient Funds"
      open={this.state.isNotEnoughBalance}
      onClose={() => {
        this.setState({ isNotEnoughBalance: false });
      }}
      onActionText="Close"
      onContinue={() => {
        this.setState({ isNotEnoughBalance: false });
      }}
    >
      <div style={{ textAlign: 'center' }}>
        Your payment request exceeds your available balance. Please deposit
        funds before completing your request. Thank you
      </div>
    </StandardModal>
  );
  buildCancelModal = () => (
    <StandardModal
      headerText="Cancel Request"
      open={this.state.cancelModal}
      onClose={() => {
        this.setState({ cancelModal: false });
      }}
      onActionText="Cancel"
      onContinue={() => {
        sessionStorage.removeItem('payment_amount');
        sessionStorage.removeItem('payment_payee');
        sessionStorage.removeItem('payment_images');
        sessionStorage.removeItem('payment_pay_at');
        sessionStorage.removeItem('payment_pickup_at');
        sessionStorage.removeItem('payment_remarks');
        this.setState({ cancelModal: false });
        this.props.history.goBack();
      }}
    >
      <div style={{ textAlign: 'center' }}>
        Cancel this request? By clicking confirm, the changes will be discarded.
      </div>
    </StandardModal>
  );
  buildNoPayeeModal = () => (
    <StandardModal
      headerText="User Payee Warning"
      open={this.state.openNoPayeeModal}
      onClose={() => this.props.history.goBack()}
      onActionText="Back"
      onContinue={() => this.props.history.goBack()}
    >
      <div style={{ textAlign: 'left' }}>
        There is no payee assigned for this user, please contact Titan Officer
        to start using this feature.
      </div>
    </StandardModal>
  );
  handleClickDate = (id, mindate) => {
    this.setState({ [id]: mindate });
  };
  handleChange = (event, { name = undefined, value }) => {
    /* eslint-disable */
    this.setState({ showPickupDate: false });
    if (this.state.hasOwnProperty(name)) {
      this.setState({ [name]: value });
      if (name === 'pay_at') {
        this.setState({ pickup_at: '' });
      }
    }

    const { serverTime } = this.state;
    const currentDate = moment(serverTime).format(APP.DATE_FORMAT);
    const maxPickupDate =
      this.state.pay_at !== ''
        ? getMaxPickupDate(this.state.pay_at, 3)
        : currentDate;

    // noOfWorkingDays
    const minPickupDate =
      this.state.pay_at !== '' ? getDateForward(2, currentDate) : currentDate;
    this.setState({ maxPickupDate, minPickupDate, showPickupDate: true });
    /* eslint-enable */
  };
  handleChangeDropDown = (e, { value }) => {
    this.setState({ payee: value });
    sessionStorage.setItem(`payment_payee`, value);
  };
  handleUpdatePickupAtValues = () => {
    const { serverTime } = this.state;
    const currentDate = moment(serverTime).format(APP.DATE_FORMAT);
    const maxPickupDate =
      this.state.pay_at !== ''
        ? getMaxPickupDate(this.state.pay_at, 3)
        : currentDate;

    // noOfWorkingDays
    const minPickupDate = getDateForward(2, currentDate);
    return { maxPickupDate, minPickupDate };
  };
  handleDayChangePayAt = (selectedDay, modifiers, dayPickerInput) => {
    const state = 'pay_at';
    this.handleSetDateState(selectedDay, modifiers, dayPickerInput, state);
    /* eslint-disable */
    const { minPickupDate } = this.handleUpdatePickupAtValues();
    this.setState({ pickup_at: minPickupDate, isPickUpAtDisabled: false });
    /* eslint-enable */
  };
  handleDayChangePickUpAt = (selectedDay, modifiers, dayPickerInput) => {
    const state = 'pickup_at';
    this.handleSetDateState(selectedDay, modifiers, dayPickerInput, state);
  };
  handleSetDateState = (selectedDay, modifiers, dayPickerInput, state) => {
    const input = dayPickerInput.getInput();
    const selectedDateFromMoment = moment(selectedDay).format(APP.DATE_FORMAT);
    /* eslint-disable */
    this.setState({
      [state]: selectedDateFromMoment,
      isEmpty: !input.value.trim(),
      isDisabled: modifiers.disabled === true,
    });
    sessionStorage.setItem(`payment_${state}`, selectedDateFromMoment);
  };
  /* eslint-enable */
  formatDate = (date, format) => moment(date).format(format);
  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value });
    sessionStorage.setItem(`payment_${e.target.name}`, e.target.value);
  };
  handleInputAmount = e => {
    const { amount } = this.state;
    let formattedAmount = formatMoney(amount);
    if (amount > 9999999) {
      alert("Can't accept deposit more than 10,000,000.00");
      formattedAmount = formatMoney(0);
    }
    this.setState({ [e.target.name]: `$ ${formattedAmount}` });
  };
  onSelectText = e => {
    const formattedAmount = this.state.amount.replace(',', '').replace('$', '');
    this.setState({ [e.target.name]: formattedAmount });
  };
  onSubmitForm = e => {
    e.preventDefault();
    const { wallet } = this.props.sendpayment;
    /* eslint-disable */
    const { payee, pay_at, pickup_at, amount, uploadedFiles } = this.state;
    /* eslint-enable */
    const amountFormatted = amount.replace(/,/g, '').replace('$', '');
    const actualAmount =
      parseFloat(amountFormatted) + parseFloat(this.getFee());
    if (wallet && actualAmount > wallet[0].balance) {
      this.setState({ isNotEnoughBalance: true });
      return;
    }
    /* eslint-disable */
    let errors = [];
    if (amount.trim() === '' || amount === null) {
      errors.push('Amount is required.');
    }
    if (pay_at === '') {
      errors.push('Schedule payment date is required.');
    }
    if (pickup_at == '') {
      errors.push('Cash pickup date  is required.');
    }
    if (!payee) {
      errors.push('Payee is required.');
    }
    if (uploadedFiles.length === 0) {
      errors.push('Please upload at least 1 backup.');
    }

    if (errors.length > 0) {
      this.setState({ open: false, errorList: errors });
    } else {
      this.setState({ open: true, errorList: [] });
    }
    /* eslint-enable */
  };
  enableSubmitButton = () => {
    /* eslint-disable */
    let errors = [];
    const { payee, pay_at, pickup_at, amount, uploadedFiles } = this.state;
    if (amount.trim() === '' || amount === null || amount === 0 || amount === parseFloat(0).toFixed(2)) {
      errors.push('Amount is required.');
    }
    if (pay_at === '') {
      errors.push('Schedule payment date is required.');
    }
    if (pickup_at === '') {
      errors.push('Cash pickup date  is required.');
    }
    if (uploadedFiles.length === 0) {
      errors.push('Please upload at least 1 backup.');
    }

    if (!payee) {
      errors.push('Payee is required.');
    }
    return errors.length > 0;
    /* eslint-enable */
  };
  onImageDrop = files => {
    this.props.fileUpload(files);
    const { uploadedFiles } = this.state;
    this.setState({
      uploadedFiles: [...uploadedFiles, ...files],
    });
  };
  submitData = () => {
    const { feeObject } = this.props.sendpayment;
    const submitData = this.state;
    submitData.fee = this.getFee();
    submitData.feeObject = feeObject;
    this.props.onSubmit(submitData);
  };
  /* eslint-disable */
  show = dimmer => () => this.setState({ dimmer, open: true });
  /* eslint-enable */
  close = () => this.setState({ open: false });
  openShowModal = (stateName, isOpen) => this.setState({ [stateName]: isOpen });
  buildErrorMessage = () => {
    /* eslint-disable */
    const { errorList } = this.state;
    let errorItems = [];
    errorList.forEach((error, index) => {
      errorItems.push(<List.Item key={index} id={index}>{error}</List.Item>);
    });
    return errorList.length > 0 ? (
      <Message negative>
        <Message.Header>Unable to Process Request</Message.Header>
        <List bulleted>
          {errorItems}
        </List>
      </Message>
    ) : null;
    /* eslint-enable */
  };
  closeWarning = () => {
    const amountSelector = document.querySelector('#amount');
    amountSelector.focus();
    this.setState({ openWarning: false });
  };
  buildModalWarning = () => {
    const { openWarning, amountError } = this.state;
    return (
      <Modal size="mini" open={openWarning}>
        <Modal.Header>Warning</Modal.Header>
        <Modal.Content>
          <p>{amountError}</p>
        </Modal.Content>
        <Modal.Actions>
          <Button
            onClick={() => this.closeWarning()}
            color="orange"
            icon="checkmark"
            labelPosition="right"
            content="Ok"
          />
        </Modal.Actions>
      </Modal>
    );
  };
  switchTransactionToggle = (e, type) => {
    const tablinks = document.querySelectorAll('.tablinksTransaction');
    /* eslint-disable */
    for (let i = 0; i < tablinks.length; i++) {
      const tablink = tablinks[i];
      tablink.className = tablink.className.replace(' tab-active', '');
    }
    /* eslint-enable */
    e.currentTarget.className += ' tab-active';
    this.setState({ transactionType: type });
  };
  disableUpDownArrow = e => {
    /* eslint-disable */
    const restrictedKeys = [
      16,17,18,19,20,33,34,36,
      38,40,45,65,66,67,68,69,
      70,71,72,73,74,75,76,77,
      78,79,80,81,82,83,84,85,
      86,87,88,89,90,91,92,93,
      94,95,106,107,109,111,186,
      187,189,191,192,219,220,221,222,
    ];
    if (restrictedKeys.indexOf(e.keyCode) > -1) {
      e.preventDefault();
    }
  };
  /* eslint-disable */
  getPayeeValue = id => {
    const { optionsPayee } = this.state;
    if (optionsPayee) {
      for (let i = 0; i < optionsPayee.length; i++) {
        const payeeObject = optionsPayee[i];
        if (payeeObject.value.toString() === id) {
          return payeeObject.text;
        };
      };
    };
    return null;
  }
  getTrasactionFee = () => {
    const payAtData = this.state.pay_at;
    const { currentUser } = this.props;
    const fee = this.getFee();
    this.props.getFeeTransaction({ userId: currentUser.id, date: payAtData, fee });
  }
  divideAmountOnTier = (amount, totalDeposit) => {
    const { feeTierObject } = this.props.sendpayment;
    const firstFeeTier = feeTierObject[0];
    const secondFeeTier = feeTierObject[1];
    const thirdfeeTier = feeTierObject[2];
    let firstTierAmount = 0;
    let secondTierAmount = 0;
    let thirdTierAmount = 0;
    const amountParsed = parseFloat(amount);
    const totalDepositParsed = parseFloat(totalDeposit);
    const totalAmount = amountParsed + totalDepositParsed;
    if (totalDepositParsed > 0) {
      if (totalDepositParsed < firstFeeTier.payment_threshold) {
        if (totalAmount > secondFeeTier.payment_threshold) {
          firstTierAmount = firstFeeTier.payment_threshold - totalDepositParsed;
          secondTierAmount = secondFeeTier.payment_threshold - firstFeeTier.payment_threshold;
          thirdTierAmount = amountParsed - firstTierAmount - secondTierAmount;
        } else if (totalAmount > firstFeeTier.payment_threshold) {
          firstTierAmount = firstFeeTier.payment_threshold - totalDepositParsed;
          secondTierAmount = amountParsed - firstTierAmount;
        } else {
          firstTierAmount = amountParsed;
        }
      } else if (totalDepositParsed < secondFeeTier.payment_threshold) {
        if (totalAmount > secondFeeTier.payment_threshold) {
          secondTierAmount =
            secondFeeTier.payment_threshold - totalDepositParsed;
          thirdTierAmount = amountParsed - secondTierAmount;
        } else {
          secondTierAmount = amountParsed;
        }
      } else if (totalDepositParsed > secondFeeTier.payment_threshold) {
        thirdTierAmount = amountParsed;
      }
      return { firstTierAmount, secondTierAmount, thirdTierAmount };
    }

    if (amountParsed < firstFeeTier.payment_threshold) {
      firstTierAmount = amountParsed;
    } else if (amountParsed < secondFeeTier.payment_threshold) {
      firstTierAmount = firstFeeTier.payment_threshold;
      secondTierAmount = amountParsed - firstTierAmount;
    } else if (amountParsed > secondFeeTier.payment_threshold) {
      firstTierAmount = firstFeeTier.payment_threshold; // 600, 000
      secondTierAmount = secondFeeTier.payment_threshold - firstTierAmount;
      thirdTierAmount = amountParsed - secondFeeTier.payment_threshold; // 400, 000
    }
    return { firstTierAmount, secondTierAmount, thirdTierAmount };
  };
  // payment_threshold
  getComputedFee = () => {
    const { sendpayment } = this.props;
    const { feeTierObject, feeTransaction } = sendpayment;
    const { amount } = this.state;
    const transactionFee = feeTransaction;
    let feeTotal = 0;
    if (transactionFee && feeTierObject) {
      const origAmount = amount.replace(/,/g, '').replace('$', '');
      const segregateAmountByTier = this.divideAmountOnTier(
        origAmount,
        transactionFee.total_transaction_amount,
      );
      /* eslint-disable */
      const firstFeeTier = feeTierObject[0];
      const secondFeeTier = feeTierObject[1];
      const thirdFeeTier = feeTierObject[2];

      const { firstTierAmount, secondTierAmount, thirdTierAmount } = segregateAmountByTier;

      const firstTierFee = firstFeeTier.payment_rate * firstTierAmount;
      const secondTierFee = secondFeeTier.payment_rate * secondTierAmount;
      const thirdTierFee = thirdFeeTier.payment_rate * thirdTierAmount;

      feeTotal = firstTierFee + secondTierFee + thirdTierFee;
      /* eslint-enable */
      const logData = {
        feeTotal,
        firstTierAmount,
        secondTierAmount,
        thirdTierAmount,
        firstTierFee,
        secondTierFee,
        thirdTierFee,
        transactionFee,
        feeTierObject,
        inputAmount: origAmount,
      };
      if (this.state.open) {
        this.props.sendLog(logData);
      }
    }
    return feeTotal;
  };
  divideAmountOnMinimumVoulue = (amount, totalDeposit) => {
    const { feeTierObject } = this.props.sendpayment;
    const firstFeeTier = feeTierObject[0];
    const secondFeeTier = feeTierObject[1];
    let belowMinimum = 0;
    let aboveMinimum = 0;
    const amountParsed = parseFloat(amount);
    const totalDepositParsed = parseFloat(totalDeposit);
    const totalAmount = amountParsed + totalDepositParsed;
    if (totalDepositParsed > 0) {
      if (totalDepositParsed <= firstFeeTier.payment_threshold) {
        if (totalAmount > firstFeeTier.payment_threshold) {
          belowMinimum = firstFeeTier.payment_threshold - totalDepositParsed;
          aboveMinimum = amountParsed - belowMinimum;
        } else {
          aboveMinimum = amountParsed;
        }
      } else {
        aboveMinimum = amountParsed;
      }
      return { belowMinimum, aboveMinimum };
    }
    if (amountParsed <= firstFeeTier.payment_threshold) {
      belowMinimum = amountParsed;
    } else if (amountParsed > secondFeeTier.payment_threshold) {
      belowMinimum = firstFeeTier.payment_threshold;
      aboveMinimum = amountParsed - belowMinimum;
    }
    return { belowMinimum, aboveMinimum };
  };
  getMinimumVolumeFee = () => {
    const { sendpayment } = this.props;
    const { feeTierObject, transactionFee } = sendpayment;
    const { amount } = this.state;
    let feeTotal = 0;
    if (transactionFee && feeTierObject) {
      const origAmount = amount.replace(/,/g, '').replace('$', '');
      const segregateAmountByTier = this.divideAmountOnMinimumVoulue(
        origAmount,
        transactionFee.total_transaction_amount,
      );
      /* eslint-disable */
      const belowMinimumTier = feeTierObject[0];
      const aboveMinimumTier = feeTierObject[1];

      const { belowMinimum, aboveMinimum } = segregateAmountByTier;

      const belowMinimumAmount = belowMinimumTier.payment_rate * belowMinimum;
      const aboveMinimumAmount = aboveMinimumTier.payment_rate * aboveMinimum;

      feeTotal = belowMinimumAmount + aboveMinimumAmount;
      /* eslint-enable */
      // const logData = {
      //   feeTotal,
      //   firstTierAmount,
      //   secondTierAmount,
      //   thirdTierAmount,
      //   firstTierFee,
      //   secondTierFee,
      //   thirdTierFee,
      //   transactionFee,
      //   feeTierObject,
      //   inputAmount: origAmount,
      // };
      // if (this.state.openConfirmModal) {
      //   this.props.sendLog(logData);
      // }
    }
    return feeTotal;
  };
  getFee = () => {
    const { feeObject } = this.props.sendpayment;
    const { transactionType, amount } = this.state;
    if (transactionType === 'ELECTRONIC') {
      return 0;
    }
    if (!feeObject) {
      return 0;
    }
    const origAmount = amount.replace(/,/g, '').replace('$', '');
    if (feeObject.fee_setting === 'flat_rate') {
      // get flat rate
      return parseFloat(origAmount) * parseFloat(feeObject.payment_flat_rate);
    } else if (feeObject.fee_setting === 'fee_tier_v1') {
      return this.getComputedFee();
    } else if (feeObject.fee_setting === 'fee_tier_v2') {
      // get 2nd fee tier
      return this.getMinimumVolumeFee();
    }
    return 0;
  };
  /* eslint-enable */
  buildModal = () => {
    const { open, amount, payee, remarks, transactionType } = this.state;
    const pickuAtData = this.state.pickup_at;
    const payAtData = this.state.pay_at;
    /* eslint-disable */
    const pickUpAt = moment(pickuAtData, APP.DATE_TIME_FORMAT).format(
      APP.DATE_DISPLAY_FORMAT,
    );
    const payAt = moment(payAtData, APP.DATE_TIME_FORMAT).format(
      APP.DATE_DISPLAY_FORMAT,
    );
    const origAmount = amount.replace(/,/g, '').replace('$', '');
    // const taxPaymentRate = feeObject ? feeObject.tax_payment_rate : 0;
    const feeAmount = this.getFee();
    const totalAmount = parseFloat(origAmount) + parseFloat(feeAmount);
    const { loading } = this.props.sendpayment;
    /* eslint-enable */
    return (
      <StandardModal
        headerText="Proceed this Request?"
        open={open}
        onClose={this.close}
        onActionText="CONFIRM"
        onContinue={this.submitData}
        btnDisabled={loading}
      >
        <div style={{ textAlign: 'left' }}>
          <Header style={{ textAlign: 'center', marginBottom: 20 }}>
            Summary
          </Header>
          <Grid size="large" centered divided columns={2}>
            <Grid.Column textAlign="left">
              <Header as="h3">Amount</Header>
              <p className="confirmText">{amount}</p>
              <Header as="h3">Payment Date</Header>
              <p className="confirmText">{payAt}</p>
              <Header as="h3">Pickup Date</Header>
              <p className="confirmText">
                Titan Officer will confirm cash pickup shortly.
              </p>
              <Header as="h3">Transaction Type</Header>
              <p className="confirmText">{transactionType}</p>
              <Header as="h3">Memo</Header>
              <p className="confirmText">{remarks || <br />}</p>
            </Grid.Column>
            <Grid.Column textAlign="left">
              <Header as="h3">Payee</Header>
              <p className="confirmText">
                {this.getPayeeValue(payee) || <br />}
              </p>
              <Header as="h3">Fee</Header>
              <p className="confirmText">$ {formatMoney(feeAmount)}</p>
              <Header as="h3">Total Amount</Header>
              <p className="confirmText">$ {formatMoney(totalAmount)}</p>
              {transactionType === 'PAPER' ? (
                <p className="confirmText">
                  Final Fees will be calculated based on completed transactions
                  for the current period.
                </p>
              ) : (
                <p className="confirmText">
                  Fee is free for Electronic Transactions.
                </p>
              )}
            </Grid.Column>
          </Grid>
        </div>
      </StandardModal>
    );
  };
  buildPickUpDayPicker = (
    serverTimeMomentMonth,
    maxPickupMomentMonth,
    minPickupMomentMonth,
    minPickupDate,
    maxPickupDate,
  ) => (
    <Form.Field>
      <h4 className="p-t-10 headerClass">
        Cash Pickup
        <RequiredText>*</RequiredText>
      </h4>
      <DayPickerInput
        className="materialInput"
        value={this.state.pickup_at}
        onDayChange={this.handleDayChangePickUpAt}
        placeholder={APP.DATE_FORMAT}
        format={APP.DATE_FORMAT}
        initialMonth={new Date(minPickupMomentMonth)}
        formatDate={this.formatDate}
        inputProps={{
          readOnly: true,
          disabled: this.state.pay_at === '',
        }}
        dayPickerProps={{
          fromMonth: new Date(minPickupMomentMonth),
          toMonth: new Date(maxPickupMomentMonth),
          modifiers: {
            disabled: [
              {
                daysOfWeek: [0, 6],
              },
              {
                before: new Date(minPickupDate),
              },
              {
                after: new Date(maxPickupDate),
              },
            ],
          },
        }}
      />
      <HelpText>
        <FormattedMessage {...messages.pickupPaymentText} />
      </HelpText>
      <div className="contentSeparator" />
    </Form.Field>
  );
  getFileSrc = file => {
    let src = null;
    if (file.type === 'image/jpeg' || file.type === 'image/png') {
      src = file.preview;
    } else if (file.type === 'application/pdf') {
      src = pdfImage;
    } else {
      src = fileIcon;
    }
    return src;
  };
  cancelRequest = () => {
    this.setState({ cancelModal: true });
  };
  render() {
    const { payee, remarks, serverTime, optionsPayee } = this.state;
    const { loading } = this.props.sendpayment;
    const serverTimeMoment = moment(serverTime).format(APP.DATE_FORMAT);
    console.log(serverTime);
    // const serverTimeMomentMonth = moment(serverTime).format(
    //   APP.DATE_MONTH_FORMAT,
    // );
    const serverTimeHourMoment = moment(serverTime).format('H');
    // 14 is 2pm in military time
    const daysForward = serverTimeHourMoment > 14 ? 6 : 5;
    const minPayDate = getDateForward(daysForward, serverTimeMoment);
    // console.log('min pay date', minPayDate, daysForward);
    const minPayDateMonth = moment(minPayDate).format(APP.DATE_MONTH_FORMAT);
    // const { maxPickupDate, minPickupDate } = this.handleUpdatePickupAtValues();
    // const minPickupMomentMonth = moment(minPickupDate).format(
    //   APP.DATE_MONTH_FORMAT,
    // );
    // const maxPickupMomentMonth = moment(maxPickupDate).format(
    //   APP.DATE_MONTH_FORMAT,
    // );
    const previewStyle = {
      display: 'inline',
      width: 70,
      height: 70,
      padding: 5,
    };

    const previewSpan = {
      display: 'inline',
    };
    const closeButton = {
      position: 'absolute',
      width: '30px',
      height: '30px',
      objectFit: 'cover',
    };

    return (
      <ContainerWrapper>
        {this.buildModal()}
        {this.buildModalWarning()}
        {this.buildNoPayeeModal()}
        {this.buildNotEnoughBalance()}
        {this.buildCancelModal()}
        <div>
          <div className="row">
            <div className="pull-right" style={{ marginRight: 50 }}>
              <Button circular onClick={() => this.cancelRequest()}>
                X
              </Button>
            </div>
            <div className="col-lg-2 col-md-3 col-sm-3" />
            <div className=" col-lg-8 col-md-6 col-sm-6 col-xs-12">
              <div className="card-box service-mode m-t-30 m-b-30 p-l-r-30 content-box contentBox">
                {this.buildErrorMessage()}
                <h4 className="headerClass">
                  Enter Payee
                  <RequiredText>*</RequiredText>
                </h4>
                <Dropdown
                  value={Number(payee)}
                  className="materialSelection"
                  onChange={this.handleChangeDropDown}
                  options={optionsPayee}
                  placeholder="Choose Payee"
                  selection
                  fluid
                />
                <div className="contentSeparator" />
                <Form size="huge" autoComplete="off" loading={loading}>
                  {/* <Form.Field> */}
                  {/* <div className="service-mode-button w3-bar w3-black"> */}
                  {/* <button */}
                  {/* className="w3-bar-item w3-button tablinksTransaction tab-active left-tab" */}
                  {/* onClick={e => this.switchTransactionToggle(e, 'PAPER')} */}
                  {/* > */}
                  {/* Paper */}
                  {/* </button> */}
                  {/* <button */}
                  {/* className="w3-bar-item w3-button tablinksTransaction right-tab" */}
                  {/* onClick={e => */}
                  {/* this.switchTransactionToggle(e, 'ELECTRONIC') */}
                  {/* } */}
                  {/* > */}
                  {/* Electronic */}
                  {/* </button> */}
                  {/* </div> */}
                  {/* </Form.Field> */}
                  <Form.Field>
                    <h4 className="p-t-10 headerClass">
                      Enter Amount
                      <RequiredText>*</RequiredText>
                    </h4>
                    <input
                      className="materialInput"
                      type="text"
                      step="0.00"
                      min="0"
                      name="amount"
                      id="amount"
                      value={this.state.amount}
                      placeholder="Amount"
                      onChange={this.handleInput}
                      onSelect={this.onSelectText}
                      onBlur={this.handleInputAmount}
                      autoComplete="off"
                      onKeyDown={e => this.disableUpDownArrow(e)}
                    />
                    <div className="contentSeparator" />
                  </Form.Field>
                  {/*
                  <Form.Field>
                    <h4 className="p-t-10">Fee</h4>
                    <Input
                      disabled
                      label={{ icon: 'dollar' }}
                      fluid
                      labelPosition="left"
                      type="number"
                      step="0.00"
                      name="fee"
                      value={this.state.fee}
                      placeholder="Fee"
                      onChange={this.handleInput}
                      onBlur={this.handleInputAmount}
                    />
                  </Form.Field>
                  */}
                  <Form.Field>
                    <h4 className="p-t-10 headerClass">
                      Upload Backup
                      <RequiredText>*</RequiredText>
                    </h4>
                    <div className="uploadFile">
                      {this.props.sendpayment.filesLoading ? (
                        <Dimmer active inverted>
                          <Loader size="massive">Loading</Loader>
                        </Dimmer>
                      ) : null}
                      <Dropzone
                        className=" col-lg-8 col-md-6 col-sm-6 col-xs-12 dropZone"
                        onDrop={this.onImageDrop}
                        multiple
                        accept="image/*,application/pdf,
                        application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel,
                        application/doc,
                        application/ms-doc,
                        application/msword,
                        application/vnd.openxmlformats-officedocument.wordprocessingml.document,
                        application/vnd.ms-excel,
                        .csv
                        "
                      >
                        <p>Drop your files or click here to upload</p>
                        {/* eslint-disable */
                          this.state.uploadedFiles.length > 0 && (
                          <div>
                            {this.state.uploadedFiles.map((file, key) => (
                              <span key={key} style={previewSpan}>
                                <a onClick={(e) => {
                                  e.preventDefault();
                                  e.stopPropagation();
                                  this.props.removeFile(file.id);
                                }
                                }>
                                  <img
                                  alt=""
                                  style={closeButton}
                                  key={key}
                                  src={closeImage}
                                  />
                                 </a>
                                <img
                                  alt="Preview"
                                  key={key}
                                  src={this.getFileSrc(file)}
                                  style={previewStyle}
                                />
                                <b style={{fontSize: 10}}>{file.name}</b>
                              </span>
                            ))}
                          </div>
                        )
                          /* eslint-enable */
                        }
                      </Dropzone>
                    </div>
                    <div className="clearfix" />
                    <div className="contentSeparator" />
                  </Form.Field>
                  <Form.Field>
                    <h4 className="p-t-10 headerClass">
                      Schedule Payment
                      <RequiredText>*</RequiredText>
                    </h4>
                    <DayPickerInput
                      className="materialInput"
                      value={this.state.pay_at}
                      onDayChange={this.handleDayChangePayAt}
                      placeholder={APP.DATE_FORMAT}
                      format={APP.DATE_FORMAT}
                      initialMonth={new Date(minPayDateMonth)}
                      formatDate={this.formatDate}
                      inputProps={{ readOnly: true }}
                      dayPickerProps={{
                        fromMonth: new Date(minPayDateMonth),
                        // month: new Date(minPayDateMonth),
                        modifiers: {
                          disabled: [
                            {
                              daysOfWeek: [0, 6],
                            },
                            {
                              before: new Date(minPayDate),
                            },
                          ],
                        },
                      }}
                    />
                    <HelpText>
                      <FormattedMessage {...messages.paymentDateText} />
                    </HelpText>
                    <div className="contentSeparator" />
                  </Form.Field>
                  {/*eslint-disable*/
                    /* this.state.pay_at !== '' <-- temporary disable this
                      ? this.buildPickUpDayPicker(
                          serverTimeMomentMonth,
                          maxPickupMomentMonth,
                          minPickupMomentMonth,
                          minPickupDate,
                          maxPickupDate,
                        )
                          : null
                      /* eslint-enable */}
                  <Form.Field>
                    <h4 className="p-t-10 headerClass">Memo</h4>
                    <TextArea
                      className="materialInput"
                      name="remarks"
                      onChange={this.handleInput}
                      rows={3}
                      placeholder="Note to your payee."
                      value={remarks}
                    />
                    <div className="contentSeparator" />
                  </Form.Field>
                  <Button
                    className="buttonSubmit"
                    fluid
                    size="massive"
                    color="orange"
                    disabled={this.enableSubmitButton()}
                    onClick={(e) => {
                      this.getTrasactionFee();
                      this.onSubmitForm(e);
                    }}
                  >
                    Submit
                  </Button>
                  <Button
                    className="buttonSubmitCancel"
                    fluid
                    size="massive"
                    onClick={() => this.cancelRequest()}
                  >
                    Cancel
                  </Button>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </ContainerWrapper>
    );
  }
}

SendPayment.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onReset: PropTypes.func.isRequired,
  getMemberPayee: PropTypes.func.isRequired,
  fileUpload: PropTypes.func.isRequired,
  getTime: PropTypes.func.isRequired,
  getPayee: PropTypes.func.isRequired,
  sendpayment: PropTypes.object.isRequired,
  getUserFeeRate: PropTypes.object,
  getUserFeeTierRate: PropTypes.any,
  getFeeTransaction: PropTypes.func.isRequired,
  currentUser: PropTypes.object.isRequired,
  history: PropTypes.any,
  sendLog: PropTypes.any,
  getWallet: PropTypes.any,
  removeFile: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  sendpayment: makeSelectSendPayment(),
  currentUser: makeSelectCurrentUser(),
});

const mapDispatchToProps = {
  onSubmit,
  onReset,
  fileUpload,
  getTime,
  getPayee,
  getMemberPayee,
  getUserFeeRate,
  getUserFeeTierRate,
  getFeeTransaction,
  sendLog,
  getWallet,
  removeFile,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'sendPayment', reducer });
const withSaga = injectSaga({ key: 'sendPayment', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(SendPayment);
