import { fromJS } from 'immutable';
import sendPaymentReducer from '../reducer';

describe('sendPaymentReducer', () => {
  it('returns the initial state', () => {
    expect(sendPaymentReducer(undefined, {})).toEqual(fromJS({}));
  });
});
