/*
 *
 * SendPayment actions
 *
 */

import {
  ON_SUBMIT,
  ON_SUCCESS,
  ON_FAILED,
  ON_RESET,
  FILE_UPLOAD,
  FILE_UPLOAD_ERROR,
  FILE_UPLOAD_SUCCESS,
  GET_TIME,
  GET_TIME_SUCCESS,
  GET_TIME_ERROR,
  GET_PAYEE,
  GET_PAYEE_SUCCESS,
  GET_PAYEE_ERROR,
  GET_MEMBER_PAYEE,
  GET_MEMBER_PAYEE_SUCCESS,
  GET_MEMBER_PAYEE_ERROR,
  GET_USER_FEE_RATE,
  GET_USER_FEE_RATE_ERROR,
  GET_USER_FEE_RATE_SUCCESS,
  GET_USER_FEE_TIER_RATE,
  GET_USER_FEE_TIER_RATE_SUCCESS,
  GET_USER_FEE_TIER_RATE_ERROR,
  GET_FEE_TRANSACTION,
  GET_FEE_TRANSACTION_SUCCESS,
  GET_FEE_TRANSACTION_ERROR,
  SEND_LOG,
  SEND_LOG_ERROR,
  SEND_LOG_SUCCESS,
  GET_WALLET,
  GET_WALLET_SUCCESS,
  GET_WALLET_ERROR,
  REMOVE_FILE,
  REMOVE_FILE_SUCCESS,
  REMOVE_FILE_ERROR,
} from './constants';

export function onSubmit(data) {
  return {
    type: ON_SUBMIT,
    data,
  };
}

export function onSuccess(data) {
  return {
    type: ON_SUCCESS,
    data,
  };
}

export function onFailed(data) {
  return {
    type: ON_FAILED,
    data,
  };
}

export function onReset() {
  return {
    type: ON_RESET,
  };
}

export function fileUpload(data) {
  return {
    type: FILE_UPLOAD,
    data,
  };
}

export function fileUploadSuccess(data) {
  return {
    type: FILE_UPLOAD_SUCCESS,
    data,
  };
}

export function fileUploadError(data) {
  return {
    type: FILE_UPLOAD_ERROR,
    data,
  };
}

export function getTime() {
  return {
    type: GET_TIME,
  };
}

export function getTimeSuccess(data) {
  return {
    type: GET_TIME_SUCCESS,
    data,
  };
}

export function getTimeError(data) {
  return {
    type: GET_TIME_ERROR,
    data,
  };
}

export function getPayee() {
  return {
    type: GET_PAYEE,
  };
}

export function getPayeeSuccess(data) {
  return {
    type: GET_PAYEE_SUCCESS,
    data,
  };
}

export function getPayeeError(data) {
  return {
    type: GET_PAYEE_ERROR,
    data,
  };
}

export function getMemberPayee() {
  return {
    type: GET_MEMBER_PAYEE,
  };
}

export function getMemberPayeeSuccess(data) {
  return {
    type: GET_MEMBER_PAYEE_SUCCESS,
    data,
  };
}

export function getMemberPayeeError(error) {
  return {
    type: GET_MEMBER_PAYEE_ERROR,
    error,
  };
}

export function getUserFeeRate(data) {
  return {
    type: GET_USER_FEE_RATE,
    data,
  };
}

export function getUserFeeRateSuccess(data) {
  return {
    type: GET_USER_FEE_RATE_SUCCESS,
    data,
  };
}

export function getUserFeeRateError(error) {
  return {
    type: GET_USER_FEE_RATE_ERROR,
    error,
  };
}

export function getUserFeeTierRate(data) {
  return {
    type: GET_USER_FEE_TIER_RATE,
    data,
  };
}

export function getUserFeeTierRateSuccess(data) {
  return {
    type: GET_USER_FEE_TIER_RATE_SUCCESS,
    data,
  };
}

export function getUserFeeTierRateError(error) {
  return {
    type: GET_USER_FEE_TIER_RATE_ERROR,
    error,
  };
}

export function getFeeTransaction(data) {
  return {
    type: GET_FEE_TRANSACTION,
    data,
  };
}

export function getFeeTransactionSuccess(data) {
  return {
    type: GET_FEE_TRANSACTION_SUCCESS,
    data,
  };
}

export function getFeeTransactionError(error) {
  return {
    type: GET_FEE_TRANSACTION_ERROR,
    error,
  };
}

export function sendLog(data) {
  return {
    type: SEND_LOG,
    data,
  };
}

export function sendLogSuccess(data) {
  return {
    type: SEND_LOG_SUCCESS,
    data,
  };
}

export function sendLogError(error) {
  return {
    type: SEND_LOG_ERROR,
    error,
  };
}

export function getWallet() {
  return {
    type: GET_WALLET,
  };
}

export function getWalletSuccess(data) {
  return {
    type: GET_WALLET_SUCCESS,
    data,
  };
}

export function getWalletError(error) {
  return {
    type: GET_WALLET_ERROR,
    error,
  };
}

export function removeFile(data) {
  return {
    type: REMOVE_FILE,
    data,
  };
}

export function removeFileSuccess(data) {
  return {
    type: REMOVE_FILE_SUCCESS,
    data,
  };
}

export function removeFileError(error) {
  return {
    type: REMOVE_FILE_ERROR,
    error,
  };
}
