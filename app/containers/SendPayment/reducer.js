/*
 *
 * SendPayment reducer
 *
 */

import { fromJS } from 'immutable';
import {
  ON_SUBMIT,
  ON_SUCCESS,
  ON_FAILED,
  ON_RESET,
  FILE_UPLOAD,
  FILE_UPLOAD_SUCCESS,
  FILE_UPLOAD_ERROR,
  GET_TIME,
  GET_TIME_SUCCESS,
  GET_TIME_ERROR,
  GET_PAYEE,
  GET_PAYEE_SUCCESS,
  GET_PAYEE_ERROR,
  GET_MEMBER_PAYEE,
  GET_MEMBER_PAYEE_SUCCESS,
  GET_MEMBER_PAYEE_ERROR,
  GET_USER_FEE_RATE,
  GET_USER_FEE_RATE_ERROR,
  GET_USER_FEE_RATE_SUCCESS,
  GET_USER_FEE_TIER_RATE,
  GET_USER_FEE_TIER_RATE_ERROR,
  GET_USER_FEE_TIER_RATE_SUCCESS,
  GET_FEE_TRANSACTION,
  GET_FEE_TRANSACTION_SUCCESS,
  GET_FEE_TRANSACTION_ERROR,
  GET_WALLET,
  GET_WALLET_SUCCESS,
  GET_WALLET_ERROR,
  REMOVE_FILE,
  REMOVE_FILE_SUCCESS,
  REMOVE_FILE_ERROR,
} from './constants';

export const initialState = fromJS({
  loading: false,
  data: null,
  error: null,
  errorList: [],
  filesLoading: false,
  files: [],
  currentTime: null,
  option: null,
  payeeLoaded: false,
  feeObject: null,
  feeTierObject: null,
  feeTransaction: null,
  wallet: null,
  isUploaded: false,
});

function sendPaymentReducer(state = initialState, action) {
  switch (action.type) {
    case ON_SUBMIT:
      return state
        .set('loading', true)
        .set('error', null)
        .set('errorList', []);
    case ON_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', []);
    case ON_FAILED:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('errorList', []);
    case ON_RESET:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', []);
    case FILE_UPLOAD:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', [])
        .set('isUploaded', false)
        .set('filesLoading', true);
    case FILE_UPLOAD_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', action.data)
        .set('isUploaded', true)
        .set('filesLoading', false);
    case FILE_UPLOAD_ERROR:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', [])
        .set('filesError', action.data.error)
        .set('isUploaded', false)
        .set('filesLoading', false);
    case REMOVE_FILE:
      return state
        .set('filesLoading', true)
        .set('isUploaded', false)
        .set('error', null)
        .set('files', null);
    case REMOVE_FILE_SUCCESS:
      return state
        .set('filesLoading', false)
        .set('isUploaded', true)
        .set('error', null)
        .set('files', action.data);
    case REMOVE_FILE_ERROR:
      return state
        .set('filesLoading', false)
        .set('isUploaded', false)
        .set('error', action.error)
        .set('files', null);
    case GET_TIME:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', [])
        .set('filesError', null)
        .set('filesLoading', false)
        .set('currentTime', null);
    case GET_TIME_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', [])
        .set('filesError', null)
        .set('filesLoading', false)
        .set('currentTime', action.data.current_time);
    case GET_TIME_ERROR:
      return state
        .set('loading', false)
        .set('error', 'Error getting time.')
        .set('errorList', [])
        .set('files', [])
        .set('filesError', null)
        .set('filesLoading', false)
        .set('currentTime', null);
    case GET_PAYEE:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', [])
        .set('filesError', null)
        .set('filesLoading', false)
        .set('payeeLoaded', false)
        .set('option', null);
    case GET_PAYEE_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', [])
        .set('filesError', null)
        .set('filesLoading', false)
        .set('payeeLoaded', true)
        .set('option', action.data);
    case GET_PAYEE_ERROR:
      return state
        .set('loading', false)
        .set('error', 'Error getting options.')
        .set('errorList', [])
        .set('files', [])
        .set('option', [])
        .set('filesError', null)
        .set('payeeLoaded', false)
        .set('filesLoading', false);
    case GET_MEMBER_PAYEE:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', [])
        .set('filesError', null)
        .set('filesLoading', false)
        .set('payeeLoaded', false)
        .set('option', null);
    case GET_MEMBER_PAYEE_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('errorList', [])
        .set('files', [])
        .set('filesError', null)
        .set('filesLoading', false)
        .set('payeeLoaded', true)
        .set('option', action.data);
    case GET_MEMBER_PAYEE_ERROR:
      return state
        .set('loading', false)
        .set('error', 'Error getting options.')
        .set('errorList', [])
        .set('files', [])
        .set('filesError', null)
        .set('option', [])
        .set('payeeLoaded', false)
        .set('filesLoading', false);
    case GET_USER_FEE_RATE:
      return state
        .set('loading', true)
        .set('error', null)
        .set('feeObject', null);
    case GET_USER_FEE_RATE_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('feeObject', action.data);
    case GET_USER_FEE_RATE_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('feeObject', null);
    case GET_USER_FEE_TIER_RATE:
      return state
        .set('loading', true)
        .set('error', null)
        .set('feeObject', null);
    case GET_USER_FEE_TIER_RATE_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('feeTierObject', null);
    case GET_USER_FEE_TIER_RATE_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('feeTierObject', action.data);
    case GET_FEE_TRANSACTION:
      return state
        .set('loading', true)
        .set('error', null)
        .set('feeTransaction', null);
    case GET_FEE_TRANSACTION_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('feeTransaction', action.data);
    case GET_FEE_TRANSACTION_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('feeTransaction', null);
    case GET_WALLET:
      return state
        .set('loading', true)
        .set('error', null)
        .set('wallet', null);
    case GET_WALLET_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('wallet', action.data);
    case GET_WALLET_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('wallet', null);
    default:
      return state;
  }
}

export default sendPaymentReducer;
