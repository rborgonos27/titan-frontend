/*
 * SendPayment Messages
 *
 * This contains all the text for the SendPayment component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.SendPayment.header',
    defaultMessage: 'This is SendPayment container !',
  },
  paymentDateText: {
    id: 'app.containers.SendPayment.paymentDateText',
    defaultMessage:
      'Calendar is based on US, Pacific Standard time (PST.) When scheduling a payment, the earliest payment date you' +
      ' can select is after 3 business days (you cannot select the current day.) Requests made after 2pm PST will be' +
      ' processed the following business day. Payment that fall on a weekend or bank holiday will be processed the next' +
      ' business day. Allow extra time for funds transfer.',
  },
  pickupPaymentText: {
    id: 'app.containers.SendPayment.pickupPaymentText',
    defaultMessage:
      'Cash pickup must be scheduled minimum 2 business days from payment date.' +
      ' Please choose a later payment date if you need more time.',
  },
});
