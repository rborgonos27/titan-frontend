/*
 *
 * ProfilePage constants
 *
 */

export const DEFAULT_ACTION = 'app/ProfilePage/DEFAULT_ACTION';

export const GET_PROFILE = 'app/ProfilePage/GET_PROFILE';
export const GET_PROFILE_SUCCESS = 'app/ProfilePage/GET_PROFILE_SUCCESS';
export const GET_PROFILE_ERROR = 'app/ProfilePage/GET_PROFILE_ERROR';

export const UPDATE_PROFILE = 'app/ProfilePage/UPDATE_PROFILE';
export const UPDATE_PROFILE_SUCCESS = 'app/ProfilePage/UPDATE_PROFILE_SUCCESS';
export const UPDATE_PROFILE_ERROR = 'app/ProfilePage/UPDATE_PROFILE_ERROR';

export const UPDATE_PASSWORD = 'app/ProfilePage/UPDATE_PASSWORD';
export const UPDATE_PASSWORD_SUCCESS = 'app/ProfilePage/UPDATE_PASSWORD_SUCCESS';
export const UPDATE_PASSWORD_ERROR = 'app/ProfilePage/UPDATE_PASSWORD_ERROR';
