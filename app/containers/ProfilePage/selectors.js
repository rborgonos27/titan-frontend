import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the profilePage state domain
 */
const selectGlobal = state => state.get('global');
const selectProfilePageDomain = state => state.get('profilePage', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by ProfilePage
 */

const makeSelectProfilePage = () =>
  createSelector(selectProfilePageDomain, substate => substate.toJS());

const makeSelectCurrentUser = () => createSelector(
  selectGlobal,
  globalState => globalState.get('userData')
);

export default makeSelectProfilePage;
export { selectProfilePageDomain, makeSelectCurrentUser };
