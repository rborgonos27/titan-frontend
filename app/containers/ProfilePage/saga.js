import { all, takeLatest, call, put } from 'redux-saga/effects';
import request from 'utils/request';
import { APP } from 'config';
import moment from 'moment';
import { GET_PROFILE, UPDATE_PROFILE, UPDATE_PASSWORD } from './constants';
import {
  getProfileSuccess,
  getProfileErrror,
  updateProfileSuccess,
  updateProfileError,
  updatePasswordSuccess,
  updatePasswordError,
} from './actions';
import { getCurrentUser } from '../App/actions';
import { optionsFormData } from '../../utils/options';

export function* runGetProfile() {
  const API_URI = APP.API_URL;
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userProfile = yield call(
      request,
      `${API_URI}/current_user/`,
      optionsFormData('GET', null, token),
    );
    if (userProfile.id) {
      yield put(getProfileSuccess(userProfile));
    } else {
      yield put(getProfileErrror(userProfile));
    }
  } catch (error) {
    yield put(getProfileErrror(error));
  }
}

export function* updateProfile(form) {
  try {
    const API_URI = APP.API_URL;
    const { user, data } = form.data;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userRequest = yield call(
      request,
      `${API_URI}/user/account/${user.id}/`,
      optionsFormData('GET', null, token),
    );
    if (userRequest.id) {
      const formData = new FormData();
      formData.append('email', data.email);
      formData.append('username', userRequest.username);
      formData.append('password', userRequest.password);
      formData.append('first_name', userRequest.first_name);
      formData.append('last_name', userRequest.last_name);
      formData.append('business_name', userRequest.business_name);
      formData.append('primary_contact_name', data.primaryContactName);
      formData.append('primary_contact_number', data.primaryContactNumber);
      formData.append('address', userRequest.address);
      formData.append('is_first_time_member', userRequest.is_first_time_member);
      formData.append(
        'business_contact_number',
        userRequest.business_contact_number,
      );
      formData.append(
        'principal_city_of_business',
        userRequest.principal_city_of_business,
      );
      const userUpdateRequest = yield call(
        request,
        `${API_URI}/user/${user.id}/`,
        optionsFormData('PUT', formData, token),
      );
      if (data.profileImage) {
        const formDataFile = new FormData();
        formDataFile.append('filename', data.profileImage);
        formDataFile.append('url', 'filename');
        formDataFile.append('mime_type', data.profileImage.type);
        formDataFile.append('resource_id', userRequest.id);
        formDataFile.append('type', 'USER');
        formDataFile.append(
          'expire_at',
          moment()
            .add(1, 'hours')
            .format('YYYY-MM-DD h:m:s'),
        );
        yield call(
          request,
          `${API_URI}/user_file/`,
          optionsFormData('POST', formDataFile, token),
        );
        yield put(getCurrentUser());
      }
      if (userUpdateRequest.id) {
        yield put(updateProfileSuccess(userUpdateRequest));
        yield call(
          request,
          `${API_URI}/update/user/notif`,
          optionsFormData('POST', formData, token),
        );
      } else {
        yield put(updateProfileError(userUpdateRequest.message));
      }
    }
  } catch (error) {
    yield put(updateProfileError(error));
  }
}

export function* updatePassword(form) {
  try {
    const API_URI = APP.API_URL;
    const { user, data } = form.data;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userRequest = yield call(
      request,
      `${API_URI}/user/account/${user.id}/`,
      optionsFormData('GET', null, token),
    );
    let userUpdateRequest = null;
    const formData = new FormData();
    if (userRequest.id) {
      if (data.password !== '') {
        formData.append('email', userRequest.email);
        formData.append('username', userRequest.username);
        formData.append('password', data.password);
        formData.append('first_name', userRequest.first_name);
        formData.append('last_name', userRequest.last_name);
        formData.append('business_name', userRequest.business_name);
        formData.append('primary_contact_name', userRequest.primary_contact_name);
        formData.append('address', userRequest.address);
        formData.append('is_first_time_member', userRequest.is_first_time_member);
        formData.append(
          'business_contact_number',
          userRequest.business_contact_number,
        );
        formData.append('user_type', userRequest.user_type);
        formData.append(
          'primary_contact_number',
          userRequest.primary_contact_number,
        );
        formData.append(
          'principal_city_of_business',
          userRequest.principal_city_of_business,
        );
        userUpdateRequest = yield call(
          request,
          `${API_URI}/profile/user/${user.id}/`,
          optionsFormData('PUT', formData, token),
        );
      }
      if (data.profileImage) {
        const formDataFile = new FormData();
        formDataFile.append('filename', data.profileImage);
        formDataFile.append('url', 'filename');
        formDataFile.append('mime_type', data.profileImage.type);
        formDataFile.append('resource_id', userRequest.id);
        formDataFile.append('type', 'USER');
        formDataFile.append(
          'expire_at',
          moment()
            .add(1, 'hours')
            .format('YYYY-MM-DD h:m:s'),
        );
        yield call(
          request,
          `${API_URI}/user_file/`,
          optionsFormData('POST', formDataFile, token),
        );
        yield put(getCurrentUser());
      }
      if (userUpdateRequest.id) {
        yield put(updatePasswordSuccess(userUpdateRequest));
        yield call(
          request,
          `${API_URI}/update/user/notif`,
          optionsFormData('POST', formData, token),
        );
      } else {
        yield put(updatePasswordError(userUpdateRequest.message));
      }
    }
  } catch (error) {
    yield put(updatePasswordError(error));
  }
}

export function* getProfileWatcher() {
  yield takeLatest(GET_PROFILE, runGetProfile);
}

export function* updateProfileWatcher() {
  yield takeLatest(UPDATE_PROFILE, updateProfile);
}

export function* updatePasswordWatcher() {
  yield takeLatest(UPDATE_PASSWORD, updatePassword);
}

export default function* rootSaga() {
  yield all([
    getProfileWatcher(),
    updateProfileWatcher(),
    updatePasswordWatcher(),
  ]);
}
