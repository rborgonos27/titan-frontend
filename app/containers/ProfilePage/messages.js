/*
 * ProfilePage Messages
 *
 * This contains all the text for the ProfilePage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ProfilePage.header',
    defaultMessage: 'This is ProfilePage container !',
  },
  username: {
    id: 'app.containers.ProfilePage.username',
    defaultMessage: 'Username',
  },
  userLevel: {
    id: 'app.containers.ProfilePage.userLevel',
    defaultMessage: 'User Level',
  },
  firstName: {
    id: 'app.containers.ProfilePage.firstName',
    defaultMessage: 'First Name',
  },
  lastName: {
    id: 'app.containers.ProfilePage.lastName',
    defaultMessage: 'Last Name',
  },
  middleName: {
    id: 'app.containers.ProfilePage.middleName',
    defaultMessage: 'Middle Name',
  },
  primaryContactName: {
    id: 'app.containers.ProfilePage.primaryContactName',
    defaultMessage: 'Primary Contact',
  },
  email: {
    id: 'app.containers.ProfilePage.email',
    defaultMessage: 'Email',
  },
  primaryContactNumber: {
    id: 'app.containers.ProfilePage.primaryContactNumber',
    defaultMessage: 'Primary Contact Number',
  },
  principalCityOfBusiness: {
    id: 'app.containers.ProfilePage.principalCityOfBusiness',
    defaultMessage: 'Principal City of Business',
  },
  updatePassword: {
    id: 'app.containers.RequestViewPage.updatePassword',
    defaultMessage: 'Update Password',
  },
  confirmPassword: {
    id: 'app.containers.RequestViewPage.confirmPassword',
    defaultMessage: 'Retype Password',
  },
});
