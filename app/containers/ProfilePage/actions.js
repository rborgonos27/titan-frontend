/*
 *
 * ProfilePage actions
 *
 */

import {
  GET_PROFILE,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_ERROR,
  UPDATE_PROFILE,
  UPDATE_PROFILE_SUCCESS,
  UPDATE_PROFILE_ERROR,
  UPDATE_PASSWORD,
  UPDATE_PASSWORD_SUCCESS,
  UPDATE_PASSWORD_ERROR,
} from './constants';

export function getProfile() {
  return {
    type: GET_PROFILE,
  };
}

export function getProfileSuccess(data) {
  return {
    type: GET_PROFILE_SUCCESS,
    data,
  };
}

export function getProfileErrror(error) {
  return {
    type: GET_PROFILE_ERROR,
    error,
  };
}

export function updateProfile(data) {
  return {
    type: UPDATE_PROFILE,
    data,
  };
}

export function updateProfileSuccess(data) {
  return {
    type: UPDATE_PROFILE_SUCCESS,
    data,
  };
}

export function updateProfileError(error) {
  return {
    type: UPDATE_PROFILE_ERROR,
    error,
  };
}

export function updatePassword(data) {
  return {
    type: UPDATE_PASSWORD,
    data,
  };
}

export function updatePasswordSuccess(data) {
  return {
    type: UPDATE_PASSWORD_SUCCESS,
    data,
  };
}

export function updatePasswordError(error) {
  return {
    type: UPDATE_PASSWORD_ERROR,
    error,
  };
}

