/*
 *
 * ProfilePage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  GET_PROFILE,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_ERROR,
  UPDATE_PROFILE,
  UPDATE_PROFILE_SUCCESS,
  UPDATE_PROFILE_ERROR,
  UPDATE_PASSWORD,
  UPDATE_PASSWORD_SUCCESS,
  UPDATE_PASSWORD_ERROR,
} from './constants';

export const initialState = fromJS({
  loading: false,
  data: {},
  error: null,
  errorList: [],
});

function profilePageReducer(state = initialState, action) {
  switch (action.type) {
    case GET_PROFILE:
      return state.set('loading', true);
    case GET_PROFILE_SUCCESS:
      return state.set('loading', false).set('data', action.data);
    case GET_PROFILE_ERROR:
      return state.set('loading', false).set('error', action.error);
    case UPDATE_PROFILE:
      return state.set('loading', true);
    case UPDATE_PROFILE_SUCCESS:
      return state.set('loading', false).set('data', action.data);
    case UPDATE_PROFILE_ERROR:
      return state.set('loading', false).set('error', action.error);
    case UPDATE_PASSWORD:
      return state.set('loading', true);
    case UPDATE_PASSWORD_SUCCESS:
      return state.set('loading', false).set('data', action.data);
    case UPDATE_PASSWORD_ERROR:
      return state.set('loading', false).set('error', action.error);
    default:
      return state;
  }
}

export default profilePageReducer;
