/**
 *
 * ProfilePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import StandardModal from 'components/StandardModal';
import MainAccount from 'components/MainAccount';
import Gravatar from 'react-gravatar';
import {
  Divider,
  Grid,
  Tab,
  Form,
  Input,
  Button,
  Message,
} from 'semantic-ui-react';
import { Loading } from 'components/Utils';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectProfilePage, { makeSelectCurrentUser } from './selectors';
import { getProfile, updateProfile, updatePassword } from './actions';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export class ProfilePage extends React.Component {
  componentWillMount() {
    this.props.getProfile();
  }
  componentWillReceiveProps(nextProps) {
    const { data } = nextProps.profilepage;
    this.setState({
      email: data.email,
      primaryContactName: data.primary_contact_name,
      primaryContactNumber: data.primary_contact_number,
      openConfirmModal: false,
      errorMessage: '',
      password: '',
      confirmPassword: '',
    });
  }
  state = {
    password: '',
    confirmPassword: '',
    email: '',
    primaryContactName: '',
    primaryContactNumber: '',
    profileImage: null,
    openConfirmModal: false,
    updateType: 'PROFILE',
    errorMessage: '',
  };
  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleInputNumber = e => {
    /* eslint-disable */
    const x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
    this.setState({ [e.target.name]: e.target.value });
    /* eslint-enable */
  };
  handleTabChange = (e, data) => {
    const { activeIndex } = data;
    this.setState({ updateType: activeIndex === 0 ? 'PROFILE' : 'PASSWORD' });
  };
  openModal = modalState => this.setState({ [modalState]: true });
  enableSubmitButton = () => {
    const { data } = this.props.profilepage;
    const {
      password,
      confirmPassword,
      email,
      primaryContactName,
      primaryContactNumber,
      updateType,
      profileImage,
    } = this.state;

    let disabled = true;
    if (updateType === 'PROFILE') {
      disabled =
        email === '' ||
        primaryContactName === '' ||
        primaryContactNumber === '';
      if (disabled) return true;
      if (primaryContactName !== data.primary_contact_name) return false;
      if (email !== data.email) return false;
      if (primaryContactNumber !== data.primary_contact_number) return false;
      if (profileImage) return false;
      disabled = true;
    } else if (updateType === 'PASSWORD') {
      if (profileImage) return false;
      disabled =
        password === '' ||
        confirmPassword === '' ||
        password !== confirmPassword;
    }

    return disabled;
  };
  submitData = () => {
    const { currentUser } = this.props;
    const submitData = {
      user: currentUser,
      data: this.state,
    };
    const { password, confirmPassword, updateType } = this.state;
    if (updateType === 'PASSWORD') {
      if (password !== confirmPassword) {
        const errorMessage = 'Password is not matched.';
        this.setState({ errorMessage, openConfirmModal: false });
      } else {
        this.props.updatePassword(submitData);
      }
    } else {
      this.props.updateProfile(submitData);
    }
  };
  closeModal = modalState => this.setState({ [modalState]: false });
  buildConfirmModal = () => {
    const { openConfirmModal } = this.state;
    const { loading } = this.props.profilepage;
    return (
      <StandardModal
        headerText="Proceed this Request?"
        open={openConfirmModal}
        onClose={() => this.closeModal('openConfirmModal')}
        onActionText="CONFIRM"
        onContinue={() => this.submitData()}
        btnDisabled={loading}
      >
        <div style={{ textAlign: 'left' }}>
          Are you sure you want to update your profile info?
        </div>
      </StandardModal>
    );
  };
  renderBasicInfo = () => (
    <Form>
      <Grid className="tabGrid">
        <Grid.Row>
          <Grid.Column>
            <div className="profileLabel">
              <FormattedMessage {...messages.email} />
            </div>
            <div className="profileData">
              <Form.Field>
                <Input
                  onChange={this.handleInput}
                  type="text"
                  name="email"
                  id="email"
                  size="large"
                  value={this.state.email}
                />
              </Form.Field>
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="profileLabel">
              <FormattedMessage {...messages.primaryContactName} />
            </div>
            <div className="profileData">
              <Form.Field>
                <Input
                  onChange={this.handleInput}
                  type="text"
                  name="primaryContactName"
                  id="primaryContactName"
                  size="large"
                  value={this.state.primaryContactName}
                />
              </Form.Field>
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="profileLabel">
              <FormattedMessage {...messages.primaryContactNumber} />
            </div>
            <div className="profileData">
              <Form.Field>
                <Input
                  onChange={this.handleInputNumber}
                  type="text"
                  name="primaryContactNumber"
                  id="primaryContactNumber"
                  size="large"
                  value={this.state.primaryContactNumber}
                />
              </Form.Field>
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Form>
  );
  renderSecurity = () => (
    <Form>
      <Grid className="tabGrid">
        <Grid.Row>
          <Grid.Column>
            <div className="profileLabel">
              <FormattedMessage {...messages.updatePassword} />
            </div>
            <div className="profileData">
              <Form.Field>
                <Input
                  onChange={this.handleInput}
                  type="password"
                  name="password"
                  id="password"
                  size="large"
                  value={this.state.password}
                />
              </Form.Field>
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="profileLabel">
              <FormattedMessage {...messages.confirmPassword} />
            </div>
            <div className="profileData">
              <Form.Field>
                <Input
                  onChange={this.handleInput}
                  type="password"
                  name="confirmPassword"
                  id="confirmPassword"
                  size="large"
                  value={this.state.confirmPassword}
                />
              </Form.Field>
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Form>
  );
  readURL = file => {
    const reader = new FileReader();

    reader.onload = e => {
      document.querySelector('#previewImg').src = e.target.result;
    };

    reader.readAsDataURL(file);
  };
  render() {
    const { currentUser, profilepage } = this.props;
    const { loading, data } = profilepage;
    const panes = [
      {
        menuItem: 'Basic Info',
        render: () => <Tab.Pane>{this.renderBasicInfo()}</Tab.Pane>,
      },
      {
        menuItem: 'Security',
        render: () => <Tab.Pane>{this.renderSecurity()}</Tab.Pane>,
      },
    ];
    const disabled = this.enableSubmitButton();
    return (
      <ContainerWrapper>
        <CardBox>
          <Loading loading={loading} />
          {this.state.errorMessage !== '' ? (
            <Message error>{this.state.errorMessage}</Message>
          ) : null}
          {!loading ? this.buildConfirmModal() : null}
          <MainAccount currentUser={currentUser} />
          <Grid>
            <Grid.Row>
              <Grid.Column>
                {currentUser.profile_picture || this.state.profileImage ? (
                  <img
                    src={currentUser.profile_picture}
                    alt=""
                    className="previewImg"
                    id="previewImg"
                  />
                ) : (
                  <Gravatar
                    email={currentUser.email}
                    size={100}
                    rating="pg"
                    default="identicon"
                    className="img-circle"
                  />
                )}
                <span style={{ padding: 10, fontSize: 25 }}>
                  {`${data.first_name} ${data.last_name}`}
                </span>
                {`@${data.username}`}

                <div className="profileImageUpload">
                  <Button
                    onClick={() => {
                      document.querySelector('#profilePicture').click();
                    }}
                  >
                    Upload Profile Picture
                  </Button>
                  <input
                    type="file"
                    id="profilePicture"
                    name="file"
                    encType="multipart/form-data"
                    accept="image/*"
                    onChange={e => {
                      /* eslint-disable */
                      const file = e.target.files[0];
                      this.setState({ profileImage: file });
                      this.readURL(file);
                      /* eslint-enable */
                    }}
                  />
                </div>
              </Grid.Column>
            </Grid.Row>
            <Divider />
            <Grid.Row>
              <Tab
                onTabChange={this.handleTabChange}
                className="tabMain"
                panes={panes}
              />
            </Grid.Row>
            <Grid.Row>
              <div className="buttonSaveProfile">
                <Button
                  onClick={() => this.openModal('openConfirmModal')}
                  floated="right"
                  color="orange"
                  disabled={disabled}
                >
                  Update Profile
                </Button>
              </div>
            </Grid.Row>
          </Grid>
        </CardBox>
      </ContainerWrapper>
    );
  }
}

ProfilePage.propTypes = {
  getProfile: PropTypes.func.isRequired,
  updateProfile: PropTypes.func.isRequired,
  updatePassword: PropTypes.func.isRequired,
  profilepage: PropTypes.any,
  currentUser: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  profilepage: makeSelectProfilePage(),
  currentUser: makeSelectCurrentUser(),
});
const mapDispatchToProps = { getProfile, updateProfile, updatePassword };

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
const withReducer = injectReducer({ key: 'profilePage', reducer });
const withSaga = injectSaga({ key: 'profilePage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ProfilePage);
