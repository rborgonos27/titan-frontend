import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the accountProfilePage state domain
 */

const selectGlobal = state => state.get('global');

const selectAccountProfilePageDomain = state =>
  state.get('accountProfilePage', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by AccountProfilePage
 */

const makeSelectAccountProfilePage = () =>
  createSelector(selectAccountProfilePageDomain, substate => substate.toJS());

const makeSelectCurrentUser = () => createSelector(
  selectGlobal,
  globalState => globalState.get('userData')
);

export default makeSelectAccountProfilePage;
export { selectAccountProfilePageDomain, makeSelectCurrentUser };
