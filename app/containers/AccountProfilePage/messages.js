/*
 * AccountProfilePage Messages
 *
 * This contains all the text for the AccountProfilePage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AccountProfilePage.header',
    defaultMessage: 'Account Profile',
  },
  memberAccountId: {
    id: 'app.containers.AccountProfilePage.memberAccountId',
    defaultMessage: 'Member Account Id',
  },
  locationId: {
    id: 'app.containers.AccountProfilePage.locationId',
    defaultMessage: 'Location Id',
  },
  memberAccountName: {
    id: 'app.containers.AccountProfilePage.memberAccountName',
    defaultMessage: 'Member Name',
  },
  businessAddress: {
    id: 'app.containers.AccountProfilePage.businessAddress',
    defaultMessage: 'Business Address',
  },
  businessContactNumber: {
    id: 'app.containers.AccountProfilePage.businessContactNumber',
    defaultMessage: 'Business Contact',
  },
  memberSince: {
    id: 'app.containers.AccountProfilePage.memberSince',
    defaultMessage: 'Member Since',
  },
  principalCityOfBusiness: {
    id: 'app.containers.AccountProfilePage.principalCityOfBusiness',
    defaultMessage: 'Principal City of Business',
  },
});
