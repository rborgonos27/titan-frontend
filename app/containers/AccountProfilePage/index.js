/**
 *
 * AccountProfilePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import StandardModal from 'components/StandardModal';
// import MainAccount from 'components/MainAccount';
import Gravatar from 'react-gravatar';
import moment from 'moment';
import { APP } from 'config';
import { Divider, Grid } from 'semantic-ui-react';
import { Loading } from 'components/Utils';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { getProfile, updateProfile } from './actions';
import makeSelectAccountProfilePage, {
  makeSelectCurrentUser,
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export class AccountProfilePage extends React.Component {
  /* eslint-disable */
  state = {
    businessContactNumber: '',
    openConfirmModal: false,
    errorMessage: '',
    principalCityOfBusiness: '',
  };
  /* eslint-enable */
  componentWillMount() {
    this.props.getProfile();
  }
  componentWillReceiveProps(nextProps) {
    const { data } = nextProps.accountprofilepage;
    /* eslint-disable */
    this.setState({
      businessContactNumber: data.business_contact_number,
      principalCityOfBusiness: data.principal_city_of_business,
      openConfirmModal: false,
      errorMessage: '',
    });
    /* eslint-enable */
  }
  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  disabledButton = () => {
    const { data } = this.props.accountprofilepage;
    const { businessContactNumber, principalCityOfBusiness } = this.state;
    let disabledButton = true;
    disabledButton =
      businessContactNumber === '' || principalCityOfBusiness === '';
    if (disabledButton) return true;
    if (businessContactNumber !== data.business_contact_number) return false;
    if (principalCityOfBusiness !== data.principal_city_of_business)
      return false;
    return true;
  };
  openModal = modalState => this.setState({ [modalState]: true });
  closeModal = modalState => this.setState({ [modalState]: false });
  buildConfirmModal = () => {
    const { openConfirmModal } = this.state;
    const { loading } = this.props.accountprofilepage;
    return (
      <StandardModal
        headerText="Proceed this Request?"
        open={openConfirmModal}
        onClose={() => this.closeModal('openConfirmModal')}
        onActionText="CONFIRM"
        onContinue={() => this.submitData()}
        btnDisabled={loading}
      >
        <div style={{ textAlign: 'left' }}>
          Are you sure you want to update your profile info?
        </div>
      </StandardModal>
    );
  };
  submitData = () => {
    const { currentUser } = this.props;
    const submitData = {
      user: currentUser,
      data: this.state,
    };
    this.props.updateProfile(submitData);
  };
  render() {
    const { currentUser, accountprofilepage } = this.props;
    const { loading, data } = accountprofilepage;
    const dateJoinedData = data.parent_user
      ? data.parent_user.date_joined
      : data.date_joined;
    const dateJoined = moment(dateJoinedData, APP.DATE_TIME_FORMAT).format(
      APP.DATE_DISPLAY_FORMAT,
    );
    // const disabledButton = this.disabledButton();
    console.log('currentUser', currentUser);
    console.log('data', data);
    const completeName = data.parent_user
      ? `${data.parent_user.first_name} ${data.parent_user.last_name}`
      : `${data.first_name} ${data.last_name}`;
    const profilePicture = data.parent_user
      ? `${data.parent_user.profile_picture}`
      : `${data.profile_picture}`;
    const accountEmail = data.parent_user
      ? `${data.parent_user.email}`
      : `${data.email}`;
    const accountUserName = data.parent_user
      ? `@${data.parent_user.username}`
      : `@${data.username}`;
    return (
      <ContainerWrapper>
        <CardBox>
          <Loading loading={loading} />
          <h3>
            <FormattedMessage {...messages.header} />
          </h3>
          {!loading ? this.buildConfirmModal() : null}
          <Grid>
            <Grid.Row>
              <Grid.Column>
                {profilePicture ? (
                  <img src={profilePicture} alt="" className="previewImg" />
                ) : (
                  <Gravatar
                    email={accountEmail}
                    size={100}
                    rating="pg"
                    default="identicon"
                    className="img-circle"
                  />
                )}
                <span style={{ padding: 10, fontSize: 25 }}>
                  {completeName}
                </span>
                {accountUserName}
              </Grid.Column>
            </Grid.Row>
            <Divider />
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.memberAccountId} />
                </div>
                <div className="profileData">
                  {data.parent_user
                    ? data.parent_user.member_account_id
                    : data.member_account_id}
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.locationId} />
                </div>
                <div className="profileData">
                  {data.parent_user
                    ? data.parent_user.location_id
                    : data.location_id}
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.memberAccountName} />
                </div>
                <div className="profileData">
                  {data.parent_user
                    ? data.parent_user.business_name
                    : data.business_name}
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.businessAddress} />
                </div>
                <div className="profileData">
                  {data.parent_user ? data.parent_user.address : data.address}
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.businessContactNumber} />
                </div>
                <div className="profileData">
                  {data.parent_user
                    ? data.parent_user.business_contact_number
                    : data.business_contact_number}
                  {/* eslint-disable */}
                  {/*<Form>*/}
                    {/*<Form.Field>*/}
                      {/*<Input*/}
                        {/*onChange={this.handleInput}*/}
                        {/*type="text"*/}
                        {/*name="businessContactNumber"*/}
                        {/*id="businessContactNumber"*/}
                        {/*size="large"*/}
                        {/*value={this.state.businessContactNumber}*/}
                      {/*/>*/}
                    {/*</Form.Field>*/}
                  {/*</Form>*/}
                  {/* eslint-enable */}
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.principalCityOfBusiness} />
                </div>
                <div className="profileData">
                  {data.parent_user
                    ? data.parent_user.principal_city_of_business
                    : data.principal_city_of_business}
                  {/* eslint-disable */}
                  {/*<Form>*/}
                    {/*<Form.Field>*/}
                      {/*<Input*/}
                        {/*onChange={this.handleInput}*/}
                        {/*type="text"*/}
                        {/*name="principalCityOfBusiness"*/}
                        {/*id="principalCityOfBusiness"*/}
                        {/*size="large"*/}
                        {/*value={this.state.principalCityOfBusiness}*/}
                      {/*/>*/}
                    {/*</Form.Field>*/}
                  {/*</Form>*/}
                  {/* eslint-enable */}
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.memberSince} />
                </div>
                <div className="profileData">{dateJoined}</div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              {/* eslint-disable */}
              {/*<div className="buttonSaveProfile">*/}
                {/*<Button*/}
                  {/*onClick={() => this.openModal('openConfirmModal')}*/}
                  {/*floated="right"*/}
                  {/*color="orange"*/}
                  {/*disabled={disabledButton}*/}
                {/*>*/}
                  {/*Update Account*/}
                {/*</Button>*/}
              {/*</div>*/}
              {/* eslint-enable */}
            </Grid.Row>
          </Grid>
        </CardBox>
      </ContainerWrapper>
    );
  }
}

AccountProfilePage.propTypes = {
  getProfile: PropTypes.func.isRequired,
  updateProfile: PropTypes.func.isRequired,
  currentUser: PropTypes.any,
  accountprofilepage: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  accountprofilepage: makeSelectAccountProfilePage(),
  currentUser: makeSelectCurrentUser(),
});

const mapDispatchToProps = { getProfile, updateProfile };

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'accountProfilePage', reducer });
const withSaga = injectSaga({ key: 'accountProfilePage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AccountProfilePage);
