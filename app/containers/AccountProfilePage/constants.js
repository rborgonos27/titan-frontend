/*
 *
 * AccountProfilePage constants
 *
 */

export const DEFAULT_ACTION = 'app/AccountProfilePage/DEFAULT_ACTION';

export const GET_PROFILE = 'app/AccountProfilePage/GET_PROFILE';
export const GET_PROFILE_SUCCESS = 'app/AccountProfilePage/GET_PROFILE_SUCCESS';
export const GET_PROFILE_ERROR = 'app/AccountProfilePage/GET_PROFILE_ERROR';

export const UPDATE_PROFILE = 'app/AccountProfilePage/UPDATE_PROFILE';
export const UPDATE_PROFILE_SUCCESS =
  'app/AccountProfilePage/UPDATE_PROFILE_SUCCESS';
export const UPDATE_PROFILE_ERROR =
  'app/AccountProfilePage/UPDATE_PROFILE_ERROR';
