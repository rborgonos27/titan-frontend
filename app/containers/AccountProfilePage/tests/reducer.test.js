import { fromJS } from 'immutable';
import accountProfilePageReducer from '../reducer';

describe('accountProfilePageReducer', () => {
  it('returns the initial state', () => {
    expect(accountProfilePageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
