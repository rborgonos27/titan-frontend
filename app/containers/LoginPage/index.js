/**
 *
 * LoginPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Form, Button, Input, Icon, Message } from 'semantic-ui-react';
import StandardModal from 'components/StandardModal';

import TitanBanner from 'themes/assets/images/logo.png';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { makeSelectLoginPage, makeSelectError } from './selectors';
import reducer from './reducer';
import saga from './saga';
import { login, reset, sendMessage, forgotPassword } from './actions';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export class LoginPage extends React.Component {
  state = {
    form: {
      email: localStorage.getItem('username')
        ? localStorage.getItem('username')
        : '',
      password: '',
      location: null,
      forgotPasswordUserNameOrEmail: null,
    },
    openAttempModal: false,
    openForgotPassword: false,
  };
  componentDidMount() {
    this.props.reset();
    const { location } = this.props.history;
    const form = Object.assign({}, this.state.form);
    form.location = location;
    /* eslint-disable */
    this.setState({ form });
    /* eslint-enable */
  }
  componentWillReceiveProps(nextProps) {
    const { attemp } = nextProps.loginpage;
    if (attemp >= 5) {
      this.setState({ openAttempModal: true });
      if (this.state.openAttempModal) {
        this.props.sendMessage(this.state.form);
      }
    }
  }
  handleForgotPassword = () => {
    const { forgotPasswordUserNameOrEmail } = this.state.form;
    if (forgotPasswordUserNameOrEmail) {
      this.props.forgotPassword(this.state.form);
    }
  };
  handleSubmit = e => {
    e.preventDefault();
    const { form } = this.state;
    this.props.login(form);
  };
  onChange = e => {
    const form = Object.assign({}, this.state.form);
    const { name, value } = e.target;
    form[name] = value;
    this.setState({ form });
  };
  showErrorMessage() {
    const { error } = this.props;

    return error ? <Message negative>{error}</Message> : null;
  }
  showSuccessMessage() {
    const { message } = this.props.loginpage;

    return message ? <Message positive>{message}</Message> : null;
  }
  buildMaxAttempModal = () => (
    <StandardModal
      headerText="Maximum Login Attempt"
      open={this.state.openAttempModal}
      onClose={() => {
        this.setState({ openAttempModal: false });
      }}
      onActionText="Close"
      onContinue={() => {
        this.setState({ openAttempModal: false });
      }}
    >
      <div>
        Your account has been temporarily suspended due to failed attempts.
        Please check your email to reset password. Thank you
      </div>
    </StandardModal>
  );
  buildForgotPasswordModal = () => (
    <StandardModal
      headerText="Forgot Password"
      open={this.state.openForgotPassword}
      onClose={() => {
        this.setState({ openForgotPassword: false });
      }}
      onActionText="Submit"
      onContinue={() => {
        this.handleForgotPassword();
        this.setState({ openForgotPassword: false });
      }}
    >
      <div>
        <Input fluid iconPosition="left" placeholder="Email or Username">
          <Icon name="at" />
          <input
            name="forgotPasswordUserNameOrEmail"
            value={this.state.form.forgotPasswordUserNameOrEmail}
            onChange={this.onChange}
          />
        </Input>
      </div>
    </StandardModal>
  );
  render() {
    const { form } = this.state;
    const { loading } = this.props.loginpage;
    return (
      <div>
        {this.buildMaxAttempModal()}
        {this.buildForgotPasswordModal()}
        <div className="clearfix" />
        <div className="row">
          <div className="login-banner col-lg-4">
            <div className="banner-header">
              <img alt="" src={TitanBanner} />
              <p>
                <FormattedMessage {...messages.subTitle} />
              </p>
            </div>
          </div>
          <div className="login-height col-lg-8">
            <div className="login-content">
              <div className="panel-heading">
                <h1 className="text-center">
                  <strong>
                    <FormattedMessage {...messages.welcome} />
                  </strong>
                </h1>
                <div className="text-center">
                  <FormattedMessage {...messages.welcomeMessage} />
                </div>
              </div>
              <div className="panel-body">
                <div className="login-wrap">
                  {this.showErrorMessage()}
                  {this.showSuccessMessage()}
                  <Form size="massive" loading={loading}>
                    <Form.Field>
                      <Input fluid iconPosition="left" placeholder="Email">
                        <Icon name="at" />
                        <input
                          name="email"
                          value={form.email}
                          onChange={this.onChange}
                        />
                      </Input>
                    </Form.Field>
                    <Form.Field>
                      <Input fluid iconPosition="left" placeholder="Password">
                        <Icon name="lock" />
                        <input
                          type="password"
                          name="password"
                          value={form.password}
                          onChange={this.onChange}
                        />
                      </Input>
                    </Form.Field>
                    <Form.Field>
                      <Button
                        fluid
                        size="massive"
                        onClick={this.handleSubmit}
                        color="orange"
                        disabled={this.props.loginpage.attemp >= 5}
                      >
                        <FormattedMessage {...messages.login} />
                      </Button>
                    </Form.Field>
                  </Form>
                  <div className="text-center login-text">
                    <div>
                      {/* eslint-disable */}
                      <a
                        href="#"
                        className="text-primary m-l-5"
                        onClick={() =>
                          this.setState({ openForgotPassword: true })
                        }
                      >
                      {/* eslint-enable */}
                        <b>Forgot Password?</b>
                      </a>
                    </div>
                    <FormattedMessage {...messages.signupMessage} />
                    <a
                      href="https://www.titan-vault.com/#site-footer"
                      className="text-primary m-l-5"
                    >
                      <b>
                        <FormattedMessage {...messages.signup} />
                      </b>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

LoginPage.propTypes = {
  login: PropTypes.func.isRequired,
  sendMessage: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  forgotPassword: PropTypes.func.isRequired,
  loginpage: PropTypes.object.isRequired,
  error: PropTypes.any,
  history: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  loginpage: makeSelectLoginPage(),
  error: makeSelectError(),
});

const mapDispatchToProps = { login, reset, sendMessage, forgotPassword };

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'loginPage', reducer });
const withSaga = injectSaga({ key: 'loginPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(LoginPage);
