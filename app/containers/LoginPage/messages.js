/*
 * LoginPage Messages
 *
 * This contains all the text for the LoginPage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.LoginPage.header',
    defaultMessage: 'This is LoginPage container !',
  },
  subTitle: {
    id: 'app.containers.LoginPage.subTitle',
    defaultMessage:
      'The new way of providing financial services for cash-intensive companies.' +
      ' Titan Vault help you store and insure cash and make payments electronically.',
  },
  welcome: {
    id: 'app.containers.LoginPage.welcome',
    defaultMessage: 'Welcome',
  },
  welcomeMessage: {
    id: 'app.containers.LoginPage.welcomeMessage',
    defaultMessage: 'Sign in to continue to Titan Vault',
  },
  login: {
    id: 'app.containers.LoginPage.login',
    defaultMessage: 'Log In',
  },
  signup: {
    id: 'app.containers.LoginPage.signup',
    defaultMessage: "Let's Talk",
  },
  signupMessage: {
    id: 'app.containers.LoginPage.signupMessage',
    defaultMessage: 'Not a Titan Vault Member yet?',
  },
  errMsg: {
    id: 'app.containers.LoginPage.errMsg',
    defaultMessage: 'There was some errors with your submission!',
  },
});
