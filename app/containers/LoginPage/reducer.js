/*
 *
 * LoginPage reducer
 *
 */

import { fromJS } from 'immutable';
import { LOGIN_SUCCESS, LOGIN_FAILED, LOGIN, ON_RESET, FORGOT_PASSWORD, FORGOT_PASSWORD_ERROR, FORGOT_PASSWORD_SUCCESS } from './constants';

export const initialState = fromJS({
  loading: false,
  error: null,
  errorList: [],
  attemp: 0,
});

function loginPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN: {
      const attempCount = state.get('attemp') + 1;
      let error = null;
      if (attempCount >= 5) {
        error =
          'Your account has been temporarily suspended due to failed attempts. Please check your email to reset password. Thank you';
      }
      return state
        .set('error', error)
        .set('errorList', [])
        .set('loading', true)
        .set('attemp', attempCount);
    }
    case LOGIN_FAILED:
      return state
        .set('error', action.data.error)
        .set('errorList', [action.data.error])
        .set('loading', false);
    case LOGIN_SUCCESS:
      return state
        .set('error', null)
        .set('errorList', [])
        .set('attemp', 0)
        .set('loading', false);
    case FORGOT_PASSWORD:
      return state
        .set('error', null)
        .set('errorList', [])
        .set('loading', true);
    case FORGOT_PASSWORD_SUCCESS:
      return state
        .set('error', null)
        .set('errorList', [])
        .set('message', action.data.message)
        .set('loading', false);
    case FORGOT_PASSWORD_ERROR:
      return state
        .set('error', action.error.message)
        .set('errorList', [])
        .set('message', null)
        .set('loading', false);
    case ON_RESET:
      return state
        .set('error', null)
        .set('errorList', [])
        .set('attemp', 0)
        .set('loading', false);
    default:
      return state;
  }
}

export default loginPageReducer;
