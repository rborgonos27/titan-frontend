/*
 *
 * LoginPage actions
 *
 */

import {
  DEFAULT_ACTION,
  LOGIN,
  LOGIN_FAILED,
  LOGIN_SUCCESS,
  ON_RESET,
  SEND_MESSAGE,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function login(data) {
  return {
    type: LOGIN,
    data,
  };
}

export function loginSuccess(data) {
  return {
    type: LOGIN_SUCCESS,
    data,
  };
}

export function loginFailed(data) {
  return {
    type: LOGIN_FAILED,
    data,
  };
}

export function forgotPassword(data) {
  return {
    type: FORGOT_PASSWORD,
    data,
  };
}

export function forgotPasswordSuccess(data) {
  return {
    type: FORGOT_PASSWORD_SUCCESS,
    data,
  };
}

export function forgotPasswordError(error) {
  return {
    type: FORGOT_PASSWORD_ERROR,
    error,
  };
}

export function reset() {
  return {
    type: ON_RESET,
  };
}

export function sendMessage(data) {
  return {
    type: SEND_MESSAGE,
    data,
  };
}
