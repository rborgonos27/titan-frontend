/*
 *
 * LoginPage constants
 *
 */

export const DEFAULT_ACTION = 'app/LoginPage/DEFAULT_ACTION';
export const LOGIN = 'app/LoginPage/LOGIN';
export const LOGIN_SUCCESS = 'app/LoginPage/LOGIN_SUCCESS';
export const LOGIN_FAILED = 'app/LoginPage/LOGIN_FAILED';
export const ON_RESET = 'app/LoginPage/ON_RESET';
export const SEND_MESSAGE = 'app/LoginPage/SEND_MESSAGE';
export const FORGOT_PASSWORD = 'app/LoginPage/FORGOT_PASSWORD';
export const FORGOT_PASSWORD_SUCCESS = 'app/LoginPage/FORGOT_PASSWORD_SUCCESS';
export const FORGOT_PASSWORD_ERROR = 'app/LoginPage/FORGOT_PASSWORD_ERROR';
