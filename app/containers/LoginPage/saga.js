import { call, put, all, takeLatest } from 'redux-saga/effects';
import ReactGA from 'react-ga';
import request, { getReqErrMsg } from 'utils/request';
import { options, optionsFormData } from 'utils/options';
import { push } from 'react-router-redux';
import moment from 'moment';
import { APP, GA } from 'config';
import { loginSuccessAction, loginErrorAction } from 'containers/App/actions';
import {
  loginSuccess,
  loginFailed,
  forgotPasswordError,
  forgotPasswordSuccess,
} from './actions';
import { LOGIN, SEND_MESSAGE, FORGOT_PASSWORD } from './constants';

/* eslint-disable */
const optionsSpecial = (method, body = null) => {
  const opt = {
    headers: {
      'Content-type': 'application/json',
      Accept: 'application/json',
    },
    method,
  };
  if (body) opt.body = JSON.stringify(body);
  return opt;
};
/* eslint-enable */

export function* login(form) {
  try {
    ReactGA.initialize(GA.TRACKING_ID);
    const API_URI = APP.API_URL;
    const formData = Object.assign({}, form.data);
    formData.username_or_email = formData.email;
    const loginResponse = yield call(
      request,
      `${API_URI}/user/login`,
      options('POST', formData),
    );
    ReactGA.event({
      category: 'Login',
      action: `Submit Login Info with username: ${JSON.stringify({
        user: formData.email,
      })}`,
      label: 'User Login',
    });
    if (loginResponse.success) {
      sessionStorage.clear();
      localStorage.clear();
      /* eslint-disable */
      const currentServer =
        location.protocol
        +'//'+location.hostname
        +(location.port ? ':'+location.port: '');
      /* eslint-enable */
      sessionStorage.setItem('currentServer', currentServer);
      const loginData = loginResponse.data;
      const token = JSON.stringify(loginData.token);
      let userData = JSON.stringify(loginData.user);
      ReactGA.event({
        category: 'Login',
        action: `Login Successful, entering app ${loginData.user.full_name} ${
          loginData.user.user_type
        }`,
        label: 'User Login Success',
      });
      localStorage.setItem('token', token);
      localStorage.setItem(
        'token_created',
        moment().format('YYYY-MM-DD hh:mm:ss a'),
      );
      localStorage.setItem('userData', userData);
      userData = JSON.parse(userData);
      const redirectString = decodeURIComponent(
        form.data.location.search,
      ).replace('?redirect=', '');
      const redirect = redirectString === '' ? '/' : redirectString;
      yield put(loginSuccessAction({ token, userData, redirect }));
      yield put(loginSuccess({ token, userData }));
      yield put(push('/'));
    } else {
      ReactGA.event({
        category: 'Login',
        action: `Error login ${JSON.stringify(loginResponse)}`,
        label: 'User Login Failed',
      });
      yield put(loginFailed({ error: loginResponse.non_field_errors[0] }));
      yield put(loginErrorAction(getReqErrMsg(loginResponse)));
    }
  } catch (error) {
    ReactGA.event({
      category: 'Login',
      action: `Error login ${error}`,
      label: 'User Login Failed',
    });
    yield put(loginFailed(getReqErrMsg(error)));
    yield put(loginErrorAction(getReqErrMsg(error)));
  }
}

export function* sendMessage(info) {
  try {
    const API_URI = APP.API_URL;
    const resetPasswordData = new FormData();
    resetPasswordData.append('username_or_email', info.data.email);
    yield call(
      request,
      `${API_URI}/send_attempts/`,
      optionsFormData('POST', resetPasswordData),
    );
  } catch (error) {
    console.log(error);
  }
}

export function* forgotPassword(info) {
  try {
    const API_URI = APP.API_URL;
    const resetPasswordData = new FormData();
    resetPasswordData.append(
      'username_or_email',
      info.data.forgotPasswordUserNameOrEmail,
    );
    const loginResponse = yield call(
      request,
      `${API_URI}/forgot_password/`,
      optionsFormData('POST', resetPasswordData),
    );
    if (loginResponse) {
      yield put(forgotPasswordSuccess(loginResponse));
    } else {
      yield put(forgotPasswordError(loginResponse));
    }
  } catch (error) {
    yield put(forgotPasswordError(error));
  }
}

export function* loginWatcher() {
  yield takeLatest(LOGIN, login);
}

export function* sendMessageWatcher() {
  yield takeLatest(SEND_MESSAGE, sendMessage);
}

export function* forgotPasswordWatcher() {
  yield takeLatest(FORGOT_PASSWORD, forgotPassword);
}

// Individual exports for testing
export default function* defaultSaga() {
  yield all([loginWatcher(), sendMessageWatcher(), forgotPasswordWatcher()]);
}
