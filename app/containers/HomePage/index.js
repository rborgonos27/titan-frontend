/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
// import ProgressBar from 'react-line-progress';
import ProgressBar from 'progressbar.js';
import ReactSpeedometer from 'react-d3-speedometer';

import ContainerWrapper from 'components/ContainerWrapper';
import ChartPolar from 'components/ChartPolar';
import PageTitle from 'components/PageTitle';
import CardBox from 'components/CardBox';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
class HomePage extends React.PureComponent {
  state = {
    auth: sessionStorage.getItem('auth') ? sessionStorage.getItem('auth') : null,
  }
  componentWillMount() {
    if (!this.state.auth) {
      // this.props.history.push('/login');
    }
  }
  componentDidMount() {
    const progressbar = document.querySelector('#progressbar1');
    const bar = new ProgressBar.Line(progressbar, {
      strokeWidth: 4,
      easing: 'easeInOut',
      duration: 1400,
      color: '#FFEA82',
      trailColor: '#eee',
      trailWidth: 1,
      style: {
        radius: '50px',
      },
      svgStyle: { width: '100%', height: '100%'},
      from: { color: '#FFEA82' },
      to: { color: '#faad41' },
      step: (state, barline) => {
        barline.path.setAttribute('stroke', state.color);
      },
    });
    bar.animate(0.8);
    // this.createGauge('PreviewGaugeMeter_2', 0.5);
    this.createGauge('riskLevel', 0.5);
    this.createGauge('depositLevel', 0.36);
    this.createGauge('coynsScore', 0.56);
  }
  createGauge = (selector, gaugeValue) => {
    const guageMeter = document.querySelector(`#${selector}`);
    const guage = new ProgressBar.Circle(guageMeter, {
      strokeWidth: 6,
      color: '#FFEA82',
      trailColor: '#eee',
      trailWidth: 1,
      easing: 'easeInOut',
      duration: 1400,
      svgStyle: null,
      text: {
        value: '',
        alignToBottom: false,
      },
      from: { color: '#FFEA82' },
      to: { color: '#ff4400' },
      // Set default step function for all animate calls
      step: (state, barCircle) => {
        barCircle.path.setAttribute('stroke', state.color);
        const value = Math.round(barCircle.value() * 100);
        if (value === 0) {
          barCircle.setText('');
        } else {
          barCircle.setText(`${value} %`);
        }
        // barCircle.text.style.color = state.color;
      },
    });
    guage.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
    guage.text.style.fontSize = '3rem';

    guage.animate(gaugeValue);
  };
  render() {
    return (
      <div>
        <Helmet>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="width=device-width,
             initial-scale=1.0"
          />
        </Helmet>
        <ContainerWrapper>
          <div className="row">
            <div className="col-lg-8">
              <PageTitle>
                <FormattedMessage {...messages.totalDeposit} />
              </PageTitle>
              <CardBox>
                <div className="col-lg-6">
                  <ChartPolar />
                </div>
                <div className="col-lg-6 total-deposit">
                  <i className="fa fa-circle pull-left bullet orange" />
                  <div className="pull-left">
                    <h2 className="m-t-0">
                      <b>$ 1,000</b>
                    </h2>
                    <div>Amount is available right now</div>
                  </div>
                  <span className="clearfix" style={{ clear: 'both' }} />
                  <i className="fa fa-circle pull-left bullet orange-2" />
                  <div className="pull-left">
                    <h2 className="m-t-0">
                      <b>$ 250</b>
                    </h2>
                    <div>Amount in progress</div>
                  </div>
                  <div
                    id="progressbar1"
                    style={{ clear: 'both', marginLeft: 20 }}
                  />
                  <div className="pull-left" style={{ marginLeft: 20 }}>
                    Deposit request
                  </div>
                  <div className="pull-right">5 days left</div>
                </div>
              </CardBox>
            </div>
            <div className="col-lg-4">
              <PageTitle>
                <FormattedMessage {...messages.totalTransacted} />
              </PageTitle>
              <div className="card-box text-center p-t-40 p-b-40">
                <div className="m-t-0 m-b-20 widget-link">
                  <i className="widget-wallet icon-wallet icon-dashboard-widget orange" />
                </div>
                <h1 className="m-t-0 m-b-20">
                  <b>$ 20,000</b>
                </h1>
                <div>Total amount has been</div>
              </div>
            </div>
          </div>
          <div className="row">
            <PageTitle>
              <FormattedMessage {...messages.totalPayments} />
            </PageTitle>
            <div className="col-lg-5">
              <CardBox>
                <h4 className="m-t-0 m-b-20 header-title text-center">
                  <b>Speedometer</b>
                </h4>
                <div className="text-center">
                  Payment has been made since Day <span>1</span>
                </div>
                <br />
                <br />
                <div className="clearfix" />
                <div className="text-center">
                  <ReactSpeedometer
                    segments={10}
                    minValue={100}
                    maxValue={500}
                    width={300}
                    height={180}
                    startColor="#FFEA82"
                    endColor="#ff4400"
                    value={250}
                    needleColor="steelblue"
                    needleTransitionDuration={4000}
                    needleTransition="easeElastic"
                    currentValueText="USD: ${value}"
                  />
                </div>
              </CardBox>
            </div>
            <div className="col-lg-7">
              <CardBox>
                <h4 className="m-t-0 header-title">
                  <b>Top 5 Details</b>
                </h4>
                <div className="p-20">
                  <div className="table-responsive">
                    <table className="table m-0">
                      <thead>
                        <tr>
                          <th className="gray">Date</th>
                          <th className="gray">Type</th>
                          <th className="gray">Gross</th>
                          <th className="gray">Fee</th>
                          <th className="gray">Amount</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td className="black">1 Mar, 2017</td>
                          <td className="black">Payment</td>
                          <td className="orange">$ 207,00 USD</td>
                          <td className="orange">$ -5,37 USD</td>
                          <td className="orange">$ 201,63 USD</td>
                        </tr>
                        <tr>
                          <td className="black">27 Feb, 2017</td>
                          <td className="black">Payment</td>
                          <td className="orange">$ 59,00 USD</td>
                          <td className="orange">$ -2,54 USD</td>
                          <td className="orange">$ 56,46 USD</td>
                        </tr>
                        <tr>
                          <td className="black">25, Feb, 2017</td>
                          <td className="black">Payment</td>
                          <td className="orange">$ 77,00 USD</td>
                          <td className="orange">$ -3,30 USD</td>
                          <td className="orange">$ 73,7 USD</td>
                        </tr>
                        <tr>
                          <td className="black">17 Feb, 2017</td>
                          <td className="black">Payment</td>
                          <td className="orange">$ 44,00 USD</td>
                          <td className="orange">$ -2,00 USD</td>
                          <td className="orange">$ 42,00 USD</td>
                        </tr>
                        <tr>
                          <td className="black">17 Feb, 2017</td>
                          <td className="black">Payment</td>
                          <td className="orange">$ 50,00 USD</td>
                          <td className="orange">$ -0,04 USD</td>
                          <td className="orange">$ 49,96 USD</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </CardBox>
            </div>
          </div>
          <div className="row">
            <PageTitle>
              <FormattedMessage {...messages.memberOverview} />
            </PageTitle>
            <div>
              <div className="col-lg-4">
                <div className="card-box text-center p-t-40 p-b-40">
                  <div className="m-t-0 m-b-20 widget-link">
                    <i className="widget-wallet icon-user icon-dashboard-widget orange" />
                  </div>
                  <h2 className="m-t-0 m-b-20">
                    <b>15 Jul 2017</b>
                  </h2>
                  <div>Member since</div>
                </div>
              </div>
              <div className="col-lg-8">
                <div className="card-box">
                  <h4 className="m-t-0 header-title">
                    <b>Member Ranking / Tier</b>
                  </h4>
                  <div className="widget-chart text-center progress-bar-wrap">
                    <br />
                    <div className="col-lg-4 progress-bar-titan">
                      <div id="riskLevel" /> <br />
                      <div>Risk Level</div>
                    </div>
                    <div className="col-lg-4 progress-bar-titan">
                      <div id="depositLevel" /> <br />
                      <div>Deposit Level</div>
                    </div>
                    <div className="col-lg-4 progress-bar-titan">
                      <div id="coynsScore" /> <br />
                      <div>Coyns Score</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </ContainerWrapper>
      </div>
    );
  }
}

HomePage.protoTypes = {
  history: PropTypes.any,
};

export default HomePage;
