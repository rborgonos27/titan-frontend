/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.HomePage.header',
    defaultMessage: 'This is HomePage component!',
  },
  totalDeposit: {
    id: 'app.components.HomePage.totalDeposit',
    defaultMessage: 'Total Deposit',
  },
  totalTransacted: {
    id: 'app.components.HomePage.totalTransacted',
    defaultMessage: 'Total Transacted',
  },
  totalPayments: {
    id: 'app.components.HomePage.totalPayments',
    defaultMessage: 'Total Payments',
  },
  memberOverview: {
    id: 'app.components.HomePage.memberOverview',
    defaultMessage: 'Member Overview',
  },
});
