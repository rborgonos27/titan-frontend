/*
 *
 * ManagePayeePage constants
 *
 */

export const DEFAULT_ACTION = 'app/ManagePayeePage/DEFAULT_ACTION';

export const GOTO_PAYEE = 'app/ManagePayeePage/GOTO_PAYEE';

export const ADD_PAYEE = 'app/ManagePayeePage/ADD_PAYEE';
export const ADD_PAYEE_SUCCESS = 'app/ManagePayeePage/ADD_PAYEE_SUCCESS';
export const ADD_PAYEE_ERROR = 'app/ManagePayeePage/ADD_PAYEE_ERROR';

export const GET_USER = 'app/ManagePayeePage/GET_USER';
export const GET_USER_SUCCESS = 'app/ManagePayeePage/GET_USER_SUCCESS';
export const GET_USER_ERROR = 'app/ManagePayeePage/GET_USER_ERROR';

export const GET_PAYEE = 'app/ManagePayeePage/GET_PAYEE';
export const GET_PAYEE_SUCCESS = 'app/ManagePayeePage/GET_PAYEE_SUCCESS';
export const GET_PAYEE_ERROR = 'app/ManagePayeePage/GET_PAYEE_ERROR';

export const GET_USER_PAYEE = 'app/ManagePayeePage/GET_USER_PAYEE';
export const GET_USER_PAYEE_SUCCESS =
  'app/ManagePayeePage/GET_USER_PAYEE_SUCCESS';
export const GET_USER_PAYEE_ERROR = 'app/ManagePayeePage/GET_USER_PAYEE_ERROR';

export const UPDATE_PAYEE = 'app/ManagePayeePage/UPDATE_PAYEE';
export const UPDATE_PAYEE_SUCCESS = 'app/ManagePayeePage/UPDATE_PAYEE_SUCCESS';
export const UPDATE_PAYEE_ERROR = 'app/ManagePayeePage/UPDATE_PAYEE_ERROR';

export const ON_RESET = 'app/ManagePayeePage/ON_RESET';

export const GET_STATE_CITIES = 'app/ManagePayeePage/GET_STATE_CITIES';
export const GET_STATE_CITIES_ERROR = 'app/ManagePayeePage/GET_STATE_CITIES_ERROR';
export const GET_STATE_CITIES_SUCCESS =
  'app/ManagePayeePage/GET_STATE_CITIES_SUCCESS';
