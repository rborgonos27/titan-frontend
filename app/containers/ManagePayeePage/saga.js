import { all, call, put, takeLatest } from 'redux-saga/effects';
import ReactGA from 'react-ga';
import { push } from 'react-router-redux';
import request from 'utils/request';
import { APP, GA } from 'config';
import { optionsFormData } from 'utils/options';
import {
  ADD_PAYEE,
  GET_USER,
  GET_PAYEE,
  GET_USER_PAYEE,
  UPDATE_PAYEE,
  GET_STATE_CITIES,
  GOTO_PAYEE,
} from './constants';
import {
  addPayeeSuccess,
  addPayeeError,
  getUserSuccess,
  getUserError,
  getPayeeSuccess,
  getPayeeError,
  getUserPayeeError,
  getUserPayeeSuccess,
  updatePayeeSuccess,
  updatePayeeError,
  getStateCitiesSuccess,
  getStateCitiesError,
} from './actions';

export function* addPayee(payee) {
  try {
    ReactGA.initialize(GA.TRACKING_ID);
    const API_URI = APP.API_URL;
    const formData = new FormData();
    const {
      payeeName,
      bankName,
      bankAccount,
      bankAccountName,
      address,
      address2,
      state,
      city,
      zipCode,
      bankRoutingNumber,
      addedUser,
      payeeEmail,
      currentUser,
    } = payee.data;
    formData.append('name', payeeName);
    formData.append('bank_name', bankName);
    formData.append('bank_account_name', bankAccountName);
    formData.append('bank_account', bankAccount);
    formData.append('bank_routing_number', bankRoutingNumber);
    formData.append('email', payeeEmail);
    formData.append('address', address);
    formData.append('address_2', address2);
    formData.append('state', state);
    const status =
      currentUser.user_type === 'super_admin' ? 'APPROVED' : 'REQUESTED';
    formData.append('status', status);
    formData.append('city', city);
    formData.append('zip_code', zipCode);
    const token = yield JSON.parse(localStorage.getItem('token'));
    const payeeResponse = yield call(
      request,
      `${API_URI}/payee/`,
      optionsFormData('POST', formData, token),
    );
    ReactGA.event({
      category: 'Payee',
      action: `Submit payee details: ${JSON.stringify({
        payeeName,
        bankName,
        bankAccount,
        bankAccountName,
        address,
        bankRoutingNumber,
        addedUser,
      })}`,
      label: 'Manage Payee',
    });
    if (payeeResponse.name) {
      const userPayeeFormData = new FormData();
      /* eslint-disable */
      for (const i in addedUser) {
        const user = addedUser[i];
        userPayeeFormData.append('user_id', user.id);
        userPayeeFormData.append('payee_id', payeeResponse.id);
        const userPayeeResponse = yield call(
          request,
          `${API_URI}/user_payee/`,
          optionsFormData('POST', userPayeeFormData, token),
        );
        /* eslint-enable */
      }
      ReactGA.event({
        category: 'Payee',
        action: `Payee Add Success`,
        label: 'Manage Payee Success',
      });
      if (currentUser.user_type !== 'super_admin') {
        const payeeNotification = new FormData();
        payeeNotification.append(
          'name',
          `${currentUser.first_name} ${currentUser.last_name}`,
        );
        payeeNotification.append('payee_id', payeeResponse.id);
        payeeNotification.append('payee_name', payeeResponse.name);

        yield call(
          request,
          `${API_URI}/new/payee/notif`,
          optionsFormData('POST', payeeNotification, token),
        );
      }
      if (currentUser.user_type === 'super_admin') {
        yield put(push('/payee'));
      }
      yield put(addPayeeSuccess(payeeResponse));
    } else {
      yield put(addPayeeError(payeeResponse));
      ReactGA.event({
        category: 'Payee',
        action: `Payee Add Success ${JSON.stringify(payeeResponse)}`,
        label: 'Manage Payee Failed',
      });
    }
  } catch (error) {
    ReactGA.event({
      category: 'Payee',
      action: `Payee Add Success ${JSON.stringify(error)}`,
      label: 'Manage Payee Failed',
    });
    yield put(addPayeeError(error));
  }
}

export function* goToPayee() {
  yield put(push('/payee'));
}
export function* getUser(user) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userResponse = yield call(
      request,
      `${API_URI}/admin/user/detail/?search=${user.data}`,
      optionsFormData('GET', null, token),
    );
    if (userResponse.results) {
      yield put(getUserSuccess(userResponse));
    }
  } catch (error) {
    yield put(getUserError(error));
  }
}

export function* getPayee(payee) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const payeeResponse = yield call(
      request,
      `${API_URI}/payee/${payee.data}/`,
      optionsFormData('GET', null, token),
    );
    yield put(getPayeeSuccess(payeeResponse));
  } catch (error) {
    yield put(getPayeeError(error));
  }
}

export function* getUserPayee(payee) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const payeeResponse = yield call(
      request,
      `${API_URI}/payee_user/?payee_id=${payee.data}`,
      optionsFormData('GET', null, token),
    );
    if (payeeResponse.success) {
      const payeeUserData = [];
      /* eslint-disable */
      for (let i = 0; i < payeeResponse.data.length; i++) {
        const userPayeeData = payeeResponse.data[i];
        payeeUserData.push(userPayeeData.user);
      }
      /* eslint-enable */
      yield put(getUserPayeeSuccess(payeeUserData));
    } else {
      yield put(getUserPayeeSuccess(getUserPayeeError.message));
    }
  } catch (error) {
    yield put(getUserPayeeSuccess(error));
  }
}

export function* updatePayee(payee) {
  try {
    const API_URI = APP.API_URL;
    const formData = new FormData();
    const {
      payeeName,
      bankName,
      bankAccount,
      bankAccountName,
      address,
      address2,
      state,
      city,
      zipCode,
      bankRoutingNumber,
      addedUser,
      payeeId,
      payeeEmail,
    } = payee.data;
    formData.append('name', payeeName);
    formData.append('bank_name', bankName);
    formData.append('bank_account_name', bankAccountName);
    formData.append('bank_account', bankAccount);
    formData.append('bank_routing_number', bankRoutingNumber);
    formData.append('email', payeeEmail);
    formData.append('address', address);
    formData.append('address_2', address2);
    formData.append('state', state);
    formData.append('city', city);
    formData.append('zip_code', zipCode);
    const token = yield JSON.parse(localStorage.getItem('token'));
    const payeeResponse = yield call(
      request,
      `${API_URI}/payee/${payeeId}/`,
      optionsFormData('PUT', formData, token),
    );
    if (payeeResponse.name) {
      // delete payee user
      yield call(
        request,
        `${API_URI}/payee_user/?payee_id=${payeeId}`,
        optionsFormData('DELETE', null, token),
      );
      const userPayeeFormData = new FormData();
      /* eslint-disable */
      for (const i in addedUser) {
        const user = addedUser[i];
        userPayeeFormData.append('user_id', user.id);
        userPayeeFormData.append('payee_id', payeeResponse.id);
        const userPayeeResponse = yield call(
          request,
          `${API_URI}/user_payee/`,
          optionsFormData('POST', userPayeeFormData, token),
        );
      }
      /* eslint-enable */
      yield put(updatePayeeSuccess(payeeResponse));
      yield put(push('/payee'));
    } else {
      yield put(updatePayeeError(payeeResponse));
    }
  } catch (error) {
    yield put(updatePayeeError(error));
  }
}

export function* getStateCities() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const stateCities = yield call(
      request,
      `${API_URI}/state_cities/`,
      optionsFormData('GET', null, token),
    );
    if (stateCities.data) {
      yield put(getStateCitiesSuccess(stateCities.data));
    } else {
      yield put(getStateCitiesSuccess(stateCities.error));
    }
  } catch (error) {
    yield put(getStateCitiesError(error));
  }
}

export function* addPayeeWatcher() {
  yield takeLatest(ADD_PAYEE, addPayee);
}

export function* getUserWatcher() {
  yield takeLatest(GET_USER, getUser);
}

export function* getPayeeWatcher() {
  yield takeLatest(GET_PAYEE, getPayee);
}

export function* getUserPayeeWatcher() {
  yield takeLatest(GET_USER_PAYEE, getUserPayee);
}

export function* updatePayeeWatcher() {
  yield takeLatest(UPDATE_PAYEE, updatePayee);
}

export function* getStateCitiesWatcher() {
  yield takeLatest(GET_STATE_CITIES, getStateCities);
}

export function* goToPayeeWatcher() {
  yield takeLatest(GOTO_PAYEE, goToPayee);
}

export default function* rootSaga() {
  yield all([
    addPayeeWatcher(),
    getUserWatcher(),
    getPayeeWatcher(),
    getUserPayeeWatcher(),
    updatePayeeWatcher(),
    getStateCitiesWatcher(),
    goToPayeeWatcher(),
  ]);
}
