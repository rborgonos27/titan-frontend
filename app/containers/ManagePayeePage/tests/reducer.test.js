import { fromJS } from 'immutable';
import managePayeePageReducer from '../reducer';

describe('managePayeePageReducer', () => {
  it('returns the initial state', () => {
    expect(managePayeePageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
