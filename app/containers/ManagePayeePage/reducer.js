/*
 *
 * ManagePayeePage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  ADD_PAYEE,
  ADD_PAYEE_SUCCESS,
  ADD_PAYEE_ERROR,
  GET_USER,
  GET_USER_SUCCESS,
  GET_USER_ERROR,
  GET_PAYEE,
  GET_PAYEE_SUCCESS,
  GET_PAYEE_ERROR,
  GET_USER_PAYEE,
  GET_USER_PAYEE_SUCCESS,
  GET_USER_PAYEE_ERROR,
  ON_RESET,
  GET_STATE_CITIES,
  GET_STATE_CITIES_SUCCESS,
  GET_STATE_CITIES_ERROR,
} from './constants';

export const initialState = fromJS({
  loading: false,
  data: null,
  error: null,
  errorList: [],
  loadingUser: false,
  userData: [],
  userError: null,
  loadingPayeeUser: false,
  payeeUser: [],
  stateCities: null,
  userNotFound: false,
  newPayeeModal: false,
});

function managePayeePageReducer(state = initialState, action) {
  switch (action.type) {
    case ON_RESET:
      return state
        .set('loading', false)
        .set('userNotFound', false)
        .set('data', null)
        .set('newPayeeModal', false)
        .set('error', null)
        .set('errorList', [])
        .set('loadingUser', false)
        .set('userError', null)
        .set('loadingPayeeUser', false)
        .set('payeeUser', []);
    case ADD_PAYEE:
      return state
        .set('userNotFound', false)
        .set('newPayeeModal', false)
        .set('loading', true)
        .set('data', null)
        .set('error', null)
        .set('errorList', []);
    case ADD_PAYEE_SUCCESS:
      return state
        .set('userNotFound', false)
        .set('newPayeeModal', true)
        .set('loading', false)
        .set('data', action.data)
        .set('error', null)
        .set('errorList', []);
    case ADD_PAYEE_ERROR:
      return state
        .set('userNotFound', false)
        .set('newPayeeModal', false)
        .set('loading', false)
        .set('data', null)
        .set('error', action.error)
        .set('errorList', []);
    case GET_USER:
      return state
        .set('userNotFound', false)
        .set('newPayeeModal', false)
        .set('loadingUser', true)
        .set('userData', [])
        .set('userError', null);
    case GET_USER_SUCCESS:
      return state
        .set('loadingUser', false)
        .set('newPayeeModal', false)
        .set('userData', action.data)
        .set('userNotFound', action.data.results.length === 0)
        .set('userError', null);
    case GET_USER_ERROR:
      return state
        .set('loadingUser', false)
        .set('newPayeeModal', false)
        .set('userNotFound', true)
        .set('userData', [])
        .set('userError', action.error);
    case GET_PAYEE:
      return state
        .set('userNotFound', false)
        .set('newPayeeModal', false)
        .set('loading', true)
        .set('error', null)
        .set('data', null);
    case GET_PAYEE_SUCCESS:
      return state
        .set('userNotFound', false)
        .set('newPayeeModal', false)
        .set('loading', false)
        .set('error', null)
        .set('data', action.data);
    case GET_PAYEE_ERROR:
      return state
        .set('userNotFound', false)
        .set('newPayeeModal', false)
        .set('loading', false)
        .set('error', action.error)
        .set('data', null);
    case GET_USER_PAYEE:
      return state
        .set('userNotFound', false)
        .set('newPayeeModal', false)
        .set('loadingPayeeUser', true)
        .set('payeeUser', null)
        .set('data', null)
        .set('error', null);
    case GET_USER_PAYEE_SUCCESS:
      return state
        .set('userNotFound', false)
        .set('newPayeeModal', false)
        .set('loadingPayeeUser', false)
        .set('payeeUser', action.data)
        .set('data', null)
        .set('error', null);
    case GET_USER_PAYEE_ERROR:
      return state
        .set('userNotFound', false)
        .set('newPayeeModal', false)
        .set('loadingPayeeUser', false)
        .set('payeeUser', null)
        .set('data', null)
        .set('error', action.error);
    case GET_STATE_CITIES:
      return state
        .set('userNotFound', false)
        .set('newPayeeModal', false)
        .set('loading', true)
        .set('error', null)
        .set('stateCities', null);
    case GET_STATE_CITIES_ERROR:
      return state
        .set('userNotFound', false)
        .set('newPayeeModal', false)
        .set('loading', false)
        .set('error', action.error)
        .set('stateCities', null);
    case GET_STATE_CITIES_SUCCESS:
      return state
        .set('userNotFound', false)
        .set('newPayeeModal', false)
        .set('loading', false)
        .set('error', null)
        .set('stateCities', action.data);
    default:
      return state;
  }
}

export default managePayeePageReducer;
