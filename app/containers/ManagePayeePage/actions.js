
/*
 *
 * ManagePayeePage actions
 *
 */

import {
  ADD_PAYEE,
  ADD_PAYEE_SUCCESS,
  ADD_PAYEE_ERROR,
  GET_USER,
  GET_USER_SUCCESS,
  GET_USER_ERROR,
  GET_PAYEE,
  GET_PAYEE_SUCCESS,
  GET_PAYEE_ERROR,
  GET_USER_PAYEE,
  GET_USER_PAYEE_SUCCESS,
  GET_USER_PAYEE_ERROR,
  UPDATE_PAYEE,
  UPDATE_PAYEE_SUCCESS,
  UPDATE_PAYEE_ERROR,
  ON_RESET,
  GET_STATE_CITIES,
  GET_STATE_CITIES_SUCCESS,
  GET_STATE_CITIES_ERROR,
  GOTO_PAYEE,
} from './constants';

export function addPayee(data) {
  return {
    type: ADD_PAYEE,
    data,
  };
}

export function addPayeeSuccess(data) {
  return {
    type: ADD_PAYEE_SUCCESS,
    data,
  };
}

export function addPayeeError(error) {
  return {
    type: ADD_PAYEE_ERROR,
    error,
  };
}

export function getUser(data) {
  return {
    type: GET_USER,
    data,
  };
}

export function getUserSuccess(data) {
  return {
    type: GET_USER_SUCCESS,
    data,
  };
}

export function onReset() {
  return {
    type: ON_RESET,
  };
}

export function getUserError(error) {
  return {
    type: GET_USER_ERROR,
    error,
  };
}

export function getPayee(data) {
  return {
    type: GET_PAYEE,
    data,
  };
}

export function getPayeeSuccess(data) {
  return {
    type: GET_PAYEE_SUCCESS,
    data,
  };
}

export function getPayeeError(error) {
  return {
    type: GET_PAYEE_ERROR,
    error,
  };
}

export function getUserPayee(data) {
  return {
    type: GET_USER_PAYEE,
    data,
  };
}

export function getUserPayeeSuccess(data) {
  return {
    type: GET_USER_PAYEE_SUCCESS,
    data,
  };
}

export function getUserPayeeError(error) {
  return {
    type: GET_USER_PAYEE_ERROR,
    error,
  };
}

export function updatePayee(data) {
  return {
    type: UPDATE_PAYEE,
    data,
  };
}

export function updatePayeeSuccess(data) {
  return {
    type: UPDATE_PAYEE_SUCCESS,
    data,
  };
}

export function updatePayeeError(error) {
  return {
    type: UPDATE_PAYEE_ERROR,
    error,
  };
}

export function getStateCities() {
  return {
    type: GET_STATE_CITIES,
  };
}

export function getStateCitiesSuccess(data) {
  return {
    type: GET_STATE_CITIES_SUCCESS,
    data,
  };
}

export function getStateCitiesError(error) {
  return {
    type: GET_STATE_CITIES_ERROR,
    error,
  };
}

export function goToPayee() {
  return {
    type: GOTO_PAYEE,
  };
}
