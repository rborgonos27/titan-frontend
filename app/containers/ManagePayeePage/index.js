/**
 *
 * ManagePayeePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import StandardModal from 'components/StandardModal';
import {
  Divider,
  Grid,
  Form,
  Icon,
  Input,
  TextArea,
  Button,
  Dropdown,
} from 'semantic-ui-react';
import { Loading, RequiredText } from 'components/Utils';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectManagePayeePage, {
  makeSelectPayeeId,
  makeSelectCurrentUser,
} from './selectors';
import {
  addPayee,
  getUser,
  getPayee,
  getUserPayee,
  updatePayee,
  getStateCities,
  goToPayee,
} from './actions';
import reducer from './reducer';
import saga from './saga';
import UserList from './UserList';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export class ManagePayeePage extends React.Component {
  state = {
    openModal: false,
    openModalDelete: false,
    payeeId: '',
    recordAction: '',
    actionName: '',
    payeeName: '',
    bankName: '',
    bankAccount: '',
    bankAccountName: '',
    payeeEmail: '',
    address: '',
    address2: '',
    state: '',
    city: '',
    zipCode: '',
    bankRoutingNumber: '',
    searchInput: '',
    addedUser: [],
    availableUser: [],
    currentUser: null,
    notFoundModal: false,
    newPayeeModal: false,
  };
  componentWillMount() {
    const { payeeId, currentUser } = this.props;
    if (payeeId) {
      this.props.getPayee(payeeId);
      this.props.getUserPayee(payeeId);
    }
    this.props.getStateCities();
    if (currentUser) {
      // const addedUser = [];
      // addedUser.push(currentUser);
      this.setState({ currentUser });
    }
  }
  componentWillReceiveProps(nextProps) {
    const {
      loading,
      loadingUser,
      loadingPayeeUser,
      userData,
      data,
      payeeUser,
      userNotFound,
      newPayeeModal,
    } = nextProps.managepayeepage;
    if (!loadingUser && userData.results) {
      this.setState({ availableUser: userData.results });
    }
    if (!loading && data) {
      this.setState({
        payeeId: data.id,
        payeeName: data.name,
        bankName: data.bank_name,
        bankAccount: data.bank_account,
        bankAccountName: data.bank_account_name,
        address: data.address,
        bankRoutingNumber: data.bank_routing_number,
        payeeEmail: data.email,
        address2: data.address_2,
        state: this.sentenceCase(data.state),
        city: this.sentenceCase(data.city),
        zipCode: data.zip_code,
        actionName: 'EDIT',
      });
    }
    if (!loadingPayeeUser && payeeUser) {
      this.setState({ addedUser: payeeUser });
    }
    const { currentUser } = this.props;
    if (currentUser) {
      this.setState({ currentUser });
    }
    if (currentUser && currentUser.user_type !== 'super_admin') {
      const addedUser = [];
      const defaultUser = currentUser.parent_user
        ? currentUser.parent_user
        : currentUser;
      addedUser.push(defaultUser);
      this.setState({ addedUser, newPayeeModal });
    }
    if (userNotFound) {
      this.setState({ notFoundModal: true });
    }
  }
  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleDropDown = (e, { name, value }) => {
    this.setState({ [name]: value });
  };
  /* eslint-disable */
  sentenceCase = (str) => {
    if ((str===null) || (str===''))
      return false;
    else
      str = str.toString();

    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  };
  /* eslint-enable */
  getState = () => {
    const { stateCities } = this.props.managepayeepage;
    const states = [];
    if (!stateCities) return [];
    if (stateCities) {
      const stateKeys = Object.keys(stateCities);
      /* eslint-disable */
      for (let i=0; i< stateKeys.length; i++) {
        const state = {
          key: stateKeys[i],
          value: stateKeys[i],
          text: stateKeys[i],
        };
        states.push(state);
      }
      /* eslint-enable */
    }
    return states;
  };
  getCities = () => {
    const { state } = this.state;
    const { stateCities } = this.props.managepayeepage;
    if (!stateCities) return [];
    if (state === '') return [];
    const citiesFromState = stateCities[state];
    if (!citiesFromState) return [];
    const cities = [];
    /* eslint-disable */
    for (let i=0; i < citiesFromState.length; i++) {
      const cityState = this.sentenceCase(citiesFromState[i]);
      const city = {
        key: cityState,
        value: cityState,
        text: cityState,
      };
      cities.push(city);
    }
    /* eslint-enable */
    return cities;
  };
  closeModal = modalState => this.setState({ [modalState]: false });
  openModal = modalState => this.setState({ [modalState]: true });
  submitData = () => {
    const { actionName } = this.state;
    if (actionName === 'EDIT') {
      this.props.updatePayee(this.state);
    } else {
      this.props.addPayee(this.state);
    }
  };
  hideAddUser = () => {
    const {
      payeeName,
      bankName,
      bankAccount,
      bankAccountName,
      address,
      bankRoutingNumber,
      city,
      state,
      zipCode,
    } = this.state;
    return (
      payeeName === '' ||
      bankName === '' ||
      bankAccount === '' ||
      bankAccountName === '' ||
      address === '' ||
      bankRoutingNumber === '' ||
      state === '' ||
      zipCode === '' ||
      city === ''
    );
  };
  diableSubmitButton = () => {
    const { addedUser } = this.state;
    return this.hideAddUser() || addedUser.length === 0;
  };
  buildNewPayeeModal = () => {
    const { newPayeeModal } = this.state;
    return (
      <StandardModal
        headerText="New Payee Request"
        open={newPayeeModal}
        onClose={() => this.props.goToPayee()}
        onActionText="OK"
        onContinue={() => this.props.goToPayee()}
      >
        <div>
          Your request has been submitted. New payee will be added within 2
          business days. Contact memberservices@titan-vault.com for any questions in the meantime.
        </div>
        <div>Thank you</div>
      </StandardModal>
    );
  };
  buildNotFoundModal = () => {
    const { notFoundModal } = this.state;
    return (
      <StandardModal
        headerText="Warning"
        open={notFoundModal}
        onClose={() => this.closeModal('notFoundModal')}
        onActionText="CLOSE"
        onContinue={() => this.closeModal('notFoundModal')}
      >
        <p>User not found.</p>
      </StandardModal>
    );
  };
  buildConfirmModal = () => {
    const {
      openModal,
      payeeName,
      bankName,
      bankAccount,
      bankAccountName,
      address,
      bankRoutingNumber,
      payeeEmail,
    } = this.state;
    const { loading } = this.props.managepayeepage;
    return (
      <StandardModal
        headerText="Proceed this Request?"
        open={openModal}
        onClose={() => this.closeModal('openModal')}
        onActionText="CONFIRM"
        onContinue={() => this.submitData()}
        btnDisabled={loading}
      >
        <Grid columns={2} padded>
          <Grid.Row>
            <Grid.Column>
              <b>Payee Name</b>
            </Grid.Column>
            <Grid.Column>{payeeName}</Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <b>Bank Name</b>
            </Grid.Column>
            <Grid.Column>{bankName}</Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <b>Bank Account</b>
            </Grid.Column>
            <Grid.Column>{bankAccount}</Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <b>Bank Account Name</b>
            </Grid.Column>
            <Grid.Column>{bankAccountName}</Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <b>Email</b>
            </Grid.Column>
            <Grid.Column>{payeeEmail}</Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <b>Address</b>
            </Grid.Column>
            <Grid.Column>{address}</Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <b>Bank Routing Number</b>
            </Grid.Column>
            <Grid.Column>{bankRoutingNumber}</Grid.Column>
          </Grid.Row>
        </Grid>
      </StandardModal>
    );
  };
  getMemberByFilter = e => {
    const { searchInput } = this.state;
    if (e.keyCode === 13) {
      this.props.getUser(searchInput);
    }
  };
  addUserToPayee = (user, id) => {
    const { addedUser, availableUser } = this.state;
    const newUser = addedUser;
    newUser.push(user);
    const newAvailableUser = availableUser;
    newAvailableUser.splice(id, 1);
    this.setState({ addedUser: newUser, availableUser: newAvailableUser });
  };
  removeUserToPayee = (user, id) => {
    const { addedUser, availableUser } = this.state;
    const newAvailableUser = availableUser;
    newAvailableUser.push(user);
    const newUser = addedUser;
    newUser.splice(id, 1);
    this.setState({ addedUser: newUser, availableUser: newAvailableUser });
  };
  render() {
    const { loading, userData, loadingUser } = this.props.managepayeepage;
    const { availableUser } = this.state;
    const userListData = userData.results ? availableUser : [];
    const { currentUser } = this.props;
    const {
      payeeName,
      bankName,
      bankAccount,
      bankAccountName,
      address,
      address2,
      state,
      city,
      zipCode,
      bankRoutingNumber,
      searchInput,
      payeeEmail,
    } = this.state;
    return (
      <ContainerWrapper>
        <CardBox>
          <h3>
            <FormattedMessage {...messages.header} />
          </h3>
          <Divider />
          <Loading loading={loading} />
          {this.buildConfirmModal()}
          {this.buildNotFoundModal()}
          {this.buildNewPayeeModal()}
          <Grid>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.payeename} />
                  <RequiredText>*</RequiredText>
                </div>
                <div className="profileData">
                  <Form>
                    <Form.Field>
                      <Input
                        onChange={this.handleInput}
                        type="text"
                        name="payeeName"
                        id="payeeName"
                        size="large"
                        fluid
                        value={payeeName}
                        placeholder="Enter Payee Name..."
                      />
                    </Form.Field>
                  </Form>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.bankname} />
                  <RequiredText>*</RequiredText>
                </div>
                <div className="profileData">
                  <Form>
                    <Form.Field>
                      <Input
                        onChange={this.handleInput}
                        type="text"
                        name="bankName"
                        id="bankName"
                        size="large"
                        fluid
                        value={bankName}
                        placeholder="Enter Bank Name..."
                      />
                    </Form.Field>
                  </Form>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.bankaccount} />
                  <RequiredText>*</RequiredText>
                </div>
                <div className="profileData">
                  <Form>
                    <Form.Field>
                      <Input
                        onChange={this.handleInput}
                        type="text"
                        name="bankAccount"
                        id="bankAccount"
                        size="large"
                        fluid
                        value={bankAccount}
                        placeholder="Enter Bank Account..."
                      />
                    </Form.Field>
                  </Form>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.bankaccountname} />
                  <RequiredText>*</RequiredText>
                </div>
                <div className="profileData">
                  <Form>
                    <Form.Field>
                      <Input
                        onChange={this.handleInput}
                        type="text"
                        name="bankAccountName"
                        id="bankAccountName"
                        size="large"
                        fluid
                        value={bankAccountName}
                        placeholder="Enter Bank Account Name..."
                      />
                    </Form.Field>
                  </Form>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.payeeEmail} />
                  <RequiredText>*</RequiredText>
                </div>
                <div className="profileData">
                  <Form>
                    <Form.Field>
                      <Input
                        onChange={this.handleInput}
                        type="text"
                        name="payeeEmail"
                        id="payeeEmail"
                        size="large"
                        fluid
                        value={payeeEmail}
                        placeholder="Enter Payee Email..."
                      />
                    </Form.Field>
                  </Form>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.address} />
                  <RequiredText>*</RequiredText>
                </div>
                <div className="profileData">
                  <Form>
                    <Form.Field>
                      <TextArea
                        onChange={this.handleInput}
                        name="address"
                        id="address"
                        size="large"
                        row={3}
                        value={address}
                        placeholder="Enter Address..."
                      />
                    </Form.Field>
                  </Form>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.address2} />
                </div>
                <div className="profileData">
                  <Form>
                    <Form.Field>
                      <TextArea
                        onChange={this.handleInput}
                        name="address2"
                        id="address2"
                        size="large"
                        row={3}
                        value={address2}
                        placeholder="Enter Address 2..."
                      />
                    </Form.Field>
                  </Form>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.state} />
                  <RequiredText>*</RequiredText>
                </div>
                <div className="profileData">
                  <Form>
                    <Form.Field>
                      <Dropdown
                        options={this.getState()}
                        placeholder="Choose State"
                        className="userTypesClass"
                        search
                        name="state"
                        id="state"
                        selection
                        fluid
                        value={state}
                        onChange={this.handleDropDown}
                      />
                    </Form.Field>
                  </Form>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.city} />
                  <RequiredText>*</RequiredText>
                </div>
                <div className="profileData">
                  <Form>
                    <Form.Field>
                      <Dropdown
                        options={this.getCities()}
                        placeholder="Choose City"
                        className="userTypesClass"
                        search
                        name="city"
                        id="city"
                        selection
                        fluid
                        value={city}
                        onChange={this.handleDropDown}
                      />
                    </Form.Field>
                  </Form>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.zipCode} />
                  <RequiredText>*</RequiredText>
                </div>
                <div className="profileData">
                  <Form>
                    <Form.Field>
                      <Input
                        onChange={this.handleInput}
                        type="text"
                        name="zipCode"
                        id="zipCode"
                        size="large"
                        fluid
                        value={zipCode}
                        placeholder="Enter Bank Routing Number..."
                      />
                    </Form.Field>
                  </Form>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.bankroutingnumber} />
                  <RequiredText>*</RequiredText>
                </div>
                <div className="profileData">
                  <Form>
                    <Form.Field>
                      <Input
                        onChange={this.handleInput}
                        type="text"
                        name="bankRoutingNumber"
                        id="bankRoutingNumber"
                        size="large"
                        fluid
                        value={bankRoutingNumber}
                        placeholder="Enter Bank Routing Number..."
                      />
                    </Form.Field>
                  </Form>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Divider />
            {!this.hideAddUser() && currentUser.user_type === 'super_admin' ? (
              <Grid.Row>
                <Grid.Column>
                  <div className="profileData">
                    <h4>Assign Member</h4>
                  </div>
                </Grid.Column>
              </Grid.Row>
            ) : null}
            {!this.hideAddUser() && currentUser.user_type === 'super_admin' ? (
              <Grid.Row>
                <Grid.Column>
                  <div className="profileData">
                    <Form>
                      <Form.Field>
                        <Input>
                          <input
                            onChange={this.handleInput}
                            type="text"
                            name="searchInput"
                            id="searchInput"
                            size="large"
                            fluid
                            value={searchInput}
                            placeholder="Search Member..."
                            onKeyDown={this.getMemberByFilter}
                          />
                          <Button
                            onClick={() => this.props.getUser(searchInput)}
                          >
                            <Icon name="search" />
                          </Button>
                        </Input>
                      </Form.Field>
                    </Form>
                  </div>
                </Grid.Column>
              </Grid.Row>
            ) : null}
            <Grid.Row>
              <Grid.Column>
                {/*eslint-disable*/}
                {!this.hideAddUser() &&
                currentUser.user_type === 'super_admin' ?
                  (
                    <div className="listStyle">
                      <h4>Added User</h4>
                      <UserList
                        list={this.state.addedUser}
                        buttonAction={this.removeUserToPayee}
                        action="remove"
                      />
                      <Divider />
                      <h4>Available User</h4>
                      {!loadingUser ? (
                        <UserList
                          list={userListData}
                          buttonAction={this.addUserToPayee}
                          action="add"
                        />
                      ) : (
                        <Loading loading={loadingUser} />
                      )}
                    </div>
                  )
                : null}
                {/* eslint-enable */}
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileData">
                  <Button onClick={() => this.props.history.goBack()}>
                    Back
                  </Button>
                  <Button
                    onClick={() => this.openModal('openModal')}
                    color="orange"
                    disabled={this.diableSubmitButton()}
                  >
                    Submit
                  </Button>
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </CardBox>
      </ContainerWrapper>
    );
  }
}

ManagePayeePage.propTypes = {
  addPayee: PropTypes.func.isRequired,
  getUser: PropTypes.func.isRequired,
  managepayeepage: PropTypes.any,
  payeeId: PropTypes.any,
  history: PropTypes.any,
  getPayee: PropTypes.func.isRequired,
  getUserPayee: PropTypes.func.isRequired,
  updatePayee: PropTypes.func.isRequired,
  getStateCities: PropTypes.func.isRequired,
  goToPayee: PropTypes.func.isRequired,
  currentUser: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  managepayeepage: makeSelectManagePayeePage(),
  payeeId: makeSelectPayeeId(),
  currentUser: makeSelectCurrentUser(),
});

const mapDispatchToProps = {
  addPayee,
  getUser,
  getPayee,
  getUserPayee,
  updatePayee,
  getStateCities,
  goToPayee,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'managePayeePage', reducer });
const withSaga = injectSaga({ key: 'managePayeePage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ManagePayeePage);
