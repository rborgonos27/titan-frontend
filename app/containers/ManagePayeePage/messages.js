/*
 * ManagePayeePage Messages
 *
 * This contains all the text for the ManagePayeePage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ManagePayeePage.header',
    defaultMessage: 'Payee Management',
  },
  payeename: {
    id: 'app.containers.ManagePayeePage.payeename',
    defaultMessage: 'Payee Name',
  },
  bankname: {
    id: 'app.containers.ManagePayeePage.bankname',
    defaultMessage: 'Bank Name',
  },
  bankaccount: {
    id: 'app.containers.ManagePayeePage.bankaccount',
    defaultMessage: 'Bank Account',
  },
  bankaccountname: {
    id: 'app.containers.ManagePayeePage.bankaccountname',
    defaultMessage: 'Bank Account Name',
  },
  address: {
    id: 'app.containers.ManagePayeePage.address',
    defaultMessage: 'Address',
  },
  address2: {
    id: 'app.containers.ManagePayeePage.address2',
    defaultMessage: 'Address 2',
  },
  state: {
    id: 'app.containers.ManagePayeePage.state',
    defaultMessage: 'State',
  },
  city: {
    id: 'app.containers.ManagePayeePage.city',
    defaultMessage: 'City',
  },
  zipCode: {
    id: 'app.containers.ManagePayeePage.zipCode',
    defaultMessage: 'Zip Code',
  },
  bankroutingnumber: {
    id: 'app.containers.ManagePayeePage.bankroutingnumber',
    defaultMessage: 'Bank Routing Number',
  },
  payeeEmail: {
    id: 'app.containers.ManagePayeePage.payeeEmail',
    defaultMessage: 'Payee Email',
  },
});
