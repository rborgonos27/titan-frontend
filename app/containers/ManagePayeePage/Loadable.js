/**
 *
 * Asynchronously loads the component for ManagePayeePage
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
