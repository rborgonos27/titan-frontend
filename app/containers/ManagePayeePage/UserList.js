import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { Button, List } from 'semantic-ui-react';
import Gravatar from 'react-gravatar';

const UserList = ({ list, buttonAction, action }) => {
  /* eslint-disable */
  let listRow = [];
  for (let i = 0; i<list.length; i++) {
    const user = list[i];
    listRow.push(
      <List.Item id={i} key={i}>
        <List.Content floated="right">
          <Button color={action === 'add' ? 'green' : 'red'} onClick={() => buttonAction(user, i)}>
            { action === 'add' ? 'Add' : 'Remove' }
          </Button>
        </List.Content>
        <List.Content floated="left">
          <Gravatar
            email={user.email}
            size={25}
            rating="pg"
            default="identicon"
            className="img-circle"
          />
        </List.Content>
        <List.Content>
          <List.Header>{`User Name: ${user.first_name} ${user.last_name}`}</List.Header>
          <List.Header>{`Member Name: ${user.business_name}`}</List.Header>
          {`@${user.username} | ${user.member_account_id}`}
        </List.Content>
      </List.Item>
    )
  }
  /* eslint-enable */
  return (
    <List divided verticalAlign="middle">
      {listRow}
    </List>
  );
};

UserList.propTypes = {
  list: PropTypes.any.isRequired,
  buttonAction: PropTypes.func.isRequired,
  action: PropTypes.any.isRequired,
};

export default UserList;
