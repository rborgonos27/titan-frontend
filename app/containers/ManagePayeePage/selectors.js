import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the managePayeePage state domain
 */

const selectGlobal = state => state.get('global');

const selectManagePayeePageDomain = state =>
  state.get('managePayeePage', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by ManagePayeePage
 */

const makeSelectCurrentUser = () =>
  createSelector(selectGlobal, globalState => globalState.get('userData'));

const makeSelectManagePayeePage = () =>
  createSelector(selectManagePayeePageDomain, substate => substate.toJS());

const makeSelectPayeeId = () =>
  createSelector(selectGlobal, globalState => globalState.get('payeeId'));

export default makeSelectManagePayeePage;
export { selectManagePayeePageDomain, makeSelectPayeeId, makeSelectCurrentUser };
