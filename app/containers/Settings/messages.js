/*
 * Settings Messages
 *
 * This contains all the text for the Settings component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Settings.header',
    defaultMessage: 'Settings',
  },
  username: {
    id: 'app.containers.Settings.username',
    defaultMessage: 'Change Username',
  },
  password: {
    id: 'app.containers.Settings.password',
    defaultMessage: 'Change Password',
  },
  twoStepVerification: {
    id: 'app.containers.Settings.twoStepVerification',
    defaultMessage: 'Set up 2-step Verification',
  },
  notification: {
    id: 'app.containers.Settings.notification',
    defaultMessage: 'Manage Notification',
  },
  tools: {
    id: 'app.containers.Settings.tools',
    defaultMessage: 'Admin Tools',
  },
});
