/**
 *
 * Settings
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import { Grid, Divider } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectSettings, { makeSelectCurrentUser } from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export class Settings extends React.Component {
  render() {
    const { currentUser } = this.props;
    return (
      <ContainerWrapper>
        <CardBox>
          <h3>
            <FormattedMessage {...messages.header} />
          </h3>
          <Divider />
          <Grid>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.username} />
                </div>
                <div className="profileData">
                  <NavLink to="/change/username">
                    Click here to Change Username
                  </NavLink>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.password} />
                </div>
                <div className="profileData">
                  <NavLink to="/profile">Click here to Change Password</NavLink>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.twoStepVerification} />
                </div>
                <div className="profileData">
                  Click Here to Set up 2-step Verification
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.notification} />
                </div>
                <div className="profileData">
                  Click Here to Manage Notification
                </div>
              </Grid.Column>
            </Grid.Row>
            {currentUser.user_type === 'super_admin' ? (
              <Grid.Row>
                <Grid.Column>
                  <div className="profileLabel">
                    <FormattedMessage {...messages.tools} />
                  </div>
                  <div className="profileData">
                    <NavLink to="/tools">Click Here to Use Admin Tools</NavLink>
                  </div>
                </Grid.Column>
              </Grid.Row>
            ) : null}
          </Grid>
        </CardBox>
      </ContainerWrapper>
    );
  }
}

Settings.propTypes = {
  dispatch: PropTypes.func.isRequired,
  currentUser: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  settings: makeSelectSettings(),
  currentUser: makeSelectCurrentUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'settings', reducer });
const withSaga = injectSaga({ key: 'settings', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Settings);
