/**
 *
 * TempUserLandingPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import CardBox from 'components/CardBox';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Segment, Button, Divider } from 'semantic-ui-react';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectTempUserLandingPage, {
  makeSelectCurrentUser,
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import { logout } from './actions';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export class TempUserLandingPage extends React.Component {
  render() {
    const { currentUser } = this.props;
    const paragraphStyle = {
      textAlign: 'center',
      fontSize: 20,
      color: '#AAAAAA',
      paddingBottom: 15,
    };
    const paragraphEmailStyle = {
      textAlign: 'center',
      fontSize: 15,
      color: '#AA0000',
      paddingTop: 20,
      paddingBottom: 15,
    };
    return (
      <Segment className="segmentStyle" color="orange">
        <h3 style={{ textAlign: 'center' }}>
          <FormattedMessage {...messages.header} />
          {` ${currentUser.first_name}`}
        </h3>
        <Divider />
        <CardBox>
          <div style={paragraphStyle}>
            We are currently processing the activation of your account. Our
            titan vault officer will contact you for the KYC.
          </div>
          <div style={paragraphStyle}>
            You will receive an email when your titan vault account is
            activated.
          </div>
          <div style={paragraphEmailStyle}>
            For inquiries, you may email to Titan Vault support at
            support@titan-vault.com.
          </div>
          <div style={{ textAlign: 'center' }}>
            <Button
              className="buttonSubmit"
              fluid
              onClick={() => this.props.logout()}
              color="orange"
            >
              Log Out
            </Button>
          </div>
        </CardBox>
      </Segment>
    );
  }
}

TempUserLandingPage.propTypes = {
  logout: PropTypes.func.isRequired,
  currentUser: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  tempuserlandingpage: makeSelectTempUserLandingPage(),
  currentUser: makeSelectCurrentUser(),
});

const mapDispatchToProps = { logout };

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'tempUserLandingPage', reducer });
const withSaga = injectSaga({ key: 'tempUserLandingPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(TempUserLandingPage);
