import { fromJS } from 'immutable';
import tempUserLandingPageReducer from '../reducer';

describe('tempUserLandingPageReducer', () => {
  it('returns the initial state', () => {
    expect(tempUserLandingPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
