import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectGlobal = state => state.get('global');
/**
 * Direct selector to the tempUserLandingPage state domain
 */

const selectTempUserLandingPageDomain = state =>
  state.get('tempUserLandingPage', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by TempUserLandingPage
 */

const makeSelectTempUserLandingPage = () =>
  createSelector(selectTempUserLandingPageDomain, substate => substate.toJS());

const makeSelectCurrentUser = () =>
  createSelector(selectGlobal, globalState => globalState.get('userData'));

export default makeSelectTempUserLandingPage;
export { selectTempUserLandingPageDomain, makeSelectCurrentUser };
