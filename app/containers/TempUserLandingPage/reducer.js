/*
 *
 * TempUserLandingPage reducer
 *
 */

import { fromJS } from 'immutable';
import { LOG_OUT } from './constants';

export const initialState = fromJS({});

function tempUserLandingPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOG_OUT:
      return state;
    default:
      return state;
  }
}

export default tempUserLandingPageReducer;
