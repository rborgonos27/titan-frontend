import { put, takeLatest, all } from 'redux-saga/effects';
import { logoutAction } from 'containers/App/actions';
import { LOG_OUT } from './constants';

export function* logout() {
  yield put(logoutAction());
}

export function* logoutWatcher() {
  yield takeLatest(LOG_OUT, logout);
}

export default function* rootSaga() {
  yield all([logoutWatcher()]);
}
