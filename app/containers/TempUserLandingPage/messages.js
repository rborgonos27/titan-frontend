/*
 * TempUserLandingPage Messages
 *
 * This contains all the text for the TempUserLandingPage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.TempUserLandingPage.header',
    defaultMessage: 'Welcome to Titan Vault',
  },
});
