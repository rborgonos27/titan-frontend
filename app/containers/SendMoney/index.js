/**
 *
 * SendMoney
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { NavLink } from 'react-router-dom';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import ContainerWrapper from 'components/ContainerWrapper';

import makeSelectSendMoney from './selectors';
import reducer from './reducer';
import saga from './saga';

/* eslint-disable react/prefer-stateless-function */
export class SendMoney extends React.Component {
  render() {
    return (
      <ContainerWrapper>
        <div className="row">
          <div className="col-lg-2" />
          <NavLink
            to="/"
            className="deposit-wrap col-lg-4 col-md-6 col-sm-6 col-xs-12"
          >
            <div className="card-box make-deposit text-center">
              <img alt="" src="themes/assets/images/deposit.png" />
              <h4>Make Deposit</h4>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry
              </p>
              <button
                className="btn btn-deposit waves-effect waves-light"
                type="submit"
                to="/"
              >
                <strong>Next</strong>
              </button>
            </div>
          </NavLink>
          <NavLink
            to="/payment"
            className="payment-wrap col-lg-4 col-md-6 col-sm-6 col-xs-12"
          >
            <div className="card-box send-payment text-center">
              <img alt="" src="themes/assets/images/payment.png" />
              <h4>Send Payment</h4>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry
              </p>
              <button
                className="btn btn-payment waves-effect waves-light"
                type="submit"
              >
                <strong>Next</strong>
              </button>
            </div>
          </NavLink>
          <div className="col-lg-2" />
        </div>
        <div cassName="row">
          <div className="col-lg-2">

          </div>
        </div>
      </ContainerWrapper>
    );
  }
}

SendMoney.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  sendmoney: makeSelectSendMoney(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'sendMoney', reducer });
const withSaga = injectSaga({ key: 'sendMoney', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(SendMoney);
