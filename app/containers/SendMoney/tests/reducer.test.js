import { fromJS } from 'immutable';
import sendMoneyReducer from '../reducer';

describe('sendMoneyReducer', () => {
  it('returns the initial state', () => {
    expect(sendMoneyReducer(undefined, {})).toEqual(fromJS({}));
  });
});
