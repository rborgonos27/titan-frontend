/*
 * SendMoney Messages
 *
 * This contains all the text for the SendMoney component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.SendMoney.header',
    defaultMessage: 'This is SendMoney container !',
  },
});
