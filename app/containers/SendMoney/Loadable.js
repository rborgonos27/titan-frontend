/**
 *
 * Asynchronously loads the component for SendMoney
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
