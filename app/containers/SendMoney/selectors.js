import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the sendMoney state domain
 */

const selectSendMoneyDomain = state => state.get('sendMoney', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by SendMoney
 */

const makeSelectSendMoney = () =>
  createSelector(selectSendMoneyDomain, substate => substate.toJS());

export default makeSelectSendMoney;
export { selectSendMoneyDomain };
