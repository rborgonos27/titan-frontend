import { put, takeLatest, all } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { logoutSuccessAction } from 'containers/App/actions';
import { RESET_PASSWORD_REDIRECT } from './constants';

export function* resetPasswordRedirect(resetPassword) {
  yield put(logoutSuccessAction());
  localStorage.removeItem('token');
  localStorage.removeItem('token_created');
  localStorage.removeItem('userData');
  sessionStorage.clear();
  yield put(push(`/reset_password/${resetPassword.data}`));
}
export function* resetPasswordRedirectWatcher() {
  yield takeLatest(RESET_PASSWORD_REDIRECT, resetPasswordRedirect);
}
export default function* rootSaga() {
  yield all([resetPasswordRedirectWatcher()]);
}
