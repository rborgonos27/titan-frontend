/*
 * ResetPasswordRedirect Messages
 *
 * This contains all the text for the ResetPasswordRedirect component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ResetPasswordRedirect.header',
    defaultMessage: 'This is ResetPasswordRedirect container !',
  },
});
