/**
 *
 * ResetPasswordRedirect
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectResetPasswordRedirect from './selectors';
import { resetPasswordRedirect } from './actions';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export class ResetPasswordRedirect extends React.Component {
  componentDidMount() {
    const { match } = this.props;
    const { id } = match.params;
    this.props.resetPasswordRedirect(id);
  }
  render() {
    return (
      <div>
        <FormattedMessage {...messages.header} />
      </div>
    );
  }
}

ResetPasswordRedirect.propTypes = {
  resetPasswordRedirect: PropTypes.func.isRequired,
  match: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  resetpasswordredirect: makeSelectResetPasswordRedirect(),
});

const mapDispatchToProps = { resetPasswordRedirect };

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'resetPasswordRedirect', reducer });
const withSaga = injectSaga({ key: 'resetPasswordRedirect', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ResetPasswordRedirect);
