/*
 *
 * ResetPasswordRedirect actions
 *
 */

import { DEFAULT_ACTION, RESET_PASSWORD_REDIRECT } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function resetPasswordRedirect(data) {
  return {
    type: RESET_PASSWORD_REDIRECT,
    data,
  };
}
