import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the resetPasswordRedirect state domain
 */

const selectResetPasswordRedirectDomain = state =>
  state.get('resetPasswordRedirect', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by ResetPasswordRedirect
 */

const makeSelectResetPasswordRedirect = () =>
  createSelector(selectResetPasswordRedirectDomain, substate =>
    substate.toJS(),
  );

export default makeSelectResetPasswordRedirect;
export { selectResetPasswordRedirectDomain };
