import { fromJS } from 'immutable';
import resetPasswordRedirectReducer from '../reducer';

describe('resetPasswordRedirectReducer', () => {
  it('returns the initial state', () => {
    expect(resetPasswordRedirectReducer(undefined, {})).toEqual(fromJS({}));
  });
});
