/*
 *
 * ResetPasswordRedirect constants
 *
 */

export const DEFAULT_ACTION = 'app/ResetPasswordRedirect/DEFAULT_ACTION';
export const RESET_PASSWORD_REDIRECT =
  'app/ResetPasswordRedirect/RESET_PASSWORD_REDIRECT';
