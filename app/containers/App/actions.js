import {
  LOGIN,
  LOGIN_ERROR,
  LOGIN_SUCCESS,
  REFRESH,
  REFRESH_ERROR,
  REFRESH_SUCCESS,
  LOGOUT,
  LOGOUT_ERROR,
  LOGOUT_SUCCESS,
  CLOSE_SESSION,
  EXPIRE_SESSION,
  SET_PAYEE_ID,
  GET_CURRENT_USER,
  GET_CURRENT_USER_SUCCESS,
  GET_CURRENT_USER_ERROR,
  GET_USER_FEE_RATE_SUCCESS,
  GET_USER_FEE_TIER_RATE_SUCCESS,
  SEND_MESSAGE,
  SEND_MESSAGE_ERROR,
  SEND_MESSAGE_SUCCESS,
  RESET_MESSAGE,
} from './constants';

export function loginAction(data) {
  return {
    type: LOGIN,
    data,
  };
}

export function loginSuccessAction(data) {
  return {
    type: LOGIN_SUCCESS,
    data,
  };
}

export function loginErrorAction(data) {
  return {
    type: LOGIN_ERROR,
    data,
  };
}

export function refreshToken() {
  return {
    type: REFRESH,
  };
}

export function refreshTokenSuccess(data) {
  return {
    type: REFRESH_SUCCESS,
    data,
  };
}

export function refreshTokenError(data) {
  return {
    type: REFRESH_ERROR,
    data,
  };
}

export function logoutAction() {
  return {
    type: LOGOUT,
  };
}

export function logoutSuccessAction(data) {
  return {
    type: LOGOUT_SUCCESS,
    data,
  };
}

export function logoutErrorAction(data) {
  return {
    type: LOGOUT_ERROR,
    data,
  };
}

export function closeSession() {
  return {
    type: CLOSE_SESSION,
  };
}

export function expireSession() {
  return {
    type: EXPIRE_SESSION,
  };
}

export function setPayeeId(data) {
  return {
    type: SET_PAYEE_ID,
    data,
  };
}

export function getCurrentUser() {
  return {
    type: GET_CURRENT_USER,
  };
}

export function getCurrentUserSuccess(data) {
  return {
    type: GET_CURRENT_USER_SUCCESS,
    data,
  };
}

export function getCurrentUserError(error) {
  return {
    type: GET_CURRENT_USER_ERROR,
    error,
  };
}

export function getUserFeeRateSuccess(data) {
  return {
    type: GET_USER_FEE_RATE_SUCCESS,
    data,
  };
}

export function getUserFeeTierRateSuccess(data) {
  return {
    type: GET_USER_FEE_TIER_RATE_SUCCESS,
    data,
  };
}

export function sendMessage(data) {
  return {
    type: SEND_MESSAGE,
    data,
  };
}

export function sendMessageError(error) {
  return {
    type: SEND_MESSAGE_ERROR,
    error,
  };
}

export function sendMessageSuccess(data) {
  return {
    type: SEND_MESSAGE_SUCCESS,
    data,
  };
}

export function resetMessage() {
  return {
    type: RESET_MESSAGE,
  };
}
