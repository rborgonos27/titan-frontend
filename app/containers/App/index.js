/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { userIsAuthenticated, userIsNotAuthenticated } from 'utils/authService';
import { APP } from 'config';

import Sidebar from 'components/Sidebar/Loadable';
import Topbar from 'components/Topbar';
import LoadScripts from 'components/LoadScripts';

import HomePage from 'containers/HomePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import LoginPage from 'containers/LoginPage/Loadable';
import SendMoney from 'containers/SendMoney/Loadable';
import SendPaymentPage from 'containers/SendPayment/Loadable';
import SessionModal from 'components/SessionModal';
import moment from 'moment';

import DepositPage from 'containers/DepositPage/Loadable';
import DashBoardPage from 'containers/DashBoard/Loadable';
import RequestViewPage from 'containers/RequestViewPage/Loadable';
import PayeeViewPage from 'containers/PayeeViewPage/Loadable';
import PayeePage from 'containers/PayeePage/Loadable';
import UsersPage from 'containers/UsersPage/Loadable';
import ProfilePage from 'containers/ProfilePage/Loadable';
import MemberServicesPage from 'containers/MemberServicesPage/Loadable';
import AccountProfilePage from 'containers/AccountProfilePage/Loadable';
import Settings from 'containers/Settings/Loadable';
import ChangeUserNamePage from 'containers/ChangeUserNamePage/Loadable';
import ManagePayeePage from 'containers/ManagePayeePage/Loadable';
import LoginRedirectPage from 'containers/LoginRedirect/Loadable';
import ResetPasswordRedirectPage from 'containers/ResetPasswordRedirect/Loadable';
import TempUserPage from 'containers/TempUserLandingPage/Loadable';
import ResetPasswordPage from 'containers/ResetPasswordPage/Loadable';
import PrivacyPage from 'containers/PrivacyPage/Loadable';
import AccountPage from 'containers/AccountPage/Loadable';
import PayeeTermsPage from 'containers/PayeeTerms/Loadable';
import MemberUserPage from 'containers/MemberUser/Loadable';
import ToolsPage from 'containers/Tools/Loadable';

import saga from './saga';
import reducer from './reducer';
import {
  logoutAction,
  expireSession,
  closeSession,
  refreshToken,
  sendMessage,
  resetMessage,
} from './actions';
import {
  makeSelectIsAuthenticated,
  makeSelectCurrentUser,
  makeSelectOpenSessionModal,
  makeSelectLoading,
  makeSelectMessage,
} from './selectors';

// const RedirectLogin = userIsNotAuthenticated(LoginRedirectPage);
const Login = userIsNotAuthenticated(LoginPage);
const ResetPassword = userIsNotAuthenticated(ResetPasswordPage);
const Home = userIsAuthenticated(HomePage);
const SendPayment = userIsAuthenticated(SendPaymentPage);
const Deposit = userIsAuthenticated(DepositPage);
const DashBoard = userIsAuthenticated(DashBoardPage);
const RequestView = userIsAuthenticated(RequestViewPage);
const PayeeView = userIsAuthenticated(PayeeViewPage);
const Payee = userIsAuthenticated(PayeePage);
const UsersView = userIsAuthenticated(UsersPage);
const ProfileView = userIsAuthenticated(ProfilePage);
const MembersServicesView = userIsAuthenticated(MemberServicesPage);
const AccountProfile = userIsAuthenticated(AccountProfilePage);
const SettingsPage = userIsAuthenticated(Settings);
const ChangeUserName = userIsAuthenticated(ChangeUserNamePage);
const ManagePayee = userIsAuthenticated(ManagePayeePage);
const TempUserLandingPage = userIsAuthenticated(TempUserPage);
const Privacy = userIsAuthenticated(PrivacyPage);
const Account = userIsAuthenticated(AccountPage);
const MemberUser = userIsAuthenticated(MemberUserPage);
const Tools = userIsAuthenticated(ToolsPage);
const PayeeTerms = PayeeTermsPage;

const App = props => {
  const { isAuthenticated, currentUser, openSessionModal } = props;
  const isFirstTimeMember =
    currentUser.is_first_time_member &&
    currentUser.is_first_time_member === true;
  const isTemporaryUser =
    currentUser.user_type && currentUser.user_type === 'temporary';
  let timeOut = null;
  window.addEventListener('resize', () => {
    const { clientWidth } = document.body;
    const wrapper = document.querySelector('#wrapper'); // eslint-disable-line no-alert
    if (clientWidth < 1154) {
      wrapper.className = 'enlarged forced';
    } else {
      wrapper.className = '';
    }
  });
  const header = (
    <div>
      <Topbar
        user={currentUser}
        logout={props.logoutAction}
        sendMessage={props.sendMessage}
        loading={props.loading}
        message={props.message}
        resetMessage={props.resetMessage}
      />
      <Sidebar user={currentUser} />
    </div>
  );
  if (isAuthenticated) {
    const sessionWillExpireAt = moment()
      .add(APP.MINUTES_TIMEOUT, 'minutes')
      .format(APP.DATE_TIME_FORMAT);
    sessionStorage.setItem('sessionWillExpireAt', sessionWillExpireAt);
    setInterval(() => {
      const savedTime = moment(sessionStorage.getItem('sessionWillExpireAt'));
      const thisTime = moment();
      if (thisTime > savedTime) {
        props.expireSession();
      }
    }, 1000);

    if (openSessionModal) {
      timeOut = setTimeout(() => props.logoutAction(), 20000);
    }
  }
  const payeeTerms = window.location.pathname;
  const isPayeeTerms = payeeTerms.indexOf('payee-terms-agreement') > 0;
  const shouldNotShowUpdateCredentials =
    isPayeeTerms || isTemporaryUser || isFirstTimeMember;
  const footer = <div>{isAuthenticated ? <LoadScripts /> : null}</div>;
  return (
    <div id="wrapper">
      {isAuthenticated && !shouldNotShowUpdateCredentials ? header : null}
      {isAuthenticated && !shouldNotShowUpdateCredentials ? (
        <SessionModal
          open={openSessionModal}
          onContinue={() => {
            clearTimeout(timeOut);
            props.refreshToken();
          }}
          onClose={() => props.logoutAction()}
        />
      ) : null}
      <Switch>
        <Route exact path="/" component={DashBoard} />
        <Route exact path="/home" component={Home} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/money" component={SendMoney} />
        <Route exact path="/deposit" component={Deposit} />
        <Route exact path="/payment" component={SendPayment} />
        {/* <Route exact path="/statement" component={SendPayment} /> */}
        <Route exact path="/payee" component={Payee} />
        <Route exact path="/payee/:id" component={PayeeView} />
        <Route exact path="/manage/payee" component={ManagePayee} />
        {/* <Route exact path="/pickup" component={SendPayment} /> */}
        <Route exact path="/login" component={Login} />
        <Route exact path="/request/:id" component={RequestView} />
        <Route exact path="/reset_password/:id" component={ResetPassword} />
        <Route exact path="/user" component={UsersView} />
        <Route exact path="/profile" component={ProfileView} />
        <Route exact path="/services" component={MembersServicesView} />
        <Route exact path="/privacy" component={Privacy} />
        <Route exact path="/account" component={AccountProfile} />
        <Route exact path="/settings" component={SettingsPage} />
        <Route exact path="/redirect/login/:id" component={LoginRedirectPage} />
        <Route exact path="/tools" component={ToolsPage} />
        <Route
          exact
          path="/resetpassword/redirect/:id"
          component={ResetPasswordRedirectPage}
        />
        <Route exact path="/change/username" component={ChangeUserName} />
        <Route exact path="/temp/user" component={TempUserLandingPage} />
        <Route exact path="/statement" component={Account} />
        <Route exact path="/payee-terms-agreement/:id" component={PayeeTerms} />
        <Route exact path="/member_user" component={MemberUser} />
        <Route component={NotFoundPage} />
      </Switch>
      {isAuthenticated && !shouldNotShowUpdateCredentials ? footer : null}
    </div>
  );
};

App.propTypes = {
  isAuthenticated: PropTypes.bool,
  currentUser: PropTypes.object,
  logoutAction: PropTypes.func,
  refreshToken: PropTypes.func,
  expireSession: PropTypes.func,
  sendMessage: PropTypes.func,
  resetMessage: PropTypes.func,
  openSessionModal: PropTypes.bool,
  loading: PropTypes.bool,
  message: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  isAuthenticated: makeSelectIsAuthenticated(),
  currentUser: makeSelectCurrentUser(),
  openSessionModal: makeSelectOpenSessionModal(),
  loading: makeSelectLoading(),
  message: makeSelectMessage(),
});

const mapDispatchToProps = {
  logoutAction,
  expireSession,
  closeSession,
  refreshToken,
  sendMessage,
  resetMessage,
};

const withSaga = injectSaga({ key: 'app', saga });

const withReducer = injectReducer({ key: 'app', reducer });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default withRouter(
  compose(
    withConnect,
    withSaga,
    withReducer,
  )(App),
);
