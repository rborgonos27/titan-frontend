import { call, takeLatest, put, all } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import moment from 'moment';
import { APP } from 'config';
import request, { getReqErrMsg } from 'utils/request';
import { optionsFormData } from 'utils/options';
import {
  LOGOUT,
  REFRESH,
  GET_CURRENT_USER,
  SEND_MESSAGE,
} from 'containers/App/constants';
import {
  logoutSuccessAction,
  refreshTokenSuccess,
  refreshTokenError,
  getCurrentUser,
  getCurrentUserError,
  getCurrentUserSuccess,
  sendMessageError,
  sendMessageSuccess,
} from 'containers/App/actions';

export function* logoutUser() {
  yield put(logoutSuccessAction());
  localStorage.removeItem('token');
  localStorage.removeItem('token_created');
  localStorage.removeItem('userData');
  sessionStorage.clear();
  yield put(push('/login'));
}

export function* refreshToken() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const formData = new FormData();
    formData.append('token', token);
    const loginResponse = yield call(
      request,
      `${API_URI}/refresh`,
      optionsFormData('POST', formData, token),
    );
    if (loginResponse.success) {
      sessionStorage.clear();
      localStorage.clear();
      const loginData = loginResponse.data;
      const tokenData = JSON.stringify(loginData.token);
      let userData = JSON.stringify(loginData.user);
      localStorage.setItem('token', tokenData);
      localStorage.setItem(
        'token_created',
        moment().format('YYYY-MM-DD hh:mm:ss a'),
      );
      localStorage.setItem('userData', userData);
      userData = JSON.parse(userData);
      yield put(refreshTokenSuccess({ token, userData }));
      yield put(getCurrentUser());
    } else {
      yield put(refreshTokenError(getReqErrMsg(loginResponse)));
    }
  } catch (error) {
    yield put(refreshTokenError(getReqErrMsg(error)));
  }
}

export function* runGetCurrentUser() {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userRequest = yield call(
      request,
      `${API_URI}/current_user/`,
      optionsFormData('GET', null, token),
    );
    if (userRequest.id) {
      yield put(getCurrentUserSuccess(userRequest));
    } else {
      yield put(getCurrentUserError(userRequest));
    }
  } catch (error) {
    yield put(getCurrentUserError(error));
  }
}

export function* sendMessage(messageData) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const { data } = messageData;
    const { message, user } = data;
    console.log(messageData);
    const formData = new FormData();
    formData.append('email', user.email);
    formData.append('user_id', user.id);
    formData.append('member_name', `${user.first_name} ${user.last_name}`);
    formData.append('message', message);
    const sendMessageResponse = yield call(
      request,
      `${API_URI}/send_message/`,
      optionsFormData('POST', formData, token),
    );
    if (sendMessageResponse.success) {
      yield put(sendMessageSuccess(sendMessageResponse.message));
    }
  } catch (error) {
    console.log(error);
    yield put(sendMessageError(error));
  }
}

export function* sendMessageWatcher() {
  yield takeLatest(SEND_MESSAGE, sendMessage);
}

export function* logoutWatcher() {
  yield takeLatest(LOGOUT, logoutUser);
}

export function* refreshWatcher() {
  yield takeLatest(REFRESH, refreshToken);
}

export function* getCurrentUserWatcher() {
  yield takeLatest(GET_CURRENT_USER, runGetCurrentUser);
}

export default function* rootSaga() {
  yield all([
    logoutWatcher(),
    refreshWatcher(),
    getCurrentUserWatcher(),
    sendMessageWatcher(),
  ]);
}
