/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const LOGIN = 'app/App/LOGIN';
export const LOGIN_ERROR = 'app/App/LOGIN_ERROR';
export const LOGIN_SUCCESS = 'app/App/LOGIN_SUCCESS';

export const REFRESH = 'app/App/REFRESH';
export const REFRESH_ERROR = 'app/App/REFRESH_ERROR';
export const REFRESH_SUCCESS = 'app/App/REFRESH_SUCCESS';

export const LOGOUT = 'app/App/LOGOUT';
export const LOGOUT_ERROR = 'app/App/LOGOUT_ERROR';
export const LOGOUT_SUCCESS = 'app/App/LOGOUT_SUCCESS';

export const EXPIRE_SESSION = 'app/App/EXPIRE_SESSION';
export const CLOSE_SESSION = 'app/App/CLOSE_SESSION';

export const SET_PAYEE_ID = 'app/App/SET_PAYEE_ID';
export const DEFAULT_LOCALE = 'en';

export const GET_CURRENT_USER = 'app/App/GET_CURRENT_USER';
export const GET_CURRENT_USER_SUCCESS = 'app/App/GET_CURRENT_USER_SUCCESS';
export const GET_CURRENT_USER_ERROR = 'app/App/GET_CURRENT_USER_ERROR';

export const GET_USER_FEE_RATE_SUCCESS = 'app/App/GET_USER_FEE_RATE_SUCCESS';
export const GET_USER_FEE_TIER_RATE_SUCCESS =
  'app/App/GET_USER_FEE_TIER_RATE_SUCCESS';

export const SEND_MESSAGE = 'app/App/SEND_MESSAGE';
export const SEND_MESSAGE_SUCCESS = 'app/App/SEND_MESSAGE_SUCCESS';
export const SEND_MESSAGE_ERROR = 'app/App/SEND_MESSAGE_ERROR';

export const RESET_MESSAGE = 'app/App/RESET_MESSAGE';
