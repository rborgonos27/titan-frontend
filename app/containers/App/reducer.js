/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import {
  LOGIN,
  LOGIN_ERROR,
  LOGIN_SUCCESS,
  REFRESH,
  REFRESH_ERROR,
  REFRESH_SUCCESS,
  LOGOUT,
  LOGOUT_ERROR,
  LOGOUT_SUCCESS,
  CLOSE_SESSION,
  EXPIRE_SESSION,
  SET_PAYEE_ID,
  GET_CURRENT_USER,
  GET_CURRENT_USER_SUCCESS,
  GET_CURRENT_USER_ERROR,
  GET_USER_FEE_RATE_SUCCESS,
  GET_USER_FEE_TIER_RATE_SUCCESS,
  SEND_MESSAGE,
  SEND_MESSAGE_ERROR,
  SEND_MESSAGE_SUCCESS,
  RESET_MESSAGE,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  currentUser: false,
  isAuthenticated: false,
  loading: false,
  error: false,
  token: '',
  userData: {},
  openSessionModal: false,
  redirect: '',
  payeeId: null,
  feeObject: null,
  feeTierObject: null,
  message: null,
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return state
        .set('openSessionModal', false)
        .set('loading', true)
        .set('isAuthenticated', false)
        .set('successful', false)
        .set('error', false);
    case LOGIN_ERROR:
      return state
        .set('isAuthenticated', false)
        .set('token', '')
        .set('error', true)
        .set('loading', false)
        .set('messages', action.data)
        .set('successful', false);
    case LOGIN_SUCCESS: {
      const { userData, token, redirect } = action.data;
      return state
        .set('openSessionModal', false)
        .set('error', false)
        .set('userData', userData)
        .set('isAuthenticated', true)
        .set('token', token)
        .set('loading', false)
        .set('successful', true)
        .set('redirect', redirect);
    }
    case REFRESH:
      return state
        .set('openSessionModal', false)
        .set('loading', true)
        .set('successful', false)
        .set('error', false);
    case REFRESH_ERROR: {
      return state
        .set('isAuthenticated', false)
        .set('token', '')
        .set('error', true)
        .set('loading', false)
        .set('messages', action.data)
        .set('successful', false);
    }
    case REFRESH_SUCCESS: {
      const { userData, token } = action.data;
      return state
        .set('openSessionModal', false)
        .set('error', false)
        .set('userData', userData)
        .set('isAuthenticated', true)
        .set('token', token)
        .set('loading', false)
        .set('successful', true);
    }
    case LOGOUT:
      return state
        .set('loading', true)
        .set('successful', false)
        .set('error', false)
        .set('openSessionModal', false);
    case LOGOUT_ERROR:
      return state
        .set('error', true)
        .set('loading', false)
        .set('successful', false)
        .set('openSessionModal', false);
    case LOGOUT_SUCCESS:
      return state
        .set('error', false)
        .set('userData', {})
        .set('isAuthenticated', false)
        .set('token', '')
        .set('loading', false)
        .set('successful', true)
        .set('openSessionModal', false);
    case CLOSE_SESSION:
      return state.set('openSessionModal', false);
    case EXPIRE_SESSION:
      return state.set('openSessionModal', true);
    case SET_PAYEE_ID:
      return state.set('payeeId', action.data);
    case GET_CURRENT_USER:
      return state.set('loading', true).set('error', null);
    case GET_CURRENT_USER_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('userData', action.data);
    case GET_CURRENT_USER_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('userData', null);
    case GET_USER_FEE_RATE_SUCCESS:
      return state.set('feeObject', action.data);
    case GET_USER_FEE_TIER_RATE_SUCCESS:
      return state.set('feeTierObject', action.data);
    case SEND_MESSAGE:
      return state
        .set('loading', true)
        .set('error', null)
        .set('message', '');
    case SEND_MESSAGE_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error)
        .set('message', null);
    case SEND_MESSAGE_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('message', action.data);
    case RESET_MESSAGE:
      return state
        .set('loading', false)
        .set('error', null)
        .set('message', null);
    default:
      return state;
  }
}

export default appReducer;
