import { createSelector } from 'reselect';

const selectGlobal = (state) => state.get('global');
const selectRoute = state => state.get('route');

const makeSelectCurrentUser = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('userData')
);

const makeSelectIsAuthenticated = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('isAuthenticated')
);

const makeSelectLoading = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('loading')
);

const makeSelectError = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('error')
);

const makeSelectOpenSessionModal= () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('openSessionModal')
);

const makeSelectSessionDuration= () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('sessionDuration')
);

const makeSelectTimeOutId= () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('timeOutId')
);

const makeSelectMessage = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('message')
);

const makeSelectLocation = () =>
  createSelector(selectRoute, routeState => routeState.get('location').toJS());

export {
  selectGlobal,
  makeSelectIsAuthenticated,
  makeSelectLoading,
  makeSelectError,
  makeSelectCurrentUser,
  makeSelectLocation,
  makeSelectOpenSessionModal,
  makeSelectSessionDuration,
  makeSelectTimeOutId,
  makeSelectMessage,
};
