/*
 *
 * ChangeUserNamePage constants
 *
 */

export const DEFAULT_ACTION = 'app/ChangeUserNamePage/DEFAULT_ACTION';

export const GET_PROFILE = 'app/ChangeUserNamePage/GET_PROFILE';
export const GET_PROFILE_SUCCESS = 'app/ChangeUserNamePage/GET_PROFILE_SUCCESS';
export const GET_PROFILE_ERROR = 'app/ChangeUserNamePage/GET_PROFILE_ERROR';

export const UPDATE_USERNAME = 'app/ChangeUserNamePage/UPDATE_USERNAME';
export const UPDATE_USERNAME_SUCCESS =
  'app/ChangeUserNamePage/UPDATE_USERNAME_SUCCESS';
export const UPDATE_USERNAME_ERROR =
  'app/ChangeUserNamePage/UPDATE_USERNAME_ERROR';
