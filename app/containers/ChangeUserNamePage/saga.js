import { all, takeLatest, call, put } from 'redux-saga/effects';
import request from 'utils/request';
import { APP } from 'config';
import { GET_PROFILE, UPDATE_USERNAME } from './constants';
import {
  getProfileSuccess,
  getProfileError,
  updateUserNameSuccess,
  updateUserNameError,
} from './actions';
import { optionsFormData } from '../../utils/options';

export function* runGetProfile() {
  const API_URI = APP.API_URL;
  try {
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userProfile = yield call(
      request,
      `${API_URI}/current_user/`,
      optionsFormData('GET', null, token),
    );
    if (userProfile.id) {
      yield put(getProfileSuccess(userProfile));
    } else {
      yield put(getProfileError(userProfile));
    }
  } catch (error) {
    yield put(getProfileError(error));
  }
}

export function* updateUserName(form) {
  try {
    const API_URI = APP.API_URL;
    const { user, data } = form.data;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const userRequest = yield call(
      request,
      `${API_URI}/user/account/${user.id}/`,
      optionsFormData('GET', null, token),
    );
    if (userRequest.id) {
      const formData = new FormData();
      formData.append('email', userRequest.email);
      formData.append('username', data.newUsername);
      formData.append('password', userRequest.password);
      formData.append('first_name', userRequest.first_name);
      formData.append('last_name', userRequest.last_name);
      formData.append('business_name', userRequest.business_name);
      formData.append('user_type', userRequest.user_type);
      formData.append('primary_contact_name', userRequest.primary_contact_name);
      formData.append('is_first_time_member', userRequest.is_first_time_member);
      formData.append(
        'primary_contact_number',
        userRequest.primary_contact_number,
      );
      formData.append(
        'principal_city_of_business',
        userRequest.principal_city_of_business,
      );
      formData.append(
        'business_contact_number',
        userRequest.business_contact_number,
      );
      const userUpdateRequest = yield call(
        request,
        `${API_URI}/user/account/${user.id}/`,
        optionsFormData('PUT', formData, token),
      );
      if (userUpdateRequest.id) {
        yield put(updateUserNameSuccess(userUpdateRequest));
        yield call(
          request,
          `${API_URI}/update/user/notif`,
          optionsFormData('POST', formData, token),
        );
      } else {
        yield put(updateUserNameError(userUpdateRequest.message));
      }
    }
  } catch (error) {
    yield put(updateUserNameError(error));
  }
}

export function* getProfileWatcher() {
  yield takeLatest(GET_PROFILE, runGetProfile);
}

export function* updateUserNameWatcher() {
  yield takeLatest(UPDATE_USERNAME, updateUserName);
}

export default function* rootSaga() {
  yield all([getProfileWatcher(), updateUserNameWatcher()]);
}
