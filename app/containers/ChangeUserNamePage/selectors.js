import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the changeUserNamePage state domain
 */
const selectGlobal = state => state.get('global');
const selectChangeUserNamePageDomain = state =>
  state.get('changeUserNamePage', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by ChangeUserNamePage
 */

const makeSelectChangeUserNamePage = () =>
  createSelector(selectChangeUserNamePageDomain, substate => substate.toJS());

const makeSelectCurrentUser = () =>
  createSelector(selectGlobal, globalState => globalState.get('userData'));

export default makeSelectChangeUserNamePage;
export { selectChangeUserNamePageDomain, makeSelectCurrentUser };
