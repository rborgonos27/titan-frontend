import { fromJS } from 'immutable';
import changeUserNamePageReducer from '../reducer';

describe('changeUserNamePageReducer', () => {
  it('returns the initial state', () => {
    expect(changeUserNamePageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
