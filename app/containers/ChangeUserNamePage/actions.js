/*
 *
 * ChangeUserNamePage actions
 *
 */

import {
  GET_PROFILE,
  GET_PROFILE_ERROR,
  GET_PROFILE_SUCCESS,
  UPDATE_USERNAME,
  UPDATE_USERNAME_ERROR,
  UPDATE_USERNAME_SUCCESS,
} from './constants';

export function getProfile() {
  return {
    type: GET_PROFILE,
  };
}

export function getProfileSuccess(data) {
  return {
    type: GET_PROFILE_SUCCESS,
    data,
  };
}

export function getProfileError(error) {
  return {
    type: GET_PROFILE_ERROR,
    error,
  };
}

export function updateUserName(data) {
  return {
    type: UPDATE_USERNAME,
    data,
  };
}

export function updateUserNameError(error) {
  return {
    type: UPDATE_USERNAME_ERROR,
    error,
  };
}

export function updateUserNameSuccess(data) {
  return {
    type: UPDATE_USERNAME_SUCCESS,
    data,
  };
}
