/*
 * ChangeUserNamePage Messages
 *
 * This contains all the text for the ChangeUserNamePage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ChangeUserNamePage.header',
    defaultMessage: 'Update User Name',
  },
  username: {
    id: 'app.containers.ChangeUserNamePage.username',
    defaultMessage: 'User Name',
  },
  changeUsername: {
    id: 'app.containers.ChangeUserNamePage.changeUsername',
    defaultMessage: 'Update User Name',
  },
});
