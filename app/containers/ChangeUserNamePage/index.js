/**
 *
 * ChangeUserNamePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import StandardModal from 'components/StandardModal';
import Gravatar from 'react-gravatar';
import { Divider, Grid, Form, Input, Button, Message } from 'semantic-ui-react';
import { Loading } from 'components/Utils';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectChangeUserNamePage, {
  makeSelectCurrentUser,
} from './selectors';
import { getProfile, updateUserName } from './actions';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export class ChangeUserNamePage extends React.Component {
  state = {
    newUsername: '',
    openConfirmModal: false,
    username: '',
  };
  componentWillMount() {
    this.props.getProfile();
  }
  componentWillReceiveProps(nextProps) {
    const { data } = nextProps.changeusernamepage;
    this.setState({
      openConfirmModal: false,
      username: data.username,
    });
  }
  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  openModal = modalState => this.setState({ [modalState]: true });
  closeModal = modalState => this.setState({ [modalState]: false });
  submitData = () => {
    const { currentUser } = this.props;
    const submitData = {
      user: currentUser,
      data: this.state,
    };
    this.props.updateUserName(submitData);
  };
  disabledButton = () => {
    const { data } = this.props.changeusernamepage;
    const { newUsername } = this.state;
    let disabledButton = true;
    disabledButton = newUsername === '';
    if (data.username === newUsername) return true;
    return disabledButton;
  };
  buildConfirmModal = () => {
    const { openConfirmModal } = this.state;
    const { loading } = this.props.changeusernamepage;
    return (
      <StandardModal
        headerText="Proceed this Request?"
        open={openConfirmModal}
        onClose={() => this.closeModal('openConfirmModal')}
        onActionText="CONFIRM"
        onContinue={() => this.submitData()}
        btnDisabled={loading}
      >
        <div style={{ textAlign: 'left' }}>
          Are you sure you want to update your profile info?
        </div>
      </StandardModal>
    );
  };
  render() {
    const { currentUser, changeusernamepage } = this.props;
    const { data, loading, error } = changeusernamepage;
    const { username } = this.state;
    const errorMessage = error ? (
      <Grid.Row>
        <Grid.Column>
          <div className="profileLabel">
            <Message error>{error}</Message>
          </div>
        </Grid.Column>
      </Grid.Row>
    ) : null;
    return (
      <ContainerWrapper>
        <CardBox>
          <h3>
            <FormattedMessage {...messages.header} />
          </h3>
          {!loading ? this.buildConfirmModal() : null}
          <Loading loading={loading} />
          <Grid>
            <Grid.Row>
              <Grid.Column>
                <Gravatar
                  email={currentUser.email}
                  size={100}
                  rating="pg"
                  default="identicon"
                  className="img-circle"
                />
                <span style={{ padding: 10, fontSize: 25 }}>
                  {`${data.first_name} ${data.last_name}`}
                </span>
                {`@${username}`}
              </Grid.Column>
            </Grid.Row>
            <Divider />
            {errorMessage}
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.username} />
                </div>
                <div className="profileData">{data.username}</div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <div className="profileLabel">
                  <FormattedMessage {...messages.changeUsername} />
                </div>
                <div className="profileData">
                  <Form>
                    <Form.Field>
                      <Input
                        onChange={this.handleInput}
                        type="text"
                        name="newUsername"
                        id="newUsername"
                        size="large"
                        value={this.state.newUsername}
                      />
                    </Form.Field>
                  </Form>
                </div>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <div className="buttonSaveProfile" style={{ paddingLeft: 20 }}>
                <Button
                  onClick={() => this.openModal('openConfirmModal')}
                  color="orange"
                  disabled={this.disabledButton()}
                >
                  Update User Name
                </Button>
              </div>
            </Grid.Row>
          </Grid>
        </CardBox>
      </ContainerWrapper>
    );
  }
}

ChangeUserNamePage.propTypes = {
  getProfile: PropTypes.func.isRequired,
  updateUserName: PropTypes.func.isRequired,
  changeusernamepage: PropTypes.any,
  currentUser: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  changeusernamepage: makeSelectChangeUserNamePage(),
  currentUser: makeSelectCurrentUser(),
});

const mapDispatchToProps = { getProfile, updateUserName };

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'changeUserNamePage', reducer });
const withSaga = injectSaga({ key: 'changeUserNamePage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ChangeUserNamePage);
