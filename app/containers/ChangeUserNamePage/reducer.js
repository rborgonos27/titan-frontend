/*
 *
 * ChangeUserNamePage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  GET_PROFILE,
  GET_PROFILE_ERROR,
  GET_PROFILE_SUCCESS,
  UPDATE_USERNAME,
  UPDATE_USERNAME_ERROR,
  UPDATE_USERNAME_SUCCESS,
} from './constants';

export const initialState = fromJS({
  loading: false,
  data: {},
  error: null,
  errorList: [],
});

function changeUserNamePageReducer(state = initialState, action) {
  switch (action.type) {
    case GET_PROFILE:
      return state.set('loading', true).set('error', null);
    case GET_PROFILE_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('data', action.data);
    case GET_PROFILE_ERROR:
      return state.set('loading', false).set('error', action.error);
    case UPDATE_USERNAME:
      return state.set('loading', true).set('error', null);
    case UPDATE_USERNAME_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null)
        .set('data', action.data);
    case UPDATE_USERNAME_ERROR:
      return state.set('loading', false).set('error', action.error);
    default:
      return state;
  }
}

export default changeUserNamePageReducer;
