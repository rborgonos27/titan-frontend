/*
 * Tools Messages
 *
 * This contains all the text for the Tools component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Tools.header',
    defaultMessage: 'Admin Tool',
  },
});
