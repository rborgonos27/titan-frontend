/*
 *
 * Tools reducer
 *
 */

import { fromJS } from 'immutable';
import {
  CLEAR_EMAIL,
  CLEAR_EMAIL_ERROR,
  CLEAR_EMAIL_SUCCESS,
} from './constants';

export const initialState = fromJS({
  loading: false,
  data: null,
  error: null,
});

function toolsReducer(state = initialState, action) {
  const { data, error } = action;
  switch (action.type) {
    case CLEAR_EMAIL:
      return state
        .set('loading', true)
        .set('data', null)
        .set('error', null);
    case CLEAR_EMAIL_SUCCESS:
      return state
        .set('loading', false)
        .set('data', data)
        .set('error', null);
    case CLEAR_EMAIL_ERROR:
      return state
        .set('loading', false)
        .set('error', error)
        .set('data', null);
    default:
      return state;
  }
}

export default toolsReducer;
