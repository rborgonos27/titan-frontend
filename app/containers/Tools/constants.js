/*
 *
 * Tools constants
 *
 */

export const DEFAULT_ACTION = 'app/Tools/DEFAULT_ACTION';

export const CLEAR_EMAIL = 'app/Tools/CLEAR_EMAIL';
export const CLEAR_EMAIL_SUCCESS = 'app/Tools/CLEAR_EMAIL_SUCCESS';
export const CLEAR_EMAIL_ERROR = 'app/Tools/CLEAR_EMAIL_ERROR';
