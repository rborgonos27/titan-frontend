import { all, takeLatest, call, put } from 'redux-saga/effects';
import { APP } from 'config';
import request from 'utils/request';
import { optionsFormData } from 'utils/options';
import { CLEAR_EMAIL } from './constants';
import { clearEmailError, clearEmailSuccess } from './actions';

export function* runClearEmail(emailData) {
  try {
    const API_URI = APP.API_URL;
    const token = yield JSON.parse(localStorage.getItem('token'));
    const { email } = emailData.data;
    const emailFormData = new FormData();
    emailFormData.append('email', email);
    const emailResponse = yield call(
      request,
      `${API_URI}/tools/clear_email/?email=${email}`,
      optionsFormData('POST', emailFormData, token),
    );
    if (emailResponse.success) {
      yield put(clearEmailSuccess(emailResponse.message));
    } else {
      yield put(clearEmailError(emailResponse.message));
    }
  } catch (error) {
    yield put(clearEmailError(error));
  }
}
export function* clearEmailWatcher() {
  yield takeLatest(CLEAR_EMAIL, runClearEmail);
}
export default function* rootSaga() {
  yield all([clearEmailWatcher()]);
}
