import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the tools state domain
 */

const selectToolsDomain = state => state.get('tools', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by Tools
 */

const makeSelectTools = () =>
  createSelector(selectToolsDomain, substate => substate.toJS());

export default makeSelectTools;
export { selectToolsDomain };
