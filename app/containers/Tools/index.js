/**
 *
 * Tools
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import CardBox from 'components/CardBox';
import ContainerWrapper from 'components/ContainerWrapper';
import { Title, HelpText, Loading } from 'components/Utils';
import { Divider, Button, Input, Message } from 'semantic-ui-react';
import StandardModal from 'components/StandardModal';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectTools from './selectors';
import { clearEmail } from './actions';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
export class Tools extends React.Component {
  state = {
    openEmailModal: false,
    email: '',
  };
  onChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };
  submitData = () => {
    const openEmailModal = false;
    const email = '';
    const emailData = this.state;
    this.setState({ openEmailModal, email });
    this.props.clearEmail(emailData);
  };
  buildEmailModal = () => {
    const { openEmailModal, email } = this.state;
    return (
      <StandardModal
        open={openEmailModal}
        onContinue={this.submitData}
        onActionText="Run"
        btnDisabled={email === ''}
        onClose={() => this.setState({ openEmailModal: false })}
      >
        <div>
          <h5>Enter Email Address</h5>
          <Input
            type="email"
            name="email"
            fluid
            onChange={this.onChange}
            value={email}
          />
        </div>
      </StandardModal>
    );
  };
  render() {
    const { loading, data, error } = this.props.tools;
    return (
      <ContainerWrapper>
        <CardBox>
          {this.buildEmailModal()}
          <Title>
            <FormattedMessage {...messages.header} />
          </Title>
          <Divider />
          {data ? <Message positive>{data}</Message> : null}
          {error ? <Message negative>{error}</Message> : null}
          <Loading loading={loading} />
          <HelpText>
            {`Click "Clear Email" button to free email address.`}
          </HelpText>
          <Button
            positive
            onClick={() => this.setState({ openEmailModal: true })}
          >
            Clear Email
          </Button>
        </CardBox>
      </ContainerWrapper>
    );
  }
}

Tools.propTypes = {
  clearEmail: PropTypes.func.isRequired,
  tools: PropTypes.any.isRequired,
};

const mapStateToProps = createStructuredSelector({
  tools: makeSelectTools(),
});

const mapDispatchToProps = { clearEmail };

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'tools', reducer });
const withSaga = injectSaga({ key: 'tools', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Tools);
