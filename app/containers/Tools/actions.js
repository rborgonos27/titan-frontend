/*
 *
 * Tools actions
 *
 */

import {
  DEFAULT_ACTION,
  CLEAR_EMAIL,
  CLEAR_EMAIL_SUCCESS,
  CLEAR_EMAIL_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function clearEmail(data) {
  return {
    type: CLEAR_EMAIL,
    data,
  };
}

export function clearEmailSuccess(data) {
  return {
    type: CLEAR_EMAIL_SUCCESS,
    data,
  };
}

export function clearEmailError(error) {
  return {
    type: CLEAR_EMAIL_ERROR,
    error,
  };
}
