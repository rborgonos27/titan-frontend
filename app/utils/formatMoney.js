const formatMoney = (
  amount,
  decimalCount = 2,
  decimal = '.',
  thousands = ',',
) => {
  try {
    /*eslint-disable*/
    let myDecimalCount = Math.abs(decimalCount);
    myDecimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? '-' : '';

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(myDecimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return (
      negativeSign +
      (j ? i.substr(0, j) + thousands : '') +
      i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands) +
      (myDecimalCount
        ? decimal +
        Math.abs(amount - i).toFixed(myDecimalCount).slice(2) : '')
    );
  } catch (e) {
    console.log(e);
  }
  return null;
  /* eslint-enable */
};

export { formatMoney };
