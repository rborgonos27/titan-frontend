import request from 'utils/request';
import { options } from 'utils/options';
import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect';
import { APP } from 'config';
import { loginSuccessAction, loginErrorAction } from 'containers/App/actions';

export function reqToken(data) {
  const url = `${APP.API_URL}/api/v1/login`;
  return request(url, options('POST', data));
}

export function checkAuthorization(store) {
  const storedToken = localStorage.getItem('token');
  const userData = JSON.parse(localStorage.getItem('userData'));
  if (storedToken) {
    const token = storedToken;
    const createdDate = new Date(localStorage.getItem('token_created'));
    const created = Math.round(createdDate.getTime() / 1000);
    const ttl = 3600;
    const expiry = created + ttl;
    const now = Math.round(new Date().getTime() / 1000);
    if (now > expiry) {
      localStorage.removeItem('token');
      localStorage.removeItem('userData');
      localStorage.removeItem('token_created');
      return store.dispatch(loginErrorAction('Token Expired'));
    }
    return store.dispatch(loginSuccessAction({ token, userData }));
  }
  return store.dispatch(loginErrorAction('No token'));
}

export const userIsAuthenticated = connectedRouterRedirect({
  redirectPath: '/login',
  authenticatedSelector: state => state.get('global').get('isAuthenticated'),
  wrapperDisplayName: 'UserIsAuthenticated',
});

export const userIsNotAuthenticated = connectedRouterRedirect({
  redirectPath: state => state.get('global').get('redirect'),
  authenticatedSelector: state => !state.get('global').get('isAuthenticated'),
  wrapperDisplayName: 'UserIsNotAuthenticated',
});
