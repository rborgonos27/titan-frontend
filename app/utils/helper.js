const getParams = url => {
  /* eslint-disable */
  let params = {};
  const parser = document.createElement('a');
  parser.href = url;
  const query = parser.search.substring(1);
  const vars = query.split('&');
  for (let i = 0; i < vars.length; i++) {
    const pair = vars[i].split('=');
    params[pair[0]] = decodeURIComponent(pair[1]);
  }
  return params;
  /* eslint-enable */
};

const decodeUrlValue = value => {
  const urlDecode = decodeURIComponent(value);
  const beforeDecode = urlDecode.split("'");
  const decodedValue = atob(beforeDecode[1]);
  return decodedValue;
};

const generateRandomPassword = length => {
  /* eslint-disable */
  const chars =
    'abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890';
  let pass = '';
  for (let x = 0; x < length; x++) {
    const i = Math.floor(Math.random() * chars.length);
    pass += chars.charAt(i);
  }
  return pass;
  /* eslint-enable */
};

export { getParams, decodeUrlValue, generateRandomPassword };
