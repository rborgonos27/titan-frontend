const options = (method, body = null) => {
  const opt = {
    headers: {
      'Content-type': 'application/json',
      Accept: 'application/json',
    },
    method,
  };
  if (body) opt.body = JSON.stringify(body);
  return opt;
};

const optionsFormData = (method, body = null, auth = null) => {
  const opt = {
    headers: {
      Accept: 'application/json',
      Authorization: auth ? `JWT ${auth}` : '',
    },
    method,
  };
  if (body) opt.body = body;
  return opt;
};

export { options, optionsFormData };
